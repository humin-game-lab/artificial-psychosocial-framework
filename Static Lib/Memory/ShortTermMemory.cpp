#include "Memory/ShortTermMemory.hpp"
#include "Memory/MemorySystem.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
MemoryAndEvent::~MemoryAndEvent()
{
    memory.m_isGarbage=true;
    selfEvent.m_isGarbage=true;
}

//////////////////////////////////////////////////////////////////////////
MemoryAndEvent::MemoryAndEvent(MemoryAndEvent const& source)
    : memory(source.memory)
    , selfEvent(source.selfEvent)
    , repeatedTimes(source.repeatedTimes)
{

}

//////////////////////////////////////////////////////////////////////////
MemoryAndEvent& MemoryAndEvent::operator=(MemoryAndEvent const& source)
{
    memory=source.memory;
    selfEvent =source.selfEvent;
    repeatedTimes=source.repeatedTimes;
    return *this;
}

//////////////////////////////////////////////////////////////////////////
ShortTermMemory::ShortTermMemory(MemorySystem* memSys)
    : m_memSystem(memSys)
{
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::Initialize(Memory const& mem)
{
    InvalidateAll();
    m_memories[0].memory = mem;   
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::InvalidateAll()
{
    for (int i = 0; i < STM_CAPACITY; i++) {
        m_memories[i].memory.m_isGarbage = true;
        m_memories[i].selfEvent.m_isGarbage = true;
        m_memories[i].repeatedTimes = 0;
    }
    m_latestIdx = 0;
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::AddNewMemory(Memory const& newMem)
{
    //TODO add consideration for repeated self state
    m_memories[m_latestIdx].memory = (newMem);
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::AddNewEvent(GameEvent const& newSelfEvent)
{
    //TODO add repeated self action?
    m_memories[m_latestIdx].selfEvent = (newSelfEvent);
    m_memories[m_latestIdx].repeatedTimes = 1;
    m_latestIdx = GetCorrectedMemoryIdx(m_latestIdx+1);
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::SearchForSingleMemoryAndEvent(GetMemorySimilarity memFunc, Memory const& mem, GetEventSimilarity eventFunc, 
    GameEvent const& event, unsigned int maxLength, std::vector<MemoryTrace>& foundTraces) const
{
    for (int i = 0; i < STM_CAPACITY; i++) {
        MemoryAndEvent const& current = m_memories[i];
        if (!current.memory.m_isGarbage && !current.selfEvent.m_isGarbage &&
            memFunc(mem, current.memory) >= MEMORY_SIMILAR_THRESHOLD && 
            eventFunc(event, current.selfEvent) >= EVENT_SIMILAR_THRESHOLD) {
            MemoryTrace temp;
            foundTraces.push_back(temp);
            GetMemories(i, i + (int)maxLength - 1, foundTraces.back());
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::SearchForQueryList(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces) const
{
    if (list.IsEmpty() || maxRange < 1) {
        return;
    }

    std::vector<std::vector<int>> routes; 
    std::vector<int> starts;
    QueryNode* first = list.PopFront();
    bool isLastMemory = true;
    //Find starts
    if (first->type == eQueryType::QUERY_MEMORY) {
        MemoryQueryNode* memNode = static_cast<MemoryQueryNode*>(first);
        FindAllMemoryIndexes(memNode->pair.second,memNode->pair.first,MEMORY_SIMILAR_THRESHOLD, starts);        
    }
    else if (first->type == eQueryType::QUERY_EVENT) {
        EventQueryNode* eventNode = static_cast<EventQueryNode*>(first);
        FindAllMemoryIndexes(eventNode->pair.second, eventNode->pair.first, EVENT_SIMILAR_THRESHOLD, starts);
        isLastMemory = false;
    }
    else {  //not valid, return
        return;
    }

    //Build starts
    for (int idx : starts) {
        std::vector<int> route;
        route.push_back(idx);
        routes.push_back(route);
    }

    ProgressWithQueryList(list, routes, maxRange-1, isLastMemory);

    //Build traces
    for (std::vector<int> const& route : routes) {
        MemoryTrace temp;
        foundTraces.push_back(temp);
        GenerateMemoryTrace(route, foundTraces.back());
    }
}

//////////////////////////////////////////////////////////////////////////
Memory const* ShortTermMemory::GetLatestMemory() const
{
    return &m_memories[m_latestIdx].memory;
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::GetMemory(int idx, Memory& mem) const
{
    mem = m_memories[GetCorrectedMemoryIdx(idx)].memory;
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::GetRecentMemories(unsigned int length, MemoryTrace& memories) const
{
    GetMemories(m_latestIdx-length+1,m_latestIdx, memories);
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::GetMemories(int startIdx, int endIdx, MemoryTrace& memories) const
{
    bool started = false;
    for (int i = startIdx; i <= endIdx; i++) {
        int cur = GetCorrectedMemoryIdx(i);
        MemoryAndEvent const& current = m_memories[cur];
        if (current.memory.m_isGarbage && !started) {
            continue;
        }
        else if (current.memory.m_isGarbage && started) {
            break;
        }

        memories.emplace_back(current);
        started = true;
    }
}

//////////////////////////////////////////////////////////////////////////
int ShortTermMemory::GetCorrectedMemoryIdx(int rawIdx) const
{
    int idx = rawIdx%STM_CAPACITY;
    if (idx < 0) {
        idx = STM_CAPACITY+idx;
    }
    return idx;
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::CleanDecayedMemory()
{
    for (MemoryAndEvent& memEvent : m_memories) {
        if (!memEvent.memory.m_isGarbage &&
            memEvent.memory.IsFaded()) {
            memEvent.memory.m_isGarbage = true;
        }

        if (!memEvent.selfEvent.m_isGarbage &&
            memEvent.selfEvent.m_actionInfo && 
            memEvent.selfEvent.IsFaded()) {
            memEvent.selfEvent.m_isGarbage = true;
            memEvent.repeatedTimes = 0;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::Decay()
{
    for(MemoryAndEvent& memEvent : m_memories){
        if (!memEvent.memory.m_isGarbage) {
            memEvent.memory.Forget();
        }
        if (!memEvent.selfEvent.m_isGarbage) {
            memEvent.selfEvent.Forget();
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::Mutate()
{
    for (MemoryAndEvent& memEvent : m_memories) {
        if (!memEvent.memory.m_isGarbage) {
            memEvent.memory.Mutate();
        }
        if (!memEvent.selfEvent.m_isGarbage) {
            memEvent.selfEvent.Mutate();
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::UpdateForRecentRepetition(unsigned int memoryLength, Memory const& general, size_t repititionTimes)
{
    //TODO improve repetition based on general memory?
    general;
    constexpr float REPEAT_ENHANCE_DELTA = .05f;

    if (repititionTimes <= 1) { //comes from STM, ignore
        return;
    }

    int startIdx =m_latestIdx - (int)memoryLength;

    float enhanceFactor = RangeMapLinearFloat(std::sqrtf((float)repititionTimes), 1.f,5.f,0.f,1.f);
    enhanceFactor = ClampFloat(enhanceFactor, 0.f, 1.f) * REPEAT_ENHANCE_DELTA;
    for (int i = startIdx; i <= m_latestIdx; i++) {
        int idx = GetCorrectedMemoryIdx(i);
        if (m_memories[idx].memory.m_isGarbage || m_memories[idx].selfEvent.m_isGarbage) {
            continue;
        }

        //mental
        APF_Lib::MentalState& ms = m_memories[idx].memory.m_mentalState;
        ms.PolarizeSmoothStartAndEnd(enhanceFactor);

        //Action certainty
        GameEvent& event = m_memories[idx].selfEvent;
        event.Enhance(enhanceFactor);

        m_memories[idx].repeatedTimes += 1;
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::GenerateMemoryTrace(std::vector<int> const& route, MemoryTrace& trace) const
{
    //assume all memory and event are not garbage
    int startIdx = route.front();
    int endIdx = route.back()<startIdx? route.back()+STM_CAPACITY:route.back();
    GetMemories(startIdx,endIdx, trace);
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::FindAllMemoryIndexes(Memory const& target, GetMemorySimilarity memFunc, float similarity, std::vector<int>& indexes) const
{
    for (int i = 0; i < STM_CAPACITY; i++) {
        Memory const& mem = m_memories[i].memory;
        if (!mem.m_isGarbage && memFunc(target, mem) > similarity) {
            indexes.push_back(i);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::FindAllMemoryIndexes(GameEvent const& target, GetEventSimilarity eventFunc, float similarity, std::vector<int>& indexes) const
{
    //TODO always consider trace start with memory
    for (int i = 0; i < STM_CAPACITY; i++) {
        GameEvent const& event = m_memories[i].selfEvent;
        if (!m_memories[i].memory.m_isGarbage && !event.m_isGarbage && eventFunc(target, event) > similarity) {
            indexes.push_back(i);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::ProgressWithQueryList(QueryList const& list, std::vector<std::vector<int>>& routes, unsigned int maxRange, bool isLastMemory) const
{
    if (maxRange < 0 || routes.empty() || list.IsEmpty()) {
        if (list.IsEmpty() && maxRange > 0 && !routes.empty()) {    //extend routes to max range, query finished
            for (std::vector<int>& route : routes) {
                route.push_back(route.back()+maxRange);
            }
        }
        
        return;
    }

    QueryNode* curNode = list.PopFront();
    if (curNode->type == eQueryType::QUERY_MEMORY) {
        MemoryQueryNode* memNode = static_cast<MemoryQueryNode*>(curNode);
        GetMemorySimilarity func = memNode->pair.first;
        Memory const& target = memNode->pair.second;

        if (isLastMemory) { //ignore action
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<int>& route = routes[i];
                MemoryAndEvent const& last = m_memories[route.back()];
                if (last.selfEvent.m_isGarbage) {   //delete route
                    routes.erase(routes.begin()+i);
                    totalSize--;
                    continue;
                }

                int nextIdx = GetCorrectedMemoryIdx(route.back()+1);
                Memory const& nextMem = m_memories[nextIdx].memory;
                if (!nextMem.m_isGarbage && func(target, nextMem) > MEMORY_SIMILAR_THRESHOLD) {
                    route.push_back(nextIdx);
                    i++;
                }
                else {
                    routes.erase(routes.begin() + i);
                    totalSize--;
                }
            }
        }
        else {  //calculate memory
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<int>& route = routes[i];
                int nextIdx =GetCorrectedMemoryIdx(route.back()+1);
                Memory const& nextMem = m_memories[nextIdx].memory;
                if (!nextMem.m_isGarbage && func(target, nextMem) > MEMORY_SIMILAR_THRESHOLD) {
                    route.push_back(nextIdx);
                    i++;
                }
                else {
                    routes.erase(routes.begin()+i);
                    totalSize--;
                }
            }
        }

        isLastMemory = true;
    }
    else if(curNode->type==eQueryType::QUERY_EVENT){
        EventQueryNode* eventNode = static_cast<EventQueryNode*>(curNode);
        GetEventSimilarity func = eventNode->pair.first;
        GameEvent const& target = eventNode->pair.second;

        if (isLastMemory) { //calculate action
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<int>& route = routes[i];
                GameEvent const& event = m_memories[route.back()].selfEvent;
                if (!event.m_isGarbage && func(target, event) > EVENT_SIMILAR_THRESHOLD) {
                    i++;
                }
                else {
                    routes.erase(routes.begin() + i);
                    totalSize--;
                }
            }
        }
        else {  //ignore memory
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<int>& route = routes[i];
                int nextIdx = GetCorrectedMemoryIdx(route.back() + 1);
                GameEvent const& nextEvent = m_memories[nextIdx].selfEvent;
                if (!m_memories[nextIdx].memory.m_isGarbage && !nextEvent.m_isGarbage && 
                    func(target, nextEvent) > EVENT_SIMILAR_THRESHOLD) {
                    route.push_back(nextIdx);
                    i++;
                }
                else {
                    routes.erase(routes.begin() + i);
                    totalSize--;
                }
            }
        }
        isLastMemory = false;
    }

    ProgressWithQueryList(list,routes, maxRange-1, isLastMemory);
}

//////////////////////////////////////////////////////////////////////////
int ShortTermMemory::GetOwnerIndex() const
{
    return m_memSystem->GetOwnerIndex();
}

//////////////////////////////////////////////////////////////////////////
void ShortTermMemory::ClearGameObject(GameObject const* objectToDelete)
{
    for (MemoryAndEvent& memEvent : m_memories) {
        if (memEvent.selfEvent.m_target == objectToDelete) {
            memEvent.selfEvent.m_target = nullptr;
        }
    }
}
