#include "Memory/GameState.hpp"
#include "Memory/MemorySystem.hpp"
#include "Memory/GameActor.hpp"
#include "APF/APF_Lib.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
float GameState::GetSimilarity(GameState const& stateA, GameState const& stateB)
{
    //objects similarity
    float object = 0.f;
    if(!stateA.objectPerceivedStates.empty() && !stateB.objectPerceivedStates.empty()){
        float objSimilar = 0.f;
        size_t intersect = 0;
        size_t emptyA=0;
        for (auto it = stateA.objectPerceivedStates.begin(); it != stateA.objectPerceivedStates.end(); it++) {
            if (it->second.states.empty()) {
                emptyA++;
            }
            else{
                ObjectState const* obStateB = stateB.FindStateOfObject(it->first);
                if (obStateB != nullptr) {
                    objSimilar += ObjectState::GetSimilarity(it->second, *obStateB);
                    intersect++;
                }
            }
        }
        size_t emptyB=0;
        for (auto it = stateB.objectPerceivedStates.begin(); it != stateB.objectPerceivedStates.end(); it++) {
            if (it->second.states.empty()) {
                emptyB++;
            }
        }
        size_t objTotal = stateA.objectPerceivedStates.size() + stateB.objectPerceivedStates.size() - intersect - emptyA - emptyB;
        if (objTotal == 0) {
            object=1.f;
        }
        else{
            object = objSimilar / (float)(objTotal);
        }
    }
    else if (stateA.objectPerceivedStates.empty() && stateB.objectPerceivedStates.empty()) {
        //Similarity is 1 if both objectStates are empty
        object = 1.f;
    }

    //actors
    float actor = 0.f;
    if(!stateA.actorPerceivedStates.empty() && !stateB.actorPerceivedStates.empty()){
        float actorSimilar = 0.f;
        size_t intersect = 0;
        for (auto it = stateA.actorPerceivedStates.begin(); it != stateA.actorPerceivedStates.end(); it++) {
            ActorState const* actorStateB = stateB.FindStateOfActor(it->first);
            if (actorStateB != nullptr) {
                actorSimilar += ActorState::GetSimilarity(it->second, *actorStateB);
                intersect++;
            }
        }
        actor = actorSimilar / (float)(stateA.actorPerceivedStates.size() + stateB.actorPerceivedStates.size()-intersect);
    }
    else if (stateA.actorPerceivedStates.empty() && stateB.actorPerceivedStates.empty()) {
        //Similarity is 1 if both actorStates are empty
        actor=1.f;
    }

    return object*.9f+actor*.1f;
}

//////////////////////////////////////////////////////////////////////////
void GameState::CombineStates(GameState const& stateA, GameState const& stateB, float alpha, GameState& combined)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    //object states
    for (auto it = stateA.objectPerceivedStates.begin(); it != stateA.objectPerceivedStates.end(); it++) {
        ObjectState const* obStateB = stateB.FindStateOfObject(it->first);
        if (obStateB != nullptr) {  //object state in both A and B
            ObjectState obCombine;
            ObjectState::CombineStates(it->second, *obStateB, alpha, obCombine);
            combined.objectPerceivedStates[it->first] = obCombine;
        }
        else {  //object only in A
            combined.objectPerceivedStates[it->first] = ObjectState::GetMultipliedObjectState(it->second, alpha);
        }
    }
    for (auto it = stateB.objectPerceivedStates.begin(); it != stateB.objectPerceivedStates.end(); it++) {
        ObjectState const* obStateA = stateA.FindStateOfObject(it->first);
        if (obStateA == nullptr) {  //object state only in B
            combined.objectPerceivedStates[it->first] = ObjectState::GetMultipliedObjectState(it->second, 1.f-alpha);
        }
    }

    //actor states
    for (auto it = stateA.actorPerceivedStates.begin(); it != stateA.actorPerceivedStates.end(); it++) {
        ActorState const* actorStateB = stateB.FindStateOfActor(it->first);
        if (actorStateB != nullptr) {  //state in both A and B
            ActorState obCombine;
            ActorState::CombineStates(it->second, *actorStateB, alpha, obCombine);
            combined.actorPerceivedStates[it->first] = obCombine;
        }
        else {  //states only in A
             ActorState::GetMultipliedState(it->second, alpha, combined.actorPerceivedStates[it->first]);
        }
    }
    for (auto it = stateB.actorPerceivedStates.begin(); it != stateB.actorPerceivedStates.end(); it++) {
        ActorState const* actorStateA = stateA.FindStateOfActor(it->first);
        if (actorStateA == nullptr) {  //state only in B
            ActorState::GetMultipliedState(it->second, 1.f-alpha, combined.actorPerceivedStates[it->first]);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void GameState::UpdateStateForObject(GameObject* object)
{
    //TODO store actual perceived state, instead of direct copying
    ObjectState const* state = object->GetSelfState();
    if (state) {
        objectPerceivedStates[object] = *state;
    }
    else {
        objectPerceivedStates[object].Clear();
    }
}

//////////////////////////////////////////////////////////////////////////
void GameState::UpdateStateForActor(GameActor* actor)
{
    ActorState& state = actorPerceivedStates[actor];    
    //use actor's real-time emotion
    APF_Lib::MM::MemorySystem::GetMemoryAPF()->GetANPCsMentalState(state.mentalState, actor->GetIndex());

    ObjectState const* self = actor->GetSelfState();
    if (self) {
        //TODO store perceived state, instead of direct copying
        state.basicInfo = *self;
    }
    else {
        state.basicInfo.Clear();
    }
    state.currentEvent = actor->GetCurrentAction();
}

//////////////////////////////////////////////////////////////////////////
void GameState::Clear()
{
    objectPerceivedStates.clear();
    actorPerceivedStates.clear();
}

//////////////////////////////////////////////////////////////////////////
void GameState::Mutate()
{
    for (auto obIt = objectPerceivedStates.begin(); obIt != objectPerceivedStates.end(); obIt++) {
        obIt->second.Mutate();
    }
    for (auto actorIt = actorPerceivedStates.begin(); actorIt != actorPerceivedStates.end(); actorIt++) {
        actorIt->second.Mutate();
    }
}

//////////////////////////////////////////////////////////////////////////
void GameState::Forget()
{
    for (auto obIt = objectPerceivedStates.begin(); obIt != objectPerceivedStates.end(); obIt++) {
        obIt->second.Forget();
    }
    for (auto actorIt = actorPerceivedStates.begin(); actorIt != actorPerceivedStates.end(); actorIt++) {
        actorIt->second.Forget();
    }
}

//////////////////////////////////////////////////////////////////////////
ObjectState const* GameState::FindStateOfObject(GameObject* object) const
{
    std::map<GameObject*, ObjectState>::const_iterator it=objectPerceivedStates.find(object);
    if (it != objectPerceivedStates.end()) {
        return &it->second;
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
ActorState const* GameState::FindStateOfActor(GameActor* actor) const
{
    std::map<GameActor*, ActorState>::const_iterator it = actorPerceivedStates.find(actor);
    if (it != actorPerceivedStates.end()) {
        return &it->second;
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
bool GameState::IsEmpty() const
{
    return objectPerceivedStates.empty() && actorPerceivedStates.empty();
}

//////////////////////////////////////////////////////////////////////////
bool GameState::IsFaded() const
{
    for (std::map<GameObject*, ObjectState>::const_iterator it = objectPerceivedStates.cbegin();
        it != objectPerceivedStates.cend(); it++) {
        if (!it->second.IsFaded()) {
            return false;
        }
    }

    for (std::map<GameActor*, ActorState>::const_iterator it = actorPerceivedStates.cbegin();
        it != actorPerceivedStates.cend(); it++) {
        if (!it->second.IsFaded()) {
            return false;
        }
    }

    return true;
}
