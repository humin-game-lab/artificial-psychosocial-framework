#include "Memory/ObjectState.hpp"
#include "Memory/MemCommon.hpp"
#include "APF/LibCommon.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
float ObjectState::GetSimilarity(ObjectState const& stateA, ObjectState const& stateB)
{
    //states
    float jaccard=0.f;
    if (!stateA.states.empty() && !stateB.states.empty()) {
        float similarSum = 0.f;
        size_t commonNum = 0;
        for (ObjectStateAspect const& aspectA : stateA.states) {
            ObjectStateAspect const* aspectB = stateB.FindAspect(aspectA);
            if (aspectB != nullptr) {
                similarSum += ObjectStateAspect::GetSimilarity(aspectA, *aspectB);
                commonNum++;
            }
        }
        //TODO refine states compare
        size_t totalUnique = stateA.states.size()+stateB.states.size()-commonNum;
        jaccard = similarSum / (float)(totalUnique);
    }
    else if(stateA.states.empty() && stateB.states.empty()){
        jaccard=1.f;
    }

    return jaccard;
}

//////////////////////////////////////////////////////////////////////////
void ObjectState::CombineStates(ObjectState const& stateA, ObjectState const& stateB, float alpha, ObjectState& combined)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    if (!stateB.states.empty()) {
        for (ObjectStateAspect const& aspectA : stateA.states) {
            ObjectStateAspect const* aspectB = stateB.FindAspect(aspectA);
            if (aspectB != nullptr) {   //states in both a and b
                combined.states.emplace_back(aspectB->state, Interpolate(aspectA.confidence, aspectB->confidence, alpha));
            }
            else {  //states only in A
                combined.states.emplace_back(aspectA.state, aspectA.confidence * alpha);
            }
        }
    }
    
    if (!stateA.states.empty()) {
        for (ObjectStateAspect const& aspectB : stateB.states) {
            if (stateA.FindAspect(aspectB) == nullptr) {    //states only in b
                combined.states.emplace_back(aspectB.state, aspectB.confidence * (1.f - alpha));
            }
        }
    }    
}

//////////////////////////////////////////////////////////////////////////
ObjectState ObjectState::GetMultipliedObjectState(ObjectState const& state, float alpha)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    ObjectState result = state;
    for (ObjectStateAspect& s : result.states) {
        s.confidence *= alpha;
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
ObjectState::ObjectState(ObjectState const& copyFrom)
    : states(copyFrom.states)
{
}

//////////////////////////////////////////////////////////////////////////
void ObjectState::Mutate()
{
    for (ObjectStateAspect& aspect : states) {
        float delta = STATE_CONFIDENCE_MUTATE_EXTENT* (RandomZeroToOne()-.5f);
        aspect.confidence = ClampFloat(aspect.confidence+delta, 0.f, 1.f);
    }
}

//////////////////////////////////////////////////////////////////////////
void ObjectState::Forget()
{
    for (ObjectStateAspect& aspect : states) {
        float delta = -.5f*STATE_CONFIDENCE_MUTATE_EXTENT * RandomZeroToOne();
        aspect.confidence = ClampFloat(aspect.confidence + delta, 0.f, 1.f);
    }
}

//////////////////////////////////////////////////////////////////////////
void ObjectState::Clear()
{
    states.clear();
}

//////////////////////////////////////////////////////////////////////////
bool ObjectState::IsFaded() const
{
    for (ObjectStateAspect const& aspect : states) {
        if (aspect.confidence > STATE_CONFIDENCE_MUTATE_EXTENT) {
            return false;
        }
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////
float ObjectState::GetTotalDeltaValence() const
{
    float deltaObjectStates = MIN_UNIT_VALUE;
    for (ObjectStateAspect const& aspect : states) {
        deltaObjectStates += aspect.confidence * aspect.state->valenceDelta;
    }
    return deltaObjectStates;
}

//////////////////////////////////////////////////////////////////////////
ObjectStateAspect const* ObjectState::FindAspect(ObjectStateAspect const& aspect) const
{
    for (std::vector<ObjectStateAspect>::const_iterator it = states.cbegin(); it != states.cend(); it++) {
        if (*it == aspect) {
            return &(*it);
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
ObjectStateAspect* ObjectState::FindAspect(std::string const& stateName) const
{
    for (std::vector<ObjectStateAspect>::const_iterator it = states.cbegin(); it != states.cend(); it++) {
        if ((*it).state->name == stateName) {
            return const_cast<ObjectStateAspect*>(&(*it));
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
bool ObjectState::SetState(std::string const& stateName, float certainty)
{
    for (std::vector<ObjectStateAspect>::iterator it = states.begin(); it != states.end(); it++) {
        if ((*it).state->name == stateName) {
            (*it).confidence = certainty;
            return true;
        }
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
bool ObjectState::AddState(std::string const& stateName, float certainty)
{
    StateInfo const* stateInfo = StateInfo::GetStateFromName(stateName);
    if (stateInfo) {
        states.emplace_back(&stateInfo->state, certainty);
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
float ObjectStateAspect::GetSimilarity(ObjectStateAspect const& aspectA, ObjectStateAspect const& aspectB)
{
    constexpr float STATE_CONFIDENCE_MAX_SIMILAR = .95f; //for inside object state aspect

    State const* stateA = aspectA.state;
    State const* stateB = aspectB.state;
    if (stateA->name != stateB->name) {
        return 0.f;
    }

    //TODO confidence counts up to 0.3
    float confidenceDelta = AbsFloat(aspectA.confidence-aspectB.confidence);    //0-1
    return (1.f-STATE_CONFIDENCE_MAX_SIMILAR)+(1.f-confidenceDelta)*STATE_CONFIDENCE_MAX_SIMILAR;
}

//////////////////////////////////////////////////////////////////////////
ObjectStateAspect::ObjectStateAspect(State const* s, float confident)
    : state(s)
    , confidence(confident)
{

}

//////////////////////////////////////////////////////////////////////////
bool ObjectStateAspect::operator==(ObjectStateAspect const& other) const
{
    return (other.state->name==state->name);
}


//////////////////////////////////////////////////////////////////////////
StateInfo const* StateInfo::GetStateFromName(std::string const& name)
{
    for (StateInfo const& info : sPossibleStates) {
        if (info.state.name == name) {
            return &info;
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
StateInfo::StateInfo(std::string const& name, float delta)
{
    state.name = name;
    state.valenceDelta = delta;
}

std::vector<APF_Lib::MM::StateInfo> APF_Lib::MM::StateInfo::sPossibleStates;
