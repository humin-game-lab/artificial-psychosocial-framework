#include "Memory/GameObject.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
GameObject::GameObject(GameTime const& birthTime)
    : m_birthTime(birthTime)
{    
}

//////////////////////////////////////////////////////////////////////////
void GameObject::SetNewState(ObjectState const& newState)
{
    if (m_selfState == nullptr) {
        m_selfState = new ObjectState();
    }

    *m_selfState = newState;
}

//////////////////////////////////////////////////////////////////////////
void GameObject::SetName(std::string const& newName)
{
    m_name = newName;
}

//////////////////////////////////////////////////////////////////////////
void GameObject::SetAPFIndex(int newIdx)
{
    m_apfIdx = newIdx;
}

//////////////////////////////////////////////////////////////////////////
GameObject::~GameObject()
{
    if (m_selfState) {
        //TODO delete could cause memory managing bug "Read access violation on null+X address"
        delete m_selfState;
        m_selfState = nullptr;
    }
}
