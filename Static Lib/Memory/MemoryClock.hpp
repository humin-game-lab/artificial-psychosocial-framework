#pragma once

#include "Memory/Delegate.hpp"
#include "Memory/GameTime.hpp"

namespace APF_Lib {
    namespace MM {
        class MemoryClock
        {
        public:
            Delegate<GameTime const&> onMinuteIncrease;
            Delegate<GameTime const&> onHourIncrease;
            Delegate<GameTime const&> onDayIncrease;
            Delegate<GameTime const&> onMonthIncrease;
            Delegate<GameTime const&> onYearIncrease;

        public:
            MemoryClock();
            ~MemoryClock();

            void Update(float deltaSeconds);
            void Pause();
            void Resume();
            void SetScale(float newScale);

            float    GetMutatePossibility() const;
            GameTime GetTotalTime() const { return m_totalTime; }
            float    GetScale() const { return m_timeScale; }
            bool     IsPaused() const { return m_isPaused; }
            std::string ToString() const;

        public:
            static void BeginFrame(float deltaSeconds);
            static void SetGlobalTimeScale(float timeScale);
            static float GetGlobalTimeScale();

        private:
            GameTime m_totalTime;

            float m_timeScale = 1.f;
            bool m_isPaused = false;

            float m_minFrameTime = 0.f;
            float m_maxFrameTime = 1.f;
        };
    }
}
