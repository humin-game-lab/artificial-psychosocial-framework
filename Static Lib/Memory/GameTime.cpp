#include "Memory/GameTime.hpp"
#include "Memory/MemCommon.hpp"
#include "APF/LibCommon.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
GameTime::GameTime(int y, int mon, int d, int h, int mins, float sec)
    : m_years(y)
    , m_months(mon)
    , m_days(d)
    , m_hours(h)
    , m_minutes(mins)
    , m_seconds(sec)
{

}

//////////////////////////////////////////////////////////////////////////
float GameTime::GetTotalSeconds() const
{
    return (float)((((m_years*12+m_months)*30+m_days)*24+m_hours)*60+m_minutes)*60.f+m_seconds;
}

//////////////////////////////////////////////////////////////////////////
float GameTime::GetMutatePossibility() const
{
    float possibility = RangeMapLinearFloat(GetTotalSeconds(), GameTime::MIN_MUTATE_TIME_SECONDS,
        GameTime::MAX_MUTATE_TIME_SECONDS, 0.f, 1.f);
    return ClampFloat(possibility, 0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator>(GameTime const& compare) const
{
    if (CompareWith(compare) > 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator<(GameTime const& compare) const
{
    if (CompareWith(compare) < 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator<=(GameTime const& compare) const
{
    if (CompareWith(compare) <= 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
GameTime const GameTime::operator+(GameTime const& addOn) const
{
    GameTime result;
    float totalSec = m_seconds+addOn.m_seconds;
    int tempMin = (int)totalSec/60;
    result.m_seconds = totalSec-(float)tempMin*60.f;

    int totalMin = m_minutes + addOn.m_minutes + tempMin;
    int tempHour = totalMin/60;
    result.m_minutes = totalMin-tempHour*60;

    int totalHour = m_hours + addOn.m_hours + tempHour;
    int tempDay = totalHour/24;
    result.m_hours = totalHour-tempDay*24;

    int totalDay = m_days+addOn.m_days+tempDay;
    int tempMon = totalDay/30;
    result.m_days = totalDay-tempMon*30;

    int totalMon = m_months+addOn.m_months + tempMon;
    int tempYear = totalMon/12;
    result.m_months = totalMon - tempYear*12;

    result.m_years = m_years+addOn.m_years+tempYear;
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string GameTime::ToString() const
{
    return std::to_string(m_years) + " year " +
        std::to_string(m_months) + " month " +
        std::to_string(m_days) + " day " + 
        std::to_string(m_hours) + " hour " +
        std::to_string(m_minutes) + " min " +
        std::to_string(m_seconds) + " sec";
}

//////////////////////////////////////////////////////////////////////////
GameTime GameTime::GetRandomGameTimeInRange(int yStart, int monStart, int dStart, int hStart, int minStart, float sStart, int yEnd, int monEnd, int dEnd, int hEnd, int minEnd, float sEnd)
{
    GameTime result;
    bool alreadyLarge = false;
    result.m_years = RandomIntInRange(yStart, yEnd);
    if (yEnd > yStart && result.m_years!=yEnd) {
        monStart=dStart=hStart=minStart=0; sStart=0.f;
        monEnd=MAX_MONTHS;
        dEnd=MAX_DAYS;
        hEnd=MAX_HOURS;
        minEnd=MAX_MINUTES;
        sEnd=MAX_SECONDS;
        alreadyLarge = true;
    }
    result.m_months = RandomIntInRange(monStart, monEnd);
    if (!alreadyLarge && monEnd > monStart && result.m_months!=monEnd) {
        dStart = hStart = minStart = 0; sStart = 0.f;
        dEnd = MAX_DAYS;
        hEnd = MAX_HOURS;
        minEnd = MAX_MINUTES;
        sEnd = MAX_SECONDS;
        alreadyLarge = true;
    }
    result.m_days = RandomIntInRange(dStart, dEnd);
    if (!alreadyLarge && dEnd > dStart && result.m_days!=dEnd) {
        hStart = minStart = 0; sStart = 0.f;
        hEnd = MAX_HOURS;
        minEnd = MAX_MINUTES;
        sEnd = MAX_SECONDS;
        alreadyLarge = true;
    }
    result.m_hours = RandomIntInRange(hStart, hEnd);
    if (!alreadyLarge && hEnd > hStart && result.m_hours!=hEnd) {
        minStart = 0; sStart = 0.f;
        minEnd = MAX_MINUTES;
        sEnd = MAX_SECONDS;
        alreadyLarge = true;
    }
    result.m_minutes = RandomIntInRange(minStart, minEnd);
    if (!alreadyLarge && minEnd > minStart && result.m_minutes!=minEnd) {
        sStart = 0.f;
        sEnd = MAX_SECONDS;
    }
    result.m_seconds = RandomFloatInRange(sStart, sEnd);
    return result;
}

const float GameTime::MAX_SECONDS = 59.9f;
GameTime GameTime::MIN_MUTATE_TIME(0,0,0,0,30,0.f);
GameTime GameTime::MAX_MUTATE_TIME(10,0,0,0,0,0.f);

const float GameTime::MIN_MUTATE_TIME_SECONDS=GameTime::MIN_MUTATE_TIME.GetTotalSeconds();
const float GameTime::MAX_MUTATE_TIME_SECONDS=GameTime::MAX_MUTATE_TIME.GetTotalSeconds();

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator!=(GameTime const& compare) const
{
    if (CompareWith(compare) != 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator==(GameTime const& compare) const
{
    if (CompareWith(compare) == 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
bool GameTime::operator>=(GameTime const& compare) const
{
    if (CompareWith(compare) >= 0) {
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
int GameTime::CompareWith(GameTime const& compare) const
{
    if (m_years > compare.m_years) {
        return 1;
    }
    else if (m_years < compare.m_years) {
        return -1;
    }
    else if (m_months > compare.m_months) { //years equal
        return 1;
    }
    else if (m_months < compare.m_months) {
        return -1;
    }
    else if (m_days > compare.m_days) {   //months equal
        return 1;
    }
    else if (m_days < compare.m_days) {
        return -1;
    }
    else if (m_hours > compare.m_hours) {   //days equal
        return 1;
    }
    else if (m_hours < compare.m_hours) {
        return -1;
    }
    else if (m_minutes > compare.m_minutes) {   //hours equal
        return 1;
    }
    else if (m_minutes < compare.m_minutes) {
        return -1;
    }
    else if (m_seconds > compare.m_seconds) {   //seconds equal
        return 1;
    }
    else if (m_seconds < compare.m_seconds) {
        return -1;
    }
    else {  //totally equal
        return 0;
    }
}
