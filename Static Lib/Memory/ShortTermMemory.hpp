#pragma once

#include "Memory/Memory.hpp"
#include "Memory/MemCommon.hpp"

namespace APF_Lib {
    namespace MM {
        struct GameEvent;
        class MemorySystem;
        class QueryList;

        struct MemoryAndEvent   //trace in STM and episodic
        {        
        public:
            MemoryAndEvent() = default;
            ~MemoryAndEvent();
            MemoryAndEvent(MemoryAndEvent const& source);
            MemoryAndEvent& operator=(MemoryAndEvent const& source);

        public:
            Memory memory;
            GameEvent selfEvent;   //the action follows memory

            unsigned int repeatedTimes = 0;
        };

        class ShortTermMemory
        {
            friend class MemorySystem;

        public:
            static constexpr int STM_CAPACITY = 20;

            ShortTermMemory(MemorySystem* memSys);
            ~ShortTermMemory() = default;

            void Initialize(Memory const& mem);
            void InvalidateAll();
            void ClearGameObject(GameObject const* objectToDelete);

            void AddNewMemory(Memory const& newMem);
            void AddNewEvent(GameEvent const& newSelfEvent);

            //search
            void SearchForSingleMemoryAndEvent(GetMemorySimilarity memFunc, Memory const& mem, GetEventSimilarity eventFunc, 
                GameEvent const& event, unsigned int maxLength, std::vector<MemoryTrace>& foundTraces) const;
            void SearchForQueryList(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces) const;

            int GetOwnerIndex() const;
            Memory const* GetLatestMemory() const;
            void GetMemory(int idx, Memory& mem) const;
            void GetRecentMemories(unsigned int length, MemoryTrace& memories) const;
            MemorySystem* GetMemorySystem() const {return m_memSystem;}

        protected:
            void CleanDecayedMemory();
            virtual void Decay();   //decay all valid STM
            virtual void Mutate();  //mutate all valid STM
            virtual void UpdateForRecentRepetition(unsigned int memoryLength, Memory const& general, size_t repititionTimes); //update recent memories for repetition

            //accessor
            void GetMemories(int startIdx, int endIdx, MemoryTrace& memories) const;
            int GetCorrectedMemoryIdx(int rawIdx) const;
            void GenerateMemoryTrace(std::vector<int> const& route, MemoryTrace& trace) const;

            //find
            void FindAllMemoryIndexes(Memory const& target, GetMemorySimilarity memFunc, float similarity, std::vector<int>& indexes) const;
            void FindAllMemoryIndexes(GameEvent const& target, GetEventSimilarity eventFunc, float similarity, std::vector<int>& indexes) const;
            void ProgressWithQueryList(QueryList const& list, std::vector<std::vector<int>>& routes, unsigned int maxRange, bool isLastMemory) const;

        protected:
            MemorySystem* m_memSystem = nullptr;
            MemoryAndEvent m_memories[STM_CAPACITY];
            int m_latestIdx = 0;
        };
    }
}

