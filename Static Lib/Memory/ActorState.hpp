#pragma once

#include "Memory/ObjectState.hpp"
#include "Memory/GameEvent.hpp"
#include "APF/MentalState.hpp"

namespace APF_Lib {
    namespace MM {
        struct ActorState
        {        
        public:
            static float GetSimilarity(ActorState const& stateA, ActorState const& stateB);
            static bool CombineStates(ActorState const& stateA, ActorState const& stateB, float alpha, ActorState& combined);//when event unequal, return false
            static void GetMultipliedState(ActorState const& state, float alpha, ActorState& multiplied);

            virtual void Mutate();
            virtual void Forget();

            virtual bool IsFaded() const;

        public:
            ObjectState basicInfo;              //(my) perceived objectState for this actor   
            APF_Lib::MentalState mentalState;    //(my) perceived emotion for this actor
            GameEvent currentEvent;      //current doing action

        };
    }
}
