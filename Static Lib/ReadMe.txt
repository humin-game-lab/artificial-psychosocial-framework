Author: Lawrence (Jake) Klinkert
Date: 04 May 2020

This is the static library for the Artificial Psychosocial Framework.

Any functions created here can be used by outside programs as long as those programs have the .h file (located in Code/file.hpp) and the .lib file (located in  x64/Release/APF_Lib.lib)

For any new functions created
	- best to rebuild the library
	- then find the associated .h files and .lib files to include into the other software

For Unreal builds
	- only rebuild in release x64
	- Make sure the .lib file, has ends with .x64.lib