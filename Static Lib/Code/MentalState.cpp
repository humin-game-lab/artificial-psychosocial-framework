#include "MentalState.hpp"

#include <cstdlib>
#include <cstring>


static constexpr float VALENCE_MOODS_COMBINE_WEIGHTS[2] = { .3f, .6f };


void APF_Lib::MentalState::GetMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES]) const
{
	memcpy(out_mental_state, &m_mentalState, NUM_MENTAL_FEATURES * sizeof(float));
}


void APF_Lib::MentalState::GetMentalState(MentalState& out_mental_state) const
{
    memcpy(out_mental_state.m_mentalState, &m_mentalState, NUM_MENTAL_FEATURES*sizeof(float));
}

void APF_Lib::MentalState::GetValence(float(&out_valence_state)[NUM_VALENCE_FEATURES]) const
{
	memcpy(out_valence_state, &m_mentalState[MENTAL_FEATURE_POSITIVE], NUM_VALENCE_FEATURES * sizeof(float));
}


void APF_Lib::MentalState::GetMood(float(&out_mood_state)[NUM_MOOD_FEATURES]) const
{
	memcpy(out_mood_state, &m_mentalState[MENTAL_FEATURE_PLEASED], NUM_MOOD_FEATURES * sizeof(float));
}


void APF_Lib::MentalState::GetEmotion(float(&out_emotional_state)[NUM_EMOTION_FEATURES]) const
{
	memcpy(out_emotional_state, &m_mentalState[MENTAL_FEATURE_HOPE], NUM_EMOTION_FEATURES * sizeof(float));
}


void APF_Lib::MentalState::GetFeeling(float(&out_feeling_state)[NUM_FEELING_FEATURES]) const
{
	memcpy(out_feeling_state, &m_mentalState[MENTAL_FEATURE_SATISFACTION], NUM_FEELING_FEATURES * sizeof(float));
}


void APF_Lib::MentalState::SetMentalState(const float(&new_mental_state)[NUM_MENTAL_FEATURES])
{
	memcpy(&m_mentalState, new_mental_state, NUM_MENTAL_FEATURES * sizeof(float));

	for (int feature_idx = 0; feature_idx < NUM_MENTAL_FEATURES; ++feature_idx)
	{
		m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::SetMentalState(const MentalState& new_mental_state)
{
    memcpy(&m_mentalState, new_mental_state.m_mentalState, NUM_MENTAL_FEATURES * sizeof(float));

    for (int feature_idx = 0; feature_idx < NUM_MENTAL_FEATURES; ++feature_idx)
    {
        m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    }
}

void APF_Lib::MentalState::SetValence(const float(& new_valence_state)[NUM_VALENCE_FEATURES])
{
	memcpy(&m_mentalState[MENTAL_FEATURE_POSITIVE], new_valence_state, NUM_VALENCE_FEATURES * sizeof(float));

	for (int feature_idx = MENTAL_FEATURE_POSITIVE; feature_idx <= MENTAL_FEATURE_NEGATIVE; ++feature_idx)
	{
		m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


void APF_Lib::MentalState::SetMood(const float(& new_mood_state)[NUM_MOOD_FEATURES])
{
	memcpy(&m_mentalState[MENTAL_FEATURE_PLEASED], new_mood_state, NUM_MOOD_FEATURES * sizeof(float));

	for (int feature_idx = MENTAL_FEATURE_PLEASED; feature_idx <= MENTAL_FEATURE_DISAPPROVING; ++feature_idx)
	{
		m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


void APF_Lib::MentalState::SetEmotion(const float(& new_emotional_state)[NUM_EMOTION_FEATURES])
{
	memcpy(&m_mentalState[MENTAL_FEATURE_HOPE], new_emotional_state, NUM_EMOTION_FEATURES * sizeof(float));

	for (int feature_idx = MENTAL_FEATURE_HOPE; feature_idx <= MENTAL_FEATURE_REPROACH; ++feature_idx)
	{
		m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


void APF_Lib::MentalState::SetFeeling(const float(& new_feeling_state)[NUM_FEELING_FEATURES])
{
	memcpy(&m_mentalState[MENTAL_FEATURE_SATISFACTION], new_feeling_state, NUM_FEELING_FEATURES * sizeof(float));

	for (int feature_idx = MENTAL_FEATURE_SATISFACTION; feature_idx <= MENTAL_FEATURE_ANGER; ++feature_idx)
	{
		m_mentalState[feature_idx] = ClampFloat(m_mentalState[feature_idx], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


float& APF_Lib::MentalState::operator[](const int idx)
{
	return m_mentalState[idx];
}


const float& APF_Lib::MentalState::operator[](const int idx) const
{
	return m_mentalState[idx];
}


float& APF_Lib::MentalState::operator[](const MentalFeature enumeration)
{
	return m_mentalState[enumeration];
}


const float& APF_Lib::MentalState::operator[](const MentalFeature enumeration) const
{
	return m_mentalState[enumeration];
}


APF_Lib::MentalState APF_Lib::MentalState::operator+(MentalState& mental_state)
{
	MentalState new_mental_state;

	for (int mental_feature = 0; mental_feature < NUM_MENTAL_FEATURES; ++mental_feature)
	{
		new_mental_state[mental_feature] = this->m_mentalState[mental_feature] + mental_state[mental_feature];
		new_mental_state[mental_feature] = ClampFloat(new_mental_state[mental_feature], 
			MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}

	return new_mental_state;
}


void APF_Lib::MentalState::operator+=(MentalState const& mental_state)
{
	for (int mental_feature = 0; mental_feature < NUM_MENTAL_FEATURES; ++mental_feature)
	{
		m_mentalState[mental_feature] += mental_state[mental_feature];
		
		m_mentalState[mental_feature] = ClampFloat(m_mentalState[mental_feature], 
			MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


void APF_Lib::MentalState::operator+=(float(& out_mental_state)[NUM_MENTAL_FEATURES])
{
	for (int mental_feature = 0; mental_feature < NUM_MENTAL_FEATURES; ++mental_feature)
	{
		m_mentalState[mental_feature] += out_mental_state[mental_feature];

		m_mentalState[mental_feature] = ClampFloat(m_mentalState[mental_feature],
			MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


void APF_Lib::MentalState::operator-=(float(& out_mental_state)[NUM_MENTAL_FEATURES])
{
	for (int mental_feature = 0; mental_feature < NUM_MENTAL_FEATURES; ++mental_feature)
	{
		m_mentalState[mental_feature] -= out_mental_state[mental_feature];

		m_mentalState[mental_feature] = ClampFloat(m_mentalState[mental_feature],
			MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	}
}


APF_Lib::MentalState APF_Lib::MentalState::GenerateRandomMentalState()
{
	MentalState new_mental_state;

	for (int mental_feature = 0; mental_feature < NUM_MENTAL_FEATURES; ++mental_feature)
	{
		new_mental_state[mental_feature] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	}

	return new_mental_state;
}


APF_Lib::MentalState APF_Lib::MentalState::GenerateDecayMentalState()
{
	MentalState decay_mental_state;

	for (int emo_idx = 0; emo_idx < NUM_MENTAL_FEATURES; ++emo_idx)
	{
		decay_mental_state[emo_idx] = MENTAL_FEATURE_DECAY_VALUE;
	}

	return decay_mental_state;
}

std::string APF_Lib::MentalState::ToString()
{
	return std::string("{" + 
		std::to_string(m_mentalState[MENTAL_FEATURE_POSITIVE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_NEGATIVE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_PLEASED]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISPLEASED]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_LIKING]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISLIKING]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_APPROVING]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISAPPROVING]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_HOPE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_FEAR]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_JOY]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISTRESS]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_LOVE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_HATE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_INTEREST]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISGUST]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_PRIDE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_SHAME]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_ADMIRATION]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_REPROACH]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_SATISFACTION]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_FEARS_CONFIRMED]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_RELIEF]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_DISAPPOINTMENT]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_HAPPY_FOR]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_RESENTMENT]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_GLOATING]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_PITY]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_GRATIFICATION]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_REMORSE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_GRATITUDE]) + ", " +
		std::to_string(m_mentalState[MENTAL_FEATURE_ANGER]) + "} \n");
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityForFloatInMentalStates(float a, float b)
{
    float result = RangeMapLinearFloat(AbsFloat(a - b), MENTAL_SIMILAR_MIN, MENTAL_SIMILAR_MAX, 1.f, 0.f);
    return ClampFloat(result, 0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityForMoods(float const (&moodA)[NUM_MOOD_FEATURES], float const (&moodB)[NUM_MOOD_FEATURES], float const (&weights)[2])
{
    float fraction = 1.f / (float)NUM_MOOD_FEATURES;
    float result = 0.f;
    for (int i = 0; i < NUM_MOOD_FEATURES; i++) {
        float temp = GetSimilarityForFloatInMentalStates(moodA[i], moodB[i]);
        if (i % 2 == 0) {
            temp *= weights[0] + weights[0];
        }
        else {
            temp *= weights[1] + weights[1];
        }
        result += temp;
    }
    result *= fraction;
    return result;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityForMentalStates(MentalState const& msA, MentalState const& msB)
{
    //TODO refine mental state similar compare
    float valenceSimilar = GetSimilarityInValenceForMentalStates(msA, msB);
    float moodSimilar = GetSimilarityInMoodForMentalStates(msA, msB);
    float emotionSimilar = GetSimilarityInEmotionForMentalStates(msA, msB);
    float feelingSimilar = GetSimilarityInFeelingForMentalStates(msA, msB);

    return valenceSimilar * .7f + (moodSimilar + emotionSimilar + feelingSimilar) * .1f;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityInValenceForMentalStates(MentalState const& msA, MentalState const& msB)
{
    float valenceA[NUM_VALENCE_FEATURES];
    msA.GetValence(valenceA);
    float valenceB[NUM_VALENCE_FEATURES];
    msB.GetValence(valenceB);

    float similaritySum = 0.f;
    for (int i = 0; i < NUM_VALENCE_FEATURES; i++) {
        similaritySum += GetSimilarityForFloatInMentalStates(valenceA[i], valenceB[i]);
    }
    return similaritySum / (float)NUM_VALENCE_FEATURES;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityInMoodForMentalStates(MentalState const& msA, MentalState const& msB)
{
    float moodA[NUM_MOOD_FEATURES];
    msA.GetMood(moodA);
    float moodB[NUM_MOOD_FEATURES];
    msB.GetMood(moodB);

    float similaritySum = 0.f;
    for (int i = 0; i < NUM_MOOD_FEATURES; i++) {
        similaritySum += GetSimilarityForFloatInMentalStates(moodA[i], moodB[i]);
    }
    return similaritySum / (float)NUM_MOOD_FEATURES;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityInEmotionForMentalStates(MentalState const& msA, MentalState const& msB)
{
    float emotionA[NUM_EMOTION_FEATURES];
    msA.GetEmotion(emotionA);
    float emotionB[NUM_EMOTION_FEATURES];
    msB.GetEmotion(emotionB);

    float similaritySum = 0.f;
    for (int i = 0; i < NUM_EMOTION_FEATURES; i++) {
        similaritySum += GetSimilarityForFloatInMentalStates(emotionA[i], emotionB[i]);
    }
    return similaritySum / (float)NUM_EMOTION_FEATURES;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetSimilarityInFeelingForMentalStates(MentalState const& msA, MentalState const& msB)
{
    float feelingA[NUM_FEELING_FEATURES];
    msA.GetFeeling(feelingA);
    float feelingB[NUM_FEELING_FEATURES];
    msB.GetFeeling(feelingB);

    float similaritySum = 0.f;
    for (int i = 0; i < NUM_FEELING_FEATURES; i++) {
        similaritySum += GetSimilarityForFloatInMentalStates(feelingA[i], feelingB[i]);
    }
    return similaritySum / (float)NUM_FEELING_FEATURES;
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::PolarizeSmoothStartAndEnd(float delta)
{
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        float raw = InverseSmoothStartAndEnd(m_mentalState[i]);
        float actualDelta = raw > 0.5 ? delta : -delta;
        m_mentalState[i] = SmoothStartAndEnd(ClampFloat(raw + actualDelta, MIN_UNIT_VALUE, MAX_UNIT_VALUE));
    }
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetAveragedMental() const
{
    float sum = 0.f;
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        sum += m_mentalState[i];
    }
    return sum / NUM_MENTAL_FEATURES;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetMaxMental() const
{
    float max = 0.f;
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        if (max < m_mentalState[i]) {
            max = m_mentalState[i];
        }
    }
    return max;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MentalState::GetMentalStateSalience() const
{
    float average = GetAveragedMental();
    float maxValue = GetMaxMental();
    return .3f * average + .7f * maxValue;
}

//////////////////////////////////////////////////////////////////////////
bool APF_Lib::MentalState::IsSaturated() const
{
    return (GetMaxMental() >= MAX_UNIT_VALUE);
}

//////////////////////////////////////////////////////////////////////////
bool APF_Lib::MentalState::IsLowSaturated() const
{
    return (GetMaxMental() <= .7f);
}

//////////////////////////////////////////////////////////////////////////
bool APF_Lib::MentalState::IsFaded() const
{
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        if (m_mentalState[i] > MENTAL_DEGRADE_DELTA) {
            return false;
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::MultiplyFactor(float factor)
{
    //TODO refine to operator * ?
    factor = ClampFloat(factor, 0.f, 1.f);
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        m_mentalState[i] *= factor;
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::Decay()
{
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        float temp = InverseSmoothStartAndEnd(m_mentalState[i]);
        temp = temp - MENTAL_DEGRADE_DELTA * RandomZeroToOne();
        temp = ClampFloat(temp, 0.f, 1.f);
        m_mentalState[i] = SmoothStartAndEnd(temp);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::Mutate()
{
    constexpr float MENTAL_MUTATE_EXTENT = .05f;

    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        m_mentalState[i] += (RandomZeroToOne() - .5f) * MENTAL_MUTATE_EXTENT;
        m_mentalState[i] = ClampFloat(m_mentalState[i], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::SetFromRawAndProspect(MentalState const& raw, MentalState const& prospect, float similarity)
{
    MentalState rawCopy = raw;

    m_mentalState[MENTAL_FEATURE_HOPE] = prospect[MENTAL_FEATURE_POSITIVE];
    m_mentalState[MENTAL_FEATURE_FEAR] = prospect[MENTAL_FEATURE_NEGATIVE];

    //emotion based prospect
    m_mentalState[MENTAL_FEATURE_SATISFACTION] = ClampFloat(rawCopy[MENTAL_FEATURE_POSITIVE] *
        ClampFloat(RangeMapLinearFloat(prospect[MENTAL_FEATURE_POSITIVE] - rawCopy[MENTAL_FEATURE_POSITIVE], -.6f, .5f, 0.f, 1.f), 0.f, 1.2f), 0.f, 1.f);
    m_mentalState[MENTAL_FEATURE_FEARS_CONFIRMED] = ClampFloat(rawCopy[MENTAL_FEATURE_NEGATIVE]*
        ClampFloat(RangeMapLinearFloat(prospect[MENTAL_FEATURE_NEGATIVE]- rawCopy[MENTAL_FEATURE_NEGATIVE], -.6f, .5f, 0.f, 1.f), 0.f, 1.2f), 0.f, 1.f);
    m_mentalState[MENTAL_FEATURE_RELIEF] = ClampFloat((prospect[MENTAL_FEATURE_NEGATIVE] - rawCopy[MENTAL_FEATURE_NEGATIVE])*1.7f, 0.f, 1.f);
    m_mentalState[MENTAL_FEATURE_DISAPPOINTMENT] = ClampFloat((prospect[MENTAL_FEATURE_POSITIVE] - rawCopy[MENTAL_FEATURE_POSITIVE])*1.7f, 0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::GetDeltaMentalState(MentalState const& before, MentalState const& after, MentalState& delta)
{
    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        delta[i] = after[i] - before[i];
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::CombineMentalStates(MentalState const& msA, MentalState const& msB, float alpha, MentalState& combined)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
        combined[i] = Interpolate(msA[i], msB[i], alpha);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::CombineMentalStatesClamped(MentalState const& base, MentalState const& target, float baseMaxDelta, MentalState& combined)
{
    //valence
    for (int i = 0; i < MENTAL_FEATURE_PLEASED; i++) {
        float delta = target[i] - base[i];
        float signDelta = delta > 0.f ? 1.f : -1.f;
        delta = ClampFloat(AbsFloat(delta), 0.f, VALENCE_MOODS_COMBINE_WEIGHTS[0] * baseMaxDelta);
        combined[i] = ClampFloat(base[i] + delta * signDelta, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    }

    //moods
    for (int i = MENTAL_FEATURE_PLEASED; i < MENTAL_FEATURE_HOPE; i++) {
        float delta = target[i] - base[i];
        float signDelta = delta > 0.f ? 1.f : -1.f;
        delta = ClampFloat(AbsFloat(delta), 0.f, VALENCE_MOODS_COMBINE_WEIGHTS[1] * baseMaxDelta);
        combined[i] = ClampFloat(base[i] + delta * signDelta, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    }

    //emotions & feelings
    for (int i = MENTAL_FEATURE_HOPE; i < NUM_MENTAL_FEATURES; i++) {
        float delta = target[i] - base[i];
        float signDelta = delta > 0.f ? 1.f : -1.f;
        delta = ClampFloat(AbsFloat(delta), 0.f, baseMaxDelta);
        combined[i] = ClampFloat(base[i] + delta * signDelta, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::CombineMentalValences(MentalState const& msA, MentalState const& msB, float alpha, float(&valences)[NUM_VALENCE_FEATURES])
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    for (int i = 0; i < NUM_VALENCE_FEATURES; i++) {
        valences[i] = Interpolate(msA[i], msB[i], alpha);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MentalState::CombineMentalMoods(MentalState const& msA, MentalState const& msB, float alpha, float(&moods)[NUM_MOOD_FEATURES])
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    for (int i = 0; i < NUM_MOOD_FEATURES; i++) {
        int apfIdx = MENTAL_FEATURE_PLEASED + i;
        moods[i] = Interpolate(msA[apfIdx], msB[apfIdx], alpha);
    }
}
