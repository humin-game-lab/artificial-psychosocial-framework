from FiniteStateMachine import State
from FiniteStateMachine import Action

class StateMachine:
    """The main FSM class managing states and transitions."""

    def __init__(self, initial_state: State):
        self.initial_state = initial_state
        self.current_state = initial_state

    def update(self) -> list[Action]:
        """Update the state machine and return a list of actions."""

        triggered = None

        # Check transitions in the current state
        for transition in self.current_state.get_transitions():
            if transition.is_triggered():
                triggered = transition
                break

        # Handle state transition if any
        if triggered:
            target_state = triggered.get_target_state()
            actions = self.current_state.get_exit_actions()
            actions += triggered.get_actions()
            actions += target_state.get_entry_actions()
            self.current_state = target_state
            return actions
        else:
            return self.current_state.get_actions()