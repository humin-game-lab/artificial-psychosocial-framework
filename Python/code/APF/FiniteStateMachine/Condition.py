from abc import ABC, abstractmethod


class Condition(ABC):
    """Abstract base class for all conditions."""
    @abstractmethod
    def test(self) -> bool:
        """Evaluate the condition."""
        pass


class FloatCondition(Condition):
    """A condition based on a floating-point range."""
    def __init__(self, min_value: float, max_value: float, test_value: float):
        self.min_value = min_value
        self.max_value = max_value
        self.test_value = test_value

    def test(self) -> bool:
        return self.min_value <= self.test_value <= self.max_value


class AndCondition(Condition):
    """A condition that requires all conditions to be true."""

    def __init__(self, condition_a: Condition, condition_b: Condition):
        self.condition_a = condition_a
        self.condition_b = condition_b

    def test(self) -> bool:
        return self.condition_a.test() and self.condition_b.test()


class NotCondition(Condition):
    """A condition that inverts another condition's result."""

    def __init__(self, condition: Condition):
        self.condition = condition

    def test(self) -> bool:
        return not self.condition.test()


class OrCondition(Condition):
    """A condition that requires at least one condition to be true."""

    def __init__(self, condition_a: Condition, condition_b: Condition):
        self.condition_a = condition_a
        self.condition_b = condition_b

    def test(self) -> bool:
        return self.condition_a.test() or self.condition_b.test()