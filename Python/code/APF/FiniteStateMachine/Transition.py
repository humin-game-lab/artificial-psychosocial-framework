from FiniteStateMachine import State
from FiniteStateMachine import Condition
from FiniteStateMachine import Action


class Transition:
    """A class representing a state transition."""

    def __init__(self, target_state: State, condition: Condition, actions: list[Action] = None):
        self.target_state = target_state
        self.condition = condition
        self.actions = actions if actions else []

    def is_triggered(self) -> bool:
        return self.condition.test()

    def get_actions(self) -> list[Action]:
        return self.actions

    def get_target_state(self) -> State:
        return self.target_state
