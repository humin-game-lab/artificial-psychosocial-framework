from abc import ABC, abstractmethod

from FiniteStateMachine import Action

class State(ABC):
    """Abstract base class for states."""

    def __init__(self, name):
        self.name = name

    @abstractmethod
    def get_actions(self) -> list[Action]:
        """Get the actions for this state."""
        pass

    @abstractmethod
    def get_entry_actions(self) -> list[Action]:
        """Get the entry actions for this state."""
        pass

    @abstractmethod
    def get_exit_actions(self) -> list[Action]:
        """Get the exit actions for this state."""
        pass

    @abstractmethod
    def get_transitions(self) -> list['Transition']:
        """Get the transitions for this state."""
        pass


class IdleState(State):
    def get_actions(self) -> list[Action]:
        return [Action.PatrolAction()]

    def get_entry_actions(self) -> list[Action]:
        return []

    def get_exit_actions(self) -> list[Action]:
        return []

    def get_transitions(self) -> list['Transition']:
        return []