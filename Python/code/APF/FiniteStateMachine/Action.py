from abc import ABC, abstractmethod


class Action(ABC):
    """Abstract base class for actions."""

    @abstractmethod
    def execute(self):
        """Execute the action."""
        pass


class PatrolAction(Action):
    def execute(self):
        print("Patrolling...")


class DefendAction(Action):
    def execute(self):
        print("Defending...")


class SleepAction(Action):
    def execute(self):
        print("Sleeping...")
