Author: Lawrence (Jake) Klinkert
Date: 04 May 2020

This is running a simple experiment to demonstrate how performing scenarios with different personalities will yield different emotional histories and social relations.

You can run the application by going to Run/EmotionalModeling_x64.exe

There are drop-down menus to view the histories in the top window. The bottom window holds all the actions that you perform onto the ANPC above.

if you press ~ and type in logData, then the application will log the list of actions performed, the emotional state of the ANPC, the history of the social relation, and the history of certainty for the different relations.

A known bug is that the application will only precess 256 actions. It is best to reset the application once getting close to this value. Otherwise, you will be propped with an exception.

To change the actions, you'll need to open up the project solution. Go to Game.cpp, and in void Game::InitActions() you can put in your action and the effect it has onto the ANPC. 0 is something the ANPC does not want to happen, and 1 is somthing that the ANPC would like to happen to it.

To change the social relations refrence points, you'll need to open up the project solution. Go to Game.cpp, and in InitSocialRelations() you can put in your social relations. 