#pragma once
#include "Engine/Math/Vec2.hpp"


struct Plane2
{
public:
	Plane2();
	~Plane2();
	explicit Plane2(const Vec2& normal_vec, float distance);
	explicit Plane2(const Vec2& start, const Vec2& end, const Vec2& origin);

	Vec2 ClosestPoint(const Vec2& pos);
	
private:
	Vec2	m_normal = Vec2(0.0f, 1.0f);
	float	m_signedDistance = 0.0f; //how far do I push the plane from the origin, in the direction of the normal
};
