#include "Engine/Math/Plane2.hpp"
#include "Engine/Math/MathUtils.hpp"

Plane2::Plane2() = default;
Plane2::~Plane2() = default;

Plane2::Plane2(const Vec2& normal_vec, const float distance):
	m_normal(normal_vec), m_signedDistance(distance) { }


// Plane2::Plane2(const Vec2& start_point, const Vec2& end_point)
// {
// 	Vec2 dir_vec = end_point - start_point;
// 
// 	//rotate -90 degrees
// 	Vec2 norm_s = Vec2(-1.0f * dir_vec.y, dir_vec.x);
// 	Vec2 norm_e = Vec2(dir_vec.y, -1.0f * dir_vec.x);
// 	Vec2 norm_dir = norm_e - norm_s;
// 	m_normal = norm_dir.GetNormalized();
// 
// 	
// }


Vec2 Plane2::ClosestPoint(const Vec2& pos)
{
	const float line_dis = DotProduct(pos, m_normal);
	const float dis_between_lines = m_signedDistance - line_dis;
	const Vec2 dir_to_point_on_line = dis_between_lines * m_normal;
	return pos + dir_to_point_on_line;
}



