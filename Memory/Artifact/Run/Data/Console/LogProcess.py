import sys
import time

Mentals=[
    #Valence
    "POSITIVE",
    "NEGATIVE",
    #Mood
    "PLEASED",
    "DISPLEASED",
    "LIKING",
    "DISLIKING",
    "APPROVING",
    "DISAPPROVING",
    #Emotion
    "HOPE",
    "FEAR",
    "JOY",
    "DISTRESS",
    "LOVE",
    "HATE",
    "INTEREST",
    "DISGUST",
    "PRIDE",
    "SHAME",
    "ADMIRATION",
    "REPROACH",
    #Feelings
    "SATISFACTION",
    "FEARS_CONFIRMED",
    "RELIEF",
    "DISAPPOINTMENT",
    "HAPPY_FOR",
    "RESENTMENT",
    "GLOATING",
    "PITY",
    "GRATIFICATION",
    "REMORSE",
    "GRATITUDE",
    "ANGER"]

interests=["POSITIVE", "NEGATIVE","PLEASED", "DISPLEASED",
"HOPE","FEAR","JOY","DISTRESS","SATISFACTION",
"FEARS_CONFIRMED","RELIEF","DISAPPOINTMENT"]

actions=["Hold a jar of","Touch with foot","Close eyes and stand in a room with",
    "Pick up with a sheet of paper","Pick up with a gloved hand",
    "Hold in bare hand","Allow to crawl on pants leg","Allow to crawl on bare arm",
    "Hold in each hand", "would be hurt by","Rewarded with", "Actually hurt by"]

objects=["Lego","Video","Candy","Cricket","Gift card"]

def GetMentalListFromString(mentalString,mentals):
    mList = mentalString.split(",")
    result=[]
    for m in mentals:
        result.append(mList[Mentals.index(m)])
    return result

def GetPraiseListFromString(praiseString):
    aList = praiseString.split(';')
    result=[0.0 for a in actions]
    for a in aList:
        trunks = a.split(':')
        result[actions.index(trunks[0])] = trunks[1]
    return result

def GetAttitudeListFromString(attitudeString):
    oList = attitudeString.split(';')
    result = ["0,0" for o in objects]
    for o in oList:
        trunks = o.split(':')
        idx = objects.index(trunks[0])
        result[idx] = trunks[1]
    return result

def ProcessLog(logName, mentals):
    session = "Session "
    action="Action "
    sId = -1
    aId = -1
    fOut = open(logName+".csv","w")
    mentalString = ",".join(mentals)
    praiseString = ",".join(actions)
    attitudeSpecific = []
    for o in objects:
        attitudeSpecific.append(o+"_l")
        attitudeSpecific.append(o+"_f")
    attString=",".join(attitudeSpecific)
    fOut.write("Action,SessionIdx,ActionIdx,"+mentalString+","+praiseString+","+attString+"\n")
    with open(logName,"r") as f:
        line = f.readline()
        while line:
            if len(line)==0:
                pass
            elif session in line:
                temp = line[len(session):]
                space=temp.find(' ')
                sId = (int)(temp[:space])
                temp = temp[temp.find(action)+len(action):]
                space = temp.find(' ')
                aId = (int)(temp[:space])
            else:
                actionName=line.rstrip('\n')

                line = f.readline()
                mentalString = line[line.find('{')+1:line.rfind(',')]
                mList = GetMentalListFromString(mentalString,mentals)
                mentalresult = ",".join(mList)

                line = f.readline()
                praiseString = line[line.find('{')+1:line.rfind(';')]
                pList = GetPraiseListFromString(praiseString)
                praiseResult=",".join(pList)

                line = f.readline()
                attitudeString = line[line.find('{')+1:line.rfind(';')]
                aList = GetAttitudeListFromString(attitudeString)
                attitudeResult = ",".join(aList)

                string = (actionName+",{0},{1},"+mentalresult+","+praiseResult+","+attitudeResult+"\n").format(sId,aId)
                fOut.write(string)
            line = f.readline()
    fOut.close()

def ProcessConsoleLog(fileName):
    localtime = time.localtime(time.time())
    writeName = (fileName+"_{0}{1}{2}{3}{4}").format(localtime.tm_mon, localtime.tm_mday, localtime.tm_hour, localtime.tm_min, localtime.tm_sec)
    with open(writeName+".csv", "w") as fOut:
        fOut.write("name,"+",".join(Mentals)+"\n")
        with open(fileName, "r") as f:
            line = f.readline() # bitmap
            line = f.readline()
            while line:
                if len(line)<2:
                    pass
                else:
                    line = line.replace('{','')
                    line = line.replace('}','')
                    parts = line.strip().split(': ')
                    fOut.write(",".join(parts)+"\n")
                line = f.readline()

if __name__=="__main__":
    logname = "log_1110_1654.txt"
    if len(sys.argv)>1:
        logname = sys.argv[1]
        print("parsing "+logname)
    ProcessLog(logname,interests)
    ProcessConsoleLog("log.txt")