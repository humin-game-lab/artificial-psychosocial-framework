EventResults=[
    # hold jar of
'        <Memory>\
            <MentalState valence="pos:.05;neg:.5" emotion="distress:.5;joy:.3;fear:.3" mood="displeased:.5;pleased:.2" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:.4;hurting:.3"/>\
            </GameState>\
        </Memory>',
# touch with foot
'       <Memory>\
            <MentalState valence="pos:.05;neg:.55" emotion="distress:.55;joy:.27;fear:.4" mood="displeased:.54;pleased:.2" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.3;moving:.8;hurting:.4"/>\
            </GameState>\
        </Memory>',
#close eye and stand in a room
'        <Memory>\
            <MentalState valence="pos:.05;neg:.6" emotion="distress:.6;joy:.24;fear:.5" mood="displeased:.57;pleased:.15" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.2;moving:.9;hurting:.5"/>\
            </GameState>\
        </Memory>',
# pick up with paper
'        <Memory>\
            <MentalState valence="pos:.05;neg:.65" emotion="distress:.65;joy:.2;fear:.6" mood="displeased:.6;pleased:.15" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.4;moving:.4;hurting:.6"/>\
            </GameState>\
        </Memory>',
# pick up with glove
'        <Memory>\
            <MentalState valence="pos:.05;neg:.7" emotion="distress:.7;joy:.17;fear:.65" mood="displeased:.64;pleased:.1" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.2;moving:.3;hurting:.65"/>\
            </GameState>\
        </Memory>',
# hold in bare hand
'        <Memory>\
            <MentalState valence="pos:.05;neg:.75" emotion="distress:.75;joy:.14;fear:.7" mood="displeased:.67;pleased:.1" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.1;moving:.6;hurting:.7"/>\
            </GameState>\
        </Memory>',
# crawl on pants
'        <Memory>\
            <MentalState valence="pos:.05;neg:.8" emotion="distress:.8;joy:.1;fear:.75" mood="displeased:.7;pleased:.05" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.1;moving:.8;hurting:.8"/>\
            </GameState>\
        </Memory>',
# crawl on arm
'        <Memory>\
            <MentalState valence="pos:.05;neg:.85" emotion="distress:.85;joy:.07;fear:.8" mood="displeased:.74;pleased:.05" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.1;moving:.9;hurting:.9"/>\
            </GameState>\
        </Memory>',
# hold in both hand
'        <Memory>\
            <MentalState valence="pos:.05;neg:.9" emotion="distress:.9;joy:.04;fear:.85" mood="displeased:.77;pleased:.05" feeling="relief:.05;fears:0.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:.1;moving:.7;hurting:1"/>\
            </GameState>\
        </Memory>'
]

ImagineMemories=[
    # would hurt
'       <Action actor="Bob" action="would be hurt by" patient="Cricket:Object" certainty="1"/>\
        <Memory>\
            <MentalState valence="neg:1" emotion="distress:1;fear:1" mood="displeased:1" feeling="fears:1"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:0;moving:1;hurting:1"/>\
            </GameState>\
        </Memory>'
]

EventMemories=[
# hold jar of
'        <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Hold a jar of" patient="Cricket:Object" certainty="1" />\n',
# touch with foot
'        <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Touch with foot" patient="Cricket:Object" certainty="1" />\n',
#close eye and stand in a room
'        <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Close eyes and stand in a room with" patient="Cricket:Object" certainty="1" />\n',
# pick up with paper
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Pick up with a sheet of paper" patient="Cricket:Object" certainty="1" />\n',
# pick up with glove
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Pick up with a gloved hand" patient="Cricket:Object" certainty="1" />\n',
# hold in bare hand
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Hold in bare hand" patient="Cricket:Object" certainty="1" />\n',
# crawl on pants
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Allow to crawl on pants leg" patient="Cricket:Object" certainty="1" />\n',
# crawl on arm
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Allow to crawl on bare arm" patient="Cricket:Object" certainty="1" />\n',
# hold in both hand
'       <Memory>\
            <MentalState valence="" emotion="" mood="" feeling=""/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>\
        <Action actor="Bob" action="Hold in each hand" patient="Cricket:Object" certainty="1" />\n'
]

ReinforceMemories=[
# candy
'       <Action actor="Bob" action="Rewarded with" patient="Candy:Object" certainty="1"/>\
        <Memory>\
            <MentalState valence="neg:0.3;pos:.3" emotion="distress:.4;joy:.5;fear:.3;relief:.5" mood="displeased:.4;pleased:.5" feeling="relief:.1;fears:.2;satisfaction:.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>',
# gift card
'       <Action actor="Bob" action="Rewarded with" patient="Gift card:Object" certainty="1"/>\
        <Memory>\
            <MentalState valence="neg:0.3;pos:.4" emotion="distress:.3;joy:.6;fear:.3;relief:.5" mood="displeased:.4;pleased:.5" feeling="relief:.1;fears:.2;satisfaction:.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>',
# video
'       <Action actor="Bob" action="Rewarded with" patient="Video:Object" certainty="1"/>\
        <Memory>\
            <MentalState valence="neg:0.3;pos:.5" emotion="distress:.2;joy:.7;fear:.3;relief:.5" mood="displeased:.4;pleased:.5" feeling="relief:.1;fears:.2;satisfaction:.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>',
# lego
'       <Action actor="Bob" action="Rewarded with" patient="Lego:Object" certainty="1"/>\
        <Memory>\
            <MentalState valence="neg:0.3;pos:.6" emotion="distress:.1;joy:.8;fear:.3;relief:.5" mood="displeased:.4;pleased:.5" feeling="relief:.1;fears:.2;satisfaction:.2"/>\
            <GameState>\
                <ObjectState name="Cricket" states="contained:1;moving:0;hurting:0"/>\
            </GameState>\
        </Memory>'
]

def WriteSTMCombinations():
    f = open("ShortTermMemories.xml",'w')
    f.write("<STMs>\n")

    # cricket event
    
    imagine = ImagineMemories[0]
    for j in range(100):
        for i in range(len(EventMemories)):
            f.write("    <STM name=\"Bob\">\n")
            f.write(EventMemories[i])
            f.write(EventResults[i]+"\n")
            f.write(imagine+"\n")
            f.write("    </STM>\n")

    # # reinforcement
    # for i in range(len(EventResults)):        
    #     for reward in ReinforceMemories:
    #         f.write("    <STM name=\"Bob\">\n")
    #         f.write(EventResults[i]+"\n")
    #         f.write(reward+"\n")
    #         f.write("    </STM>\n")

    f.write("</STMs>")
    f.close()

def WritePersonality(idx, f):
    idxStr = "{0:05b}".format(idx)
    string = "  <Personality name=\"\" O=\"{0}\" C=\"{1}\" E=\"{2}\" A=\"{3}\" N=\"{4}\"/>\n".format(*idxStr)
    f.write(string)

def GeneratePersonality():
    f = open("Personalities.xml","w")
    f.write("<Personalities>\n")

    for x in range(32):
        WritePersonality(x, f)

    f.write("</Personalities>")
    f.close()

if __name__=="__main__":
    #GeneratePersonality()
    WriteSTMCombinations()