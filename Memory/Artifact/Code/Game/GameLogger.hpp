#pragma once

#include "Memory/Logger.hpp"

class GameLogger : public APF_Lib::MM::Logger
{
public:
    void LogError(char const* format, ...) override;
    void LogInfo(std::string const& info) override;
};