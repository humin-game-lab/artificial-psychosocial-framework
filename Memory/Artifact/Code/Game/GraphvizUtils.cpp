#include "Game/GraphvizUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/FileUtils.hpp"
#include "Engine/Core/StringUtils.hpp"

#include <windows.h>

//////////////////////////////////////////////////////////////////////////
void RenderGraphvizImage(std::string const& dotLanguage, std::string const& outputImg)
{
    STARTUPINFOA si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    std::string cmdLine = Stringf("./dot.exe -Tpng %s -o %s", dotLanguage.c_str(), outputImg.c_str());
    LPSTR cmd = const_cast<char*>(cmdLine.c_str());
    //using cmd version
    if (!CreateProcessA(NULL,
        cmd,
        NULL,
        NULL,
        FALSE,
        0,
        NULL,
        NULL,
        &si,
        &pi)) {
        g_theConsole->PrintError("Fail to execute dot.exe command");
        return;
    }

    WaitForSingleObject(pi.hProcess, INFINITE);

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
}
