#include "Game/MemoryUtils.hpp"
#include "Game/APFUtils.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Memory/Memory.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/FileUtils.hpp"

//////////////////////////////////////////////////////////////////////////
static APF_Lib::MentalFeature GetMentalFeatureFromString(std::string const& rawStr)
{
    std::string str = GetLowerCases(Trim(rawStr));
    //valence
    if (str == "pos" || str == "positive") {
        return APF_Lib::MENTAL_FEATURE_POSITIVE;
    }
    else if (str == "neg" || str == "negative") {
        return APF_Lib::MENTAL_FEATURE_NEGATIVE;
    }
    //mood
    else if (str == "pleased") {
        return APF_Lib::MENTAL_FEATURE_PLEASED;
    }
    else if (str == "displeased") {
        return APF_Lib::MENTAL_FEATURE_DISPLEASED;
    }
    else if (str == "liking") {
        return APF_Lib::MENTAL_FEATURE_LIKING;
    }
    else if (str == "disliking") {
        return APF_Lib::MENTAL_FEATURE_DISLIKING;
    }
    else if (str == "approving") {
        return APF_Lib::MENTAL_FEATURE_APPROVING;
    }
    else if (str == "disapproving") {
        return APF_Lib::MENTAL_FEATURE_DISAPPROVING;
    }
    //emotion
    else if (str == "hope") {
        return APF_Lib::MENTAL_FEATURE_HOPE;
    }
    else if (str == "fear") {
        return APF_Lib::MENTAL_FEATURE_FEAR;
    }
    else if (str == "joy") {
        return APF_Lib::MENTAL_FEATURE_JOY;
    }
    else if (str == "distress") {
        return APF_Lib::MENTAL_FEATURE_DISTRESS;
    }
    else if (str == "love") {
        return APF_Lib::MENTAL_FEATURE_LOVE;
    }
    else if (str == "hate") {
        return APF_Lib::MENTAL_FEATURE_HATE;
    }
    else if (str == "interest") {
        return APF_Lib::MENTAL_FEATURE_INTEREST;
    }
    else if (str == "disgust") {
        return APF_Lib::MENTAL_FEATURE_DISGUST;
    }
    else if (str == "pride") {
        return APF_Lib::MENTAL_FEATURE_PRIDE;
    }
    else if (str == "shame") {
        return APF_Lib::MENTAL_FEATURE_SHAME;
    }
    else if (str == "admiration") {
        return APF_Lib::MENTAL_FEATURE_ADMIRATION;
    }
    else if (str == "reproach") {
        return APF_Lib::MENTAL_FEATURE_REPROACH;
    }
    //feelings
    else if (str == "satisfaction") {
        return APF_Lib::MENTAL_FEATURE_SATISFACTION;
    }
    else if (str == "fears") {
        return APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED;
    }
    else if (str == "relief") {
        return APF_Lib::MENTAL_FEATURE_RELIEF;
    }
    else if (str == "disappointment") {
        return APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT;
    }
    else if (str == "happyfor") {
        return APF_Lib::MENTAL_FEATURE_HAPPY_FOR;
    }
    else if (str == "resentment") {
        return APF_Lib::MENTAL_FEATURE_RESENTMENT;
    }
    else if (str == "gloating") {
        return APF_Lib::MENTAL_FEATURE_GLOATING;
    }
    else if (str == "pity") {
        return APF_Lib::MENTAL_FEATURE_PITY;
    }
    else if (str == "gratification") {
        return APF_Lib::MENTAL_FEATURE_GRATIFICATION;
    }
    else if (str == "remorse") {
        return APF_Lib::MENTAL_FEATURE_REMORSE;
    }
    else if (str == "gratitude") {
        return APF_Lib::MENTAL_FEATURE_GRATITUDE;
    }
    else if (str == "anger") {
        return APF_Lib::MENTAL_FEATURE_ANGER;
    }
    else return APF_Lib::MENTAL_FEATURE_UNKNOWN;
}

//////////////////////////////////////////////////////////////////////////
static APF_Lib::PatientType GetPatientTypeFromString(std::string const& rawStr)
{
    std::string str = GetLowerCases(Trim(rawStr));
    if (str == "object") {
        return APF_Lib::PatientType::PATIENT_OBJECT;
    }
    else if (str == "anpc") {
        return APF_Lib::PatientType::PATIENT_ANPC;
    }
    else {
        return APF_Lib::PatientType::PATIENT_UNKNOWN;
    }
}

//////////////////////////////////////////////////////////////////////////
static void SetMentalStateFromString(APF_Lib::MentalState& state, std::string const& str)
{
    if (str.empty()) {
        return;
    }

    Strings chunks = SplitStringOnDelimiter(str, ';');
    for (std::string const& chunk : chunks) {
        Strings cells = SplitStringOnDelimiter(chunk, ':');
        if (cells.size() != 2) {
            g_theConsole->PrintError("Fail to parse mentalState chunk");
            continue;
        }

        APF_Lib::MentalFeature feature = GetMentalFeatureFromString(cells[0]);
        if (feature == APF_Lib::MENTAL_FEATURE_UNKNOWN) {
            g_theConsole->PrintError(Stringf("Fail to translate %s to mental feature", cells[0].c_str()));
            continue;
        }
        state[feature] = StringConvert(cells[1].c_str(), .0f);
    }
}

//////////////////////////////////////////////////////////////////////////
static std::string GetGraphvizSimpleTextForMentalState(APF_Lib::MentalState const& state)
{
    std::string result = "Mood: {";
    float moods[APF_Lib::NUM_MOOD_FEATURES];
    state.GetMood(moods);
    for (int i = 0; i < (int)APF_Lib::NUM_MOOD_FEATURES; i++) {
        result += Stringf("%.1f,", moods[i]);
    }
    result += "}\\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
static void PopulateGameStateFromXML(APF_Lib::MM::GameState& gameState, XmlElement const* element)
{
    //randomize start time between sequence and sequence+1 day 
    XmlElement const* objState = element->FirstChildElement("ObjectState");
    while (objState != nullptr) {
        std::string objName = ParseXmlAttribute(*objState, "name", "");
        APF_Lib::MM::GameObject* obj = g_theGame->GetObjectFromName(objName);
        if (obj != nullptr) {
            APF_Lib::MM::ObjectState newState;
            MemoryUtils::PopulateObjectStateFromXML(newState, objState);
            gameState.objectPerceivedStates[obj] = newState;
        }

        objState = objState->NextSiblingElement("ObjectState");
    }

    //TODO actor state not parsing now
}

//////////////////////////////////////////////////////////////////////////
static void PopulateMemoryFromXML(APF_Lib::MM::Memory& mem, XmlElement const* memElement)
{
    XmlElement const* mental = memElement->FirstChildElement("MentalState");
    SetMentalStateFromString(mem.m_mentalState, ParseXmlAttribute(*mental, "valence", ""));
    SetMentalStateFromString(mem.m_mentalState, ParseXmlAttribute(*mental, "emotion", ""));
    SetMentalStateFromString(mem.m_mentalState, ParseXmlAttribute(*mental, "mood", ""));
    SetMentalStateFromString(mem.m_mentalState, ParseXmlAttribute(*mental, "feeling", ""));

    XmlElement const* gameState = memElement->FirstChildElement("GameState");
    PopulateGameStateFromXML(mem.m_gameState, gameState);

    XmlElement const* selfState = memElement->FirstChildElement("SelfState");
    if (selfState) {
        mem.m_selfState = new APF_Lib::MM::ObjectState();
        MemoryUtils::PopulateObjectStateFromXML(*mem.m_selfState, selfState);
    }

    mem.m_isGarbage = false;
}

//////////////////////////////////////////////////////////////////////////
static void PopulateGameEventFromXML(APF_Lib::MM::GameEvent& event, XmlElement const* element, int ownerIdx)
{
    std::string actorName = ParseXmlAttribute(*element, "actor", "");
    APF_Lib::MM::GameActor* actor = g_theGame->GetActorFromName(actorName);
    if (actor == nullptr) {
        g_theConsole->PrintError(Stringf("Fail to load actor of name %s", actorName.c_str()));
        return;
    }

    std::string actionName = ParseXmlAttribute(*element, "action", "");
    event.m_actionInfo = APF_Lib::MM::ActionInfo::GetActionOfName(actionName);
    if (event.m_actionInfo == nullptr) {
        g_theConsole->PrintError(Stringf("Fail to load action of name %s", actionName.c_str()));
        return;
    }

    std::string patientRaw = ParseXmlAttribute(*element, "patient", "");
    Strings patients = SplitStringOnDelimiter(patientRaw, ':');
    if (patients.size() != 2) {
        g_theConsole->PrintError("Fail to parse patient, wanted format: 'Apple:Object'");
        return;
    }
    APF_Lib::PatientType patientType = GetPatientTypeFromString(patients[1]);
    if (patientType == APF_Lib::PatientType::PATIENT_UNKNOWN) {
        g_theConsole->PrintError(Stringf("Fail to recognize patient type %s", patients[1].c_str()));
        return;
    }
    else if (patientType == APF_Lib::PatientType::PATIENT_OBJECT) {
        APF_Lib::MM::GameObject* o = g_theGame->GetObjectFromName(patients[0]);
        event.m_patient = o->GetIndex();
        event.m_target = g_theGame->GetObjectFromAPFIdx(event.m_patient);
    }
    else if (patientType == APF_Lib::PatientType::PATIENT_ANPC) {
        APF_Lib::MM::GameActor* patient = g_theGame->GetActorFromName(patients[0]);
        event.m_patient = patient->GetIndex();
    }

    event.m_owner = ownerIdx;
    event.m_actor = actor->GetIndex();
    event.m_action = event.m_actionInfo->apfIdx;
    event.m_patientType = patientType;
    event.m_certainty = ParseXmlAttribute(*element, "certainty", 1.f);
    event.m_isGarbage = false;
}

//////////////////////////////////////////////////////////////////////////
static void PopulateSTMFromXML(APF_Lib::MM::ShortTermMemory& stm, XmlElement const* stmElement)
{
    XmlElement const* mem = stmElement->FirstChildElement("Memory");
    XmlElement const* act = stmElement->FirstChildElement("Action");
    stm.InvalidateAll();
    unsigned int number=0;
    while (mem != nullptr) {
        APF_Lib::MM::Memory memory;
        PopulateMemoryFromXML(memory, mem);
        stm.AddNewMemory(memory);
        number++;

        if (act != nullptr) {
            APF_Lib::MM::GameEvent action;
            PopulateGameEventFromXML(action, act, stm.GetOwnerIndex());
            stm.AddNewEvent(action);

            act = act->NextSiblingElement("Action");
        }

        mem = mem->NextSiblingElement("Memory");
    }

    APF_Lib::MM::MemoryTrace trace;
    stm.GetRecentMemories(number, trace);
    stm.GetMemorySystem()->GetLTM()->AddNewMemoryTrace(trace);
}

//////////////////////////////////////////////////////////////////////////
static void ParseRawADJString(std::map<std::string, float>& adjectives, std::string const& rawADJ)
{
    Strings trunks = SplitStringOnDelimiter(rawADJ, ';');
    for (std::string const& trunk : trunks) {
        Strings cells = SplitStringOnDelimiter(Trim(trunk), ':');
        if (cells.size() != 2 || cells[0].empty()) {
            g_theConsole->PrintError(Stringf("action ADJ string %s format wrong", trunk.c_str()));
        }
        else {
            float delta = StringConvert(cells[1].c_str(), 0.f);
            adjectives[cells[0]] = delta;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateAttitudesFromFile(std::string const& filePath)
{
    std::vector<std::string> lines = FileReadLines(filePath);
    if (lines.size() < 2) {
        g_theConsole->PrintError("fail to init from Attitudes file");
        return;
    }

    Strings obNames = SplitStringOnDelimiter(lines[0], '\t');
    std::vector<APF_Lib::MM::GameObject*> objects;
    objects.push_back(nullptr);
    for (size_t i = 1; i < obNames.size(); i++) {
        objects.push_back(g_theGame->GetObjectFromName(obNames[i]));
    }

    for (size_t i = 1; i < lines.size(); i++) {	//ignore first line
        Strings trunks = SplitStringOnDelimiter(lines[i], '\t');
        if (trunks.size() != obNames.size()) {
            g_theConsole->PrintError(Stringf("Line %s in Attitudes format wrong", lines[i].c_str()));
            continue;
        }

        APF_Lib::MM::GameActor* actor = g_theGame->GetActorFromName(trunks[0]);
        for (size_t j = 1; j < trunks.size(); j++) {
            Strings vals = SplitStringOnDelimiter(trunks[j], ',');
            if (vals.size() != 2) {
                g_theConsole->PrintError("Failed to parse attitudes file values");
                continue;
            }

            float liking = StringConvert(vals[0].c_str(), .5f);
            float familarity = StringConvert(vals[1].c_str(), .5f);
            g_APF->AddAttitude(actor->GetIndex(), objects[j]->GetIndex(), liking, familarity);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateActionsFromXML(std::string const& filePath)
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile(filePath.c_str());
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError(Stringf("Failed to load xml %s", filePath.c_str()));
        return;
    }

    XmlElement const* root = xmlDoc.RootElement();
    XmlElement const* action = root->FirstChildElement("Action");
    APF_Lib::MM::ActionInfo::sPossibleActions.clear();
    while (action != nullptr) {
        std::string name = ParseXmlAttribute(*action, "name", "");
        if (!name.empty()) {
            float effect = ParseXmlAttribute(*action, "effect", .5f);

            APF_Lib::MM::ActionInfo& newInfo = APF_Lib::MM::ActionInfo::sPossibleActions.emplace_back(name, effect);
            g_APF->AddAction(name.c_str(), effect);

            std::string tagRaw = ParseXmlAttribute(*action, "tags", "");
            newInfo.tags.SetTags(tagRaw);

            std::string ADJ = ParseXmlAttribute(*action, "ADJ", "");
            if (!ADJ.empty()) {
                ParseRawADJString(newInfo.adjectives, ADJ);
            }
        }

        action = action->NextSiblingElement("Action");
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateSTMsFromXML(std::string const& filePath)
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile(filePath.c_str());
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError(Stringf("Failed to load xml %s", filePath.c_str()));
        return;
    }

    XmlElement const* stm = xmlDoc.RootElement()->FirstChildElement("STM");
    while (stm != nullptr) {
        std::string name = ParseXmlAttribute(*stm, "name", "");
        APF_Lib::MM::GameActor* a = g_theGame->GetActorFromName(name);
        if (a != nullptr) {
            PopulateSTMFromXML(*a->GetMemorySystem()->GetSTM(), stm);
            a->GetMemorySystem()->UpdateForHourIncrease(APF_Lib::MM::GameTime());
        }

        stm = stm->NextSiblingElement("STM");
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateStatesFromXML(std::string const& filePath)
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile(filePath.c_str());
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError(Stringf("Failed to load xml %s", filePath.c_str()));
        return;
    }

    XmlElement const* s = xmlDoc.RootElement()->FirstChildElement("State");
    APF_Lib::MM::StateInfo::sPossibleStates.clear();
    while (s != nullptr) {
        std::string name = ParseXmlAttribute(*s, "name", "");
        if (!name.empty()) {
            float delta = ParseXmlAttribute(*s, "delta", .0f);
            APF_Lib::MM::StateInfo::sPossibleStates.emplace_back(name, delta);
        }
        s = s->NextSiblingElement("State");
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::UpdateActionIndexes()
{
    for (APF_Lib::MM::ActionInfo& aInfo : APF_Lib::MM::ActionInfo::sPossibleActions) {
        aInfo.apfIdx = g_APF->GetActionIndex(aInfo.actionName.c_str());
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::UpdateANPCIndexes(std::vector<APF_Lib::MM::GameActor*>& actors)
{
    for (APF_Lib::MM::GameActor* a : actors) {
        a->SetAPFIndex(g_APF->GetANPCIndex(a->GetName().c_str()));
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::UpdateObjectIndexes(std::vector<APF_Lib::MM::GameObject*>& objects)
{
    for (APF_Lib::MM::GameObject* o : objects) {
        o->SetAPFIndex(g_APF->GetObjectIndex(o->GetName().c_str()));
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateGameObjectFromXML(APF_Lib::MM::GameObject& gameObject, XmlElement const* element)
{
    gameObject.SetName(ParseXmlAttribute(*element, "name", ""));
    if (gameObject.GetName().empty()) {
        g_theConsole->PrintError("Parsing object of name null");
        return;
    }

    g_APF->AddObject(gameObject.GetName().c_str());

    std::string rawTags = ParseXmlAttribute(*element, "tags", "");
    gameObject.GetTagsToChange().SetTags(rawTags);
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateObjectStateFromXML(APF_Lib::MM::ObjectState& objState, XmlElement const* element)
{
    if (element == nullptr) {
        return;
    }

    std::string statesStr = ParseXmlAttribute(*element, "states", "");
    if (statesStr.empty()) {
        return;
    }

    Strings trunks = SplitStringOnDelimiter(statesStr, ';');
    for (std::string const& trunk : trunks) {
        Strings cells = SplitStringOnDelimiter(trunk, ':');
        if (cells.size() != 2) {
            g_theConsole->PrintError(Stringf("Failed to parse object states %s", trunk.c_str()));
            continue;
        }

        APF_Lib::MM::StateInfo const* info = APF_Lib::MM::StateInfo::GetStateFromName(cells[0]);
        if (info == nullptr) {
            g_theConsole->PrintError(Stringf("Fail to find stateInfo of %s", cells[0].c_str()));
            continue;
        }
        objState.states.emplace_back(&info->state, StringConvert(cells[1].c_str(), 0.f));
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PopulateActorFromXML(APF_Lib::MM::GameActor& actor, XmlElement const* element)
{
    actor.SetName(ParseXmlAttribute(*element, "name", ""));
    if (actor.GetName().empty()) {
        g_theConsole->PrintError("Parsing actor of name null");
        return;
    }

    std::string personalityName = ParseXmlAttribute(*element, "personality", "");
    APF_Lib::TraitPersonality p;
    if (!GetPersonalityOfName(personalityName, p)) {
        g_theConsole->PrintString(Rgba8::YELLOW, Stringf("personality %s of %s not exist", personalityName.c_str(), actor.GetName().c_str()));
    }

    g_APF->AddANPC(actor.GetName().c_str(), p[0], p[1], p[2], p[3], p[4]);

    std::string rawTags = ParseXmlAttribute(*element, "tags", "");
    actor.GetTagsToChange().SetTags(rawTags);
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::ObjectState const& objState)
{
    std::string result;
    if (objState.states.empty()) {
        return result;
    }

    result += "States: { ";
    for (APF_Lib::MM::ObjectStateAspect const& selfState : objState.states) {
        APF_Lib::MM::State const* state = selfState.state;
        result += state->name;
        if (g_debugRenderMode == eDebugMode::DEBUG_VERBOSE) {
            result += Stringf("(%.2f)", state->valenceDelta);
        }
        result += Stringf(": %.2f, ", selfState.confidence);
    }
    result += " }\\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::GameEvent const& event)
{
    std::string result;
    APF_Lib::Action const& action = g_APF->GetAction(event.m_action);
    result = action.m_name;
    if (g_debugRenderMode == DEBUG_VERBOSE) {
        result += "(" + Stringf("%.2f", action.m_effect) + ")";
    }
    result += " ";

    if (event.m_patientType == APF_Lib::PatientType::PATIENT_ANPC) {
        APF_Lib::ANPC const& patient = g_APF->GetANPC(event.m_patient);
        result += patient.m_name;
    }
    else if (event.m_patientType == APF_Lib::PATIENT_OBJECT) {
        APF_Lib::Object const& patient = g_APF->GetObject(event.m_patient);
        result += patient.m_name;
    }
    else {
        result += "-";
    }
    result += "\\n";

    if (g_debugRenderMode == eDebugMode::DEBUG_VERBOSE) {
        result += Stringf("Certain: %.2f\\n", event.m_certainty);
        //result += m_tags.GetGraphvizDebugText();
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::GameState const& gameState)
{
    std::string result;

    if (!gameState.objectPerceivedStates.empty()) {
        for (std::map<APF_Lib::MM::GameObject*, APF_Lib::MM::ObjectState>::const_iterator it = gameState.objectPerceivedStates.cbegin();
            it != gameState.objectPerceivedStates.cend(); it++) {
            if (!it->second.states.empty()) {
                result += it->first->GetName() + ":\\n";
                result += GetGraphvizDebugText(it->second);
            }
        }
    }

    if (!gameState.actorPerceivedStates.empty()) {
        for (std::map<APF_Lib::MM::GameActor*, APF_Lib::MM::ActorState>::const_iterator it = gameState.actorPerceivedStates.cbegin();
            it != gameState.actorPerceivedStates.cend(); it++) {
            result += it->first->GetName() + ":\\n";
            result += GetGraphvizDebugText(it->second);
        }
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::ActorState const& actorState)
{
    std::string result;
    if (g_debugRenderMode == DEBUG_VERBOSE) {
        result = GetGraphvizVerboseTextForMentalState(actorState.mentalState);
    }
    else {
        result = GetGraphvizSimpleTextForMentalState(actorState.mentalState);
    }
    result += "Current: " + GetGraphvizDebugText(actorState.currentEvent);
    result += GetGraphvizDebugText(actorState.basicInfo);
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::Memory const& memory)
{
    std::string result;
    if (g_debugRenderMode == DEBUG_VERBOSE) {
        result = GetGraphvizVerboseTextForMentalState(memory.m_mentalState);
    }
    else {
        result = GetGraphvizSimpleTextForMentalState(memory.m_mentalState);
    }
    if (memory.m_selfState) {
        result += GetGraphvizDebugText(*memory.m_selfState);
    }
    result += GetGraphvizDebugText(memory.m_gameState);
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::MemoryTrace const& trace)
{
    std::string result;
    for (APF_Lib::MM::MemoryAndEvent const& memEvent : trace) {
        result += GetGraphvizDebugText(memEvent.memory);
        result += "\n";
        if (!memEvent.selfEvent.m_isGarbage) {
            result += GetGraphvizDebugText(memEvent.selfEvent);
            result += "\n";
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::LTMEdge const& edge)
{
    std::string result = edge.GetIDText();
    result += "[shape=box label=\"";
    APF_Lib::MM::GameEvent event;
    edge.GetEvent(event);
    result += GetGraphvizDebugText(event);
    result += Stringf("Probability: %.2f", edge.GetProbability()) + "\\n";

    if (g_debugRenderMode == eDebugMode::DEBUG_VERBOSE) {
        result += Stringf("Times: %u", edge.GetHappenTimes());
    }
    result += "\"];\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetGraphvizDebugText(APF_Lib::MM::LTMNode const& node)
{
    std::string result = node.GetIDText();
    result += "[shape=ellipse label=\"";
    result += GetGraphvizDebugText(node.GetMemory());
    result += "\"];\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetDebugText(APF_Lib::MM::GameObject const& object)
{
    std::string name = object.GetName();
    APF_Lib::MM::ObjectState const* state = object.GetSelfState();
    if (state) {
        name += " ";
        name += GetGraphvizDebugText(*state);
    }
    return name;
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryUtils::GetDebugText(APF_Lib::MentalState const& mentalstate)
{
    std::string result;
    result += Stringf("Valence:\n  pos %.2f\n  neg %.2f\n", mentalstate[APF_Lib::MENTAL_FEATURE_POSITIVE], mentalstate[APF_Lib::MENTAL_FEATURE_NEGATIVE]);
    result += Stringf("Mood:\n  please %.2f\n  displease %.2f\n", mentalstate[APF_Lib::MENTAL_FEATURE_PLEASED], mentalstate[APF_Lib::MENTAL_FEATURE_DISPLEASED]);
    result += Stringf("Emotion:\n  hope %.2f\n  fear %.2f\n\
  joy %.2f\n  distress %.2f\n", mentalstate[APF_Lib::MENTAL_FEATURE_HOPE], mentalstate[APF_Lib::MENTAL_FEATURE_FEAR],
        mentalstate[APF_Lib::MENTAL_FEATURE_JOY], mentalstate[APF_Lib::MENTAL_FEATURE_DISTRESS]);
    result += Stringf("Feeling:\n  satisfy %.2f\n  fear-confirm %.2f\n\
  relief %.2f\n  disappoint %.2f", mentalstate[APF_Lib::MENTAL_FEATURE_SATISFACTION], mentalstate[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED],
        mentalstate[APF_Lib::MENTAL_FEATURE_RELIEF], mentalstate[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT]);
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::vector<ActionAccessInfo> MemoryUtils::GetAllActionsOfTag(std::string const& tag)
{
    std::vector<ActionAccessInfo> result;
    for (size_t i = 0; i < APF_Lib::MM::ActionInfo::sPossibleActions.size(); i++) {
        APF_Lib::MM::ActionInfo& info = APF_Lib::MM::ActionInfo::sPossibleActions[i];
        if (info.tags.HasTag(tag)) {
            result.emplace_back(info.actionName, (int)i);
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
std::vector<ActionAccessInfo> MemoryUtils::GetAllActionsWithoutTags(std::vector<std::string> const& tags)
{
    std::vector<ActionAccessInfo> result;
    for (size_t i = 0; i < APF_Lib::MM::ActionInfo::sPossibleActions.size(); i++) {
        APF_Lib::MM::ActionInfo& info = APF_Lib::MM::ActionInfo::sPossibleActions[i];
        if (info.tags.HasSameTags(tags) == 0) {
            result.emplace_back(info.actionName, (int)i);
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::PolarCombineEmotionDistantNodes(APF_Lib::MM::LTMNode* nodeA, APF_Lib::MM::LTMNode* nodeB)
{
    APF_Lib::MentalState newMental;
    APF_Lib::MentalState& mentalA = nodeA->GetMentalState();
    APF_Lib::MentalState oldMentalA = mentalA;
    APF_Lib::MentalState& mentalB = nodeB->GetMentalState();
    APF_Lib::MentalState oldMentalB = mentalB;
    APF_Lib::MentalState::CombineMentalStates(mentalA, mentalB, .7f, newMental);
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalA, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalA);
    nodeA->AddMentalHistory(mentalA);
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalB, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalB);
    nodeB->AddMentalHistory(mentalB);
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::NeutralCombineEmotionDistantNodes(APF_Lib::MM::LTMNode* nodeA, APF_Lib::MM::LTMNode* nodeB)
{
    //neutralize
    APF_Lib::MentalState newMental;
    APF_Lib::MentalState& mentalA = nodeA->GetMentalState();
    APF_Lib::MentalState oldMentalA = mentalA;
    APF_Lib::MentalState& mentalB = nodeB->GetMentalState();
    APF_Lib::MentalState oldMentalB = mentalB;
    APF_Lib::MentalState::CombineMentalStates(mentalA, mentalB, .5f, newMental);
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalA, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalA);
    nodeA->AddMentalHistory(mentalA);
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalB, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalB);
    nodeB->AddMentalHistory(mentalB);
}

//////////////////////////////////////////////////////////////////////////
float MemoryUtils::GetShortSightMulplierForProspectGeneration(size_t idx, size_t totalNumber)
{
    if (idx >= APF_Lib::MM::LTM_PROSPECT_MAX_RANGE || totalNumber == 0) {
        return 0.f;
    }

    constexpr float LTM_PROSPECT_MULTIPLIERS[APF_Lib::MM::LTM_PROSPECT_MAX_RANGE] = { .2f, .6f, .15f, .03f, .02f };
    float indexTotal = LTM_PROSPECT_MULTIPLIERS[idx];
    float total = 0.f;
    for (size_t i = 0; i < totalNumber; i++) {
        total += LTM_PROSPECT_MULTIPLIERS[i];
    }
    return indexTotal / total;
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::GetTraceMentalValence(APF_Lib::MM::LTMConstNodeList const& trace, APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float(&valences)[APF_Lib::NUM_VALENCE_FEATURES])
{
    if (trace.empty()) {
        valences[0] = valences[1] = 0.f;
        return;
    }
    else if (trace.size() == 1) {
        APF_Lib::MentalState const& ms = trace[0]->GetAverageMentalState();
        ms.GetValence(valences);
        return;
    }

    size_t totalNum = trace.size();
    float* multipliers = new float[(int)totalNum];
    for (size_t i = 0; i < totalNum; i++) {
        multipliers[i] = funcPtr(i, totalNum);
    }

    float accumulateMultiplier = multipliers[0] + multipliers[1];
    APF_Lib::MentalState::CombineMentalValences(trace[0]->GetAverageMentalState(), trace[1]->GetAverageMentalState(), multipliers[0] / accumulateMultiplier, valences);
    for (size_t i = 2; i < totalNum; i++) {
        float newMultiplier = multipliers[i];
        APF_Lib::MentalState tempValence;
        tempValence.SetValence(valences);
        APF_Lib::MentalState::CombineMentalValences(tempValence, trace[i]->GetAverageMentalState(), accumulateMultiplier / (accumulateMultiplier + newMultiplier), valences);
        accumulateMultiplier += newMultiplier;
    }

    delete[] multipliers;
}

//////////////////////////////////////////////////////////////////////////
std::vector<APF_Lib::MM::LTMConstNodeList> MemoryUtils::RankTracesByMentalValance(std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float const (&weights)[2])
{
    std::vector<APF_Lib::MM::LTMConstNodeList> result;
    std::vector<float> valences;

    for (APF_Lib::MM::LTMConstNodeList const& trace : traces) {
        float vals[APF_Lib::NUM_VALENCE_FEATURES];
        GetTraceMentalValence(trace, funcPtr, vals);

        float val = vals[0] * weights[0] + vals[1] * weights[1];

        bool inserted = false;
        for (size_t i = 0; i < valences.size(); i++) {
            if (val > valences[i]) {
                valences.insert(valences.begin() + i, val);
                result.insert(result.begin() + i, trace);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            valences.push_back(val);
            result.push_back(trace);
        }
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::GenerateMentalSalientProspectMemoryFromTraces(APF_Lib::MM::Memory& result, std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, APF_Lib::MentalState const& mental, APF_Lib::MM::GetProspectMultiplierFunction funcPtr)
{
    UNUSED(mental);
    float const (&weights)[2] = POSITIVE_WEIGHTS;
    std::vector<APF_Lib::MM::LTMConstNodeList> ranked = RankTracesByMentalValance(traces, funcPtr, weights);
    constexpr float rankMultiplier[3] = { .5f,.3f,.2f };
    if (traces.empty()) {
        return;
    }

    APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(result, ranked[0], funcPtr);
    if (traces.size() == 1) {
        return;
    }

    APF_Lib::MM::Memory rank1;
    APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(rank1, ranked[1], funcPtr);
    APF_Lib::MM::Memory::GetCombinedMemory(result, result, rank1, rankMultiplier[0] / (rankMultiplier[1] + rankMultiplier[0]));
    if (traces.size() == 2) {
        return;
    }

    APF_Lib::MM::Memory rank2;
    APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(rank2, ranked[2], funcPtr);
    APF_Lib::MM::Memory::GetCombinedMemory(result, rank2, result, rankMultiplier[2]);
}

//////////////////////////////////////////////////////////////////////////
void MemoryUtils::InitializeActor(APF_Lib::MM::GameActor& actor)
{
    APF_Lib::MentalState random = APF_Lib::MentalState::GenerateRandomMentalState();
    g_APF->SetANPCsMentalState(actor.GetIndex(), random);
}
