#include "Game/Game.hpp"
#include "Game/App.hpp"
#include "Game/GameCommon.hpp"
#include "Memory/MemoryClock.hpp"
#include "Game/GraphvizUtils.hpp"
#include "Game/APFUtils.hpp"
#include "Game/GameLogger.hpp"
#include "Game/MemoryUtils.hpp"
#include "Engine/Math/RandomNumberGenerator.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Renderer/RenderContext.hpp"
#include "Engine/Renderer/Camera.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Renderer/TextureView.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Input/InputSystem.hpp"
#include "Engine/Core/NamedStrings.hpp"
#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/Clock.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/EventSystem.hpp"
#include "Engine/Core/FileUtils.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "APF/APF_Lib.hpp"

#include "ThirdParty/imgui/imgui.h"
#include "ThirdParty/imgui/imgui_plot.h"

static std::string sEndingText;

static std::string sLTMImgPath="Temp";
static int sLTMImgPathSuffix=0;

static Texture* sLTMImgTex = nullptr;
static float sWorldHeight = 1080.f;
static float sLastMouseScrollValue = 0.f;
static float sWorldAspect = 1.7f;

enum eLoggedMentals
{
    VALENCE_POS = 0,
    VALENCE_NEG,

    MOOD_PLEASED,
    MOOD_DISAPLEASED,

    EMOTION_HOPE,
    EMOTION_FEAR,
    EMOTION_JOY,
    EMOTION_DISRESS,

    FEELING_SATISFY,
    FEELING_FEARS,
    FEELING_RELIEF,
    FEELING_DISPPOINT,

    NUM_LOG_MENTALS
};

static int sMentalLogStartIdx = 0;
static int sMentalLogEndIdx = NUM_LOG_MENTALS;

struct GameEventInfo
{
	ActionAccessInfo action;
	APF_Lib::MM::GameObject const* object;

	//TODO may expand to GameState
	std::map<APF_Lib::MM::GameObject const*, APF_Lib::MM::ObjectState> newStates;

	GameEventInfo(ActionAccessInfo const& info, APF_Lib::MM::GameObject const* ob)
		: action(info), object(ob)	{}

	std::string ToString() const 
	{
        return action.name + " " + object->GetName();
	}
};

static std::vector<GameEventInfo> sPossibleEvents;
static std::vector<GameEventInfo> sPossibleRewards;

struct AttitudePraise
{
	std::map<APF_Lib::MM::ActionInfo const*, float> praises;
	std::map<APF_Lib::MM::GameObject const*, APF_Lib::TraitAttitude> attitudes;

	std::string GetLogText() const
	{
		std::string result = "Praises:{";
		for (auto it = praises.begin(); it != praises.end(); it++) {
			result += it->first->actionName + ":";
			result += Stringf("%.5f;",it->second);
		}
		result += "}\n";

		result+="Attitudes:{";
		for (auto it = attitudes.begin(); it != attitudes.end(); it++) {
			result+=it->first->GetName()+":";
			APF_Lib::TraitAttitude const& att = it->second;
			result+=Stringf("%.5f,%.5f;",att.m_traits[0], att.m_traits[1]);
		}
		result+="}\n";
		return result;
	}
};

struct GameHistory
{
public:
    int eventIdx;
	int rewardIdx;
    APF_Lib::MentalState mentalAfter;
	APF_Lib::MentalState mentalBefore;
	AttitudePraise attitudePraiseBefore;
	AttitudePraise attitudePraiseAfter;
    APF_Lib::MM::GameTime time;
	int sessionIdx=-1;
	int actionIdx=-1;

    static char const* sLoggedMentalNames[NUM_LOG_MENTALS]; 
	static ImColor sMentalColors[NUM_LOG_MENTALS];
};

char const* GameHistory::sLoggedMentalNames[NUM_LOG_MENTALS] = { "Positive",
        "Negative", "Pleased", "Displeased", "Hope", "Fear", "Joy",
        "Distress", "Satisfaction", "Fear_confirmed", "Relief", "Disappointment" };

ImColor GameHistory::sMentalColors[NUM_LOG_MENTALS] = { 
    ImColor(128, 0, 0), ImColor(200,50,0),  ImColor(255,100,0),
    ImColor(200,150,0),	ImColor(255,0,100), ImColor(200,0,150),
    ImColor(220,0,180), ImColor(255,0,180), ImColor(0,255,100),
	ImColor(0,200,150), ImColor(0,220,180), ImColor(0,255,180)};

static int sSessionBeginIdx=0;
static APF_Lib::MentalState sStartMental;
static std::vector<APF_Lib::MentalState> sStartMentalHistories;
static std::vector<AttitudePraise> sStartAttitudePraises;
static std::vector<GameHistory> sGameHistories;

#include <queue>

static std::queue<APF_Lib::MentalState> sEmojiStack;
static APF_Lib::MentalState sCurrentEmotionForEmoji;
static std::queue<APF_Lib::TraitAttitude> sAttitudeStack;
static APF_Lib::TraitAttitude sCurrentAttitudeForEmoji;
static float sEmojiCounter=0.f;
static constexpr float sEmojiChangeTime=1.f;

static APF_Lib::MM::GameTime sInterSessionTime(0, 0, 7, 0, 0, 0.f);
static APF_Lib::MM::GameTime sTargetTime;


//////////////////////////////////////////////////////////////////////////
static int GetMentalStateIndex(eLoggedMentals logMental)
{
	switch (logMental) {
	case VALENCE_POS:       return (int)APF_Lib::MENTAL_FEATURE_POSITIVE;
	case VALENCE_NEG:       return (int)APF_Lib::MENTAL_FEATURE_NEGATIVE;
	case MOOD_PLEASED:      return (int)APF_Lib::MENTAL_FEATURE_PLEASED;
	case MOOD_DISAPLEASED:  return (int)APF_Lib::MENTAL_FEATURE_DISPLEASED;
	case EMOTION_HOPE:      return (int)APF_Lib::MENTAL_FEATURE_HOPE;
	case EMOTION_FEAR:      return (int)APF_Lib::MENTAL_FEATURE_FEAR;
	case EMOTION_JOY:       return (int)APF_Lib::MENTAL_FEATURE_JOY;
	case EMOTION_DISRESS:   return (int)APF_Lib::MENTAL_FEATURE_DISTRESS;
	case FEELING_SATISFY:   return (int)APF_Lib::MENTAL_FEATURE_SATISFACTION;
	case FEELING_FEARS:     return (int)APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED;
	case FEELING_RELIEF:    return (int)APF_Lib::MENTAL_FEATURE_RELIEF;
	case FEELING_DISPPOINT: return (int)APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT;
	default: return -1;
	}
}

//////////////////////////////////////////////////////////////////////////
static void GetPositiveAndNegativeAverageForLoggedMentals(float const(&mentals)[APF_Lib::NUM_MENTAL_FEATURES], float& posAvg, float& negAvg)
{
	float posSum = 0.f;
	float posCnt = 1.f;//6.f;
	float negSum = 0.f;
	float negCnt = 1.f;//6.f;

	posSum += mentals[APF_Lib::MENTAL_FEATURE_POSITIVE];
	/*posSum += mentals[APF_Lib::MENTAL_FEATURE_PLEASED];
	posSum += mentals[APF_Lib::MENTAL_FEATURE_HOPE];
	posSum += mentals[APF_Lib::MENTAL_FEATURE_JOY];
	posSum += mentals[APF_Lib::MENTAL_FEATURE_SATISFACTION];
	posSum += mentals[APF_Lib::MENTAL_FEATURE_RELIEF];*/
	posAvg = posSum / posCnt;

	negSum += mentals[APF_Lib::MENTAL_FEATURE_NEGATIVE];
	/*negSum += mentals[APF_Lib::MENTAL_FEATURE_DISPLEASED];
	negSum += mentals[APF_Lib::MENTAL_FEATURE_FEAR];
	negSum += mentals[APF_Lib::MENTAL_FEATURE_DISTRESS];
	negSum += mentals[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED];
	negSum += mentals[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT];*/
	negAvg = negSum / negCnt;
}

//////////////////////////////////////////////////////////////////////////
static float GetMentalValue(const void* data, int idx)
{
    float const* floats = reinterpret_cast<float const*>(data);
    return floats[idx];
}

//////////////////////////////////////////////////////////////////////////
static void GetActionNames(const char** actions, std::vector<std::string>& rewardTempArray, std::string const& noReward)
{
    int curHistorySize = (int)sGameHistories.size() - sSessionBeginIdx;
    for (int i = 0; i < curHistorySize; i++) {
        actions[2 * i] = sPossibleEvents[sGameHistories[i + sSessionBeginIdx].eventIdx].action.name.c_str();
        int rewardIdx = sGameHistories[i + sSessionBeginIdx].rewardIdx;
        if (rewardIdx >= 0) {
            GameEventInfo const& r = sPossibleRewards[rewardIdx];
            std::string rewardTemp = r.action.name + " " + r.object->GetName();
            rewardTempArray.push_back(rewardTemp);
            actions[2 * i + 1] = rewardTempArray.back().c_str();
        }
        else {
            actions[2 * i + 1] = noReward.c_str();
        }
    }
}

//////////////////////////////////////////////////////////////////////////
static void RenderAttitudeHistories()
{
	if (sStartAttitudePraises.empty()) {
		return;
	}

	std::vector<APF_Lib::MM::GameObject*> const& objects = g_theGame->GetAllObjects();
	size_t objectNum = objects.size();
	size_t numAttitudes = objectNum*2;
	float** attitudes = new float*[numAttitudes];
	int curHistorySize = (int)sStartAttitudePraises.size();

	//attitude values
	for (int i = 0; i < objectNum; i++) {
		attitudes[i*2] = new float[curHistorySize];
        attitudes[i * 2 + 1] = new float[curHistorySize];
        APF_Lib::MM::GameObject* ob = objects[i];

		float* likings = attitudes[i*2];
		float* familarities = attitudes[i*2+1];

		for (int j = 0; j < curHistorySize; j++) {
			APF_Lib::TraitAttitude const& attBefore = sStartAttitudePraises[j].attitudes[ob];
			likings[j] = attBefore.m_traits[APF_Lib::ATTITUDE_LIKING];
			familarities[j] = attBefore.m_traits[APF_Lib::ATTITUDE_FAMILIARITY];
		}
	}

	//attitude names
	ImColor* colors = new ImColor[numAttitudes];
	std::vector<std::string> nameStrings;
	for (int i = 0; i < objectNum; i++) {
		std::string const& n = objects[i]->GetName();
		nameStrings.push_back(n+"_l");
		colors[i*2].SetHSV((float)(i*2)/(float)(numAttitudes),1.f,1.f);
		nameStrings.push_back(n+"_f");
		colors[i*2+1].SetHSV((float)(i*2+1) / (float)(numAttitudes), 1.f, 1.f);
	}

	//action names
	ImGui::PlotMultiLines("", (int)(objectNum*2), &nameStrings[0], colors, GetMentalValue, reinterpret_cast<const void* const*>(attitudes),
		curHistorySize, MIN_UNIT_VALUE, MAX_UNIT_VALUE, ImVec2(1250.f, 160.f),nullptr);

	for (int i = 0; i < numAttitudes; i++) {
		delete[] attitudes[i];
	}
	delete[] attitudes;
	delete[] colors;
}

//////////////////////////////////////////////////////////////////////////
static void RenderPraiseHistories()
{
    if (sStartAttitudePraises.empty()) {
        return;
    }

    size_t actionNum = APF_Lib::MM::ActionInfo::sPossibleActions.size();
    float** praises = new float* [actionNum];
    int curHistorySize = (int)sStartAttitudePraises.size();

    //attitude values
    for (int i = 0; i < actionNum; i++) {
        praises[i] = new float[curHistorySize];

        APF_Lib::MM::ActionInfo const& a = APF_Lib::MM::ActionInfo::sPossibleActions[i];
        float* p = praises[i];

        for (int j = 0; j < curHistorySize; j++) {
            p[j] = sStartAttitudePraises[j].praises[&a];
        }
    }

    //attitude names
    ImColor* colors = new ImColor[actionNum];
    std::vector<std::string> nameStrings;
    for (int i = 0; i < actionNum; i++) {
        std::string const& n = APF_Lib::MM::ActionInfo::sPossibleActions[i].actionName;
        nameStrings.push_back(n);
        colors[i].SetHSV((float)(i) / (float)(actionNum), 1.f, 1.f);
    }

    //action names
    ImGui::PlotMultiLines("", (int)actionNum, &nameStrings[0], colors, GetMentalValue, reinterpret_cast<const void* const*>(praises),
        curHistorySize, MIN_UNIT_VALUE, MAX_UNIT_VALUE, ImVec2(1250.f, 160.f), nullptr);

    for (int i = 0; i < actionNum; i++) {
        delete[] praises[i];
    }
    delete[] colors;
}

//////////////////////////////////////////////////////////////////////////
static void RenderEmotionHistories()
{
	if (sGameHistories.size() <= sSessionBeginIdx) {
		return;
	}

	int emotionSize = sMentalLogEndIdx-sMentalLogStartIdx;
    float** values = new float* [emotionSize];
	int curHistorySize = (int)sGameHistories.size()-sSessionBeginIdx;
	std::string noReward = "No Reward";
	std::vector<std::string> rewardTempArray;
	const char** actions = new const char*[2*curHistorySize];
	GetActionNames(actions, rewardTempArray, noReward);

	int curMentalSize = curHistorySize*2+1;
	const char** names = new const char*[emotionSize];
	ImColor* colors = new ImColor[emotionSize];
	for(int mIdx=sMentalLogStartIdx;mIdx<sMentalLogEndIdx;mIdx++){
		names[mIdx-sMentalLogStartIdx] = GameHistory::sLoggedMentalNames[mIdx];
		colors[mIdx-sMentalLogStartIdx] = GameHistory::sMentalColors[mIdx];
		values[mIdx-sMentalLogStartIdx] = new float[curMentalSize];
		float* curVal = values[mIdx-sMentalLogStartIdx];
		int actualIndex = GetMentalStateIndex((eLoggedMentals)mIdx);
		curVal[0] = sStartMental[actualIndex];
        for (int i = 0; i < curHistorySize; i++) {
            curVal[2*i+2] = sGameHistories[i+sSessionBeginIdx].mentalAfter[actualIndex];
			curVal[2*i+1]=sGameHistories[i+sSessionBeginIdx].mentalBefore[actualIndex];
        }
	}

    ImGui::PlotMultiLines("", emotionSize, names, colors,
			GetMentalValue, reinterpret_cast<const void* const*>(values), curMentalSize,
			MIN_UNIT_VALUE, MAX_UNIT_VALUE, ImVec2(1250.f,160.f), actions);

	for (int mIdx = 0; mIdx < emotionSize; mIdx++) {
		delete[] values[mIdx];
	}
	delete[] values;
	delete[] colors;
	delete[] names;
	delete[] actions;
}

//////////////////////////////////////////////////////////////////////////
static float GetTargetAndColorForEmoji(float const(&ms)[APF_Lib::NUM_MENTAL_FEATURES], ImColor& color)
{
	float target=0.f;
    float posAvg = 0.f;
    float negAvg = 0.f;
    GetPositiveAndNegativeAverageForLoggedMentals(ms, posAvg, negAvg);
    color = ImVec4(posAvg, 0.f, negAvg, 1.f);
    target = -posAvg;
    if (negAvg > posAvg) {
        target = negAvg;
    }
	return target;
}

//////////////////////////////////////////////////////////////////////////
static float GetTargetAndColorForEmoji(APF_Lib::TraitAttitude const& att, ImColor& color)
{
	float target = att.m_traits[APF_Lib::ATTITUDE_LIKING];
	color = ImVec4(ClampZeroToOne(target*2.f),0.f,ClampZeroToOne(att.m_traits[APF_Lib::ATTITUDE_FAMILIARITY]*2.f),1.f);
	target = .5f-target;
	return target;
}

//////////////////////////////////////////////////////////////////////////
void Game::LogGameHistory()
{
	std::string log;
	int sessionIdx=-1;
	for (GameHistory const& history:sGameHistories) {
        if (history.sessionIdx != sessionIdx) {
            sessionIdx = history.sessionIdx;
			log+="Init Mental\n";
			log+=GetGraphvizVerboseTextForMentalState(sStartMentalHistories[sessionIdx])+"\n";
			log+=sStartAttitudePraises[sessionIdx].GetLogText();
		}
		log += Stringf("Session %i Action %i Time %s\n", history.sessionIdx,history.actionIdx,history.time.ToString().c_str());
		log += sPossibleEvents[history.eventIdx].ToString() + "\n";
		log += GetGraphvizVerboseTextForMentalState(history.mentalBefore)+"\n";
		log+=history.attitudePraiseBefore.GetLogText();
        if (history.rewardIdx >= 0) {
            log+= sPossibleRewards[history.rewardIdx].ToString() + "\n";			
        }
        else {
            log+= "NO Reward\n";
        }
		log += GetGraphvizVerboseTextForMentalState(history.mentalAfter)+"\n";
		log+=history.attitudePraiseAfter.GetLogText();
	}

	if (m_sessionIdx < GAME_SESSION_NUMBER) {
		log+="Win Ending\n";
		log += GetGraphvizVerboseTextForMentalState(sStartMentalHistories.back())+"\n";
		log += sStartAttitudePraises.back().GetLogText();
	}

	Time curTime = GetRealWorldTime();
	std::string logName = Stringf("Data/Console/log_%i%i_%i%i.txt",curTime.mon,curTime.day,curTime.hour,curTime.min);
	if (!FileWriteToDisk(logName, &log[0], log.size())) {
		g_theConsole->PrintError("Fail to save game history log");
	}
}

//////////////////////////////////////////////////////////////////////////
static void DrawActualEmoji(float target, ImColor const& color)
{
    ImVec2 winSize = ImGui::GetWindowSize();
    float radius = MinFloat(winSize.x, winSize.y) * .4f;
    ImVec2 centerPos = ImGui::GetWindowPos() + ImVec2(winSize.x * .5f, winSize.y * .6f);
	ImVec2 baseMouth = centerPos;
	if (target < 0.f) {
		baseMouth+=ImVec2(0.f,radius*.7f);
	}
    ImVec2 leftDir(-radius, 0.f);
    ImVec2 upDir(0.f, radius * target);
    ImDrawList* drawList = ImGui::GetWindowDrawList();
	//mouth
    drawList->AddBezierCurve(baseMouth + leftDir + upDir, baseMouth + leftDir, baseMouth - leftDir, baseMouth - leftDir + upDir, color, 10.f);
	//eyes
	ImVec2 leftEye = leftDir*.3f;
	ImVec2 upEye = upDir*.7f;
	ImVec2 baseEye = centerPos+ImVec2(-radius*.4f,-radius*.5f);
	if (target < 0.f) {
		baseEye +=ImVec2(0.f,-radius*.5f);
	}
	drawList->AddBezierCurve(baseEye+leftEye-upEye,baseEye+leftEye,baseEye-leftEye,baseEye-leftEye-upEye,color,5.f);
	baseEye = centerPos+ ImVec2(radius * .4f, -radius * .5f);
    if (target < 0.f) {
        baseEye += ImVec2(0.f, -radius * .5f);
    }
	drawList->AddBezierCurve(baseEye+leftEye-upEye,baseEye+leftEye,baseEye-leftEye,baseEye-leftEye-upEye,color,5.f);
}

//////////////////////////////////////////////////////////////////////////
static AttitudePraise GetCurrentAttitudePraise()
{
	APF_Lib::MM::GameActor* target = g_theGame->GetActorFromName("Bob");
	std::vector<APF_Lib::MM::GameObject*> objects = g_theGame->GetAllObjects();
    AttitudePraise attPraise;
    for (APF_Lib::MM::GameObject const* ob : objects) {
        APF_Lib::TraitAttitude att = g_APF->GetAttitude(target->GetIndex(), ob->GetIndex());
        attPraise.attitudes[ob] = att;
    }
    for (APF_Lib::MM::ActionInfo const& a : APF_Lib::MM::ActionInfo::sPossibleActions) {
        float praise = g_APF->GetPraise(target->GetIndex(), a.apfIdx);
        attPraise.praises[&a] = praise;
    }
	return attPraise;
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderMentalEmoji() const
{
	if (sEmojiCounter <= 0.f && sEmojiStack.empty()) {
        float mentals[APF_Lib::NUM_MENTAL_FEATURES];
        g_APF->GetANPCsMentalState(mentals, m_actors[0]->GetIndex());
        sCurrentEmotionForEmoji.SetMentalState(mentals);
		ImColor color;
		float target = GetTargetAndColorForEmoji(mentals, color);
		DrawActualEmoji(target, color);
		return;
	}

    static APF_Lib::MentalState lastEmoState;
    static APF_Lib::MentalState nextMental = sCurrentEmotionForEmoji;
    if (sEmojiCounter <= 0.f) {
        lastEmoState = sCurrentEmotionForEmoji;
		sCurrentEmotionForEmoji = sEmojiStack.front();
        nextMental = sCurrentEmotionForEmoji;
		sEmojiStack.pop();
		sEmojiCounter = sEmojiChangeTime;
	}

	APF_Lib::MentalState curMental;
	APF_Lib::MentalState::CombineMentalStates(lastEmoState,nextMental,sEmojiCounter/sEmojiChangeTime,curMental);
	sCurrentEmotionForEmoji = curMental;
	sEmojiCounter-=(float)m_gameClock->GetLastDeltaSeconds();

	float curMentalFloatsp[APF_Lib::NUM_MENTAL_FEATURES];
	curMental.GetMentalState(curMentalFloatsp);
	
	ImColor color;
	float target =GetTargetAndColorForEmoji(curMentalFloatsp, color);
	DrawActualEmoji(target, color);
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderAttitudeEmoji() const
{
	APF_Lib::MM::GameObject const* object = GetObjectFromName("Cricket");
	if (sEmojiCounter <= 0.f && sAttitudeStack.empty()) {
		APF_Lib::TraitAttitude att = g_APF->GetAttitude(m_actors[0]->GetIndex(), object->GetIndex());
		ImColor color;
		float target = GetTargetAndColorForEmoji(att, color);
		DrawActualEmoji(target, color);
		return;
	}

	static APF_Lib::TraitAttitude lastAttitude;
	static APF_Lib::TraitAttitude nextAttitude = sCurrentAttitudeForEmoji;
	if (sEmojiCounter <= 0.f) {
		lastAttitude = sCurrentAttitudeForEmoji;
		sCurrentAttitudeForEmoji = sAttitudeStack.front();
		nextAttitude = sCurrentAttitudeForEmoji;
		sAttitudeStack.pop();
		sEmojiCounter = sEmojiChangeTime;
	}

	APF_Lib::TraitAttitude curAttitude;
	CombineAttitude(lastAttitude,nextAttitude, sEmojiCounter/sEmojiChangeTime, curAttitude);
	sCurrentAttitudeForEmoji = curAttitude;
	sEmojiCounter -= (float)m_gameClock->GetLastDeltaSeconds();

	ImColor color;
	float target = GetTargetAndColorForEmoji(curAttitude, color);
	DrawActualEmoji(target, color);
}

//////////////////////////////////////////////////////////////////////////
static void PopulateObjectStatesFromActionState(GameEventInfo& newInfo, APF_Lib::MM::GameObject const* object, XmlElement const* actionStateElement)
{
    XmlElement const* osElement = actionStateElement->FirstChildElement("ObjectState");
    while (osElement != nullptr) {
        APF_Lib::MM::ObjectState newState;
        MemoryUtils::PopulateObjectStateFromXML(newState, osElement);
		std::string name=ParseXmlAttribute(*osElement, "name",  "-");
		if(name=="-"){
			newInfo.newStates[object] = newState;
		}
		else {
			APF_Lib::MM::GameObject const* target = g_theGame->GetObjectFromName(name);
			newInfo.newStates[target] = newState;
		}
        osElement = osElement->NextSiblingElement("ObjectState");
    }
}

//////////////////////////////////////////////////////////////////////////
static GameEventInfo const* GetRewardEventInfoForActionName(std::string const& action)
{
	for (GameEventInfo const& info : sPossibleRewards) {
		if (info.action.name == action) {
			return &info;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
static std::map<APF_Lib::MM::GameObject const*, APF_Lib::MM::ObjectState> const* GetStateChangeForEvent(APF_Lib::MM::GameEvent const& event)
{
	int actionAPFIdx = event.m_action;
	int objectAPFIdx = event.m_patient;

	for (GameEventInfo const& info : sPossibleEvents) {
		if (APF_Lib::MM::ActionInfo::sPossibleActions[info.action.idx].apfIdx == actionAPFIdx &&
			info.object->GetIndex() == objectAPFIdx) {
			return &info.newStates;
		}
	}

	for (GameEventInfo const& reward : sPossibleRewards) {
		if (APF_Lib::MM::ActionInfo::sPossibleActions[reward.action.idx].apfIdx == actionAPFIdx &&
			reward.object->GetIndex() == objectAPFIdx) {
			return &reward.newStates;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
Game::Game()
{
}

//////////////////////////////////////////////////////////////////////////
void Game::StartUp()
{
	//Init
	g_theRNG = new RandomNumberGenerator();
	g_theGame = this;
	g_theInput->PushMouseOptions(eMousePositionMode::MOUSE_ABSOLUTE, true, false);
	m_gameClock = new Clock();
	g_theRenderer->SetupParentClock(m_gameClock);
	APF_Lib::MM::Logger::sLogger = new GameLogger();

	g_theFont = g_theRenderer->CreateOrGetBitmapFont("Data/Fonts/SquirrelFixedFont");
	g_theRNG = new RandomNumberGenerator();
	m_worldCamera = new Camera();
	m_uiCamera = new Camera();

	//init camera settings
	Vec2 resolution = g_theApp->GetWindowDimensions();
	sWorldHeight = resolution.y;
	sWorldAspect = resolution.x/resolution.y;
	Vec2 halfSize = resolution*.5f;
	m_worldCamera->SetClearMode(CLEAR_COLOR_BIT, Rgba8::BLACK);
	m_worldCamera->SetOrthoView(-halfSize, halfSize);
	
	m_uiCamera->SetColorTarget(nullptr);
	m_uiCamera->SetOrthoView(-halfSize, halfSize );

	//init APF
	g_APF = APF_Lib::MM::MemorySystem::GetMemoryAPF();
    m_worldClock = new APF_Lib::MM::MemoryClock();
	g_theEvents->RegisterMethodEvent("OnConfirmPressed", this, &Game::OnNewEventSubmitted, "confirm pressed to submit new event", eEventFlag::EVENT_GAME);
}

//////////////////////////////////////////////////////////////////////////
void Game::ShutDown()
{	
	LogGameHistory();

	g_theEvents->UnsubscribeObject(this);
	g_theInput->PopMouseOptions();

	g_APF->TerminateEventHandler();
	g_APF->TerminateANPCsMentalStates();
	g_APF->TerminateRelations();
	g_APF->TerminateVocabulary();

	for (APF_Lib::MM::GameActor* actor : m_actors) {
		delete actor;
	}
	m_actors.clear();

	for (APF_Lib::MM::GameObject* object : m_objects) {
		delete object;
	}
	m_objects.clear();

	APF_Lib::MM::MemorySystem::ClearMemoryAPF();
	delete m_worldClock;
    delete m_worldCamera;
	delete m_uiCamera;
    delete g_theRNG;

	g_APF = nullptr;
}

//////////////////////////////////////////////////////////////////////////
void Game::BeginFrame()
{
	APF_Lib::MM::MemoryClock::BeginFrame((float)m_gameClock->GetLastDeltaSeconds());
}

//////////////////////////////////////////////////////////////////////////
void Game::Update()
{	
	if (!m_startedLoading) {	//first frame
		m_startedLoading = true;
	}
	else if(m_isLoading){	//loading start
		LoadAndInitialize();
	}
	else{	//load finished
		if (m_isProgressing) {
			if (m_worldClock->GetTotalTime() >= sTargetTime) {
				StartNewSession();
			}
		}

		g_theConsole->Update();
        UpdateForInput();
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::Render() const
{
	RenderForGame();

    RenderForUI();

	g_theConsole->Render(g_theRenderer);
}

//////////////////////////////////////////////////////////////////////////
void Game::SpawnActor(XmlElement const* element)
{
	APF_Lib::MM::GameActor* newActor = new APF_Lib::MM::GameActor(m_worldClock->GetTotalTime());
	APF_Lib::MM::MemorySystem* memSys = newActor->GetMemorySystem();
	memSys->Initialize();
	m_actors.push_back(newActor);
    MemoryUtils::PopulateActorFromXML(*newActor, element);
}

//////////////////////////////////////////////////////////////////////////
void Game::SpawnObject(XmlElement const* element)
{
	APF_Lib::MM::GameObject* newObj = new APF_Lib::MM::GameObject(m_worldClock->GetTotalTime());
	m_objects.push_back(newObj);
	MemoryUtils::PopulateGameObjectFromXML(*newObj, element);
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MM::GameActor* Game::GetActorFromName(std::string const& name) const
{
	for (APF_Lib::MM::GameActor* a : m_actors) {
		if (a->GetName() == name) {
			return a;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MM::GameObject* Game::GetObjectFromName(std::string const& name) const
{
	for (APF_Lib::MM::GameObject* o : m_objects) {
		if (o->GetName() == name) {
			return o;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MM::GameObject* Game::GetObjectFromAPFIdx(int apfIdx) const
{
	for (APF_Lib::MM::GameObject* o : m_objects) {
		if (o->GetIndex() == apfIdx) {
			return o;
		}
	}

	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
std::vector<APF_Lib::MM::GameObject const*> Game::GetAllObjectsOfTag(std::string const& name) const
{
	std::vector<APF_Lib::MM::GameObject const*> result;
	for (APF_Lib::MM::GameObject const* ob : m_objects) {
		if (ob->GetTags().HasTag(name)) {
			result.push_back(ob);
		}
	}
	return result;
}

//////////////////////////////////////////////////////////////////////////
bool Game::OnNewEventSubmitted(EventArgs& args)
{
	UNUSED(args);
	ProcessSelection();
	m_actionIdx++;
	if (m_actionIdx >= GAME_ACTION_NUMBER) {
		m_actionIdx=0;
		m_sessionIdx++;
		ProgressToNextSession();
		if (m_sessionIdx >= GAME_SESSION_NUMBER) {
			EndGame();
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderActorLTMToDisk(APF_Lib::MM::GameActor const* actor, std::string const& imgFileName) const
{
	APF_Lib::MM::LongTermMemory const* ltm = actor->GetMemorySystem()->GetLTM();
	std::string graphvizText = ltm->GetTextForGraph(MemoryUtils::GetGraphvizDebugText, MemoryUtils::GetGraphvizDebugText);
	if (!FileWriteToDisk(imgFileName, &graphvizText[0], graphvizText.size())) {
		g_theConsole->PrintError(Stringf("Fail to save %s on disk", imgFileName.c_str()));
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderLTMImgToScreen(std::string const& imgFileName, APF_Lib::MM::GameActor const* actor) const
{
    RenderActorLTMToDisk(actor, "Temp.dot");
    RenderGraphvizImage("Temp.dot", imgFileName);

	sLTMImgTex = g_theRenderer->CreateOrGetTextureFromFile(imgFileName.c_str(), true, false);
}

//////////////////////////////////////////////////////////////////////////
void Game::LoadAndInitialize()
{
	//xml reading
	PopulatePersonalitiesFromXML("Data/Definitions/Personalities.xml");
	PopulateSocialRolesFromXML("Data/Definitions/SocialRoles.xml");	
	MemoryUtils::PopulateStatesFromXML("Data/Definitions/States.xml");

    //init vocabs
    g_APF->InitializeVocabulary();
	InitializeForActors();
    InitializeForObjects();
	MemoryUtils::PopulateActionsFromXML("Data/Definitions/Actions.xml");

	g_APF->SortVocabulary();
	MemoryUtils::UpdateANPCIndexes(m_actors);
	MemoryUtils::UpdateObjectIndexes(m_objects);
	MemoryUtils::UpdateActionIndexes();

    //init anpc mental
    g_APF->InitializeANPCsMentalStates();
	PostInitializeActorsAndObjects();

	//init relations
	g_APF->InitializeRelations();
    //Adding relations

	//init events
	g_APF->InitializeEventHandler();

	MemoryUtils::PopulateSTMsFromXML("Data/Definitions/ShortTermMemories.xml");

	MemoryUtils::PopulateAttitudesFromFile("Data/Definitions/Attitudes.txt");
	InitializePraises();
	InitializePossibleEvents();

	StartNewSession();
	m_isLoading = false;
}

//////////////////////////////////////////////////////////////////////////
void Game::InitializeForActors()
{
	XmlDocument xmlDoc;
	XmlError code = xmlDoc.LoadFile("Data/Definitions/Actors.xml");
	if (code != XmlError::XML_SUCCESS) {
		g_theConsole->PrintError("Failed to load Data/Definitions/Actors.xml");
		return;
	}

	XmlElement const* actor = xmlDoc.RootElement()->FirstChildElement("Actor");
	while (actor != nullptr) {
		SpawnActor(actor);
		actor = actor->NextSiblingElement("Actor");
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::InitializeForObjects()
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile("Data/Definitions/Objects.xml");
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError("Failed to load Data/Definitions/Objects.xml");
        return;
    }

    XmlElement const* obj = xmlDoc.RootElement()->FirstChildElement("Object");
    while (obj != nullptr) {
        SpawnObject(obj);
        obj = obj->NextSiblingElement("Object");
    }
}

//////////////////////////////////////////////////////////////////////////
void Game::PostInitializeActorsAndObjects()
{
	for (APF_Lib::MM::GameActor* a : m_actors) {
		MemoryUtils::InitializeActor(*a);
		a->GetMemorySystem()->m_getProspectFromTraces = MemoryUtils::GenerateMentalSalientProspectMemoryFromTraces;
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::InitializePraises()
{
	for (APF_Lib::MM::GameActor* a : m_actors) {
		for (APF_Lib::MM::ActionInfo& info : APF_Lib::MM::ActionInfo::sPossibleActions) {
			g_APF->AddPraise(a->GetIndex(), info.apfIdx, info.effect);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::InitializePossibleEvents()
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile("Data/Definitions/ActionStates.xml");
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError("Failed to load Data/Definitions/ActionStates.xml");
        return;
    }

	std::map<std::string, XmlElement const*> actionStates;
	XmlElement const* asElement = xmlDoc.RootElement()->FirstChildElement("ActionState");
	while (asElement)	{
		std::string actionName = ParseXmlAttribute(*asElement,"action","");
		actionStates[actionName] = asElement;
		asElement = asElement->NextSiblingElement("ActionState");
	}

	std::vector<std::string> nonInsectTags;
	nonInsectTags.push_back("reward");
	nonInsectTags.push_back("imagine");
	std::vector<ActionAccessInfo> events = MemoryUtils::GetAllActionsWithoutTags(nonInsectTags);
	std::vector<APF_Lib::MM::GameObject const*> eventObs = GetAllObjectsOfTag("insect");
	//TODO Use the first one as insect this term
	APF_Lib::MM::GameObject const* insect = eventObs[0];
	sPossibleEvents.clear();
	for (ActionAccessInfo& e : events) {
		GameEventInfo& newInfo = sPossibleEvents.emplace_back(e, insect);
		auto it = actionStates.find(e.name);
		if (it != actionStates.end()) {
			PopulateObjectStatesFromActionState(newInfo,insect,it->second);
		}
	}

	events = MemoryUtils::GetAllActionsOfTag("reward");
	eventObs = GetAllObjectsOfTag("reward");
	//TODO Use the first one as reward action
    ActionAccessInfo& reward = events[0];
	sPossibleRewards.clear();
    auto it = actionStates.find(reward.name);
	for (APF_Lib::MM::GameObject const* ob: eventObs) {
		if(reward.name!="Actually hurt by"){
            GameEventInfo& newReward = sPossibleRewards.emplace_back(reward, ob);
            PopulateObjectStatesFromActionState(newReward, ob, it->second);
		}
	}
	ActionAccessInfo& actualHurt = events[1];
	it = actionStates.find(actualHurt.name);
	GameEventInfo& hurtEvent = sPossibleRewards.emplace_back(actualHurt, insect);
	PopulateObjectStatesFromActionState(hurtEvent, insect, it->second);
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateForInput()
{
	if (g_theConsole->IsOpen() || m_isLoading) {
		return;
	}

	if (g_theInput->WasKeyJustPressed(KEY_TILDE)) {
        bool isOpen = g_theConsole->IsOpen();
        g_theConsole->SetIsOpen(!isOpen);
	}

	if (g_theInput->WasKeyJustPressed(KEY_ESC)) {
		EndGame();
	}

	//camera back to normal
	if (g_theInput->WasKeyJustPressed('O')) {
		m_worldCamera->SetPosition(Vec3::ZERO);
		m_worldCamera->SetProjectionOrthographic(1080.f);
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::ProcessSelection()
{
    GameHistory history;

	LogCurrentMentalToConsole("start");

	int actorIdx = m_actors[0]->GetIndex();
    g_APF->DegradeMentalStates(APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA);

	LogCurrentMentalToConsole("degrade");

	float mentalBefore[APF_Lib::NUM_MENTAL_FEATURES];
	g_APF->GetANPCsMentalState(mentalBefore, actorIdx);

	GameEventInfo const& info = sPossibleEvents[m_eventSelection];
	UpdateForNewEvent(info.action.idx, info.object);
	history.eventIdx = m_eventSelection;

    float mentalFloats[APF_Lib::NUM_MENTAL_FEATURES];
	g_APF->GetANPCsMentalState(mentalFloats,actorIdx);
	history.mentalBefore.SetMentalState(mentalFloats);

	APF_Lib::MM::GameObject* o = GetObjectFromName("Cricket");
    APF_Lib::TraitAttitude att = g_APF->GetAttitude(actorIdx,o->GetIndex());
    sAttitudeStack.push(att);	

    g_APF->DegradeMentalStates(APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA, APF_Lib::MENTAL_DEGRADE_DELTA);
    LogCurrentMentalToConsole("degrade");

	AttitudePraise attPraise = GetCurrentAttitudePraise();
	history.attitudePraiseBefore = attPraise;

    if (m_reinforceSelection >= 0) {
        GameEventInfo const& reinforce = sPossibleRewards[m_reinforceSelection];
        UpdateForNewEvent(reinforce.action.idx, reinforce.object);

		if (reinforce.action.name == "Actually hurt by") {
			//Update states
			UpdateForNoRewardEvent();
		}
	}
	else {
		UpdateForNoRewardEvent();
	}
    history.rewardIdx = m_reinforceSelection;

	g_APF->GetANPCsMentalState(mentalFloats, actorIdx);
	history.mentalAfter.SetMentalState(mentalFloats);

	LogCurrentMentalToConsole("end");

    att = g_APF->GetAttitude(actorIdx, o->GetIndex());
    sAttitudeStack.push(att);

    attPraise = GetCurrentAttitudePraise();
    history.attitudePraiseAfter = attPraise;

    history.time = m_worldClock->GetTotalTime();	
	history.sessionIdx = m_sessionIdx;
    history.actionIdx = m_actionIdx;
	sGameHistories.emplace_back(history);

	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(500.f);
	APF_Lib::MM::MemoryClock::BeginFrame(1.f);
	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(0.f);
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateForNewEvent(int actionIdx, APF_Lib::MM::GameObject const* object)
{
	APF_Lib::MM::GameEvent event;
    //construct current action
    event.m_actionInfo = &APF_Lib::MM::ActionInfo::sPossibleActions[actionIdx];
	event.m_target = object;
    event.m_isGarbage = false;
    event.m_owner = event.m_actor = m_actors[0]->GetIndex();
    event.m_action = APF_Lib::MM::ActionInfo::sPossibleActions[actionIdx].apfIdx;
    event.m_patient = object->GetIndex();
    //TODO fixed patient type to object
    event.m_patientType = APF_Lib::PATIENT_OBJECT;
	event.m_certainty = 1.f;

	UpdateForNewEvent(event);
	event.m_isGarbage=true;
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateForNewEvent(APF_Lib::MM::GameEvent const& event)
{
	UpdateNewEventForSelfInfo(event);
	UpdateNewEventForOthers();
	UpdateActorMemories();
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateForNoRewardEvent()
{
	GameEventInfo const* info = GetRewardEventInfoForActionName("Rewarded with");
	if (info == nullptr) {
		g_theConsole->PrintError("Fail to load event info of Rewarded with");
		return;
	}

	APF_Lib::MM::GameEvent event;
    //construct current action
    event.m_isGarbage = false;
    event.m_owner = event.m_actor = m_actors[0]->GetIndex();
    event.m_action = APF_Lib::MM::ActionInfo::sPossibleActions[info->action.idx].apfIdx;

	//update object
    for (auto it = info->newStates.begin(); it != info->newStates.end(); it++) {
		APF_Lib::MM::GameObject* object = const_cast<APF_Lib::MM::GameObject*>(it->first);
        object->SetNewState(it->second);
    }

	UpdateNewEventForOthers();

	for (APF_Lib::MM::GameActor* a : m_actors) {
		a->GetMemorySystem()->UpdateWithoutAppraisal(event);
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateNewEventForSelfInfo(APF_Lib::MM::GameEvent const& event)
{
	for (APF_Lib::MM::GameActor* a : m_actors) {
		a->UpdateForSelfAction(event);
	}

	std::map<APF_Lib::MM::GameObject const*, APF_Lib::MM::ObjectState> const* newStates = GetStateChangeForEvent(event);
	if (newStates) {
		for (auto it = newStates->begin(); it != newStates->end(); it++) {
			APF_Lib::MM::GameObject* object = const_cast<APF_Lib::MM::GameObject*>(it->first);
			object->SetNewState(it->second);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateNewEventForOthers()
{
    for (APF_Lib::MM::GameActor* a : m_actors) {
        a->UpdateForSensingObjects(m_objects);
		a->UpdateForSensingActors(m_actors);
    }
}

//////////////////////////////////////////////////////////////////////////
void Game::UpdateActorMemories()
{
    for (APF_Lib::MM::GameActor* a : m_actors) {
        a->UpdateCurrentActionToMemorySystem();
    }
}

//////////////////////////////////////////////////////////////////////////
void Game::ProgressToNextSession()
{
	for (APF_Lib::MM::GameActor* a : m_actors) {
		a->ConsolidateAndRefreshSTM();
	}
	
	m_isProgressing = true;
	sTargetTime = m_worldClock->GetTotalTime()+sInterSessionTime;
	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(100000);
}

//////////////////////////////////////////////////////////////////////////
void Game::StartNewSession()
{
	APF_Lib::MM::GameActor* target = m_actors[0];
    //empty all
	APF_Lib::MM::GameState gameState;
    APF_Lib::MM::ObjectState state;
	for (APF_Lib::MM::GameObject* ob : m_objects) {
		ob->SetNewState(state);
		gameState.UpdateStateForObject(ob);
	}

	APF_Lib::MM::GameObject* cricket = GetObjectFromName("Cricket");
	//check satisfying ending requirement
	float cricketLiking = g_APF->GetAttitude(target->GetIndex(), cricket->GetIndex())[APF_Lib::ATTITUDE_LIKING];
	if (cricketLiking < GAME_LOSE_LIKING || cricketLiking>GAME_WIN_LIKING) {
		EndGame();
	}

    //init cricket state
    state.AddState("contained", 1.f);
    state.AddState("moving", 0.f);
    state.AddState("hurting", 0.f);
	cricket->SetNewState(state);

	//init actor mental states: valence
	APF_Lib::MentalState mental;
	SetMentalStateRandomly(mental,.1f,.1f);
	g_APF->SetANPCsMentalState(target->GetIndex(),mental);

    sStartMental = mental;
    sStartMentalHistories.push_back(sStartMental);
	
	AttitudePraise attPraise = GetCurrentAttitudePraise();
	sStartAttitudePraises.push_back(attPraise);

	//STM
	gameState.UpdateStateForObject(cricket);
	APF_Lib::MM::Memory curMemory;
	target->GenerateMemory(gameState, curMemory);
	target->InitializeSTM(curMemory);

	sSessionBeginIdx = (int)sGameHistories.size();
    m_isProgressing = false;
	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(0.f);
}

//////////////////////////////////////////////////////////////////////////
static void AppendLikeHateForObject(std::string& text, std::string const& name, float liking)
{
	if (liking < .27f) {
		text+="hates "+name+" very much";
	}
	else if (liking < .5f) {
		text += "hates "+name+" for some extent";
	}
	else if (liking < .73f) {
		text += "likes "+name+" for some extent";
	}
	else {
		text += "likes "+name +" very much";
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::EndGame()
{
	m_isEnded = true;    

	int session = m_actionIdx==0? m_sessionIdx : m_sessionIdx+1;
	int actorIdx = m_actors[0]->GetIndex();
	APF_Lib::MM::GameObject const* cricket = GetObjectFromName("Cricket");
	float originLiking = sStartAttitudePraises[0].attitudes[cricket].m_traits[APF_Lib::ATTITUDE_LIKING];
	float currentLiking = g_APF->GetAttitude(actorIdx, cricket->GetIndex()).m_traits[APF_Lib::ATTITUDE_LIKING];
	float deltaLiking = currentLiking-originLiking;
	std::string likeText;
	AppendLikeHateForObject(likeText, "cricket", currentLiking);
	std::string endText = "a HUGE success!";
	if (currentLiking < .27f) {
		endText = "a quite failure...";
	}
	else if (currentLiking < .5f) {
		endText = "making some progress.";
	}
	sEndingText = Stringf("After %i sessions, Bob gains %f liking to cricket, and currently %s. \nThe treatment is %s ", 
		session, deltaLiking, likeText.c_str(), endText.c_str());
	if (m_sessionIdx >= GAME_SESSION_NUMBER) {
		sEndingText+= "But you have run out of time: Bob is moving to another city. ";
	}
	sEndingText += "\nIn the meantime, after the treatment, Bob: \n";
	for (APF_Lib::MM::GameObject const* obj : m_objects) {
		if (obj != cricket) {
			originLiking = sStartAttitudePraises[0].attitudes[obj].m_traits[APF_Lib::ATTITUDE_LIKING];
			currentLiking = g_APF->GetAttitude(actorIdx, obj->GetIndex()).m_traits[APF_Lib::ATTITUDE_LIKING];
			std::string name = obj->GetName();
			sEndingText += Stringf("  gains %f liking to %s and ", currentLiking-originLiking, name.c_str());
			AppendLikeHateForObject(sEndingText, name, currentLiking);
			sEndingText+="; \n";
		}
	}
}

//////////////////////////////////////////////////////////////////////////
bool Game::IsGlobalTimePaused() const
{
	return APF_Lib::MM::MemoryClock::GetGlobalTimeScale()==0.f;
}

//////////////////////////////////////////////////////////////////////////
void Game::AdjustTimeScale(bool isDescrease, bool isIncrease, bool togglePause) const
{
    float deltaSeconds = (float)m_gameClock->GetLastDeltaSeconds();
    float newGlobalTimeScale = APF_Lib::MM::MemoryClock::GetGlobalTimeScale();
    float newScale = newGlobalTimeScale < 1.f ? TIME_SLOW_RATE * deltaSeconds : TIME_FAST_RATE * deltaSeconds;
    if (isDescrease) {	//smaller world clock time
        newGlobalTimeScale = (newGlobalTimeScale * (1.f - newScale));
    }
    if (isIncrease) {	//larger world clock time
        newGlobalTimeScale = (newGlobalTimeScale * (1.f + newScale));
    }
    static float lastTimeScale = 1.f;
    if (togglePause) {
        if (newGlobalTimeScale > 0.f) {
            lastTimeScale = newGlobalTimeScale;
            newGlobalTimeScale = 0.f;
        }
        else {
            newGlobalTimeScale = lastTimeScale;
        }
    }
	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(newGlobalTimeScale);
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderForGame() const
{
    g_theRenderer->BeginCamera(m_worldCamera);
    g_theRenderer->DisableDepth();

	//TODO game render here

    g_theRenderer->EndCamera(m_worldCamera);
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderForUI() const
{
	AABB2 uiBound = m_uiCamera->GetBounds();
	Vec2 uiDim = uiBound.GetDimensions();

	if (m_isLoading) {
        g_theRenderer->BeginCamera(m_uiCamera);
        g_theRenderer->DisableDepth();

        std::vector<Vertex_PCU> textVerts;
		g_theFont->AddVertsForTextInBox2D(textVerts, uiBound,uiDim.y*.1f, "Loading...");
        g_theRenderer->BindDiffuseTexture(g_theFont->GetTexture());
        g_theRenderer->DrawVertexArray(textVerts);

        g_theRenderer->EndCamera(m_uiCamera);

		return;
	}

	RenderForGameUI();
	RenderForUIDebug();	

	if (m_isEnded) {
		bool popup = true;
		ImGui::Begin("Simulation Ended", &popup, 
			ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoNav);
		ImGui::TextColored(ImVec4(1.f,1.f,0.f,1.f), "Simulation Ended!");
		ImGui::Text(sEndingText.c_str());
		if(ImGui::Button("Restart")){
			sEndingText.clear();
			sSessionBeginIdx = 0;
			sStartMentalHistories.clear();
			sStartAttitudePraises.clear();
			sGameHistories.clear();
			sEmojiCounter=0.f;
			g_theApp->RequestRestartGame();
		}
		ImGui::SameLine();
		if (ImGui::Button("Quit")) {
			g_theApp->HandleQuitRequisted();
		}
		ImGui::End();
	}
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderForGameUI() const
{
	APF_Lib::MM::GameActor* target = m_actors[0];
	
	//actor state
	std::string name = Stringf("ANPC %s State",target->GetName().c_str());
	bool stateOpen = true;
	ImGui::Begin(name.c_str(), &stateOpen, 
		ImGuiWindowFlags_NoDecoration|ImGuiWindowFlags_NoInputs|ImGuiWindowFlags_NoNav);
	ImGui::TextColored(ImVec4(0.f,1.f,1.f,1.f), name.c_str());
	ImGui::Separator();
	//personality
	ImGui::TextColored(ImVec4(0.f, .9f, .9f, .8f),"Personality");
	ImGui::Text(target->GetPersonalityTextForUI().c_str());
	ImGui::Separator();
	// emotion
	ImGui::TextColored(ImVec4(0.f, .9f, .9f, .8f),"Emotion");
	APF_Lib::MentalState mental = m_actors[0]->GetCurrentMentalState();
	ImGui::Text(MemoryUtils::GetDebugText(mental).c_str());
	ImGui::End();

	//emoji
	bool emojiOpen=true;
	ImGui::Begin("Delayed Emoji", &emojiOpen, 
		ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoNav);
	//RenderMentalEmoji();
	RenderAttitudeEmoji();
	ImGui::End();

	//Emotion & mood change
	bool temporalOpen = true;
	ImGui::Begin("Temporal Mental Change", &temporalOpen, 
		ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_NoResize);
	if(ImGui::CollapsingHeader("Mental of Current Session")){
		RenderEmotionHistories();
	}
	if (ImGui::CollapsingHeader("Attitudes through Sessions")) {
		RenderAttitudeHistories();
	}
	if (ImGui::CollapsingHeader("Praises through Sessions")) {
		RenderPraiseHistories();
	}
	ImGui::End();

	//History
	bool historyOpen = true;
	ImGui::Begin("History", &historyOpen, 
		ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_NoResize);
	for (size_t i = sGameHistories.size(); i > 0; i--) {
		GameHistory const& history = sGameHistories[i-1];
		ImGui::Text(Stringf("Session %i Action %i Time %s", history.sessionIdx, history.actionIdx,history.time.ToString().c_str()).c_str());
        ImGui::TextColored(ImVec4(.1f, .5f, .9f, 1.f), sPossibleEvents[history.eventIdx].ToString().c_str());
		ImGui::TextColored(ImVec4(.8f, .1f, .1f, 1.f), GetGraphvizVerboseTextForMentalState(history.mentalBefore).c_str());
		if(history.rewardIdx>=0){
			ImGui::TextColored(ImVec4(.5f, .1f, .9f, 1.f), sPossibleRewards[history.rewardIdx].ToString().c_str());
		}
		else {
			ImGui::TextColored(ImVec4(.5f, .1f, .9f, 1.f), "NO Reward");
		}
        ImGui::TextColored(ImVec4(.8f, .1f, .1f, 1.f), GetGraphvizVerboseTextForMentalState(history.mentalAfter).c_str());
		ImGui::NewLine();
	}
	ImGui::End();

	//Player Interactions
	bool interactOpen = true;
    ImGui::Begin("Player Interactions", &interactOpen, ImGuiWindowFlags_NoResize);
    //current selection
    ImGui::Text("Selected Event: %s", sPossibleEvents[m_eventSelection].ToString().c_str());	
	if(m_reinforceSelection>=0){
		ImGui::Text("Selected Reinforcement: %s", sPossibleRewards[m_reinforceSelection].ToString().c_str());	
	}
	else if (m_reinforceSelection==-1){
		ImGui::Text("Selected Reinforcement: NO Reinforcement");
	}
	else {
		ImGui::Text("Selected Reinforcement: Actual Hurting");
	}
	
	if(!m_isProgressing){
        if (!m_isEnded && ImGui::Button("Confirm")) {
            g_theEvents->FireEvent("OnConfirmPressed");
        }

		ImGui::Columns(2);
		//Event radio	
		ImGui::TextColored(ImVec4(0.f, .9f, .9f, .8f),"Event Options:");
		for (int i=0;i<(int)sPossibleEvents.size();i++) {
			if (ImGui::RadioButton(sPossibleEvents[i].ToString().c_str(), m_eventSelection == i)) {
				m_eventSelection = i; 
			}
		}
		ImGui::NextColumn();
        //reinforce radio
		ImGui::TextColored(ImVec4(0.f, .9f, .9f, .8f), "Reinforcement Options:");
		if (ImGui::RadioButton("NO Reward", m_reinforceSelection == -1)) {
			m_reinforceSelection = -1;
		}
		for (int i = 0; i < (int)sPossibleRewards.size(); i++) {
			if (ImGui::RadioButton(sPossibleRewards[i].ToString().c_str(), m_reinforceSelection == i)) {
				m_reinforceSelection = i;
			}
		}
	}
	else {
		ImGui::NewLine();
		ImGui::Text("Waiting for next session");
	}
	ImGui::End();
}

//////////////////////////////////////////////////////////////////////////
void Game::RenderForUIDebug() const
{
	if (!g_isDebugDrawing) {
		return;
	}

    static bool LTMNodeDraw = false;
    static bool isVerbose = g_debugRenderMode == eDebugMode::DEBUG_VERBOSE;
	APF_Lib::MM::GameActor* target = m_actors[0];
    if (isVerbose) {
        g_debugRenderMode = eDebugMode::DEBUG_VERBOSE;
    }
    else {
        g_debugRenderMode = eDebugMode::DEBUG_LIGHT;
    }

    //In Game UI
    ImGui::Begin("Debug Window", &g_isDebugDrawing, 
		ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_NoResize);	
    ImGui::Checkbox("LTM Node Graph", &LTMNodeDraw);// Edit bool storing our window open/close state
    ImGui::Checkbox("Verbose Debug", &isVerbose);
    ImGui::Text("Game Time: %s", m_worldClock->ToString().c_str());

	//Time scale
	if (!m_isEnded) {
        ImGui::Button("-");
        bool isDecrease = ImGui::IsItemActive();
        ImGui::SameLine();
        ImGui::Text("Time scale: %.2f", m_worldClock->GetScale());
        ImGui::SameLine();
        ImGui::Button("+");
        bool isIncrease = ImGui::IsItemActive();
        ImGui::SameLine();
        bool togglePause = ImGui::Button(IsGlobalTimePaused() ? "Unpause" : "Pause");
        AdjustTimeScale(isDecrease, isIncrease, togglePause);
	}    

	//Emotion draw
	ImGui::Text("Emotion History Range:");
	ImGui::SliderInt("Min Index",&sMentalLogStartIdx, 0, sMentalLogEndIdx-1); 
	ImGui::SliderInt("Max Index", &sMentalLogEndIdx, sMentalLogStartIdx+1, NUM_LOG_MENTALS);

	//attitudes 
    ImGui::Separator();
    ImVec2 windSize = ImGui::GetWindowSize();
	ImGui::BeginChild("Attitudes:", ImVec2(0.f, 120.f));
    ImGui::TextColored(ImVec4(1.f, 1.f, 0.f, 1.f), "Attitudes");
    ImGui::Columns(3);
    ImGui::Text("Name");	ImGui::NextColumn();
    ImGui::Text("Liking");	ImGui::NextColumn();
    ImGui::Text("Familarity");	ImGui::NextColumn();
    for (APF_Lib::MM::GameObject const* o : m_objects) {
        APF_Lib::TraitAttitude att = g_APF->GetAttitude(target->GetIndex(), o->GetIndex());
		ImGui::Text(o->GetName().c_str());	ImGui::NextColumn();
        ImGui::Text(Stringf("%f", att.m_traits[APF_Lib::ATTITUDE_LIKING]).c_str()); ImGui::NextColumn();
        ImGui::Text(Stringf("%f", att.m_traits[APF_Lib::ATTITUDE_FAMILIARITY]).c_str());	ImGui::NextColumn();
    }
    ImGui::EndChild();

	// praises
	ImGui::Separator();
	ImGui::BeginChild("Praises:",ImVec2(0.f, 240.f));
    ImGui::TextColored(ImVec4(1.f, 1.f, 0.f, 1.f), "Praises");
    ImGui::Columns(2);
    ImGui::Text("Name"); ImGui::NextColumn();
    ImGui::Text("Praise"); ImGui::NextColumn();
    for (APF_Lib::MM::ActionInfo const& a : APF_Lib::MM::ActionInfo::sPossibleActions) {
        float praise = g_APF->GetPraise(target->GetIndex(), a.apfIdx);
        ImGui::Text(a.actionName.c_str()); ImGui::NextColumn();
        std::string text = Stringf("%f", praise);
        ImGui::Text(text.c_str()); ImGui::NextColumn();
    }
	ImGui::EndChild();

	// State delta
	ImGui::Separator();
	ImGui::BeginChild("States:", ImVec2(0.f,90.f));
    ImGui::TextColored(ImVec4(1.f, 1.f, 0.f, 1.f), "States");
    ImGui::Columns(2);
    ImGui::Text("State"); ImGui::NextColumn();
    ImGui::Text("Delta Valence"); ImGui::NextColumn();
	for (APF_Lib::MM::StateInfo const& state : APF_Lib::MM::StateInfo::sPossibleStates) {
		ImGui::Text(state.state.name.c_str()); ImGui::NextColumn();
		ImGui::Text(Stringf("%f", state.state.valenceDelta).c_str()); ImGui::NextColumn();
	}
	ImGui::EndChild();

	//objects
	ImGui::Separator();
	ImGui::Text("Cricket:");
	ImGui::Text(MemoryUtils::GetDebugText(*m_objects[0]).c_str());

	//fps
	ImGui::Separator();
    ImGui::Text("fps: % .1f", 1.f / (float)m_gameClock->GetLastDeltaSeconds());
    ImGui::End();

	//LTM Node Graph drawing
    if (LTMNodeDraw) {
        ImGui::Begin("LTM Node Graph", &LTMNodeDraw, ImGuiWindowFlags_HorizontalScrollbar);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
        if (ImGui::Button("Refresh")) {
            std::string fullPath = sLTMImgPath;
            fullPath += ToString(sLTMImgPathSuffix++);
            fullPath += ".png";
            RenderLTMImgToScreen(fullPath, m_actors[0]);
        }

        if (sLTMImgTex) {
            IntVec2 texSize = sLTMImgTex->GetTextureSize();
			ImVec2 rawSize((float)texSize.x, (float)texSize.y);
			static ImVec2 sizeFactor(1.f,1.f);
			if (g_theInput->IsKeyDown(KEY_PLUS)) {
				sizeFactor*=1.01f;
			}
			if (g_theInput->IsKeyDown(KEY_MINUS)) {
				sizeFactor*=0.99f;
			}
            TextureView* texView = sLTMImgTex->GetOrCreateShaderResourceView();
            ImGui::Image(texView->GetSRVHandle(), rawSize*sizeFactor);
        }
        ImGui::End();
    }
}
