#pragma once
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Math/RandomNumberGenerator.hpp"
#include "APF/APF_Lib.hpp"

constexpr float FONT_DEFAULT_ASPECT = .8f;

class App;
class RenderContext;
class InputSystem;
class RandomNumberGenerator;
class AudioSystem;
class BitmapFont;
class UISystem;
class Game;
class APF_Lib::APF;

extern App* g_theApp;
extern Game* g_theGame;
extern RandomNumberGenerator* g_theRNG;
extern AudioSystem* g_theAudio;
extern RenderContext* g_theRenderer;
extern InputSystem* g_theInput;
extern BitmapFont* g_theFont;
extern UISystem* g_theUI;
extern APF_Lib::APF* g_APF;

extern bool g_isDebugDrawing;

//////////////////////////////////////////////////////////////////////////
// Global Settings
constexpr int GAME_SESSION_NUMBER = 30;
constexpr int GAME_ACTION_NUMBER = 5;
constexpr float GAME_LOSE_LIKING = .005f;
constexpr float GAME_WIN_LIKING = .5f;
constexpr float TIME_SLOW_RATE = .3f;
constexpr float TIME_FAST_RATE = 3.f;
constexpr float POSITIVE_WEIGHTS[APF_Lib::NUM_VALENCE_FEATURES] = { .6f,.4f };
constexpr float NEGATIVE_WEIGHTS[APF_Lib::NUM_VALENCE_FEATURES] = { .4f,.6f };


//////////////////////////////////////////////////////////////////////////
// Debug Mode
enum eDebugMode
{
    DEBUG_VERBOSE,
    DEBUG_LIGHT
};

extern eDebugMode g_debugRenderMode;

struct APF_Lib::MentalState;
void LogCurrentMentalToConsole(char const* operation);
void LogMentalToConsole(APF_Lib::MentalState const& ms, char const* operation);
