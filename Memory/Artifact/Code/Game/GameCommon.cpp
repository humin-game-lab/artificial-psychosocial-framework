#include "Game/GameCommon.hpp"
#include "Engine/Input/XboxController.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Core/DevConsole.hpp"

App* g_theApp = nullptr;
RenderContext* g_theRenderer = nullptr;
InputSystem* g_theInput = nullptr;
RandomNumberGenerator* g_theRNG = nullptr;
AudioSystem* g_theAudio = nullptr;
BitmapFont* g_theFont = nullptr;
Game* g_theGame = nullptr;
UISystem* g_theUI = nullptr;
APF_Lib::APF* g_APF = nullptr;

bool g_isDebugDrawing = false;

eDebugMode g_debugRenderMode = eDebugMode::DEBUG_LIGHT;

//////////////////////////////////////////////////////////////////////////
static std::string GetMentalString(APF_Lib::MentalState const& ms, char const* operation)
{
    std::string result(operation);
    result += ": ";
    for (int i = 0; i < 32; i++) {
        result += ToString(ms[i]);
        if(i<31){
            result +=",";
        }
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LogCurrentMentalToConsole(char const* operation)
{
    float ms[32];
    g_APF->GetANPCsMentalState(ms, 0);
    APF_Lib::MentalState msState;
    msState.SetMentalState(ms);
    LogMentalToConsole(msState, operation);
}

//////////////////////////////////////////////////////////////////////////
void LogMentalToConsole(APF_Lib::MentalState const& ms, char const* operation)
{
    std::string str = GetMentalString(ms, operation);
    g_theConsole->PrintString(Rgba8::WHITE, str);
}
