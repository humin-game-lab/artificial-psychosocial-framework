#include "Game/APFUtils.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Engine/Core/XMLUtils.hpp"
#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Math/RandomNumberGenerator.hpp"


//////////////////////////////////////////////////////////////////////////
std::string GetGraphvizVerboseTextForMentalState(APF_Lib::MentalState const& state)
{
    std::string result = "Mental: {";
    for (int i = 0; i < (int)APF_Lib::NUM_MENTAL_FEATURES; i++) {
        result += Stringf("%.1f,",state[i]);
    }
    result+="}\\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
void SetMentalStateRandomly(APF_Lib::MentalState& ms, float base, float variation)
{
    for (int i = 0; i < APF_Lib::NUM_MENTAL_FEATURES; i++) {
        ms[i] = ClampZeroToOne(g_theRNG->RollRandomFloatInRange(base-variation,base+variation));
    }
}

//////////////////////////////////////////////////////////////////////////
// Traits
//////////////////////////////////////////////////////////////////////////
static std::map<std::string, APF_Lib::TraitPersonality> sPersonalities;
static std::map<std::string, APF_Lib::TraitSocialRole> sSocialRoles;

//////////////////////////////////////////////////////////////////////////
void PopulatePersonalitiesFromXML(std::string const& filePath)
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile(filePath.c_str());
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError(Stringf("Failed to load xml %s", filePath.c_str()));
        return;
    }

    XmlElement const* root = xmlDoc.RootElement();
    XmlElement const* personality = root->FirstChildElement("Personality");
    while (personality != nullptr) {
        std::string name = ParseXmlAttribute(*personality, "name", "");
        if (!name.empty()) {
            float o = ParseXmlAttribute(*personality, "O", 0.f);
            float c = ParseXmlAttribute(*personality, "C", 0.f);
            float e = ParseXmlAttribute(*personality, "E", 0.f);
            float a = ParseXmlAttribute(*personality, "A", 0.f);
            float n = ParseXmlAttribute(*personality, "N", 0.f);

            sPersonalities[name] = APF_Lib::TraitPersonality(o,c,e,a,n);
        }

        personality = personality->NextSiblingElement("Personality");
    }
}

//////////////////////////////////////////////////////////////////////////
void PopulateSocialRolesFromXML(std::string const& filePath)
{
    XmlDocument xmlDoc;
    XmlError code = xmlDoc.LoadFile(filePath.c_str());
    if (code != XmlError::XML_SUCCESS) {
        g_theConsole->PrintError(Stringf("Failed to load xml %s", filePath.c_str()));
        return;
    }

    XmlElement const* root = xmlDoc.RootElement();
    XmlElement const* socialRole = root->FirstChildElement("SocialRole");
    while (socialRole != nullptr) {
        std::string name = ParseXmlAttribute(*socialRole, "name", "");
        if (!name.empty()) {
            float l = ParseXmlAttribute(*socialRole, "L", 0.f);
            float d = ParseXmlAttribute(*socialRole, "D", 0.f);
            float s = ParseXmlAttribute(*socialRole, "S", 0.f);
            float f = ParseXmlAttribute(*socialRole, "F", 0.f);

            sSocialRoles[name] = APF_Lib::TraitSocialRole(l,d,s,f);
        }

        socialRole = socialRole->NextSiblingElement("SocialRole");
    }
}

//////////////////////////////////////////////////////////////////////////
bool GetPersonalityOfName(std::string const& name, APF_Lib::TraitPersonality& personality)
{
    std::map<std::string, APF_Lib::TraitPersonality>::const_iterator it = sPersonalities.find(name);

    if (it != sPersonalities.end()) {
        personality = it->second;
        return true;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
void CombineAttitude(APF_Lib::TraitAttitude const& attA, APF_Lib::TraitAttitude const& attB, float alpha, APF_Lib::TraitAttitude& combined)
{
    combined.m_traits[APF_Lib::ATTITUDE_LIKING] = attA.m_traits[APF_Lib::ATTITUDE_LIKING]*alpha+ attB.m_traits[APF_Lib::ATTITUDE_LIKING]*(1.f-alpha);
    combined.m_traits[APF_Lib::ATTITUDE_FAMILIARITY] = attA.m_traits[APF_Lib::ATTITUDE_FAMILIARITY]*alpha+ attB.m_traits[APF_Lib::ATTITUDE_FAMILIARITY]*(1.f-alpha);
}

//////////////////////////////////////////////////////////////////////////
ActionAccessInfo::ActionAccessInfo(std::string const& actionName, int index)
    : name(actionName)
    , idx(index)
{

}
