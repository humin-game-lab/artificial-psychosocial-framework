#pragma once

#include "APF/APF_Lib.hpp"
#include "Engine/Core/EventSystem.hpp"
#include "Engine/Core/XMLUtils.hpp"
#include "Memory/GameActor.hpp"

class RandomNumberGenerator;
class Camera;
class Clock;
class APF_Lib::MM::MemoryClock;
struct APF_Lib::MM::GameEvent;

//////////////////////////////////////////////////////////////////////////
class Game 
{
public:
	Game();
	~Game()=default;
	void StartUp();
	void ShutDown();

	void BeginFrame();
	void Update();
	void Render()const;

	void SpawnActor(XmlElement const* element);
	void SpawnObject(XmlElement const* element);

	APF_Lib::MM::GameActor* GetActorFromName(std::string const& name) const;
	APF_Lib::MM::GameObject* GetObjectFromName(std::string const& name) const;
	APF_Lib::MM::GameObject* GetObjectFromAPFIdx(int apfIdx) const;
	std::vector<APF_Lib::MM::GameObject*> const& GetAllObjects() const {return m_objects;}
	std::vector<APF_Lib::MM::GameObject const*> GetAllObjectsOfTag(std::string const& name) const;

	bool OnNewEventSubmitted(EventArgs& args);

	//Debug
	void RenderActorLTMToDisk(APF_Lib::MM::GameActor const* actor, std::string const& imgFileName) const;
	void RenderLTMImgToScreen(std::string const& imgFileName, APF_Lib::MM::GameActor const* actor) const;

private:
	std::vector<APF_Lib::MM::GameActor*> m_actors;
	std::vector<APF_Lib::MM::GameObject*> m_objects;

	bool m_isLoading = true;
	bool m_startedLoading = false;

	bool m_isEnded = false;
	bool m_isProgressing = false;
	int m_sessionIdx=0;
	int m_actionIdx=0;

	mutable int m_eventSelection=0;
	mutable int m_reinforceSelection=0;

	Clock* m_gameClock = nullptr;
	APF_Lib::MM::MemoryClock* m_worldClock = nullptr;
	Camera* m_worldCamera = nullptr;
	Camera* m_uiCamera = nullptr;
	
private:
	void LoadAndInitialize();
	void InitializeForActors();
	void InitializeForObjects();
	void PostInitializeActorsAndObjects();
	void InitializePraises();	//Initialize praise to action basic effect
	void InitializePossibleEvents();

	void UpdateForInput();

	void ProcessSelection();
	void UpdateForNewEvent(int actionIdx, APF_Lib::MM::GameObject const* object);
	void UpdateForNewEvent(APF_Lib::MM::GameEvent const& event);
	void UpdateForNoRewardEvent();
	void UpdateNewEventForSelfInfo(APF_Lib::MM::GameEvent const& event);
	void UpdateNewEventForOthers();
	void UpdateActorMemories();

	void ProgressToNextSession();
	void StartNewSession();
	void EndGame();
	void LogGameHistory();

	bool IsGlobalTimePaused() const;
	void AdjustTimeScale(bool isDescrease, bool isIncrease, bool togglePause) const;

	void RenderForGame() const;
	void RenderForUI() const;
	void RenderMentalEmoji() const;
	void RenderAttitudeEmoji() const;
	void RenderForGameUI() const;
	void RenderForUIDebug() const;
};