#pragma once

#include <string>

void RenderGraphvizImage(std::string const& dotLanguage, std::string const& outputImg);