#pragma once

#include "Engine/Core/XMLUtils.hpp"
#include "Memory/ObjectState.hpp"
#include "Memory/Memory.hpp"
#include "Memory/GameActor.hpp"

struct APF_Lib::MentalState;
struct ActionAccessInfo;
class APF_Lib::MM::LTMNode;

struct MemoryUtils
{
    static void PopulateAttitudesFromFile(std::string const& filePath);
    static void PopulateActionsFromXML(std::string const& filePath);
    static void PopulateSTMsFromXML(std::string const& filePath);
    static void PopulateStatesFromXML(std::string const& filePath);

    static void UpdateActionIndexes();
    static void UpdateANPCIndexes(std::vector<APF_Lib::MM::GameActor*>& actors);
    static void UpdateObjectIndexes(std::vector<APF_Lib::MM::GameObject*>& objects);

    static void PopulateGameObjectFromXML(APF_Lib::MM::GameObject& gameObject, XmlElement const* objElement);
    static void PopulateObjectStateFromXML(APF_Lib::MM::ObjectState& objState, XmlElement const* stateElement);
    static void PopulateActorFromXML(APF_Lib::MM::GameActor& actor, XmlElement const* actorElement);

    static std::string GetGraphvizDebugText(APF_Lib::MM::GameState const& gameState);
    static std::string GetGraphvizDebugText(APF_Lib::MM::ObjectState const& objState);
    static std::string GetGraphvizDebugText(APF_Lib::MM::GameEvent const& event);
    static std::string GetGraphvizDebugText(APF_Lib::MM::ActorState const& actorState);
    static std::string GetGraphvizDebugText(APF_Lib::MM::Memory const& memory);
    static std::string GetGraphvizDebugText(APF_Lib::MM::MemoryTrace const& trace);
    static std::string GetGraphvizDebugText(APF_Lib::MM::LTMEdge const& edge);
    static std::string GetGraphvizDebugText(APF_Lib::MM::LTMNode const& node);

    static std::string GetDebugText(APF_Lib::MM::GameObject const& object);
    static std::string GetDebugText(APF_Lib::MentalState const& mental);

    static std::vector<ActionAccessInfo> GetAllActionsOfTag(std::string const& tag);
    static std::vector<ActionAccessInfo> GetAllActionsWithoutTags(std::vector<std::string> const& tags);

    static void PolarCombineEmotionDistantNodes(APF_Lib::MM::LTMNode* nodeA, APF_Lib::MM::LTMNode* nodeB);
    static void NeutralCombineEmotionDistantNodes(APF_Lib::MM::LTMNode* nodeA, APF_Lib::MM::LTMNode* nodeB);

    static float GetShortSightMulplierForProspectGeneration(size_t idx, size_t totalNumber);
    static void GetTraceMentalValence(APF_Lib::MM::LTMConstNodeList const& trace, APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float(&valences)[APF_Lib::NUM_VALENCE_FEATURES]);
    static std::vector<APF_Lib::MM::LTMConstNodeList> RankTracesByMentalValance(std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float const (&weights)[2]);
    static void GenerateMentalSalientProspectMemoryFromTraces(APF_Lib::MM::Memory& result, std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, APF_Lib::MentalState const& mental, APF_Lib::MM::GetProspectMultiplierFunction funcPtr);

    static void InitializeActor(APF_Lib::MM::GameActor& actor);
};

