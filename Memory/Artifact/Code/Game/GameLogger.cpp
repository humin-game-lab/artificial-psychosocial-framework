#include "Game/GameLogger.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/DevConsole.hpp"
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////
void GameLogger::LogError(char const* format, ...)
{
    constexpr int STRING_STACK_LENGTH = 2048;
    char textLiteral[STRING_STACK_LENGTH];
    va_list variableArgumentList;
    va_start(variableArgumentList, format);
    vsnprintf_s(textLiteral, STRING_STACK_LENGTH, _TRUNCATE, format, variableArgumentList);
    va_end(variableArgumentList);
    textLiteral[STRING_STACK_LENGTH - 1] = '\0';
    std::string content(textLiteral);
    g_theConsole->PrintError(content);
}

//////////////////////////////////////////////////////////////////////////
void GameLogger::LogInfo(std::string const& info)
{
    g_theConsole->PrintString(Rgba8::WHITE, info);
}
