#pragma once

#include <map>
#include "Memory/GameTags.hpp"
#include "APF/Trait.hpp"
#include "APF/MentalState.hpp"


//////////////////////////////////////////////////////////////////////////
//vocabulary
struct ActionAccessInfo
{
public:
    std::string name;
    int idx=-1;

    ActionAccessInfo(std::string const& actionName, int index);
};


//////////////////////////////////////////////////////////////////////////
// Traits

void PopulatePersonalitiesFromXML(std::string const& filePath);
void PopulateSocialRolesFromXML(std::string const& filePath);

bool GetPersonalityOfName(std::string const& name, APF_Lib::TraitPersonality& personality);

void CombineAttitude(APF_Lib::TraitAttitude const& attA, APF_Lib::TraitAttitude const& attB, float alpha, APF_Lib::TraitAttitude& combined);


//////////////////////////////////////////////////////////////////////////
// Mental States

std::string GetGraphvizVerboseTextForMentalState(APF_Lib::MentalState const& state);

void SetMentalStateRandomly(APF_Lib::MentalState& ms,float base, float variation);
