#include "Relations.hpp"

APF_Lib::Relation::Relation(const char* from, const char* to) :
	m_from(from), m_to(to)
{
}


APF_Lib::Relation::Relation(const std::string& from, const std::string& to) :
	m_from(from), m_to(to)
{
}


APF_Lib::Relation& APF_Lib::Relation::operator=(const Relation& copy)
{
	m_from = copy.m_from;
	m_to = copy.m_to;

	return *this;
}


std::string APF_Lib::Relation::ToString()
{
	return std::string("{From:'" + m_from + "', To:'" + m_to + "'}");
}


APF_Lib::Attitude::Attitude(const char* anpc_name, const char* entity_name, const float liking, const float familiarity):
	Relation(anpc_name, entity_name), m_attitude{liking, familiarity}
{
}


APF_Lib::Attitude::Attitude(const std::string& anpc_name, const std::string& entity_name, const float liking, const float familiarity):
	Relation(anpc_name, entity_name), m_attitude{ liking, familiarity }
{
}


std::string APF_Lib::Attitude::ToString()
{
	return std::string("{From:'" + m_from + "', To:'" + m_to + "' , ("
		+ std::to_string(m_attitude[0]) + ", "
		+ std::to_string(m_attitude[1]) + ")}");
}


APF_Lib::Praise::Praise(const char* anpc_name, const char* action_name, const float valence):
	Relation(anpc_name, action_name), m_valence(valence)
{
}


APF_Lib::Praise::Praise(const std::string& anpc_name, const std::string& action_name, const float valence) :
	Relation(anpc_name, action_name), m_valence(valence)

{
}


std::string APF_Lib::Praise::ToString()
{
	return std::string("{From:'" + m_from + "', To:'" + m_to + "' , " + std::to_string(m_valence) + "}");
}


APF_Lib::Social::Social(const char* anpc_name, const char* action_name, const float l, const float d, const float s,
                        const float f): Relation(anpc_name, action_name), m_socialRole(l, d, s, f)
{
}

APF_Lib::Social::Social(const std::string& anpc_name, const std::string& action_name, const float l, const float d,
	const float s, const float f): Relation(anpc_name, action_name), m_socialRole(l, d, s, f)
{
}

std::string APF_Lib::Social::ToString()
{
	return std::string("{From:'" + m_from + "', To:'" + m_to + "' , (" + 
		std::to_string(m_socialRole[SOCIAL_ROLE_LIKING]) + ", " +
		std::to_string(m_socialRole[SOCIAL_ROLE_DOMINANCE]) + ", " +
		std::to_string(m_socialRole[SOCIAL_ROLE_SOLIDARITY]) + ", " +
		std::to_string(m_socialRole[SOCIAL_ROLE_FAMILIARITY]) + ")}");
}
