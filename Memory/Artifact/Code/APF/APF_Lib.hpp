#pragma once
#include <vector>
#include <queue>

#include "Event.hpp"
#include "MentalState.hpp"
#include "Vocabulary.hpp"
#include "Relations.hpp"


namespace APF_Lib
{
	class APF
	{
		
	public:		
		APF();
		~APF();

		//Vocabulary 
		void InitializeVocabulary();
		void TerminateVocabulary();
		void SortVocabulary();
		void PrintAllVocabulary();
		
		void AddANPC(const char* name, float o, float c, float e, float a, float n);
		void AddObject(const char* name);
		void AddAction(const char* name, float effect);

		int GetANPCIndex(const char* name) const;
		int GetObjectIndex(const char* name) const;
		int GetActionIndex(const char* name) const;

		ANPC& GetANPC(const int indx);
		Object& GetObject(const int indx);
		Action& GetAction(const int indx);

		//Relations
		void InitializeRelations();
		void TerminateRelations();
		void PrintAllRelations();
		
		void AddAttitude(const char* anpc_name, const char* object_name, const float liking, const float familiarity);
		void AddPraise(const char* anpc_name, const char* action_name, const float valence);
 		void AddSocial(const char* from_anpc_name, const char* to_anpc_name,
 			float liking, float dominance, float solidarity, float familiarity);
		void AddAttitude(const int anpc_idx, const int object_idx, const float liking, const float familiarity);
		void AddPraise(const int anpc_idx, const int action_idx, const float valence);
		void AddSocial(const int from_anpc_idx, const int to_anpc_idx,
			float liking, float dominance, float solidarity, float familiarity);

		TraitAttitude& GetAttitude(const char* anpc_name, const char* object_name);
		TraitAttitude& GetAttitude(const int  anpc_idx, const int object_idx);

		float GetPraise(const char* anpc_name, const char* action_name);
		float GetPraise(const int anpc_idx, const int action_idx);

		TraitSocialRole& GetSocial(const char* from_anpc_name, const char* to_anpc_name);
		TraitSocialRole& GetSocial(const int from_anpc_idx, const int to_anpc_idx);


		//Emotion Generation
		void InitializeANPCsMentalStates();
		void TerminateANPCsMentalStates();
		void PrintAllMentalStates();

		void SetANPCsMentalState(const char* anpc_name, const float(&new_mental_state)[NUM_MENTAL_FEATURES]);
		void SetANPCsValence(const char* anpc_name, const float(&new_valence_state)[NUM_VALENCE_FEATURES]);
		void SetANPCsMood(const char* anpc_name, const float(&new_mood_state)[NUM_MOOD_FEATURES]);
		void SetANPCsEmotion(const char* anpc_name, const float(&new_emotional_state)[NUM_EMOTION_FEATURES]);
		void SetANPCsFeeling(const char* anpc_name, const float(&new_feeling_state)[NUM_FEELING_FEATURES]);

		void SetANPCsMentalState(const int anpc_idx, const MentalState& new_mental_state);
		void SetANPCsMentalState(const int anpc_idx, const float(&new_mental_state)[NUM_MENTAL_FEATURES]);
		void SetANPCsValence(const int anpc_idx, const float(&new_valence_state)[NUM_VALENCE_FEATURES]);
		void SetANPCsMood(const int anpc_idx, const float(&new_mood_state)[NUM_MOOD_FEATURES]);
		void SetANPCsEmotion(const int anpc_idx, const float(&new_emotional_state)[NUM_EMOTION_FEATURES]);
		void SetANPCsFeeling(const int anpc_idx, const float(&new_feeling_state)[NUM_FEELING_FEATURES]);

		void GetANPCsMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const char* anpc_name) const;
		void GetANPCsValence(float(&out_valence_state)[NUM_VALENCE_FEATURES], const char* anpc_name) const;
		void GetANPCsMood(float(&out_mood_state)[NUM_MOOD_FEATURES], const char* anpc_name) const;
		void GetANPCsEmotion(float(&out_emotional_state)[NUM_EMOTION_FEATURES], const char* anpc_name) const;
		void GetANPCsFeelings(float(&out_feeling_state)[NUM_FEELING_FEATURES], const char* anpc_name) const;

		void GetANPCsMentalState(MentalState& out_mental_state, const int anpc_idx) const;
		void GetANPCsMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const int anpc_idx) const;
		void GetANPCsValence(float(&out_valence_state)[NUM_VALENCE_FEATURES], const int anpc_idx) const;
		void GetANPCsMood(float(&out_mood_state)[NUM_MOOD_FEATURES], const int anpc_idx) const;
		void GetANPCsEmotion(float(&out_emotional_state)[NUM_EMOTION_FEATURES], const int anpc_idx) const;
		void GetANPCsFeelings(float(&out_feeling_state)[NUM_FEELING_FEATURES], const int anpc_idx) const;

		void DegradeMentalStates(const float valence = DEGRADE_VALENCE, const float mood = DEGRADE_MOOD, const float emotion = DEGRADE_EMOTION, const float feelings = DEGRADE_FEELINGS);
		
 		//Event Handling
		void InitializeEventHandler();
		void TerminateEventHandler();

		void AddEventWithObject(const char* anpc_owner_name, const char* anpc_actor_name, const char* action_name, const char* patient_object_name, const float certainty);
		void AddEventWithANPC(const char* anpc_owner_name, const char* anpc_actor_name, const char* action_name, const char* patient_anpc_name, const float certainty);

		void AddEventWithObject(const int anpc_owner_idx, const int anpc_actor_idx, const int action_idx, const int patient_object_idx, const float certainty);
		void AddEventWithANPC(const int anpc_owner_idx, const int anpc_actor_idx, const int action_idx, const int patient_anpc_idx, const float certainty);

		void ProcessEvents();
		
		
		//TODO: Cmdlet Functions 
		// Create CSV file for all ATTITUDES
		// Create CSV file for all PRAISES
		// Create CSV file for all SOCIALS

		//TODO: read in Traits
		// Add a Personality Trait
		// Add a Social Relation Trait
		// Read a Trait name, and substitute for random offset of the same trait

		// Manually overriting
		void OverwriteAttitude(const int  anpc_idx, const int object_idx, const float l, const float f);
		void OverwritePraise(const int anpc_idx, const int action_idx, const float p);
		void OverwriteSocialRelation(const int from_anpc_idx, const int to_anpc_idx, const float l, const float d, const float s, const float f);
		

	private:

		//TODO: Emotion Generation
		void GenerateMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const Event& event);

		//TODO: Social Relations Update
		void UpdateSocailRelation(const int anpc_owner_idx, const int anpc_actor_idx, const float(&new_mental_state)[NUM_MENTAL_FEATURES]);
		
		//Debuging
		void SortAnpcList();
		void SortObjectList();
		void SortActionList();

		void PrintAllAnpcs();
		void PrintAllObjects();
		void PrintAllActions();

		void PrintAllAttitudes();
		void PrintAllPraises();
		void PrintAllSocials();

		int BinarySearchAnpcs(const char* name, int l, int r) const;
		int BinarySearchObjects(const char* name, int l, int r) const;
		int BinarySearchActions(const char* name, int l, int r) const;

		
		
	private:
		std::vector<ANPC> m_anpcs;
		std::vector<Object> m_objects;
		std::vector<Action> m_actions;
//		std::vector<Adjective> m_adjectives;

		std::vector<Attitude> m_attitudes;
		std::vector<Praise> m_praises;
		std::vector<Social> m_socials;

		std::vector<MentalState> m_anpcsMentalState;
		
		std::vector<Event> m_events;
	};


}
