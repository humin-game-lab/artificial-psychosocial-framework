#include "APF_Lib.hpp"

APF_Lib::APF::APF()
{
}


APF_Lib::APF::~APF()
{
}


void APF_Lib::APF::InitializeVocabulary()
{
	m_anpcs = std::vector<APF_Lib::ANPC>();
	m_objects = std::vector<APF_Lib::Object>();
	m_actions = std::vector<APF_Lib::Action>();
}


void APF_Lib::APF::TerminateVocabulary()
{
	m_anpcs.clear();
	m_anpcs.shrink_to_fit();

	m_objects.clear();
	m_objects.shrink_to_fit();

	m_actions.clear();
	m_actions.shrink_to_fit();
}


void APF_Lib::APF::AddANPC(const char* name, const float o, const float c, const float e, 
                           const float a, const float n)
{
	const float openness = ClampFloat(o, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float conscientiousness = ClampFloat(c, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float extraversion = ClampFloat(e, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float agreeableness = ClampFloat(a, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float neuroticism = ClampFloat(n, MIN_UNIT_VALUE, MAX_UNIT_VALUE);

	m_anpcs.push_back(APF_Lib::ANPC(name, openness, conscientiousness, extraversion, agreeableness, neuroticism));

	//Debuging
	//printf("ANPC: %s added to APF \n", m_anpcs.back().ToString().c_str());
}


void APF_Lib::APF::AddObject(const char* name)
{
	m_objects.push_back(APF_Lib::Object(name));

	//Debuging
	//printf("Object: %s added to APF \n", m_objects.back().ToString().c_str());
}


void APF_Lib::APF::AddAction(const char* name, const float effect)
{
	const float bound_effect = ClampFloat(effect, MIN_UNIT_VALUE, MAX_UNIT_VALUE);

	m_actions.push_back(APF_Lib::Action(name, bound_effect));

	//Debuging
	//printf("Action: %s added to APF \n", m_actions.back().ToString().c_str());
}


//This function expects m_anpcs to be sorted
int APF_Lib::APF::GetANPCIndex(const char* name) const
{
	return BinarySearchAnpcs(name, 0, static_cast<int>(m_anpcs.size()) - 1);
}


//This function expects m_objects to be sorted
int APF_Lib::APF::GetObjectIndex(const char* name) const
{
	return BinarySearchObjects(name, 0, static_cast<int>(m_objects.size()) - 1);
}


//This function expects m_actions to be sorted
int APF_Lib::APF::GetActionIndex(const char* name) const
{
	return BinarySearchActions(name, 0, static_cast<int>(m_actions.size()) - 1);
}


APF_Lib::ANPC& APF_Lib::APF::GetANPC(const int indx)
{
	return m_anpcs[indx];
}


APF_Lib::Object& APF_Lib::APF::GetObject(const int indx)
{
	return m_objects[indx];
}


APF_Lib::Action& APF_Lib::APF::GetAction(const int indx)
{
	return m_actions[indx];
}


void APF_Lib::APF::InitializeRelations()
{
	// attitudes = ANPC X Objects
	m_attitudes.resize(m_anpcs.size() * m_objects.size(), Attitude("none", "none", -1.0f, -1.0f));

	// praises = ANPC X actions
	m_praises.resize(m_anpcs.size() * m_actions.size(), Praise("none", "none", -1.0f));

	// social = ANPC X ANPC
	m_socials.resize(m_anpcs.size() * m_anpcs.size(), Social("none", "none", 0.0f, 0.0f, 0.0f, 0.0f));
}


void APF_Lib::APF::TerminateRelations()
{
	m_attitudes.clear();
	m_attitudes.shrink_to_fit();

	m_praises.clear();
	m_praises.shrink_to_fit();

	m_socials.clear();
	m_socials.shrink_to_fit();
}


void APF_Lib::APF::PrintAllRelations()
{
	PrintAllAttitudes();
	PrintAllPraises();
	PrintAllSocials();
}


void APF_Lib::APF::AddAttitude(const char* anpc_name, const char* object_name, const float liking, const float familiarity)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(anpc_name);
	const int y = GetObjectIndex(object_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int attitude_idx = x + width * y;
	const float liking_bound = ClampFloat(liking, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float familiarity_bound = ClampFloat(familiarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_attitudes[attitude_idx] = Attitude(anpc_name, object_name, liking_bound, familiarity_bound);
}


void APF_Lib::APF::AddPraise(const char* anpc_name, const char* action_name, const float valence)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(anpc_name);
	const int y = GetActionIndex(action_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int praise_idx = x + width * y;
	const float valence_bound = ClampFloat(valence, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_praises[praise_idx] = Praise(anpc_name, action_name, valence_bound);
}


void APF_Lib::APF::AddSocial(const char* from_anpc_name, const char* to_anpc_name, float liking, float dominance,
	float solidarity, float familiarity)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(from_anpc_name);
	const int y = GetANPCIndex(to_anpc_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int anpc_idx = x + width * y;
	const float l = ClampFloat(liking, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float d = ClampFloat(dominance, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float s = ClampFloat(solidarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float f = ClampFloat(familiarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_socials[anpc_idx] = Social(from_anpc_name, to_anpc_name, l, d, s, f);
}


void APF_Lib::APF::AddAttitude(const int anpc_idx, const int object_idx, const float liking, const float familiarity)
{
	const std::string anpc_name = m_anpcs[anpc_idx].m_name;
	const std::string object_name = m_objects[object_idx].m_name;
	
	const int width = static_cast<int>(m_anpcs.size());
	const int attitude_idx = anpc_idx + width * object_idx;
	const float liking_bound = ClampFloat(liking, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float familiarity_bound = ClampFloat(familiarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_attitudes[attitude_idx] = Attitude(anpc_name, object_name, liking_bound, familiarity_bound);
}


void APF_Lib::APF::AddPraise(const int anpc_idx, const int action_idx, const float valence)
{
	const std::string anpc_name = m_anpcs[anpc_idx].m_name;
	const std::string action_name = m_actions[action_idx].m_name;

	const int width = static_cast<int>(m_anpcs.size());
	const int praise_idx = anpc_idx + width * action_idx;
	const float valence_bound = ClampFloat(valence, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_praises[praise_idx] = Praise(anpc_name, action_name, valence_bound);
}


void APF_Lib::APF::AddSocial(const int from_anpc_idx, const int to_anpc_idx, const float liking, const float dominance,
	const float solidarity, const float familiarity)
{
	const std::string from_anpc_name = m_anpcs[from_anpc_idx].m_name;
	const std::string to_anpc_name = m_anpcs[to_anpc_idx].m_name;
	
	const int width = static_cast<int>(m_anpcs.size());
	const int anpc_idx = from_anpc_idx + width * to_anpc_idx;
	const float l = ClampFloat(liking, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float d = ClampFloat(dominance, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float s = ClampFloat(solidarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	const float f = ClampFloat(familiarity, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_socials[anpc_idx] = Social(from_anpc_name, to_anpc_name, l, d, s, f);
}


APF_Lib::TraitAttitude& APF_Lib::APF::GetAttitude(const char* anpc_name, const char* object_name)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(anpc_name);
	const int y = GetObjectIndex(object_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int attitude_idx = x + width * y;
	return m_attitudes[attitude_idx].m_attitude;
}


APF_Lib::TraitAttitude&  APF_Lib::APF::GetAttitude(const int anpc_idx, const int object_idx)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int attitude_idx = anpc_idx + width * object_idx;
	return m_attitudes[attitude_idx].m_attitude;
}


float APF_Lib::APF::GetPraise(const char* anpc_name, const char* action_name)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(anpc_name);
	const int y = GetActionIndex(action_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int praise_idx = x + width * y;
	return m_praises[praise_idx].m_valence;
}


float APF_Lib::APF::GetPraise(const int anpc_idx, const int action_idx)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int praise_idx = anpc_idx + width * action_idx;
	return m_praises[praise_idx].m_valence;
}


APF_Lib::TraitSocialRole& APF_Lib::APF::GetSocial(const char* from_anpc_name, const char* to_anpc_name)
{
	// treating 1d vector as a 2d vector
	const int x = GetANPCIndex(from_anpc_name);
	const int y = GetANPCIndex(to_anpc_name);
	const int width = static_cast<int>(m_anpcs.size());

	const int anpc_idx = x + width * y;
	return m_socials[anpc_idx].m_socialRole;
	
}


APF_Lib::TraitSocialRole& APF_Lib::APF::GetSocial(const int from_anpc_idx, const int to_anpc_idx)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int anpc_idx = from_anpc_idx + width * to_anpc_idx;
	return m_socials[anpc_idx].m_socialRole;
}


void APF_Lib::APF::InitializeANPCsMentalStates()
{
	m_anpcsMentalState.resize(m_anpcs.size(), MentalState());
}


void APF_Lib::APF::TerminateANPCsMentalStates()
{
	m_anpcsMentalState.clear();
	m_anpcsMentalState.shrink_to_fit();
}


void APF_Lib::APF::PrintAllMentalStates()
{
	printf("Printing entire ANPC Mental State list: \n");
	const int num_mental_states = static_cast<int>(m_anpcsMentalState.size());

	for (int state_idx = 0; state_idx < num_mental_states; ++state_idx)
	{
		printf("%d: %s \n",
			state_idx,
			m_anpcsMentalState[state_idx].ToString().c_str());

	}
}


void APF_Lib::APF::SetANPCsMentalState(const char* anpc_name, const float(& new_mental_state)[NUM_MENTAL_FEATURES])
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	SetANPCsMentalState(anpc_idx, new_mental_state);
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::APF::SetANPCsMentalState(const int anpc_idx, const MentalState& new_mental_state)
{
	m_anpcsMentalState[anpc_idx].SetMentalState(new_mental_state);
}

void APF_Lib::APF::SetANPCsValence(const char* anpc_name, const float(& new_valence_state)[NUM_VALENCE_FEATURES])
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	SetANPCsValence(anpc_idx, new_valence_state);
}


void APF_Lib::APF::SetANPCsMood(const char* anpc_name, const float(& new_mood_state)[NUM_MOOD_FEATURES])
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	SetANPCsMood(anpc_idx, new_mood_state);
}


void APF_Lib::APF::SetANPCsEmotion(const char* anpc_name, const float(& new_emotional_state)[NUM_EMOTION_FEATURES])
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	SetANPCsEmotion(anpc_idx, new_emotional_state);
}


void APF_Lib::APF::SetANPCsFeeling(const char* anpc_name, const float(& new_feeling_state)[NUM_FEELING_FEATURES])
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	SetANPCsFeeling(anpc_idx, new_feeling_state);
}


void APF_Lib::APF::SetANPCsMentalState(const int anpc_idx, const float(& new_mental_state)[NUM_MENTAL_FEATURES])
{
	m_anpcsMentalState[anpc_idx].SetMentalState(new_mental_state);
}


void APF_Lib::APF::SetANPCsValence(const int anpc_idx, const float(& new_valence_state)[NUM_VALENCE_FEATURES])
{
	m_anpcsMentalState[anpc_idx].SetValence(new_valence_state);
}


void APF_Lib::APF::SetANPCsMood(const int anpc_idx, const float(& new_mood_state)[NUM_MOOD_FEATURES])
{
	m_anpcsMentalState[anpc_idx].SetMood(new_mood_state);
}


void APF_Lib::APF::SetANPCsEmotion(const int anpc_idx, const float(& new_emotional_state)[NUM_EMOTION_FEATURES])
{
	m_anpcsMentalState[anpc_idx].SetEmotion(new_emotional_state);
}


void APF_Lib::APF::SetANPCsFeeling(const int anpc_idx, const float(& new_feeling_state)[NUM_FEELING_FEATURES])
{
	m_anpcsMentalState[anpc_idx].SetFeeling(new_feeling_state);
}


void APF_Lib::APF::GetANPCsMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const char* anpc_name) const
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	GetANPCsMentalState(out_mental_state, anpc_idx);
}


void APF_Lib::APF::GetANPCsMentalState(MentalState& out_mental_state, const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetMentalState(out_mental_state);
}

void APF_Lib::APF::GetANPCsValence(float(&out_valence_state)[NUM_VALENCE_FEATURES], const char* anpc_name) const
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	GetANPCsValence(out_valence_state, anpc_idx);
}


void APF_Lib::APF::GetANPCsMood(float(&out_mood_state)[NUM_MOOD_FEATURES], const char* anpc_name) const
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	GetANPCsMood(out_mood_state, anpc_idx);
}


void APF_Lib::APF::GetANPCsEmotion(float(&out_emotional_state)[NUM_EMOTION_FEATURES], const char* anpc_name) const
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	GetANPCsEmotion(out_emotional_state, anpc_idx);
}


void APF_Lib::APF::GetANPCsFeelings(float(&out_feeling_state)[NUM_FEELING_FEATURES], const char* anpc_name) const
{
	const int anpc_idx = GetANPCIndex(anpc_name);

	GetANPCsFeelings(out_feeling_state, anpc_idx);
}


void APF_Lib::APF::GetANPCsMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetMentalState(out_mental_state);
}


void APF_Lib::APF::GetANPCsValence(float(&out_valence_state)[NUM_VALENCE_FEATURES], const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetValence(out_valence_state);
}


void APF_Lib::APF::GetANPCsMood(float(& out_mood_state)[NUM_MOOD_FEATURES], const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetMood(out_mood_state);
}


void APF_Lib::APF::GetANPCsEmotion(float(& out_emotional_state)[NUM_EMOTION_FEATURES], const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetEmotion(out_emotional_state);
}


void APF_Lib::APF::GetANPCsFeelings(float(& out_feeling_state)[NUM_FEELING_FEATURES], const int anpc_idx) const
{
	m_anpcsMentalState[anpc_idx].GetFeeling(out_feeling_state);
}


void APF_Lib::APF::DegradeMentalStates(const float valence, const float mood, const float emotion, const float feelings)
{
	float degraded_mental_state[32] = 
	{
		valence,
		valence,

		mood,
		mood,
		mood,
		mood,
		mood,
		mood,

		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,
		emotion,

		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings,
		feelings
	};
	
	const int num_anpc = static_cast<int>(m_anpcsMentalState.size());

	for(int anpc_idx = 0; anpc_idx < num_anpc; ++anpc_idx)
	{
		m_anpcsMentalState[anpc_idx] -= degraded_mental_state;
	}
}

void APF_Lib::APF::InitializeEventHandler()
{
	m_events = std::vector<APF_Lib::Event>();
}

void APF_Lib::APF::TerminateEventHandler()
{
	m_events.clear();
	m_events.shrink_to_fit();
}

void APF_Lib::APF::AddEventWithObject(const char* anpc_owner_name, const char* anpc_actor_name, const char* action_name,
	const char* patient_object_name, const float certainty)
{
	const int owner_idx = GetANPCIndex(anpc_owner_name);
	const int actor_idx = GetANPCIndex(anpc_actor_name);
	const int action_idx = GetActionIndex(action_name);
	const int object_idx = GetObjectIndex(patient_object_name);
	const float certainty_bound = ClampFloat(certainty, MIN_UNIT_VALUE, MAX_UNIT_VALUE);

	AddEventWithObject(owner_idx, actor_idx, action_idx, object_idx, certainty_bound);
}


void APF_Lib::APF::AddEventWithANPC(const char* anpc_owner_name, const char* anpc_actor_name, const char* action_name,
	const char* patient_anpc_name, const float certainty)
{
	const int owner_idx = GetANPCIndex(anpc_owner_name);
	const int actor_idx = GetANPCIndex(anpc_actor_name);
	const int action_idx = GetActionIndex(action_name);
	const int anpc_idx = GetObjectIndex(patient_anpc_name);
	const float certainty_bound = ClampFloat(certainty, MIN_UNIT_VALUE, MAX_UNIT_VALUE);

	AddEventWithANPC(owner_idx, actor_idx, action_idx, anpc_idx, certainty_bound);
}


void APF_Lib::APF::AddEventWithObject(const int anpc_owner_idx, const int anpc_actor_idx, const int action_idx,
	const int patient_object_idx, const float certainty)
{
	const float certainty_bound = ClampFloat(certainty, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_events.push_back(Event(anpc_owner_idx, anpc_actor_idx, action_idx, patient_object_idx, PATIENT_OBJECT, certainty_bound));
}


void APF_Lib::APF::AddEventWithANPC(const int anpc_owner_idx, const int anpc_actor_idx, const int action_idx,
	const int patient_anpc_idx, float certainty)
{
	const float certainty_bound = ClampFloat(certainty, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	m_events.push_back(Event(anpc_owner_idx, anpc_actor_idx, action_idx, patient_anpc_idx, PATIENT_ANPC, certainty_bound));
}

void APF_Lib::APF::ProcessEvents()
{
	const int num_events = static_cast<int>(m_events.size());

	for(int event_idx = 0; event_idx < num_events; ++event_idx)
	{
		float new_mental_state[NUM_MENTAL_FEATURES] = {MIN_UNIT_VALUE};
		GenerateMentalState(new_mental_state, m_events[event_idx]);

		m_anpcsMentalState[m_events[event_idx].m_owner] += new_mental_state;
		
		UpdateSocailRelation(m_events[event_idx].m_owner, m_events[event_idx].m_actor, new_mental_state);
	}

	TerminateEventHandler();
}


void APF_Lib::APF::OverwriteAttitude(const int anpc_idx, const int object_idx, const float l, const float f)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int attitude_idx = anpc_idx + width * object_idx;
	m_attitudes[attitude_idx].m_attitude[ATTITUDE_LIKING] = l;
	m_attitudes[attitude_idx].m_attitude[ATTITUDE_FAMILIARITY] = f;
}


void APF_Lib::APF::OverwritePraise(const int anpc_idx, const int action_idx, const float p)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int praise_idx = anpc_idx + width * action_idx;
	m_praises[praise_idx].m_valence = p;
}


void APF_Lib::APF::OverwriteSocialRelation(const int from_anpc_idx, const int to_anpc_idx, 
	const float l, const float d, const float s, const float f)
{
	const int width = static_cast<int>(m_anpcs.size());
	const int anpc_idx = from_anpc_idx + width * to_anpc_idx;
	m_socials[anpc_idx].m_socialRole.m_traits[SOCIAL_ROLE_LIKING] = l;
	m_socials[anpc_idx].m_socialRole.m_traits[SOCIAL_ROLE_DOMINANCE] = d;
	m_socials[anpc_idx].m_socialRole.m_traits[SOCIAL_ROLE_SOLIDARITY] = s;
	m_socials[anpc_idx].m_socialRole.m_traits[SOCIAL_ROLE_FAMILIARITY] = f;
}


void APF_Lib::APF::SortVocabulary()
{
	SortAnpcList();
	SortObjectList();
	SortActionList();
}


void APF_Lib::APF::PrintAllVocabulary()
{
	PrintAllObjects();
	PrintAllActions();
	PrintAllAnpcs();
}


void APF_Lib::APF::GenerateMentalState(float(&out_mental_state)[NUM_MENTAL_FEATURES], const Event& event)
{
	ANPC owner = GetANPC(event.m_owner);
	ANPC actor = GetANPC(event.m_actor);
	Action action = GetAction(event.m_action);
	std::string patient;
	float certainty = event.m_certainty;

	float owner_action = GetPraise(event.m_owner, event.m_action);
	float owner_patient = 0.0f;


	switch (event.m_patientType)
	{
		case PATIENT_OBJECT:
		{
//			// Does not make sense, because the owner is an ANPC, not an Object, and these are in different parallel arrays
// 			if (event.m_owner == event.m_patient)
// 			{
// 				break;
// 			}
				
			owner_patient = GetAttitude(event.m_owner, event.m_patient)[ATTITUDE_LIKING];
			break;
		}
		case PATIENT_ANPC:
		{
			if(event.m_owner == event.m_patient)
			{
				break;
			}
				
			owner_patient = GetSocial(event.m_owner, event.m_patient)[SOCIAL_ROLE_LIKING];
			break;
		}
		default:
		{
			return; // cannot generate an emotion without a patient
		}
	}


	
	//Calculating Valiance
	int num_pos_val = 0;
	int num_neg_val = 0;
	float pos_val = 0.f;
	float neg_val = 0.f;

	if (event.m_owner != event.m_actor)
	{
		TraitSocialRole owner_actor = GetSocial(event.m_owner, event.m_actor);

		if (owner_actor[SOCIAL_ROLE_LIKING] > 0.5f)
		{
			++num_pos_val;
			const float value = RangeMapLinearFloat(owner_actor[SOCIAL_ROLE_LIKING], 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
			pos_val += value;
		}
		else
		{
			++num_neg_val;
			const float value = RangeMapLinearFloat(owner_actor[SOCIAL_ROLE_LIKING], 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
			neg_val += value;
		}
	}

	if (owner_action > 0.5f)
	{
		++num_pos_val;
		const float value = RangeMapLinearFloat(owner_action, 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
		pos_val += value;
	}
	else
	{
		++num_neg_val;
		const float value = RangeMapLinearFloat(owner_action, 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
		neg_val += value;
	}

	switch (event.m_patientType)
	{
		case PATIENT_OBJECT:
		{
			if (owner_patient > 0.5f)
			{
				++num_pos_val;
				const float value = RangeMapLinearFloat(owner_patient, 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
				pos_val += value;
			}
			else
			{
				++num_neg_val;
				const float value = RangeMapLinearFloat(owner_patient, 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
				neg_val += value;
			}
			break;
		}
		case PATIENT_ANPC: //TODO: need to look over this again, calculating the "self or other ANPC"
		{
			if (event.m_owner != event.m_patient)
			{
				if (owner_patient > 0.5f)
				{
					++num_pos_val;
					const float value = RangeMapLinearFloat(owner_patient, 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
					pos_val += value;
				}
				else
				{
					++num_neg_val;
					const float value = RangeMapLinearFloat(owner_patient, 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
					neg_val += value;
				}
			}

			break;
		}
	}

	
	if(num_pos_val != 0)
	{
		pos_val /= static_cast<float>(num_pos_val);
		out_mental_state[MENTAL_FEATURE_POSITIVE] = SmoothStart3(pos_val);
	}

	
	if(num_neg_val != 0)
	{
		neg_val /= static_cast<float>(num_neg_val);
		out_mental_state[MENTAL_FEATURE_NEGATIVE] = SmoothStart3(neg_val);
	}

	
	//Calculate Mood
	///TODO: Consequence of the Event
	///requires to look at all possible results, and the actual result
	///
	out_mental_state[MENTAL_FEATURE_PLEASED] = SmoothStart2(pos_val);
	out_mental_state[MENTAL_FEATURE_DISPLEASED] = SmoothStart2(neg_val);

	///Aspect Of Object
	///If someone we love gets hit, we like it... this does not make sense
	///See if we can use adjectives instead
	if(owner_patient > 0.5f)
	{
		float value = RangeMapLinearFloat(owner_patient, 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);

		value = value * pos_val;
		out_mental_state[MENTAL_FEATURE_LIKING] = value;
	}
	else
	{
		float value = RangeMapLinearFloat(owner_patient, 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);

		value = value * neg_val;
		out_mental_state[MENTAL_FEATURE_DISLIKING] = value;
	}

	
	///Action of ANPC
	if (owner_action > 0.5f)
	{
		float value = RangeMapLinearFloat(owner_action, 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
		value = value * pos_val;
		out_mental_state[MENTAL_FEATURE_APPROVING] = value;
	}
	else
	{
		float value = RangeMapLinearFloat(owner_action, 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
		value = value * neg_val;
		out_mental_state[MENTAL_FEATURE_DISAPPOINTMENT] = value;
	}



	
	//Calculate Emotion
	if(certainty < MIN_CERTAINTY)
	{
		///Possible Consequence
		///requires to look at top possible reactions
		float personality_bias = owner.m_personality[PERSONALITY_OPENNESS] + (1.0f - owner.m_personality[PERSONALITY_NEUROTICISM]);
		personality_bias /= 2.0f;
		
		out_mental_state[MENTAL_FEATURE_HOPE] = out_mental_state[MENTAL_FEATURE_PLEASED] * personality_bias;
		out_mental_state[MENTAL_FEATURE_FEAR] = out_mental_state[MENTAL_FEATURE_DISPLEASED] * (1.0f-personality_bias);
	}
	else
	{
		///Actual Consequence
		///requires to save the event, see the reaction, and then calculate
		float personality_bias = (1.0f - owner.m_personality[PERSONALITY_NEUROTICISM]);
		
		out_mental_state[MENTAL_FEATURE_JOY] = out_mental_state[MENTAL_FEATURE_PLEASED] * personality_bias;
		out_mental_state[MENTAL_FEATURE_DISTRESS] = out_mental_state[MENTAL_FEATURE_DISPLEASED] * (1.0f - personality_bias);
	}


	///TODO: Familiar/Unfamiliar Aspect (looking at the adjectives of the actor)
	switch(event.m_patientType)
	{
	case PATIENT_OBJECT:
		{
			TraitAttitude owner_patient_object = GetAttitude(event.m_owner, event.m_patient);
			
			if (owner_patient_object[ATTITUDE_FAMILIARITY] > MIN_FAMILIARITY)
			{
				//Familiar Aspect
				if (owner_patient_object[ATTITUDE_LIKING] > 0.5f)
				{
					float value = RangeMapLinearFloat(owner_patient_object[ATTITUDE_LIKING], 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
					const float personality_bias = owner.m_personality[PERSONALITY_EXTROVERSION];
					value = value * pos_val * personality_bias;
					out_mental_state[MENTAL_FEATURE_LOVE] = value;
				}
				else
				{
					float value = RangeMapLinearFloat(owner_patient_object[ATTITUDE_LIKING], 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
					const float personality_bias = owner.m_personality[PERSONALITY_EXTROVERSION];
					value = value * neg_val * personality_bias;
					out_mental_state[MENTAL_FEATURE_HATE] = value;
				}
			}
			else
			{
				//Unfamiliar Aspect
				if (owner_patient_object[SOCIAL_ROLE_LIKING] > 0.5f)
				{
					float value = RangeMapLinearFloat(owner_patient_object[SOCIAL_ROLE_LIKING], 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
					const float personality_bias = owner.m_personality[PERSONALITY_OPENNESS];
					value = value * pos_val * personality_bias;
					out_mental_state[MENTAL_FEATURE_INTEREST] = value;
				}
				else
				{
					float value = RangeMapLinearFloat(owner_patient_object[SOCIAL_ROLE_LIKING], 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
					const float personality_bias = owner.m_personality[PERSONALITY_OPENNESS];
					value = value * neg_val * personality_bias;
					out_mental_state[MENTAL_FEATURE_DISGUST] = value;
				}
			}
			break;
		}
	case PATIENT_ANPC: 
		{

		if (event.m_owner != event.m_patient)
		{
			TraitSocialRole owner_patient_anpc = GetSocial(event.m_owner, event.m_patient);

				if (owner_patient_anpc[SOCIAL_ROLE_FAMILIARITY] > MIN_FAMILIARITY)
				{
					//Familiar Aspect
					if (owner_patient_anpc[SOCIAL_ROLE_LIKING] > 0.5f)
					{
						float value = RangeMapLinearFloat(owner_patient_anpc[SOCIAL_ROLE_LIKING], 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
						const float personality_bias = owner.m_personality[PERSONALITY_EXTROVERSION];
						value = value * pos_val * personality_bias;
						out_mental_state[MENTAL_FEATURE_LOVE] = value;
					}
					else
					{
						float value = RangeMapLinearFloat(owner_patient_anpc[SOCIAL_ROLE_LIKING], 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
						const float personality_bias = owner.m_personality[PERSONALITY_EXTROVERSION];
						value = value * neg_val * personality_bias;
						out_mental_state[MENTAL_FEATURE_HATE] = value;
					}
				}
				else
				{
					//Unfamiliar Aspect
					if (owner_patient_anpc[SOCIAL_ROLE_LIKING] > 0.5f)
					{
						float value = RangeMapLinearFloat(owner_patient_anpc[SOCIAL_ROLE_LIKING], 0.5f, 1.0f, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
						const float personality_bias = owner.m_personality[PERSONALITY_OPENNESS];
						value = value * pos_val * personality_bias;
						out_mental_state[MENTAL_FEATURE_INTEREST] = value;
					}
					else
					{
						float value = RangeMapLinearFloat(owner_patient_anpc[SOCIAL_ROLE_LIKING], 0.0f, 0.5f, MAX_UNIT_VALUE, MIN_UNIT_VALUE);
						const float personality_bias = owner.m_personality[PERSONALITY_OPENNESS];
						value = value * neg_val * personality_bias;
						out_mental_state[MENTAL_FEATURE_DISGUST] = value;
					}
				}
			}
		}
	}
	

	///Self/OTher ANPC
	if(event.m_owner == event.m_actor)
	{
		//Self ANPC
		float pos_bias = owner.m_personality[PERSONALITY_CONSCIENTIOUSNESS] * owner.m_personality[PERSONALITY_NEUROTICISM];
		pos_bias /= 2.0f;

		float neg_bias = owner.m_personality[PERSONALITY_EXTROVERSION] * (1.0f - owner.m_personality[PERSONALITY_NEUROTICISM]);
		neg_bias /= 2.0f;

		out_mental_state[MENTAL_FEATURE_PRIDE] = out_mental_state[MENTAL_FEATURE_APPROVING] * pos_val * pos_bias;
		out_mental_state[MENTAL_FEATURE_SHAME] = out_mental_state[MENTAL_FEATURE_DISAPPROVING] * neg_val * neg_bias;

		if(certainty > MIN_CERTAINTY)
		{
			///TODO: Related Consequence and Action for Self ANPC

			out_mental_state[MENTAL_FEATURE_GRATIFICATION] = out_mental_state[MENTAL_FEATURE_PRIDE] * pos_val * pos_bias;
			out_mental_state[MENTAL_FEATURE_REMORSE] = out_mental_state[MENTAL_FEATURE_SHAME] * neg_val * neg_bias;
		}
	}
	else
	{
		//Other ANPC
		float pos_bias = owner.m_personality[PERSONALITY_CONSCIENTIOUSNESS] * owner.m_personality[PERSONALITY_NEUROTICISM];
		pos_bias /= 2.0f;

		float neg_bias = owner.m_personality[PERSONALITY_EXTROVERSION] * (1.0f - owner.m_personality[PERSONALITY_NEUROTICISM]);
		neg_bias /= 2.0f;
		
		out_mental_state[MENTAL_FEATURE_ADMIRATION] = out_mental_state[MENTAL_FEATURE_APPROVING] * pos_val * pos_bias;
		out_mental_state[MENTAL_FEATURE_REPROACH] = out_mental_state[MENTAL_FEATURE_DISAPPROVING] * neg_val * neg_bias;

		if (certainty > MIN_CERTAINTY)
		{
			///TODO: Related Consequence and Action for Other ANPC
			out_mental_state[MENTAL_FEATURE_GRATITUDE] = out_mental_state[MENTAL_FEATURE_PRIDE] * pos_val * pos_bias;
			out_mental_state[MENTAL_FEATURE_ANGER] = out_mental_state[MENTAL_FEATURE_SHAME] * neg_val * neg_bias;
		}
	}
	
	//Calculate Feelings
	///TODO: Consequence Confirms Prospective Desirable Consequence
	///TODO: Consequence Confirms Prospective Undesirable Consequence
	///TODO: Consequence Discconfirms Prospective Undesirable Consequence
	///TODO: Consequence Discconfirms Prospective Desirable Consequence
	///TODO: Consequence Presumed to be Desirable For Other
	///TODO: Consequence Presumed to be Undesirable For Other
}


void APF_Lib::APF::UpdateSocailRelation(const int anpc_owner_idx, const int anpc_actor_idx,
	const float(& new_mental_state)[NUM_MENTAL_FEATURES])
{
	if(anpc_owner_idx == anpc_actor_idx)
	{
		return;
	}

	float liking_summation = 0.0f;
	float dominance_summation = 0.0f;
	float solidarity_summation = 0.0f;
	float familiarity_summation = 0.0f;

	float liking_instance = 0.0f;
	float dominance_instance = 0.0f;
	float solidarity_instance = 0.0f;
	float familiarity_instance = 0.0f;


	familiarity_summation += FAMILIARITY_GROWTH;

	for (int feat_idx = 0; feat_idx < NUM_MENTAL_FEATURES; ++feat_idx)
	{
		float emotion_value = new_mental_state[feat_idx];

		if (emotion_value == 0.0f) continue;

		switch (feat_idx)
		{
		case MENTAL_FEATURE_POSITIVE:
		{
			break;
		}

		case MENTAL_FEATURE_NEGATIVE:
		{
			break;
		}

		case MENTAL_FEATURE_PLEASED:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;
			break;
		}

		case MENTAL_FEATURE_DISPLEASED:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_LIKING:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_DISLIKING:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_APPROVING:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_DISAPPROVING:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_HOPE:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_FEAR:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_JOY:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_DISTRESS:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}


		case MENTAL_FEATURE_LOVE:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_HATE:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_INTEREST:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_DISGUST:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}


		case MENTAL_FEATURE_PRIDE:
		{
			dominance_summation += emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_SHAME:
		{
			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_ADMIRATION:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_REPROACH:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			dominance_summation += emotion_value;
			dominance_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		
		case MENTAL_FEATURE_SATISFACTION:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			dominance_summation += emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_FEARS_CONFIRMED:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_RELIEF:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_DISAPPOINTMENT:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_HAPPY_FOR:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_RESENTMENT:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_GLOATING:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			dominance_summation += emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_PITY:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_GRATIFICATION:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_REMORSE:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_GRATITUDE:
		{
			liking_summation += emotion_value;
			liking_instance += 1.0f;

			dominance_summation -= emotion_value;
			dominance_instance += 1.0f;

			break;
		}

		case MENTAL_FEATURE_ANGER:
		{
			liking_summation -= emotion_value;
			liking_instance += 1.0f;

			solidarity_summation -= emotion_value;
			solidarity_instance += 1.0f;

			break;
		}

		}
	}

	if (liking_instance != 0.0f) liking_summation /= liking_instance;
	if (dominance_instance != 0.0f) dominance_summation /= dominance_instance;
	if (solidarity_instance != 0.0f) solidarity_summation /= solidarity_instance;
	if (familiarity_instance != 0.0f) familiarity_summation /= familiarity_instance;

	TraitSocialRole relation = GetSocial(anpc_owner_idx, anpc_actor_idx);

	relation[SOCIAL_ROLE_LIKING] += ClampFloat(liking_summation, -1.0f, 1.0f);
	relation[SOCIAL_ROLE_DOMINANCE] += ClampFloat(dominance_summation, -1.0f, 1.0f);
	relation[SOCIAL_ROLE_SOLIDARITY] += ClampFloat(solidarity_summation, -1.0f, 1.0f);
	relation[SOCIAL_ROLE_FAMILIARITY] += ClampFloat(familiarity_summation, -1.0f, 1.0f);

	relation[SOCIAL_ROLE_LIKING] = ClampFloat(relation[SOCIAL_ROLE_LIKING], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	relation[SOCIAL_ROLE_DOMINANCE] = ClampFloat(relation[SOCIAL_ROLE_DOMINANCE], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	relation[SOCIAL_ROLE_SOLIDARITY] = ClampFloat(relation[SOCIAL_ROLE_SOLIDARITY], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
	relation[SOCIAL_ROLE_FAMILIARITY] = ClampFloat(relation[SOCIAL_ROLE_FAMILIARITY], MIN_UNIT_VALUE, MAX_UNIT_VALUE);
}


void APF_Lib::APF::SortAnpcList()
{
	std::sort(m_anpcs.begin(), m_anpcs.end());
}


void APF_Lib::APF::SortObjectList()
{
	std::sort(m_objects.begin(), m_objects.end());
}


void APF_Lib::APF::SortActionList()
{
	std::sort(m_actions.begin(), m_actions.end());
}


void APF_Lib::APF::PrintAllAnpcs()
{
	printf("Printing entire ANPC list: \n");
	const int num_anpcs = static_cast<int>(m_anpcs.size());

	for (int anpc_idx = 0; anpc_idx < num_anpcs; ++anpc_idx)
	{
		printf("%d: %s \n",
			anpc_idx,
			m_anpcs[anpc_idx].ToString().c_str());
	}
}


void APF_Lib::APF::PrintAllObjects()
{
	printf("Printing entire Object list: \n");
	const int num_objs = static_cast<int>(m_objects.size());

	for (int obj_idx = 0; obj_idx < num_objs; ++obj_idx)
	{
		printf("%d: %s \n",
			obj_idx,
			m_objects[obj_idx].ToString().c_str());

	}
}


void APF_Lib::APF::PrintAllActions()
{
	printf("Printing entire Action list: \n");
	const int num_actions = static_cast<int>(m_actions.size());

	for (int act_idx = 0; act_idx < num_actions; ++act_idx)
	{
		printf("%d: %s \n",
			act_idx,
			m_actions[act_idx].ToString().c_str());
	}
}


void APF_Lib::APF::PrintAllAttitudes()
{
	printf("Printing entire Attitude list: \n");
	const int num_attitudes = static_cast<int>(m_attitudes.size());

	for (int attitude_idx = 0; attitude_idx < num_attitudes; ++attitude_idx)
	{
		printf("%d: %s \n",
			attitude_idx,
			m_attitudes[attitude_idx].ToString().c_str());
	}
}


void APF_Lib::APF::PrintAllPraises()
{
	printf("Printing entire Praise list: \n");
	const int num_praises = static_cast<int>(m_praises.size());

	for (int praise_idx = 0; praise_idx < num_praises; ++praise_idx)
	{
		printf("%d: %s \n",
			praise_idx,
			m_praises[praise_idx].ToString().c_str());
	}
}


void APF_Lib::APF::PrintAllSocials()
{
	printf("Printing entire Social list: \n");
	const int num_socials = static_cast<int>(m_socials.size());

	for (int social_idx = 0; social_idx < num_socials; ++social_idx)
	{
		printf("%d: %s \n",
			social_idx,
			m_socials[social_idx].ToString().c_str());
	}
}


int APF_Lib::APF::BinarySearchAnpcs(const char* name, int l, int r) const
{
	if (r >= l) 
	{
		const int mid = l + (r - l) / 2;
		const int compare = m_anpcs[mid].m_name.compare(name);
		
		if (compare == 0)
		{
			return mid;
		}

		if (compare < 0)
		{
			return BinarySearchAnpcs(name, mid + 1, r);
		}
		else // (compare > 0)
		{
			return BinarySearchAnpcs(name, l, mid - 1);
		}
	}

	return -1;
}


int APF_Lib::APF::BinarySearchObjects(const char* name, int l, int r) const
{
	if (r >= l)
	{
		const int mid = l + (r - l) / 2;
		const int compare = m_objects[mid].m_name.compare(name);

		if (compare == 0)
		{
			return mid;
		}

		if (compare < 0)
		{
			return BinarySearchObjects(name, mid + 1, r);
		}
		else // (compare > 0)
		{
			return BinarySearchObjects(name, l, mid - 1);
		}
	}

	return -1;
}


int APF_Lib::APF::BinarySearchActions(const char* name, int l, int r) const
{
	if (r >= l)
	{
		const int mid = l + (r - l) / 2;
		const int compare = m_actions[mid].m_name.compare(name);

		if (compare == 0)
		{
			return mid;
		}

		if (compare < 0)
		{
			return BinarySearchActions(name, mid + 1, r);
		}
		else // (compare > 0)
		{
			return BinarySearchActions(name, l, mid - 1);
		}
	}

	return -1;
}

