#pragma once

namespace APF_Lib {

#define MIN_UNIT_VALUE 0.0f
#define MAX_UNIT_VALUE 1.0f
#define MENTAL_FEATURE_DECAY_VALUE -0.01f
#define MIN_CERTAINTY 0.9f
#define MIN_FAMILIARITY 0.5f

#define DEGRADE_VALENCE 0.00001f
#define DEGRADE_MOOD 0.0001f
#define DEGRADE_EMOTION 0.001f
#define DEGRADE_FEELINGS 0.001f

#define FAMILIARITY_GROWTH 0.02f
#define FAMILIARITY_DECAY 0.02f

    // memory system added
    constexpr float MENTAL_SIMILAR_MIN = .05f;
    constexpr float MENTAL_SIMILAR_MAX = .5f;
    constexpr float MENTAL_DEGRADE_DELTA = .047f;
	    
	// key codes
	constexpr int SHIFT_KEY = 16;
	constexpr int ESC_KEY = 27;
	constexpr int SPACE_BAR = 32;
	constexpr int LEFT_ARROW = 37;
	constexpr int UP_ARROW = 38;
	constexpr int RIGHT_ARROW = 39;
	constexpr int DOWN_ARROW = 40;
	constexpr int NUM_0_KEY = 48;
	constexpr int NUM_1_KEY = 49;
	constexpr int NUM_2_KEY = 50;
	constexpr int NUM_3_KEY = 51;
	constexpr int NUM_4_KEY = 52;
	constexpr int NUM_5_KEY = 53;
	constexpr int NUM_6_KEY = 54;
	constexpr int NUM_7_KEY = 55;
	constexpr int NUM_8_KEY = 56;
	constexpr int NUM_9_KEY = 57;
	constexpr int A_KEY = 65;
	constexpr int B_KEY = 66;
	constexpr int C_KEY = 67;
	constexpr int D_KEY = 68;
	constexpr int E_KEY = 69;
	constexpr int F_KEY = 70;
	constexpr int G_KEY = 71;
	constexpr int H_KEY = 72;
	constexpr int I_KEY = 73;
	constexpr int J_KEY = 74;
	constexpr int K_KEY = 75;
	constexpr int L_KEY = 76;
	constexpr int M_KEY = 77;
	constexpr int N_KEY = 78;
	constexpr int O_KEY = 79;
	constexpr int P_KEY = 80;
	constexpr int Q_KEY = 81;
	constexpr int R_KEY = 82;
	constexpr int S_KEY = 83;
	constexpr int T_KEY = 84;
	constexpr int U_KEY = 85;
	constexpr int V_KEY = 86;
	constexpr int W_KEY = 87;
	constexpr int X_KEY = 88;
	constexpr int Y_KEY = 89;
	constexpr int Z_KEY = 90;
	constexpr int F1_KEY = 112;
	constexpr int F2_KEY = 113;
	constexpr int F8_KEY = 119;
	constexpr int TILDE_KEY = 192;

	inline float RangeMapLinearFloat(const float in_value, const float in_start, const float in_end, const float out_start,
		const float out_end)
	{
		if (in_end == in_start) return 0.5f * (out_start + out_end);

		const float in_range = in_end - in_start;
		const float out_range = out_end - out_start;
		const float in_distance = in_value - in_start;
		const float in_fraction = in_distance / in_range;
		const float out_distance = in_fraction * out_range;
		const float out_value = out_start + out_distance;
		return out_value;
	}

	inline float ClampFloat(const float value, const float min_value, const float max_value)
	{
		if (value < min_value) return min_value;
		if (value > max_value) return max_value;
	
		return value;
	}

	inline float SmoothStart2(const float input_zero_to_one)
	{
		return input_zero_to_one * input_zero_to_one;
	}

	inline float SmoothStart3(const float input_zero_to_one)
	{
		return input_zero_to_one * input_zero_to_one * input_zero_to_one;
	}

	inline float SmoothStart4(const float input_zero_to_one)
	{
		return input_zero_to_one * input_zero_to_one * input_zero_to_one * input_zero_to_one;
	}

	inline float SmoothStart5(const float input_zero_to_one)
	{
		return input_zero_to_one * input_zero_to_one * input_zero_to_one * input_zero_to_one * input_zero_to_one;
	}

    inline float AbsFloat(const float value)
    {
        return value > 0.f ? value : -value;
    }

    inline float Interpolate(const float valueA, const float valueB, const float fractionOfA)
    {
        return fractionOfA * valueA + (1.f - fractionOfA) * valueB;
    }

    float SmoothStartAndEnd(const float input_zero_to_one);
    float InverseSmoothStartAndEnd(const float input_zero_to_one);
    float RandomZeroToOne();
};