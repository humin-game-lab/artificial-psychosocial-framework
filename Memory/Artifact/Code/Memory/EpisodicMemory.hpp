#pragma once

#include "Memory/Memory.hpp"

namespace APF_Lib {
    namespace MM {
        class QueryList;
        class LongTermMemory;

        class EpisodicMemory
        {
        public:
            static float GetCertaintyForMemoryTrace(MemoryTrace const& trace);

            EpisodicMemory(MemoryTrace const& trace);

            void ClearGameObject(GameObject const* objectToDelete);
            void CleanDecayedMemory();
            virtual void Decay();
            virtual void Mutate();

            virtual bool IsFaded() const;
            virtual float GetMutateProbability() const;            
            virtual void GetMutatedTrace(LongTermMemory const& ltm, MemoryTrace& trace) const;

            bool ConsistWithQueryList(QueryList const& list) const;
            unsigned int GetTraceSize() const;
            void GetSelfQueryList(QueryList& list) const;
            MemoryAndEvent const* GetValidMemAndEventOfIndex(int idx) const;

        protected:
            MemoryTrace m_trace;
            std::vector<bool> m_faded;
            MemoryClock m_clock;

            //specific information
            float m_certainty = 0.f;

            void AppendSelfToList(std::vector<MemoryTrace>& traces) const;
        };
    }
}
