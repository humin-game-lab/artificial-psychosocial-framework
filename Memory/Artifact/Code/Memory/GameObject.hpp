#pragma once

#include "Memory/ObjectState.hpp"
#include "Memory/GameTime.hpp"
#include "Memory/GameTags.hpp"

namespace APF_Lib {
    namespace MM {
        //smart object to remember possible states & actions
        class GameObject 
        {
        public:
            GameObject(GameTime const& birthTime);
            ~GameObject();

            virtual void SetNewState(ObjectState const& newState);
            void SetName(std::string const& newName);
            void SetAPFIndex(int newIdx);

            ObjectState const* GetSelfState() const { return m_selfState; }
            std::string GetName() const { return m_name; }
            int         GetIndex() const { return m_apfIdx; }
            GameTags const& GetTags() const {return m_tags;}
            GameTags& GetTagsToChange() {return m_tags;}

        protected:
            GameTime m_birthTime;
            std::string m_name;
            int m_apfIdx = -1;
            GameTags m_tags;

            ObjectState* m_selfState = nullptr;

            //TODO store all possible states & actions
            //std::vector<StateName> m_stateChoices;
            //std::vector<ActionName> m_actionChoices;
        };
    }
}

