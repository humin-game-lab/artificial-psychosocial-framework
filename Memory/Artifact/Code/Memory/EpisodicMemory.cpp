#include "Memory/ShortTermMemory.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Memory/MemoryQuerySystem.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
static void TrimMemoryTrace(MemoryTrace& trace)
{
    for (std::vector<MemoryAndEvent>::iterator it = trace.begin(); it != trace.end(); ) {
        MemoryAndEvent& memEvent = *it;
        if (memEvent.memory.m_isGarbage && memEvent.selfEvent.m_isGarbage) {
            it = trace.erase(it);
        }
        else {
            break;
        }
    }

    for (int i = (int)trace.size() - 1; i >= 0; i--) {
        MemoryAndEvent& memEvent = trace[i];
        if (!(memEvent.memory.m_isGarbage && memEvent.selfEvent.m_isGarbage)) {
            trace.erase(trace.begin() + i + 1, trace.end());
            break;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
EpisodicMemory::EpisodicMemory(MemoryTrace const& trace)
    : m_trace(trace)
{
    TrimMemoryTrace(m_trace);
    m_certainty = GetCertaintyForMemoryTrace(trace);
    for (MemoryAndEvent const& memEvent : m_trace) {
        memEvent;
        m_faded.push_back(false);
    }
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::CleanDecayedMemory()
{
    for (size_t i=0; i<m_trace.size(); i++) {
        MemoryAndEvent const& memEvent = m_trace[i];
        if (!m_faded[i] && memEvent.memory.IsFaded() && memEvent.selfEvent.IsFaded()) {
            m_faded[i] = true;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::Decay()
{
    if (NearlyEqual(m_certainty, MAX_UNIT_VALUE, STATE_CONFIDENCE_MUTATE_EXTENT)) {
        return;
    }

    for (size_t i=0;i<m_trace.size();i++) {
        if (m_faded[i]) {
            continue;
        }

        MemoryAndEvent& memEvent = m_trace[i];
        memEvent.memory.Forget();
        if (!memEvent.selfEvent.m_isGarbage) {
            memEvent.selfEvent.Forget();
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::Mutate()
{
    if (NearlyEqual(m_certainty, MAX_UNIT_VALUE, STATE_CONFIDENCE_MUTATE_EXTENT)) {
        return;
    }

    for (size_t i = 0; i < m_trace.size(); i++) {
        if (m_faded[i]) {
            continue;
        }

        MemoryAndEvent& memEvent = m_trace[i];
        memEvent.memory.Mutate();
        if (!memEvent.selfEvent.m_isGarbage) {
            memEvent.selfEvent.Mutate();
        }
    }
}

//////////////////////////////////////////////////////////////////////////
bool EpisodicMemory::IsFaded() const
{
    for (bool const& b : m_faded) {
        if (!b) {
            return false;
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////
float EpisodicMemory::GetMutateProbability() const
{
    return m_certainty*m_clock.GetMutatePossibility();
}

//////////////////////////////////////////////////////////////////////////
unsigned int EpisodicMemory::GetTraceSize() const
{
    return (unsigned int)m_trace.size();
}

//////////////////////////////////////////////////////////////////////////
bool EpisodicMemory::ConsistWithQueryList(QueryList const& list) const
{
    size_t index = 0;
    bool isLastMemory = false;
    while(!list.IsEmpty()){
        QueryNode* curNode = list.PopFront();
        if (curNode->type == eQueryType::QUERY_MEMORY) {
            if (isLastMemory) {
                index++;
            }

            isLastMemory = true;
            MemoryQueryNode* memNode = static_cast<MemoryQueryNode*>(curNode);
            GetMemorySimilarity func = memNode->pair.first;
            Memory const& mem = memNode->pair.second;
            if (!m_faded[index] && index<m_trace.size() 
                && func(mem, m_trace[index].memory) <= MEMORY_SIMILAR_THRESHOLD*.9f) {
                return false;
            }
        }
        else if(curNode->type==eQueryType::QUERY_EVENT){
            if (!isLastMemory) {
                index++;
            }

            isLastMemory = false;
            EventQueryNode* eventNode = static_cast<EventQueryNode*>(curNode);
            GetEventSimilarity func = eventNode->pair.first;
            GameEvent const& event = eventNode->pair.second;
            if (!m_faded[index] && index < m_trace.size() 
                && func(event, m_trace[index].selfEvent) <= EVENT_SIMILAR_THRESHOLD) {
                return false;
            }

            index++;
        }

        if (index >= m_trace.size()) {
            return true;
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::GetSelfQueryList(QueryList& list) const
{
    list.Clear();
    list.SimpleAddNodes(m_trace, Memory::GetSimilarity, GameEvent::GetSimilarity);
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::AppendSelfToList(std::vector<MemoryTrace>& traces) const
{
    traces.push_back(m_trace);
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::GetMutatedTrace(LongTermMemory const& ltm, MemoryTrace& result) const
{
    QueryList query;
    query.SimpleAddNodes(m_trace, Memory::GetSimilarity, GameEvent::GetSimilarity);
    std::vector<std::vector<LTMConstNodeAndEdge>> traces;
    ltm.RetrieveAllNodesOfQueryListWithinRange(query, (unsigned int)m_trace.size(), traces);

    query.Clear();
    if (traces.size() < 2) {
        result = m_trace;
        return;
    }

    int idx = RandomIntInRange(0,(int)traces.size()-1);
    std::vector<LTMConstNodeAndEdge> const& trace = traces[idx];
    float mutatePossibility = GetMutateProbability();

    for (size_t i=0;i<trace.size();i++) {
        LTMConstNodeAndEdge const& nodeAndEdge = trace[i];
        LTMNode const* node = nodeAndEdge.node;
        MemoryAndEvent memEvent;
        memEvent.memory = node->GetMemory();        
        if (RandomZeroToOne() <= mutatePossibility || m_faded[i]) {  //replace
            if (i == 0) {
                node = ltm.GetRandomStartingLTMNode(*trace[i+1].node);
            }
            else if (i + 1 == trace.size()) {
                node = ltm.GetRandomEndingLTMNode(*trace[i-1].node);
            }
            else {
                node = ltm.GetRandomLTMNode(*trace[i - 1].node, *trace[i + 1].node);
            }
            memEvent.memory = node->GetMemory();
        }
        if (RandomZeroToOne() <= mutatePossibility) {   //normal mutate
            memEvent.memory.Mutate();
        }

        if (nodeAndEdge.edge) {
            nodeAndEdge.edge->GetEvent(memEvent.selfEvent);
            if ((RandomZeroToOne() <= mutatePossibility
                    && i+1<trace.size()) || m_faded[i]) {  //replace
                LTMEdge const* newAction = LTMNode::GetAnyActionBetweenNodes(node, trace[i+1].node);
                newAction->GetEvent(memEvent.selfEvent);
            }
            if (RandomZeroToOne() <= mutatePossibility) {   //normal mutate
                memEvent.selfEvent.Mutate();
            }
        }
        result.push_back(memEvent);
    }
}

//////////////////////////////////////////////////////////////////////////
MemoryAndEvent const* EpisodicMemory::GetValidMemAndEventOfIndex(int idx) const
{
    if (idx < 0 || idx >= m_trace.size() || m_faded[idx]) {
        return nullptr;
    }

    return &m_trace[idx];
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MM::EpisodicMemory::GetCertaintyForMemoryTrace(MemoryTrace const& trace)
{
    if (trace.size() < 1) {
        return 0.f;
    }

    float certainty = 0.f;
    size_t lastIdx = trace.size() - 1;
    float fraction = 1.f / (float)lastIdx;
    for (size_t i = 0; i < lastIdx; i++) {
        MemoryAndEvent const& memEvent = trace[i];
        certainty += memEvent.selfEvent.m_certainty;
    }
    certainty *= fraction;

    return certainty;
}

//////////////////////////////////////////////////////////////////////////
void EpisodicMemory::ClearGameObject(GameObject const* objectToDelete)
{
    for (MemoryAndEvent& memEvent : m_trace) {
        if (memEvent.selfEvent.m_target == objectToDelete) {
            memEvent.selfEvent.m_target = nullptr;
        }
    }
}
