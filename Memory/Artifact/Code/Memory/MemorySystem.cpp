#include "Memory/MemorySystem.hpp"
#include "Memory/GameActor.hpp"
#include "Memory/Logger.hpp"
#include "APF/APF_Lib.hpp"

using namespace APF_Lib::MM;

APF_Lib::APF* APF_Lib::MM::MemorySystem::gMemoryAPF = nullptr;

//////////////////////////////////////////////////////////////////////////
void MemorySystem::ClearMemoryAPF()
{
    if (gMemoryAPF) {
        delete gMemoryAPF;
        gMemoryAPF = nullptr;
    }
}

//////////////////////////////////////////////////////////////////////////
MemorySystem::MemorySystem(GameActor* owner)
    : m_owner(owner)
{    
    m_LTM = new LongTermMemory(this);
    m_STM = new ShortTermMemory(this);
}

//////////////////////////////////////////////////////////////////////////
MemorySystem::~MemorySystem()
{
    delete m_STM;
    m_STM = nullptr;
    delete m_LTM;
    m_LTM = nullptr;
}

//////////////////////////////////////////////////////////////////////////
static constexpr float VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[3] = { .3f, .5f, .2f };
static constexpr float VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[3] = { .3f, .2f, .5f };

//////////////////////////////////////////////////////////////////////////
static float GetDeltaPraiseFromMentalStates(APF_Lib::MentalState const& ms)
{
    float posSum = 0.f;
    float negSum = 0.f;

    posSum += ms[APF_Lib::MENTAL_FEATURE_PRIDE];
    posSum += ms[APF_Lib::MENTAL_FEATURE_GRATIFICATION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_SATISFACTION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_RELIEF];
    float posAvg = posSum * .25f;
    posAvg = posAvg * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[2] +
        ms[APF_Lib::MENTAL_FEATURE_APPROVING] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[1] +
        ms[APF_Lib::MENTAL_FEATURE_POSITIVE] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[0];

    negSum += ms[APF_Lib::MENTAL_FEATURE_SHAME];
    negSum += ms[APF_Lib::MENTAL_FEATURE_REMORSE];
    negSum += ms[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED];
    negSum += ms[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT];
    float negAvg = negSum * .25f;
    negAvg = negAvg * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[2] +
        ms[APF_Lib::MENTAL_FEATURE_DISAPPROVING] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_NEGATIVE] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[0];

    return APF_Lib::MM::PolarSmoothStart2(posAvg) - APF_Lib::MM::PolarSmoothStart2(negAvg);
}

//////////////////////////////////////////////////////////////////////////
static float GetDeltaPraiseFromDeltaMentalStates(APF_Lib::MentalState const& ms)
{
    //TODO("no consideration for other agent action");
    float posSum = 0.f;
    float negSum = 0.f;

    posSum += ms[APF_Lib::MENTAL_FEATURE_PRIDE];
    posSum += ms[APF_Lib::MENTAL_FEATURE_GRATIFICATION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_SATISFACTION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_RELIEF];
    float posAvg = posSum * .25f;
    posAvg = posAvg * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[2] +
        ms[APF_Lib::MENTAL_FEATURE_APPROVING] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[1] +
        ms[APF_Lib::MENTAL_FEATURE_POSITIVE] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[0];

    negSum += ms[APF_Lib::MENTAL_FEATURE_SHAME];
    negSum += ms[APF_Lib::MENTAL_FEATURE_REMORSE];
    negSum += ms[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED];
    negSum += ms[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT];
    float negAvg = negSum * .25f;
    negAvg = negAvg * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[2] +
        ms[APF_Lib::MENTAL_FEATURE_DISAPPROVING] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_NEGATIVE] * VALENCE_MOODS_OTHERS_PRAISE_WEIGHTS[0];

    return posAvg - negAvg;
}


//////////////////////////////////////////////////////////////////////////
static float GetDeltaAttitudeLikingFromMentalState(APF_Lib::MentalState const& ms)
{
    float posSum = 0.f;
    posSum += ms[APF_Lib::MENTAL_FEATURE_SATISFACTION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_RELIEF];
    posSum += ms[APF_Lib::MENTAL_FEATURE_LOVE];
    posSum /= 3.f;
    float posAvg = posSum * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[2]
        + ms[APF_Lib::MENTAL_FEATURE_LIKING] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_POSITIVE] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[0];

    float negSum = 0.f;
    negSum += ms[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED];
    negSum += ms[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT];
    negSum += ms[APF_Lib::MENTAL_FEATURE_HATE];
    negSum/=3.f;
    float negAvg = negSum * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[2]
        + ms[APF_Lib::MENTAL_FEATURE_DISLIKING] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_NEGATIVE] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[0];

    return APF_Lib::MM::PolarSmoothStart2(posAvg) - APF_Lib::MM::PolarSmoothStart2(negAvg);
}

//////////////////////////////////////////////////////////////////////////
static float GetDeltaAttitudeLikingFromDeltaMentalState(APF_Lib::MentalState const& ms)
{
    float posSum = 0.f;
    posSum += ms[APF_Lib::MENTAL_FEATURE_SATISFACTION];
    posSum += ms[APF_Lib::MENTAL_FEATURE_RELIEF];
    posSum += ms[APF_Lib::MENTAL_FEATURE_LOVE];
    posSum /= 3.f;
    float posAvg = posSum * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[2]
        + ms[APF_Lib::MENTAL_FEATURE_LIKING] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_POSITIVE] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[0];

    float negSum = 0.f;
    negSum += ms[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED];
    negSum += ms[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT];
    negSum += ms[APF_Lib::MENTAL_FEATURE_HATE];
    negSum /= 3.f;
    float negAvg = negSum * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[2]
        + ms[APF_Lib::MENTAL_FEATURE_DISLIKING] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[1]
        + ms[APF_Lib::MENTAL_FEATURE_NEGATIVE] * VALENCE_MOODS_OTHERS_LIKING_WEIGHTS[0];

    return posAvg - negAvg;
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdatePraiseForEmotionAndEmotionChange(int actorIdx, int actionIdx, MentalState const& emo, MentalState const& delta)
{
    constexpr float PRAISE_MAX_DELTA = .03f;

    APF* memAPF = GetMemoryAPF();
    float oldPraise = memAPF->GetPraise(actorIdx, actionIdx);
    float emoPraiseDelta = GetDeltaPraiseFromMentalStates(emo);
    float deltaPraiseDelta = GetDeltaPraiseFromMentalStates(delta);
    float newPraiseDelta = RangeMapLinearFloat(emoPraiseDelta + deltaPraiseDelta, -0.22f, 0.22f, -PRAISE_MAX_DELTA, PRAISE_MAX_DELTA);
    newPraiseDelta = ClampFloat(newPraiseDelta, -PRAISE_MAX_DELTA, PRAISE_MAX_DELTA);
    float rawPraise = InverseSmoothStartAndEnd(oldPraise);
    float newPraise = SmoothStartAndEnd(ClampFloat(rawPraise + newPraiseDelta, 0.f, 1.f));
    memAPF->OverwritePraise(actorIdx, actionIdx, newPraise);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateAttitudeForEmotionAndEmotionChange(int actorIdx, int objIdx, MentalState const& emo, MentalState const& delta)
{
    constexpr float ATTITUDE_LIKING_MAX_DELTA = .055f;
    constexpr float ATTITUDE_FAMILARITY_MAX_DELTA = .02f;
    
    APF* memAPF = GetMemoryAPF();
    TraitAttitude oldAttitude = memAPF->GetAttitude(actorIdx, objIdx);
    float oldLiking = oldAttitude.m_traits[ATTITUDE_LIKING];
    float emoLikingDelta = GetDeltaAttitudeLikingFromMentalState(emo);
    float deltaLikingDelta = GetDeltaAttitudeLikingFromDeltaMentalState(delta) * 8.f;
    float newLikingDelta = RangeMapLinearFloat(emoLikingDelta + deltaLikingDelta, -.7f, .6f, -ATTITUDE_LIKING_MAX_DELTA, ATTITUDE_LIKING_MAX_DELTA);
    newLikingDelta = ClampFloat(newLikingDelta, -ATTITUDE_LIKING_MAX_DELTA, ATTITUDE_LIKING_MAX_DELTA);
    float rawLiking = InverseSmoothStartAndEnd(oldLiking);
    float newLiking = SmoothStartAndEnd(ClampFloat(rawLiking + newLikingDelta, 0.f, 1.f));
    float newFamiliar = ClampFloat(oldAttitude.m_traits[ATTITUDE_FAMILIARITY]+ATTITUDE_FAMILARITY_MAX_DELTA, MIN_UNIT_VALUE, MAX_UNIT_VALUE);
    memAPF->OverwriteAttitude(actorIdx, objIdx, newLiking, newFamiliar);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateStateForEmotionAndEmotionChange(State* state, MentalState const& totalEmo, MentalState const& deltaEmo)
{
    constexpr float STATE_DELTA_MAX_DELTA = .01f;

    float emoStateDelta = GetDeltaAttitudeLikingFromMentalState(totalEmo);
    float deltaStateDelta = GetDeltaAttitudeLikingFromDeltaMentalState(deltaEmo);
    float newStateDelta = RangeMapLinearFloat(emoStateDelta + deltaStateDelta, -.2f, .2f, -STATE_DELTA_MAX_DELTA, STATE_DELTA_MAX_DELTA);
    newStateDelta = ClampFloat(newStateDelta, -STATE_DELTA_MAX_DELTA, STATE_DELTA_MAX_DELTA);
    float newState = ClampFloat(newStateDelta + state->valenceDelta, -.5f, .5f);
    state->valenceDelta = newState;
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::AppraiseForEvent(MentalState& newEmotions, Memory const& curState, GameEvent const& action, Memory const& futureState)
{
    Memory tempMem = futureState;
    if (action.m_actionInfo == nullptr) {
        return;
    }

    APF* apfLib = GetMemoryAPF();
    MentalState mentalBefore;
    apfLib->GetANPCsMentalState(mentalBefore, GetOwnerIndex());

    //object states delta
    TraitAttitude att;
    float newLiking = 0.f;
    if (action.m_patientType == PATIENT_OBJECT) {
        att = apfLib->GetAttitude(action.m_owner, action.m_patient);
        if (action.m_target != nullptr) {
            ObjectState const* targetState = action.m_target->GetSelfState();
            if (targetState) {
                float deltaObjectStates = targetState->GetTotalDeltaValence();
                newLiking = ClampFloat(att.m_traits[ATTITUDE_LIKING] + deltaObjectStates, 0.f, MAX_UNIT_VALUE);
                apfLib->OverwriteAttitude(action.m_owner, action.m_patient, newLiking,
                    att.m_traits[ATTITUDE_FAMILIARITY]);
            }            
        }
    }

    //action states delta
    float praise = apfLib->GetPraise(action.m_owner, action.m_action);
    float deltaPraise = action.GetTotalDeltaValence();
    float newPraise = ClampFloat(deltaPraise + praise, 0.f, MAX_UNIT_VALUE);
    apfLib->OverwritePraise(action.m_owner, action.m_action, newPraise);

    //Generate emotion without prospect
    if (action.m_patientType == PATIENT_OBJECT) {
        apfLib->AddEventWithObject(action.m_owner, action.m_actor, action.m_action, action.m_patient, action.m_certainty);
    }
    else {
        apfLib->AddEventWithANPC(action.m_owner, action.m_actor, action.m_action, action.m_patient, action.m_certainty);
    }
    apfLib->ProcessEvents();

    //reset delta revised praise & attitudes
    if (action.m_patientType == PATIENT_OBJECT) {
        apfLib->OverwriteAttitude(action.m_owner, action.m_patient, att.m_traits[ATTITUDE_LIKING],
            att.m_traits[ATTITUDE_FAMILIARITY]);
    }
    apfLib->OverwritePraise(action.m_owner, action.m_action, praise);

    //Generate memory instance
    tempMem = curState;
    std::vector<LTMConstNodeList> traces;
    m_LTM->RetrieveAllTracesWithinRange(&tempMem, LTM_PROSPECT_MAX_RANGE, traces);
    if (traces.empty()) {   //couldn't retrieve from LTM
        apfLib->GetANPCsMentalState(newEmotions, GetOwnerIndex()); 

        //TODO("joy, distress customized calculations?");
    }
    else {  
        //successfully retrieved from LTM
        Memory prospectMemory;
        m_getProspectFromTraces(prospectMemory, traces, mentalBefore, m_prospectMultiplierFunc);
        tempMem.m_gameState = futureState.m_gameState;
        tempMem.m_selfState = futureState.m_selfState;

        float similarity = Memory::GetSimilarity(tempMem, prospectMemory);

        MentalState firstMental;
        apfLib->GetANPCsMentalState(firstMental, GetOwnerIndex());
        MentalState::CombineMentalStatesClamped(firstMental, prospectMemory.m_mentalState, PROSPECT_MENTAL_COMBINE_DELTA, tempMem.m_mentalState);

        tempMem.m_mentalState.SetFromRawAndProspect(firstMental, prospectMemory.m_mentalState, similarity);
        newEmotions = tempMem.m_mentalState;
    }

    apfLib->SetANPCsMentalState(GetOwnerIndex(), newEmotions);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateForNewEvent(GameEvent const& action)
{
    APF* apfLib = GetMemoryAPF();
    MentalState mentalBefore;
    apfLib->GetANPCsMentalState(mentalBefore,GetOwnerIndex());

    //object states delta
    TraitAttitude att;
    float newLiking = 0.f;
    if (action.m_patientType == PATIENT_OBJECT) {
        att = apfLib->GetAttitude(action.m_owner, action.m_patient);
        if (action.m_target != nullptr) {
            ObjectState const* targetState = action.m_target->GetSelfState();
            if (targetState) {
                float deltaObjectStates = targetState->GetTotalDeltaValence();
                newLiking = ClampFloat( att.m_traits[ATTITUDE_LIKING] + deltaObjectStates, 0.f, MAX_UNIT_VALUE);
                apfLib->OverwriteAttitude(action.m_owner, action.m_patient, newLiking,
                    att.m_traits[ATTITUDE_FAMILIARITY]);
            }
        }
    }

    //action states delta
    float praise = apfLib->GetPraise(action.m_owner, action.m_action);
    float deltaPraise = action.GetTotalDeltaValence();
    float newPraise = ClampFloat(deltaPraise+praise, 0.f, MAX_UNIT_VALUE);
    apfLib->OverwritePraise(action.m_owner, action.m_action, newPraise);

    //Generate emotion without prospect
    if(action.m_patientType==PATIENT_OBJECT){
        apfLib->AddEventWithObject(action.m_owner,action.m_actor,action.m_action,action.m_patient,action.m_certainty);
    }
    else {
        apfLib->AddEventWithANPC(action.m_owner, action.m_actor, action.m_action, action.m_patient, action.m_certainty);
    }
    apfLib->ProcessEvents();

    std::string stats = "praise "+std::to_string(newPraise)+" liking "+std::to_string(newLiking);
    Logger::GetLogger()->LogInfo(stats);

    //reset delta revised praise & attitudes
    if (action.m_patientType == PATIENT_OBJECT) {
        apfLib->OverwriteAttitude(action.m_owner, action.m_patient, att.m_traits[ATTITUDE_LIKING],
            att.m_traits[ATTITUDE_FAMILIARITY]);
    }
    apfLib->OverwritePraise(action.m_owner, action.m_action, praise);

    //Generate memory instance
    Memory tempMemory = *(m_STM->GetLatestMemory());
    std::vector<LTMConstNodeList> traces;
    m_LTM->RetrieveAllTracesWithinRange(&tempMemory, LTM_PROSPECT_MAX_RANGE, traces);
    //m_LTM->FilterMemoryChainWihAction(traces, action);
    if (traces.empty()) {   //couldn't retrieve from LTM
        apfLib->GetANPCsMentalState(tempMemory.m_mentalState, GetOwnerIndex()); 
        tempMemory.m_gameState = m_owner->GetSensedGameState();
        if (m_owner->GetSelfState()) {
            tempMemory.SetSelfState(*m_owner->GetSelfState());
        }

        //TODO("joy, distress custom calculations for memory?");
    }
    else {  //successfully retrieved from LTM
        m_prospectMemory = tempMemory;
        m_getProspectFromTraces(m_prospectMemory, traces, mentalBefore,m_prospectMultiplierFunc);
        tempMemory.m_gameState = m_owner->GetSensedGameState();
        if (m_owner->GetSelfState()) {
            tempMemory.SetSelfState(*m_owner->GetSelfState());
        }

        Logger::GetLogger()->LogInfo("pure prospect: "+m_prospectMemory.m_mentalState.ToString());

        float similarity = Memory::GetSimilarity(tempMemory, m_prospectMemory);

        MentalState firstMental;
        apfLib->GetANPCsMentalState(firstMental, GetOwnerIndex());
        MentalState::CombineMentalStatesClamped(firstMental,m_prospectMemory.m_mentalState,PROSPECT_MENTAL_COMBINE_DELTA,tempMemory.m_mentalState);

        Logger::GetLogger()->LogInfo("combined: "+ tempMemory.m_mentalState.ToString());

        tempMemory.m_mentalState.SetFromRawAndProspect(tempMemory.m_mentalState, m_prospectMemory.m_mentalState, similarity);

        Logger::GetLogger()->LogInfo("prospect revised"+ tempMemory.m_mentalState.ToString());
    }

    apfLib->SetANPCsMentalState(GetOwnerIndex(), tempMemory.m_mentalState);
    
    m_STM->AddNewEvent(action);
    m_STM->AddNewMemory(tempMemory);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateWithoutAppraisal(GameEvent const& action)
{
    //generate prospect
    Memory tempMemory = *(m_STM->GetLatestMemory());
    std::vector<LTMConstNodeList> traces;
    m_LTM->RetrieveAllTracesWithinRange(&tempMemory, LTM_PROSPECT_MAX_RANGE, traces);
    tempMemory.m_isGarbage = false;
    tempMemory.m_gameState = m_owner->GetSensedGameState();
    if (m_owner->GetSelfState()) {
        tempMemory.SetSelfState(*m_owner->GetSelfState());
    }
    if (!traces.empty()) {
        GetMemoryAPF()->GetANPCsMentalState(tempMemory.m_mentalState, GetOwnerIndex());
        m_prospectMemory = tempMemory;
        m_getProspectFromTraces(m_prospectMemory, traces, tempMemory.m_mentalState, m_prospectMultiplierFunc);

        Logger::GetLogger()->LogInfo("pure prospect: "+ m_prospectMemory.m_mentalState.ToString());

        float similarity = Memory::GetSimilarity(tempMemory, m_prospectMemory);
        tempMemory.m_mentalState.SetFromRawAndProspect(tempMemory.m_mentalState, m_prospectMemory.m_mentalState, similarity);

        Logger::GetLogger()->LogInfo("prospect revised: "+ tempMemory.m_mentalState.ToString());

        GetMemoryAPF()->SetANPCsMentalState(GetOwnerIndex(), tempMemory.m_mentalState);
    }

    m_STM->AddNewEvent(action);
    m_owner->UpdateForSelfAction(action);
    m_STM->AddNewMemory(tempMemory);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateForRepetition()
{
    //detect current memory VS others
    constexpr unsigned int MEMORY_RECENT_NUMBER = 3;
    MemoryTrace recentMem;
    m_STM->GetRecentMemories(MEMORY_RECENT_NUMBER, recentMem);
    QueryList memList;
    memList.SimpleAddNodes(recentMem, Memory::GetSimilarity, GameEvent::GetSimilarity);

    std::vector<MemoryTrace> repititions;
    SearchForQueryList(memList, 5, repititions);
    Memory general;
    Memory::GenerateGeneralMemoryFromTraces(general, repititions);

    m_STM->UpdateForRecentRepetition(MEMORY_RECENT_NUMBER, general, repititions.size());
    //TODO needs LTM update?
    //TODO current vs episodic

    memList.Clear();
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateForHourIncrease(GameTime const& time)
{
    time;   //unused
    //TODO("Update for blank nodes");
    m_LTM->CombineLTMNodesRandomly();
    m_LTM->CombineLTMEdges();
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateForMinuteIncrease(GameTime const& time)
{
    m_STM->CleanDecayedMemory();

    if(time.m_minutes%2==0){
        UpdateForRepetition();

        if (time.m_minutes % 4 == 0) {
            m_STM->Decay();

            if (time.m_minutes % 8 == 0) {
                m_STM->Mutate();
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::SearchForSingleMemoryAndEvent(GetMemorySimilarity memFunc, Memory const& mem, GetEventSimilarity eventFunc, 
    GameEvent const& event, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces)
{
    std::vector<LTMConstNodeList> tracesInLTM;
    m_LTM->RetrieveAllTracesWithinRange(&mem, memFunc, event, eventFunc, maxRange, tracesInLTM);
    for (LTMConstNodeList const& trace : tracesInLTM) {
        LTMConstNodeList nonBlank;
        m_LTM->GenerateNonBlankTrace(trace, nonBlank);
        MemoryTrace tempTrace;
        foundTraces.push_back(tempTrace);
        m_LTM->GenerateAndMutateTrace(trace, foundTraces.back());
    }

    m_STM->SearchForSingleMemoryAndEvent(memFunc,mem,eventFunc,event,maxRange, foundTraces);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::SearchForQueryList(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces)
{
    m_LTM->RetrieveAllTracesOfQueryListWithinRange(list, maxRange, foundTraces);
    m_STM->SearchForQueryList(list,maxRange, foundTraces);
    m_LTM->RetrieveAllTracesOfQueryListInEpisodicMemory(list, foundTraces);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::ConsolidateSTMToLTM()
{
    MemoryTrace newTrace;
    m_STM->GetMemories(0, ShortTermMemory::STM_CAPACITY-1, newTrace);
    UpdateAPFForMemories(newTrace);
    m_LTM->AddNewMemoryTrace(newTrace);

    constexpr unsigned int MEMORY_RECENT_NUMBER = 9;
    MemoryTrace recentMem;
    m_STM->GetMemories(0, MEMORY_RECENT_NUMBER, recentMem);
    QueryList memList;
    memList.SimpleAddNodes(recentMem, Memory::GetSimilarity, GameEvent::GetSimilarity);
    m_LTM->ActivateAndUpdateEpisodicMemories(memList);
    memList.Clear();
}

//////////////////////////////////////////////////////////////////////////
int MemorySystem::GetOwnerIndex() const
{
    return m_owner->GetIndex();
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::UpdateAPFForMemories(MemoryTrace const& trace)
{
    if (trace.empty()) {
        return;
    }

    int ownerIdx = GetOwnerIndex();
    int totalMemNum = 0;
    float totalMental[NUM_MENTAL_FEATURES] = {0.f};
    /*MentalState startMental = trace[0].memory.m_mentalState;
    totalEmotion += startMental;*/

    struct StateMentalConfidence
    {
        MentalState state;
        float confidence = 0.f;
    };

    //categorize all actions with memories
    std::unordered_map<int, std::vector<Memory const*>> actions;
    std::unordered_map<GameObject const*, std::vector<Memory const*>> objects;
    std::unordered_map<State const*, std::vector<StateMentalConfidence>> states;
    for (int i = 1; i < (int)trace.size(); i++) {
        GameEvent const& pastEvent = trace[i - 1].selfEvent;
        int actId = pastEvent.m_action;
        Memory const* temp = &trace[i].memory;
        //TODO("special dealing for no object actions");        

        totalMemNum++;
        MentalState const& curMental = temp->m_mentalState;
        for (int j = 0; j < NUM_MENTAL_FEATURES; j++) {
            totalMental[j] += curMental[j];
        }
        if (pastEvent.m_patientType != PATIENT_UNKNOWN) {
            actions[actId].push_back(temp);
        }
        for (std::map<GameObject*, ObjectState>::const_iterator it = temp->m_gameState.objectPerceivedStates.cbegin();
            it != temp->m_gameState.objectPerceivedStates.cend(); it++) {
            std::vector<ObjectStateAspect> const& tempStates = it->second.states;
            if (!tempStates.empty()) {
                for (ObjectStateAspect const& apsect : tempStates) {
                    StateMentalConfidence mentalCon;
                    for (int j = 0; j < NUM_MENTAL_FEATURES; j++) {
                        mentalCon.state[j] = curMental[j] * apsect.confidence;
                    }
                    mentalCon.confidence = apsect.confidence;
                    states[apsect.state].push_back(mentalCon);
                }
            }
        }
        if (pastEvent.m_patientType == PATIENT_OBJECT) {
            objects[pastEvent.m_target].push_back(temp);
        }
    }

    //conclusive emotion EmoCon calculation
    float fraction = 1.f / (float)totalMemNum;
    for (int j = 0; j < NUM_MENTAL_FEATURES; j++) {
        totalMental[j] *= fraction;
    }

    MentalState totalEmotion;
    totalEmotion.SetMentalState(totalMental);
    Logger::GetLogger()->LogInfo("averaged in session: "+ totalEmotion.ToString());

    //process each action
    for (auto const& action : actions) {
        const int actId = action.first;
        std::vector<Memory const*>const& mems = action.second;
        //calculate the overall emotion EmoAct toward one kind of action
        MentalState totalActEmo;
        float actFraction = 1.f / (float)(mems.size());
        for (Memory const* curMem : mems) {
            MentalState curMental = curMem->m_mentalState;
            for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
                totalActEmo[i] += curMental[i] * actFraction;
            }
        }
        //calculate new praise according to difference of EmoCon and EmoAct
        ActionInfo const* info = ActionInfo::GetActionOfAPFIdx(actId);
        std::string avgInfo = "average action "+info->actionName;
        Logger::GetLogger()->LogInfo(avgInfo+": "+ totalActEmo.ToString());
        MentalState delta;
        MentalState::GetDeltaMentalState(totalActEmo, totalEmotion, delta);
        std::string deltaInfo = "delta of action "+info->actionName;
        Logger::GetLogger()->LogInfo(deltaInfo+": "+ delta.ToString());
        m_updatePraiseForEmotions(ownerIdx, actId, totalActEmo, delta);
    }

    //process each object
    for (auto const& obj : objects) {
        GameObject const* objPtr = obj.first;
        if (objPtr == nullptr) {
            continue;
        }

        std::vector<Memory const*>const& mems = obj.second;
        //calculate the overall emotion EmoPatient toward one kind of patient
        MentalState totalObjEmo;
        float objFraction = 1.f / (float)(mems.size());
        for (Memory const* curMem : mems) {
            MentalState curMental = curMem->m_mentalState;
            for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
                totalObjEmo[i] += curMental[i] * objFraction;
            }
        }
        //calculate new attitude according to difference of EmoCon and EmoPatient
        std::string avgInfo = "average object "+objPtr->GetName();
        Logger::GetLogger()->LogInfo(avgInfo+": "+ totalObjEmo.ToString());
        MentalState delta;
        MentalState::GetDeltaMentalState(totalObjEmo, totalEmotion, delta);
        std::string deltaInfo = "delta of object "+objPtr->GetName();
        Logger::GetLogger()->LogInfo(deltaInfo+": "+ delta.ToString());
        m_updateAttitudeForEmotions(ownerIdx, objPtr->GetIndex(), totalEmotion, delta);
    }

    //process each states
    for (auto const& state : states) {
        State* s = const_cast<State*>(state.first);
        float totalStateEmo[NUM_MENTAL_FEATURES] = {0.f};
        float totalConfidence = 0.f;
        for (StateMentalConfidence const& menCon : state.second) {
            for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
                totalStateEmo[i] += menCon.state[i] * menCon.confidence;
            }
            totalConfidence += menCon.confidence;
        }
        float conFrac = 1.f / totalConfidence;
        for (int i = 0; i < NUM_MENTAL_FEATURES; i++) {
            totalStateEmo[i] *= conFrac;
        }

        MentalState totalStateMental;
        totalStateMental.SetMentalState(totalStateEmo);
        MentalState delta;
        MentalState::GetDeltaMentalState(totalStateMental, totalEmotion, delta);
        m_updateStateForEmotions(s, totalStateMental, delta);
    }
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::Initialize()
{
    m_getProspectFromTraces = Memory::GenerateMoodsSimilarMemoryFromTraces;
    m_prospectMultiplierFunc = Memory::GetFarSightMulplierForProspectGeneration;
    m_updateAttitudeForEmotions = UpdateAttitudeForEmotionAndEmotionChange;
    m_updatePraiseForEmotions = UpdatePraiseForEmotionAndEmotionChange;
    m_updateStateForEmotions = UpdateStateForEmotionAndEmotionChange;

    m_clock.onHourIncrease.SubscribeMethod(m_LTM, &LongTermMemory::UpdateEpisodicMemory);
    m_clock.onHourIncrease.SubscribeMethod(this, &MemorySystem::UpdateForHourIncrease);
    m_clock.onMinuteIncrease.SubscribeMethod(this, &MemorySystem::UpdateForMinuteIncrease);
}

//////////////////////////////////////////////////////////////////////////
void MemorySystem::ClearGameObject(GameObject const* objectToDelete)
{
    m_STM->ClearGameObject(objectToDelete);
    m_LTM->ClearGameObject(objectToDelete);

    if (m_prospectAction.m_target == objectToDelete) {
        m_prospectAction.m_target = objectToDelete;
    }
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::APF* MemorySystem::GetMemoryAPF()
{
    if (gMemoryAPF==nullptr) {
        gMemoryAPF = new APF_Lib::APF();        
    }
    return gMemoryAPF;
}

