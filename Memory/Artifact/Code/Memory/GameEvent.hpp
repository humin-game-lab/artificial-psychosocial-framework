#pragma once

#include "APF/Event.hpp"
#include "Memory/GameTags.hpp"
#include <map>

namespace APF_Lib {
    namespace MM {
        struct ActionInfo;
        class GameObject;

        struct GameEvent : public APF_Lib::Event
        {
        public:
            static float GetSimilarity(GameEvent const& eventA, GameEvent const& eventB);

            virtual void Mutate();
            virtual void Forget();
            virtual void Enhance(float delta);

            bool operator==(GameEvent const& other) const;  //except adj, confidence
            bool operator!=(GameEvent const& other) const;  //except adj, confidence
            virtual bool IsFaded() const;
            virtual float GetTotalDeltaValence() const;

        public:
            std::vector<std::string> m_adjectives;
            ActionInfo const* m_actionInfo = nullptr;
            GameObject const* m_target = nullptr;
            bool m_isGarbage = true;
        };

        // blackboard
        struct ActionInfo
        {
        public:
            static std::vector<ActionInfo> sPossibleActions;
            static ActionInfo const* GetActionOfName(std::string const& actionName);
            static ActionInfo const* GetActionOfAPFIdx(int apfIdx);
            static float GetSimilarity(ActionInfo const& infoA, ActionInfo const& infoB);

            ActionInfo(std::string const& name, float effect);

            float GetAdjectiveDelta(std::string const& adj) const;

        public:
            std::string actionName;
            int apfIdx = -1;
            float effect = 0.f;
            //TODO type for targets, NPC/object

            GameTags tags;
            std::map<std::string, float> adjectives;
        };
    }
}

