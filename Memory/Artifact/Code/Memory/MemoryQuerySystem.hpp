#pragma once

#include <utility>
#include <vector>
#include "Memory/Memory.hpp"
#include "Memory/GameEvent.hpp"
#include "Memory/MemCommon.hpp"

namespace APF_Lib {
    namespace MM {
        enum eQueryType
        {
            QUERY_INVALID,
            QUERY_MEMORY,
            QUERY_EVENT
        };

        using MemoryQueryPair = std::pair<GetMemorySimilarity, Memory const&>;
        using EventQueryPair = std::pair<GetEventSimilarity, GameEvent const&>;

        struct QueryNode
        {
            eQueryType type = QUERY_INVALID;
        };

        struct MemoryQueryNode : public QueryNode
        {
            MemoryQueryPair pair;

            MemoryQueryNode(MemoryQueryPair const& p);
        };

        struct EventQueryNode : public QueryNode
        {
            EventQueryPair pair;

            EventQueryNode(EventQueryPair const& p);
        };

        class QueryList
        {
        public:
            void Clear();

            void SimpleAddNodes(MemoryTrace const& trace, GetMemorySimilarity memFunc, GetEventSimilarity eventFunc);

            void AddNode(GetMemorySimilarity func, Memory const& mem);
            void AddNode(GetEventSimilarity func, GameEvent const& event);

            QueryNode* PopFront() const;
            bool IsEmpty() const;

        protected:
            std::vector<QueryNode*> m_queryNodes;
            mutable unsigned int m_index = 0;
        };
    }
}
