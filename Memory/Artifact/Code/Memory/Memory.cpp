#include "Memory/Memory.hpp"
#include "Memory/ShortTermMemory.hpp"
#include "Memory/LongTermMemory.hpp"
#include "APF/LibCommon.hpp"

//TODO("social relations in memory");

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
float Memory::GetSimilarity(Memory const& memA, Memory const& memB)
{
    //TODO add real similarity memory calculation
    //self state info
    float selfInfoSimilar = 1.f;
    if (memA.m_selfState && memB.m_selfState) {
        selfInfoSimilar = ObjectState::GetSimilarity(*memA.m_selfState, *memB.m_selfState);
    }
    else if (memA.m_selfState || memB.m_selfState) {
        selfInfoSimilar = 0.f;
    }
    ////mental state
    //float mentalSimilar = GetSimilarityForMentalStates(memA.m_mentalState,memB.m_mentalState);
    //all game states range map clamp
    float stateSimilar = GameState::GetSimilarity(memA.m_gameState, memB.m_gameState);
    return selfInfoSimilar*.1f+stateSimilar*.9f;
}

//////////////////////////////////////////////////////////////////////////
void Memory::GetCombinedMemory(Memory& combined, Memory const& memA, Memory const& memB, float alpha)
{
    //TODO("combine with different techniques, connected to personality");

    Memory result;
    if (memA.m_selfState && memB.m_selfState && 
        !(memA.m_selfState->states.empty() && memB.m_selfState->states.empty())) {
        result.m_selfState = new ObjectState();
        ObjectState::CombineStates(*memA.m_selfState, *memB.m_selfState, alpha, *result.m_selfState);
    }
    else if (memA.m_selfState) {
        result.m_selfState = new ObjectState(*memA.m_selfState);
    }
    else if (memB.m_selfState) {
        result.m_selfState = new ObjectState(*memB.m_selfState);
    }
    APF_Lib::MentalState::CombineMentalStates(memA.m_mentalState, memB.m_mentalState, alpha, result.m_mentalState);
    GameState::CombineStates(memA.m_gameState, memB.m_gameState, alpha, result.m_gameState);
    combined = result;
}

//////////////////////////////////////////////////////////////////////////
Memory::Memory(Memory const& newMem)
    : m_mentalState(newMem.m_mentalState)
    , m_gameState(newMem.m_gameState)
    , m_isGarbage(newMem.m_isGarbage)
{
    if (newMem.m_selfState) {
        m_selfState = new ObjectState(*newMem.m_selfState);
    }
}

//////////////////////////////////////////////////////////////////////////
void Memory::Mutate()
{
    //TODO("Refine memory mutate: given input extent");
    m_mentalState.Mutate();
    m_gameState.Mutate();
    if (m_selfState) {
        m_selfState->Mutate();
    }
}

//////////////////////////////////////////////////////////////////////////
void Memory::Forget()
{
    //TODO("refine memory forget");
    m_mentalState.Decay();
    m_gameState.Forget();
    if (m_selfState) {
        m_selfState->Forget();
    }
}

//////////////////////////////////////////////////////////////////////////
bool Memory::IsFaded() const
{
    return m_mentalState.IsFaded() &&
        m_gameState.IsFaded() &&
        (m_selfState==nullptr || m_selfState->IsFaded());
}

//////////////////////////////////////////////////////////////////////////
void Memory::RankTracesByMoodsSimilar(std::vector<LTMConstNodeList> const& traces, APF_Lib::MentalState const& moods, GetProspectMultiplierFunction funcPtr, float const (&weights)[2], std::vector<LTMConstNodeList>& ranked)
{
    std::vector<float> similars;
    float moodValues[APF_Lib::NUM_MOOD_FEATURES] = { MIN_UNIT_VALUE };
    moods.GetMood(moodValues);

    for (LTMConstNodeList const& trace : traces) {
        float mood[APF_Lib::NUM_MOOD_FEATURES] = { MIN_UNIT_VALUE };
        GetTraceMentalMood(trace, funcPtr, mood);
        float similar = APF_Lib::MentalState::GetSimilarityForMoods(mood, moodValues, weights);

        bool inserted = false;
        for (size_t i = 0; i < similars.size(); i++) {
            if (similar > similars[i]) {
                similars.insert(similars.begin() + i, similar);
                ranked.insert(ranked.begin() + i, trace);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            similars.push_back(similar);
            ranked.push_back(trace);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void Memory::GenerateProspectMemoryFromTrace(Memory& result, LTMConstNodeList const& trace, GetProspectMultiplierFunction funcPtr)
{
    if (trace.empty()) {
        return;
    }
    else if (trace.size() == 1) {
        result = trace[0]->GetMemory();
        return;
    }

    //result.m_isGarbage = true;
    size_t totalNum = trace.size();
    float* multipliers = new float[(int)totalNum];
    for (size_t i = 0; i < totalNum; i++) {
        multipliers[i] = funcPtr(i, totalNum);
    }

    float accumulateMultiplier = multipliers[0] + multipliers[1];
    Memory::GetCombinedMemory(result, trace[0]->GetMemory(), trace[1]->GetMemory(), multipliers[0] / accumulateMultiplier);
    for (size_t i = 2; i < totalNum; i++) {
        float newMultiplier = multipliers[i];
        Memory::GetCombinedMemory(result, result, trace[i]->GetMemory(), accumulateMultiplier / (accumulateMultiplier + newMultiplier));
        accumulateMultiplier += newMultiplier;
    }

    delete[] multipliers;
}

//////////////////////////////////////////////////////////////////////////
float Memory::GetFarSightMulplierForProspectGeneration(size_t idx, size_t totalNumber)
{
    if (idx >= LTM_PROSPECT_MAX_RANGE || totalNumber == 0) {
        return 0.f;
    }

    constexpr float LTM_PROSPECT_MULTIPLIERS[LTM_PROSPECT_MAX_RANGE] = { .05f,.3f,.4f,.15f,.1f };
    float indexTotal = LTM_PROSPECT_MULTIPLIERS[idx];
    float total = 0.f;
    for (size_t i = 0; i < totalNumber; i++) {
        total += LTM_PROSPECT_MULTIPLIERS[i];
    }
    return indexTotal / total;
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MM::Memory::GetTraceMentalMood(LTMConstNodeList const& trace, GetProspectMultiplierFunction funcPtr, float(&moods)[APF_Lib::NUM_MOOD_FEATURES])
{
    if (trace.empty()) {
        for (int i = 0; i < APF_Lib::NUM_MOOD_FEATURES; i++) {
            moods[i] = MIN_UNIT_VALUE;
        }
        return;
    }
    else if (trace.size() == 1) {
        APF_Lib::MentalState const& ms = trace[0]->GetAverageMentalState();
        ms.GetMood(moods);
        return;
    }

    size_t totalNum = trace.size();
    float* multipliers = new float[(int)totalNum];
    for (size_t i = 0; i < totalNum; i++) {
        multipliers[i] = funcPtr(i, totalNum);
    }

    float accumulateMultiplier = multipliers[0] + multipliers[1];
    APF_Lib::MentalState::CombineMentalMoods(trace[0]->GetAverageMentalState(), trace[1]->GetAverageMentalState(), multipliers[0] / accumulateMultiplier, moods);
    for (size_t i = 2; i < totalNum; i++) {
        float newMultiplier = multipliers[i];
        APF_Lib::MentalState tempMoods;
        tempMoods.SetMood(moods);
        APF_Lib::MentalState::CombineMentalMoods(tempMoods, trace[i]->GetAverageMentalState(), accumulateMultiplier / (accumulateMultiplier + newMultiplier), moods);
        accumulateMultiplier += newMultiplier;
    }

    delete[] multipliers;
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MM::Memory::GenerateMoodsSimilarMemoryFromTraces(Memory& result, std::vector<LTMConstNodeList> const& traces, APF_Lib::MentalState const& moods, GetProspectMultiplierFunction funcPtr)
{
    float const (&weights)[2] = { .6f,.4f };
    std::vector<LTMConstNodeList> ranked;
    RankTracesByMoodsSimilar(traces, moods, funcPtr, weights, ranked);
    constexpr float rankMultiplier[3] = { .5f,.3f,.2f };
    if (traces.empty()) {
        return;
    }

    GenerateProspectMemoryFromTrace(result, ranked[0], funcPtr);
    if (traces.size() == 1) {
        return;
    }

    Memory rank1;
    GenerateProspectMemoryFromTrace(rank1, ranked[1], funcPtr);
    Memory::GetCombinedMemory(result, result, rank1, rankMultiplier[0] / (rankMultiplier[1] + rankMultiplier[0]));
    if (traces.size() == 2) {
        return;
    }

    Memory rank2;
    GenerateProspectMemoryFromTrace(rank2, ranked[2], funcPtr);
    Memory::GetCombinedMemory(result, rank2, result, rankMultiplier[2]);
}

//////////////////////////////////////////////////////////////////////////
Memory& Memory::operator=(Memory const& newMem)
{
    m_mentalState = newMem.m_mentalState;
    m_gameState = newMem.m_gameState;
    m_isGarbage = newMem.m_isGarbage;
    if (newMem.m_selfState) {
        SetSelfState(*newMem.m_selfState);
    }

    return *this;
}

//////////////////////////////////////////////////////////////////////////
Memory::~Memory()
{
    if (m_selfState) {
        delete m_selfState;
        m_selfState = nullptr;
    }
}

//////////////////////////////////////////////////////////////////////////
void Memory::SetSelfState(ObjectState const& selfState)
{
    if (m_selfState) {
        delete m_selfState;
    }

    m_selfState = new ObjectState(selfState);
}

//////////////////////////////////////////////////////////////////////////
void Memory::GenerateGeneralMemoryFromTraces(Memory& general, std::vector<MemoryTrace> const& traces)
{
    std::vector<Memory> result;
    for (MemoryTrace const& trace : traces) {
        if (trace.empty()) {
            continue;
        }

        Memory traceGeneral = trace[0].memory;
        float deltaMultiplier = 1.f / (float)trace.size();
        float accumulateMultiplier = deltaMultiplier;
        for (size_t i = 1; i < trace.size(); i++) {
            Memory::GetCombinedMemory(traceGeneral, traceGeneral, trace[i].memory, accumulateMultiplier / (accumulateMultiplier + deltaMultiplier));
            accumulateMultiplier += deltaMultiplier;
        }
        result.push_back(traceGeneral);
    }

    if (result.empty()) {
        return;
    }

    general = result[0];
    float delta = 1.f / (float)result.size();
    float accumulateMultiplier = delta;
    for (size_t i = 1; i < result.size(); i++) {
        Memory::GetCombinedMemory(general, general, result[i], accumulateMultiplier / (accumulateMultiplier + delta));
        accumulateMultiplier += delta;
    }
}
