#pragma once

#include "Memory/GameTime.hpp"
#include "Memory/MemorySystem.hpp"
#include "Memory/ActorState.hpp"
#include "Memory/GameObject.hpp"

namespace APF_Lib {
    namespace MM {
        class GameActor : public GameObject
        {
        public:
            GameActor(GameTime const& birthTime);
            ~GameActor();

            void InitializeSTM(Memory const& mem);

            virtual void UpdateForSelfAction(GameEvent const& event);   //remember to self's m_currentAction
            virtual void UpdateForSensingObjects(std::vector<GameObject*>& objects);    //update m_sensedState with ObjectState from objects
            virtual void UpdateForSensingActors(std::vector<GameActor*>& actors);   //update m_sensedState with ActorState from actors
            virtual void UpdateCurrentActionToMemorySystem();   //appraise and remember for m_currentAction

            virtual void ConsolidateAndRefreshSTM();

            //Accessors
            GameEvent const& GetCurrentAction() const { return m_currentAction; }
            GameState const& GetSensedGameState() const {return m_sensedState;}
            MemorySystem* GetMemorySystem() const {return m_memSystem;}
            void  GenerateMemory(GameState const& gameState, Memory& generated) const;  //init memory with selfState and emotions
            MentalState GetCurrentMentalState() const;
            std::string GetPersonalityTextForUI() const;

        protected:
            MemorySystem* m_memSystem = nullptr;
            GameEvent m_currentAction;
            GameState m_sensedState;

            //TODO store all possible actions
            //std::vector<ActionName> m_actionChoices;
        };
    }
}

