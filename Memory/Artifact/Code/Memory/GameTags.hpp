#pragma once

#include <string>
#include <vector>

namespace APF_Lib {
    namespace MM {
        // all lower case, trimmed, unique tags
        struct GameTags
        {
        public:
            static float GetSimilarityForTags(GameTags const& tagA, GameTags const& tagB);

            GameTags() = default;
            ~GameTags() = default;
            GameTags(GameTags const& tags);

            void SetTags(std::string const& rawTagsString);
            void AddTag(std::string const& tag);    //uniquely set, clean tag
            void RemoveTag(std::string const& tag); //first clean tag

            bool HasTag(std::string const& tag) const;
            int  HasSameTags(std::vector<std::string> const& otherTags) const;

        public:
            std::vector<std::string> m_tags;            
            //TODO advanced tag definitions, float vector || bit maps; 
            //TODO tags hierarchy
        };
    }
}
