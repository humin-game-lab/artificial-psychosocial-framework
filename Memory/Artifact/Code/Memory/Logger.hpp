#pragma once

#include <string>
#include <vector>

namespace APF_Lib {
    namespace MM {
        class Logger
        {
        public:
            static Logger* sLogger;
            static Logger* GetLogger();

            virtual void LogError(char const* format, ...);
            virtual void LogInfo(std::string const& info);

            bool DumpLogToDisk(char const* filePath);

        protected:
            std::vector<std::string> m_logs;
        };
    }
}
