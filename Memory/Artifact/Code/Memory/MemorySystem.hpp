#pragma once

#include "Memory/ShortTermMemory.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Memory/MemoryQuerySystem.hpp"


namespace APF_Lib {
    class APF;

    namespace MM {
        class GameActor;

        class MemorySystem
        {
            friend class GameActor;

        public:
            static constexpr int STM_CONSOLIDATE_ADJACENT_NUM = 1;
            static constexpr float PROSPECT_MENTAL_COMBINE_DELTA = .1f;
            static APF* gMemoryAPF;
            static APF* GetMemoryAPF();
            static void ClearMemoryAPF();

            static void UpdateAttitudeForEmotionAndEmotionChange(int actorIdx, int objIdx, APF_Lib::MentalState const& emo, APF_Lib::MentalState const& delta);
            static void UpdatePraiseForEmotionAndEmotionChange(int actorIdx, int actionIdx, APF_Lib::MentalState const& emo, APF_Lib::MentalState const& delta);
            static void UpdateStateForEmotionAndEmotionChange(APF_Lib::MM::State* state, APF_Lib::MentalState const& totalEmo, APF_Lib::MentalState const& deltaEmo);

            MemorySystem(GameActor* owner);
            ~MemorySystem();

            virtual void Initialize();  //init function pointers, delegates
            void ClearGameObject(GameObject const* objectToDelete);

            virtual void AppraiseForEvent(MentalState& newEmotions, Memory const& curState, GameEvent const& action, Memory const& futureState);
            virtual void UpdateForNewEvent(GameEvent const& action);    //appraise based on memories and APF
            virtual void UpdateWithoutAppraisal(GameEvent const& action);
            virtual void UpdateForRepetition();
            virtual void UpdateForHourIncrease(GameTime const& time);
            virtual void UpdateForMinuteIncrease(GameTime const& time);

            virtual void ConsolidateSTMToLTM(); //remember all valid STM to LTM and Episodic

            //Search
            void SearchForSingleMemoryAndEvent(GetMemorySimilarity memFunc, Memory const& mem,
                GetEventSimilarity eventFunc, GameEvent const& event, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces);
            void SearchForQueryList(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& foundTraces);

            int GetOwnerIndex() const;
            ShortTermMemory* GetSTM() const {return m_STM;}
            LongTermMemory* GetLTM() const {return m_LTM;}

            //traces, current mental, function
            std::function<void(Memory&, std::vector<LTMConstNodeList> const&, APF_Lib::MentalState const&, 
                GetProspectMultiplierFunction)> m_getProspectFromTraces;    
            //actorIdx, action Idx, totalEmotion, deltaEmotion
            std::function<void(int, int, MentalState const&, MentalState const&)> m_updatePraiseForEmotions;   
            //actorIdx, object Idx, totalEmotion, deltaEmotion
            std::function<void(int, int, MentalState const&, MentalState const&)> m_updateAttitudeForEmotions;
            //state, totalEmotion, deltaEmotion
            std::function<void(State*, MentalState const&, MentalState const&)> m_updateStateForEmotions;
            GetProspectMultiplierFunction m_prospectMultiplierFunc;

        protected:
            virtual void UpdateAPFBBForSocial() {}    //TODO update APF for dynamic social relations; never called
            virtual void UpdateAPFForMemories(MemoryTrace const& trace);    //praise and attitudes

        protected:
            GameActor* m_owner = nullptr;
            ShortTermMemory* m_STM = nullptr;
            LongTermMemory* m_LTM = nullptr;
            MemoryClock m_clock;

            //instant working memory; book keeping
            Memory m_prospectMemory;
            GameEvent m_prospectAction;
        };
    }
}

