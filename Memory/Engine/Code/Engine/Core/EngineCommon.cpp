#include "Engine/Core/EngineCommon.hpp"

DevConsole* g_theConsole = nullptr;
NamedStrings* g_gameConfigBlackboard = nullptr;
EventSystem* g_theEvents = nullptr;
Network* g_theNetwork = nullptr;

eEndianMode gPlatformEndian = eEndianMode::INVALID_ENDIAN;

//////////////////////////////////////////////////////////////////////////
void InitializePlatformEndian()
{
    if (gPlatformEndian != eEndianMode::INVALID_ENDIAN) {
        return;
    }

    unsigned long long number = 0x12345678;
    unsigned char* firstPtr =(unsigned char*) &number;
    unsigned char first = *firstPtr;
    if (first == 0x12) {
        gPlatformEndian = eEndianMode::BIG_ENDIAN;
    }
    else if (first == 0x78) {
        gPlatformEndian = eEndianMode::LITTLE_ENDIAN;
    }
    else {
        gPlatformEndian=eEndianMode::INVALID_ENDIAN;
    }
}

//////////////////////////////////////////////////////////////////////////
unsigned long long int ChangeEndian64Bit(unsigned long long int const& value)
{
    unsigned long long int temp = value;
    temp = ((temp & 0x00000000000000ff) << 56u) |
        ((temp & 0x000000000000ff00) << 40u) |
        ((temp & 0x0000000000ff0000) << 24u) |
        ((temp & 0x00000000ff000000) << 8u) |
        ((temp & 0x000000ff00000000) >> 8u) |
        ((temp & 0x0000ff0000000000) >> 24u) |
        ((temp & 0x00ff000000000000) >> 40u) |
        ((temp & 0xff00000000000000) >> 56u);
    return temp;
}

//////////////////////////////////////////////////////////////////////////
unsigned int ChangeEndian32Bit(unsigned int const& value)
{
    unsigned int temp = value;
    temp = ((temp & 0x000000ff) << 24) |
        ((temp & 0x0000ff00) << 8) |
        ((temp & 0x00ff0000) >> 8) |
        ((temp & 0xff000000) >> 24);
    return temp;
}

//////////////////////////////////////////////////////////////////////////
unsigned short ChangeEndian16Bit(unsigned short const& value)
{
    unsigned short temp = value;
    temp = ((temp & 0x00ff) << 8) |
        ((temp & 0xff00) >> 8);
    return temp;
}
