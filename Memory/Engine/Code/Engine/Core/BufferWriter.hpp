#pragma once

#include "Engine/Core/EngineCommon.hpp"
#include <vector>
#include <string>

struct Vec2;
struct Vec3;
struct Rgba8;
struct AABB2;
struct IntVec2;
struct Vertex_PCU;

class BufferWriter
{
public:
    BufferWriter(std::vector<byte_t>& bytes);

    void SetEndianMode(eEndianMode const& newEndian);
    eEndianMode GetEndianMode() const;

    void AppendByte(unsigned char const& uChr);
    void AppendBool(bool value);
    void AppendChar(char const& chr);
    void AppendUShort(unsigned short const& uShort);
    void AppendShort(short const& sh);
    void AppendUint32(unsigned int const& uInt);
    void AppendInt32(int const& in);
    void AppendInt64(long long int const& int64);
    void AppendUInt64(unsigned long long int const& uInt64);
    void AppendFloat(float const& fl);
    void AppendDouble(double const& db);
    void AppendStringZeroTerminated(std::string const& str);
    void AppendStringAfter32BitLength(std::string const& str);
    void AppendIntVec2(IntVec2 const& vec);
    void AppendVec2(Vec2 const& vec);
    void AppendVec3(Vec3 const& vec);
    void AppendRgb(Rgba8 const& color);
    void AppendRgba8(Rgba8 const& color);
    void AppendAABB2(AABB2 const& bounds);
    void AppendVertexPCU(Vertex_PCU const& vert);

    void OverwriteUInt(unsigned int const& value, unsigned int const& posOffsetFromStart);

    unsigned int GetCurrentBufferSize() const;

private:
    std::vector<byte_t>& m_bytes;

    bool m_isReverseEndian = false;

    void AppendRawBytes(byte_t const* start, unsigned int length);
};