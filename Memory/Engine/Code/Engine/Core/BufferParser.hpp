#pragma once

#include "Engine/Core/EngineCommon.hpp"
#include <vector>
#include <string>

struct Vec2;
struct Vec3;
struct Rgba8;
struct IntVec2;
struct AABB2;
struct Vertex_PCU;

class BufferParser
{
public:
    BufferParser(byte_t const* dataPtr, unsigned int const& length);
    BufferParser(std::vector<byte_t> const& bytes);

    void SetEndianMode(eEndianMode const& newEndian);
    eEndianMode GetEndianMode() const;

    byte_t          ParseByte();
    bool            ParseBool();
    char            ParseChar();
    unsigned short  ParseUShort();
    short           ParseShort();
    unsigned int    ParseUint32();
    int             ParseInt32();
    long long int   ParseInt64();
    unsigned long long int  ParseUInt64();
    float           ParseFloat();
    double          ParseDouble();
    void            ParseStringZeroTerminated(std::string& result);
    void            ParseStringAfter32BitLength(std::string& result);
    Vec3            ParseVec3();
    Vec2            ParseVec2();
    IntVec2         ParseIntVec2();
    Rgba8           ParseRgba8();
    Rgba8           ParseRgb();
    AABB2           ParseAABB2();
    Vertex_PCU      ParseVertex_PCU();

    void JumpToReadPosition( unsigned int pos);

    unsigned int GetCurrentPos() const {return m_currentPos;}

private:
    byte_t const* m_data = nullptr;
    unsigned int m_length = 0;
    unsigned int m_currentPos = 0;

    bool m_isReverseEndian = false;

    //TODO safety checks
    byte_t const* ParseRawBytes(unsigned int length);    
};