#include "Engine/Core/BufferParser.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Core/Vertex_PCU.hpp"

//////////////////////////////////////////////////////////////////////////
BufferParser::BufferParser(byte_t const* dataPtr, unsigned int const& length)
    : m_data(dataPtr)
    , m_length(length)
{
    InitializePlatformEndian();
}

//////////////////////////////////////////////////////////////////////////
BufferParser::BufferParser(std::vector<byte_t> const& bytes)
    : m_data(bytes.data())
    , m_length((unsigned int)bytes.size())
{
    InitializePlatformEndian();
}

//////////////////////////////////////////////////////////////////////////
void BufferParser::SetEndianMode(eEndianMode const& newEndian)
{
    m_isReverseEndian = !(newEndian == gPlatformEndian);
}

//////////////////////////////////////////////////////////////////////////
eEndianMode BufferParser::GetEndianMode() const
{
    if (!m_isReverseEndian) {
        return gPlatformEndian;
    }

    switch (gPlatformEndian)
    {
    case INVALID_ENDIAN: return INVALID_ENDIAN;
    case LITTLE_ENDIAN:  return BIG_ENDIAN;
    case BIG_ENDIAN:     return LITTLE_ENDIAN;
    }

    return INVALID_ENDIAN;
}

//////////////////////////////////////////////////////////////////////////
byte_t BufferParser::ParseByte()
{
    byte_t const* result = ParseRawBytes(1);
    return *result;
}

//////////////////////////////////////////////////////////////////////////
bool BufferParser::ParseBool()
{
    byte_t b = ParseByte();
    return b!='\0';
}

//////////////////////////////////////////////////////////////////////////
char BufferParser::ParseChar()
{
    char const* result = (char const*)ParseRawBytes(1);
    return *result;
}

//////////////////////////////////////////////////////////////////////////
unsigned short BufferParser::ParseUShort()
{
    unsigned short const* result = (unsigned short const*)ParseRawBytes(sizeof(short));
    unsigned short temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian16Bit(temp);
    }
    return temp;
}

//////////////////////////////////////////////////////////////////////////
short BufferParser::ParseShort()
{
    unsigned short const* result = (unsigned short const*)ParseRawBytes(sizeof(short));
    unsigned short temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian16Bit(temp);
    }
    return (short)temp;
}

//////////////////////////////////////////////////////////////////////////
unsigned int BufferParser::ParseUint32()
{
    unsigned int const* result = (unsigned int const*)ParseRawBytes(sizeof(unsigned int));
    unsigned int temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian32Bit(temp);
    }
    return temp;
}

//////////////////////////////////////////////////////////////////////////
int BufferParser::ParseInt32()
{
    unsigned int const* result = (unsigned int const*)ParseRawBytes(sizeof(int));
    unsigned int temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian32Bit(temp);
    }
    return (int)temp;
}

//////////////////////////////////////////////////////////////////////////
long long int BufferParser::ParseInt64()
{
    unsigned long long int const* result = (unsigned long long int const*)ParseRawBytes(sizeof(unsigned long long int));
    unsigned long long int temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian64Bit(temp);
    }
    return (long long int)temp;
}

//////////////////////////////////////////////////////////////////////////
unsigned long long int BufferParser::ParseUInt64()
{
    unsigned long long int const* result = (unsigned long long int const*)ParseRawBytes(sizeof(unsigned long long int));
    unsigned long long int temp = *result;
    if (m_isReverseEndian) {
        temp = ChangeEndian64Bit(temp);
    }
    return temp;
}

//////////////////////////////////////////////////////////////////////////
float BufferParser::ParseFloat()
{
    unsigned int value = ParseUint32();
    float* temp = (float*)&value;
    return *temp;
}

//////////////////////////////////////////////////////////////////////////
double BufferParser::ParseDouble()
{
    unsigned long long int value = ParseUInt64();
    double* temp = (double*)&value;
    return *temp;
}

//////////////////////////////////////////////////////////////////////////
void BufferParser::ParseStringZeroTerminated(std::string& result)
{
    while (true) {
        char chr = ParseChar();
        if (chr == '\0') {
            return;
        }

        result += chr;
    }
}

//////////////////////////////////////////////////////////////////////////
void BufferParser::ParseStringAfter32BitLength(std::string& result)
{
    unsigned int num = ParseUint32();
    char const* start = (char const*)ParseRawBytes(num);
    result = std::string(start, num);
}

//////////////////////////////////////////////////////////////////////////
Vec3 BufferParser::ParseVec3()
{
    Vec3 result;
    result.x = ParseFloat();
    result.y = ParseFloat();
    result.z = ParseFloat();
    return result;
}

//////////////////////////////////////////////////////////////////////////
Vec2 BufferParser::ParseVec2()
{
    Vec2 result;
    result.x = ParseFloat();
    result.y = ParseFloat();
    return result;
}

//////////////////////////////////////////////////////////////////////////
IntVec2 BufferParser::ParseIntVec2()
{
    IntVec2 result;
    result.x = ParseInt32();
    result.y = ParseInt32();
    return result;
}

//////////////////////////////////////////////////////////////////////////
Rgba8 BufferParser::ParseRgba8()
{
    Rgba8 color;
    color.r = ParseByte();
    color.g = ParseByte();
    color.b = ParseByte();
    color.a = ParseByte();
    return color;
}

//////////////////////////////////////////////////////////////////////////
Rgba8 BufferParser::ParseRgb()
{
    Rgba8 color;
    color.r = ParseByte();
    color.g = ParseByte();
    color.b = ParseByte();
    return color;
}

//////////////////////////////////////////////////////////////////////////
AABB2 BufferParser::ParseAABB2()
{
    AABB2 result;
    result.mins = ParseVec2();
    result.maxs = ParseVec2();
    return result;
}

//////////////////////////////////////////////////////////////////////////
Vertex_PCU BufferParser::ParseVertex_PCU()
{
    Vertex_PCU vert;
    vert.position = ParseVec3();
    vert.tint = ParseRgba8();
    vert.uvTexCoords = ParseVec2();
    return vert;
}

//////////////////////////////////////////////////////////////////////////
void BufferParser::JumpToReadPosition(unsigned int pos)
{
    m_currentPos = pos;
    GUARANTEE_OR_DIE(m_currentPos>=0&&m_currentPos<=m_length, "ByfferParser jump to invalid");
}

//////////////////////////////////////////////////////////////////////////
byte_t const* BufferParser::ParseRawBytes(unsigned int length)
{
    GUARANTEE_OR_DIE(m_currentPos<m_length, "BufferParser read out of range");
    byte_t const* result = &(m_data[m_currentPos]);
    m_currentPos += length;
    GUARANTEE_OR_DIE(m_currentPos <= m_length, "BufferParser reading out of max length");
    return result;
}
