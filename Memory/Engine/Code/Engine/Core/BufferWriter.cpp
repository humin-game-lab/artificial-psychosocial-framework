#include "Engine/Core/BufferWriter.hpp"
#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/IntVec2.hpp"

//////////////////////////////////////////////////////////////////////////
BufferWriter::BufferWriter(std::vector<byte_t>& bytes)
    : m_bytes(bytes)
{
    InitializePlatformEndian();
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::SetEndianMode(eEndianMode const& newEndian)
{
    m_isReverseEndian = newEndian!=gPlatformEndian;
}

//////////////////////////////////////////////////////////////////////////
eEndianMode BufferWriter::GetEndianMode() const
{
    if (!m_isReverseEndian) {
        return gPlatformEndian;
    }

    switch (gPlatformEndian)
    {
    case INVALID_ENDIAN: return INVALID_ENDIAN;
    case LITTLE_ENDIAN:  return BIG_ENDIAN;
    case BIG_ENDIAN:     return LITTLE_ENDIAN;
    }

    return INVALID_ENDIAN;
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendByte(unsigned char const& uChr)
{
    m_bytes.push_back(uChr);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendBool(bool value)
{
    char chr = value?'1':'\0';
    AppendChar(chr);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendChar(char const& chr)
{
    byte_t const* ptr = (byte_t const*)&chr;
    m_bytes.push_back(*ptr);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendUShort(unsigned short const& uShort)
{
    unsigned short temp = uShort;
    if (m_isReverseEndian) {
        temp = ChangeEndian16Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(unsigned short));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendShort(short const& sh)
{
    unsigned short temp = (unsigned short)sh;
    if (m_isReverseEndian) {
        temp = ChangeEndian16Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(short));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendUint32(unsigned int const& uInt)
{
    unsigned int temp = uInt;
    if (m_isReverseEndian) {
        temp = ChangeEndian32Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(unsigned int));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendInt32(int const& in)
{
    unsigned int temp =(unsigned int)in;
    if (m_isReverseEndian) {
        temp = ChangeEndian32Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(int));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendInt64(long long int const& int64)
{
    unsigned long long int temp = (unsigned long long int) int64;
    if (m_isReverseEndian) {
        temp = ChangeEndian64Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(long long int));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendUInt64(unsigned long long int const& uInt64)
{
    unsigned long long int temp = uInt64;
    if (m_isReverseEndian) {
        temp = ChangeEndian64Bit(temp);
    }
    AppendRawBytes((byte_t const*)&temp, sizeof(unsigned long long int));
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendFloat(float const& fl)
{
    unsigned int const* value = (unsigned int const*)&fl;
    AppendUint32(*value);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendDouble(double const& db)
{
    unsigned long long int const* value = (unsigned long long int const*)&db;
    AppendUInt64(*value);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendStringZeroTerminated(std::string const& str)
{
    std::string temp = str+'\0';
    AppendRawBytes((byte_t const*)&temp[0], (unsigned int)temp.size());
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendStringAfter32BitLength(std::string const& str)
{
    unsigned int size = (unsigned int)str.size();
    AppendUint32(size);
    AppendRawBytes((byte_t const*)&str[0], (unsigned int)str.size());
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendIntVec2(IntVec2 const& vec)
{
    AppendInt32(vec.x);
    AppendInt32(vec.y);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendVec2(Vec2 const& vec)
{
    AppendFloat(vec.x);
    AppendFloat(vec.y);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendVec3(Vec3 const& vec)
{
    AppendFloat(vec.x);
    AppendFloat(vec.y);
    AppendFloat(vec.z);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendRgb(Rgba8 const& color)
{
    AppendChar(color.r);
    AppendChar(color.g);
    AppendChar(color.b);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendRgba8(Rgba8 const& color)
{
    AppendChar(color.r);
    AppendChar(color.g);
    AppendChar(color.b);
    AppendChar(color.a);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendAABB2(AABB2 const& bounds)
{
    AppendVec2(bounds.mins);
    AppendVec2(bounds.maxs);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendVertexPCU(Vertex_PCU const& vert)
{
    AppendVec3(vert.position);
    AppendRgba8(vert.tint);
    AppendVec2(vert.uvTexCoords);
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::OverwriteUInt(unsigned int const& value, unsigned int const& posOffsetFromStart)
{
    unsigned int num = sizeof(unsigned int);
    if (posOffsetFromStart > m_bytes.size()-num) {
        ERROR_RECOVERABLE("BufferWriter overwrites on non existing byte");
        return;
    }

    byte_t const* start = (byte_t const*)&value;
    for (unsigned int i = 0; i < num; i++) {
        m_bytes[posOffsetFromStart+i] = start[i];
    }
}

//////////////////////////////////////////////////////////////////////////
unsigned int BufferWriter::GetCurrentBufferSize() const
{
    return (unsigned int)m_bytes.size();
}

//////////////////////////////////////////////////////////////////////////
void BufferWriter::AppendRawBytes(byte_t const* start, unsigned int length)
{
    for (unsigned int i = 0; i < length; i++) {
        m_bytes.push_back(start[i]);
    }
}
