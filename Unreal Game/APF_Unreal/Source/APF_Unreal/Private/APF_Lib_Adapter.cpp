// Fill out your copyright notice in the Description page of Project Settings.


#include "APF_Lib_Adapter.h"
#include "Engine/Engine.h"

#define APF_DEBUG


UAPF_Lib_Adapter::UAPF_Lib_Adapter()
{
}


UAPF_Lib_Adapter::UAPF_Lib_Adapter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}


UAPF_Lib_Adapter::~UAPF_Lib_Adapter()
{
}


void UAPF_Lib_Adapter::APF_Initialize()
{
	if (GAPF_Instance == nullptr)
	{
		GAPF_Instance = new APF_Lib::APF();
	}

#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Initialized"));
#endif
}


void UAPF_Lib_Adapter::APF_Terminate()
{
	if (GAPF_Instance != nullptr)
	{
		delete GAPF_Instance;
		GAPF_Instance = nullptr;
	}

#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Terminated"));
#endif
}




void UAPF_Lib_Adapter::Initialize_Vocabulary()
{
	GAPF_Instance->InitializeVocabulary();

#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Initialize Vocabulary"));
#endif
}


void UAPF_Lib_Adapter::Terminate_Vocabulary()
{
	GAPF_Instance->TerminateVocabulary();

#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Terminate Vocabulary"));
#endif
}


void UAPF_Lib_Adapter::Sort_Vocabulary()
{
	GAPF_Instance->SortVocabulary();

#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Sort Vocabulary"));
#endif 
}


void UAPF_Lib_Adapter::Print_Vocabulary()
{
	GAPF_Instance->PrintAllVocabulary();
}


void UAPF_Lib_Adapter::Add_Object(const FName& label)
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label = TCHAR_TO_ANSI(*string_label);

	GAPF_Instance->AddObject(char_label);


#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add Object (label:");
	debug_message += string_label;
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif
}


void UAPF_Lib_Adapter::Add_Action(const FName& label, const float effect)
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label = TCHAR_TO_ANSI(*string_label);

	GAPF_Instance->AddAction(char_label, effect);

#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add Action (label:");
	debug_message += string_label;
	debug_message += " effect:";
	debug_message += FString::SanitizeFloat(effect);
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif  
}


void UAPF_Lib_Adapter::Add_ANPC(const FName& label, const float openness, const float conscientiousness, 
	const float extraversion, const float agreeableness, const float neuroticism)
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label= TCHAR_TO_ANSI(*string_label);

	GAPF_Instance->AddANPC(char_label, openness, conscientiousness, extraversion, agreeableness, neuroticism);


#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add ANPC (label:");
	debug_message += string_label;
	debug_message += " o:";
	debug_message += FString::SanitizeFloat(openness);
	debug_message += " c: ";
	debug_message += FString::SanitizeFloat(conscientiousness);
	debug_message += " e: ";
	debug_message += FString::SanitizeFloat(extraversion);
	debug_message += " a: ";
	debug_message += FString::SanitizeFloat(agreeableness);
	debug_message += " n: ";
	debug_message += FString::SanitizeFloat(neuroticism);
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif 
}


int UAPF_Lib_Adapter::Get_ANPC_Index(const FName& label) const
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label = TCHAR_TO_ANSI(*string_label);
	const int anpc_index = GAPF_Instance->GetANPCIndex(char_label);


#ifdef APF_DEBUG
	FString debug_message = TEXT("APF ANPC Index [");
	debug_message += string_label;
	debug_message += "] is ";
	debug_message += FString::FromInt(anpc_index);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif 

	return anpc_index;
}


int UAPF_Lib_Adapter::Get_Object_Index(const FName& label) const
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label = TCHAR_TO_ANSI(*string_label);
	const int object_index = GAPF_Instance->GetObjectIndex(char_label);


#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Object Index [");
	debug_message += string_label;
	debug_message += "] is ";
	debug_message += FString::FromInt(object_index);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif

	return object_index;
}


int UAPF_Lib_Adapter::Get_Action_Index(const FName& label) const
{
	const FString string_label = label.GetPlainNameString();
	const char* char_label = TCHAR_TO_ANSI(*string_label);
	const int action_index = GAPF_Instance->GetActionIndex(char_label);


#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Action Index [");
	debug_message += string_label;
	debug_message += "] is ";
	debug_message += FString::FromInt(action_index);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif

	return action_index;
}


void UAPF_Lib_Adapter::Initialize_Relations()
{
	GAPF_Instance->InitializeRelations();


#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Initialize Relations"));
#endif
}


void UAPF_Lib_Adapter::Terminate_Relations()
{
	GAPF_Instance->TerminateRelations();


#ifdef APF_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("APF Terminate Relations"));
#endif
}


void UAPF_Lib_Adapter::Print_Relations()
{
	GAPF_Instance->PrintAllRelations();
}


void UAPF_Lib_Adapter::Add_Attitude(const FName& anpc_label, const FName& object_label, const float valence,
	const float familiarity)
{
	const FString anpc_string_label = anpc_label.GetPlainNameString();
	const char* anpc_char_label = TCHAR_TO_ANSI(*anpc_string_label);

	const FString object_string_label = object_label.GetPlainNameString();
	const char* object_char_label = TCHAR_TO_ANSI(*anpc_string_label);

	GAPF_Instance->AddAttitude(anpc_char_label, object_char_label, valence, familiarity);

#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add Attitude (anpc_label: ");
	debug_message += anpc_string_label;
	debug_message += " object_label: ";
	debug_message += object_string_label;
	debug_message += " valence: ";
	debug_message += FString::SanitizeFloat(valence);
	debug_message += " familiarity: ";
	debug_message += FString::SanitizeFloat(familiarity);
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif
}


void UAPF_Lib_Adapter::Add_Praise(const FName& anpc_label, const FName& action_label, const float valence)
{
	const FString anpc_string_label = anpc_label.GetPlainNameString();
	const char* anpc_char_label = TCHAR_TO_ANSI(*anpc_string_label);

	const FString action_string_label = action_label.GetPlainNameString();
	const char* action_char_label = TCHAR_TO_ANSI(*action_string_label);

	GAPF_Instance->AddPraise(anpc_char_label, action_char_label, valence);

#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add Praise (anpc_label: ");
	debug_message += anpc_string_label;
	debug_message += " action_label: ";
	debug_message += action_string_label;
	debug_message += " valence: ";
	debug_message += FString::SanitizeFloat(valence);
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif
}


void UAPF_Lib_Adapter::Add_Social(const FName& from_anpc_label, const FName& to_anpc_label,
	const float liking, const float dominance, const float solidarity, const float familiarity)
{
	const FString from_anpc_string_label = from_anpc_label.GetPlainNameString();
	const char* from_anpc_char_label = TCHAR_TO_ANSI(*from_anpc_string_label);

	const FString to_anpc_string_label = to_anpc_label.GetPlainNameString();
	const char* to_anpc_char_label = TCHAR_TO_ANSI(*to_anpc_string_label);

	GAPF_Instance->AddSocial(from_anpc_char_label, to_anpc_char_label,
		liking, dominance, solidarity, familiarity);

#ifdef APF_DEBUG
	FString debug_message = TEXT("APF Add Social (from_anpc_label: ");
	debug_message += from_anpc_string_label;
	debug_message += " to_anpc_label: ";
	debug_message += to_anpc_string_label;
	debug_message += " l: ";
	debug_message += FString::SanitizeFloat(liking);
	debug_message += " d: ";
	debug_message += FString::SanitizeFloat(dominance);
	debug_message += " s: ";
	debug_message += FString::SanitizeFloat(solidarity);
	debug_message += " f: ";
	debug_message += FString::SanitizeFloat(familiarity);
	debug_message += " )";
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, debug_message);
#endif
}

