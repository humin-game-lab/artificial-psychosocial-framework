// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "APF_Lib.hpp"
#include "Engine/DataTable.h"
#include "APF_Lib_Adapter.generated.h"

static APF_Lib::APF* GAPF_Instance;


/**
 * 
 */
UCLASS(Blueprintable, BluePrintType)
class APF_UNREAL_API UAPF_Lib_Adapter : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_vocabulary_object = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_vocabulary_action = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_vocabulary_anpc = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_relations_attitude = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_relations_praise = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Vocabulary)
		UDataTable* p_relations_social = nullptr;

public:
	UAPF_Lib_Adapter();
	explicit UAPF_Lib_Adapter(const FObjectInitializer & ObjectInitializer);
	~UAPF_Lib_Adapter();

	
	//APF system functions
	UFUNCTION(BlueprintCallable, Category = APF)
		void APF_Initialize();

	UFUNCTION(BlueprintCallable, Category = APF)
		void APF_Terminate();


	
	//Vocabulary functions
	UFUNCTION(BlueprintCallable, Category = APF)
		void Initialize_Vocabulary();

	UFUNCTION(BlueprintCallable, Category = APF)
		void Terminate_Vocabulary();

	UFUNCTION(BlueprintCallable, Category = APF)
		void Sort_Vocabulary();

	UFUNCTION(BlueprintCallable, Category = APF)
		void Print_Vocabulary();
	
	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_Object(const FName& label);

	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_Action(const FName& label, float effect);

	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_ANPC(const FName& label, float openness, float conscientiousness, float extraversion, float agreeableness, float neuroticism);

	UFUNCTION(BlueprintCallable, Category = APF)
		int Get_ANPC_Index(const FName& label) const;

	UFUNCTION(BlueprintCallable, Category = APF)
		int Get_Object_Index(const FName& label) const;

	UFUNCTION(BlueprintCallable, Category = APF)
		int Get_Action_Index(const FName& label) const;



	UFUNCTION(BlueprintCallable, Category = APF)
		void Initialize_Relations();

	UFUNCTION(BlueprintCallable, Category = APF)
		void Terminate_Relations();

	UFUNCTION(BlueprintCallable, Category = APF)
		void Print_Relations();
	
	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_Attitude(const FName& anpc_label, const FName& object_label, const float valence, const float familiarity = 0.5f);

	//UFUNCTION(BlueprintCallable, Category = APF)
		//void Add_Attitude(const int anpc_idx, const int object_idx, const float valence, const float familiarity = 0.5f);


	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_Praise(const FName& anpc_label, const FName& action_label, float valence);

	//UFUNCTION(BlueprintCallable, Category = APF)
		//void Add_Praise(const int anpc_idx, const int action_idx, const float valence);


	UFUNCTION(BlueprintCallable, Category = APF)
		void Add_Social(const FName& from_anpc_label, const FName& to_anpc_label, float liking, float dominance, float solidarity, float familiarity);

	//UFUNCTION(BlueprintCallable, Category = APF)
		//void Add_Social(const int from_anpc_idx, const int to_anpc_idx, float liking, float dominance, float solidarity, float familiarity);
	
};


