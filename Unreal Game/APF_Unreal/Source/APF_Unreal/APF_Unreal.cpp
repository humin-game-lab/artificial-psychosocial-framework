// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "APF_Unreal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, APF_Unreal, "APF_Unreal" );
