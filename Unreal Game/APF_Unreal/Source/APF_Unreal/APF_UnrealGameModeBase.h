// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "APF_UnrealGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class APF_UNREAL_API AAPF_UnrealGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
