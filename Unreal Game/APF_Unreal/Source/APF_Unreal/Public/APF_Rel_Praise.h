// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Rel_Praise.generated.h"

/**
 * 
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Rel_Praise : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FAPF_Rel_Praise() :
		anpc_label("default"),
		action_label("default"),
		valence(0.5f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName anpc_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName action_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float valence;
	
};
