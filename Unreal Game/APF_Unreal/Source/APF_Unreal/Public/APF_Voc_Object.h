// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Voc_Object.generated.h"

/**
 *
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Voc_Object : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FAPF_Voc_Object() :
		label("default")
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF_Voc_Obj)
		FName label;
};
