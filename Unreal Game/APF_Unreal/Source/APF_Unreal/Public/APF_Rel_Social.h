// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Rel_Social.generated.h"

/**
 * 
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Rel_Social : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FAPF_Rel_Social() :
		anpc_from_label("default"),
		anpc_to_label("default"),
		liking(0.5f),
		dominance(0.5f),
		solidarity(0.5f),
		familiarity(0.5f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName anpc_from_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName anpc_to_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float liking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float dominance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float solidarity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float familiarity;
	
};
