// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Rel_Attitude.generated.h"

/**
 * 
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Rel_Attitude : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FAPF_Rel_Attitude() :
		anpc_label("default"),
		obj_label("default"),
		valence(0.5f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName anpc_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName obj_label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float valence;
	
};
