// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Voc_ANPC.generated.h"

/**
 *
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Voc_ANPC : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FAPF_Voc_ANPC() :
		label("default"),
		openness(0.5f),
		conscientiousness(0.5f),
		extraversion(0.5f),
		agreeableness(0.5f),
		neuoticism(0.5f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float openness;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float conscientiousness;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float extraversion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float agreeableness;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float neuoticism;
};
