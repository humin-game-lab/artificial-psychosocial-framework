// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "APF_Voc_Action.generated.h"

/**
 *
 */
USTRUCT(BluePrintType)
struct APF_UNREAL_API FAPF_Voc_Action : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FAPF_Voc_Action() :
		label("default"),
		effect(0.0f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		FName label;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = APF)
		float effect;
};
