// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APF_UNREAL_APF_Rel_Attitude_generated_h
#error "APF_Rel_Attitude.generated.h already included, missing '#pragma once' in APF_Rel_Attitude.h"
#endif
#define APF_UNREAL_APF_Rel_Attitude_generated_h

#define APF_Unreal_Source_APF_Unreal_Public_APF_Rel_Attitude_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAPF_Rel_Attitude_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> APF_UNREAL_API UScriptStruct* StaticStruct<struct FAPF_Rel_Attitude>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID APF_Unreal_Source_APF_Unreal_Public_APF_Rel_Attitude_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
