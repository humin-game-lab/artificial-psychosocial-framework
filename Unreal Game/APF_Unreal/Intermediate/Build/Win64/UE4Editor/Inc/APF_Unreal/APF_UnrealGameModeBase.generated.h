// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APF_UNREAL_APF_UnrealGameModeBase_generated_h
#error "APF_UnrealGameModeBase.generated.h already included, missing '#pragma once' in APF_UnrealGameModeBase.h"
#endif
#define APF_UNREAL_APF_UnrealGameModeBase_generated_h

#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_SPARSE_DATA
#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_RPC_WRAPPERS
#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAPF_UnrealGameModeBase(); \
	friend struct Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAPF_UnrealGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/APF_Unreal"), NO_API) \
	DECLARE_SERIALIZER(AAPF_UnrealGameModeBase)


#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAPF_UnrealGameModeBase(); \
	friend struct Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAPF_UnrealGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/APF_Unreal"), NO_API) \
	DECLARE_SERIALIZER(AAPF_UnrealGameModeBase)


#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAPF_UnrealGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAPF_UnrealGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAPF_UnrealGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAPF_UnrealGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAPF_UnrealGameModeBase(AAPF_UnrealGameModeBase&&); \
	NO_API AAPF_UnrealGameModeBase(const AAPF_UnrealGameModeBase&); \
public:


#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAPF_UnrealGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAPF_UnrealGameModeBase(AAPF_UnrealGameModeBase&&); \
	NO_API AAPF_UnrealGameModeBase(const AAPF_UnrealGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAPF_UnrealGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAPF_UnrealGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAPF_UnrealGameModeBase)


#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_12_PROLOG
#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_SPARSE_DATA \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_RPC_WRAPPERS \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_INCLASS \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_SPARSE_DATA \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APF_UNREAL_API UClass* StaticClass<class AAPF_UnrealGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID APF_Unreal_Source_APF_Unreal_APF_UnrealGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
