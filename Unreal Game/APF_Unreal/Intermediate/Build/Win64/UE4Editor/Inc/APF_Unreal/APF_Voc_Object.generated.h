// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APF_UNREAL_APF_Voc_Object_generated_h
#error "APF_Voc_Object.generated.h already included, missing '#pragma once' in APF_Voc_Object.h"
#endif
#define APF_UNREAL_APF_Voc_Object_generated_h

#define APF_Unreal_Source_APF_Unreal_Public_APF_Voc_Object_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> APF_UNREAL_API UScriptStruct* StaticStruct<struct FAPF_Voc_Object>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID APF_Unreal_Source_APF_Unreal_Public_APF_Voc_Object_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
