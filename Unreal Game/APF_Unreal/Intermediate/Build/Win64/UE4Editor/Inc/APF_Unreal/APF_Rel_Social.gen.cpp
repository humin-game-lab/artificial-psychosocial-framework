// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Public/APF_Rel_Social.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Rel_Social() {}
// Cross Module References
	APF_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FAPF_Rel_Social();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
// End Cross Module References
class UScriptStruct* FAPF_Rel_Social::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APF_UNREAL_API uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Social_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPF_Rel_Social, Z_Construct_UPackage__Script_APF_Unreal(), TEXT("APF_Rel_Social"), sizeof(FAPF_Rel_Social), Get_Z_Construct_UScriptStruct_FAPF_Rel_Social_Hash());
	}
	return Singleton;
}
template<> APF_UNREAL_API UScriptStruct* StaticStruct<FAPF_Rel_Social>()
{
	return FAPF_Rel_Social::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPF_Rel_Social(FAPF_Rel_Social::StaticStruct, TEXT("/Script/APF_Unreal"), TEXT("APF_Rel_Social"), false, nullptr, nullptr);
static struct FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Social
{
	FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Social()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("APF_Rel_Social")),new UScriptStruct::TCppStructOps<FAPF_Rel_Social>);
	}
} ScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Social;
	struct Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_familiarity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_familiarity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_solidarity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_solidarity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_dominance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_dominance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_liking_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_liking;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_anpc_to_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_anpc_to_label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_anpc_from_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_anpc_from_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPF_Rel_Social>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_familiarity_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_familiarity = { "familiarity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, familiarity), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_familiarity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_familiarity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_solidarity_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_solidarity = { "solidarity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, solidarity), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_solidarity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_solidarity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_dominance_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_dominance = { "dominance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, dominance), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_dominance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_dominance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_liking_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_liking = { "liking", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, liking), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_liking_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_liking_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_to_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_to_label = { "anpc_to_label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, anpc_to_label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_to_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_to_label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_from_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Social.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_from_label = { "anpc_from_label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Social, anpc_from_label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_from_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_from_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_familiarity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_solidarity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_dominance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_liking,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_to_label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::NewProp_anpc_from_label,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"APF_Rel_Social",
		sizeof(FAPF_Rel_Social),
		alignof(FAPF_Rel_Social),
		Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPF_Rel_Social()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Social_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_APF_Unreal();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APF_Rel_Social"), sizeof(FAPF_Rel_Social), Get_Z_Construct_UScriptStruct_FAPF_Rel_Social_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPF_Rel_Social_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Social_Hash() { return 1838328755U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
