// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Public/APF_Voc_ANPC.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Voc_ANPC() {}
// Cross Module References
	APF_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_ANPC();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
// End Cross Module References
class UScriptStruct* FAPF_Voc_ANPC::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APF_UNREAL_API uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPF_Voc_ANPC, Z_Construct_UPackage__Script_APF_Unreal(), TEXT("APF_Voc_ANPC"), sizeof(FAPF_Voc_ANPC), Get_Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Hash());
	}
	return Singleton;
}
template<> APF_UNREAL_API UScriptStruct* StaticStruct<FAPF_Voc_ANPC>()
{
	return FAPF_Voc_ANPC::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPF_Voc_ANPC(FAPF_Voc_ANPC::StaticStruct, TEXT("/Script/APF_Unreal"), TEXT("APF_Voc_ANPC"), false, nullptr, nullptr);
static struct FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_ANPC
{
	FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_ANPC()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("APF_Voc_ANPC")),new UScriptStruct::TCppStructOps<FAPF_Voc_ANPC>);
	}
} ScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_ANPC;
	struct Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_neuoticism_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_neuoticism;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_agreeableness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_agreeableness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_extraversion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_extraversion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_conscientiousness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_conscientiousness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_openness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_openness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPF_Voc_ANPC>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_neuoticism_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_neuoticism = { "neuoticism", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, neuoticism), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_neuoticism_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_neuoticism_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_agreeableness_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_agreeableness = { "agreeableness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, agreeableness), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_agreeableness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_agreeableness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_extraversion_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_extraversion = { "extraversion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, extraversion), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_extraversion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_extraversion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_conscientiousness_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_conscientiousness = { "conscientiousness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, conscientiousness), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_conscientiousness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_conscientiousness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_openness_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_openness = { "openness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, openness), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_openness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_openness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_ANPC.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_ANPC, label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_neuoticism,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_agreeableness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_extraversion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_conscientiousness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_openness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::NewProp_label,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"APF_Voc_ANPC",
		sizeof(FAPF_Voc_ANPC),
		alignof(FAPF_Voc_ANPC),
		Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_ANPC()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_APF_Unreal();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APF_Voc_ANPC"), sizeof(FAPF_Voc_ANPC), Get_Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_ANPC_Hash() { return 1096118183U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
