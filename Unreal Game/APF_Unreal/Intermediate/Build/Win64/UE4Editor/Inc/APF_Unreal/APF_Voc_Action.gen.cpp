// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Public/APF_Voc_Action.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Voc_Action() {}
// Cross Module References
	APF_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_Action();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
// End Cross Module References
class UScriptStruct* FAPF_Voc_Action::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APF_UNREAL_API uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Action_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPF_Voc_Action, Z_Construct_UPackage__Script_APF_Unreal(), TEXT("APF_Voc_Action"), sizeof(FAPF_Voc_Action), Get_Z_Construct_UScriptStruct_FAPF_Voc_Action_Hash());
	}
	return Singleton;
}
template<> APF_UNREAL_API UScriptStruct* StaticStruct<FAPF_Voc_Action>()
{
	return FAPF_Voc_Action::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPF_Voc_Action(FAPF_Voc_Action::StaticStruct, TEXT("/Script/APF_Unreal"), TEXT("APF_Voc_Action"), false, nullptr, nullptr);
static struct FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Action
{
	FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Action()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("APF_Voc_Action")),new UScriptStruct::TCppStructOps<FAPF_Voc_Action>);
	}
} ScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Action;
	struct Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_effect_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_effect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/APF_Voc_Action.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPF_Voc_Action>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_effect_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_Action.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_effect = { "effect", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_Action, effect), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_effect_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_effect_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Voc_Action.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_Action, label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_effect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::NewProp_label,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"APF_Voc_Action",
		sizeof(FAPF_Voc_Action),
		alignof(FAPF_Voc_Action),
		Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_Action()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Action_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_APF_Unreal();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APF_Voc_Action"), sizeof(FAPF_Voc_Action), Get_Z_Construct_UScriptStruct_FAPF_Voc_Action_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPF_Voc_Action_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Action_Hash() { return 844024581U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
