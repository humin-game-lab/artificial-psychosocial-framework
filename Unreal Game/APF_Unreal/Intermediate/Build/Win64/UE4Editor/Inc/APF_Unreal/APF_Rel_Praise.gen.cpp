// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Public/APF_Rel_Praise.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Rel_Praise() {}
// Cross Module References
	APF_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FAPF_Rel_Praise();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
// End Cross Module References
class UScriptStruct* FAPF_Rel_Praise::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APF_UNREAL_API uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Praise_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPF_Rel_Praise, Z_Construct_UPackage__Script_APF_Unreal(), TEXT("APF_Rel_Praise"), sizeof(FAPF_Rel_Praise), Get_Z_Construct_UScriptStruct_FAPF_Rel_Praise_Hash());
	}
	return Singleton;
}
template<> APF_UNREAL_API UScriptStruct* StaticStruct<FAPF_Rel_Praise>()
{
	return FAPF_Rel_Praise::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPF_Rel_Praise(FAPF_Rel_Praise::StaticStruct, TEXT("/Script/APF_Unreal"), TEXT("APF_Rel_Praise"), false, nullptr, nullptr);
static struct FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Praise
{
	FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Praise()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("APF_Rel_Praise")),new UScriptStruct::TCppStructOps<FAPF_Rel_Praise>);
	}
} ScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Rel_Praise;
	struct Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_valence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_valence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_action_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_action_label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_anpc_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_anpc_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Public/APF_Rel_Praise.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPF_Rel_Praise>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_valence_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Praise.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_valence = { "valence", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Praise, valence), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_valence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_valence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_action_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Praise.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_action_label = { "action_label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Praise, action_label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_action_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_action_label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_anpc_label_MetaData[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Public/APF_Rel_Praise.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_anpc_label = { "anpc_label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Rel_Praise, anpc_label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_anpc_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_anpc_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_valence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_action_label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::NewProp_anpc_label,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"APF_Rel_Praise",
		sizeof(FAPF_Rel_Praise),
		alignof(FAPF_Rel_Praise),
		Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPF_Rel_Praise()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Praise_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_APF_Unreal();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APF_Rel_Praise"), sizeof(FAPF_Rel_Praise), Get_Z_Construct_UScriptStruct_FAPF_Rel_Praise_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPF_Rel_Praise_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPF_Rel_Praise_Hash() { return 2191644309U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
