// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef APF_UNREAL_APF_Lib_Adapter_generated_h
#error "APF_Lib_Adapter.generated.h already included, missing '#pragma once' in APF_Lib_Adapter.h"
#endif
#define APF_UNREAL_APF_Lib_Adapter_generated_h

#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_SPARSE_DATA
#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAdd_Social) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_from_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_to_anpc_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_liking); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dominance); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_solidarity); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_familiarity); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Social(Z_Param_Out_from_anpc_label,Z_Param_Out_to_anpc_label,Z_Param_liking,Z_Param_dominance,Z_Param_solidarity,Z_Param_familiarity); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Praise) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_action_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_valence); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Praise(Z_Param_Out_anpc_label,Z_Param_Out_action_label,Z_Param_valence); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Attitude) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_object_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_valence); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_familiarity); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Attitude(Z_Param_Out_anpc_label,Z_Param_Out_object_label,Z_Param_valence,Z_Param_familiarity); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrint_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Print_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTerminate_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Terminate_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_Action_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_Action_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_Object_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_Object_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_ANPC_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_ANPC_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_ANPC) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_openness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_conscientiousness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_extraversion); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_agreeableness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_neuroticism); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_ANPC(Z_Param_Out_label,Z_Param_openness,Z_Param_conscientiousness,Z_Param_extraversion,Z_Param_agreeableness,Z_Param_neuroticism); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Action) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_effect); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Action(Z_Param_Out_label,Z_Param_effect); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Object) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Object(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrint_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Print_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSort_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sort_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTerminate_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Terminate_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAPF_Terminate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->APF_Terminate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAPF_Initialize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->APF_Initialize(); \
		P_NATIVE_END; \
	}


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAdd_Social) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_from_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_to_anpc_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_liking); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_dominance); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_solidarity); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_familiarity); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Social(Z_Param_Out_from_anpc_label,Z_Param_Out_to_anpc_label,Z_Param_liking,Z_Param_dominance,Z_Param_solidarity,Z_Param_familiarity); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Praise) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_action_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_valence); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Praise(Z_Param_Out_anpc_label,Z_Param_Out_action_label,Z_Param_valence); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Attitude) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_anpc_label); \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_object_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_valence); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_familiarity); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Attitude(Z_Param_Out_anpc_label,Z_Param_Out_object_label,Z_Param_valence,Z_Param_familiarity); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrint_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Print_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTerminate_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Terminate_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize_Relations) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize_Relations(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_Action_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_Action_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_Object_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_Object_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGet_ANPC_Index) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->Get_ANPC_Index(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_ANPC) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_openness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_conscientiousness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_extraversion); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_agreeableness); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_neuroticism); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_ANPC(Z_Param_Out_label,Z_Param_openness,Z_Param_conscientiousness,Z_Param_extraversion,Z_Param_agreeableness,Z_Param_neuroticism); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Action) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_effect); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Action(Z_Param_Out_label,Z_Param_effect); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAdd_Object) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_label); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Add_Object(Z_Param_Out_label); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrint_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Print_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSort_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Sort_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTerminate_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Terminate_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitialize_Vocabulary) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Initialize_Vocabulary(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAPF_Terminate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->APF_Terminate(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAPF_Initialize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->APF_Initialize(); \
		P_NATIVE_END; \
	}


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAPF_Lib_Adapter(); \
	friend struct Z_Construct_UClass_UAPF_Lib_Adapter_Statics; \
public: \
	DECLARE_CLASS(UAPF_Lib_Adapter, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/APF_Unreal"), NO_API) \
	DECLARE_SERIALIZER(UAPF_Lib_Adapter)


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUAPF_Lib_Adapter(); \
	friend struct Z_Construct_UClass_UAPF_Lib_Adapter_Statics; \
public: \
	DECLARE_CLASS(UAPF_Lib_Adapter, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/APF_Unreal"), NO_API) \
	DECLARE_SERIALIZER(UAPF_Lib_Adapter)


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAPF_Lib_Adapter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAPF_Lib_Adapter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAPF_Lib_Adapter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAPF_Lib_Adapter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAPF_Lib_Adapter(UAPF_Lib_Adapter&&); \
	NO_API UAPF_Lib_Adapter(const UAPF_Lib_Adapter&); \
public:


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAPF_Lib_Adapter(UAPF_Lib_Adapter&&); \
	NO_API UAPF_Lib_Adapter(const UAPF_Lib_Adapter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAPF_Lib_Adapter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAPF_Lib_Adapter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAPF_Lib_Adapter)


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_PRIVATE_PROPERTY_OFFSET
#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_17_PROLOG
#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_PRIVATE_PROPERTY_OFFSET \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_SPARSE_DATA \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_RPC_WRAPPERS \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_INCLASS \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_PRIVATE_PROPERTY_OFFSET \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_SPARSE_DATA \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_INCLASS_NO_PURE_DECLS \
	APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> APF_UNREAL_API UClass* StaticClass<class UAPF_Lib_Adapter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID APF_Unreal_Source_APF_Unreal_Classes_APF_Lib_Adapter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
