// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Public/APF_Voc_Object.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Voc_Object() {}
// Cross Module References
	APF_UNREAL_API UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_Object();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
// End Cross Module References
class UScriptStruct* FAPF_Voc_Object::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern APF_UNREAL_API uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Object_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAPF_Voc_Object, Z_Construct_UPackage__Script_APF_Unreal(), TEXT("APF_Voc_Object"), sizeof(FAPF_Voc_Object), Get_Z_Construct_UScriptStruct_FAPF_Voc_Object_Hash());
	}
	return Singleton;
}
template<> APF_UNREAL_API UScriptStruct* StaticStruct<FAPF_Voc_Object>()
{
	return FAPF_Voc_Object::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAPF_Voc_Object(FAPF_Voc_Object::StaticStruct, TEXT("/Script/APF_Unreal"), TEXT("APF_Voc_Object"), false, nullptr, nullptr);
static struct FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Object
{
	FScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Object()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("APF_Voc_Object")),new UScriptStruct::TCppStructOps<FAPF_Voc_Object>);
	}
} ScriptStruct_APF_Unreal_StaticRegisterNativesFAPF_Voc_Object;
	struct Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/APF_Voc_Object.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAPF_Voc_Object>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewProp_label_MetaData[] = {
		{ "Category", "APF_Voc_Obj" },
		{ "ModuleRelativePath", "Public/APF_Voc_Object.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAPF_Voc_Object, label), METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::NewProp_label,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"APF_Voc_Object",
		sizeof(FAPF_Voc_Object),
		alignof(FAPF_Voc_Object),
		Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAPF_Voc_Object()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Object_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_APF_Unreal();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("APF_Voc_Object"), sizeof(FAPF_Voc_Object), Get_Z_Construct_UScriptStruct_FAPF_Voc_Object_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAPF_Voc_Object_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAPF_Voc_Object_Hash() { return 901010699U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
