// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/APF_UnrealGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_UnrealGameModeBase() {}
// Cross Module References
	APF_UNREAL_API UClass* Z_Construct_UClass_AAPF_UnrealGameModeBase_NoRegister();
	APF_UNREAL_API UClass* Z_Construct_UClass_AAPF_UnrealGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
// End Cross Module References
	void AAPF_UnrealGameModeBase::StaticRegisterNativesAAPF_UnrealGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_AAPF_UnrealGameModeBase_NoRegister()
	{
		return AAPF_UnrealGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "APF_UnrealGameModeBase.h" },
		{ "ModuleRelativePath", "APF_UnrealGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAPF_UnrealGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::ClassParams = {
		&AAPF_UnrealGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAPF_UnrealGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAPF_UnrealGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAPF_UnrealGameModeBase, 2233700906);
	template<> APF_UNREAL_API UClass* StaticClass<AAPF_UnrealGameModeBase>()
	{
		return AAPF_UnrealGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAPF_UnrealGameModeBase(Z_Construct_UClass_AAPF_UnrealGameModeBase, &AAPF_UnrealGameModeBase::StaticClass, TEXT("/Script/APF_Unreal"), TEXT("AAPF_UnrealGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAPF_UnrealGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
