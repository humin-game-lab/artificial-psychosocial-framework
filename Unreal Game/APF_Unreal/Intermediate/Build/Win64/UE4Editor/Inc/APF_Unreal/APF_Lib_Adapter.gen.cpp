// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "APF_Unreal/Classes/APF_Lib_Adapter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPF_Lib_Adapter() {}
// Cross Module References
	APF_UNREAL_API UClass* Z_Construct_UClass_UAPF_Lib_Adapter_NoRegister();
	APF_UNREAL_API UClass* Z_Construct_UClass_UAPF_Lib_Adapter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_APF_Unreal();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations();
	APF_UNREAL_API UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References
	void UAPF_Lib_Adapter::StaticRegisterNativesUAPF_Lib_Adapter()
	{
		UClass* Class = UAPF_Lib_Adapter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Add_Action", &UAPF_Lib_Adapter::execAdd_Action },
			{ "Add_ANPC", &UAPF_Lib_Adapter::execAdd_ANPC },
			{ "Add_Attitude", &UAPF_Lib_Adapter::execAdd_Attitude },
			{ "Add_Object", &UAPF_Lib_Adapter::execAdd_Object },
			{ "Add_Praise", &UAPF_Lib_Adapter::execAdd_Praise },
			{ "Add_Social", &UAPF_Lib_Adapter::execAdd_Social },
			{ "APF_Initialize", &UAPF_Lib_Adapter::execAPF_Initialize },
			{ "APF_Terminate", &UAPF_Lib_Adapter::execAPF_Terminate },
			{ "Get_Action_Index", &UAPF_Lib_Adapter::execGet_Action_Index },
			{ "Get_ANPC_Index", &UAPF_Lib_Adapter::execGet_ANPC_Index },
			{ "Get_Object_Index", &UAPF_Lib_Adapter::execGet_Object_Index },
			{ "Initialize_Relations", &UAPF_Lib_Adapter::execInitialize_Relations },
			{ "Initialize_Vocabulary", &UAPF_Lib_Adapter::execInitialize_Vocabulary },
			{ "Print_Relations", &UAPF_Lib_Adapter::execPrint_Relations },
			{ "Print_Vocabulary", &UAPF_Lib_Adapter::execPrint_Vocabulary },
			{ "Sort_Vocabulary", &UAPF_Lib_Adapter::execSort_Vocabulary },
			{ "Terminate_Relations", &UAPF_Lib_Adapter::execTerminate_Relations },
			{ "Terminate_Vocabulary", &UAPF_Lib_Adapter::execTerminate_Vocabulary },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics
	{
		struct APF_Lib_Adapter_eventAdd_Action_Parms
		{
			FName label;
			float effect;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_effect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_effect = { "effect", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Action_Parms, effect), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Action_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_effect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_Action", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_Action_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics
	{
		struct APF_Lib_Adapter_eventAdd_ANPC_Parms
		{
			FName label;
			float openness;
			float conscientiousness;
			float extraversion;
			float agreeableness;
			float neuroticism;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_neuroticism;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_agreeableness;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_extraversion;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_conscientiousness;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_openness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_neuroticism = { "neuroticism", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, neuroticism), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_agreeableness = { "agreeableness", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, agreeableness), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_extraversion = { "extraversion", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, extraversion), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_conscientiousness = { "conscientiousness", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, conscientiousness), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_openness = { "openness", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, openness), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_ANPC_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_neuroticism,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_agreeableness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_extraversion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_conscientiousness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_openness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_ANPC", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_ANPC_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics
	{
		struct APF_Lib_Adapter_eventAdd_Attitude_Parms
		{
			FName anpc_label;
			FName object_label;
			float valence;
			float familiarity;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_familiarity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_familiarity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_valence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_valence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_object_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_object_label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_anpc_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_anpc_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_familiarity_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_familiarity = { "familiarity", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Attitude_Parms, familiarity), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_familiarity_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_familiarity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_valence_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_valence = { "valence", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Attitude_Parms, valence), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_valence_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_valence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_object_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_object_label = { "object_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Attitude_Parms, object_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_object_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_object_label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_anpc_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_anpc_label = { "anpc_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Attitude_Parms, anpc_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_anpc_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_anpc_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_familiarity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_valence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_object_label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::NewProp_anpc_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "CPP_Default_familiarity", "0.500000" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_Attitude", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_Attitude_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics
	{
		struct APF_Lib_Adapter_eventAdd_Object_Parms
		{
			FName label;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Object_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_Object", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_Object_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics
	{
		struct APF_Lib_Adapter_eventAdd_Praise_Parms
		{
			FName anpc_label;
			FName action_label;
			float valence;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_valence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_action_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_action_label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_anpc_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_anpc_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_valence = { "valence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Praise_Parms, valence), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_action_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_action_label = { "action_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Praise_Parms, action_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_action_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_action_label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_anpc_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_anpc_label = { "anpc_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Praise_Parms, anpc_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_anpc_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_anpc_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_valence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_action_label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::NewProp_anpc_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "Comment", "//UFUNCTION(BlueprintCallable, Category = APF)\n//void Add_Attitude(const int anpc_idx, const int object_idx, const float valence, const float familiarity = 0.5f);\n" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
		{ "ToolTip", "UFUNCTION(BlueprintCallable, Category = APF)\nvoid Add_Attitude(const int anpc_idx, const int object_idx, const float valence, const float familiarity = 0.5f);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_Praise", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_Praise_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics
	{
		struct APF_Lib_Adapter_eventAdd_Social_Parms
		{
			FName from_anpc_label;
			FName to_anpc_label;
			float liking;
			float dominance;
			float solidarity;
			float familiarity;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_familiarity;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_solidarity;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_dominance;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_liking;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_to_anpc_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_to_anpc_label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_from_anpc_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_from_anpc_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_familiarity = { "familiarity", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, familiarity), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_solidarity = { "solidarity", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, solidarity), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_dominance = { "dominance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, dominance), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_liking = { "liking", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, liking), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_to_anpc_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_to_anpc_label = { "to_anpc_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, to_anpc_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_to_anpc_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_to_anpc_label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_from_anpc_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_from_anpc_label = { "from_anpc_label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventAdd_Social_Parms, from_anpc_label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_from_anpc_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_from_anpc_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_familiarity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_solidarity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_dominance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_liking,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_to_anpc_label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::NewProp_from_anpc_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "Comment", "//UFUNCTION(BlueprintCallable, Category = APF)\n//void Add_Praise(const int anpc_idx, const int action_idx, const float valence);\n" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
		{ "ToolTip", "UFUNCTION(BlueprintCallable, Category = APF)\nvoid Add_Praise(const int anpc_idx, const int action_idx, const float valence);" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Add_Social", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventAdd_Social_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "Comment", "//APF system functions\n" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
		{ "ToolTip", "APF system functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "APF_Initialize", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "APF_Terminate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics
	{
		struct APF_Lib_Adapter_eventGet_Action_Index_Parms
		{
			FName label;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_Action_Index_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_Action_Index_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Get_Action_Index", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventGet_Action_Index_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics
	{
		struct APF_Lib_Adapter_eventGet_ANPC_Index_Parms
		{
			FName label;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_ANPC_Index_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_ANPC_Index_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Get_ANPC_Index", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventGet_ANPC_Index_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics
	{
		struct APF_Lib_Adapter_eventGet_Object_Index_Parms
		{
			FName label;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_label_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_label;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_Object_Index_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_label = { "label", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APF_Lib_Adapter_eventGet_Object_Index_Parms, label), METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_label_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_label_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::NewProp_label,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Get_Object_Index", nullptr, nullptr, sizeof(APF_Lib_Adapter_eventGet_Object_Index_Parms), Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Initialize_Relations", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "Comment", "//Vocabulary functions\n" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
		{ "ToolTip", "Vocabulary functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Initialize_Vocabulary", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Print_Relations", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Print_Vocabulary", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Sort_Vocabulary", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Terminate_Relations", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics::Function_MetaDataParams[] = {
		{ "Category", "APF" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAPF_Lib_Adapter, nullptr, "Terminate_Vocabulary", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAPF_Lib_Adapter_NoRegister()
	{
		return UAPF_Lib_Adapter::StaticClass();
	}
	struct Z_Construct_UClass_UAPF_Lib_Adapter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_relations_social_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_relations_social;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_relations_praise_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_relations_praise;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_relations_attitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_relations_attitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_vocabulary_anpc_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_vocabulary_anpc;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_vocabulary_action_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_vocabulary_action;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_p_vocabulary_object_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_p_vocabulary_object;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAPF_Lib_Adapter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_APF_Unreal,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAPF_Lib_Adapter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Action, "Add_Action" }, // 1747926027
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_ANPC, "Add_ANPC" }, // 3478071485
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Attitude, "Add_Attitude" }, // 3231811467
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Object, "Add_Object" }, // 3213019921
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Praise, "Add_Praise" }, // 4090252026
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Add_Social, "Add_Social" }, // 157294332
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Initialize, "APF_Initialize" }, // 3510435343
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_APF_Terminate, "APF_Terminate" }, // 593473451
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Action_Index, "Get_Action_Index" }, // 1805796285
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Get_ANPC_Index, "Get_ANPC_Index" }, // 356531912
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Get_Object_Index, "Get_Object_Index" }, // 1445322937
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Relations, "Initialize_Relations" }, // 2213650499
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Initialize_Vocabulary, "Initialize_Vocabulary" }, // 2898823946
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Relations, "Print_Relations" }, // 3896371879
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Print_Vocabulary, "Print_Vocabulary" }, // 777437521
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Sort_Vocabulary, "Sort_Vocabulary" }, // 3733687021
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Relations, "Terminate_Relations" }, // 3808702555
		{ &Z_Construct_UFunction_UAPF_Lib_Adapter_Terminate_Vocabulary, "Terminate_Vocabulary" }, // 720860801
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "APF_Lib_Adapter.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_social_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_social = { "p_relations_social", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_relations_social), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_social_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_social_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_praise_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_praise = { "p_relations_praise", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_relations_praise), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_praise_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_praise_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_attitude_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_attitude = { "p_relations_attitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_relations_attitude), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_attitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_attitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_anpc_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_anpc = { "p_vocabulary_anpc", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_vocabulary_anpc), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_anpc_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_anpc_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_action_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_action = { "p_vocabulary_action", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_vocabulary_action), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_action_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_action_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_object_MetaData[] = {
		{ "Category", "APF_Vocabulary" },
		{ "ModuleRelativePath", "Classes/APF_Lib_Adapter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_object = { "p_vocabulary_object", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPF_Lib_Adapter, p_vocabulary_object), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_object_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_object_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAPF_Lib_Adapter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_social,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_praise,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_relations_attitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_anpc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_action,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPF_Lib_Adapter_Statics::NewProp_p_vocabulary_object,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAPF_Lib_Adapter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAPF_Lib_Adapter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAPF_Lib_Adapter_Statics::ClassParams = {
		&UAPF_Lib_Adapter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UAPF_Lib_Adapter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAPF_Lib_Adapter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAPF_Lib_Adapter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAPF_Lib_Adapter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAPF_Lib_Adapter, 4047214464);
	template<> APF_UNREAL_API UClass* StaticClass<UAPF_Lib_Adapter>()
	{
		return UAPF_Lib_Adapter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAPF_Lib_Adapter(Z_Construct_UClass_UAPF_Lib_Adapter, &UAPF_Lib_Adapter::StaticClass, TEXT("/Script/APF_Unreal"), TEXT("UAPF_Lib_Adapter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAPF_Lib_Adapter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
