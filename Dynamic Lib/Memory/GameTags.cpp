#include "Memory/GameTags.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
float GameTags::GetSimilarityForTags(GameTags const& tagA, GameTags const& tagB)
{
    //TODO refine tag similarity compare
    //currently Jaccard coefficient
    int sameCount = tagA.HasSameTags(tagB.m_tags);
    int totalCount = (int)(tagA.m_tags.size() + tagB.m_tags.size());
    return (float)sameCount/(float)(totalCount-sameCount);
}

//////////////////////////////////////////////////////////////////////////
GameTags::GameTags(GameTags const& tags)
{
    m_tags = tags.m_tags;
}

//////////////////////////////////////////////////////////////////////////
// Delete space characters at beginning and end of string
static std::string Trim(std::string const& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && isspace(*it))
        it++;

    std::string::const_reverse_iterator rit = s.rbegin();
    while (rit.base() != it && isspace(*rit))
        rit++;

    return std::string(it, rit.base());
}

//////////////////////////////////////////////////////////////////////////
static std::string GetLowerCases(std::string const& s)
{
    std::string result = s;
    for (int chrIdx = 0; chrIdx < (int)s.length(); chrIdx++)
    {
        result[chrIdx] = (char)std::tolower(s[chrIdx]);
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
// Split string for every delimiter char
static std::vector<std::string> SplitStringOnDelimiter(std::string const& originalString, char delimiterToSplitOn)
{
    std::vector<std::string> splitResult;
    std::string leftString = originalString;
    size_t lastSplitPos = 0;

    for (size_t splitPos = 0;; )
    {
        splitPos = leftString.find(delimiterToSplitOn, lastSplitPos);
        std::string subString(leftString, lastSplitPos, splitPos - lastSplitPos);
        splitResult.push_back(subString);

        if (splitPos == std::string::npos)
            break;

        lastSplitPos = splitPos + 1;
    }

    return splitResult;
}

//////////////////////////////////////////////////////////////////////////
void GameTags::SetTags(std::string const& rawTagsString)
{
    std::vector<std::string> chunks = SplitStringOnDelimiter(rawTagsString,',');
    for (std::string& chunk : chunks) {
        std::string tag = GetLowerCases(Trim(chunk));
        AddTag(tag);
    }
}

//////////////////////////////////////////////////////////////////////////
void GameTags::AddTag(std::string const& tag)
{
    if (!HasTag(tag)) {
        m_tags.push_back(tag);
    }
}

//////////////////////////////////////////////////////////////////////////
void GameTags::RemoveTag(std::string const& tag)
{
    for (std::vector<std::string>::iterator it = m_tags.begin(); it != m_tags.end(); it++) {
        if (*it == tag) {
            m_tags.erase(it);
            return;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
bool GameTags::HasTag(std::string const& tag) const
{
    for (std::string const& t : m_tags) {
        if (t == tag) {
            return true;
        }
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////
int GameTags::HasSameTags(std::vector<std::string> const& otherTags) const
{
    int sameCount = 0;
    for (std::string const& tag : otherTags) {
        if (HasTag(tag)) {
            sameCount++;
        }
    }
    return sameCount;
}

