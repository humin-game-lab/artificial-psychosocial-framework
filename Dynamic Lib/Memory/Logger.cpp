#include "Memory/Logger.hpp"
#include "Memory/GameTags.hpp"
#include <stdarg.h>

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
// combine list of string with delimiter
static std::string CombineStringsWithDelimiter(std::vector<std::string> const& tags, char delimiter)
{
    std::string result;
    int length = (int)tags.size();
    for (int idx = 0; idx < length - 1; idx++)
    {
        result += tags[idx];
        result += delimiter;
    }
    if (length > 0) {
        result += tags[length - 1];
    }
    return result;
}


//////////////////////////////////////////////////////////////////////////
void Logger::LogError(char const* format, ...)
{
    constexpr int STRING_STACK_LENGTH = 2048;
    char textLiteral[STRING_STACK_LENGTH];
    va_list variableArgumentList;
    va_start(variableArgumentList, format);
    vsnprintf_s(textLiteral, STRING_STACK_LENGTH, _TRUNCATE, format, variableArgumentList);
    va_end(variableArgumentList);
    textLiteral[STRING_STACK_LENGTH - 1] = '\0';
    m_logs.push_back("ERROR: "+std::string(textLiteral));
}

//////////////////////////////////////////////////////////////////////////
void Logger::LogInfo(std::string const& info)
{
    m_logs.push_back("INFO: "+info);
}

//////////////////////////////////////////////////////////////////////////
bool Logger::DumpLogToDisk(char const* filePath)
{
    FILE* fp = nullptr;
    fopen_s(&fp, filePath, "wb");
    if (fp == nullptr) {
        return false;
    }

    std::string total = CombineStringsWithDelimiter(m_logs,'\n');
    fwrite(&total[0], sizeof(char), total.size(), fp);
    fclose(fp);
    return true;
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MM::Logger* APF_Lib::MM::Logger::sLogger = nullptr;

//////////////////////////////////////////////////////////////////////////
APF_Lib::MM::Logger* APF_Lib::MM::Logger::GetLogger()
{
    if (sLogger == nullptr) {
        sLogger = new APF_Lib::MM::Logger();
    }
    return sLogger;
}
