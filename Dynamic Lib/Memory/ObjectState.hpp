#pragma once

#include <string>
#include <vector>

namespace APF_Lib {
    
    namespace MM {
        // State that attached to APF vocabulary
        struct State
        {
            std::string name;
            float valenceDelta = 0.f;    //-1.f~1.f added to valence
        };

        // The actual state info attached to Object in game
        struct ObjectStateAspect
        {
        public:
            static float GetSimilarity(ObjectStateAspect const& aspectA, ObjectStateAspect const& aspectB);

            ObjectStateAspect(State const* s, float confident);

            bool operator==(ObjectStateAspect const& other) const;

        public:
            float confidence = 0.f;   //0-1 in confidence
            State const* state;           //directly use sPossibleStates memory    

            //TODO state temporal
            //GameTime startTime; GameTime interval; float deltaValence;
        };

        //states of an object
        struct ObjectState
        {
        public:
            static float GetSimilarity(ObjectState const& stateA, ObjectState const& stateB);
            static void CombineStates(ObjectState const& stateA, ObjectState const& stateB, float alpha, ObjectState& combined);
            static ObjectState GetMultipliedObjectState(ObjectState const& state, float alpha);

            ObjectState() = default;
            ~ObjectState() = default;
            ObjectState(ObjectState const& copyFrom);

            bool SetState(std::string const& stateName, float certainty);
            bool AddState(std::string const& stateName, float certainty);

            virtual void Mutate();
            virtual void Forget();
            void Clear();

            virtual bool  IsFaded() const;
            virtual float GetTotalDeltaValence() const;

            ObjectStateAspect* FindAspect(std::string const& stateName) const;
            ObjectStateAspect const* FindAspect(ObjectStateAspect const& aspect) const;

        public:
            std::vector<ObjectStateAspect> states;
        };

        //black boards for state
        struct StateInfo
        {
        public:
            static std::vector<StateInfo> sPossibleStates;            
            static StateInfo const* GetStateFromName(std::string const& name);

            StateInfo(std::string const& name, float delta);

        public:
            APF_Lib::MM::State state;
            //TODO extra information for state comparison: tags?
        };
    }
}
