#pragma once

#include <string>

namespace APF_Lib {
    namespace MM {
        struct GameTime
        {
        public:
            GameTime() = default;
            GameTime(int y, int mon, int d, int h, int mins, float sec);

            float GetTotalSeconds() const;
            float GetMutatePossibility() const;

            bool operator>(GameTime const& compare)  const;
            bool operator<(GameTime const& compare)  const;
            bool operator<=(GameTime const& compare) const;
            bool operator>=(GameTime const& compare) const;
            bool operator==(GameTime const& compare) const;
            bool operator!=(GameTime const& compare) const;

            GameTime const operator+(GameTime const& addOn) const;

            std::string ToString() const;

            static GameTime GetRandomGameTimeInRange(int yStart, int monStart, int dStart, int hStart, int minStart, float sStart,
                int yEnd, int monEnd, int dEnd, int hEnd, int minEnd, float sEnd);

            static const int MAX_MONTHS = 11;
            static const int MAX_DAYS = 29;
            static const int MAX_HOURS = 23;
            static const int MAX_MINUTES = 59;
            static const float MAX_SECONDS;
            static GameTime MIN_MUTATE_TIME;
            static GameTime MAX_MUTATE_TIME;
            static const float MIN_MUTATE_TIME_SECONDS;
            static const float MAX_MUTATE_TIME_SECONDS;

        public:
            int m_years = 0;
            int m_months = 0;
            int m_days = 0;
            int m_hours = 0;
            int m_minutes = 0;
            float m_seconds = 0.f;

        private:
            int CompareWith(GameTime const& compare) const; //0 equal, 1 greater, -1 smaller
        };
    }
}
