#pragma once

#include "Memory/Memory.hpp"

namespace APF_Lib {
    namespace MM {
        class QueryList;
        class LongTermMemory;

        class EpisodicMemory
        {
        public:
            static float GetCertaintyForMemoryTrace(MemoryTrace const& trace);  //used for episodic certainty calculation

            EpisodicMemory(MemoryTrace const& trace);

            void ClearGameObject(GameObject const* objectToDelete); //clear GameObject ptr to be deleted in episode
            void CleanDecayedMemory();
            virtual void Decay();
            virtual void Mutate();

            virtual bool IsFaded() const;   //the whole trace is faded
            virtual float GetMutateProbability() const;            
            virtual void GetMutatedTrace(LongTermMemory const& ltm, MemoryTrace& trace) const;  //mutate based on semantic trace

            bool ConsistWithQueryList(QueryList const& list) const;
            unsigned int GetTraceSize() const;
            void GetSelfQueryList(QueryList& list) const;   //compose simple query list of this episode
            MemoryAndEvent const* GetValidMemAndEventOfIndex(int idx) const;

        protected:
            MemoryTrace m_trace;    //episodic memory
            std::vector<bool> m_faded;  //whether each MemoryAndEvent is faded
            MemoryClock m_clock;    //existing time

            //specific information
            float m_certainty = 0.f;    //used to represent mutate possibility

            void AppendSelfToList(std::vector<MemoryTrace>& traces) const;
        };
    }
}
