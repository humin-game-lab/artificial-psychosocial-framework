#include "Trait.hpp"

//----------------------------------------------------------------------------------------------
APF_Lib::TraitPersonality::TraitPersonality() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitPersonality::~TraitPersonality() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitPersonality::TraitPersonality(const TraitPersonality& copy) :
	m_traits{ copy.m_traits[PERSONALITY_OPENNESS], copy.m_traits[PERSONALITY_CONSCIENTIOUSNESS],
	copy.m_traits[PERSONALITY_EXTROVERSION], copy.m_traits[PERSONALITY_AGREEABLENESS],
	copy.m_traits[PERSONALITY_NEUROTICISM] }
{
}


//----------------------------------------------------------------------------------------------
APF_Lib::TraitPersonality::TraitPersonality(const float o, const float c, const float e, const float a, const float n) :
	m_traits{ o, c, e, a, n }
{
}


//----------------------------------------------------------------------------------------------
float& APF_Lib::TraitPersonality::operator[](const int idx)
{
	return m_traits[idx];
}


//----------------------------------------------------------------------------------------------
APF_Lib::TraitSocialRole::TraitSocialRole() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitSocialRole::~TraitSocialRole() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitSocialRole::TraitSocialRole(const TraitSocialRole& copy) :
	m_traits{ copy.m_traits[SOCIAL_ROLE_LIKING], copy.m_traits[SOCIAL_ROLE_DOMINANCE],
	copy.m_traits[SOCIAL_ROLE_SOLIDARITY], copy.m_traits[SOCIAL_ROLE_FAMILIARITY]}
{
}


//----------------------------------------------------------------------------------------------
APF_Lib::TraitSocialRole::TraitSocialRole(const float l, const float d, const float s, const float f) :
	m_traits{ l, d, s, f }
{
}


//----------------------------------------------------------------------------------------------
float& APF_Lib::TraitSocialRole::operator[](const int idx)
{
	return m_traits[idx];
}


//----------------------------------------------------------------------------------------------
APF_Lib::TraitAttitude::TraitAttitude() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitAttitude::~TraitAttitude() = default;


//----------------------------------------------------------------------------------------------
APF_Lib::TraitAttitude::TraitAttitude(const TraitAttitude& copy):
	m_traits{ copy.m_traits[ATTITUDE_LIKING], copy.m_traits[ATTITUDE_FAMILIARITY]}
{
}


//----------------------------------------------------------------------------------------------
APF_Lib::TraitAttitude::TraitAttitude(float l, float f):
	m_traits{l, f}
{
}


//----------------------------------------------------------------------------------------------
float& APF_Lib::TraitAttitude::operator[](int idx)
{
	return m_traits[idx];
}
