#pragma once
#include <string>
#include "Trait.hpp"

namespace APF_Lib
{
	/**
	 * Base struct used to define everything in the virtual world for APF to understand
	 */
	struct Vocab
	{
		
	public:
		std::string m_name = "";

	public:		
		Vocab() = default;
		virtual ~Vocab() = default;
		Vocab(const Vocab& copy) = default;
		Vocab(Vocab&& move) = default;

		explicit Vocab(const char* name);
		explicit Vocab(const std::string& name);

		Vocab& operator= (const Vocab& copy);
		Vocab& operator= (Vocab&& move) = default;
		bool operator == (const Vocab& vocab_to_comp) const;
		bool operator != (const Vocab& vocab_to_comp) const;
		bool operator < (const Vocab& vocab_to_comp) const;
		bool operator <= (const Vocab& vocab_to_comp) const;
		bool operator > (const Vocab& vocab_to_comp) const;
		bool operator >= (const Vocab& vocab_to_comp) const;
		

		virtual std::string ToString();
	};
	
	
	/**
	 * Struct used to define all NPCs in the virtual world.
	 * APF will treat the NPCs as ANPC.
	 * ANPC are defined by their personality, and the emotional generation depends on these values
	 */
	struct ANPC : public virtual Vocab
	{

	public:
		TraitPersonality m_personality = TraitPersonality(0.f, 0.f, 0.f, 0.f, 0.f);

	public:
		ANPC() = default;
		virtual ~ANPC() = default;
		ANPC(const ANPC& copy) = default;
		ANPC(ANPC&& move) = default;
		ANPC& operator= (const ANPC& copy) = default;
		ANPC& operator= (ANPC&& move) = default;
		
		explicit ANPC(const char* name, const float o, const float c, const float e, const float a, const float n);

		virtual std::string ToString() override;
	};
	

	/**
	 *  Struct used to define all Objects in the virtual world
	 *	This uses the base struct of Vocabulary, and does not have any additional members
	 */
	struct Object : public virtual Vocab
	{
		
	public:
		Object() = default;
		virtual ~Object() = default;
		Object(const Object& copy) = default;
		Object(Object&& move) = default;
		Object& operator= (const Object& copy) = default;
		Object& operator= (Object&& move) = default;

		explicit Object(const char* name);
		explicit Object(const std::string& name);

	};

	/**
	 * Struct used to define all possible Actions in the virtual world
	 * Effect represents the negative (0.0) to positive (1.0) outcome towards the ANPC undergoing the action 
	 */
	struct Action : public virtual Vocab
	{

	public:
		float m_effect = 0.0f;
		
	public:
		Action() = default;
		virtual ~Action() = default;
		Action(const Action& copy) = default;
		Action(Action&& move) = default;
		Action& operator= (const Action& copy) = default;
		Action& operator= (Action&& move) = default;
		
		explicit Action(const char* name, const float effect);

		virtual std::string ToString() override;
	};


// 	/**
// 	 * Struct used to define all possible Adjectives an Object, Action, or ANPC can have
// 	 */
// 	struct Adjective : public virtual Vocab
// 	{
// 	public:
// 		Adjective() = default;
// 		~Adjective() = default;
// 		Adjective(const Adjective& copy) = default;
// 		Adjective(Adjective&& move) = default;
// 		Adjective& operator= (const Adjective& copy) = default;
// 		Adjective& operator= (Adjective&& move) = default;
// 	};
}
