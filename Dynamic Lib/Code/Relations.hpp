#pragma once
#include <string>
#include "Trait.hpp"

namespace APF_Lib
{
	struct Relation
	{
	public:
		std::string		m_from;
		std::string		m_to;

	public:
		Relation() = default;
		virtual ~Relation() = default;
		Relation(const Relation& copy) = default;
		Relation(Relation&& move) = default;

		
		explicit Relation(const char* from, const char* to);
		explicit Relation(const std::string& from, const std::string& to);

		
		Relation& operator= (const Relation& copy);
		Relation& operator= (Relation&& move) = default;

		
		virtual std::string ToString();
	};

	
	struct Attitude : public virtual Relation
	{
	public:
		Attitude() = default;
		virtual ~Attitude() = default;
		Attitude(const Attitude& copy) = default;
		Attitude(Attitude&& move) = default;
		Attitude& operator= (const Attitude& copy) = default;
		Attitude& operator= (Attitude&& move) = default;

		
		explicit Attitude(const char* anpc_name, const char* entity_name, const float liking, const float familiarity);
		explicit Attitude(const std::string& anpc_name, const std::string& entity_name, const float liking, const float familiarity);

		std::string ToString() override;
		
		TraitAttitude	m_attitude;
	};

	struct Praise : public virtual Relation
	{
	public:
		Praise() = default;
		virtual ~Praise() = default;
		Praise(const Praise& copy) = default;
		Praise(Praise&& move) = default;
		Praise& operator= (const Praise& copy) = default;
		Praise& operator= (Praise&& move) = default;

		
		explicit Praise(const char* anpc_name, const char* action_name, const float valence);
		explicit Praise(const std::string& anpc_name, const std::string& action_name, const float valence);

		std::string ToString() override;
		
		float	m_valence;
	};

	struct Social : public virtual Relation
	{
	public:
		Social() = default;
		virtual ~Social() = default;
		Social(const Social& copy) = default;
		Social(Social&& move) = default;
		Social& operator= (const Social& copy) = default;
		Social& operator= (Social&& move) = default;

		
		explicit Social(const char* anpc_name, const char* action_name, const float l, const float d, const float s, const float f);
		explicit Social(const std::string& anpc_name, const std::string& action_name, const float l, const float d, const float s, const float f);

		std::string ToString() override;
		
		TraitSocialRole	m_socialRole;
	};
}