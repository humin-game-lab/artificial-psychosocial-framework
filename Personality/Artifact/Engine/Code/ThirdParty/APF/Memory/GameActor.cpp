#include "ThirdParty/APF/Memory/GameActor.hpp"
#include "ThirdParty/APF/Memory/MemorySystem.hpp"
#include "ThirdParty/APF/APF/APF_Lib.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
GameActor::GameActor(GameTime const& birthTime)
    : GameObject(birthTime)
{
    m_memSystem = new MemorySystem(this, true);
}

//////////////////////////////////////////////////////////////////////////
GameActor::~GameActor()
{
    delete m_memSystem;
}

//////////////////////////////////////////////////////////////////////////
void GameActor::UpdateForSelfAction(GameEvent const& event)
{
    //TODO "Update self state according to action"
    m_currentAction = event;
    m_currentAction.m_owner = m_apfIdx;    
}

//////////////////////////////////////////////////////////////////////////
void GameActor::UpdateCurrentActionToMemorySystem()
{
    m_memSystem->UpdateForNewEvent(m_currentAction);
    m_currentAction.m_isGarbage = true;
}

//////////////////////////////////////////////////////////////////////////
void GameActor::ConsolidateAndRefreshSTM()
{
    m_memSystem->ConsolidateSTMToLTM();
    //m_memSystem->m_STM->InvalidateAll();
}

//////////////////////////////////////////////////////////////////////////
void GameActor::GenerateMemory(GameState const& gameState, Memory& generated) const
{
    MemorySystem::GetMemoryAPF()->GetANPCsMentalState(generated.m_mentalState, m_apfIdx);
    generated.m_gameState = gameState;
    if (m_selfState) {
        generated.SetSelfState(*m_selfState);
    }
    generated.m_isGarbage = false;
}

//////////////////////////////////////////////////////////////////////////
std::string GameActor::GetPersonalityTextForUI() const
{
    APF_Lib::ANPC npc = MemorySystem::GetMemoryAPF()->GetANPC(m_apfIdx);
    float* personality = &npc.m_personality[0];
    std::string result;
    result += "O: "+ std::to_string(personality[0])+"\n";
    result += "C: "+ std::to_string(personality[1])+"\n";
    result += "E: "+ std::to_string(personality[2]) + "\n";
    result += "A: "+ std::to_string(personality[3]) + "\n";
    result += "N: "+ std::to_string(personality[4]) + "\n";
    return result;
}

//////////////////////////////////////////////////////////////////////////
void GameActor::InitializeSTM(Memory const& mem)
{
    m_memSystem->m_STM->Initialize(mem);
}

//////////////////////////////////////////////////////////////////////////
void GameActor::UpdateForSensingObjects(std::vector<GameObject*>& objects)
{
    for (GameObject* ob : objects) {
        m_sensedState.UpdateStateForObject(ob);
    }
}

//////////////////////////////////////////////////////////////////////////
void APF_Lib::MM::GameActor::UpdateForSensingActors(std::vector<GameActor*>& actors)
{
    for (GameActor* a : actors) {
        if (a != this) {
            m_sensedState.UpdateStateForActor(a);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MentalState GameActor::GetCurrentMentalState() const
{
    APF_Lib::MentalState mentalState;
    MemorySystem::GetMemoryAPF()->GetANPCsMentalState(mentalState, m_apfIdx);
    return mentalState;
}
