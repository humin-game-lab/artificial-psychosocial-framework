#include "ThirdParty/APF/Memory/MemCommon.hpp"
#include <cmath>

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MM::RandomFloatInRange(const float start, const float end)
{
    return RandomZeroToOne() * (end - start) + start;
}

//////////////////////////////////////////////////////////////////////////
int APF_Lib::MM::RandomIntInRange(const int start, const int end)
{
    if (start > end) {
        return start;
    }
    return rand() % (end - start + 1) + start;
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::MM::PolarSmoothStart2(const float input)
{
    float sign = input > 0.f ? 1.f : -1.f;
    return input * input * sign;
}
