#pragma once

#include "ThirdParty/APF/Memory/EpisodicMemory.hpp"
#include <functional>

namespace APF_Lib {
    namespace MM {
        class LongTermMemory;
        class ShortTermMemory;
        class MemorySystem;
        class LTMEdge;
        class LTMNode;
        class QueryList;
        struct GameEvent;

        using GetTextForLTMNode = std::string(*)(LTMNode const& node);
        using GetTextForLTMEdge = std::string(*)(LTMEdge const& edge);

        //////////////////////////////////////////////////////////////////////////
        // Class Clarifications
        //////////////////////////////////////////////////////////////////////////
        //for generating trace in LTM
        struct LTMConstNodeAndEdge  
        {
            LTMNode const* node = nullptr;
            LTMEdge const* edge = nullptr;

            LTMConstNodeAndEdge(LTMNode const* n, LTMEdge const* e);
        };

        //////////////////////////////////////////////////////////////////////////
        class LTMEdge
        {
            friend class LongTermMemory;
            friend class LTMNode;

        public:
            static void MutateForEdges(LTMEdgeList const& list);
            static void ForgetForEdges(LTMEdgeList const& list);

            LTMEdge();

            virtual void Mutate();
            virtual void Forget();
            float UpdateProbability(float deltaProbability = 0.f);//return actual deltaProb after clamp

            float GetProbability() const { return m_probability; }
            size_t GetHappenTimes() const { return m_times; }
            std::string GetIDText() const;
            void GetEvent(GameEvent& event) const;

        protected:
            LTMNode* m_fromNode = nullptr;
            LTMNode* m_toNode = nullptr;
            GameEvent m_event;
            size_t m_times = 0; //happening times
            float m_probability = 0.f;

            unsigned int m_id = 0;
            static unsigned int sEdgeID;
        };

        //////////////////////////////////////////////////////////////////////////
        class LTMNode
        {
            friend class LongTermMemory;

        public:
            static constexpr int MENTAL_HISTORY_SIZE = 14;

            static void GetAllEdgesBetweenNodes(LTMNode const* fromNode, LTMNode const* toNode, std::vector<LTMEdge const*>& edges);
            static LTMEdge const* GetAnyActionBetweenNodes(LTMNode const* fromNode, LTMNode const* toNode);
            static LTMEdge const* GetMutateActionBetweenNodes(float mutatePossibility, LTMNode const* fromNode, LTMNode const* toNode);
            static void CombineMentalHistory(LTMNode const* nodeA, LTMNode const* nodeB, LTMNode* nodeCombined);
            static void CompositeCombineEmotionDistantNodes(LTMNode* nodeA, LTMNode* nodeB);
            static void GenerateProspectActionFromTraces(std::vector<LTMConstNodeList> const& traces, GameEvent& prospectAction);

            LTMNode();
            LTMNode(unsigned int id);

            virtual void MutateBySelf(GameTime const& time);
            virtual void Forget(GameTime const& time);
            virtual void AddMentalHistory(APF_Lib::MentalState const& newMental);   //maintain MENTAL_HISTORY_SIZE of mental history
            void RecalculateOutEdgeProbability();   //update edges according to happening times

            virtual float GetMutatePossibility() const;
            virtual float GetForgetPossibility() const;
            virtual float GetFrequencyPossibility() const;

            APF_Lib::MentalState& GetMentalState();
            APF_Lib::MentalState const& GetMentalState() const;
            APF_Lib::MentalState GetAverageMentalState() const;
            void GetAllDistinctToNodes(LTMConstNodeList& toNodes) const;
            Memory const& GetMemory() const { return m_memory; }
            std::string GetIDText() const;

        protected:
            Memory m_memory;
            LTMEdgeList m_outEdges;
            LTMEdgeList m_inEdges;
            MemoryClock m_clock;

            std::vector<APF_Lib::MentalState> m_mentalHistories;

            bool m_isBlank = false; //if blank, retrieve difficult
            bool m_isSaturated = false;

            unsigned int m_id = 0;    //also used in all nodes index
            static unsigned int sNodeID;
        };

        //////////////////////////////////////////////////////////////////////////
        class LongTermMemory
        {
            friend class MemorySystem;

        public:
            static float GetMemoryChainMentalScore(LTMConstNodeList const& nodeList, GetMentalStatesScore func, GetProspectMultiplierFunction multipliers);

            LongTermMemory(MemorySystem* sys);
            ~LongTermMemory() = default;

            LTMNode const* GetRandomEndingLTMNode(LTMNode const& fromNode) const;   //self is ending, choose non-blank only on fromNode
            LTMNode const* GetRandomStartingLTMNode(LTMNode const& toNode) const;   //self is start, choose non-blank only on toNode
            LTMNode const* GetRandomLTMNode(LTMNode const& fromNode, LTMNode const& toNode) const;

            void GenerateNonBlankTrace(LTMConstNodeList const& rawTrace, LTMConstNodeList& nonBlankTrace) const; //terminate when non-block not exist
            virtual void GenerateAndMutateTrace(LTMConstNodeList const& rawNonBlankTrace, MemoryTrace& mutateTrace) const;
            virtual void GenerateAndMutateTrace(std::vector<LTMConstNodeAndEdge> const& rawTrace, MemoryTrace& mutateTrace) const;

            virtual void RetrieveOneTrace(Memory const* newMemory, Memory const* currentMemory, GetMentalStatesScore func, LTMConstNodeList& trace) const;
            void RetrieveAllTracesWithinRange(Memory const* startMemory, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const;
            void RetrieveAllTracesWithinRange(Memory const* startMemory, GameEvent const& currentEvent, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const;
            void RetrieveAllTracesWithinRange(Memory const* startMemory, GetMemorySimilarity memFunc, GameEvent const& currentEvent, GetEventSimilarity eventFunc, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const;
            void RetrieveAllNodesOfQueryListWithinRange(QueryList const& list, unsigned int maxRange, std::vector<std::vector<LTMConstNodeAndEdge>>& ltmTraces) const;
            void RetrieveAllTracesOfQueryListWithinRange(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& traces) const;
            void RetrieveAllTracesOfQueryListInEpisodicMemory(QueryList const& list, std::vector<MemoryTrace>& traces) const;

            virtual void ActivateAndUpdateEpisodicMemories(QueryList const& list);
            virtual void UpdateEpisodicMemory(GameTime const& time);

            void UpdateForLTMNode(LTMNode* updatedNode);    //arouse & appraise relating nodes?
            void ClearGameObject(GameObject const* objectToDelete);
            void AddNewMemoryTrace(MemoryTrace const& trace);

            std::string GetTextForGraph(GetTextForLTMNode nodeFunc, GetTextForLTMEdge edgeFunc) const;

            std::function<void(LTMNode*, LTMNode*)> m_combineEmotionUnsimilarFunc;
            GetMemorySimilarity m_compareMemorySimilarityFunc;

        protected:
            //combine
            virtual void CombineLTMNodesRandomly(); //combine with FindAny
            void ReplaceOldNodeWithNewInGraph(LTMNode const* oldNode, LTMNode* newNode);
            void CombineLTMEdges(); //only combine same edge between same nodes

            virtual void RetrieveAllTraces(Memory const* newMemory, Memory const* currentMemory, std::vector<LTMConstNodeList>& traces) const;

            //add
            LTMNode* AddNewNode(Memory const& mem);
            //delete
            void SetNodeBlank(LTMNode* node);
            void ForceDeleteNode(LTMNode const* node);    //totally remove node from graph, no delete
            //choose
            void FilterMemoryChainWihAction(std::vector<LTMConstNodeList>& chain, GameEvent const& action) const;
            void FilterMemoryChainWihAction(std::vector<LTMConstNodeList>& chain, GameEvent const& action, GetEventSimilarity func) const;
            virtual void ChooseMemoryChainMental(std::vector<LTMConstNodeList> const& chains, GetMentalStatesScore func, LTMConstNodeList& ltmTrace) const;

            //traverse & find
            void GetAllPossiblePathsWhininRangeBFS(LTMConstNodeList const& starts, unsigned int maxRange, std::vector<LTMConstNodeList>& paths) const;
            void GetAllPossiblePathsBFS(LTMConstNodeList const& starts, LTMConstNodeList const& ends, std::vector<LTMConstNodeList>& paths) const;
            virtual LTMNode const* FindAnyMemory(Memory const* target, float similarity) const;
            void FindAllMemories(Memory const* target, float similarity, LTMConstNodeList& memories) const; //use default memory similar compare func
            void FindAllMemories(Memory const* target, GetMemorySimilarity memFunc, float similarity, LTMConstNodeList& memories) const;
            void FindAllMemories(GameEvent const& target, GetEventSimilarity eventFunc, float similarity, std::vector<LTMConstNodeAndEdge>& memories)const;
            void ProgressWithQueryList(QueryList const& list, std::vector<std::vector<LTMConstNodeAndEdge>>& routes, int maxRange) const;
            virtual void ExtendRoutesForLength(std::vector<std::vector<LTMConstNodeAndEdge>>& routes, int length) const;

            //TODO update episodes
            virtual void ActivateEpisodeToUpdateSelf(EpisodicMemory const& episode);

        protected:
            std::vector<LTMNode*> m_allNodes;   //semantic graph

            std::vector<EpisodicMemory> m_episodes; //episodic memory

            MemorySystem* m_memSystem = nullptr;
        };
    }
}
