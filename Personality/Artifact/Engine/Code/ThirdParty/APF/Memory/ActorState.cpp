#include "ThirdParty/APF/Memory/ActorState.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"

using namespace APF_Lib::MM;

//TODO("multi action at same time?");

static constexpr float ACTOR_BASIC_MAX_SIMILAR = .3f;
static constexpr float ACTOR_MENTAL_MAX_SIMILAR = .5f;
static constexpr float ACTOR_EVENT_MAX_SIMILAR = 1.f - ACTOR_BASIC_MAX_SIMILAR - ACTOR_MENTAL_MAX_SIMILAR;


//////////////////////////////////////////////////////////////////////////
float ActorState::GetSimilarity(ActorState const& stateA, ActorState const& stateB)
{
    float objSimilar = ObjectState::GetSimilarity(stateA.basicInfo, stateB.basicInfo);
    float mentalSimilar = APF_Lib::MentalState::GetSimilarityForMentalStates(stateA.mentalState, stateB.mentalState);
    float eventSimilar = GameEvent::GetSimilarity(stateA.currentEvent, stateB.currentEvent);
    return ACTOR_BASIC_MAX_SIMILAR*objSimilar + mentalSimilar*ACTOR_MENTAL_MAX_SIMILAR + 
            eventSimilar*ACTOR_EVENT_MAX_SIMILAR;
}

//////////////////////////////////////////////////////////////////////////
bool ActorState::CombineStates(ActorState const& stateA, ActorState const& stateB, float alpha, ActorState& combined)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    APF_Lib::MentalState::CombineMentalStates(stateA.mentalState, stateB.mentalState, alpha, combined.mentalState);
    ObjectState::CombineStates(stateA.basicInfo, stateB.basicInfo, alpha, combined.basicInfo);

    if (stateA.currentEvent != stateB.currentEvent) {
        //TODO directly use stateA's event
        combined.currentEvent = stateA.currentEvent;
        combined.currentEvent.m_certainty *= alpha;
        return false;
    }

    combined.currentEvent.m_certainty = Interpolate(stateA.currentEvent.m_certainty, stateB.currentEvent.m_certainty, alpha);
    return true;
}

//////////////////////////////////////////////////////////////////////////
void ActorState::GetMultipliedState(ActorState const& state, float alpha, ActorState& multiplied)
{
    alpha = ClampFloat(alpha, 0.f, 1.f);

    multiplied.basicInfo = ObjectState::GetMultipliedObjectState(state.basicInfo, alpha);
    multiplied.currentEvent = state.currentEvent;
    multiplied.currentEvent.m_certainty *= alpha;
    multiplied.mentalState = state.mentalState;
    multiplied.mentalState.MultiplyFactor(alpha);
}

//////////////////////////////////////////////////////////////////////////
void ActorState::Mutate()
{
    basicInfo.Mutate();
    mentalState.Mutate();
    currentEvent.Mutate();
}

//////////////////////////////////////////////////////////////////////////
void ActorState::Forget()
{
    basicInfo.Forget();
    mentalState.Decay();
    currentEvent.Forget();
}

//////////////////////////////////////////////////////////////////////////
bool ActorState::IsFaded() const
{
    return basicInfo.IsFaded() && mentalState.IsFaded() &&
        currentEvent.IsFaded();
}
