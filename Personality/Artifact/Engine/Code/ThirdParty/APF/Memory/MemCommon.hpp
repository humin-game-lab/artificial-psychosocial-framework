#pragma once

#include "ThirdParty/APF/APF/LibCommon.hpp"

namespace APF_Lib {
    //definitions
    struct MentalState;

    namespace MM {
        //memory system global constants
        constexpr float MENTAL_SIMILAR_THRESHOLD = 0.95f;
        constexpr float MEMORY_SIMILAR_THRESHOLD = .95f;     //bigger to say two memory similar
        constexpr float EVENT_SIMILAR_THRESHOLD = .9f;
        constexpr float STATE_CONFIDENCE_MUTATE_EXTENT = .04f;
        constexpr int LTM_PROSPECT_MAX_RANGE = 5;

        struct GameEvent;
        class Memory;
        using GetMemorySimilarity = float(*)(MM::Memory const& typeA, MM::Memory const& typeB);
        using GetEventSimilarity = float(*)(MM::GameEvent const& typeA, MM::GameEvent const& typeB);
        using GetMentalStatesScore = float(*)(MentalState const&);
        using GetProspectMultiplierFunction = float(*)(size_t idx, size_t total);

        inline bool NearlyEqual(const float valueA, const float valueB, const float epsilon) 
        {
            float delta = valueA-valueB;
            float absEps = AbsFloat(epsilon);
            return delta>=-absEps && delta<=absEps;
        }

        float RandomFloatInRange(const float start, const float end);
        int   RandomIntInRange(const int start, const int end);
        float PolarSmoothStart2(const float input);	//input[-1,1]
    }
}