#pragma once

#include "ThirdParty/APF/Memory/MemoryClock.hpp"
#include "ThirdParty/APF/Memory/GameState.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"
#include "ThirdParty/APF/APF/MentalState.hpp"

namespace APF_Lib
{
    namespace MM
    {
        class LTMEdge;
        class LTMNode;
        struct MemoryAndEvent;

        typedef std::vector<LTMEdge*> LTMEdgeList;
        typedef std::vector<LTMNode*> LTMNodeList;
        typedef std::vector<LTMNode const*> LTMConstNodeList;
        typedef std::vector<MemoryAndEvent> MemoryTrace;


        class Memory
        {
        public:
            static void GetTraceMentalMood(LTMConstNodeList const& trace, GetProspectMultiplierFunction funcPtr, float(&moods)[APF_Lib::NUM_MOOD_FEATURES]);
            static float GetFarSightMulplierForProspectGeneration(size_t idx, size_t totalNumber);
            static void RankTracesByMoodsSimilar(std::vector<LTMConstNodeList> const& traces, APF_Lib::MentalState const& moods, GetProspectMultiplierFunction funcPtr, float const (&weights)[2], std::vector<LTMConstNodeList>& ranked);
            static void GenerateProspectMemoryFromTrace(Memory& result, LTMConstNodeList const& trace, GetProspectMultiplierFunction funcPtr);
            static void GenerateMoodsSimilarMemoryFromTraces(Memory& result, std::vector<LTMConstNodeList> const& traces, APF_Lib::MentalState const& moods, GetProspectMultiplierFunction funcPtr);
            static void GenerateGeneralMemoryFromTraces(Memory& result, std::vector<MemoryTrace> const& traces);
            static void GetCombinedMemory(Memory& combined, Memory const& memA, Memory const& memB, float alhpa = .5f); //a=1, pure memA; a=0, pure memB
            static float GetSimilarity(Memory const& memA, Memory const& memB); //0-1, 1 the same

            Memory() = default;
            ~Memory();
            Memory(Memory const& newMem);
            
            Memory& operator=(Memory const& newMem);

            void SetSelfState(ObjectState const& selfState);

            virtual void Mutate();
            virtual void Forget();

            virtual bool IsFaded() const;

        public:
            ObjectState* m_selfState = nullptr;            //self
            APF_Lib::MentalState m_mentalState; //self
            GameState m_gameState;      //others info

            bool m_isGarbage = true;   //used in STM

            //possible self info
            //Vec2 m_position; etc.
        };

    }
}
