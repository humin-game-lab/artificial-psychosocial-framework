#include "ThirdParty/APF/Memory/MemorySystem.hpp"
#include "ThirdParty/APF/Memory/Logger.hpp"

#include <set>
#include <queue>
#include <cmath>

using namespace APF_Lib::MM;

static constexpr float LTM_EDGE_POSSIBILITY_MUTATE_EXTENT = .1f;

//////////////////////////////////////////////////////////////////////////
template<typename T>
T GetRandomItemInVector(std::vector<T> const& vec)
{
    if (vec.empty()) {
        return T(0);
    }

    int index = RandomIntInRange(0, (int)vec.size()-1);
    return vec[index];
}

//////////////////////////////////////////////////////////////////////////
static bool IsNodeInList(LTMNode const* node, LTMConstNodeList const& list)
{
    for (std::vector<LTMNode const*>::const_iterator it = list.cbegin(); it != list.cend(); it++) {
        if (*it == node) {
            return true;
        }
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////
LongTermMemory::LongTermMemory(MemorySystem* sys)
{
    m_combineEmotionUnsimilarFunc = LTMNode::CompositeCombineEmotionDistantNodes;
    m_compareMemorySimilarityFunc = Memory::GetSimilarity;
    m_memSystem = sys;
}

//////////////////////////////////////////////////////////////////////////
LTMNode const* LongTermMemory::GetRandomEndingLTMNode(LTMNode const& fromNode) const
{
    LTMConstNodeList result;
    for (LTMEdge const* edge : fromNode.m_outEdges) {
        LTMNode const* nextN = edge->m_toNode;
        if (!nextN->m_isBlank) {
            result.push_back(nextN);
        }
    }

    return GetRandomItemInVector<LTMNode const*>(result);
}

//////////////////////////////////////////////////////////////////////////
LTMNode const* LongTermMemory::GetRandomStartingLTMNode(LTMNode const& toNode) const
{
    LTMConstNodeList result;
    for (LTMEdge const* edge : toNode.m_inEdges) {
        LTMNode const* prevN = edge->m_fromNode;
        if (!prevN->m_isBlank) {
            result.push_back(prevN);
        }
    }

    return GetRandomItemInVector<LTMNode const*>(result);
}

//////////////////////////////////////////////////////////////////////////
LTMNode const* LongTermMemory::GetRandomLTMNode(LTMNode const& fromNode, LTMNode const& toNode) const
{
    LTMConstNodeList result;
    for (LTMEdge const* fromEdge : fromNode.m_outEdges) {
        LTMNode const* tempNode = fromEdge->m_toNode;
        if (!tempNode->m_isBlank) {
            for (LTMEdge const* toEdge : toNode.m_inEdges) {
                if (tempNode == toEdge->m_fromNode) {
                    result.push_back(tempNode);
                    break;
                }
            }
        }
    }

    return GetRandomItemInVector<LTMNode const*>(result);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::GenerateNonBlankTrace(LTMConstNodeList const& rawTrace, LTMConstNodeList& nonBlankTrace) const
{
    if (rawTrace.empty()) {
        return;
    }

    size_t totalNum = rawTrace.size();
    if (totalNum<=1) {
        if (rawTrace[0]->m_isBlank) {
            return;
        }
        else {
            nonBlankTrace = rawTrace;
            return;
        }
    }

    //deal with start
    nonBlankTrace.clear();
    LTMNode const* startNode = rawTrace[0];
    if(startNode->m_isBlank){
        startNode = GetRandomStartingLTMNode(*rawTrace[1]);
    }
    if(startNode!=nullptr){
        nonBlankTrace.push_back(startNode);
    }
    else {
        return;
    }
    //deal with middle
    for (size_t i = 1; i < totalNum - 1; i++) {
        LTMNode const* thisNode = rawTrace[i];
        if(thisNode->m_isBlank){
            thisNode = GetRandomLTMNode(*rawTrace[i - 1], *rawTrace[i + 1]);
        }
        if(thisNode!=nullptr){
            nonBlankTrace.push_back(thisNode);
        }
        else {
            return;
        }
    }
    //deal with end
    LTMNode const* endNode = rawTrace[totalNum-1];
    if(endNode->m_isBlank){
        endNode = GetRandomEndingLTMNode(*rawTrace[totalNum - 2]);
    }
    if(endNode!=nullptr){
        nonBlankTrace.push_back(endNode);
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::GenerateAndMutateTrace(LTMConstNodeList const& rawNonBlankTrace, MemoryTrace& mutateTrace) const
{
    for (size_t i = 0; i < rawNonBlankTrace.size(); i++) {
        LTMNode const* node = rawNonBlankTrace[i];
        MemoryAndEvent memEvent;
        memEvent.memory = node->m_memory;
        float mutatePossibility = node->GetMutatePossibility();
        if (RandomZeroToOne() <= mutatePossibility) {
            memEvent.memory.Mutate();
        }
        if (i != rawNonBlankTrace.size() - 1) { //add action
            LTMEdge const* edge = LTMNode::GetMutateActionBetweenNodes(mutatePossibility, node, rawNonBlankTrace[i+1]);
            if (edge == nullptr) {
                Logger::GetLogger()->LogError("Failed to find edge connecting nodes");
            }
            else {
                memEvent.selfEvent = edge->m_event;
            }
        }
        mutateTrace.push_back(memEvent);
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::GenerateAndMutateTrace(std::vector<LTMConstNodeAndEdge> const& rawTrace, MemoryTrace& mutateTrace) const
{
    //TODO ("how to deal with blank nodes within trace?");
    for (LTMConstNodeAndEdge const& nodeAndEdge : rawTrace) {
        LTMNode const* node = nodeAndEdge.node;
        if (node->m_isBlank) {
            break;
        }
        MemoryAndEvent memEvent;
        memEvent.memory = node->m_memory;
        float mutatePossibility = node->GetMutatePossibility();
        if (RandomZeroToOne() <= mutatePossibility) {
            memEvent.memory.Mutate();
        }
        if(nodeAndEdge.edge){
            memEvent.selfEvent = nodeAndEdge.edge->m_event;
            if (RandomZeroToOne() <= mutatePossibility) {
                memEvent.selfEvent.Mutate();
            }
        }
        mutateTrace.push_back(memEvent);
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveOneTrace(Memory const* newMemory, Memory const* currentMemory, GetMentalStatesScore func, LTMConstNodeList& trace) const
{
    std::vector<LTMConstNodeList> traces;
    RetrieveAllTraces(newMemory, currentMemory, traces);
    ChooseMemoryChainMental(traces, func, trace);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTraces(Memory const* newMemory, Memory const* currentMemory, std::vector<LTMConstNodeList>& traces) const
{
    LTMConstNodeList startNodes, endNodes;
    FindAllMemories(currentMemory, MEMORY_SIMILAR_THRESHOLD, startNodes);
    FindAllMemories(newMemory, MEMORY_SIMILAR_THRESHOLD, endNodes);
    GetAllPossiblePathsBFS(startNodes, endNodes, traces);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTracesWithinRange(Memory const* startMemory, GameEvent const& currentEvent, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const
{
    LTMConstNodeList startNodes;
    FindAllMemories(startMemory, MEMORY_SIMILAR_THRESHOLD, startNodes);
    GetAllPossiblePathsWhininRangeBFS(startNodes,maxRange, traces);
    FilterMemoryChainWihAction(traces, currentEvent);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTracesWithinRange(Memory const* startMemory, GetMemorySimilarity memFunc, GameEvent const& currentEvent, GetEventSimilarity eventFunc, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const
{
    LTMConstNodeList startNodes;
    FindAllMemories(startMemory, memFunc, MEMORY_SIMILAR_THRESHOLD, startNodes);
    GetAllPossiblePathsWhininRangeBFS(startNodes, maxRange, traces);
    FilterMemoryChainWihAction(traces, currentEvent, eventFunc);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTracesWithinRange(Memory const* startMemory, unsigned int maxRange, std::vector<LTMConstNodeList>& traces) const
{
    LTMConstNodeList startNodes;
    FindAllMemories(startMemory, MEMORY_SIMILAR_THRESHOLD, startNodes);
    GetAllPossiblePathsWhininRangeBFS(startNodes, maxRange, traces);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllNodesOfQueryListWithinRange(QueryList const& list, unsigned int maxRange, std::vector<std::vector<LTMConstNodeAndEdge>>& ltmTraces) const
{
    if (list.IsEmpty() || maxRange < 1) {
        return;
    }

    ltmTraces.clear();
    QueryNode* first = list.PopFront();
    //Prepare start memory nodes for query
    if (first->type == eQueryType::QUERY_MEMORY) {
        MemoryQueryNode* memNode = static_cast<MemoryQueryNode*>(first);
        LTMConstNodeList starts;
        FindAllMemories(&memNode->pair.second, memNode->pair.first, MEMORY_SIMILAR_THRESHOLD, starts);
        for (LTMNode const* start : starts) {
            std::vector<LTMConstNodeAndEdge> route;
            route.emplace_back(start, nullptr);
            ltmTraces.push_back(route);
        }
    }
    else if (first->type == eQueryType::QUERY_EVENT) {
        EventQueryNode* eventNode = static_cast<EventQueryNode*>(first);
        std::vector<LTMConstNodeAndEdge> findResult;
        FindAllMemories(eventNode->pair.second, eventNode->pair.first, EVENT_SIMILAR_THRESHOLD, findResult);
        for (LTMConstNodeAndEdge const& nodeAndEdge : findResult) {
            std::vector<LTMConstNodeAndEdge> tempRoute;
            tempRoute.push_back(nodeAndEdge);
            ltmTraces.push_back(tempRoute);
        }
    }
    else {  //first node invalid
        return;
    }

    ProgressWithQueryList(list, ltmTraces, (int)maxRange - 1);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTracesOfQueryListWithinRange(QueryList const& list, unsigned int maxRange, std::vector<MemoryTrace>& traces) const
{
    if (list.IsEmpty() || maxRange<1) {
        return;
    }

    std::vector<std::vector<LTMConstNodeAndEdge>> routes;
    RetrieveAllNodesOfQueryListWithinRange(list, maxRange, routes);

    //generate traces from routes
    for (std::vector<LTMConstNodeAndEdge> const& route : routes) {
        MemoryTrace temp;
        traces.push_back(temp);
        GenerateAndMutateTrace(route, traces.back());
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::RetrieveAllTracesOfQueryListInEpisodicMemory(QueryList const& list, std::vector<MemoryTrace>& traces) const
{
    if (list.IsEmpty()) {
        return;
    }

    for (EpisodicMemory const& episode : m_episodes) {
        if (episode.ConsistWithQueryList(list)) {
            MemoryTrace trace;
            traces.emplace_back(trace);
            episode.GetMutatedTrace(*this, traces.back());
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ActivateAndUpdateEpisodicMemories(QueryList const& list)
{
    for (EpisodicMemory const& episode : m_episodes) {
        if (episode.ConsistWithQueryList(list)) {
            ActivateEpisodeToUpdateSelf(episode);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::UpdateEpisodicMemory(GameTime const& time)
{
    for (size_t i=0;i<m_episodes.size();) {
        EpisodicMemory& episode = m_episodes[i];
        if (episode.IsFaded()) {
            m_episodes.erase(m_episodes.begin()+i);
            continue;
        }

        i++;
        episode.CleanDecayedMemory();
        if (time.m_hours % 4 == 0) {
            episode.Decay();

            if (time.m_hours % 8 == 0) {
                episode.Mutate();
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::CombineLTMNodesRandomly()
{
    std::set<std::string> combined; //whether already combined

    size_t totalNum = m_allNodes.size();
    for (size_t i=0;i<totalNum;i++) {
        LTMNode* n = m_allNodes[i];
        if (n == nullptr) {
            continue;
        }

        std::string nID = n->GetIDText();
        if (combined.find(nID) != combined.end()) {
            continue;
        }
        combined.insert(nID);

        //find random similar node to this node
        LTMNode const* similarN = FindAnyMemory(&n->m_memory, MEMORY_SIMILAR_THRESHOLD);
        if (similarN != nullptr) {
            if (combined.find(similarN->GetIDText()) != combined.end()) {
                continue;
            }
            combined.insert(similarN->GetIDText());

            if (APF_Lib::MentalState::GetSimilarityForMentalStates(n->m_memory.m_mentalState, similarN->m_memory.m_mentalState)>MENTAL_SIMILAR_THRESHOLD) {
                Memory newMem;
                Memory::GetCombinedMemory(newMem, n->m_memory, similarN->m_memory);
                newMem.m_isGarbage = false;
                LTMNode* newNode = nullptr;
                if (n->m_isSaturated || similarN->m_isSaturated) {
                    //combine not saturated to saturated
                    LTMNode const* oldNode = nullptr;
                    if (n->m_isSaturated) {
                        newNode = n;
                        oldNode = similarN;
                    }
                    else {  //if both n & similarN saturated, choose n to be deleted
                        newNode = const_cast<LTMNode*>(similarN);
                        oldNode = n;
                    }

                    newNode->m_memory = newMem;
                    ReplaceOldNodeWithNewInGraph(oldNode, newNode);
                    newNode->RecalculateOutEdgeProbability();
                    for (size_t j=LTMNode::MENTAL_HISTORY_SIZE/2;j<oldNode->m_mentalHistories.size();j++) {
                        newNode->AddMentalHistory(oldNode->m_mentalHistories[j]);
                    }
                    ForceDeleteNode(oldNode);
                    delete oldNode;
                }
                else if(!n->m_isSaturated && !similarN->m_isSaturated){
                    //combine both not saturated nodes
                    newNode = AddNewNode(newMem);
                    combined.insert(newNode->GetIDText());

                    ReplaceOldNodeWithNewInGraph(n, newNode);
                    ReplaceOldNodeWithNewInGraph(similarN, newNode);
                    newNode->RecalculateOutEdgeProbability();
                    LTMNode::CombineMentalHistory(n, similarN, newNode);

                    ForceDeleteNode(n);
                    ForceDeleteNode(similarN);
                    delete n;
                    delete similarN;
                }
            }
            else {//only update emotion histories
                LTMNode* otherN = const_cast<LTMNode*>(similarN);
                m_combineEmotionUnsimilarFunc(n,otherN);
            }
            
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ReplaceOldNodeWithNewInGraph(LTMNode const* oldNode, LTMNode* newNode)
{
    for (LTMEdge* edge : oldNode->m_inEdges) {
        edge->m_toNode = newNode;
        newNode->m_inEdges.push_back(edge);
    }

    for (LTMEdge* edge : oldNode->m_outEdges) {
        edge->m_fromNode = newNode;
        newNode->m_outEdges.push_back(edge);
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::CombineLTMEdges()
{
    for (LTMNode* n : m_allNodes) {
        if (n == nullptr) {
            continue;
        }

        for (size_t i=0;i<n->m_outEdges.size();i++) {
            LTMEdge* nextEdge = n->m_outEdges[i];
            LTMNode* nextN = nextEdge->m_toNode;
            
            //Get sum of redundant edges
            float totalNum = 1.f;
            float certaintySum = nextEdge->m_event.m_certainty;
            size_t timesSum = nextEdge->m_times;
            float probabilitySum = nextEdge->m_probability;
            for (size_t j = i+1; j < n->m_outEdges.size();) {
                LTMEdge* tempEdge = n->m_outEdges[j];
                if (tempEdge->m_toNode==nextN && tempEdge->m_event == nextEdge->m_event) {
                    totalNum+=1.f;
                    certaintySum += tempEdge->m_event.m_certainty;
                    timesSum+=tempEdge->m_times;
                    probabilitySum+=tempEdge->m_probability;

                    n->m_outEdges.erase(n->m_outEdges.begin()+j);
                }
                else {
                    j++;
                }
            }

            if (totalNum == 1.f) {
                continue;
            }

            //delete in next node
            for (size_t j = 0; j < nextN->m_inEdges.size();) {
                LTMEdge* tempEdge = nextN->m_inEdges[j];
                if (tempEdge != nextEdge && tempEdge->m_event == nextEdge->m_event && tempEdge->m_fromNode==n) {
                    nextN->m_inEdges.erase(nextN->m_inEdges.begin()+j);
                    delete tempEdge;
                }
                else {
                    j++;
                }
            }

            float numberFactor = 1.f/totalNum;
            if (numberFactor < 1.f) {   //update when redundant exists
                nextEdge->m_times = timesSum;
                nextEdge->m_probability = probabilitySum;
                nextEdge->m_event.m_certainty = certaintySum*numberFactor;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
LTMNode* LongTermMemory::AddNewNode(Memory const& mem)
{
    //Find empty spot for new node
    for (size_t i=0;i<m_allNodes.size();i++) {
        if (m_allNodes[i] == nullptr) {
            LTMNode* newNode = new LTMNode((unsigned int)i);
            newNode->m_memory = Memory(mem);
            m_allNodes[i] = newNode;
            return newNode;
        }
    }

    //Push back to all nodes
    LTMNode* newNode = new LTMNode();
    newNode->m_memory = Memory(mem);
    newNode->AddMentalHistory(mem.m_mentalState);
    m_allNodes.push_back(newNode);
    return newNode;
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::AddNewMemoryTrace(MemoryTrace const& trace)
{
    //TODO directly use exist nodes & edges when adding?
    LTMNode* lastNode = nullptr;
    LTMEdge* lastEdge = nullptr;

    //initialize total edges and nodes
    //try to filter out null start and end
    //assume input memory trace always valid:
    //(M+E)+(M+E)+...+(M+' /E')
    for (MemoryAndEvent const& memEvent : trace) {
        //Split game states into separate states
        Memory const& mem = memEvent.memory;
        if (mem.m_isGarbage) {   //not valid
            continue;
        }

        LTMNode* newNode = AddNewNode(mem);
        if (lastEdge != nullptr) {
            newNode->m_inEdges.push_back(lastEdge);
            lastEdge->m_toNode = newNode;
        }

        GameEvent const& e = memEvent.selfEvent;
        if(!e.m_isGarbage){
            //Add in LTMEdge
            LTMEdge* newEdge = new LTMEdge();
            newEdge->m_times = memEvent.repeatedTimes;
            newEdge->m_probability = 1.f;
            newEdge->m_event = e;
            newEdge->m_fromNode = newNode;
            newNode->m_outEdges.push_back(newEdge);

            lastEdge = newEdge;
        }

        lastNode = newNode;
    }

    m_episodes.emplace_back(trace);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::SetNodeBlank(LTMNode* node)
{
    if(!node->m_isSaturated){
        node->m_isBlank=true;
    }
    //TODO do something weird on blank node?
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ForceDeleteNode(LTMNode const* node)
{
    if (node->m_isSaturated) {  //saturated node won't fade
        Logger::GetLogger()->LogError("force deleting saturated LTMNode");
    }

    //delete from all nodes
    for (size_t i = 0; i < m_allNodes.size(); i++) {
        if (m_allNodes[i] == node) {
            m_allNodes[i] = nullptr;
        }
    }

    //delete from graph
    for (LTMNode* n : m_allNodes) {
        if (n == nullptr) {
            continue;
        }

        //out edges
        for (size_t i=0;i<n->m_outEdges.size();) {
            LTMEdge* edge = n->m_outEdges[i];
            LTMNode* toNode = edge->m_toNode;
            if (toNode == node) {
                delete edge; 
                n->m_outEdges.erase(n->m_outEdges.begin()+i);
            }
            else {
                i++;
            }                
        }

        //in edges
        for (size_t i=0; i<n->m_inEdges.size();) {
            LTMEdge* edge = n->m_inEdges[i];
            LTMNode* fromNode = edge->m_fromNode;
            if (fromNode == node) {
                delete edge;
                n->m_inEdges.erase(n->m_inEdges.begin()+i);
            }
            else {
                i++;
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::FilterMemoryChainWihAction(std::vector<LTMConstNodeList>& chain, GameEvent const& action) const
{
    for (size_t i=0;i<chain.size();) {
        LTMConstNodeList const& list = chain[i];
        bool foundAction = false;
        if (list.size()> 1) {
            LTMNode const* start = list[0];
            std::vector<LTMEdge const*> actions;
            LTMNode::GetAllEdgesBetweenNodes(start, list[1], actions);

            for (LTMEdge const* e : actions) {
                if (e->m_event == action) {
                    foundAction = true;
                    break;
                }
            }
        }
        
        if (foundAction) {
            i++;
        }
        else {
            chain.erase(chain.begin()+i);   //delete when not fingding action
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::FilterMemoryChainWihAction(std::vector<LTMConstNodeList>& chain, GameEvent const& action, GetEventSimilarity func) const
{
    for (size_t i = 0; i < chain.size();) {
        LTMConstNodeList const& list = chain[i];
        bool foundAction = false;
        if (list.size() > 1) {
            LTMNode const* start = list[0];
            std::vector<LTMEdge const*> actions;
            LTMNode::GetAllEdgesBetweenNodes(start, list[1], actions);

            for (LTMEdge const* e : actions) {
                if (func(e->m_event, action)>=EVENT_SIMILAR_THRESHOLD) {
                    foundAction = true;
                    break;
                }
            }
        }

        if (foundAction) {
            i++;
        }
        else {
            chain.erase(chain.begin() + i); //delete when not finding action
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ChooseMemoryChainMental(std::vector<LTMConstNodeList> const& chains, GetMentalStatesScore func, LTMConstNodeList& ltmTrace) const
{
    //TODO add other choose function: probability etc.
    float maxScore = -1.f;
    for (LTMConstNodeList const& chain : chains) {
        float score = GetMemoryChainMentalScore(chain, func, m_memSystem->m_prospectMultiplierFunc);
        if (score > maxScore) {
            ltmTrace = chain;
            maxScore = score;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::GetAllPossiblePathsWhininRangeBFS(LTMConstNodeList const& starts, unsigned int maxRange, std::vector<LTMConstNodeList>& paths) const
{
    if (maxRange < 2) {
        Logger::GetLogger()->LogError("Max range of get all possible paths should be larger than 1");
        return;
    }

    std::queue<LTMConstNodeList> q;
    //Prepare starts
    for (LTMNode const* start : starts) {
        LTMConstNodeList tempPath;
        tempPath.push_back(start);
        q.push(tempPath);
    }

    while (!q.empty()) {
        LTMConstNodeList curPath = q.front();
        q.pop();
        LTMNode const* lastNode = curPath.back();
        if (curPath.size()>=(size_t)maxRange || lastNode->m_outEdges.empty()) {
            paths.push_back(curPath);
            continue;
        }
        bool noNewBranch = true;
        LTMConstNodeList toNodes;
        lastNode->GetAllDistinctToNodes(toNodes);
        for (LTMNode const* toNode : toNodes) {
            if (!IsNodeInList(toNode, curPath)) {
                LTMConstNodeList newPath(curPath);
                newPath.push_back(toNode);
                q.push(newPath);
                noNewBranch = false;
            }            
        }
        if(noNewBranch){
            paths.push_back(curPath);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::GetAllPossiblePathsBFS(LTMConstNodeList const& starts, LTMConstNodeList const& ends, std::vector<LTMConstNodeList>& paths) const
{
    std::queue<LTMConstNodeList> q;
    for (LTMNode const* start : starts) {
        LTMConstNodeList tempPath;
        tempPath.push_back(start);
        q.push(tempPath);
    }

    size_t finishLength=SIZE_MAX;
    while (!q.empty()) {
        LTMConstNodeList curPath = q.front();
        q.pop();
        if (  curPath.size() > finishLength) {
            continue;
        }

        LTMNode const* lastNode = curPath.back();
        if (IsNodeInList(lastNode, ends)) {
            paths.push_back(curPath);
            if (finishLength == SIZE_MAX) {
                finishLength = curPath.size();
            }
        }

        LTMConstNodeList toNodes;
        lastNode->GetAllDistinctToNodes(toNodes);
        for (LTMNode const* nextN : toNodes) {
            if (!IsNodeInList(nextN, curPath)) {
                LTMConstNodeList newPath(curPath);
                newPath.push_back(nextN);
                q.push(newPath);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
LTMNode const* LongTermMemory::FindAnyMemory(Memory const* target, float similarity) const
{
    for (LTMNode const* node : m_allNodes) {
        if (node == nullptr || &node->m_memory==target) {
            continue;
        }
        
        if (m_compareMemorySimilarityFunc(node->m_memory, *target) >= similarity) {
            return node;
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::FindAllMemories(Memory const* target, float similarity, LTMConstNodeList& memories) const
{
    FindAllMemories(target,m_compareMemorySimilarityFunc,similarity, memories);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::FindAllMemories(Memory const* target, GetMemorySimilarity memFunc, float similarity, LTMConstNodeList& memories) const
{
    for (LTMNode const* node : m_allNodes) {
        if (node == nullptr || &node->m_memory == target) { //TODO filtered out self node?
            continue;
        }

        if (memFunc(node->m_memory, *target) >= similarity) {
            memories.push_back(node);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::FindAllMemories(GameEvent const& target, GetEventSimilarity eventFunc, float similarity, std::vector<LTMConstNodeAndEdge>& memories) const
{
    //assume than the LTM is valid: only starts with memory and ends with memory
    for (LTMNode const* node : m_allNodes) {
        if (node == nullptr) {
            continue;
        }

        for (LTMEdge const* edge : node->m_outEdges) {
            if (eventFunc(target, edge->m_event) > similarity) {
                memories.emplace_back(node,edge);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ProgressWithQueryList(QueryList const& list, std::vector<std::vector<LTMConstNodeAndEdge>>& routes, int maxRange) const
{
    if (maxRange <= 0 || routes.empty() || list.IsEmpty()) {
        if (list.IsEmpty() && maxRange > 0 && !routes.empty()) {    //query finish, fill in routes within max Range
            ExtendRoutesForLength(routes, maxRange);
        }

        return;
    }

    bool isLastMemory = routes[0].back().edge?false:true;
    QueryNode* curNode = list.PopFront();
    if (curNode->type == eQueryType::QUERY_INVALID) {
        return;
    }
    else if (curNode->type == eQueryType::QUERY_MEMORY) {
        MemoryQueryNode* memNode = static_cast<MemoryQueryNode*>(curNode);
        GetMemorySimilarity func = memNode->pair.first;
        Memory const& target = memNode->pair.second;

        if (isLastMemory) { //jump to next memory
            int totalSize = (int)routes.size();
            for (int i=0;i<totalSize;) {
                std::vector<LTMConstNodeAndEdge>& route=routes[i];
                LTMConstNodeAndEdge& last = route.back();
                std::vector<LTMConstNodeAndEdge> choices;
                for (LTMEdge const* edge : last.node->m_outEdges) {
                    LTMNode const* nextNode = edge->m_toNode;
                    if (func(target, nextNode->m_memory) > MEMORY_SIMILAR_THRESHOLD) {
                        last.edge = edge;
                        choices.emplace_back(nextNode, edge);
                    }
                }
                if (choices.empty()) {
                    routes.erase(routes.begin()+i);
                    totalSize--;
                }
                else {
                    for (int j = 0; j < (int)choices.size() - 1; j++) {
                        std::vector<LTMConstNodeAndEdge> newRoute(routes[i]);
                        LTMConstNodeAndEdge const& temp = choices[j];
                        newRoute.back().edge = temp.edge;
                        newRoute.emplace_back(temp.node, nullptr);
                        routes.push_back(newRoute);
                    }
                    routes[i].emplace_back(choices.back().node, nullptr);
                    i++;
                }
            }
        }
        else {  //test next memory
            int totalSize= (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<LTMConstNodeAndEdge>& route = routes[i];
                LTMConstNodeAndEdge& last = route.back();
                LTMNode const* nextNode = last.edge->m_toNode;
                if (nextNode && func(target, nextNode->m_memory) > MEMORY_SIMILAR_THRESHOLD) {
                    routes[i].emplace_back(nextNode,nullptr);
                    i++;
                }
                else {
                    routes.erase(routes.begin()+i);
                    totalSize--;
                }
            }
        }
    }
    else {  //event type
        EventQueryNode* eventNode = static_cast<EventQueryNode*>(curNode);
        GetEventSimilarity func = eventNode->pair.first;
        GameEvent const& event = eventNode->pair.second;

        if (isLastMemory) { //test next action
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<LTMConstNodeAndEdge>& route = routes[i];
                LTMConstNodeAndEdge& last = route.back();
                std::vector<LTMEdge const*> choices;
                for (LTMEdge const* edge : last.node->m_outEdges) {
                    if (func(event, edge->m_event) > EVENT_SIMILAR_THRESHOLD) {
                        last.edge = edge;
                        choices.push_back(edge);
                    }
                }
                if (choices.empty()) {  //delete current route
                    routes.erase(routes.begin()+i);
                    totalSize--;
                }
                else  {
                    for (int j = 0; j < (int)choices.size() - 1; j++) {
                        std::vector<LTMConstNodeAndEdge> newRoute(routes[i]);
                        newRoute.back().edge = choices[j];
                        routes.push_back(newRoute);
                    }
                    i++;
                }
            }
        }
        else {  //ignore next memory, jump to next action
            int totalSize = (int)routes.size();
            for (int i = 0; i < totalSize;) {
                std::vector<LTMConstNodeAndEdge>& route = routes[i];
                LTMConstNodeAndEdge& last = route.back();
                std::vector<LTMConstNodeAndEdge> choices;
                LTMNode const* nextNode = last.edge->m_toNode;
                for (LTMEdge const* edge : nextNode->m_outEdges) {
                    if (func(edge->m_event, event) > EVENT_SIMILAR_THRESHOLD) {
                        choices.emplace_back(nextNode, edge);
                    }
                }
                if (choices.empty()) {  //delete current route
                    routes.erase(routes.begin()+i);
                    totalSize--;
                }
                else {
                    for (int j = 0; j < (int)choices.size() - 1; j++) {
                        std::vector<LTMConstNodeAndEdge> newRoute(routes[i]);
                        newRoute.push_back(choices[j]);
                        routes.push_back(newRoute);
                    }
                    route.push_back(choices.back());
                    i++;
                }
            }
        }
    }

    ProgressWithQueryList(list, routes, maxRange-1);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ExtendRoutesForLength(std::vector<std::vector<LTMConstNodeAndEdge>>& routes, int length) const
{
    if (length < 0) {
        return;
    }

    int totalSize =(int)routes.size();
    for (int i = 0; i < totalSize;i++) {
        std::vector<LTMConstNodeAndEdge>& route = routes[i];
        LTMConstNodeAndEdge& last = route.back();
        std::vector<LTMConstNodeAndEdge> choices;
        for (LTMEdge const* edge : last.node->m_outEdges) {
            choices.emplace_back(edge->m_toNode, edge);
        }
        if (!choices.empty()) {
            for (int j = 0; j < (int)choices.size() - 1; j++) {
                std::vector<LTMConstNodeAndEdge> newRoute(routes[i]);
                LTMConstNodeAndEdge& secondLast = newRoute.back();
                newRoute.emplace_back(choices[j].node, nullptr);
                if (last.edge) {
                    newRoute.back().edge = choices[j].edge;
                }
                else {
                    secondLast.edge = choices[j].edge;
                }
                routes.push_back(newRoute);
            }
            //Add last NodeAndEdge of routes
            if (last.edge) {
                routes[i].emplace_back(choices.back().node, choices.back().edge);
            }
            else {
                last.edge = choices.back().edge;
                routes[i].emplace_back(choices.back().node,nullptr);
            }
        }
    }

    ExtendRoutesForLength(routes, length-1);
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ActivateEpisodeToUpdateSelf(EpisodicMemory const& episode)
{
    QueryList query;
    episode.GetSelfQueryList(query);
    unsigned int traceSize = episode.GetTraceSize();
    std::vector<std::vector<LTMConstNodeAndEdge>> similars;
    RetrieveAllNodesOfQueryListWithinRange(query, traceSize, similars);
    query.Clear();
    if (similars.empty()) {
        return;
    }

    int idx = RandomIntInRange(0, (int)similars.size()-1);
    std::vector<LTMConstNodeAndEdge>& target = similars[idx];

    int finalIndex = (int)target.size()-1;
    for (int i = 0; i < finalIndex; i++) {
        LTMConstNodeAndEdge const& futureNode = target[i+1];
		MemoryAndEvent const* memEvent = episode.GetValidMemAndEventOfIndex(i);
        MentalState reappraised;
        if (memEvent) {
            m_memSystem->AppraiseForEvent(reappraised, memEvent->memory, memEvent->selfEvent, futureNode.node->GetMemory());
        }
        else {
            LTMConstNodeAndEdge const& curNode = target[i];
            GameEvent event;
            curNode.edge->GetEvent(event);
            m_memSystem->AppraiseForEvent(reappraised, curNode.node->GetMemory(), event, futureNode.node->GetMemory());
        }
        LTMNode* newNode = const_cast<LTMNode*>(futureNode.node);
        newNode->AddMentalHistory(reappraised);
    }
}

//////////////////////////////////////////////////////////////////////////
float LongTermMemory::GetMemoryChainMentalScore(LTMConstNodeList const& nodeList, GetMentalStatesScore func, GetProspectMultiplierFunction multipliers)
{
    size_t prospect_max_range = (size_t)LTM_PROSPECT_MAX_RANGE;
    size_t total = nodeList.size() < prospect_max_range ? nodeList.size() : prospect_max_range;
    float* muls = new float[(int)nodeList.size()];
    for (size_t i = 0; i < nodeList.size(); i++) {
        muls[i] = multipliers(i, total);
    }

    float result = 0.f;
    for (size_t i = 0; i < nodeList.size(); i++) {
        LTMNode const* node = nodeList[i];
        result += func(node->GetAverageMentalState()) * muls[i];
    }

    delete[] muls;
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LongTermMemory::ClearGameObject(GameObject const* objectToDelete)
{
    //graph clear
    for (LTMNode* node : m_allNodes) {
        if (node == nullptr) {
            continue;
        }

        for (LTMEdge* edge : node->m_inEdges) {
            if (edge->m_event.m_target == objectToDelete) {
                edge->m_event.m_target=nullptr;
            }
        }

        for (LTMEdge* edge : node->m_outEdges) {
            if (edge->m_event.m_target == objectToDelete) {
                edge->m_event.m_target = nullptr;
            }
        }
    }

    //episodic clear
    for (EpisodicMemory& episode : m_episodes) {
        episode.ClearGameObject(objectToDelete);
    }
}

//////////////////////////////////////////////////////////////////////////
std::string APF_Lib::MM::LongTermMemory::GetTextForGraph(GetTextForLTMNode nodeFunc, GetTextForLTMEdge edgeFunc) const
{
    std::string result = "digraph LTM{\ngraph[rankdir=\"LR\"];\n";
    for (LTMNode* n : m_allNodes) {
        if (n == nullptr) {
            continue;
        }

        std::string nID = n->GetIDText();
        result += nodeFunc(*n);
        for (LTMEdge* e : n->m_outEdges) {
            result += edgeFunc(*e);
            std::string eID = e->GetIDText();
            result += nID + " -> " + eID + "[color=Green]\n";
            LTMNode* toNode = e->m_toNode;
            result += eID + " -> " + toNode->GetIDText() + "[color=Blue]\n";
        }
    }
    result += "}";
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LTMEdge::MutateForEdges(LTMEdgeList const& list)
{
    for (size_t i = 0; i < list.size(); i++) {
        LTMEdge* edge = list[i];
        edge->Mutate();

        float deltaProbability = (RandomZeroToOne() - .5f) * LTM_EDGE_POSSIBILITY_MUTATE_EXTENT;
        deltaProbability = edge->UpdateProbability(deltaProbability);
        LTMEdge* edgeToChange = GetRandomItemInVector<LTMEdge*>(list);
        edgeToChange->UpdateProbability(-deltaProbability);
    }
}

//////////////////////////////////////////////////////////////////////////
void LTMEdge::ForgetForEdges(LTMEdgeList const& list)
{
    for (size_t i = 0; i < list.size(); i++) {
        LTMEdge* edge = list[i];
        edge->Forget();

        float deltaProbability = AbsFloat(RandomZeroToOne() - .5f) * LTM_EDGE_POSSIBILITY_MUTATE_EXTENT;
        deltaProbability = edge->UpdateProbability(-deltaProbability);
        LTMEdge* edgeToChange = GetRandomItemInVector<LTMEdge*>(list);
        edgeToChange->UpdateProbability(deltaProbability);
    }
}

//////////////////////////////////////////////////////////////////////////
LTMEdge::LTMEdge()
{
    m_id=sEdgeID++;
}

//////////////////////////////////////////////////////////////////////////
void LTMEdge::Mutate()
{
    m_event.Mutate();    
}

//////////////////////////////////////////////////////////////////////////
void LTMEdge::Forget()
{
    m_event.Forget();
}

float LTMEdge::UpdateProbability(float deltaProbability /*= 0.f*/)
{
    float prevProb = m_probability;
    m_probability += deltaProbability;
    m_probability = ClampFloat(m_probability, 0.f, MAX_UNIT_VALUE);
    return m_probability - prevProb;
}

//////////////////////////////////////////////////////////////////////////
std::string LTMEdge::GetIDText() const
{
    std::string result= "E"+std::to_string(m_id);
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LTMEdge::GetEvent(GameEvent& event) const
{
    event = m_event;
}

unsigned int LTMEdge::sEdgeID = 0;

//////////////////////////////////////////////////////////////////////////
void LTMNode::GetAllEdgesBetweenNodes(LTMNode const* fromNode, LTMNode const* toNode, std::vector<LTMEdge const*>& edges)
{
    for (LTMEdge const* outE : fromNode->m_outEdges) {
        for (LTMEdge const* inE : toNode->m_inEdges) {
            if (outE == inE) {
                edges.push_back(outE);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
LTMEdge const* LTMNode::GetAnyActionBetweenNodes(LTMNode const* fromNode, LTMNode const* toNode)
{
    std::vector<LTMEdge const*> choices;
    LTMNode::GetAllEdgesBetweenNodes(fromNode,toNode, choices);
    return GetRandomItemInVector<LTMEdge const*>(choices);
}

//////////////////////////////////////////////////////////////////////////
LTMEdge const* LTMNode::GetMutateActionBetweenNodes(float mutatePossibility, LTMNode const* fromNode, LTMNode const* toNode)
{
    if (RandomZeroToOne() <= mutatePossibility) {
        int outEdgeNum = (int)fromNode->m_outEdges.size();
        int inEdgeNum = (int)toNode->m_inEdges.size();
        int idx = RandomIntInRange(0, inEdgeNum+outEdgeNum-1);
        if (idx < outEdgeNum) {
            return fromNode->m_outEdges[idx];
        }
        else {
            return toNode->m_inEdges[idx-outEdgeNum];
        }
    }

    return GetAnyActionBetweenNodes(fromNode,toNode);
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::CombineMentalHistory(LTMNode const* nodeA, LTMNode const* nodeB, LTMNode* nodeCombined)
{
    //TODO actually use mental history as cue or else
    //TODO refine mental history combine
    auto itA = nodeA->m_mentalHistories.begin();
    auto itB = nodeB->m_mentalHistories.begin();
    for (; itA!=nodeA->m_mentalHistories.end() && itB!=nodeB->m_mentalHistories.end(); itA++,itB++) {
        nodeCombined->AddMentalHistory(*itA);
        nodeCombined->AddMentalHistory(*itB);
    }
    while (itA != nodeA->m_mentalHistories.end()) {
        nodeCombined->AddMentalHistory(*itA);
        itA++;
    }
    while (itB != nodeB->m_mentalHistories.end()) {
        nodeCombined->AddMentalHistory(*itB);
        itB++;
    }
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::GenerateProspectActionFromTraces(std::vector<LTMConstNodeList> const& traces, GameEvent& prospect)
{
    //according to action possibility: find most possible event out of all traces
    float tempPossibility = 0.f;
    LTMEdge const* edge = nullptr;
    for (LTMConstNodeList const& trace : traces) {
        if (traces.empty()) {
            return;
        }

        for (LTMEdge const* outEdge : trace[0]->m_outEdges) {
            if (outEdge->m_probability > tempPossibility) {
                edge = outEdge;
                tempPossibility = outEdge->m_probability;
            }
        }
    }

    if (edge) {
        prospect = edge->m_event;
    }
}

//////////////////////////////////////////////////////////////////////////
LTMNode::LTMNode()
{
    m_id = sNodeID++;

    m_clock.onDayIncrease.SubscribeMethod(this, &LTMNode::MutateBySelf);
    m_clock.onDayIncrease.SubscribeMethod(this, &LTMNode::Forget);

    m_mentalHistories.reserve(MENTAL_HISTORY_SIZE);
}

//////////////////////////////////////////////////////////////////////////
LTMNode::LTMNode(unsigned int id)
    : m_id(id)
{
    m_clock.onDayIncrease.SubscribeMethod(this, &LTMNode::MutateBySelf);
    m_clock.onDayIncrease.SubscribeMethod(this, &LTMNode::Forget);

    m_mentalHistories.reserve(MENTAL_HISTORY_SIZE);
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::MutateBySelf(GameTime const&)
{
    //UNUSED(time);
    float possibility = GetMutatePossibility();
    if (RandomZeroToOne() <= possibility) {
        m_memory.Mutate();
        LTMEdge::MutateForEdges(m_inEdges);
        LTMEdge::MutateForEdges(m_outEdges);

        AddMentalHistory(m_memory.m_mentalState);

        if (!m_isSaturated && m_memory.m_mentalState.IsSaturated()) {
            m_isSaturated=true;
        }
        else if (m_isSaturated && m_memory.m_mentalState.IsLowSaturated()) {
            m_isSaturated = false;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::Forget(GameTime const&)
{
    //UNUSED(time);
    float possibility = GetForgetPossibility();
    if (RandomZeroToOne() <= possibility) {
        m_memory.Forget();
        LTMEdge::ForgetForEdges(m_inEdges);
        LTMEdge::ForgetForEdges(m_outEdges);

        AddMentalHistory(m_memory.m_mentalState);

        //set blank
        if (!m_isSaturated && RandomZeroToOne() <= possibility) {
            m_isBlank = true;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::AddMentalHistory(APF_Lib::MentalState const& newMental)
{
    m_mentalHistories.push_back(newMental);
    if ((int)m_mentalHistories.size() > MENTAL_HISTORY_SIZE) {
        m_mentalHistories.erase(m_mentalHistories.begin());
    }
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::RecalculateOutEdgeProbability()
{
    size_t total = 0;
    for (LTMEdge const* e : m_outEdges) {
        total += e->m_times;
    }
    if (total == 0) {
        return;
    }

    float fraction = 1.f/(float)(total);
    for (LTMEdge* e : m_outEdges) {
        e->m_probability = (float)e->m_times*fraction;
    }
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MentalState& LTMNode::GetMentalState()
{
    return m_memory.m_mentalState;
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MentalState const& LTMNode::GetMentalState() const
{
    return m_memory.m_mentalState;
}

//////////////////////////////////////////////////////////////////////////
APF_Lib::MentalState LTMNode::GetAverageMentalState() const
{
    APF_Lib::MentalState result;
    for (size_t i = 0; i < m_mentalHistories.size(); i++) {
        result+= m_mentalHistories[i];
    }
    float sizeFactor = 1.f/(float)m_mentalHistories.size();
    for (int j = 0; j < APF_Lib::NUM_MENTAL_FEATURES; j++) {
        result[j] *=sizeFactor;
    }
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::GetAllDistinctToNodes(LTMConstNodeList& toNodes) const
{
    for (LTMEdge const* e : m_outEdges) {
        LTMNode const* toNode = e->m_toNode;
        if (!IsNodeInList(toNode, toNodes) && toNode) {
            toNodes.push_back(toNode);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
float LTMNode::GetMutatePossibility() const
{
    //TODO Saturation of node refine
    float clockMutate = m_clock.GetMutatePossibility();
    APF_Lib::MentalState avgMental = GetAverageMentalState();
    float mentalSalience = avgMental.GetMentalStateSalience();
    if(mentalSalience>.9f){
        float salienceInverse = MAX_UNIT_VALUE - mentalSalience;
        return  salienceInverse*salienceInverse * clockMutate;
    }
    else if (mentalSalience > .5f) {
        return (1-mentalSalience)*clockMutate;
    }
    else {
        return clockMutate;
    }
}

//////////////////////////////////////////////////////////////////////////
float LTMNode::GetForgetPossibility() const
{
    //TODO new forget possibility?
    float clockMutate = m_clock.GetMutatePossibility();
    APF_Lib::MentalState avgMental = GetAverageMentalState();
    float mentalSalience = avgMental.GetMentalStateSalience();
    float frequency = Interpolate(.75f,1.f, GetFrequencyPossibility());
    if (mentalSalience > .9f) {
        float salienceInverse = MAX_UNIT_VALUE - mentalSalience;
        return  salienceInverse * salienceInverse * clockMutate * frequency;
    }
    else if (mentalSalience > .5f) {
        return (1 - mentalSalience) * clockMutate * frequency;
    }
    else {
        return clockMutate * frequency;
    }
}

//////////////////////////////////////////////////////////////////////////
float LTMNode::GetFrequencyPossibility() const
{
    size_t totalTimes = 0;
    for (LTMEdge const* e : m_outEdges) {
        totalTimes+= e->m_times;
    }
    for (LTMEdge const* e : m_inEdges) {
        totalTimes += e->m_times;
    }
    float temp = std::sqrtf((float)totalTimes);
    constexpr float MAX_TIME_SQRT_FRACTION = .1f;
    return ClampFloat(temp*MAX_TIME_SQRT_FRACTION, 0.f, MAX_UNIT_VALUE);
}

//////////////////////////////////////////////////////////////////////////
std::string LTMNode::GetIDText() const
{
    std::string result = "N"+ std::to_string(m_id);
    return result;
}

//////////////////////////////////////////////////////////////////////////
void LTMNode::CompositeCombineEmotionDistantNodes(LTMNode* nodeA, LTMNode* nodeB)
{
    APF_Lib::MentalState newMental;
    APF_Lib::MentalState& mentalA = nodeA->GetMentalState();
    APF_Lib::MentalState oldMentalA = mentalA;
    APF_Lib::MentalState& mentalB = nodeB->GetMentalState();
    APF_Lib::MentalState oldMentalB = mentalB;
    float salienceA = mentalA.GetMentalStateSalience();
    float salienceB = mentalB.GetMentalStateSalience();
    if (salienceA > .8f) {
        APF_Lib::MentalState::CombineMentalStates(mentalA, mentalB, .7f, newMental);
    }
    else if (salienceB > .8f) {
        APF_Lib::MentalState::CombineMentalStates(mentalB, mentalA, .7f, newMental);
    }
    else {
        APF_Lib::MentalState::CombineMentalStates(mentalA, mentalB, .5f, newMental);
    }
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalA, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalA);
    nodeA->AddMentalHistory(mentalA);
    APF_Lib::MentalState::CombineMentalStatesClamped(oldMentalB, newMental, APF_Lib::MENTAL_SIMILAR_MIN, mentalB);
    nodeB->AddMentalHistory(mentalB);
}

unsigned int LTMNode::sNodeID = 0;

//////////////////////////////////////////////////////////////////////////
LTMConstNodeAndEdge::LTMConstNodeAndEdge(LTMNode const* n, LTMEdge const* e)
    : node(n)
    , edge(e)
{
}
