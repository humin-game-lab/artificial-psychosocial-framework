#pragma once

#include "ThirdParty/APF/Memory/ActorState.hpp"
#include <map>

namespace APF_Lib {
    namespace MM {
        class GameActor;
        class GameObject;

        // game state is always used under an actor, for others
        struct GameState
        {
        public:
            static float GetSimilarity(GameState const& stateA, GameState const& stateB);
            static void CombineStates(GameState const& stateA, GameState const& stateB, float alpha, GameState& combined);  //alpha 1, stateA; alpha 0, stateB

            void Clear();
            virtual void Mutate();
            virtual void Forget();
            virtual void UpdateStateForObject(GameObject* object);
            virtual void UpdateStateForActor(GameActor* actor);

            ObjectState const* FindStateOfObject(GameObject* object) const;
            ActorState const* FindStateOfActor(GameActor* actor) const;
            bool IsEmpty() const;
            virtual bool IsFaded() const;

        public:
            std::map<GameObject*, ObjectState> objectPerceivedStates;   //TODO may need a ObjectState* instead for polymorphism
            std::map<GameActor*, ActorState> actorPerceivedStates;      //TODO may need a ActorState* instead for polymorphism

            //possibly with player status
        };
    }
}
