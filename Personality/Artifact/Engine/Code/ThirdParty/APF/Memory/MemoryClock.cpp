#include "ThirdParty/APF/Memory/MemoryClock.hpp"
#include "ThirdParty/APF/APF/LibCommon.hpp"

using namespace APF_Lib::MM;

static std::vector<MemoryClock*> sMemClocks;

//////////////////////////////////////////////////////////////////////////
MemoryClock::MemoryClock()
{
    for (size_t i = 0; i < sMemClocks.size(); i++) {
        if (sMemClocks[i] == nullptr) {
            sMemClocks[i] = this;
            return;
        }
    }

    sMemClocks.push_back(this);
}

//////////////////////////////////////////////////////////////////////////
MemoryClock::~MemoryClock()
{
    for (size_t i = 0; i < sMemClocks.size(); i++) {
        if (sMemClocks[i] == this) {
            sMemClocks[i] = nullptr;
            return;
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::Update(float deltaSeconds)
{
    if (m_isPaused) {
        deltaSeconds = 0.f;
    }

    deltaSeconds = ClampFloat(deltaSeconds, m_minFrameTime,m_maxFrameTime);
    deltaSeconds *= m_timeScale;

    //update for deltaSeconds
    m_totalTime.m_seconds += deltaSeconds;
    while(m_totalTime.m_seconds >= 60.f) {
        m_totalTime.m_seconds -= 60.f;
        m_totalTime.m_minutes++;
        onMinuteIncrease(m_totalTime);

        if(m_totalTime.m_minutes >= 60) {
            m_totalTime.m_minutes -= 60;
            m_totalTime.m_hours++;
            onHourIncrease(m_totalTime);

            if(m_totalTime.m_hours >= 24) {
                m_totalTime.m_hours -= 24;
                m_totalTime.m_days++;
                onDayIncrease(m_totalTime);

                if(m_totalTime.m_days >= 30) {
                    m_totalTime.m_days -= 30;
                    m_totalTime.m_months++;
                    onMonthIncrease(m_totalTime);

                    if(m_totalTime.m_months >= 12) {
                        m_totalTime.m_months -= 12;
                        m_totalTime.m_years++;
                        onYearIncrease(m_totalTime);
                    }
                }
            }
        }
    }   
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::Pause()
{
    m_isPaused = true;
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::Resume()
{
    m_isPaused = false;
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::SetScale(float newScale)
{
    if (newScale < 0.f) {
        m_timeScale = 0.f;
        return;
    }

    m_timeScale = newScale;
}

//////////////////////////////////////////////////////////////////////////
float MemoryClock::GetMutatePossibility() const
{
    return m_totalTime.GetMutatePossibility();
}

//////////////////////////////////////////////////////////////////////////
std::string MemoryClock::ToString() const
{
    return m_totalTime.ToString();
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::BeginFrame(float deltaSeconds)
{
    for (size_t i=0;i<sMemClocks.size();i++) {
        MemoryClock* c = sMemClocks[i];
    //for(MemoryClock* c: sMemClocks){
        if (c) {
            c->Update(deltaSeconds);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void MemoryClock::SetGlobalTimeScale(float timeScale)
{
    for (MemoryClock* c : sMemClocks) {
        if (c) {
            c->SetScale(timeScale);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
float MemoryClock::GetGlobalTimeScale()
{
    for (MemoryClock* c : sMemClocks) {
        if (c) {
            return c->GetScale();
        }
    }
    return -1.f;
}
