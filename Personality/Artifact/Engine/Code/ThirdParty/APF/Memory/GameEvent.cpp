#include "ThirdParty/APF/Memory/GameEvent.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"

using namespace APF_Lib::MM;

std::vector<ActionInfo> ActionInfo::sPossibleActions;

//////////////////////////////////////////////////////////////////////////
float ActionInfo::GetSimilarity(ActionInfo const& infoA, ActionInfo const& infoB)
{
    //TODO "refine action info similarity"
    if (infoA.apfIdx == infoB.apfIdx) {
        return 1.f;
    }
    return .5f * GameTags::GetSimilarityForTags(infoA.tags, infoB.tags);
}

//////////////////////////////////////////////////////////////////////////
ActionInfo::ActionInfo(std::string const& name, float e)
    : actionName(name)
    , effect(e)
{
}

//////////////////////////////////////////////////////////////////////////
float ActionInfo::GetAdjectiveDelta(std::string const& adj) const
{
    std::map<std::string, float>::const_iterator it = adjectives.find(adj);
    if (it != adjectives.end()) {
        return it->second;
    }

    return 0.f;
}

//////////////////////////////////////////////////////////////////////////
ActionInfo const* ActionInfo::GetActionOfName(std::string const& actionName)
{
    for (ActionInfo const& a : sPossibleActions) {
        if (a.actionName == actionName) {
            return &a;
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
ActionInfo const* ActionInfo::GetActionOfAPFIdx(int apfIdx)
{
    for (ActionInfo const& info : sPossibleActions) {
        if (info.apfIdx == apfIdx) {
            return &info;
        }
    }

    return nullptr;
}

//////////////////////////////////////////////////////////////////////////
float GameEvent::GetSimilarity(GameEvent const& eventA, GameEvent const& eventB)
{
    if (eventA.m_isGarbage || eventB.m_isGarbage) {
        //Similarity is 0 if either is garbage
        return 0.f;
    }

    float certaintySimilar = 1.f- AbsFloat(eventA.m_certainty-eventB.m_certainty);
    float patientSimilar = (eventA.m_patientType==eventB.m_patientType&&eventA.m_patient==eventB.m_patient)?1.f:.0f;
    float actorSimilar = eventA.m_actor==eventB.m_actor?1.f:.0f;
    float ownerSimilar = eventA.m_owner==eventB.m_owner?1.f:.0f;
    float actionSimilar = 1.f;  //if both actionInfo invalid, then similarity is 1
    if(eventA.m_actionInfo && eventB.m_actionInfo){
        actionSimilar =  ActionInfo::GetSimilarity(*eventA.m_actionInfo, *eventB.m_actionInfo);
    }
    else if (eventA.m_actionInfo || eventB.m_actionInfo) {
        //When one actionInfo is invalid, the other exist, similarity is 0
        actionSimilar = 0.f;
    }
    return (certaintySimilar+patientSimilar+actorSimilar+ownerSimilar+actionSimilar)*.2f;
}

//////////////////////////////////////////////////////////////////////////
void GameEvent::Mutate()
{
    m_certainty = ClampFloat(m_certainty+(RandomZeroToOne()-.5f)*STATE_CONFIDENCE_MUTATE_EXTENT, 0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
void GameEvent::Forget()
{
    m_certainty = ClampFloat(m_certainty - AbsFloat(RandomZeroToOne()-.5f) * STATE_CONFIDENCE_MUTATE_EXTENT,
        0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
void GameEvent::Enhance(float delta)
{
    m_certainty = ClampFloat(m_certainty+delta, 0.f, 1.f);
}

//////////////////////////////////////////////////////////////////////////
bool GameEvent::operator==(GameEvent const& other) const
{
    return (m_owner == other.m_owner && m_actor == other.m_actor && m_patient == other.m_patient
        && m_action == other.m_action && m_patientType == other.m_patientType);
}

//////////////////////////////////////////////////////////////////////////
bool GameEvent::operator!=(GameEvent const& other) const
{
    return (m_owner!=other.m_owner || m_actor!=other.m_actor || m_patient!=other.m_patient
        || m_action!=other.m_action || m_patientType!=other.m_patientType);
}

//////////////////////////////////////////////////////////////////////////
bool GameEvent::IsFaded() const
{
    return m_certainty<STATE_CONFIDENCE_MUTATE_EXTENT;
}

//////////////////////////////////////////////////////////////////////////
float GameEvent::GetTotalDeltaValence() const
{
    if (m_actionInfo == nullptr) {
        return 0.f;
    }

    float delta = 0.f;
    for (std::string const& str : m_adjectives) {
        delta += m_actionInfo->GetAdjectiveDelta(str);
    }
    return delta;
}

