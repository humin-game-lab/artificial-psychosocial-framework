#include "ThirdParty/APF/Memory/MemoryQuerySystem.hpp"
#include "ThirdParty/APF/Memory/ShortTermMemory.hpp"

using namespace APF_Lib::MM;

//////////////////////////////////////////////////////////////////////////
MemoryQueryNode::MemoryQueryNode(MemoryQueryPair const& p)
    : pair(p)
{
    type = eQueryType::QUERY_MEMORY;
}

//////////////////////////////////////////////////////////////////////////
EventQueryNode::EventQueryNode(EventQueryPair const& p)
    : pair(p)
{
    type = eQueryType::QUERY_EVENT;
}

//////////////////////////////////////////////////////////////////////////
void QueryList::Clear()
{
    for (QueryNode* node : m_queryNodes) {
        delete node;
    }

    m_queryNodes.clear();
}

//////////////////////////////////////////////////////////////////////////
void QueryList::SimpleAddNodes(MemoryTrace const& trace, GetMemorySimilarity memFunc, GetEventSimilarity eventFunc)
{
    for (MemoryAndEvent const& memEvent : trace) {
        if (!memEvent.memory.m_isGarbage) {
            AddNode(memFunc, memEvent.memory);
        }
        if (!memEvent.selfEvent.m_isGarbage) {
            AddNode(eventFunc, memEvent.selfEvent);
        }
    }
}

//////////////////////////////////////////////////////////////////////////
void QueryList::AddNode(GetMemorySimilarity func, Memory const& mem)
{
    MemoryQueryPair pair(func, mem);
    MemoryQueryNode* node = new MemoryQueryNode(pair);
    m_queryNodes.push_back((QueryNode*)node);
}

//////////////////////////////////////////////////////////////////////////
void QueryList::AddNode(GetEventSimilarity func, GameEvent const& event)
{
    EventQueryPair pair(func, event);
    EventQueryNode* node = new EventQueryNode(pair);
    m_queryNodes.push_back((QueryNode*)node);
}

//////////////////////////////////////////////////////////////////////////
QueryNode* QueryList::PopFront() const
{
    QueryNode* node = m_queryNodes[m_index];
    m_index++;
    return node;
}

//////////////////////////////////////////////////////////////////////////
bool QueryList::IsEmpty() const
{
    return m_queryNodes.empty() || m_index>= (unsigned int)m_queryNodes.size();
}
