#include "Vocabulary.hpp"

#include <utility>


APF_Lib::Vocab::Vocab(const char* name) : m_name(name) {}


APF_Lib::Vocab::Vocab(const std::string& name) : m_name(name) {}


APF_Lib::Vocab& APF_Lib::Vocab::operator=(const Vocab& copy) 
{
	m_name = copy.m_name;
	return *this;
}

bool APF_Lib::Vocab::operator==(const Vocab& vocab_to_comp) const
{
	return (m_name == vocab_to_comp.m_name);
}


bool APF_Lib::Vocab::operator!=(const Vocab& vocab_to_comp) const
{
	return (m_name != vocab_to_comp.m_name);
}


bool APF_Lib::Vocab::operator<(const Vocab& vocab_to_comp) const
{
	return (m_name < vocab_to_comp.m_name);
}


bool APF_Lib::Vocab::operator<=(const Vocab& vocab_to_comp) const
{
	return (m_name <= vocab_to_comp.m_name);
}


bool APF_Lib::Vocab::operator>(const Vocab& vocab_to_comp) const
{
	return (m_name > vocab_to_comp.m_name);
}


bool APF_Lib::Vocab::operator>=(const Vocab& vocab_to_comp) const
{
	return (m_name >= vocab_to_comp.m_name);
}

std::string APF_Lib::Vocab::ToString()
{
	return std::string("{'" + m_name + "'}");
}


APF_Lib::ANPC::ANPC(const char* name, const float o, const float c, const float e, const float a, const float n) :
	Vocab(name), m_personality(o, c, e, a, n) {}


std::string APF_Lib::ANPC::ToString()
{
	return std::string("{'" + m_name + "', (" + 
		std::to_string(m_personality[PERSONALITY_OPENNESS]) + ", " + 
		std::to_string(m_personality[PERSONALITY_CONSCIENTIOUSNESS]) + ", " +
		std::to_string(m_personality[PERSONALITY_EXTROVERSION]) + ", " +
		std::to_string(m_personality[PERSONALITY_AGREEABLENESS]) + ", " +
		std::to_string(m_personality[PERSONALITY_NEUROTICISM]) + ")}");
}


APF_Lib::Object::Object(const char* name) : Vocab(name) {}


APF_Lib::Object::Object(const std::string& name) : Vocab(name) {}


APF_Lib::Action::Action(const char* name, const float effect) : Vocab(name), m_effect(effect) {}


std::string APF_Lib::Action::ToString()
{
	return std::string("{'" + m_name + "', " + std::to_string(m_effect) + "}");
}