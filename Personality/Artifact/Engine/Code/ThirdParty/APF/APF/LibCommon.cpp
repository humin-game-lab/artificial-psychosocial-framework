#include "LibCommon.hpp"
#include <cmath>

//////////////////////////////////////////////////////////////////////////
float APF_Lib::SmoothStartAndEnd(const float input_zero_to_one)
{
    if (input_zero_to_one < .5f) {
        return 2.f * input_zero_to_one * input_zero_to_one;
    }
    else {
        float temp = 1.f - input_zero_to_one;
        return -2.f * temp * temp + 1.f;
    }
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::InverseSmoothStartAndEnd(const float input_zero_to_one)
{
    if (input_zero_to_one < .5f) {
        return std::sqrtf(input_zero_to_one * .5f);
    }
    else {
        return 1.f - std::sqrtf((input_zero_to_one - 1.f) * (-.5f));
    }
}

//////////////////////////////////////////////////////////////////////////
float APF_Lib::RandomZeroToOne()
{
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

