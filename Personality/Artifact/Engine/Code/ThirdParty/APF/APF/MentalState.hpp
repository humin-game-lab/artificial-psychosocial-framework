#pragma once
#include <string>

#include "LibCommon.hpp"

namespace APF_Lib
{
	enum MentalFeature
	{
		MENTAL_FEATURE_UNKNOWN = -1,

		//Valence
		MENTAL_FEATURE_POSITIVE,
		MENTAL_FEATURE_NEGATIVE,

		//Mood
		MENTAL_FEATURE_PLEASED,
		MENTAL_FEATURE_DISPLEASED,
		MENTAL_FEATURE_LIKING,
		MENTAL_FEATURE_DISLIKING,
		MENTAL_FEATURE_APPROVING,
		MENTAL_FEATURE_DISAPPROVING,

		//Emotion
		MENTAL_FEATURE_HOPE,
		MENTAL_FEATURE_FEAR,
		MENTAL_FEATURE_JOY,
		MENTAL_FEATURE_DISTRESS,
		MENTAL_FEATURE_LOVE,
		MENTAL_FEATURE_HATE,
		MENTAL_FEATURE_INTEREST,
		MENTAL_FEATURE_DISGUST,
		MENTAL_FEATURE_PRIDE,
		MENTAL_FEATURE_SHAME,
		MENTAL_FEATURE_ADMIRATION,
		MENTAL_FEATURE_REPROACH,

		//Feelings
		MENTAL_FEATURE_SATISFACTION,
		MENTAL_FEATURE_FEARS_CONFIRMED,
		MENTAL_FEATURE_RELIEF,
		MENTAL_FEATURE_DISAPPOINTMENT,
		MENTAL_FEATURE_HAPPY_FOR,
		MENTAL_FEATURE_RESENTMENT,
		MENTAL_FEATURE_GLOATING,
		MENTAL_FEATURE_PITY,
		MENTAL_FEATURE_GRATIFICATION,
		MENTAL_FEATURE_REMORSE,
		MENTAL_FEATURE_GRATITUDE,
		MENTAL_FEATURE_ANGER,
		
		NUM_MENTAL_FEATURES
	};

	enum ValenceFeature
	{
		VALENCE_FEATURE_UNKNOWN = -1,

		VALENCE_FEATURE_POSITIVE,
		VALENCE_FEATURE_NEGATIVE,

		NUM_VALENCE_FEATURES
	};

	enum MoodFeature
	{
		MOOD_FEATURE_UNKNOWN = -1,

		MOOD_FEATURE_PLEASED,
		MOOD_FEATURE_DISPLEASED,
		MOOD_FEATURE_LIKING,
		MOOD_FEATURE_DISLIKING,
		MOOD_FEATURE_APPROVING,
		MOOD_FEATURE_DISAPPROVING,

		NUM_MOOD_FEATURES
	};

	enum EmotionFeature
	{
		EMOTION_FEATURE_UNKNOWN = -1,

		EMOTION_FEATURE_HOPE,
		EMOTION_FEATURE_FEAR,
		EMOTION_FEATURE_JOY,
		EMOTION_FEATURE_DISTRESS,
		EMOTION_FEATURE_LOVE,
		EMOTION_FEATURE_HATE,
		EMOTION_FEATURE_INTEREST,
		EMOTION_FEATURE_DISGUST,
		EMOTION_FEATURE_PRIDE,
		EMOTION_FEATURE_SHAME,
		EMOTION_FEATURE_ADMIRATION,
		EMOTION_FEATURE_REPROACH,

		NUM_EMOTION_FEATURES
	};

	enum FeelingFeature
	{
		FEELING_FEATURE_UNKNOWN = -1,

		FEELING_FEATURE_SATISFACTION,
		FEELING_FEATURE_FEARS_CONFIRMED,
		FEELING_FEATURE_RELIEF,
		FEELING_FEATURE_DISAPPOINTMENT,
		FEELING_FEATURE_HAPPY_FOR,
		FEELING_FEATURE_RESENTMENT,
		FEELING_FEATURE_GLOATING,
		FEELING_FEATURE_PITY,
		FEELING_FEATURE_GRATIFICATION,
		FEELING_FEATURE_REMORSE,
		FEELING_FEATURE_GRATITUDE,
		FEELING_FEATURE_ANGER,

		NUM_FEELING_FEATURES
	};
	
	struct MentalState
	{
	public:
        static float GetSimilarityForFloatInMentalStates(float a, float b);
        static float GetSimilarityForMoods(float const (&moodA)[NUM_MOOD_FEATURES], float const (&moodB)[NUM_MOOD_FEATURES], float const (&weights)[2]);
        static float GetSimilarityForMentalStates(MentalState const& msA, MentalState const& msB);
        static float GetSimilarityInValenceForMentalStates(MentalState const& msA, MentalState const& msB);
        static float GetSimilarityInMoodForMentalStates(MentalState const& msA, MentalState const& msB);
        static float GetSimilarityInEmotionForMentalStates(MentalState const& msA, MentalState const& msB);
        static float GetSimilarityInFeelingForMentalStates(MentalState const& msA, MentalState const& msB);

        static void GetDeltaMentalState(MentalState const& before, MentalState const& after, MentalState& delta);

        static void CombineMentalStates(MentalState const& msA, MentalState const& msB, float alpha, MentalState& combined);
        static void CombineMentalStatesClamped(MentalState const& base, MentalState const& target, float baseMaxDelta, MentalState& combined);
        static void CombineMentalValences(MentalState const& msA, MentalState const& msB, float alpha, float(&valences)[NUM_VALENCE_FEATURES]);
        static void CombineMentalMoods(MentalState const& msA, MentalState const& msB, float alpha, float(&moods)[NUM_MOOD_FEATURES]);

		MentalState() = default;
		~MentalState() = default;

		void GetMentalState(MentalState& out_mental_state) const;
		void GetMentalState(float (&out_mental_state)[NUM_MENTAL_FEATURES]) const;
		void GetValence(float(&out_valence_state)[NUM_VALENCE_FEATURES]) const;
		void GetMood(float(&out_mood_state)[NUM_MOOD_FEATURES]) const;
		void GetEmotion(float(&out_emotional_state)[NUM_EMOTION_FEATURES]) const;
		void GetFeeling(float(&out_feeling_state)[NUM_FEELING_FEATURES]) const;

        //Accessors for Memories
        float GetAveragedMental() const;
        float GetMaxMental() const;
        float GetMentalStateSalience() const;
        bool IsSaturated() const;
        bool IsLowSaturated() const;
        bool IsFaded() const;

		void SetMentalState(const MentalState& new_mental_state);
		void SetMentalState(const float(&new_mental_state)[NUM_MENTAL_FEATURES]);
		void SetValence(const float(&new_valence_state)[NUM_VALENCE_FEATURES]);
		void SetMood(const float(&new_mood_state)[NUM_MOOD_FEATURES]);
		void SetEmotion(const float(&new_emotional_state)[NUM_EMOTION_FEATURES]);
        void SetFeeling(const float(&new_feeling_state)[NUM_FEELING_FEATURES]);
		
		//Modifiers for Memories
        void SetFromRawAndProspect(MentalState const& raw, MentalState const& prospect, float similarity);
        void PolarizeSmoothStartAndEnd(float delta);
        void MultiplyFactor(float factor);
        void Decay();
        void Mutate();
		
		float& operator[] (const int idx);
		const float& operator[] (const int idx) const;
		float& operator[] (const MentalFeature enumeration);
		const float& operator[] (const MentalFeature enumeration) const;
		MentalState operator+ (MentalState& mental_state);
		void operator+=(MentalState const& mental_state);
		void operator+=(float(&out_mental_state)[NUM_MENTAL_FEATURES]);
		void operator-=(float(&out_mental_state)[NUM_MENTAL_FEATURES]);
		void operator-=(MentalState const& mental_state);

		//debug functions
		static MentalState GenerateRandomMentalState();
		static MentalState GenerateDecayMentalState();

		virtual std::string ToString();


	protected:
		float m_mentalState[NUM_MENTAL_FEATURES] = { MIN_UNIT_VALUE };
	};
}



