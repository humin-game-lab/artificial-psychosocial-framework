#include "Engine/BehaviorTree/SelectorNode.hpp"


//-----------------------------------------------------------------------------------------------
SelectorNode::SelectorNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode):
	BehaviorTreeNode(behaviorTree, nodeName, parentNode){

}


//-----------------------------------------------------------------------------------------------
SelectorNode::~SelectorNode(){
	for(int nodeIndex = 0; nodeIndex < (int)m_childNodes.size(); nodeIndex++){
		if(m_childNodes[nodeIndex]){
			delete m_childNodes[nodeIndex];
		}
	}
	m_childNodes.clear();
}


//-----------------------------------------------------------------------------------------------
bool SelectorNode::Run(float deltaSeconds){
	for (BehaviorTreeNode* node : m_childNodes) {
		if (node->m_nodeStatus == COMPLETE) {
			continue;
		}
		bool nodeResult = node->Run(deltaSeconds);
		if (node->m_nodeStatus == RUNNING) {
			m_nodeStatus = RUNNING;
			return true;
		}
		if (nodeResult) {
			if (m_nodeStatus != RUNNING) {
				m_nodeStatus = COMPLETE;
			}
			return true;
		}
		
	}
	m_nodeStatus = COMPLETE;
	return true;
}


//-----------------------------------------------------------------------------------------------
void SelectorNode::ResetNodeStatus(){
	m_nodeStatus = READY;
	for (BehaviorTreeNode* node : m_childNodes) {
		node->ResetNodeStatus();
	}
}


//-----------------------------------------------------------------------------------------------
void SelectorNode::AddChildNode(BehaviorTreeNode* node){
	m_childNodes.push_back(node);
}
