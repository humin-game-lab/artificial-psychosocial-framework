#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/DecoratorNode.hpp"
#include "Engine/BehaviorTree/SelectorNode.hpp"
#include "Engine/BehaviorTree/SequenceNode.hpp"
#include "Engine/Core/XmlUtils.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"


//-----------------------------------------------------------------------------------------------
BehaviorTree::BehaviorTree(std::string bhFileName, std::string ownerName){
	m_fileName = bhFileName;
	m_owner = ownerName;
	LoadBehaviorTreeFromFile();
}


//-----------------------------------------------------------------------------------------------
BehaviorTree::~BehaviorTree(){
	if(m_root){
		delete m_root;
		m_root = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void BehaviorTree::Run(float deltaSeconds){
	m_isPaused = false;
	if(m_root){
		m_root->Run(deltaSeconds);
	}
}


//-----------------------------------------------------------------------------------------------
void BehaviorTree::Pause(){
	m_isPaused = true;
}


//-----------------------------------------------------------------------------------------------
void BehaviorTree::Stop(){
	if (m_root) {
		m_root->ResetNodeStatus();
	}
}


//-----------------------------------------------------------------------------------------------
void BehaviorTree::Reset(){
	m_root->ResetNodeStatus();
}


//-----------------------------------------------------------------------------------------------
void BehaviorTree::LoadBehaviorTreeFromFile(){
	tinyxml2::XMLDocument behaviorTreeXML;
	behaviorTreeXML.LoadFile(m_fileName.c_str());
	tinyxml2::XMLNode* rootNode = behaviorTreeXML.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	std::string elementName = element->Name();
	ASSERT_OR_DIE(elementName == "Root", "Invalid Behavior Tree, should have root node first");
	std::string rootName = ParseXmlAttribute(*element, "name", std::string("None"));
	m_root = new RootNode(this, rootName, nullptr);

	tinyxml2::XMLElement* subNodeElement = element->FirstChildElement();
	if(subNodeElement != nullptr){
		m_root->AddChildNode(LoadNodeFromFile(*subNodeElement, m_root));
	}
}


//-----------------------------------------------------------------------------------------------
BehaviorTreeNode* BehaviorTree::LoadNodeFromFile(tinyxml2::XMLElement& elem, BehaviorTreeNode* parentNode){
	std::string nodeName = ParseXmlAttribute(elem, "name", std::string("None"));
	std::string elementName = elem.Name();
	if(elementName == "Sequence"){
		SequenceNode* sequenceNode = new SequenceNode(this, nodeName, m_root);
		tinyxml2::XMLElement* subNodeElement = elem.FirstChildElement();
		while(subNodeElement!=nullptr){
			sequenceNode->AddChildNode(LoadNodeFromFile(*subNodeElement, sequenceNode));
			subNodeElement = subNodeElement->NextSiblingElement();
		}
		return sequenceNode;
	}
	else if(elementName == "Selector"){
		SelectorNode* selectorNode = new SelectorNode(this, nodeName, m_root);
		tinyxml2::XMLElement* subNodeElement = elem.FirstChildElement();
		while (subNodeElement != nullptr) {
			selectorNode->AddChildNode(LoadNodeFromFile(*subNodeElement, selectorNode));
			subNodeElement = subNodeElement->NextSiblingElement();
		}
		return selectorNode;
	}
	else if(elementName == "Decorator"){
		DecoratorNode* decoratorNode = new DecoratorNode(this, nodeName, parentNode);
		tinyxml2::XMLElement* subNodeElement = elem.FirstChildElement();
		while (subNodeElement != nullptr) {
			std::string subnodeName = subNodeElement->Name();
			if(subnodeName == "Function"){
				std::string eventName = ParseXmlAttribute(*subNodeElement, "function", std::string("None"));
				ASSERT_OR_DIE(elementName != "None", "Decorator node missing event function");
				tinyxml2::XMLElement* argElement = subNodeElement->FirstChildElement();
				EventArgs eventArgs;
				while (argElement != nullptr) {
					std::string keyName = ParseXmlAttribute(*argElement, "key", std::string("None"));
					std::string valueName = ParseXmlAttribute(*argElement, "value", std::string("None"));
					eventArgs.SetValue(keyName, valueName);
					argElement = argElement->NextSiblingElement();
				}
				decoratorNode->AddEvent(eventName, eventArgs);
			}
			else{
				decoratorNode->m_childNode = LoadNodeFromFile(*subNodeElement, decoratorNode);
			}
			subNodeElement = subNodeElement->NextSiblingElement();
		}
		return decoratorNode;
	}
	else if (elementName == "Action") {
		std::string eventName = ParseXmlAttribute(elem, "function", std::string("None"));
		ASSERT_OR_DIE(elementName != "None", "Action node missing event function");
		tinyxml2::XMLElement* subNodeElement = elem.FirstChildElement();
		EventArgs eventArgs;
		while (subNodeElement != nullptr) {
			std::string keyName = ParseXmlAttribute(*subNodeElement, "key", std::string("None"));
			std::string valueName = ParseXmlAttribute(*subNodeElement, "value", std::string("None"));
			eventArgs.SetValue(keyName, valueName);
			subNodeElement = subNodeElement->NextSiblingElement();
		}
		ActionNode* actionNode = new ActionNode(this, nodeName, parentNode, eventArgs);
		actionNode->m_actionEventName = eventName;
		return actionNode;
	}
	else if (elementName == "Wait") {
		float time = ParseXmlAttribute(elem, "time",0.f);
		WaitNode* waitNode = new WaitNode(this, nodeName, parentNode, time);
		return waitNode;
	}
	return nullptr;
}
