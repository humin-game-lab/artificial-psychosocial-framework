#include "Engine/BehaviorTree/SequenceNode.hpp"


//-----------------------------------------------------------------------------------------------
SequenceNode::SequenceNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode):
	BehaviorTreeNode(behaviorTree, nodeName, parentNode){

}


//-----------------------------------------------------------------------------------------------
SequenceNode::~SequenceNode(){
	for (int nodeIndex = 0; nodeIndex < (int)m_childNodes.size(); nodeIndex++) {
		if (m_childNodes[nodeIndex]) {
			delete m_childNodes[nodeIndex];
		}
	}
	m_childNodes.clear();
}


//-----------------------------------------------------------------------------------------------
bool SequenceNode::Run(float deltaSeconds){
	for (BehaviorTreeNode* node : m_childNodes) {
		if(node->m_nodeStatus == COMPLETE){
			continue;
		}
		bool nodeResult= node->Run(deltaSeconds);
		if (node->m_nodeStatus == RUNNING) {
			m_nodeStatus = RUNNING;
			return true;
		}
		if(!nodeResult){
			if (m_nodeStatus != RUNNING) {
				m_nodeStatus = COMPLETE;
			}
			return true;
		}
	}
	m_nodeStatus = COMPLETE;
	return true;
}


//-----------------------------------------------------------------------------------------------
void SequenceNode::ResetNodeStatus(){
	m_nodeStatus = READY;
	for(BehaviorTreeNode* node: m_childNodes){
		node->ResetNodeStatus();
	}
}


//-----------------------------------------------------------------------------------------------
void SequenceNode::AddChildNode(BehaviorTreeNode* node){
	m_childNodes.push_back(node);
}
