#pragma once
#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"
#include <string>
#include <vector>

//-----------------------------------------------------------------------------------------------
struct SelectorNode : BehaviorTreeNode{
public:
	SelectorNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	~SelectorNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
	void AddChildNode(BehaviorTreeNode* node);
public:
	std::vector<BehaviorTreeNode*> m_childNodes;
};