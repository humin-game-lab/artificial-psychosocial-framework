#pragma once
#include <string>

class BehaviorTree;


//-----------------------------------------------------------------------------------------------
enum BehaviorTreeNodeStatus{
	COMPLETE,
	RUNNING,
	READY,
	NUM_NODE_STATUS,
};


//-----------------------------------------------------------------------------------------------
struct BehaviorTreeNode {
public:
	BehaviorTreeNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	virtual ~BehaviorTreeNode();

	virtual bool Run(float deltaSeconds) = 0;
	virtual void ResetNodeStatus() { m_nodeStatus = READY; };
public:
	BehaviorTree* m_behaviorTree;
	std::string m_nodeName;
	BehaviorTreeNode* m_parentNode = nullptr;
	BehaviorTreeNodeStatus m_nodeStatus = READY;
};


//-----------------------------------------------------------------------------------------------
struct RootNode : BehaviorTreeNode {
	RootNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	~RootNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
	void AddChildNode(BehaviorTreeNode* node);
public:
	BehaviorTreeNode* m_childNode = nullptr;
};