#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "Engine/Core/EngineCommon.hpp"

//------------------------------------------------------------------------------------------------
std::map<std::string, ActionEventFunction> ActionNode::s_subscribedActionEvents;


//------------------------------------------------------------------------------------------------
ActionNode::ActionNode(BehaviorTree* behaviorTree, std::string nodeName,
	BehaviorTreeNode* parentNode, EventArgs eventArgs) :
	BehaviorTreeNode(behaviorTree, nodeName, parentNode){
	m_nodeArgs = eventArgs;
	m_nodeArgs.SetValue("owner", m_behaviorTree->GetBehaviorTreeOwner());
}


//------------------------------------------------------------------------------------------------
ActionNode::ActionNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode):
	BehaviorTreeNode(behaviorTree, nodeName, parentNode) {
	m_nodeArgs.SetValue("owner", m_behaviorTree->GetBehaviorTreeOwner());
}


//------------------------------------------------------------------------------------------------
ActionNode::~ActionNode(){

}


//------------------------------------------------------------------------------------------------
bool ActionNode::Run(float deltaSeconds){
	UNUSED(deltaSeconds);
	m_nodeStatus = RUNNING;
	if (s_subscribedActionEvents.find(m_actionEventName) != s_subscribedActionEvents.end()) {
		bool result = s_subscribedActionEvents[m_actionEventName](m_nodeArgs);
		if (!result) {
			return true;
		}
	}
	m_nodeStatus = COMPLETE;
	return true;
}


//------------------------------------------------------------------------------------------------
void ActionNode::ResetNodeStatus(){
	m_nodeStatus = READY;
}


//------------------------------------------------------------------------------------------------
void ActionNode::SubscribeActionEventFunction(std::string const& eventName, ActionEventFunction functionPtr){
	s_subscribedActionEvents[eventName] = functionPtr;
}


//------------------------------------------------------------------------------------------------
WaitNode::WaitNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode,
	float waitTime): ActionNode(behaviorTree, nodeName, parentNode){
	m_totalTime = waitTime;
	m_remainingTime = m_totalTime;
}


//------------------------------------------------------------------------------------------------
WaitNode::~WaitNode(){

}


//------------------------------------------------------------------------------------------------
bool WaitNode::Run(float deltaSeconds){
	m_nodeStatus = RUNNING;
	m_remainingTime -= deltaSeconds;
	if(m_remainingTime <= 0.f){
		m_nodeStatus = COMPLETE;
	}
	return true;
}


//------------------------------------------------------------------------------------------------
void WaitNode::ResetNodeStatus(){
	m_nodeStatus = READY;
	m_remainingTime = m_totalTime;
}
