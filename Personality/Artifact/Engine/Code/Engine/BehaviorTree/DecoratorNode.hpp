#pragma once
#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"
#include "Engine/Core/NamedStrings.hpp"
#include <string>
#include <map>
#include <vector>

//-----------------------------------------------------------------------------------------------
typedef NamedStrings EventArgs;
typedef bool (*DecoratorEventFunction)(EventArgs& args);


//-----------------------------------------------------------------------------------------------
struct DecoratorNode : BehaviorTreeNode{
public:
	DecoratorNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	~DecoratorNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
	void AddEvent(std::string eventName, EventArgs nodeArgs);
	static void SubscribeDecoratorEventFunction(std::string const& eventName, DecoratorEventFunction functionPtr);
	static void SubscribeCommonDecorators();
public:
	static std::map<std::string, DecoratorEventFunction> s_subscribedDectoratornEvents;
	std::vector<std::string> m_decoratorEventNames;
	std::vector<EventArgs> m_nodeArgs;
	BehaviorTreeNode* m_childNode = nullptr;
};

