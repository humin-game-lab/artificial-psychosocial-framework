#include "Engine/BehaviorTree/DecoratorNode.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "Engine/BehaviorTree/BehavorTreeCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"

//-----------------------------------------------------------------------------------------------
std::map<std::string, DecoratorEventFunction> DecoratorNode::s_subscribedDectoratornEvents;


//-----------------------------------------------------------------------------------------------
DecoratorNode::DecoratorNode(BehaviorTree* behaviorTree, std::string nodeName, 
	BehaviorTreeNode* parentNode):
	BehaviorTreeNode(behaviorTree, nodeName, parentNode){
	
}


//-----------------------------------------------------------------------------------------------
DecoratorNode::~DecoratorNode(){
	if(m_childNode){
		delete m_childNode;
		m_childNode = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
bool DecoratorNode::Run(float deltaSeconds){
	bool callResults = true;
	for(int eventIndex = 0; eventIndex < static_cast<int>(m_decoratorEventNames.size()); eventIndex++){
		if (s_subscribedDectoratornEvents.find(m_decoratorEventNames[eventIndex]) != 
			s_subscribedDectoratornEvents.end()) {
			bool callResult = 
				s_subscribedDectoratornEvents[m_decoratorEventNames[eventIndex]](m_nodeArgs[eventIndex]);
			if (!callResult) {
				callResults = false;
			}
		}
	}
	if(callResults && m_childNode){
		bool nodeResult = m_childNode->Run(deltaSeconds);
		if (m_childNode->m_nodeStatus == RUNNING) {
			m_nodeStatus = RUNNING;
			return true;
		}
		if (nodeResult) {
			if (m_nodeStatus != RUNNING) {
				m_nodeStatus = COMPLETE;
			}
			return true;
		}
	}
	
	m_nodeStatus = COMPLETE;
	return callResults;
}


//-----------------------------------------------------------------------------------------------
void DecoratorNode::ResetNodeStatus(){
	m_nodeStatus = READY;
	if(m_childNode){
		m_childNode->ResetNodeStatus();
	}
}


//-----------------------------------------------------------------------------------------------
void DecoratorNode::AddEvent(std::string eventName, EventArgs nodeArgs){
	m_decoratorEventNames.push_back(eventName);
	nodeArgs.SetValue("owner", m_behaviorTree->GetBehaviorTreeOwner());
	m_nodeArgs.push_back(nodeArgs);
}


//-----------------------------------------------------------------------------------------------
void DecoratorNode::SubscribeDecoratorEventFunction(std::string const& eventName, 
	DecoratorEventFunction functionPtr){
	s_subscribedDectoratornEvents[eventName] = functionPtr;
}


//-----------------------------------------------------------------------------------------------
void DecoratorNode::SubscribeCommonDecorators(){
	s_subscribedDectoratornEvents["AlwaysTrue"] = AlwaysTrue;
	s_subscribedDectoratornEvents["AlwaysFalse"] = AlwaysFalse;
}


