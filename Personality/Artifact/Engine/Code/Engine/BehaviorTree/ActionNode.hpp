#pragma once
#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"
#include "Engine/Core/NamedStrings.hpp"
#include <string>
#include <map>

//-----------------------------------------------------------------------------------------------
typedef NamedStrings EventArgs;
typedef bool (*ActionEventFunction)(EventArgs& args);

struct ActionNode : BehaviorTreeNode{
public:
	ActionNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	ActionNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode, EventArgs eventArgs);
	~ActionNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
	static void SubscribeActionEventFunction(std::string const& eventName, ActionEventFunction functionPtr);
public:
	static std::map<std::string, ActionEventFunction> s_subscribedActionEvents;
	std::string m_actionEventName;
	EventArgs m_nodeArgs;
};


//-----------------------------------------------------------------------------------------------
struct WaitNode : ActionNode {
	WaitNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode, float waitTime);
	~WaitNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
public:
	float m_totalTime = 0.f;
	float m_remainingTime = 0.f;
};