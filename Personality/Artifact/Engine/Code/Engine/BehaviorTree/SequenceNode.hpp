#pragma once
#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"
#include <string>
#include <vector>

//-----------------------------------------------------------------------------------------------
struct SequenceNode : BehaviorTreeNode{
public:
	SequenceNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode);
	~SequenceNode();

	virtual bool Run(float deltaSeconds) override;
	virtual void ResetNodeStatus() override;
	void AddChildNode(BehaviorTreeNode* node);
public:
	std::vector<BehaviorTreeNode*> m_childNodes;
};