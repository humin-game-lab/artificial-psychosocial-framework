#include "Engine/BehaviorTree/BehavorTreeCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"


//-----------------------------------------------------------------------------------------------
bool AlwaysTrue(NamedStrings& args){
	UNUSED(args);
	return true;
}


//-----------------------------------------------------------------------------------------------
bool AlwaysFalse(NamedStrings& args){
	UNUSED(args);
	return false;
}
