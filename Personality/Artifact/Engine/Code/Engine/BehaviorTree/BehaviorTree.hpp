#pragma once
#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"
#include "ThirdParty/TinyXML2/tinyxml2.h"
#include <string>


//-----------------------------------------------------------------------------------------------
class BehaviorTree {
public:
	BehaviorTree(std::string bhFileName, std::string ownerName);
	~BehaviorTree();

	void Run(float deltaSeconds);
	void Pause();
	void Stop();

	BehaviorTreeNodeStatus GetBehaviorTreeStatus() const {return m_root->m_nodeStatus;}
	std::string GetBehaviorTreeOwner() const {return m_owner;}
	void Reset();
	bool IsBehaviorTreePaused() const {return m_isPaused;}
	bool IsBehaviorTreeRunning() const{return (m_root->m_nodeStatus == RUNNING);}
private:
	void LoadBehaviorTreeFromFile();
	BehaviorTreeNode* LoadNodeFromFile(tinyxml2::XMLElement& elem, BehaviorTreeNode* parentNode);
	
private:
	RootNode* m_root = nullptr;
	bool m_isPaused = false;

	std::string m_fileName;
	std::string m_owner;
};