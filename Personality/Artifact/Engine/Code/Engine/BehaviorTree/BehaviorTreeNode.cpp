#include "Engine/BehaviorTree/BehaviorTreeNode.hpp"


//-----------------------------------------------------------------------------------------------
BehaviorTreeNode::BehaviorTreeNode(BehaviorTree* behaviorTree, std::string nodeName, 
	BehaviorTreeNode* parentNode){
	m_behaviorTree = behaviorTree;
	m_nodeName = nodeName;
	m_parentNode = parentNode;
}

//-----------------------------------------------------------------------------------------------
BehaviorTreeNode::~BehaviorTreeNode(){

}


//-----------------------------------------------------------------------------------------------
RootNode::RootNode(BehaviorTree* behaviorTree, std::string nodeName, BehaviorTreeNode* parentNode):
	BehaviorTreeNode(behaviorTree, nodeName, parentNode){

}


//-----------------------------------------------------------------------------------------------
RootNode::~RootNode(){
	if(m_childNode){
		delete m_childNode;
		m_childNode = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
bool RootNode::Run(float deltaSeconds){
	m_nodeStatus = RUNNING;
	if(m_childNode){
		bool runResult = m_childNode->Run(deltaSeconds);
		if (m_childNode->m_nodeStatus == COMPLETE) {
			ResetNodeStatus();
		}
		return runResult;
	}
	ResetNodeStatus();
	return true;
}


//-----------------------------------------------------------------------------------------------
void RootNode::ResetNodeStatus(){
	m_nodeStatus = READY;
	if(m_childNode){
		m_childNode->ResetNodeStatus();
	}
}


//-----------------------------------------------------------------------------------------------
void RootNode::AddChildNode(BehaviorTreeNode* node){
	m_childNode = node;
}
