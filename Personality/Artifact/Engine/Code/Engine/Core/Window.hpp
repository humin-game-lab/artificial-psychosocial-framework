#pragma once
#include <string>
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Math/IntVec2.hpp"

//-----------------------------------------------------------------------------------------------
class InputSystem;


//-----------------------------------------------------------------------------------------------
struct WindowConfig {
	InputSystem* m_inputSystem = nullptr;
	std::string  m_windowTitle = "Untitled App";
	float		 m_clientAspect = 2.0f;

};

//-----------------------------------------------------------------------------------------------
class Window {
public:
	Window(WindowConfig const& windowConfig);

	void Startup();
	void BeginFrame();
	void EndFrame();
	void Shutdown();

	void* GetDisplayContext()const;
	void* GetHwnd() const;
	IntVec2 GetDimensions() const;

	void    ShowMouse(bool show);     
	bool    IsMouseVisible() const;     
	void    LockMouse(bool lock);     
	bool    IsMouseLocked() const;
	IntVec2 GetClientCenter() const;

	void OnActivated();
	void OnDeactivated();
	bool HasFocus() const;

	IntVec2 GetMouseDesktopPosition() const;
	IntVec2 GeClientRect() const;
	void SetMouseDesktopPosition(IntVec2 const& pos);

	IntVec2 GetMouseClientPosition() const;
	void SetMouseClientPosition(IntVec2 const& pos);

	static Window* GetWindowContextFotWindowHandle(void* windowHandle);

protected:
	void CreateOSWindow();
	void RunMessagePump();

public:
	WindowConfig	m_config;

protected:
	static Window*		s_mainWindow;
	void*				m_windowHandle = nullptr;
	void*				m_displayDeviceContext = nullptr;
	IntVec2				m_dimensions;
	void*				m_Hwnd = nullptr;

	bool				m_isMouseSetToBeVisible = true;
	bool				m_isMouseSetToBeLocked = false;
	bool				m_isMouseLocked = false;
};