#pragma once
#include <string>
#include <vector>
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Core/Rgba8.hpp"

class Image {
public:
	Image();
	Image(char const* imageFilePath);
	std::string const&	GetImageFilePath() const;
	IntVec2				GetDimensions() const;
	Rgba8				GetTexelColor(IntVec2 const& texelCoords) const;
	void				SetTexelColor(IntVec2 const& texelCoords, Rgba8 const& newColor);
	void				SetupAsSolidColor(int width, int height, Rgba8 const& color);
	void*				GetRawData() const;
	int					GetBytesPerPixel() const;

private:
	std::string				m_imageFilePath;
	IntVec2					m_dimensions = IntVec2(0, 0);
	std::vector<Rgba8>		m_rgbaTexels;
};
