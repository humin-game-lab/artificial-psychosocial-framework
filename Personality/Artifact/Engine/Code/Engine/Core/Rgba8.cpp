#include "Engine/Core/Rgba8.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Math/Vec4.hpp"
//#include "Engine/Math/MathUtils.hpp"
//#include "Engine/Core/EngineCommon.hpp"

const Rgba8 Rgba8::WHITE(255,255,255);
const Rgba8 Rgba8::BLACK(0,0,0);
const Rgba8 Rgba8::YELLOW(255,255,0);
const Rgba8 Rgba8::RED(255,0,0);
const Rgba8 Rgba8::GREEN(0,255,0);
const Rgba8 Rgba8::DARK_GREY(50,50,50);
const Rgba8 Rgba8::MAGENTA(255,0,255);
const Rgba8 Rgba8::CYAN(0,255,255);
const Rgba8 Rgba8::ROYAL_BLUE(83, 51, 237);
const Rgba8 Rgba8::BLUE(0, 0, 255);

//-----------------------------------------------------------------------------------------------

Rgba8::Rgba8(uchar r1, uchar g1, uchar b1, uchar a1) {
	r = r1;
	g = g1;
	b = b1;
	a = a1;
}


//-----------------------------------------------------------------------------------------------
void Rgba8::SetFromText(const char* text){
	Strings floats = SplitStringOnDelimiter(text, ',');
	r = static_cast<uchar>(atof(floats[0].c_str()));
	g = static_cast<uchar>(atof(floats[1].c_str()));
	b = static_cast<uchar>(atof(floats[2].c_str()));

	if (static_cast<int>(floats.size()) == 4) {
		a = static_cast<uchar>(atof(floats[3].c_str()));
	}
}

//-----------------------------------------------------------------------------------------------
const float* Rgba8::GetAsFloats(float* emptyList) const{
	emptyList[0] = static_cast<float>(r)/255.f;
	emptyList[1] = static_cast<float>(g)/255.f;
	emptyList[2] = static_cast<float>(b)/255.f;
	emptyList[3] = static_cast<float>(a)/255.f;
	return emptyList;
}


//-----------------------------------------------------------------------------------------------
Vec4 Rgba8::GetAsFloat4() const{
	Vec4 result;
	result.x = static_cast<float>(r) / 255.f;
	result.y = static_cast<float>(g) / 255.f;
	result.z = static_cast<float>(b) / 255.f;
	result.w = static_cast<float>(a) / 255.f;
	return result;
}


//-----------------------------------------------------------------------------------------------
const Rgba8 Rgba8::Lerp(const Rgba8& startColor, const Rgba8& endColor, float lerpValue) {
	Rgba8 returnValue;
	returnValue.r = startColor.r + static_cast<uchar>(static_cast<float>(endColor.r - startColor.r)
		* (lerpValue));
	returnValue.g = startColor.g + static_cast<uchar>(static_cast<float>(endColor.g - startColor.g)
		* (lerpValue));
	returnValue.b = startColor.b + static_cast<uchar>(static_cast<float>(endColor.b - startColor.b)
		* (lerpValue));
	returnValue.a = startColor.a + static_cast<uchar>(static_cast<float>(endColor.a - startColor.a)
		* (lerpValue));
	return returnValue;
}

//-----------------------------------------------------------------------------------------------
const Rgba8 Rgba8::GetColorFromFloats(float* floatList){
	Rgba8 color;
	color.r = static_cast<uchar>(floatList[0] * 255.f);
	color.g = static_cast<uchar>(floatList[1] * 255.f);
	color.b = static_cast<uchar>(floatList[2] * 255.f);
	color.a = static_cast<uchar>(floatList[3] * 255.f);
	return color;
}


//-----------------------------------------------------------------------------------------------
void Rgba8::operator*=(float valueToMul){
	r = static_cast<uchar>(r * valueToMul);
	g = static_cast<uchar>(g * valueToMul);
	b = static_cast<uchar>(b * valueToMul);
}


//-----------------------------------------------------------------------------------------------
bool Rgba8::operator!=(const Rgba8& compare) const{
	return (r != compare.r) || (b != compare.b) || (g != compare.g) || (a != compare.a);
}

//-----------------------------------------------------------------------------------------------
bool Rgba8::operator==(const Rgba8& compare) const{
	return (r == compare.r) && (b == compare.b) && (g == compare.g);
}


//-----------------------------------------------------------------------------------------------
void Rgba8::operator=(const Rgba8& copyFrom) {
	r = copyFrom.r;
	g = copyFrom.g;
	b = copyFrom.b;
	a = copyFrom.a;
}


