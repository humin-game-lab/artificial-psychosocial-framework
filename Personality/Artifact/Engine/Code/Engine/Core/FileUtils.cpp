#include "Engine/Core/FileUtils.hpp"
#include <fstream>

//-----------------------------------------------------------------------------------------------
bool FileReadToBuffer(std::vector<uint8_t>& outBuffer, std::string const& filename){
	std::ifstream readFile(filename, std::ios::binary);
	if (!readFile.is_open() || readFile.peek() == std::ifstream::traits_type::eof()) {
		return false;
	}

	while (!readFile.eof()) {
		char charater;
		readFile.get(charater);
		if (!readFile) {
			break;
		}
		outBuffer.push_back(static_cast<uint8_t>(charater));
	}
	
	return true;
}


//-----------------------------------------------------------------------------------------------
bool FileWriteFromBuffer(std::vector<uint8_t> const& buffer, std::string const& filename){
	FILE* pFile;
	errno_t result = fopen_s(&pFile, filename.c_str(), "wb");
	if(result != 0){
		return false;
	}
	fwrite(buffer.data(), 1, buffer.size(), pFile);
	fclose(pFile);
	return true;
}


//-----------------------------------------------------------------------------------------------
bool FileReadToString(std::string& outString, std::string const& filename){
	std::ifstream readFile(filename);
	if (!readFile.is_open() || readFile.peek() == std::ifstream::traits_type::eof()) {
		return false;
	}
	while (!readFile.eof()) {
		char charater;
		readFile.get(charater);
		outString += charater;
	}

	return true;
}
