#pragma once
#include "Vertex_PCU.hpp"
#include "Engine/Math/AABB2.hpp"
#include <vector>

struct Capsule2;
struct OBB2;
struct LineSegment2;
struct AABB3;
struct FloatRange;
class SpriteSheet;

void TransformVertexArray2D(int numVerts, Vertex_PCU* verts, float uniformScaleXY,
	float rotationDegreesAboutZ, Vec2 const& translationXY);

void RotateVertexAboutPointXY3D(int numVerts, Vertex_PCU* verts, float rotationDegrees, Vec2 const& point);
void AddVertsForQuadrangle2D(std::vector<Vertex_PCU>& verts, Vec2 const& bl, Vec2 const& br, Vec2 const& tl, Vec2 const& tr, Rgba8 color);
void AddVertsForBox3D_RH(std::vector<Vertex_PCU>& verts, std::vector<Rgba8> color, Vec3 const& center, float widthX,
	float widthY, float heightZ, bool resize = true);
void AddVertsForCube3D_RH(std::vector<Vertex_PCU>& verts, Rgba8 color, Vec3 const& center, float width, bool resize = true, AABB2 const& UVs = AABB2::ZERO_TO_ONE);
void AddVertsForCube3D_RH(std::vector<Vertex_PCU>& verts, std::vector<Rgba8> color, Vec3 const& center, 
	float width, std::vector<AABB2> UVs, bool resize = true);
void AddVertsForAABB2In3D(std::vector<Vertex_PCU>& verts, std::vector<Vec3> bounds, Rgba8 const& color, AABB2 const& UVs = AABB2::ZERO_TO_ONE);
void AddVertsForAABB2BoundIn3D(std::vector<Vertex_PCU>& verts, std::vector<Vec3> bounds, Rgba8 const& color, AABB2 const& UVs = AABB2::ZERO_TO_ONE);
void AddVertsForSphere(std::vector<Vertex_PCU>& verts, Vec3 const& center, float radius, int longitudeCuts, int latitudeCuts,
	SpriteSheet const& spriteSheet);
void AddVertsForCylinder(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end, float radius, Rgba8 color, int numOfSlices = 64, AABB2 const& UVs = AABB2::ZERO_TO_ONE);
void AddVertsForLine3D(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end, float radius, Rgba8 color, int numOfSlices = 64, bool resizeVerts = true);
void AddVertsForArrow3D(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end, float radius, Rgba8 color, int numOfSlices = 64);
void AddVertsForBoxBound3D(std::vector<Vertex_PCU>& verts, AABB3 const& bounds, Rgba8 const& color, float radius, bool resizeVerts = true);
void AddVertsForBoxBound3D(std::vector<Vertex_PCU>& verts, Vec3 const& center, Vec3 const& forward, Vec3 const& left, Vec3 const& up, Rgba8 const& color, float radius, bool resizeVerts = true);

void AddVertsForAABBZ3D(std::vector<Vertex_PCU>& verts, AABB3 const& bounds, Rgba8 const& m_isTint = Rgba8::WHITE, AABB2 const& UVs = AABB2::ZERO_TO_ONE); 
void AddVertsForCylinderZ3D(std::vector<Vertex_PCU>& verts, Vec2 const& centerXY, FloatRange const& minMaxZ, float radius, float numSlices = 64.f, Rgba8 const& m_isTint = Rgba8::WHITE, AABB2 const& UVs = AABB2::ZERO_TO_ONE); 
void AddVertsForUVSphereZ3D(std::vector<Vertex_PCU>& verts, Vec3 const& center, float radius, float numSlices = 12.f, float numStacks = 12.f, Rgba8 const& m_isTint = Rgba8::WHITE, AABB2 const& UVs = AABB2::ZERO_TO_ONE, bool reserveVerts = true);

void AddVertsForAABB2D(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, Rgba8 const& color);
void AddVertsForAABB2DBound(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, float thinkness, Rgba8 const& color);
void AddVertsForAABB2D(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, Rgba8 const& color,
	Vec2 const& uvMins, Vec2 const& uvMaxs);
void AddVertsForDisc2D(std::vector<Vertex_PCU>& verts, Vec2 const& center, float radius, Rgba8 const& color);
void AddVertsForCone2D(std::vector<Vertex_PCU>& verts, Vec2 const& center, float startDegree, float endDegree, float radius, Rgba8 const& color);
void AddVertsForHalfDisc(std::vector<Vertex_PCU>& verts, Vec2 const& center, float startAngleDegree, float radius, 
	Rgba8 const& color);
void AddVertsForCapsule2D(std::vector<Vertex_PCU>& verts, Capsule2 const& capsule, Rgba8 const& color);
void AddVertsForCapsule2D(std::vector<Vertex_PCU>& verts, Vec2 const& boneStart, Vec2 const& boneEnd,
	float radius, Rgba8 const& color);
void AddVertsForOBB2D(std::vector<Vertex_PCU>& verts, OBB2 const& box, Rgba8 const& color);
void AddVertsForLineSegment2D(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end,
	float thickness, Rgba8 const& color);
void AddVertsForLineSegment2D(std::vector<Vertex_PCU>& verts, LineSegment2 const& lineSegment,
	float thickness, Rgba8 const& color);
void AddVertsForRing(Vertex_PCU* verts, Vec2 const& center, float radius,
	float thickness, Rgba8 const& color);
void AddVertsForRing(std::vector<Vertex_PCU>& verts, Vec2 const& center, float radius,
	float thickness, Rgba8 const& color);
void AddVertsForShorterLine(Vertex_PCU* verts, Vec2 const& start, Vec2 const& end, float thickness, 
	Rgba8 const& color);
void AddVertsForShorterLine(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end,
	float thickness, Rgba8 const& color);
void AddVertsForLine(Vertex_PCU* verts, Vec2 const& start, Vec2 const& end,
	float thickness, Rgba8 const& color);
void AddVertsForLine(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end,
	float thickness, Rgba8 const& color);
void AddVertsForArrow(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end,
	float thickness, float arrowSideLength, Rgba8 const& color);
