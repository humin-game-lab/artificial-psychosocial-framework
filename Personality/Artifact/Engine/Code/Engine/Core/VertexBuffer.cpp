#include "Engine/Core/VertexBuffer.hpp"
#include "Engine/Renderer/D3DInternal.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"


#define DX_SAFE_RELEASE(obj) if(obj!=nullptr) {(obj)->Release(); (obj) = nullptr;}

//------------------------------------------------------------------------------------------------
void VertexBuffer::CopyVertexData(void const* data, size_t byteCount){
	if (byteCount > m_byteMaxSize) {
		DX_SAFE_RELEASE(m_gpuBuffer);
		D3D11_BUFFER_DESC desc = {};
		desc.ByteWidth = (UINT)byteCount;
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		desc.MiscFlags = 0;
		desc.StructureByteStride = 0;

		m_source->m_device->CreateBuffer(&desc, nullptr, &m_gpuBuffer);
		ASSERT_OR_DIE(nullptr != m_gpuBuffer, "Failed to create vertex buffer");

		m_byteMaxSize = byteCount;
	}

	D3D11_MAPPED_SUBRESOURCE mapping;
	HRESULT result = m_source->m_context->Map(
		m_gpuBuffer,
		0,
		D3D11_MAP_WRITE_DISCARD,
		0,
		&mapping
	);

	ASSERT_OR_DIE(SUCCEEDED(result), "Failed to map buffer for write");
	memcpy(mapping.pData, data, byteCount);
	m_source->m_context->Unmap(m_gpuBuffer, 0);
	m_byteSize = byteCount;
}


//------------------------------------------------------------------------------------------------
VertexBuffer::VertexBuffer(Renderer* source, size_t const maxSize){
	m_source = source;
	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = (UINT)maxSize;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.MiscFlags = 0;
	desc.StructureByteStride = 0;

	source->m_device->CreateBuffer(&desc, nullptr, &m_gpuBuffer);
	ASSERT_OR_DIE(nullptr != m_gpuBuffer, "Failed to create vertex buffer");
}


//------------------------------------------------------------------------------------------------
VertexBuffer::~VertexBuffer(){

}


//------------------------------------------------------------------------------------------------
ID3D11Buffer* VertexBuffer::GetHandle() const{
	return m_gpuBuffer;
}
