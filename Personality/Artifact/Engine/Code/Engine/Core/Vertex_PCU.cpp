#include "Engine/Core/Vertex_PCU.hpp"
//#include "Engine/Math/MathUtils.hpp"
//#include "Engine/Core/EngineCommon.hpp"


//-----------------------------------------------------------------------------------------------
Vertex_PCU::Vertex_PCU(Vec3 const& position, Rgba8 const& m_isTint, Vec2 const& uvTexCoords){
	m_position = position;
	m_color = m_isTint;
	m_uvTexCoords = uvTexCoords;
}

