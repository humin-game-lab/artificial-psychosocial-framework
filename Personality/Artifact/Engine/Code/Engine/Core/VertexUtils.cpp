#include "VertexUtils.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/Capsule2.hpp"
#include "Engine/Math/OBB2.hpp"
#include "Engine/Math/LineSegment2.hpp"
#include "Engine/Math/Mat44.hpp"
#include "Engine/Math/Vec4.hpp"
#include "Engine/Math/AABB3.hpp"
#include "Engine/Math/FloatRange.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include <math.h>

//-----------------------------------------------------------------------------------------------
// function to do all the transformation changes
void TransformVertexArray2D(int numVerts, Vertex_PCU* verts, float uniformScaleXY,
	float rotationDegreesAboutZ, Vec2 const& translationXY) {
	// loop the entire array
	for (int vertIndex = 0; vertIndex < numVerts; vertIndex++) {
		Vec3 vertexPosition = verts[vertIndex].m_position;

		// calculate radian coordinates
		float r = vertexPosition.GetLength();
		float thetaDegrees = Atan2Degrees(vertexPosition.y, vertexPosition.x);

		// scale first
		r *= uniformScaleXY;

		// add degrees
		thetaDegrees += rotationDegreesAboutZ;

		// convert radians to Cartesian for rotation
		verts[vertIndex].m_position.x = r * CosDegrees(thetaDegrees);
		verts[vertIndex].m_position.y = r * SinDegrees(thetaDegrees);

		// last do translation
		verts[vertIndex].m_position.x += translationXY.x;
		verts[vertIndex].m_position.y += translationXY.y;
	}
}


//-----------------------------------------------------------------------------------------------
void RotateVertexAboutPointXY3D(int numVerts, Vertex_PCU* verts, 
	float rotationDegrees, Vec2 const& point){
	for (int vertIndex = 0; vertIndex < numVerts; vertIndex++) {
		// translate to point coordinates
		Vec2 displacement = Vec2(verts[vertIndex].m_position.x - point.x, 
			verts[vertIndex].m_position.y - point.y);

		// calculate radian coordinates
		float r = displacement.GetLength();
		float thetaDegrees = Atan2Degrees(displacement.y, displacement.x);

		// add degrees
		thetaDegrees += rotationDegrees;

		// convert radians to Cartesian and untranslate
		verts[vertIndex].m_position.x = r * CosDegrees(thetaDegrees) + point.x;
		verts[vertIndex].m_position.y = r * SinDegrees(thetaDegrees) + point.y;
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForQuadrangle2D(std::vector<Vertex_PCU>& verts, Vec2 const& bl, Vec2 const& br, 
	Vec2 const& tl, Vec2 const& tr, Rgba8 color){
	verts.push_back(Vertex_PCU(Vec3(bl.x, bl.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(br.x, br.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(tr.x, tr.y, 0.f), color, Vec2(0.f, 0.f)));

	verts.push_back(Vertex_PCU(Vec3(tr.x, tr.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(tl.x, tl.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(bl.x, bl.y, 0.f), color, Vec2(0.f, 0.f)));
}


//-----------------------------------------------------------------------------------------------
void AddVertsForBox3D_RH(std::vector<Vertex_PCU>& verts, std::vector<Rgba8> color, Vec3 const& center,
	float widthX, float widthY, float heightZ, bool resize) {
	if (resize) {
		constexpr int NUM_CUBE_TRIS = 6;
		constexpr int NUM_CUBE_VERTS = NUM_CUBE_TRIS * 3;
		verts.reserve(verts.size() + NUM_CUBE_VERTS);
	}

	float halfWidthX = widthX * 0.5f;
	float halfWidthY = widthY * 0.5f;
	float halfWidthZ = heightZ * 0.5f;

	
	// add +x side, which is +z -y
	Vec3 bottomLeft = center + Vec3(halfWidthX, -halfWidthY, -halfWidthZ); 
	Vec3 bottomRight = center + Vec3(halfWidthX, halfWidthY, -halfWidthZ);
	Vec3 topLeft = center + Vec3(halfWidthX, -halfWidthY, halfWidthZ); 
	Vec3 topRight = center + Vec3(halfWidthX, halfWidthY, halfWidthZ);
	std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[0]);

	// add -x side which is +z +y
	aabb2SideBound[0] = center + Vec3(-halfWidthX, halfWidthY, -halfWidthZ);
	aabb2SideBound[1] = center + Vec3(-halfWidthX, -halfWidthY, -halfWidthZ);
	aabb2SideBound[2] = center + Vec3(-halfWidthX, halfWidthY, halfWidthZ);
	aabb2SideBound[3] = center + Vec3(-halfWidthX, -halfWidthY, halfWidthZ);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[1]);

	// add +y side which is +z +x
	aabb2SideBound[0] = center + Vec3(halfWidthX, halfWidthY, -halfWidthZ);
	aabb2SideBound[1] = center + Vec3(-halfWidthX, halfWidthY, -halfWidthZ);
	aabb2SideBound[2] = center + Vec3(halfWidthX, halfWidthY, halfWidthZ);
	aabb2SideBound[3] = center + Vec3(-halfWidthX, halfWidthY, halfWidthZ);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[2]);

	// add -y side which is +z -x
	aabb2SideBound[0] = center + Vec3(-halfWidthX, -halfWidthY, -halfWidthZ);
	aabb2SideBound[1] = center + Vec3(halfWidthX, -halfWidthY, -halfWidthZ);
	aabb2SideBound[2] = center + Vec3(-halfWidthX, -halfWidthY, halfWidthZ);
	aabb2SideBound[3] = center + Vec3(halfWidthX, -halfWidthY, halfWidthZ);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[3]);

	// add +z side which is +x +y
	aabb2SideBound[0] = center + Vec3(-halfWidthX, halfWidthY, halfWidthZ);
	aabb2SideBound[1] = center + Vec3(-halfWidthX, -halfWidthY, halfWidthZ);
	aabb2SideBound[2] = center + Vec3(halfWidthX, halfWidthY, halfWidthZ);
	aabb2SideBound[3] = center + Vec3(halfWidthX, -halfWidthY, halfWidthZ);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[4]);

	// add -z side which is +x -y
	aabb2SideBound[0] = center + Vec3(-halfWidthX, -halfWidthY, -halfWidthZ);
	aabb2SideBound[1] = center + Vec3(-halfWidthX, halfWidthY, -halfWidthZ);
	aabb2SideBound[2] = center + Vec3(halfWidthX, -halfWidthY, -halfWidthZ);
	aabb2SideBound[3] = center + Vec3(halfWidthX, halfWidthY, -halfWidthZ);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[5]);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCube3D_RH(std::vector<Vertex_PCU>& verts, Rgba8 color, Vec3 const& center, 
	float width, bool resize, AABB2 const& UVs){
	if (resize) {
		constexpr int NUM_CUBE_TRIS = 6;
		constexpr int NUM_CUBE_VERTS = NUM_CUBE_TRIS * 3;
		verts.reserve(verts.size() + NUM_CUBE_VERTS);
	}

	float halfWidth = width * 0.5f;


	// add +x side, which is +z -y
	Vec3 bottomLeft = center + Vec3(halfWidth, -halfWidth, -halfWidth); 
	Vec3 bottomRight = center + Vec3(halfWidth, halfWidth, -halfWidth);
	Vec3 topLeft = center + Vec3(halfWidth, -halfWidth, halfWidth); 
	Vec3 topRight = center + Vec3(halfWidth, halfWidth, halfWidth);
	std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);

	// add -x side which is +z +y
	aabb2SideBound[0] = center + Vec3(-halfWidth, halfWidth, -halfWidth); 
	aabb2SideBound[1] = center + Vec3(-halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(-halfWidth, halfWidth, halfWidth); 
	aabb2SideBound[3] = center + Vec3(-halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);

	// add +y side which is +z +x
	aabb2SideBound[0] = center + Vec3(halfWidth, halfWidth, -halfWidth); 
	aabb2SideBound[1] = center + Vec3(-halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, halfWidth, halfWidth); 
	aabb2SideBound[3] = center + Vec3(-halfWidth, halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);

	// add -y side which is +z -x
	aabb2SideBound[0] = center + Vec3(-halfWidth, -halfWidth, -halfWidth); 
	aabb2SideBound[1] = center + Vec3(halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(-halfWidth, -halfWidth, halfWidth); 
	aabb2SideBound[3] = center + Vec3(halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);

	// add +z side which is +x +y
	aabb2SideBound[0] = center + Vec3(-halfWidth, halfWidth, halfWidth); 
	aabb2SideBound[1] = center + Vec3(-halfWidth, -halfWidth, halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, halfWidth, halfWidth);
	aabb2SideBound[3] = center + Vec3(halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);

	// add -z side which is +x -y
	aabb2SideBound[0] = center + Vec3(-halfWidth, -halfWidth, -halfWidth); 
	aabb2SideBound[1] = center + Vec3(-halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, -halfWidth, -halfWidth); 
	aabb2SideBound[3] = center + Vec3(halfWidth, halfWidth, -halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color, UVs);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCube3D_RH(std::vector<Vertex_PCU>& verts, std::vector<Rgba8> color, 
	Vec3 const& center, float width, std::vector<AABB2> UVs, bool resize){
	if (resize) {
		constexpr int NUM_CUBE_TRIS = 6;
		constexpr int NUM_CUBE_VERTS = NUM_CUBE_TRIS * 3;
		verts.reserve(verts.size() + NUM_CUBE_VERTS);
	}

	float halfWidth = width * 0.5f;


	// add +x side, which is +z -y
	Vec3 bottomLeft = center + Vec3(halfWidth, -halfWidth, -halfWidth);
	Vec3 bottomRight = center + Vec3(halfWidth, halfWidth, -halfWidth);
	Vec3 topLeft = center + Vec3(halfWidth, -halfWidth, halfWidth);
	Vec3 topRight = center + Vec3(halfWidth, halfWidth, halfWidth);
	std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[0], UVs[0]);

	// add -x side which is +z +y
	aabb2SideBound[0] = center + Vec3(-halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[1] = center + Vec3(-halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(-halfWidth, halfWidth, halfWidth);
	aabb2SideBound[3] = center + Vec3(-halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[1], UVs[1]);

	// add +y side which is +z +x
	aabb2SideBound[0] = center + Vec3(halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[1] = center + Vec3(-halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, halfWidth, halfWidth);
	aabb2SideBound[3] = center + Vec3(-halfWidth, halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[2], UVs[2]);

	// add -y side which is +z -x
	aabb2SideBound[0] = center + Vec3(-halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[1] = center + Vec3(halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(-halfWidth, -halfWidth, halfWidth);
	aabb2SideBound[3] = center + Vec3(halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[3], UVs[3]);

	// add +z side which is +x +y
	aabb2SideBound[0] = center + Vec3(-halfWidth, halfWidth, halfWidth);
	aabb2SideBound[1] = center + Vec3(-halfWidth, -halfWidth, halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, halfWidth, halfWidth);
	aabb2SideBound[3] = center + Vec3(halfWidth, -halfWidth, halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[4], UVs[4]);

	// add -z side which is +x -y
	aabb2SideBound[0] = center + Vec3(-halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[1] = center + Vec3(-halfWidth, halfWidth, -halfWidth);
	aabb2SideBound[2] = center + Vec3(halfWidth, -halfWidth, -halfWidth);
	aabb2SideBound[3] = center + Vec3(halfWidth, halfWidth, -halfWidth);
	AddVertsForAABB2In3D(verts, aabb2SideBound, color[5], UVs[5]);
}




//-----------------------------------------------------------------------------------------------
// in this function, x, y are relative axis that change, z is fixed axis that doesn't change
void AddVertsForAABB2In3D(std::vector<Vertex_PCU>& verts,std::vector<Vec3> bounds, Rgba8 const& color,
	AABB2 const& UVs){
	Vec3 bottomLeft = bounds[0];
	Vec3 bottomRight = bounds[1];
	Vec3 topLeft = bounds[2];
	Vec3 topRight = bounds[3];

	verts.push_back(Vertex_PCU(topLeft, color, Vec2(UVs.m_mins.x, UVs.m_maxs.y)));
	verts.push_back(Vertex_PCU(bottomLeft, color, UVs.m_mins));
	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(UVs.m_maxs.x, UVs.m_mins.y)));

	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(UVs.m_maxs.x, UVs.m_mins.y)));
	verts.push_back(Vertex_PCU(topRight, color, UVs.m_maxs));
	verts.push_back(Vertex_PCU(topLeft, color, Vec2(UVs.m_mins.x, UVs.m_maxs.y)));
}



//-----------------------------------------------------------------------------------------------
void AddVertsForAABB2BoundIn3D(std::vector<Vertex_PCU>& verts, std::vector<Vec3> bounds, 
	Rgba8 const& color, AABB2 const& UVs){
	UNUSED(UVs);
	Vec3 bottomLeft = bounds[0];
	Vec3 bottomRight = bounds[1];
	Vec3 topLeft = bounds[2];
	Vec3 topRight = bounds[3];

	AddVertsForLine3D(verts, bottomLeft, bottomRight, 0.03f, color, 12, false);
	AddVertsForLine3D(verts, bottomRight, topRight, 0.03f, color, 12, false);
	AddVertsForLine3D(verts, topRight, topLeft, 0.03f, color, 12, false);
	AddVertsForLine3D(verts, topLeft, bottomLeft, 0.03f, color, 12, false);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForSphere(std::vector<Vertex_PCU>& verts, Vec3 const& center, float radius,int longitudeCuts,
	int latitudeCuts, SpriteSheet const& spriteSheet){
	int numOfVerts = (latitudeCuts +1) * (longitudeCuts+1) * 2 * 3;
	verts.reserve(verts.size() + numOfVerts);

	float longSectionDegrees = 360.f / static_cast<float>(longitudeCuts);
	float latSectionDegrees = 180.f / static_cast<float>(latitudeCuts);

	int uvIndex = 0;
	

	for (int latIndex = 0; latIndex < latitudeCuts; latIndex++) {

		float currentLatDegrees = 90.f - latSectionDegrees * latIndex;
		float nextLatDegrees = currentLatDegrees - latSectionDegrees;

		for (int longIndex = 0; longIndex < longitudeCuts; longIndex++) {
			float currentLongDegrees = longSectionDegrees * longIndex;
			float nextLongDegrees = currentLongDegrees + longSectionDegrees;
		
			Vec3 bottomLeft = center + radius * GetPointOnSphere(currentLongDegrees, currentLatDegrees);
			Vec3 topLeft = center + radius * GetPointOnSphere(currentLongDegrees, nextLatDegrees);
			Vec3 bottomRight = center + radius * GetPointOnSphere(nextLongDegrees, currentLatDegrees);
			Vec3 topRight = center + radius * GetPointOnSphere(nextLongDegrees, nextLatDegrees);

			Vec2 uvMins, uvMaxs;
 			spriteSheet.GetSpriteUVs(uvMins, uvMaxs, uvIndex);

			verts.push_back(Vertex_PCU(bottomLeft, Rgba8::WHITE, uvMins));
			verts.push_back(Vertex_PCU(bottomRight, Rgba8::WHITE, Vec2(uvMaxs.x, uvMins.y)));
			verts.push_back(Vertex_PCU(topLeft, Rgba8::WHITE, Vec2(uvMins.x, uvMaxs.y)));

			verts.push_back(Vertex_PCU(bottomRight, Rgba8::WHITE, Vec2(uvMaxs.x, uvMins.y)));
			verts.push_back(Vertex_PCU(topRight, Rgba8::WHITE, uvMaxs));
			verts.push_back(Vertex_PCU(topLeft, Rgba8::WHITE, Vec2(uvMins.x, uvMaxs.y)));
			uvIndex++;
		}
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCylinder(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end, 
	float radius, Rgba8 color, int numOfSlices, AABB2 const& UVs) {
	int numTris = numOfSlices * 4;
	int numVerts = 3 * numTris;
	float degreePerSide = 360.f / (float)numOfSlices;
	verts.reserve(verts.size() + numVerts);

	float uvDistance = UVs.m_maxs.x - UVs.m_mins.x;
	std::vector<Vec2>outerPosition;
	std::vector<Vec2>uvPosition;
	std::vector<Vec2>uvPositionClockWise;
	for (int traingleIndex = 0; traingleIndex < numOfSlices; traingleIndex++) {
		float angleDegrees = traingleIndex * degreePerSide;
		outerPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, radius));
		uvPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, uvDistance * 0.5f));
		uvPositionClockWise.push_back(Vec2::MakeFromPolarDegrees(360.f - angleDegrees, uvDistance * 0.5f));
	}

	float height = (end - start).GetLength();
	Vec3 center = (end - start) * 0.5f;
	for (int vertIndex = 0; vertIndex < numOfSlices; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % numOfSlices;

		// top disc
		Vec3 topCenter = center;
		topCenter.z += height * 0.5f;
		Vec2 uvCenter = UVs.GetCenter();
		verts.push_back(Vertex_PCU(topCenter, color, uvCenter));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, topCenter.z), color, 
			uvPosition[startRadiusIndex] + uvCenter));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, topCenter.z), color, 
			uvPosition[endRadiusIndex] + uvCenter));

		// bottom disc
		Vec3 bottomCenter = center;
		bottomCenter.z -= height * 0.5f;
		verts.push_back(Vertex_PCU(bottomCenter, color, uvCenter));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, bottomCenter.z), color, 
			uvPositionClockWise[endRadiusIndex] + uvCenter));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, bottomCenter.z), color, 
			uvPositionClockWise[startRadiusIndex] + uvCenter));


		// body
 		float uvSlice = uvDistance / static_cast<float>(numOfSlices);
		AABB2 bodyUV(Vec2(static_cast<float>(startRadiusIndex) * uvSlice, UVs.m_mins.y),
			Vec2(static_cast<float>(endRadiusIndex) * uvSlice, UVs.m_maxs.y));
		if (startRadiusIndex > endRadiusIndex) {
			bodyUV.m_maxs = UVs.m_maxs;
		}
		Vec3 bottomLeft = Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 bottomRight = Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 topLeft = Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, topCenter.z);
		Vec3 topRight = Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, topCenter.z);
		std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
		AddVertsForAABB2In3D(verts, aabb2SideBound, color, bodyUV);
	}

	// move cylinder to start point
	Vec3 translationTobottomPoint = verts[3].m_position - start;
	for (int vertIndex = 0; vertIndex < numVerts; vertIndex++) {
		verts[vertIndex].m_position -= translationTobottomPoint;
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForLine3D(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end,
	float radius, Rgba8 color, int numOfSlices, bool resizeVerts) {
	int numTris = numOfSlices * 4;
	int numVerts = 3 * numTris;
	float degreePerSide = 360.f / (float)numOfSlices;
	int vertsPreviousSize = static_cast<int>(verts.size());
	if(resizeVerts){
		verts.reserve(verts.size() + numVerts);
	}

	Vec3 endPoint = end - start;
	Vec3 startPoint = Vec3::ZERO;

	float coneRadius = radius * 2.f;
	std::vector<Vec2>lineOuterPosition;
	std::vector<Vec2>coneOuterPosition;
	for (int traingleIndex = 0; traingleIndex < numOfSlices; traingleIndex++) {
		float angleDegrees = traingleIndex * degreePerSide;
		lineOuterPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, radius));
		coneOuterPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, coneRadius));
	}

	float lineHeight = (endPoint - startPoint).GetLength();
	Vec3 center = startPoint + (endPoint - startPoint).GetNormalized() * lineHeight;
	for (int vertIndex = 0; vertIndex < numOfSlices; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % numOfSlices;

		// top disc
		Vec3 topCenter = center;
		topCenter.z += lineHeight * 0.5f;
		verts.push_back(Vertex_PCU(topCenter, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));

		// bottom disc
		Vec3 bottomCenter = center;
		bottomCenter.z -= lineHeight * 0.5f;
		verts.push_back(Vertex_PCU(bottomCenter, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, bottomCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, bottomCenter.z), color, Vec2(0.f, 0.f)));


		// body
		Vec3 bottomLeft = Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 bottomRight = Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 topLeft = Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, topCenter.z);
		Vec3 topRight = Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, topCenter.z);
		std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
		AddVertsForAABB2In3D(verts, aabb2SideBound, color);
	}


	Vec3 translationTobottomPoint = verts[vertsPreviousSize + 3].m_position;
	for (int vertIndex = vertsPreviousSize; vertIndex < static_cast<int>(verts.size()); vertIndex++) {
		verts[vertIndex].m_position -= translationTobottomPoint;
	}
	//rotate cylinder
	Mat44 transpose;
	float rotationX = (Vec2(endPoint.y, endPoint.z) - Vec2(startPoint.y, startPoint.z)).GetOrientationDegrees() - 90.f;
	transpose.AppendXRotation(rotationX);
	Vec3 topPoint = transpose.TransformPosition3D(verts[vertsPreviousSize].m_position);
	float rotationY = GetAngleDegreesBetweenVectors3D(endPoint, topPoint);
	if (endPoint.x < topPoint.x) {
		rotationY = -rotationY;
	}
	transpose.AppendYRotation(rotationY);

	// transpose the cylinder location
	for (int vertIndex = vertsPreviousSize; vertIndex < static_cast<int>(verts.size()); vertIndex++) {
		verts[vertIndex].m_position = transpose.TransformPosition3D(verts[vertIndex].m_position) + start;
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForArrow3D(std::vector<Vertex_PCU>& verts, Vec3 const& start, Vec3 const& end, 
	float radius, Rgba8 color, int numOfSlices){
	int numTris = numOfSlices * 6;
	int numVerts = 3 * numTris;
	float degreePerSide = 360.f / (float)numOfSlices;
	int vertsPreviousSize = static_cast<int>(verts.size());
	verts.reserve(verts.size() + numVerts);

	Vec3 endPoint = end - start;
	Vec3 startPoint = Vec3::ZERO;

	float coneRadius = radius * 2.f;
	std::vector<Vec2>lineOuterPosition;
	std::vector<Vec2>coneOuterPosition;
	for (int traingleIndex = 0; traingleIndex < numOfSlices; traingleIndex++) {
		float angleDegrees = traingleIndex * degreePerSide;
		lineOuterPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, radius));
		coneOuterPosition.push_back(Vec2::MakeFromPolarDegrees(angleDegrees, coneRadius));
	}

	float height = (endPoint - startPoint).GetLength();
	float coneHeight = coneRadius * 2.f;
	coneHeight = Clamp(coneHeight, 0.f, height * 0.5f);
	float lineHeight = height - coneHeight;
	Vec3 center = startPoint + (endPoint - startPoint).GetNormalized() * lineHeight;
	for (int vertIndex = 0; vertIndex < numOfSlices; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % numOfSlices;

		// top disc
		Vec3 topCenter = center;
		topCenter.z += lineHeight * 0.5f;
		verts.push_back(Vertex_PCU(topCenter, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));

		// bottom disc
		Vec3 bottomCenter = center;
		bottomCenter.z -= lineHeight * 0.5f;
		verts.push_back(Vertex_PCU(bottomCenter, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, bottomCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, bottomCenter.z), color, Vec2(0.f, 0.f)));


		// body
		Vec3 bottomLeft = Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 bottomRight = Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, bottomCenter.z);
		Vec3 topLeft = Vec3(lineOuterPosition[startRadiusIndex].x + center.x,
			lineOuterPosition[startRadiusIndex].y + center.y, topCenter.z);
		Vec3 topRight = Vec3(lineOuterPosition[endRadiusIndex].x + center.x,
			lineOuterPosition[endRadiusIndex].y + center.y, topCenter.z);
		std::vector<Vec3> aabb2SideBound = { bottomLeft, bottomRight, topLeft,topRight };
		AddVertsForAABB2In3D(verts, aabb2SideBound, color);

		verts.push_back(Vertex_PCU(topCenter, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(coneOuterPosition[endRadiusIndex].x + center.x,
			coneOuterPosition[endRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(coneOuterPosition[startRadiusIndex].x + center.x,
			coneOuterPosition[startRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));

		Vec3 coneTop = topCenter;
		coneTop.z += coneHeight;
		verts.push_back(Vertex_PCU(coneTop, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(coneOuterPosition[startRadiusIndex].x + center.x,
			coneOuterPosition[startRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(coneOuterPosition[endRadiusIndex].x + center.x,
			coneOuterPosition[endRadiusIndex].y + center.y, topCenter.z), color, Vec2(0.f, 0.f)));
	}


	Vec3 translationTobottomPoint = verts[vertsPreviousSize + 3].m_position;
	for (int vertIndex = vertsPreviousSize; vertIndex < static_cast<int>(verts.size()); vertIndex++) {
		verts[vertIndex].m_position -= translationTobottomPoint;
	}
	//rotate cylinder
	Mat44 transpose;
	float rotationX = (Vec2(endPoint.y, endPoint.z) - Vec2(startPoint.y, startPoint.z)).GetOrientationDegrees() - 90.f;
	transpose.AppendXRotation(rotationX);
	Vec3 topPoint = transpose.TransformPosition3D(verts[vertsPreviousSize + 15].m_position);
	float rotationY = GetAngleDegreesBetweenVectors3D(endPoint, topPoint);
	if (endPoint.x < topPoint.x) {
		rotationY = -rotationY;
	}
	transpose.AppendYRotation(rotationY);

	// transpose the cylinder location
	for (int vertIndex = vertsPreviousSize; vertIndex < static_cast<int>(verts.size()); vertIndex++) {
		verts[vertIndex].m_position = transpose.TransformPosition3D(verts[vertIndex].m_position) + start;
	}

}


//-----------------------------------------------------------------------------------------------
void AddVertsForBoxBound3D(std::vector<Vertex_PCU>& verts, AABB3 const& bounds, Rgba8 const& color, 
	float radius, bool resizeVerts){
	if (resizeVerts) {
		verts.reserve(18432);
	}
	Vec3 lowerBottomLeft = bounds.m_mins;
	Vec3 lowerBottomRight(bounds.m_maxs.x, bounds.m_mins.y, bounds.m_mins.z);
	Vec3 lowerTopLeft(bounds.m_mins.x, bounds.m_maxs.y, bounds.m_mins.z);
	Vec3 lowerTopRight(bounds.m_maxs.x, bounds.m_maxs.y, bounds.m_mins.z);

	Vec3 higherBottomLeft(bounds.m_mins.x, bounds.m_mins.y, bounds.m_maxs.z);
	Vec3 higherBottomRight(bounds.m_maxs.x, bounds.m_mins.y, bounds.m_maxs.z);
	Vec3 higherTopLeft(bounds.m_mins.x, bounds.m_maxs.y, bounds.m_maxs.z);
	Vec3 higherTopRight = bounds.m_maxs;

	AddVertsForLine3D(verts, lowerBottomLeft, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerBottomLeft, lowerBottomRight, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerBottomLeft, lowerTopLeft, radius, color, 64, false);

	AddVertsForLine3D(verts, lowerTopRight, lowerTopLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerTopRight, lowerBottomRight, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerTopRight, higherTopRight, radius, color, 64, false);

	AddVertsForLine3D(verts, higherTopLeft, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherTopLeft, lowerTopLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherTopLeft, higherTopRight, radius, color, 64, false);

	AddVertsForLine3D(verts, higherBottomRight, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherBottomRight, higherTopRight, radius, color, 64, false);
	AddVertsForLine3D(verts, higherBottomRight, lowerBottomRight, radius, color, 64, false);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForBoxBound3D(std::vector<Vertex_PCU>& verts, Vec3 const& center, Vec3 const& forward, 
	Vec3 const& left, Vec3 const& up, Rgba8 const& color, float radius, bool resizeVerts){
	if (resizeVerts) {
		verts.reserve(18432);
	}
	Vec3 lowerBottomLeft = center - forward + left - up;
	Vec3 lowerBottomRight = lowerBottomLeft - left * 2.f;
	Vec3 lowerTopLeft = lowerBottomLeft + forward * 2.f;
	Vec3 lowerTopRight = lowerBottomRight + forward * 2.f;

	Vec3 higherBottomLeft = lowerBottomLeft + up * 2.f;
	Vec3 higherBottomRight = higherBottomLeft - left * 2.f;
	Vec3 higherTopLeft = higherBottomLeft + forward * 2.f;
	Vec3 higherTopRight = higherBottomRight + forward * 2.f;

	AddVertsForLine3D(verts, lowerBottomLeft, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerBottomLeft, lowerBottomRight, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerBottomLeft, lowerTopLeft, radius, color, 64, false);

	AddVertsForLine3D(verts, lowerTopRight, lowerTopLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerTopRight, lowerBottomRight, radius, color, 64, false);
	AddVertsForLine3D(verts, lowerTopRight, higherTopRight, radius, color, 64, false);

	AddVertsForLine3D(verts, higherTopLeft, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherTopLeft, lowerTopLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherTopLeft, higherTopRight, radius, color, 64, false);

	AddVertsForLine3D(verts, higherBottomRight, higherBottomLeft, radius, color, 64, false);
	AddVertsForLine3D(verts, higherBottomRight, higherTopRight, radius, color, 64, false);
	AddVertsForLine3D(verts, higherBottomRight, lowerBottomRight, radius, color, 64, false);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForAABBZ3D(std::vector<Vertex_PCU>& verts, AABB3 const& bounds, Rgba8 const& m_isTint, 
	AABB2 const& UVs){
	Vec3 center = bounds.GetCenter();
	float width = bounds.m_maxs.x - bounds.m_mins.x;
	AddVertsForCube3D_RH(verts, m_isTint, center, width, true, UVs);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCylinderZ3D(std::vector<Vertex_PCU>& verts, Vec2 const& centerXY, 
	FloatRange const& minMaxZ, float radius, float numSlices, Rgba8 const& m_isTint, AABB2 const& UVs){
	UNUSED(UVs);
	AddVertsForCylinder(verts, Vec3(centerXY.x, centerXY.y, minMaxZ.m_min),
		Vec3(centerXY.x, centerXY.y, minMaxZ.m_max), radius, m_isTint, static_cast<int>(numSlices),
		UVs);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForUVSphereZ3D(std::vector<Vertex_PCU>& verts, Vec3 const& center, float radius, 
	float numSlices, float numStacks, Rgba8 const& m_isTint, AABB2 const& UVs, bool reserveVerts){
	int numOfVerts = (static_cast<int>(numSlices)+ 1) * (static_cast<int>(numStacks) + 1) * 2 * 3;
	if (reserveVerts) {
	verts.reserve(verts.size() + numOfVerts);
	}

	float longSectionDegrees = 360.f / numStacks;
	float latSectionDegrees = 180.f / numSlices;

	std::vector<std::pair<Vec2, Vec2>> uvs;
	float uForEachSprite = UVs.m_maxs.x / numSlices;
	float vForEachSprite = UVs.m_maxs.y / numStacks;
	for (int vIndex = static_cast<int>(numStacks); vIndex > 0; vIndex--) {
		for (int uIndex = 1; uIndex <= static_cast<int>(numSlices); uIndex++) {
			float uOffSet = 1.f / (numSlices * 100.f);
			float vOffSet = 1.f / (numStacks * 100.f);
			Vec2 uvMins(static_cast<float>(uIndex - 1) * uForEachSprite + uOffSet + UVs.m_mins.x,
				static_cast<float>(vIndex - 1) * vForEachSprite + vOffSet + UVs.m_mins.y);
			Vec2 uvMaxs(static_cast<float>(uIndex) * uForEachSprite - uOffSet + UVs.m_mins.x,
				static_cast<float>(vIndex) * vForEachSprite - vOffSet + UVs.m_mins.y);
			uvs.push_back(std::make_pair(uvMins, uvMaxs));
		}
	}

	int uvIndex = 0;
	for (int latIndex = 0; latIndex < numSlices; latIndex++) {

		float currentLatDegrees = -90.f + latSectionDegrees * latIndex;
		float nextLatDegrees = currentLatDegrees + latSectionDegrees;

		for (int longIndex = 0; longIndex < numStacks; longIndex++) {
			float currentLongDegrees = longSectionDegrees * longIndex;
			float nextLongDegrees = currentLongDegrees + longSectionDegrees;

			Vec3 topLeft = center + radius * GetPointOnSphere(currentLongDegrees, currentLatDegrees);
			Vec3 bottomLeft = center + radius * GetPointOnSphere(currentLongDegrees, nextLatDegrees);
			Vec3 topRight = center + radius * GetPointOnSphere(nextLongDegrees, currentLatDegrees);
			Vec3 bottomRight = center + radius * GetPointOnSphere(nextLongDegrees, nextLatDegrees);

			Vec2 uvMins = uvs[uvIndex].first;
			Vec2 uvMaxs = uvs[uvIndex].second;
			verts.push_back(Vertex_PCU(bottomLeft, m_isTint, uvMins));
			verts.push_back(Vertex_PCU(bottomRight, m_isTint, Vec2(uvMaxs.x, uvMins.y)));
			verts.push_back(Vertex_PCU(topLeft, m_isTint, Vec2(uvMins.x, uvMaxs.y)));

			verts.push_back(Vertex_PCU(bottomRight, m_isTint, Vec2(uvMaxs.x, uvMins.y)));
			verts.push_back(Vertex_PCU(topRight, m_isTint, uvMaxs));
			verts.push_back(Vertex_PCU(topLeft, m_isTint, Vec2(uvMins.x, uvMaxs.y)));
			uvIndex++;
		}
	}
}

//-----------------------------------------------------------------------------------------------
void AddVertsForAABB2D(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, Rgba8 const& color){
	constexpr int NUM_AABB2_TRIS = 2;
	constexpr int NUM_AABB2_VERTS = NUM_AABB2_TRIS * 3;
	verts.reserve(verts.size() + NUM_AABB2_VERTS);

	Vec3 bottomLeft = Vec3(bounds.m_mins.x, bounds.m_mins.y, 0.f);
	Vec3 bottomRight = Vec3(bounds.m_maxs.x, bounds.m_mins.y, 0.f);
	Vec3 topLeft = Vec3(bounds.m_mins.x, bounds.m_maxs.y, 0.f);
	Vec3 topRight = Vec3(bounds.m_maxs.x, bounds.m_maxs.y, 0.f);

	verts.push_back(Vertex_PCU(bottomLeft, color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(1.f, 0.f)));
	verts.push_back(Vertex_PCU(topLeft, color, Vec2(0.f, 1.f)));

	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(1.f, 0.f)));
	verts.push_back(Vertex_PCU(topLeft, color, Vec2(0.f, 1.f)));
	verts.push_back(Vertex_PCU(topRight, color, Vec2(1.f, 1.f)));

}


//-----------------------------------------------------------------------------------------------
void AddVertsForAABB2D(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, Rgba8 const& color, 
	Vec2 const& uvMins, Vec2 const& uvMaxs) {
	constexpr int NUM_AABB2_TRIS = 2;
	constexpr int NUM_AABB2_VERTS = NUM_AABB2_TRIS * 3;
	verts.reserve(verts.size() + NUM_AABB2_VERTS);

	Vec3 bottomLeft = Vec3(bounds.m_mins.x, bounds.m_mins.y, 0.f);
	Vec3 bottomRight = Vec3(bounds.m_maxs.x, bounds.m_mins.y, 0.f);
	Vec3 topLeft = Vec3(bounds.m_mins.x, bounds.m_maxs.y, 0.f);
	Vec3 topRight = Vec3(bounds.m_maxs.x, bounds.m_maxs.y, 0.f);


	verts.push_back(Vertex_PCU(topLeft, color, Vec2(uvMins.x, uvMaxs.y)));
	verts.push_back(Vertex_PCU(bottomLeft, color, Vec2(uvMins.x, uvMins.y)));
	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(uvMaxs.x, uvMins.y)));

	verts.push_back(Vertex_PCU(bottomRight, color, Vec2(uvMaxs.x, uvMins.y)));
	verts.push_back(Vertex_PCU(topRight, color, Vec2(uvMaxs.x, uvMaxs.y)));
	verts.push_back(Vertex_PCU(topLeft, color, Vec2(uvMins.x, uvMaxs.y)));
}


//-----------------------------------------------------------------------------------------------
void AddVertsForAABB2DBound(std::vector<Vertex_PCU>& verts, AABB2 const& bounds, float thinkness,  Rgba8 const& color){
	Vec2 bottomLeft(bounds.m_mins);
	Vec2 bottomRight(bounds.m_maxs.x, bounds.m_mins.y);
	Vec2 topLeft(bounds.m_mins.x, bounds.m_maxs.y);
	Vec2 topRight(bounds.m_maxs);
	AddVertsForLine(verts, bottomLeft, bottomRight, thinkness, color);
	AddVertsForLine(verts, bottomRight, topRight, thinkness, color);
	AddVertsForLine(verts, topRight, topLeft, thinkness, color);
	AddVertsForLine(verts, topLeft, bottomLeft, thinkness, color);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForDisc2D(std::vector<Vertex_PCU>& verts, Vec2 const& center, float radius, Rgba8 const& color){
	constexpr int NUM_SIDES = 20;
	constexpr int NUM_TRIS = NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	Vec2 outerPosition[NUM_SIDES];
	for (int traingleIndex = 0; traingleIndex < NUM_TRIS; traingleIndex++) {
		float angleDegrees = traingleIndex * DEGREE_PER_SIDE;
		outerPosition[traingleIndex] = Vec2::MakeFromPolarDegrees(angleDegrees, radius);
	}
	for (int vertIndex = 0; vertIndex < NUM_TRIS; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % NUM_TRIS;

		verts.push_back(Vertex_PCU(Vec3(center.x, center.y, 0), color, Vec2(0.f,0.f)));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, 0), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, 0), color, Vec2(0.f, 0.f)));
	}
}

//-----------------------------------------------------------------------------------------------
void AddVertsForCone2D(std::vector<Vertex_PCU>& verts, Vec2 const& center, float startDegree, 
	float endDegree, float radius, Rgba8 const& color){
	constexpr float SIDES = 16.f;
	float degreePerSide = (endDegree - startDegree)/SIDES;
	for (int sideNum = 0; sideNum < static_cast<int>(SIDES); sideNum++) {
		// compute angle-related terms
		float startDegrees = startDegree + degreePerSide * static_cast<float>(sideNum);
		float endDegrees = startDegree + degreePerSide * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute start and end position
		Vec3 startPos = Vec3(center.x + radius * cosStart,
			center.y + radius * sinStart, 0.f);
		Vec3 endPos = Vec3(center.x + radius * cosEnd,
			center.y + radius * sinEnd, 0.f);

		verts.push_back(Vertex_PCU(Vec3(center.x, center.y, 0.f), color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(startPos, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(endPos, color, Vec2(0.f, 0.f)));
	}
}

//-----------------------------------------------------------------------------------------------
// add vertexes for a half disc based on start degrees
void AddVertsForHalfDisc(std::vector<Vertex_PCU>& verts, Vec2 const& center, float startAngleDegree, 
	float radius, Rgba8 const& color) {

	constexpr int NUM_SIDES = 20;
	constexpr int NUM_TRIS = NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 180.f / (float)NUM_SIDES;

	for (int sideNum = 0; sideNum < NUM_SIDES; sideNum++) {
		// compute angle-related terms
		float startDegrees = startAngleDegree + DEGREE_PER_SIDE * static_cast<float>(sideNum);
		float endDegrees = startAngleDegree + DEGREE_PER_SIDE * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute start and end position
		Vec3 startPos = Vec3(center.x + radius * cosStart,
			center.y + radius * sinStart, 0.f);
		Vec3 endPos = Vec3(center.x + radius * cosEnd,
			center.y + radius * sinEnd, 0.f);

		verts.push_back(Vertex_PCU(startPos, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(endPos, color, Vec2(0.f, 0.f)));
		verts.push_back(Vertex_PCU(Vec3(center.x, center.y, 0.f), color, Vec2(0.f, 0.f)));
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCapsule2D(std::vector<Vertex_PCU>& verts, Capsule2 const& capsule, Rgba8 const& color){
	Vec2 startToEnd = capsule.m_end - capsule.m_start;
	float capsuleOrientationDegrees = startToEnd.GetOrientationDegrees();
	// add vertexes for capsule, its left half circle, middle box, and right half circle
	AddVertsForHalfDisc(verts, capsule.m_start, capsuleOrientationDegrees + 90.f, capsule.m_radius, color);
	AddVertsForShorterLine(verts, capsule.m_start, capsule.m_end, capsule.m_radius * 2.f, color);
	AddVertsForHalfDisc(verts, capsule.m_end, capsuleOrientationDegrees + 270.f, capsule.m_radius, color);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForCapsule2D(std::vector<Vertex_PCU>& verts, Vec2 const& boneStart, Vec2 const& boneEnd, 
	float radius, Rgba8 const& color){
	Vec2 startToEnd = boneEnd - boneStart;
	float capsuleOrientationDegrees = startToEnd.GetOrientationDegrees();
	AddVertsForHalfDisc(verts, boneStart, capsuleOrientationDegrees + 90.f, radius, color);
	AddVertsForShorterLine(verts, boneStart, boneEnd, radius * 2.f, color);
	AddVertsForHalfDisc(verts, boneEnd, capsuleOrientationDegrees + 270.f, radius, color);
}

 
//-----------------------------------------------------------------------------------------------
void AddVertsForOBB2D(std::vector<Vertex_PCU>& verts, OBB2 const& box, Rgba8 const& color){
	Vec2 boxCorners[4];
	box.GetCornerPoints(boxCorners);

	// draw BL, BR, TR triangle
	verts.push_back(Vertex_PCU(Vec3(boxCorners[0].x, boxCorners[0].y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(boxCorners[1].x, boxCorners[1].y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(boxCorners[2].x, boxCorners[2].y, 0.f), color, Vec2(0.f, 0.f)));

	// draw BL, TR, TL triangle
	verts.push_back(Vertex_PCU(Vec3(boxCorners[0].x, boxCorners[0].y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(boxCorners[2].x, boxCorners[2].y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(boxCorners[3].x, boxCorners[3].y, 0.f), color, Vec2(0.f, 0.f)));
}


//-----------------------------------------------------------------------------------------------
void AddVertsForLineSegment2D(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end, 
	float thickness, Rgba8 const& color){
	AddVertsForLine(verts, start, end, thickness, color);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForLineSegment2D(std::vector<Vertex_PCU>& verts, LineSegment2 const& lineSegment, 
	float thickness, Rgba8 const& color){
	AddVertsForLine(verts, lineSegment.m_start, lineSegment.m_end, thickness, color);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForRing(Vertex_PCU* verts, Vec2 const& center, float radius,
	float thickness, Rgba8 const& color) {
	float halfTickness = 0.5f * thickness;
	float innerRadius = radius - halfTickness;
	float outerRadius = radius + halfTickness;

	constexpr int NUM_SIDES = 16;
	constexpr int NUM_TRIS = 2 * NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	for (int sideNum = 0; sideNum < NUM_SIDES; sideNum++) {
		// compute angle-related terms
		float startDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum);
		float endDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute inner and outer position
		Vec3 innerStartPos = Vec3(center.x + innerRadius * cosStart,
			center.y + innerRadius * sinStart, 0.f);
		Vec3 outerStartPos = Vec3(center.x + outerRadius * cosStart,
			center.y + outerRadius * sinStart, 0.f);
		Vec3 innerEndPos = Vec3(center.x + innerRadius * cosEnd,
			center.y + innerRadius * sinEnd, 0.f);
		Vec3 outerEndPos = Vec3(center.x + outerRadius * cosEnd,
			center.y + outerRadius * sinEnd, 0.f);

		// trapezoid is made of two triangles: ABC and DEF;
		// where A is inner end, B is inner start, C is outer start
		// D is inner end, E is outer start, F is outer end
		int vertIndexA = 6 * sideNum;
		int vertIndexB = 6 * sideNum + 1;
		int vertIndexC = 6 * sideNum + 2;
		int vertIndexD = 6 * sideNum + 3;
		int vertIndexE = 6 * sideNum + 4;
		int vertIndexF = 6 * sideNum + 5;

		// Set vertex positions and color for two triangles
		verts[vertIndexA].m_position = innerEndPos;
		verts[vertIndexB].m_position = innerStartPos;
		verts[vertIndexC].m_position = outerStartPos;
		verts[vertIndexA].m_color = color;
		verts[vertIndexB].m_color = color;
		verts[vertIndexC].m_color = color;

		verts[vertIndexD].m_position = innerEndPos;
		verts[vertIndexE].m_position = outerStartPos;
		verts[vertIndexF].m_position = outerEndPos;
		verts[vertIndexD].m_color = color;
		verts[vertIndexE].m_color = color;
		verts[vertIndexF].m_color = color;
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForRing(std::vector<Vertex_PCU>& verts, Vec2 const& center, float radius, 
	float thickness, Rgba8 const& color){
	float halfTickness = 0.5f * thickness;
	float innerRadius = radius - halfTickness;
	float outerRadius = radius + halfTickness;

	constexpr int NUM_SIDES = 16;
	constexpr int NUM_TRIS = 2 * NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	for (int sideNum = 0; sideNum < NUM_SIDES; sideNum++) {
		// compute angle-related terms
		float startDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum);
		float endDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute inner and outer position
		Vec3 innerStartPos = Vec3(center.x + innerRadius * cosStart,
			center.y + innerRadius * sinStart, 0.f);
		Vec3 outerStartPos = Vec3(center.x + outerRadius * cosStart,
			center.y + outerRadius * sinStart, 0.f);
		Vec3 innerEndPos = Vec3(center.x + innerRadius * cosEnd,
			center.y + innerRadius * sinEnd, 0.f);
		Vec3 outerEndPos = Vec3(center.x + outerRadius * cosEnd,
			center.y + outerRadius * sinEnd, 0.f);
		
		verts.push_back(Vertex_PCU(innerEndPos, color, Vec2::ZERO));
		verts.push_back(Vertex_PCU(innerStartPos, color, Vec2::ZERO));
		verts.push_back(Vertex_PCU(outerStartPos, color, Vec2::ZERO));

		verts.push_back(Vertex_PCU(innerEndPos, color, Vec2::ZERO));
		verts.push_back(Vertex_PCU(outerStartPos, color, Vec2::ZERO));
		verts.push_back(Vertex_PCU(outerEndPos, color, Vec2::ZERO));
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForLine(Vertex_PCU* verts, Vec2 const& start, Vec2 const& end,
	float thickness, Rgba8 const& color) {
	// compute four corners of the line
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepFront + stepLeft;
	Vec2 frontRight = end + stepFront - stepLeft;
	Vec2 backLeft = start - stepFront + stepLeft;
	Vec2 backRight = start - stepFront - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;

	verts[0].m_position = Vec3(backRight.x, backRight.y, 0.f);
	verts[1].m_position = Vec3(backLeft.x, backLeft.y, 0.f);
	verts[2].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[3].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[4].m_position = Vec3(frontLeft.x, frontLeft.y, 0.f);
	verts[5].m_position = Vec3(backLeft.x, backLeft.y, 0.f);

	for (int vertIndex = 0; vertIndex < NUM_VERTS; vertIndex++) {
		verts[vertIndex].m_color = color;
	}
}


//-----------------------------------------------------------------------------------------------
void AddVertsForLine(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end, 
	float thickness, Rgba8 const& color){
	// compute four corners of the line
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepFront + stepLeft;
	Vec2 frontRight = end + stepFront - stepLeft;
	Vec2 backLeft = start - stepFront + stepLeft;
	Vec2 backRight = start - stepFront - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;

	verts.push_back(Vertex_PCU(Vec3(backRight.x, backRight.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(backLeft.x, backLeft.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(frontRight.x, frontRight.y, 0.f), color, Vec2(0.f, 0.f)));

	verts.push_back(Vertex_PCU(Vec3(frontRight.x, frontRight.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(frontLeft.x, frontLeft.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(backLeft.x, backLeft.y, 0.f), color, Vec2(0.f, 0.f)));

}



//-----------------------------------------------------------------------------------------------
void AddVertsForArrow(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end, 
	float thickness, float arrowSideLength, Rgba8 const& color){
	AddVertsForLine(verts, start, end, thickness, color);

	Vec2 forwardDirection = (end - start).GetNormalized();
	Vec2 rightArrowLine = forwardDirection.GetRotatedDegrees(-45.f) * arrowSideLength;
	AddVertsForLine(verts, end - rightArrowLine, end, thickness, color);

	Vec2 leftArrowLine = forwardDirection.GetRotatedDegrees(45.f) * arrowSideLength;
	AddVertsForLine(verts, end - leftArrowLine, end, thickness, color);
}


//-----------------------------------------------------------------------------------------------
void AddVertsForShorterLine(std::vector<Vertex_PCU>& verts, Vec2 const& start, Vec2 const& end, 
	float thickness, Rgba8 const& color){
	// compute four corners of the line,
	// but now it doesn't add stepfront
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepLeft;
	Vec2 frontRight = end - stepLeft;
	Vec2 backLeft = start + stepLeft;
	Vec2 backRight = start - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;

	verts.push_back(Vertex_PCU(Vec3(backRight.x, backRight.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(backLeft.x, backLeft.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(frontRight.x, frontRight.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(frontRight.x, frontRight.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(frontLeft.x, frontLeft.y, 0.f), color, Vec2(0.f, 0.f)));
	verts.push_back(Vertex_PCU(Vec3(backLeft.x, backLeft.y, 0.f), color, Vec2(0.f, 0.f)));
}

//-----------------------------------------------------------------------------------------------
// Draw a shorter line without adding/subtracting stepfront to match circles (avoid overlappings)
void AddVertsForShorterLine(Vertex_PCU* verts, Vec2 const& start, Vec2 const& end, float thickness, 
	Rgba8 const& color) {
	// compute four corners of the line,
	// but now it doesn't add stepfront
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepLeft;
	Vec2 frontRight = end - stepLeft;
	Vec2 backLeft = start + stepLeft;
	Vec2 backRight = start - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;

	verts[0].m_position = Vec3(backRight.x, backRight.y, 0.f);
	verts[1].m_position = Vec3(backLeft.x, backLeft.y, 0.f);
	verts[2].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[3].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[4].m_position = Vec3(frontLeft.x, frontLeft.y, 0.f);
	verts[5].m_position = Vec3(backLeft.x, backLeft.y, 0.f);

	for (int vertIndex = 0; vertIndex < NUM_VERTS; vertIndex++) {
		verts[vertIndex].m_color = color;
	}
}

