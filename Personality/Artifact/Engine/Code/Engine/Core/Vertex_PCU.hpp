#pragma once
#include "Engine/Core/Rgba8.hpp"
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"

struct AABB2;

//-----------------------------------------------------------------------------------------------
struct Vertex_PCU
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec3 m_position;
	Rgba8 m_color;
	Vec2 m_uvTexCoords;
public:
	// Construction/Destruction
	~Vertex_PCU() {}												// destructor (do nothing)
	Vertex_PCU() {}												// default constructor (do nothing)
	explicit Vertex_PCU(Vec3 const& position, Rgba8 const& m_isTint, Vec2 const& uvTexCoords); // explicit constructor which sets up the public values
};


