#pragma once
#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Renderer/Renderer.hpp"

// predefines
struct ID3D11Buffer; 

//------------------------------------------------------------------------------------------------
class VertexBuffer
{
	friend class Renderer; // Only the Renderer can create new Texture objects!

public:
	void CopyVertexData( void const* data, size_t byteCount ); // a lot like constant buffer, except...

	// note, could be a template
/*  	void CopyVertexArray( Vertex_PCU const* vertices, int count )    { CopyCPUData( vertices, count * sizeof(Vertex_PCU) ); }*/

	// size of a single element or vertex (just vertex pcu for now, but if we support different vertex types, this will change); 
	inline size_t GetStride() const			{ return sizeof(Vertex_PCU); }

protected:
	VertexBuffer( Renderer* source, size_t const maxSize); // can't instantiate directly; must ask Renderer to do it for you
	VertexBuffer( VertexBuffer const& copy ) = delete; // No copying allowed!  This represents GPU memory.
	virtual ~VertexBuffer();

	ID3D11Buffer* GetHandle() const;  

	// again, add other helpers as you see fit...
	// ...

protected:
	Renderer* m_source					= nullptr;	// who created me
	ID3D11Buffer* m_gpuBuffer			= nullptr;	// d3d11 handle

	size_t m_byteSize						= 0; // size of a single vertx
	size_t m_byteMaxSize					= 0; // max number of vertices allowed
};


