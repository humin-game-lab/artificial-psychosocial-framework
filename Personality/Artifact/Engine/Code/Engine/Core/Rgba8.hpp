#pragma once
typedef unsigned char uchar;

struct Vec4;

//-----------------------------------------------------------------------------------------------
struct Rgba8
{
public: 
	uchar r = 255;
	uchar g = 255;
	uchar b = 255;
	uchar a = 255;

	// static common rbga8 colors
	static const Rgba8 WHITE;
	static const Rgba8 BLACK;
	static const Rgba8 YELLOW;
	static const Rgba8 RED;
	static const Rgba8 GREEN;
	static const Rgba8 DARK_GREY;
	static const Rgba8 MAGENTA;
	static const Rgba8 CYAN;
	static const Rgba8 ROYAL_BLUE;
	static const Rgba8 BLUE;

public:
	// Construction/Destruction
	~Rgba8() {}														// destructor (do nothing)
	Rgba8() {}														// default constructor (do nothing)
	explicit Rgba8(uchar r1, uchar g1, uchar b1, uchar a1 = 255);	// explicit constructor which sets up the public values

	void SetFromText(const char* text);
	const float* GetAsFloats(float* emptyList) const;
	Vec4 GetAsFloat4() const;
	static const Rgba8 Lerp(const Rgba8& startColor, const Rgba8& endColor, float lerpValue);
	static const Rgba8 GetColorFromFloats(float* floatList);

	void operator=(const Rgba8& copyFrom);
	bool operator==(const Rgba8 & compare) const;
	bool operator!=(const Rgba8& compare) const;
	void operator*=(float valueToMul);
};


