#include "Engine/Core/EventSystem.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/NamedStrings.hpp"

EventSystem* g_theEventSystem = nullptr;

//-----------------------------------------------------------------------------------------------
EventSystem::EventSystem(EventSystemConfig const& eventSystemConfig){
	m_config = eventSystemConfig;
}


//-----------------------------------------------------------------------------------------------
void EventSystem::Startup(){

}


//-----------------------------------------------------------------------------------------------
void EventSystem::BeginFrame(){

}


//-----------------------------------------------------------------------------------------------
void EventSystem::EndFrame(){

}


//-----------------------------------------------------------------------------------------------
void EventSystem::Shutdown(){

}


//-----------------------------------------------------------------------------------------------
void EventSystem::SubscribeEventCallbackFunction(std::string const& eventName, 
	EventCallBackFunction functionPtr){
	m_eventMutex.lock();
	// add function pointer to the vector
	std::vector<EventSubscription>& subscriberListForThisEvent =
		m_subscriptionListByName[eventName];
	EventSubscription eventSubscription;
	eventSubscription.m_callBackFunctionPtr = functionPtr;
	eventSubscription.m_numTimesTriggered = 0;
	subscriberListForThisEvent.push_back(eventSubscription);
	m_eventMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
std::vector<std::string> EventSystem::GetAllRegisteredCommandNames() const{
	m_eventMutex.lock();
	std::vector<std::string> commandNames;
	for (const auto& subscribedEvent: m_subscriptionListByName) {
		commandNames.push_back(subscribedEvent.first);
	}
	m_eventMutex.unlock();
	return commandNames;
	
}


//-----------------------------------------------------------------------------------------------
void EventSystem::FireEvent(std::string const& eventName, EventArgs& args){
	// get subscription list based on event name, if event name is not exist in the map
	// [] will create an empty list and store in the map
	m_eventMutex.lock();
	std::vector<EventSubscription>& subscriberListForThisEvent =
		m_subscriptionListByName[eventName];
	for (int subscriptionIndex = 0; subscriptionIndex < static_cast<int>(subscriberListForThisEvent.size());
		subscriptionIndex++) {
		EventSubscription& subscription = subscriberListForThisEvent[subscriptionIndex];
		if (subscription.m_callBackFunctionPtr) {
			subscription.m_numTimesTriggered++;
			m_eventMutex.unlock();
			subscription.m_callBackFunctionPtr(args);
			break;
// 			if (callbackResult) {
// 				
// 			}
		}
	}
}


//-----------------------------------------------------------------------------------------------
void EventSystem::FireEvent(std::string const& eventName){
	// overload function but pass empty Event argument
	m_eventMutex.lock();
	EventArgs args;
	std::vector<EventSubscription>& subscriberListForThisEvent =
		m_subscriptionListByName[eventName];
	for (int subscriptionIndex = 0; subscriptionIndex < static_cast<int>(subscriberListForThisEvent.size());
		subscriptionIndex++) {
		EventSubscription& subscription = subscriberListForThisEvent[subscriptionIndex];
		if (subscription.m_callBackFunctionPtr) {
			subscription.m_numTimesTriggered++;
			m_eventMutex.unlock();
			subscription.m_callBackFunctionPtr(args);
			break;
// 			if (callbackResult) {
// 				
// 			}
		}
	}
	
}


//-----------------------------------------------------------------------------------------------
// function outside of class (used to call by other classes)
void SubscribeEventCallbackFunction(std::string const& eventName, EventCallBackFunction functionPtr){
	if (g_theEventSystem) {
		g_theEventSystem->SubscribeEventCallbackFunction(eventName, functionPtr);
	}
}


//-----------------------------------------------------------------------------------------------
void FireEvent(std::string const& eventName, EventArgs& args){
	if (g_theEventSystem) {
		g_theEventSystem->FireEvent(eventName, args);
	}
}


//-----------------------------------------------------------------------------------------------
void FireEvent(std::string const& eventName){
	if (g_theEventSystem) {
		g_theEventSystem->FireEvent(eventName);
	}
}
