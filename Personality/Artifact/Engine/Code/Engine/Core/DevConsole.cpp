#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Input/InputSystem.hpp"

#include <algorithm>

DevConsole* g_theConsole = nullptr;

const Rgba8 DevConsole::ERRORS(255,0,0);
const Rgba8 DevConsole::WARNING(244,208,63);
const Rgba8 DevConsole::INFO_MAJOR(0,230,64);
const Rgba8 DevConsole::INFO_MINOR(63,195,128);

//-----------------------------------------------------------------------------------------------
DevConsole::DevConsole(DevConsoleConfig const& devConsoleConfig){
	m_config = devConsoleConfig;
	if (m_config.m_eventSystem) {
		m_config.m_eventSystem->SubscribeEventCallbackFunction("CharInput", CharInput);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("KeyControl", KeyControl);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("Clear", Clear);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("ShowFrame", ShowFrame);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("HideFrame", HideFrame);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("Help", Help);
		m_config.m_eventSystem->SubscribeEventCallbackFunction("Help filter=\"\"", Help);
	}

	// push all control keys into the vector
	m_controKeys.push_back(KEYCODE_LEFT);
	m_controKeys.push_back(KEYCODE_RIGHT);
	m_controKeys.push_back(KEYCODE_UP);
	m_controKeys.push_back(KEYCODE_DOWN);
	m_controKeys.push_back(KEYCODE_HOME);
	m_controKeys.push_back(KEYCODE_END);
	m_controKeys.push_back(KEYCODE_BACKSPACE);
	m_controKeys.push_back(KEYCODE_DELETE);
	m_controKeys.push_back(KEYCODE_ENTER);
	m_controKeys.push_back(KEYCODE_ESC);

}


//-----------------------------------------------------------------------------------------------
void DevConsole::Startup(){
	m_lineMutex.lock();
	m_lines.clear();
	m_lineMutex.unlock();
	m_devConsoleClock.Reset();
	m_caretStopWatch.Start(m_devConsoleClock, 0.5);
}


//-----------------------------------------------------------------------------------------------
void DevConsole::BeginFrame(){

}


//-----------------------------------------------------------------------------------------------
void DevConsole::EndFrame(){
	m_frameNumber++;
	m_isHandlingInput = false;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::Shutdown(){

}


//-----------------------------------------------------------------------------------------------
void DevConsole::AddLine(Rgba8 const& color, std::string const& text){
	Strings	lines = SplitStringOnDelimiter(text, '\n');
	for (int lineIndex = 0; lineIndex < static_cast<int>(lines.size()); lineIndex++) {
		DevConsoleLine devConsoleLine; 
		devConsoleLine.m_color = color;
		devConsoleLine.m_text = lines[lineIndex];
		devConsoleLine.m_frameNumber = m_frameNumber;
		devConsoleLine.m_timePrinted = GetCurrentTimeSeconds();
		m_lineMutex.lock();
		m_lines.push_back(devConsoleLine);
		m_lineMutex.unlock();
	}
}


//-----------------------------------------------------------------------------------------------
void DevConsole::Render(AABB2 const& bounds, Renderer* renderder) const{
	if (m_mode == DevConsoleMode::HIDDEN) {
		return;
	}

	RenderGrayBackground(bounds, renderder);
	m_lineMutex.lock();
	RenderLines(bounds, renderder);
	RenderInput(bounds, renderder);
	m_lineMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DevConsole::Update() {

	
	if (m_isHandlingInput) {
		m_caretStopWatch.Stop();
		m_caretVisible = true;
	}
	else {
		if (m_caretStopWatch.IsStopped()) {
			m_caretStopWatch.Restart();
		}
		if (m_caretStopWatch.CheckAndDecrement()) {
			m_caretVisible = !m_caretVisible;
		}
	}

	if (static_cast<int>(m_currentInput.size()) == 0 && m_currentHistoryIndex != -1) {
		m_currentHistoryIndex = -1;
	}
}


//-----------------------------------------------------------------------------------------------
DevConsoleMode DevConsole::GetMode() const{
	return m_mode;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::SetMode(DevConsoleMode mode){
	m_mode = mode;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::Toggle(DevConsoleMode mode){
	if (m_mode == HIDDEN) {
		m_mode = mode;
	}
	else if(m_mode == mode){
		m_mode = HIDDEN;
	}
}


//-----------------------------------------------------------------------------------------------
void DevConsole::Execute(std::string const& consoleCommandText){
	EventArgs args;
	EventSystem* theEventSystem = m_config.m_eventSystem ? m_config.m_eventSystem : g_theEventSystem;

	bool isCommandValid = false;
	std::vector<std::string> commands = theEventSystem->GetAllRegisteredCommandNames();
	for (int commandIndex = 0; commandIndex < static_cast<int>(commands.size()); commandIndex++) {
		if (static_cast<int>(consoleCommandText.size()) > 4 && consoleCommandText.substr(0, 4)
			== "Help" && IsHelpCommandValid(consoleCommandText)) {
			args.SetValue("Help", consoleCommandText);
			theEventSystem->FireEvent("Help filter=\"\"", args);
			isCommandValid = true;
			break;
		}
		else if (static_cast<int>(consoleCommandText.size()) > 23 && consoleCommandText.substr(0, 23)
			== "GameSetTimeScale Scale=") {
			std::string argument = consoleCommandText.substr(23, (consoleCommandText.size() - 23));
			args.SetValue("Scale", argument);
			theEventSystem->FireEvent("GameSetTimeScale", args);
			isCommandValid = true;
			break;
		}
		else if (static_cast<int>(consoleCommandText.size()) > 30 && consoleCommandText.substr(0, 30)
			== "DebugRenderSetTimeScale Scale=") {
			std::string argument = consoleCommandText.substr(30, (consoleCommandText.size() - 30));
			args.SetValue("Scale", argument);
			theEventSystem->FireEvent("DebugRenderSetTimeScale", args);
			isCommandValid = true;
			break;
		}
		else if (static_cast<int>(consoleCommandText.size()) > 12 && consoleCommandText.substr(0, 12)
			== "LoadMap Map=") {
			std::string argument = consoleCommandText.substr(12, (consoleCommandText.size() - 12));
			args.SetValue("name", argument);
			theEventSystem->FireEvent("LoadMap Map=Name", args);
			isCommandValid = true;
			break;
		}
		else if (static_cast<int>(consoleCommandText.size()) > 16 && consoleCommandText.substr(0, 16)
			== "SpawnActor Type=") {
			std::string argument = consoleCommandText.substr(16, (consoleCommandText.size() - 16));
			args.SetValue("type", argument);
			theEventSystem->FireEvent("SpawnActor Type=<name>", args);
			isCommandValid = true;
			break;
		}
		else if (consoleCommandText == commands[commandIndex] && consoleCommandText != "GameSetTimeScale"
			&& consoleCommandText != "DebugRenderSetTimeScale") {
			args.SetValue(consoleCommandText, consoleCommandText);
			theEventSystem->FireEvent(consoleCommandText, args);
			isCommandValid = true;
			break;
		}
		
	}

	if (!isCommandValid) {
		std::string errorString = "Invalid Command \"" + consoleCommandText
			+ "\". Type Help to see all valid commands";
			AddLine(ERRORS, errorString);
	}
}

//-----------------------------------------------------------------------------------------------
bool DevConsole::IsInputCharValid(int charCode) const {
	return (charCode >= 32 && charCode <= 126 && charCode != '`' && charCode != '~');
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::AddInputChar(std::string charCode){
	int charCodeInt = std::stoi(charCode);
	if (IsInputCharValid(charCodeInt)) {
		m_isHandlingInput = true;
		int currentInputSize = static_cast<int>(m_currentInput.length());
		int caretPositionOnInput = currentInputSize - m_caretPosition;

		auto iterPos = m_currentInput.begin() + caretPositionOnInput;
		m_currentInput.insert(iterPos, (char)charCodeInt);

		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::HandleInputControl(unsigned char keyCode, DevConsoleControlKey controlKeyIndex){
	if (!m_config.m_inputSystem || !m_config.m_inputSystem->WasKeyJustPressed(keyCode)) {
		return false;
	}

	m_isHandlingInput = true;
	int currentInputSize = static_cast<int>(m_currentInput.length());

	switch (controlKeyIndex)
	{
	case KEY_LEFT: {
		if (m_caretPosition < currentInputSize) {
			m_caretPosition += 1;
		}
	}
		break;
	case KEY_RIGHT: {
		if (m_caretPosition > 0) {
			m_caretPosition -= 1;
		}
	}
		break;
	case KEY_UP: {
		int historySize = static_cast<int>(m_inputHistory.size());
		if (historySize == 0) {
			break;
		}
		if (m_currentHistoryIndex < historySize - 1) {
			m_currentHistoryIndex++;
			m_currentInput = m_inputHistory[m_currentHistoryIndex];
		}
		else if(m_currentHistoryIndex == historySize - 1){
			m_currentInput = m_inputHistory[(historySize - 1)];
		}
		m_caretPosition = 0;
	}
	break;
	case KEY_DOWN: {
		if (m_currentHistoryIndex != -1) {
			m_currentHistoryIndex--;
			if (m_currentHistoryIndex == -1) {
				m_currentInput.clear();
			}
			else {
				m_currentInput = m_inputHistory[m_currentHistoryIndex];
			}
			m_caretPosition = 0;
		}
	}
	break;
	case KEY_HOME: {
		m_caretPosition = currentInputSize;
	}
		break;
	case KEY_END: {
		m_caretPosition = 0;
	}
		break;
	case KEY_BACKSPACE: {
		if (currentInputSize > 0 && m_caretPosition != currentInputSize) {
			int caretPositionOnInput = currentInputSize - m_caretPosition - 1;

			auto iterPos = m_currentInput.begin() + caretPositionOnInput;
			m_currentInput.erase(iterPos);
		}
	}
		break;
	case KEY_DELETE: {
		if (m_caretPosition > 0) {
			int caretPositionOnInput = currentInputSize - m_caretPosition;

			auto iterPos = m_currentInput.begin() + caretPositionOnInput;
			m_currentInput.erase(iterPos);
			m_caretPosition -= 1;
		}
	}
		break;
	case KEY_ENTER: {
		if (currentInputSize > 0) {
			Execute(m_currentInput);
			for (int historyIndex = 0; historyIndex < static_cast<int>(m_inputHistory.size());
				historyIndex++) {
				if (m_inputHistory[historyIndex] == m_currentInput) {
					auto iterPos = m_inputHistory.begin() + historyIndex;
					m_inputHistory.erase(iterPos);
				}
			}
			if (static_cast<int>(m_inputHistory.size()) < m_config.m_maxHistorySize) {
				m_inputHistory.insert(m_inputHistory.begin(), m_currentInput);
			}
			m_currentInput.clear();
			m_caretPosition = 0;
		}
	}
		break;
	case KEY_ESCAPE: {
		if (currentInputSize > 0) {
			m_currentInput.clear();
			m_caretPosition = 0;
		}
		else {
			m_needToCloseConsole = true;
		}
	}
		break;
	}

	return true;
}


//-----------------------------------------------------------------------------------------------
std::vector<unsigned char> DevConsole::GetAllControlKeys() const{
	return m_controKeys;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::ClearConsole(){
	m_lineMutex.lock();
	m_lines.clear();
	m_lineMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DevConsole::SetShowFrame(bool isShowingFrame){
	m_showFrame = isShowingFrame;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::IsHelpCommandValid(std::string const& consoleCommandText){
	int commandSize = static_cast<int>(consoleCommandText.size());
	if (commandSize > 4 && commandSize < 14) {
		return false;
	}

	std::string filterString = consoleCommandText.substr(4, 9);
	if (filterString != " filter=\"" || consoleCommandText[(commandSize - 1)] != '"') {
		return false;
	}

	return true;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::RenderHelp(std::string filter) {
	// find commands based on filter
	std::vector<std::string> filteredCommands;


	EventSystem* theEventSystem = m_config.m_eventSystem ? m_config.m_eventSystem : g_theEventSystem;
	std::vector<std::string> commands = theEventSystem->GetAllRegisteredCommandNames();

	for (int commandIndex = 0; commandIndex < static_cast<int>(commands.size()); commandIndex++) {
		if (filter == "" || commands[commandIndex].find(filter) != std::string::npos) {
			filteredCommands.push_back(commands[commandIndex]);
		}
	}

	// if registered commands or no filtered commands, show string to console
	if (static_cast<int>(filteredCommands.size() == 0)) {
		AddLine(INFO_MAJOR, "");
		AddLine(WARNING, "No available commands!");
		return;
	}

	// else sort all commands and then add to lines
	AddLine(INFO_MAJOR, "");
	AddLine(INFO_MAJOR, "Available commands are: ");
	sort(filteredCommands.begin(), filteredCommands.end(), compareString);
	for (int filteredIndex = 0; filteredIndex < static_cast<int>(filteredCommands.size());
		filteredIndex++) {
		AddLine(INFO_MAJOR, "");
		AddLine(INFO_MAJOR, filteredCommands[filteredIndex]);
	}
}


//-----------------------------------------------------------------------------------------------
Renderer* DevConsole::GetRenderer() const{
	return m_config.m_renderder;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::CharInput(EventArgs& args){
	if (g_theConsole->GetMode() == HIDDEN) {
		return false;
	}
	std::string charCode = args.GetValue("CharCode", "");
	if (charCode == "") {
		return false;
	}
	return g_theConsole->AddInputChar(charCode);
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::KeyControl(EventArgs& args){
	if (g_theConsole->GetMode() == HIDDEN) {
		return false;
	}
	std::string keyCodeString = args.GetValue("KeyCode", "");
	unsigned char keyCode;
	if (keyCodeString.length() > 0) {
		int keyCodeInt = std::stoi(keyCodeString);
		keyCode = static_cast<unsigned char>(keyCodeInt);
		std::vector<unsigned char> controlKeys = g_theConsole->GetAllControlKeys();
		for (int keyIndex = 0; keyIndex < NUM_KEYS;
			keyIndex++) {
			if (keyCode == controlKeys[DevConsoleControlKey(keyIndex)]) {
				return g_theConsole->HandleInputControl(keyCode, DevConsoleControlKey(keyIndex));
			}
		}
		return false;
	}
	else {
		return false;
	}
}


bool DevConsole::Clear(EventArgs& args){
	UNUSED(args);
	if (g_theConsole->GetMode() == HIDDEN) {
		return false;
	}
	g_theConsole->ClearConsole();
	return true;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::ShowFrame(EventArgs& args){
	UNUSED(args);
	if (g_theConsole->GetMode() == HIDDEN) {
		return false;
	}
	g_theConsole->SetShowFrame(true);
	return true;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::HideFrame(EventArgs& args) {
	UNUSED(args);
	if (g_theConsole->GetMode() == HIDDEN) {
		return false;
	}
	g_theConsole->SetShowFrame(false);
	return true;
}


//-----------------------------------------------------------------------------------------------
bool DevConsole::Help(EventArgs& args){
	std::string command = args.GetValue("Help", "");
	int commandSize = static_cast<int>(command.size());

	std::string filter = "";
	if (commandSize > 13) {
		filter = command.substr(13, (commandSize - 14));
	}
	
	g_theConsole->RenderHelp(filter);
	return true;
}


//-----------------------------------------------------------------------------------------------
void DevConsole::RenderGrayBackground(AABB2 const& bounds, Renderer* renderder) const {
	// Render a back translucent quad when game is paused
	std::vector<Vertex_PCU> tempPauseQuad;
	tempPauseQuad.reserve(6);
	AABB2 pauseScreen(bounds.m_mins, bounds.m_maxs);
	Rgba8 pauseColor(0, 0, 0, 128);

	AddVertsForAABB2D(tempPauseQuad, pauseScreen, pauseColor);
	renderder->SetBlendMode(BlendMode::ALPHA);
	renderder->BindTexture(nullptr);
	renderder->DrawVertexArray(static_cast<int>(tempPauseQuad.size()), &tempPauseQuad[0]);

}


//-----------------------------------------------------------------------------------------------
void DevConsole::RenderLines(AABB2 const& bounds, Renderer* renderder) const {
	// check renderer and font validity
	Renderer* renderer = renderder ? renderder : m_config.m_renderder;
	if (!renderder) {
		ERROR_RECOVERABLE("Invalid renderer when render the development console");
		return;
	}

	std::string fontFilePath = "Data/Fonts/" + m_config.m_fontName;
	BitmapFont* font = renderer->CreateOrGetBitmapFont(fontFilePath.c_str());
	if (!font) {
		ERROR_RECOVERABLE("Cannot find font file, please check your file path");
		return;
	}
	
	// calculate related line variables
	std::vector<Vertex_PCU> vertexes;
	vertexes.reserve(static_cast<int>(m_lines.size() * 1000));
	int numOfLines = static_cast<int>(m_lines.size());
	if (numOfLines == 0) {
		return;
	}

	// get number of visible lines can be shown on dev console
	int numOfVisibleLines = 1 + static_cast<int>(m_config.m_linesOnScreen);
	Vec2 boxDimensions = bounds.GetDimensions();
	// leave 2 lines of space to decrease line height in order to leave space for input
	float lineHeight = boxDimensions.y / (static_cast<float>(numOfVisibleLines) + 
		m_config.m_lineBoxHeightBasedOnLineHeight);
	int numOfLinesBeShownOnScreen = Clamp(numOfLines, 0, numOfVisibleLines);
	int oldestLineIndex = numOfLines - numOfLinesBeShownOnScreen;

	// render lines from oldest to newest line, newest line is rendered above the input box
	for (int lineIndex = oldestLineIndex; lineIndex < numOfLines; lineIndex++) {
		DevConsoleLine const& line = m_lines[lineIndex];
		std::string text = line.m_text;
		if (m_showFrame) {
			text = "[" + std::to_string(line.m_timePrinted) + "] " + line.m_text
				+ " (Frame number: " + std::to_string(line.m_frameNumber) + ").";
		}
		int lineAltitude = numOfLines - lineIndex - 1 + m_config.m_lineBoxHeightBasedOnLineHeight;
		float lineMinY = static_cast<float>(lineAltitude) * lineHeight + bounds.m_mins.y;
		AABB2 lineBox(bounds.m_mins.x, lineMinY, bounds.m_maxs.x, lineMinY + lineHeight);

		font->AddVertsForTextInBox2D(vertexes, lineBox, lineHeight, text, line.m_color,
			m_config.m_fontSize, Vec2(0.f, 0.5f));
	}
	renderer->SetBlendMode(BlendMode::ALPHA);
	renderer->BindTexture(&font->GetTexture());
	renderer->DrawVertexArray(static_cast<int>(vertexes.size()), &vertexes[0]);
}


//-----------------------------------------------------------------------------------------------
void DevConsole::RenderInput(AABB2 const& bounds, Renderer* renderder) const{
	// check renderer and font validity again
	Renderer* renderer = renderder ? renderder : m_config.m_renderder;
	if (!renderder) {
		ERROR_RECOVERABLE("Invalid renderer when render the development console");
		return;
	}

	std::string fontFilePath = "Data/Fonts/" + m_config.m_fontName;
	BitmapFont* font = renderer->CreateOrGetBitmapFont(fontFilePath.c_str());
	if (!font) {
		ERROR_RECOVERABLE("Cannot find font file, please check your file path");
		return;
	}

	// render input box
	std::vector<Vertex_PCU> inputBoxVertexes;
	inputBoxVertexes.reserve(6);

	// get number of visible lines can be shown on dev console
	int numOfVisibleLines = 1 + static_cast<int>(m_config.m_linesOnScreen);
	Vec2 boxDimensions = bounds.GetDimensions();
	// +2 to the number of lines to decrease line height in order to leave space for input
	float lineHeight = boxDimensions.y / (static_cast<float>(numOfVisibleLines) + 2);

	// get the bound of input box which is 2 lines space high and window-width wide.
	AABB2 inputBox(bounds.m_mins, Vec2(bounds.m_maxs.x, bounds.m_mins.y + lineHeight * 
		static_cast<float>(m_config.m_lineBoxHeightBasedOnLineHeight)));
	Rgba8 inputBoxColor(194, 191, 201, 128);

	AddVertsForAABB2D(inputBoxVertexes, inputBox, inputBoxColor);
	renderder->SetBlendMode(BlendMode::ALPHA);
	renderder->BindTexture(nullptr);
	renderder->DrawVertexArray(static_cast<int>(inputBoxVertexes.size()), &inputBoxVertexes[0]);

	// string before caret
	float inputLineWidth = Clamp(lineHeight * static_cast<float>(m_currentInput.length()), 0.f,
		bounds.m_maxs.x - lineHeight);
	int inputLineSize = static_cast<int>(m_currentInput.length());
	int lineBeforeCaretSize = inputLineSize - m_caretPosition;

	float lineBeforeCaretWidth = 0;
	if (inputLineSize > 0) {
		lineBeforeCaretWidth = inputLineWidth* static_cast<float>(lineBeforeCaretSize) /
			static_cast<float>(inputLineSize);

		std::vector<Vertex_PCU> inputTextVertexes;
		font->AddVertsForTextInBox2D(inputTextVertexes, inputBox, lineHeight, m_currentInput,
			Rgba8::WHITE, m_config.m_fontSize, Vec2(0.f, 0.5f));
		renderer->SetBlendMode(BlendMode::ALPHA);
		renderer->BindTexture(&font->GetTexture());
		renderer->DrawVertexArray(static_cast<int>(inputTextVertexes.size()), &inputTextVertexes[0]);
	}

	// render caret
	std::vector<Vertex_PCU> caretVertexes;
	std::string caret = "|";
	AABB2 caretBox = inputBox;
	float fontOffset = lineHeight * 0.25f;
	float caretEndPositionX = fontOffset + lineBeforeCaretWidth;
	caretBox.m_mins.x = lineBeforeCaretWidth - fontOffset;
	caretBox.m_maxs.x = caretEndPositionX;
	uchar caretAlpha = m_caretVisible ? 255 : 0;
	Rgba8 caretColor = Rgba8(255, 255, 255, caretAlpha);
	font->AddVertsForTextInBox2D(caretVertexes, caretBox, lineHeight*2.f, caret, caretColor,
		m_config.m_fontSize * 0.25f, Vec2(0.f, 0.5f));
	renderer->SetBlendMode(BlendMode::ALPHA);
	renderer->BindTexture(&font->GetTexture());
	renderer->DrawVertexArray(static_cast<int>(caretVertexes.size()), &caretVertexes[0]);
}

