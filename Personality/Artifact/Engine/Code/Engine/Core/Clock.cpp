#include "Engine/Core/Clock.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Math/MathUtils.hpp"

static Clock g_systemClock;

//-----------------------------------------------------------------------------------------------
Clock::Clock(){
	Clock& systemClock = GetSystemClock();
	if (this != &systemClock) {
		systemClock.AddChild(this);
	}
}


//-----------------------------------------------------------------------------------------------
Clock::Clock(Clock* parent){
	parent->AddChild(this);
	m_parent = parent;
}


//-----------------------------------------------------------------------------------------------
Clock::~Clock(){
	if(m_parent){
		m_parent->RemoveChild(this);
	}
}


//-----------------------------------------------------------------------------------------------
void Clock::SetParent(Clock& parent){
	m_parent->RemoveChild(this);
	parent.AddChild(this);
	m_parent = &parent;
}


//-----------------------------------------------------------------------------------------------
void Clock::Pause(){
	m_isPaused = true;
}


//-----------------------------------------------------------------------------------------------
void Clock::Unpause(){
	m_isPaused = false;
}


//-----------------------------------------------------------------------------------------------
void Clock::TogglePause(){
	m_isPaused = !m_isPaused;
}


//-----------------------------------------------------------------------------------------------
void Clock::StepFrame(){
	m_pauseAfterFrame = true;
	m_isPaused = false;
}


//-----------------------------------------------------------------------------------------------
void Clock::SetTimeDilation(double dilationAmount){
	m_timeDilation = dilationAmount;
}


//-----------------------------------------------------------------------------------------------
void Clock::Reset(bool resetChildren /*= true */){
	m_totalSecondsPassed = 0.0;
	m_frameDeltaSeconds = 0.0;
	m_timeDilation = 1.0;
	m_frameCount = 0;

	m_isPaused = false;
	m_pauseAfterFrame = false;

	if (resetChildren) {
		for (Clock* child : m_children) {
			child->Reset();
		}
	}
}


//-----------------------------------------------------------------------------------------------
std::string Clock::GetTimeString(std::string clockName) const{
	std::string timeString = clockName;
	while ((8 - timeString.size()) > 0 ){
		timeString += " ";
	}
	std::string isPausedString = m_isPaused ? "true" : "false";
	std::string totalTimeString = std::to_string(m_totalSecondsPassed);
	std::string deltaTimeString = std::to_string(m_frameDeltaSeconds * 1000.f);
	std::string timeScaleString = std::to_string(m_timeDilation);
	timeString += "-- total: " + totalTimeString.substr(0, totalTimeString.find(".") + 2) + "s, delta: " +
		deltaTimeString.substr(0, deltaTimeString.find(".") + 2) + "ms, paused: " + isPausedString +
		", timescale: " + timeScaleString.substr(0, timeScaleString.find(".") + 2);
	return timeString;
}

//-----------------------------------------------------------------------------------------------
void Clock::SystemBeginFrame(){
	Clock& systemClock = GetSystemClock();
	systemClock.Tick();
}


//-----------------------------------------------------------------------------------------------
Clock& Clock::GetSystemClock(){
	return g_systemClock;
}


//-----------------------------------------------------------------------------------------------
void Clock::Tick(){
	double currentTime = GetCurrentTimeSeconds();
	double deltaSeconds = currentTime - m_lastUpdateTime;
	deltaSeconds = Clamp(deltaSeconds, 0.0, 0.1);

	AdvanceTime(deltaSeconds);
	m_lastUpdateTime = currentTime;
}


//-----------------------------------------------------------------------------------------------
void Clock::AdvanceTime(double deltaTimeSeconds){
	deltaTimeSeconds *= m_timeDilation;
	if (m_isPaused) {
		deltaTimeSeconds = 0.0;
	}

	m_frameDeltaSeconds = deltaTimeSeconds;
	m_totalSecondsPassed += deltaTimeSeconds;
	m_frameCount++;

	for (Clock* child : m_children) {
		child->AdvanceTime(deltaTimeSeconds);
	}

	if (m_pauseAfterFrame) {
		m_pauseAfterFrame = false;
		m_isPaused = true;
	}
}


//-----------------------------------------------------------------------------------------------
void Clock::AddChild(Clock* childClock){
	m_children.push_back(childClock);
	childClock->m_parent = this;
}


//-----------------------------------------------------------------------------------------------
void Clock::RemoveChild(Clock* childClock){
	for (int childIndex = 0; childIndex < static_cast<int>(m_children.size()); childIndex++) {
		Clock*& child = m_children[childIndex];
		if (child == childClock) {
			m_children.erase(m_children.begin() + childIndex);
		}
	}
}

