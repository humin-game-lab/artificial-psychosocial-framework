#pragma once
#include "Engine/Math/IntVec2.hpp"
#include <vector>


//-----------------------------------------------------------------------------------------------
class HeatMaps {
public:
	HeatMaps();
	HeatMaps(IntVec2 const& dimensions);
	float GetHeatAt(IntVec2 const& coords) const;
	void SetHeatAt(IntVec2 const& coords, float newHeatValue);
	void AddHeatAt(IntVec2 const& coords, float extraHeatToAdd);
	void SetAllHeat(float newHeatValue);

private:
	int GetTileIndexFromCoords(IntVec2 const& coords) const;

private:
	IntVec2 m_dimensions;
	std::vector<float> m_heatValues;
};

