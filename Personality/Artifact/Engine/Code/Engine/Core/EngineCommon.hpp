#pragma once
#include "Engine/Core/NamedStrings.hpp"
#include "Engine/Core/DevConsole.hpp"
#include "Engine/Core/EventSystem.hpp"
#include <mutex>
#define UNUSED(x) (void)(x);

extern NamedStrings g_gameConfigBlackboard;
extern DevConsole*	g_theConsole;
extern EventSystem* g_theEventSystem;
extern std::mutex g_debugDrawMutex;