#include "Engine/Core/HeatMaps.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"



//-----------------------------------------------------------------------------------------------
HeatMaps::HeatMaps(IntVec2 const& dimensions){
	m_dimensions = dimensions;
	
	// create a vector of 0.f
	int totalHeatValues = m_dimensions.x * m_dimensions.y;
	m_heatValues.reserve(totalHeatValues);
	for (int heatIndex = 0; heatIndex < totalHeatValues; heatIndex++) {
		m_heatValues.push_back(0.f);
	}
}


//-----------------------------------------------------------------------------------------------
HeatMaps::HeatMaps(){

}

//-----------------------------------------------------------------------------------------------
float HeatMaps::GetHeatAt(IntVec2 const& coords) const{
	int heatIndex = GetTileIndexFromCoords(coords);
	if (heatIndex >= static_cast<int>(m_heatValues.size())) {
		ERROR_AND_DIE("Invalid Index for HeatMap");
	}
	return m_heatValues[heatIndex];
}


//-----------------------------------------------------------------------------------------------
void HeatMaps::SetHeatAt(IntVec2 const& coords, float newHeatValue){
	int heatIndex = GetTileIndexFromCoords(coords);
	if (heatIndex >= static_cast<int>(m_heatValues.size())) {
		ERROR_AND_DIE("Invalid Index for HeatMap");
	}
	m_heatValues[heatIndex] = newHeatValue;
}


//-----------------------------------------------------------------------------------------------
void HeatMaps::AddHeatAt(IntVec2 const& coords, float extraHeatToAdd){
	int heatIndex = GetTileIndexFromCoords(coords);
	if (heatIndex >= static_cast<int>(m_heatValues.size())) {
		ERROR_AND_DIE("Invalid Index for HeatMap");
	}
	m_heatValues[heatIndex] += extraHeatToAdd;
}


//-----------------------------------------------------------------------------------------------
void HeatMaps::SetAllHeat(float newHeatValue){
	for (int heatIndex = 0; heatIndex < static_cast<int>(m_heatValues.size()); heatIndex++) {
		m_heatValues[heatIndex] = newHeatValue;
	}
}


//-----------------------------------------------------------------------------------------------
int HeatMaps::GetTileIndexFromCoords(IntVec2 const& coords) const{
	return coords.x + (coords.y * m_dimensions.x);
}
