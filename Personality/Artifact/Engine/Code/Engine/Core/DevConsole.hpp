#pragma once
#include <string>
#include <vector>
#include "Engine/Core/Rgba8.hpp"
#include "Engine/Core/EventSystem.hpp"
#include "Engine/Core/Clock.hpp"
#include "Engine/Core/Stopwatch.hpp"
#include <mutex>

//-----------------------------------------------------------------------------------------------
class Renderer;
class InputSystem;
struct AABB2;


//-----------------------------------------------------------------------------------------------
enum DevConsoleMode {
	OPEN,
	HIDDEN,
	NUM_DEVCONSOLEMODES
};

enum DevConsoleControlKey {
	KEY_LEFT = 0,
	KEY_RIGHT,
	KEY_UP,
	KEY_DOWN,
	KEY_HOME,
	KEY_END,
	KEY_BACKSPACE,
	KEY_DELETE,
	KEY_ENTER,
	KEY_ESCAPE,
	NUM_KEYS
};


//-----------------------------------------------------------------------------------------------
struct DevConsoleConfig {
	Renderer*		m_renderder = nullptr;
	EventSystem*	m_eventSystem = nullptr;
	InputSystem*	m_inputSystem = nullptr;
	std::string		m_fontName = "SquirrelFixedFont";
	float			m_fontSize = 1.f;
	float			m_linesOnScreen = 43.5f;
	int				m_lineBoxHeightBasedOnLineHeight = 2;
	int				m_maxHistorySize = 125;
};


//-----------------------------------------------------------------------------------------------
struct DevConsoleLine {
	Rgba8		m_color = Rgba8::WHITE;
	std::string m_text = "";
	int			m_frameNumber = 0;
	double		m_timePrinted = 0.0;
};


//-----------------------------------------------------------------------------------------------
class DevConsole {
public:
	bool m_needToCloseConsole = false;

public:
	DevConsole(DevConsoleConfig const& devConsoleConfig);

	void Startup();
	void BeginFrame();
	void EndFrame();
	void Shutdown();

	void AddLine(Rgba8 const& color, std::string const& text);
	void Render(AABB2 const& bounds, Renderer* renderder) const;
	void Update();

	DevConsoleMode GetMode() const;
	void SetMode(DevConsoleMode mode);
	void Toggle(DevConsoleMode mode);
	void Execute(std::string const& consoleCommandText);

	bool IsInputCharValid(int charCode) const;
	bool AddInputChar(std::string charCode);
	bool HandleInputControl(unsigned char keyCode, DevConsoleControlKey controlKeyIndex);
	std::vector<unsigned char> GetAllControlKeys() const;

	void ClearConsole();
	void SetShowFrame(bool isShowingFrame);
	bool IsHelpCommandValid(std::string const& consoleCommandText);
	void RenderHelp(std::string filter);
	Renderer* GetRenderer() const;

	static bool CharInput(EventArgs& args);
	static bool KeyControl(EventArgs& args);
	static bool Clear(EventArgs& args);
	static bool ShowFrame(EventArgs& args);
	static bool HideFrame(EventArgs& args);
	static bool Help(EventArgs& args);

	static const Rgba8 ERRORS;
	static const Rgba8 WARNING;
	static const Rgba8 INFO_MAJOR;
	static const Rgba8 INFO_MINOR;

protected:
	DevConsoleConfig				m_config;
	DevConsoleMode					m_mode = DevConsoleMode::HIDDEN;
	std::vector<DevConsoleLine>		m_lines;
	mutable std::mutex						m_lineMutex;
	int								m_frameNumber = 0;
	bool							m_showFrame = false;
	bool							m_isHandlingInput = false;
	Clock							m_devConsoleClock;

	std::string						m_currentInput;
	int								m_caretPosition = 0;
	Stopwatch						m_caretStopWatch;
	bool							m_caretVisible = true;
	
	std::vector <std::string>		m_inputHistory;
	int								m_currentHistoryIndex = -1;

	std::vector<unsigned char>		m_controKeys;

protected:
	void RenderGrayBackground(AABB2 const& bounds, Renderer* renderder) const;
	void RenderLines(AABB2 const& bounds, Renderer* renderder) const;
	void RenderInput(AABB2 const& bounds, Renderer* renderder) const;
};