#include "Engine/Core/Window.hpp"
#include "Engine/Core/EventSystem.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Input/Inputsystem.hpp"
#include "ThirdParty/imgui/imgui_impl_win32.h"
#define WIN32_LEAN_AND_MEAN		
#include <windows.h>		

//-----------------------------------------------------------------------------------------------
Window* Window::s_mainWindow = nullptr;
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//-----------------------------------------------------------------------------------------------
// call back function
LRESULT CALLBACK WindowsMessageHandlingProcedure(HWND windowHandle, UINT wmMessageCode, 
	WPARAM wParam, LPARAM lParam){
	Window* windowContext = Window::GetWindowContextFotWindowHandle(windowHandle);
	GUARANTEE_OR_DIE(windowContext != nullptr, "WindowContext was null!");
	InputSystem* input = windowContext->m_config.m_inputSystem;
	GUARANTEE_OR_DIE(input != nullptr, "No Input System!");

	if (ImGui_ImplWin32_WndProcHandler(windowHandle, wmMessageCode, wParam, lParam)) {
		return true;
	}
	switch (wmMessageCode)
	{
	case WM_CHAR:
	{
		char charCode = (char)wParam;
		EventArgs args;             
		args.SetValue("CharCode", std::to_string(charCode));
		FireEvent("CharInput", args);
		return 0;
	}
	case WM_CLOSE:
	{
		FireEvent("Quit");
		return 0; // "Consumes" this message (tells Windows "okay, we handled it")
	}

	// Raw physical keyboard "key-was-just-depressed" event (case-insensitive, not translated)
	case WM_KEYDOWN:
	{
		unsigned char asKey = (unsigned char)wParam;
		bool handleResult = false;
		if (input) {
			handleResult = input->HandleKeyPressed(asKey);
			if (handleResult) {
				EventArgs args;
				args.SetValue("KeyCode", std::to_string(asKey));
				FireEvent("KeyControl", args);
				return 0;
			}
		}
		else {
			ERROR_AND_DIE( "No Input System!");
		}

		break;
	}

	// Raw physical keyboard "key-was-just-released" event (case-insensitive, not translated)
	case WM_KEYUP:
	{
		unsigned char asKey = (unsigned char)wParam;
		bool handleResult = false;
		if (input) {
			handleResult = input->HandleKeyReleased(asKey);
			if (handleResult) {
				return 0;
			}
		}
		else {
			ERROR_AND_DIE("No Input System!");
		}
		break;
	}
	case WM_LBUTTONDOWN: {
		bool btnDown = (wParam & MK_LBUTTON) != 0;
		if ((input != nullptr) && btnDown) { 
			input->HandleMouseButtonPressed(LEFT_MOUSE_BUTTON); 
		}break;
	}
	case WM_MBUTTONDOWN: {
		bool btnDown = (wParam & MK_MBUTTON) != 0;
		if ((input != nullptr) && btnDown) {
			input->HandleMouseButtonPressed(MIDDLE_MOUSE_BUTTON);
		}break;
	}
	case WM_RBUTTONDOWN: {
		bool btnDown = (wParam & MK_RBUTTON) != 0;
		if ((input != nullptr) && btnDown) {
			input->HandleMouseButtonPressed(RIGHT_MOUSE_BUTTON);
		}break;
	}
	case WM_LBUTTONUP: {
		if ((input != nullptr)) {
			input->HandleMouseButtonReleased(LEFT_MOUSE_BUTTON);
		}break;
	}
	case WM_MBUTTONUP: {
		if ((input != nullptr)) {
			input->HandleMouseButtonReleased(MIDDLE_MOUSE_BUTTON);
		}break;
	}
	case WM_RBUTTONUP: {
		if ((input != nullptr)) {
			input->HandleMouseButtonReleased(RIGHT_MOUSE_BUTTON);
		}break;
	}
	case WM_ACTIVATE: {            
		bool isActive = wParam != WA_INACTIVE;             
		if (isActive) { 
			windowContext->OnActivated();
		} 
		else { 
			windowContext->OnDeactivated();
		}        
	} break;
	}

	// Send back to Windows any unhandled/unconsumed messages we want other apps to see 
	return DefWindowProc(windowHandle, wmMessageCode, wParam, lParam);
}


//-----------------------------------------------------------------------------------------------
Window::Window(WindowConfig const& windowConfig):
	m_config(windowConfig){
	s_mainWindow = this;
}

//-----------------------------------------------------------------------------------------------
void Window::Startup() {
	CreateOSWindow();
}

//-----------------------------------------------------------------------------------------------
void Window::BeginFrame() {
	// Process OS messages
	RunMessagePump();
}

//-----------------------------------------------------------------------------------------------
void Window::EndFrame() {
}

//-----------------------------------------------------------------------------------------------
void Window::Shutdown() {

}

//-----------------------------------------------------------------------------------------------
void* Window::GetDisplayContext() const {
	return m_displayDeviceContext;
}


//-----------------------------------------------------------------------------------------------
void* Window::GetHwnd() const{
	return m_Hwnd;
}


//-----------------------------------------------------------------------------------------------
IntVec2 Window::GetDimensions() const{
	return m_dimensions;
}


//-----------------------------------------------------------------------------------------------
void Window::ShowMouse(bool show){
	if (show ) {
		int counter;    
		do { 
			counter = ::ShowCursor(TRUE); 
		} while (counter <= 0);
	}
	else if (!show) {
		int counter;
		do {
			counter = ::ShowCursor(FALSE);
		} while (counter > -1);
	}
}


//-----------------------------------------------------------------------------------------------
bool Window::IsMouseVisible() const{
	CURSORINFO cursorInfo;     
	cursorInfo.cbSize = sizeof(CURSORINFO);
	BOOL gotInfo = ::GetCursorInfo(&cursorInfo);     
	bool cursorIsVisible = !gotInfo || ((cursorInfo.flags & CURSOR_SHOWING) != 0);

	return cursorIsVisible;
}


//-----------------------------------------------------------------------------------------------
void Window::LockMouse(bool lock){
	m_isMouseLocked = lock;
	if (lock) {
		RECT clientRect;
		POINT origin;
		origin.x = 0;
		origin.y = 0;
		::GetClientRect((HWND)m_Hwnd, &clientRect);
		::ClientToScreen((HWND)m_Hwnd, &origin);

		clientRect.left += origin.x;
		clientRect.right += origin.x;
		clientRect.bottom += origin.y;
		clientRect.top += origin.y;

		::ClipCursor(&clientRect);
	}
	else { 
		::ClipCursor(nullptr); 
	}
}


//-----------------------------------------------------------------------------------------------
bool Window::IsMouseLocked() const{
	return m_isMouseLocked;
}


//-----------------------------------------------------------------------------------------------
IntVec2 Window::GetClientCenter() const{
	RECT clientRect;
	::GetClientRect((HWND)m_Hwnd, &clientRect);
	POINT origin;
	origin.x = 0;
	origin.y = 0;
	::ClientToScreen((HWND)m_Hwnd, &origin);


	return IntVec2((clientRect.right - clientRect.left) / 2 + origin.x, 
		(clientRect.bottom - clientRect.top)/2 + origin.x);
}


//-----------------------------------------------------------------------------------------------
void Window::OnActivated(){
	if (m_config.m_inputSystem && m_config.m_inputSystem->m_currentMouseConfig) {
		ShowMouse(!m_config.m_inputSystem->m_currentMouseConfig->m_isHidden);
		LockMouse(m_config.m_inputSystem->m_currentMouseConfig->m_isLocked);
	}
}


//-----------------------------------------------------------------------------------------------
void Window::OnDeactivated(){
	LockMouse(false);
	ShowMouse(true);
	m_config.m_inputSystem->m_previousClientPosition = IntVec2(9999, 9999);
}


//-----------------------------------------------------------------------------------------------
bool Window::HasFocus() const{
	HWND hwnd = ::GetForegroundWindow();     
	return (hwnd == (HWND)m_Hwnd);
}


//-----------------------------------------------------------------------------------------------
IntVec2 Window::GetMouseDesktopPosition() const{
	POINT mousePos;     
	::GetCursorPos(&mousePos);     
	return IntVec2(mousePos.x, mousePos.y);
}


//-----------------------------------------------------------------------------------------------
IntVec2 Window::GeClientRect() const {
	RECT clientRect;
	::GetClientRect((HWND)m_Hwnd, &clientRect);
	return IntVec2(clientRect.right, clientRect.bottom);
}


//-----------------------------------------------------------------------------------------------
void Window::SetMouseDesktopPosition(IntVec2 const& pos){
	::SetCursorPos(pos.x, pos.y);
}


//-----------------------------------------------------------------------------------------------
IntVec2 Window::GetMouseClientPosition() const{
	POINT mousePos;
	::GetCursorPos(&mousePos);
	RECT clientRect;
	::GetClientRect((HWND)m_Hwnd, &clientRect);
	POINT origin;
	origin.x = 0;
	origin.y = 0;
	::ClientToScreen((HWND)m_Hwnd, &origin);


	return IntVec2(mousePos.x - clientRect.left - origin.x, mousePos.y - clientRect.top - origin.y);
}


//-----------------------------------------------------------------------------------------------
void Window::SetMouseClientPosition(IntVec2 const& pos){
	RECT clientRect;
	::GetClientRect((HWND)m_Hwnd, &clientRect);
	POINT origin;
	origin.x = 0;
	origin.y = 0;
	::ClientToScreen((HWND)m_Hwnd, &origin);

	clientRect.left += origin.x;
	clientRect.top += origin.y;
	
	::SetCursorPos(pos.x + clientRect.left, pos.y + clientRect.top);
}


//-----------------------------------------------------------------------------------------------
Window* Window::GetWindowContextFotWindowHandle(void* windowHandle) {
	UNUSED(windowHandle);

	return s_mainWindow;
}

//-----------------------------------------------------------------------------------------------
void Window::CreateOSWindow(){
	// Define a window style/class
	WNDCLASSEX windowClassDescription;
	memset(&windowClassDescription, 0, sizeof(windowClassDescription));
	windowClassDescription.cbSize = sizeof(windowClassDescription);
	windowClassDescription.style = CS_OWNDC; // Redraw on move, request own Display Context
	// Register our Windows message-handling function
	windowClassDescription.lpfnWndProc = static_cast<WNDPROC>(WindowsMessageHandlingProcedure); 
	windowClassDescription.hInstance = GetModuleHandle(NULL);
	windowClassDescription.hIcon = NULL;
	windowClassDescription.hCursor = NULL;
	windowClassDescription.lpszClassName = TEXT("Simple Window Class");
	RegisterClassEx(&windowClassDescription);

	// #SD1ToDo: Add support for fullscreen mode (requires different window style flags than windowed mode)
	const DWORD windowStyleFlags = WS_CAPTION | WS_BORDER | WS_SYSMENU | WS_OVERLAPPED;
	const DWORD windowStyleExFlags = WS_EX_APPWINDOW;

	// Get desktop rect, dimensions, aspect
	RECT desktopRect;
	HWND desktopWindowHandle = GetDesktopWindow();
	GetClientRect(desktopWindowHandle, &desktopRect);
	float desktopWidth = (float)(desktopRect.right - desktopRect.left);
	float desktopHeight = (float)(desktopRect.bottom - desktopRect.top);
	float desktopAspect = desktopWidth / desktopHeight;

	// Calculate maximum client size (as some % of desktop size)
	constexpr float maxClientFractionOfDesktop = 0.90f;
	float clientWidth = desktopWidth * maxClientFractionOfDesktop;
	float clientHeight = desktopHeight * maxClientFractionOfDesktop;
	if (m_config.m_clientAspect > desktopAspect)
	{
		// Client window has a wider aspect than desktop; shrink client height to match its width
		clientHeight = clientWidth / m_config.m_clientAspect;
	}
	else
	{
		// Client window has a taller aspect than desktop; shrink client width to match its height
		clientWidth = clientHeight * m_config.m_clientAspect;
	}

	m_dimensions = IntVec2(static_cast<int>(clientWidth), static_cast<int>(clientHeight));
	// Calculate client rect bounds by centering the client area
	float clientMarginX = 0.5f * (desktopWidth - clientWidth);
	float clientMarginY = 0.5f * (desktopHeight - clientHeight);
	RECT clientRect;
	clientRect.left = (int)clientMarginX;
	clientRect.right = clientRect.left + (int)clientWidth;
	clientRect.top = (int)clientMarginY;
	clientRect.bottom = clientRect.top + (int)clientHeight;

	// Calculate the outer dimensions of the physical window, including frame et. al.
	RECT windowRect = clientRect;
	AdjustWindowRectEx(&windowRect, windowStyleFlags, FALSE, windowStyleExFlags);

	WCHAR windowTitle[1024];
	MultiByteToWideChar(GetACP(), 0, m_config.m_windowTitle.c_str(), -1, windowTitle, 
		sizeof(windowTitle) / sizeof(windowTitle[0]));
	HWND hwnd = CreateWindowEx(
		windowStyleExFlags,
		windowClassDescription.lpszClassName,
		windowTitle,
		windowStyleFlags,
		windowRect.left,
		windowRect.top,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		windowClassDescription.hInstance,
		NULL);
	m_Hwnd = hwnd;

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HDC dc = GetDC(hwnd);
	m_windowHandle = hwnd;
	m_displayDeviceContext = dc;


	HCURSOR cursor = LoadCursor(NULL, IDC_ARROW);
	SetCursor(cursor);
}


//-----------------------------------------------------------------------------------------------
void Window::RunMessagePump() {
	MSG queuedMessage;
	for (;; )
	{
		const BOOL wasMessagePresent = PeekMessage(&queuedMessage, NULL, 0, 0, PM_REMOVE);
		if (!wasMessagePresent)
		{
			break;
		}

		TranslateMessage(&queuedMessage);
		// This tells Windows to call our "WindowsMessageHandlingProcedure" (a.k.a. "WinProc") function
		DispatchMessage(&queuedMessage); 
	}
}
