#include "Engine/Core/XmlUtils.hpp"
#include "Engine/Core/Rgba8.hpp"
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/IntVec2.hpp"

//-----------------------------------------------------------------------------------------------
int ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	int defaultValue){
	return element.IntAttribute(attributeName, defaultValue);
}


//-----------------------------------------------------------------------------------------------
char ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	char defaultValue){
	return static_cast<char>(element.IntAttribute(attributeName, defaultValue));
}


//-----------------------------------------------------------------------------------------------
bool ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	bool defaultValue){
	return element.BoolAttribute(attributeName, defaultValue);
}


//-----------------------------------------------------------------------------------------------
float ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	float defaultValue){
	return element.FloatAttribute(attributeName, defaultValue);
}


//-----------------------------------------------------------------------------------------------
Rgba8 ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	Rgba8 const& defaultValue){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValue;
	}
	Rgba8 attributeValueRgb8;
	attributeValueRgb8.SetFromText(attributeValue);
	return attributeValueRgb8;
}


//-----------------------------------------------------------------------------------------------
Vec2 ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	Vec2 const& defaultValue){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValue;
	}
	Vec2 attributeValueVec2;
	attributeValueVec2.SetFromText(attributeValue);
	return attributeValueVec2;
}


//-----------------------------------------------------------------------------------------------
IntVec2 ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	IntVec2 const& defaultValue){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValue;
	}
	IntVec2 attributeValueIntVec2;
	attributeValueIntVec2.SetFromText(attributeValue);
	return attributeValueIntVec2;
}


//-----------------------------------------------------------------------------------------------
std::string ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	std::string const& defaultValue){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValue;
	}
	return attributeValue;
}


//-----------------------------------------------------------------------------------------------
Strings ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	Strings const& defaultValues){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValues;
	}

	return SplitStringOnDelimiter(attributeValue, ',');
}


//-----------------------------------------------------------------------------------------------
Vec3 ParseXmlAttribute(tinyxml2::XMLElement const& element, char const* attributeName, 
	Vec3 const& defaultValue){
	const char* attributeValue = element.Attribute(attributeName);
	if (!attributeValue) {
		return defaultValue;
	}
	Vec3 attributeValueVec3;
	attributeValueVec3.SetFromText(attributeValue);
	return attributeValueVec3;
}

