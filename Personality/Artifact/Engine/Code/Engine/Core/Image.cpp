#include "Engine/Core/Image.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "ThirdParty/stb/stb_image.h"

//-----------------------------------------------------------------------------------------------
Image::Image(char const* imageFilePath){
	m_imageFilePath = imageFilePath;
	int imageTexelSizeX = 0; //  image width
	int imageTexelSizeY = 0; // image height
	int numComponents = 0; // how many color components the image had
	int numComponentsRequested = 4; // don't care; we support 3 (24-bit RGB) or 4 (32-bit RGBA)

	// Load (and decompress) the image RGB(A) bytes from a file on disk into a memory buffer
	stbi_set_flip_vertically_on_load(1);
	unsigned char* imageData;
	try {
		imageData = stbi_load(imageFilePath, &imageTexelSizeX, &imageTexelSizeY,
			&numComponents, numComponentsRequested);
	}
	catch (...) {
		ASSERT_OR_DIE(false, "Invalid image path");
	}
	m_dimensions.x = imageTexelSizeX;
	m_dimensions.y = imageTexelSizeY;

	// read rgba for each pixel
	int totalTexel = imageTexelSizeX * imageTexelSizeY;
	m_rgbaTexels.resize(totalTexel);
	for(int texelIndex = 0; texelIndex < totalTexel; texelIndex ++){
		Rgba8 texel;
		texel.r = imageData[texelIndex * 4 + 0];
		texel.g = imageData[texelIndex * 4 + 1];
		texel.b = imageData[texelIndex * 4 + 2];
		texel.a = imageData[texelIndex * 4 + 3];
		m_rgbaTexels[texelIndex] = texel;
	}

}


//-----------------------------------------------------------------------------------------------
Image::Image(){

}


//-----------------------------------------------------------------------------------------------
std::string const& Image::GetImageFilePath() const{
	return m_imageFilePath;
}


//-----------------------------------------------------------------------------------------------
IntVec2 Image::GetDimensions() const{
	return m_dimensions;
}


//-----------------------------------------------------------------------------------------------
Rgba8 Image::GetTexelColor(IntVec2 const& texelCoords) const{
	int texelIndex = texelCoords.x + (texelCoords.y * m_dimensions.x);
	return m_rgbaTexels[texelIndex];
}


//-----------------------------------------------------------------------------------------------
void Image::SetTexelColor(IntVec2 const& texelCoords, Rgba8 const& newColor){
	int texelIndex = texelCoords.x + (texelCoords.y * m_dimensions.x);
	m_rgbaTexels[texelIndex] = newColor;
}


//-----------------------------------------------------------------------------------------------
void Image::SetupAsSolidColor(int width, int height, Rgba8 const& color){
	m_dimensions.x = width;
	m_dimensions.y = height;

	// read rgba for each pixel
	int totalTexel = width * height;
	for (int texelIndex = 0; texelIndex < totalTexel; texelIndex++) {
		m_rgbaTexels.push_back(color);
	}
}


//-----------------------------------------------------------------------------------------------
void* Image::GetRawData() const{
	return (void*)m_rgbaTexels.data();
}


//-----------------------------------------------------------------------------------------------
// todo: switch statement later when we have multiple image formats
int Image::GetBytesPerPixel() const{
	return 4;
}
