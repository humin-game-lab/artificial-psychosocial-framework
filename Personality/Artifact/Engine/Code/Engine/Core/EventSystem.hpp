#pragma once
#include <string>
#include <map>
#include <vector>
#include <mutex>
#include "Engine/Core/Rgba8.hpp"


//-----------------------------------------------------------------------------------------------
class NamedStrings;


//-----------------------------------------------------------------------------------------------
typedef NamedStrings EventArgs;
typedef bool (*EventCallBackFunction)(EventArgs& args);


//-----------------------------------------------------------------------------------------------
struct EventSystemConfig {

};


//-----------------------------------------------------------------------------------------------
struct EventSubscription {
	int						m_numTimesTriggered = 0;
	EventCallBackFunction	m_callBackFunctionPtr = nullptr;
};


//-----------------------------------------------------------------------------------------------
class EventSystem {
public:
	EventSystem(EventSystemConfig const& eventSystemConfig);

	void Startup();
	void BeginFrame();
	void EndFrame();
	void Shutdown();

	void SubscribeEventCallbackFunction(std::string const& eventName, EventCallBackFunction functionPtr);
	std::vector<std::string> GetAllRegisteredCommandNames() const;
	void FireEvent(std::string const& eventName, EventArgs& args);
	void FireEvent(std::string const& eventName);
protected:
	EventSystemConfig										m_config;
	std::map<std::string, std::vector<EventSubscription>>	m_subscriptionListByName;
	mutable std::mutex										m_eventMutex;
};

void SubscribeEventCallbackFunction(std::string const& eventName, EventCallBackFunction functionPtr);
void FireEvent(std::string const& eventName, EventArgs& args);
void FireEvent(std::string const& eventName);