#include "Engine/Core/Stopwatch.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/Clock.hpp"

//-----------------------------------------------------------------------------------------------
Stopwatch::Stopwatch(){

}


//-----------------------------------------------------------------------------------------------
Stopwatch::Stopwatch(double secondDuration){
	m_duration = secondDuration;
}


//-----------------------------------------------------------------------------------------------
Stopwatch::Stopwatch(Clock& sourceClock, double secondDuration){
	m_sourceClock = &sourceClock;
	m_duration = secondDuration;
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Start(Clock& sourceClock, double seconds){
	m_sourceClock = &sourceClock;
	m_duration = seconds;
	m_startTime = m_sourceClock->GetTotalSeconds();
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Start(double seconds){
	m_duration = seconds;
	m_startTime = m_sourceClock->GetTotalSeconds();
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::SetClock(Clock& sourceClock){
	m_sourceClock = &sourceClock;
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Restart(){
	m_isPaused = false;
	m_startTime = m_sourceClock->GetTotalSeconds();
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Stop(){
	m_startTime = -1.0;
}


//-----------------------------------------------------------------------------------------------
double Stopwatch::GetElapsedSeconds() const{
	if (m_startTime < 0.0) {
		return 0.0;
	}
	double currentTime = m_sourceClock->GetTotalSeconds();
	if (m_isPaused) {
		return m_pausedElapsedTime;
	}
	else {
		return currentTime - m_startTime;
	}
}


//-----------------------------------------------------------------------------------------------
float Stopwatch::GetElapsedFraction() const{
	double elapsedTime = GetElapsedSeconds();
	if (m_duration > 0.0) {
		return static_cast<float>(elapsedTime / m_duration);
	}
	else {
		return 0.f;
	}
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::IsStopped() const{
	return m_startTime < 0.0;
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::HasElapsed() const{
	double elapsedSeconds = GetElapsedSeconds();
	return elapsedSeconds >= m_duration;
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::CheckAndDecrement(){
	if (HasElapsed()) {
		m_startTime += m_duration;
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::CheckAndRestart(){
	if (HasElapsed()) {
		Restart();
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
int Stopwatch::DecrementAll(){
	int decrementCount = 0;
	while (CheckAndDecrement()) {
		decrementCount++;
	}

	return decrementCount;
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Pause() {
	m_pausedElapsedTime = GetElapsedSeconds();
	m_isPaused = true;
}


//-----------------------------------------------------------------------------------------------
void Stopwatch::Resume(){
	m_isPaused = false;
	m_startTime = m_sourceClock->GetTotalSeconds() - m_pausedElapsedTime;
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::IsPaused() const{
	return m_isPaused;
}


//-----------------------------------------------------------------------------------------------
bool Stopwatch::IsRunning() const{
	return m_startTime > 0.0f && !m_isPaused;
}
