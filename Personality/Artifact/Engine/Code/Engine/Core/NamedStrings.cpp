#include "Engine/Core/NamedStrings.hpp"
#include "Engine/Core/Rgba8.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/IntVec2.hpp"



//-----------------------------------------------------------------------------------------------
NamedStrings::NamedStrings(){

}


//-----------------------------------------------------------------------------------------------
void NamedStrings::PopulateFromXmlElementAttributes(tinyxml2::XMLElement const& element){
	const tinyxml2::XMLAttribute* attribute = element.FirstAttribute();
	while (attribute) {
		m_keyValuePairs.insert(std::pair<std::string, std::string>(attribute->Name(),
			attribute->Value()));
			attribute = attribute->Next();
	}
}


//-----------------------------------------------------------------------------------------------
void NamedStrings::SetValue(std::string const& keyName, std::string const& newValue){
	m_keyValuePairs[keyName] = newValue;
}


//-----------------------------------------------------------------------------------------------
std::string NamedStrings::GetValue(std::string const& keyName, std::string const& defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	return searchResult->second;
}


//-----------------------------------------------------------------------------------------------
bool NamedStrings::GetValue(std::string const& keyName, bool defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}

	if (searchResult->second == "true") {
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
int NamedStrings::GetValue(std::string const& keyName, int defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	return atoi(searchResult->second.c_str());
}


//-----------------------------------------------------------------------------------------------
float NamedStrings::GetValue(std::string const& keyName, float defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	return static_cast<float>(atof(searchResult->second.c_str()));
}


//-----------------------------------------------------------------------------------------------
std::string NamedStrings::GetValue(std::string const& keyName, char const* defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	return searchResult->second;
}


//-----------------------------------------------------------------------------------------------
Rgba8 NamedStrings::GetValue(std::string const& keyName, Rgba8 const& defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	Rgba8 valueRgb8;
	valueRgb8.SetFromText(searchResult->second.c_str());
	return valueRgb8;
}


//-----------------------------------------------------------------------------------------------
Vec2 NamedStrings::GetValue(std::string const& keyName, Vec2 const& defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	Vec2 valueVec2;
	valueVec2.SetFromText(searchResult->second.c_str());
	return valueVec2;
}


//-----------------------------------------------------------------------------------------------
IntVec2 NamedStrings::GetValue(std::string const& keyName, IntVec2 const& defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	IntVec2 valueIntVec2;
	valueIntVec2.SetFromText(searchResult->second.c_str());
	return valueIntVec2;
}


//-----------------------------------------------------------------------------------------------
double NamedStrings::GetValue(std::string const& keyName, double defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	return atof(searchResult->second.c_str());
}


//-----------------------------------------------------------------------------------------------
Vec3 NamedStrings::GetValue(std::string const& keyName, Vec3 const& defaultValue) const{
	auto searchResult = m_keyValuePairs.find(keyName);
	if (searchResult == m_keyValuePairs.end()) {
		return defaultValue;
	}
	Vec3 valueVec3;
	valueVec3.SetFromText(searchResult->second.c_str());
	return valueVec3;
}
