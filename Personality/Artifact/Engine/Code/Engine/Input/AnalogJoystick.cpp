#include "Engine/Input/AnalogJoystick.hpp"
#include "Engine/Math/MathUtils.hpp"

//-----------------------------------------------------------------------------------------------
AnalogJoystick::AnalogJoystick() {

}


//-----------------------------------------------------------------------------------------------
AnalogJoystick::~AnalogJoystick() {

}


//-----------------------------------------------------------------------------------------------
Vec2 AnalogJoystick::GetPosition() const {
	return m_correctedPosition;
}


//-----------------------------------------------------------------------------------------------
float AnalogJoystick::GetMagnitude() const {
	return m_correctedPosition.GetLength();
}


//-----------------------------------------------------------------------------------------------
float AnalogJoystick::GetOrientationDegrees() const {
	return m_correctedPosition.GetOrientationDegrees();
}


//-----------------------------------------------------------------------------------------------
Vec2 AnalogJoystick::GetRawUncorrectedPosition() const {
	return m_rawPosition;
}


//-----------------------------------------------------------------------------------------------
float AnalogJoystick::GetInnerDeadZoneFraction() const {
	return m_innerDeadZoneFraction;
}


//-----------------------------------------------------------------------------------------------
float AnalogJoystick::GetOuterDeadZoneFraction() const {
	return m_outerDeadZoneFraction;
}


//-----------------------------------------------------------------------------------------------
void AnalogJoystick::Reset() {
	m_rawPosition = Vec2(0.f, 0.f);
	m_correctedPosition = Vec2(0.f, 0.f);
}


//-----------------------------------------------------------------------------------------------
void AnalogJoystick::SetDeadZoneThresholds(float normalizedInnerDeadZoneThreshold, 
	float normalizedOuterDeadZoneThreshold) {
	m_innerDeadZoneFraction = normalizedInnerDeadZoneThreshold;
	m_outerDeadZoneFraction = normalizedOuterDeadZoneThreshold;
}


//-----------------------------------------------------------------------------------------------
void AnalogJoystick::UpdatePosition(float rawNormalizedX, float rawNormaalizedY) {

	m_rawPosition = Vec2(rawNormalizedX, rawNormaalizedY);

	// first get the raw length of the position
	float rawMagnitude = m_rawPosition.GetLength();
	// based on the position, use rangemap to get deadzone corrected length
	float correctedR = RangeMapClamped(rawMagnitude, m_innerDeadZoneFraction,
		m_outerDeadZoneFraction, 0.f, 1.f);

	// if the corrected length is greater than 0, it means the player is moving the joystick
	if (correctedR > 0.f) {

		// get the joystick orientation
		float rawTheta = m_rawPosition.GetOrientationDegrees();
		// get corrected (x,y) position based on theta and length
		m_correctedPosition = Vec2::MakeFromPolarDegrees(rawTheta,correctedR);
	}
	else {
		m_correctedPosition = Vec2(0.f, 0.f);
	}
}
