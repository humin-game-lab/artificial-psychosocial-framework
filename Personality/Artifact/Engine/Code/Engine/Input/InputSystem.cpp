#include "Engine//Input/InputSystem.hpp"
#include "Engine/Core/Window.hpp"
#define WIN32_LEAN_AND_MEAN		// Always #define this before #including <windows.h>
#include <windows.h>			// #include this (massive, platform-specific) header in very few places

const unsigned char KEYCODE_F1 = VK_F1;
const unsigned char KEYCODE_F2 = VK_F2;
const unsigned char KEYCODE_F3 = VK_F3;
const unsigned char KEYCODE_F4 = VK_F4;
const unsigned char KEYCODE_F5 = VK_F5;
const unsigned char KEYCODE_F6 = VK_F6;
const unsigned char KEYCODE_F7 = VK_F7;
const unsigned char KEYCODE_F8 = VK_F8;
const unsigned char KEYCODE_F9 = VK_F9;
const unsigned char KEYCODE_F10 = VK_F10;
const unsigned char KEYCODE_F11 = VK_F11;
const unsigned char KEYCODE_ESC = VK_ESCAPE;
const unsigned char KEYCODE_SPACE = VK_SPACE;
const unsigned char KEYCODE_ENTER = VK_RETURN;
const unsigned char KEYCODE_LEFT = VK_LEFT;
const unsigned char KEYCODE_RIGHT = VK_RIGHT;
const unsigned char KEYCODE_UP = VK_UP;
const unsigned char KEYCODE_DOWN = VK_DOWN;
const unsigned char KEYCODE_HOME = VK_HOME;
const unsigned char KEYCODE_END = VK_END;
const unsigned char KEYCODE_BACKSPACE = VK_BACK;
const unsigned char KEYCODE_DELETE = VK_DELETE;
const unsigned char KEYCODE_INSERT = VK_INSERT;
const unsigned char KEYCODE_SHIFT = VK_SHIFT;


//-----------------------------------------------------------------------------------------------
InputSystem::InputSystem(InputSystemConfig const& intputConfig):
	m_config(intputConfig){
}


//-----------------------------------------------------------------------------------------------
InputSystem::~InputSystem() {
}


//-----------------------------------------------------------------------------------------------
void InputSystem::Startup() {
	// set controller ids
	m_controllers[0].m_id = 0;
	m_controllers[1].m_id = 1;
	m_controllers[2].m_id = 2;
	m_controllers[3].m_id = 3;
}


//-----------------------------------------------------------------------------------------------
void InputSystem::BeginFrame() {
	for (int controllerIndex = 0; controllerIndex < NUM_MAX_CONTROLLERS; controllerIndex++) {
		m_controllers[controllerIndex].Update();
	}

	if (m_windowContext && !m_windowContext->HasFocus()) {
		return;
	}
	
	IntVec2 clientPos = GetMouseClientPosition();

	if (m_windowContext && m_previousClientPosition != IntVec2(9999, 9999)) {
		m_relativeMovement = clientPos - m_previousClientPosition;

		if (m_currentMouseConfig && m_currentMouseConfig->m_isRelative) {
			IntVec2 clientCenter = m_windowContext->GetClientCenter();
			m_windowContext->SetMouseClientPosition(clientCenter);
			clientPos = clientCenter;
		}
	}
	else {
		m_relativeMovement = IntVec2(0,0);
	}
	m_previousClientPosition = clientPos;
}


//-----------------------------------------------------------------------------------------------
void InputSystem::EndFrame() {
	// when finish one frame, set waskeypressed = ispressed
	for (int keyIndex = 0; keyIndex < NUM_KEY; keyIndex++) {
		m_keyStates[keyIndex].m_wasPressedLastFrame = m_keyStates[keyIndex].m_isPressed;
	}
	for (int mouseKeyIndex = 0; mouseKeyIndex < NUM_MOUSE_BUTTONS; mouseKeyIndex++) {
		m_mouseButtonStates[mouseKeyIndex].m_wasPressedLastFrame =
			m_mouseButtonStates[mouseKeyIndex].m_isPressed;
	}
	for (int controllerIndex = 0; controllerIndex < NUM_MAX_CONTROLLERS; controllerIndex++) {
		for (int controllerButtonIndex = 0; controllerButtonIndex < NUM_XBOX_BUTTONS;
			controllerButtonIndex++) {
			KeyButtonState& buttonState = 
				m_controllers[controllerIndex].m_buttons[controllerButtonIndex];
			buttonState.m_wasPressedLastFrame = buttonState.m_isPressed;
		}
	}
}


//-----------------------------------------------------------------------------------------------
void InputSystem::Shutdown() {

}


//-----------------------------------------------------------------------------------------------
bool InputSystem::HandleKeyPressed(unsigned char keycode) {
	if (keycode > 0 && keycode < NUM_KEY) {
		m_keyStates[keycode].m_isPressed = true;
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::HandleKeyReleased(unsigned char keycode) {
	if (keycode > 0 && keycode < NUM_KEY) {
		m_keyStates[keycode].m_isPressed = false;
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::IsKeyDown(unsigned char keycode) const {
	return m_keyStates[keycode].m_isPressed;
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::WasKeyJustPressed(unsigned char keycode) const {
	return (m_keyStates[keycode].m_isPressed && !m_keyStates[keycode].m_wasPressedLastFrame);
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::WasKeyJustReleased(unsigned char keycode) const {
	return (!m_keyStates[keycode].m_isPressed && m_keyStates[keycode].m_wasPressedLastFrame);
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::HandleMouseButtonPressed(MouseInput mouseButton){
	m_mouseButtonStates[MouseInput(mouseButton)].m_isPressed = true;
	return true;
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::HandleMouseButtonReleased(MouseInput mouseButton){
	m_mouseButtonStates[MouseInput(mouseButton)].m_isPressed = false;
	return true;
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::IsMouseButtonDown(MouseInput mouseButton) const{
	return m_mouseButtonStates[MouseInput(mouseButton)].m_isPressed;
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::WasMouseButtonJustPressed(MouseInput mouseButton) const{
	return (m_mouseButtonStates[MouseInput(mouseButton)].m_isPressed &&
		!m_mouseButtonStates[MouseInput(mouseButton)].m_wasPressedLastFrame);
}


//-----------------------------------------------------------------------------------------------
bool InputSystem::WasMouseButtonJustReleased(MouseInput mouseButton) const{
	return (!m_mouseButtonStates[MouseInput(mouseButton)].m_isPressed &&
		m_mouseButtonStates[MouseInput(mouseButton)].m_wasPressedLastFrame);
}


//-----------------------------------------------------------------------------------------------
XboxController const& InputSystem::GetController(int controllerID) {
	for (int controllerIndex = 0; controllerIndex < NUM_MAX_CONTROLLERS; controllerIndex++) {
		if (m_controllers[controllerIndex].GetControllerID() == controllerID) {
			return m_controllers[controllerIndex];
		}
	}
	// must return 
	return m_controllers[0];
}


//-----------------------------------------------------------------------------------------------
void InputSystem::PushMouseConfig(MouseConfig const* config){
	m_mouseConfigs.push_back(config);
	FindHighestPriorMouseConfig();
}


//-----------------------------------------------------------------------------------------------
void InputSystem::PopMouseConfig(MouseConfig const* config){
	for (int configIndex = 0; configIndex < static_cast<int>(m_mouseConfigs.size()); configIndex++) {
		MouseConfig const*& mouseConfig = m_mouseConfigs[configIndex];
		if (mouseConfig == config) {
			m_mouseConfigs.erase(m_mouseConfigs.begin() + configIndex);
			delete mouseConfig;
			mouseConfig = nullptr;
			break;
		}
	}
	if (!m_currentMouseConfig) {
		FindHighestPriorMouseConfig();
	}
}


//-----------------------------------------------------------------------------------------------
void InputSystem::ApplyMouseConfig(MouseConfig const* config){
	m_currentMouseConfig = config;
	if (m_windowContext) {
		m_windowContext->LockMouse(m_currentMouseConfig->m_isLocked);
		m_windowContext->ShowMouse(!m_currentMouseConfig->m_isHidden);
	}
}


//-----------------------------------------------------------------------------------------------
IntVec2 InputSystem::GetMouseClientPosition() const{
	if (m_windowContext) {
		return m_windowContext->GetMouseClientPosition();
	}

	return IntVec2(0, 0);
}


//-----------------------------------------------------------------------------------------------
IntVec2 InputSystem::GetMouseRelativeMovement() const{
	return m_relativeMovement;
}


//-----------------------------------------------------------------------------------------------
void InputSystem::FindHighestPriorMouseConfig(){
	int highestPriority = -1;
	int highestPriorityIndex = -1;
	for (int configIndex = static_cast<int>(m_mouseConfigs.size())-1; configIndex >0 ; configIndex--) {
		MouseConfig const*& mouseConfig = m_mouseConfigs[configIndex];
		if (mouseConfig->m_priority > highestPriority) {
			highestPriority = mouseConfig->m_priority;
			highestPriorityIndex = configIndex;
		}
	}

	if (highestPriorityIndex > 0) {
		ApplyMouseConfig(m_mouseConfigs[highestPriorityIndex]);
	}
}
