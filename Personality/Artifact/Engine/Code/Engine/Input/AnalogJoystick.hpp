#pragma once
#include "Engine/Math/Vec2.hpp"

class AnalogJoystick {
public:
	AnalogJoystick();
	~AnalogJoystick();

	Vec2	GetPosition() const;
	float	GetMagnitude() const;
	float	GetOrientationDegrees() const;

	Vec2	GetRawUncorrectedPosition() const;
	float	GetInnerDeadZoneFraction() const;
	float	GetOuterDeadZoneFraction() const;

	void	Reset();
	void	SetDeadZoneThresholds(float normalizedInnerDeadZoneThreshold, float normalizedOuterDeadZoneThreshold);
	void	UpdatePosition(float rawNormalizedX, float rawNormaalizedY);

protected:
	Vec2	m_rawPosition;						// Flaky, doesn't reset at zero (or consistently snap to reset position) 
	Vec2	m_correctedPosition;				// DeadZone corrected position
	float	m_innerDeadZoneFraction = 0.3f;		// if R < this value, R = 0, "input range start" for corrective range map
	float	m_outerDeadZoneFraction = 0.9f;		// if R > this value, R = 1, "input range end" for corrective range map
};



