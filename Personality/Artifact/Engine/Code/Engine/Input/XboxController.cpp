#include "Engine/Input/XboxController.hpp"
#include "Engine/Math/MathUtils.hpp"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h> // must #include Windows.h before #including Xinput.h
#include <Xinput.h> // include the Xinput API header file (interface)
#pragma comment( lib, "xinput9_1_0" ) // Link in the xinput.lib static library // #Eiserloh: Xinput 1_4 doesn't work in Windows 7; use version 9_1_0 explicitly for broadest compatibility

//-----------------------------------------------------------------------------------------------
XboxController::XboxController() {
}


//-----------------------------------------------------------------------------------------------
XboxController::~XboxController() {

}


//-----------------------------------------------------------------------------------------------
bool XboxController::IsConnected() const {
	return m_isConnected;
}


//-----------------------------------------------------------------------------------------------
int XboxController::GetControllerID() const {
	return m_id;
}


//-----------------------------------------------------------------------------------------------
AnalogJoystick const& XboxController::GetLeftStick() const {
	return m_leftStick;
}


//-----------------------------------------------------------------------------------------------
AnalogJoystick const& XboxController::GetRightStick() const {
	return m_rightStick;
}


//-----------------------------------------------------------------------------------------------
float XboxController::GetLeftTrigger() const {
	return m_leftTrigger;
}


//-----------------------------------------------------------------------------------------------
float XboxController::GetRightTrigger() const {
	return m_rightTrigger;
}


//-----------------------------------------------------------------------------------------------
KeyButtonState const& XboxController::GetButton(XboxButtonID buttonID) const {
	return m_buttons[buttonID];
}


//-----------------------------------------------------------------------------------------------
bool XboxController::IsButtonDown(XboxButtonID buttonID) const {
	return m_buttons[buttonID].m_isPressed;
}


//-----------------------------------------------------------------------------------------------
bool XboxController::WasButtonJustPressed(XboxButtonID buttonID) const {
	return m_buttons[buttonID].m_isPressed && !m_buttons[buttonID].m_wasPressedLastFrame;
}


//-----------------------------------------------------------------------------------------------
bool XboxController::WasButtonJustReleased(XboxButtonID buttonID) const {
	return !m_buttons[buttonID].m_isPressed && m_buttons[buttonID].m_wasPressedLastFrame;
}


//-----------------------------------------------------------------------------------------------
void XboxController::Update() {
	// connect controller based on the ID
	XINPUT_STATE xboxControllerState;
	memset(&xboxControllerState, 0, sizeof(xboxControllerState));
	DWORD errorStatus = XInputGetState(m_id, &xboxControllerState);
	if (errorStatus == ERROR_SUCCESS) {
		m_isConnected = true;

		// update joysticks, triggers and buttons
		XINPUT_GAMEPAD const& state = xboxControllerState.Gamepad;
		UpdateJoystick(m_leftStick, state.sThumbLX, state.sThumbLY);
		UpdateJoystick(m_rightStick, state.sThumbRX, state.sThumbRY);
		UpdateTrigger(m_leftTrigger, state.bLeftTrigger);
		UpdateTrigger(m_rightTrigger, state.bRightTrigger);

		UpdateButton(XINPUT_BUTTON_A, state.wButtons, XINPUT_GAMEPAD_A);
		UpdateButton(XINPUT_BUTTON_B, state.wButtons, XINPUT_GAMEPAD_B);
		UpdateButton(XINPUT_BUTTON_X, state.wButtons, XINPUT_GAMEPAD_X);
		UpdateButton(XINPUT_BUTTON_Y, state.wButtons, XINPUT_GAMEPAD_Y);
		UpdateButton(XINPUT_BUTTON_BACK, state.wButtons, XINPUT_GAMEPAD_BACK);
		UpdateButton(XINPUT_BUTTON_START, state.wButtons, XINPUT_GAMEPAD_START);
		UpdateButton(XINPUT_BUTTON_LEFT_SHOULDER, state.wButtons, XINPUT_GAMEPAD_LEFT_SHOULDER);
		UpdateButton(XINPUT_BUTTON_RIGHT_SHOULDER, state.wButtons, XINPUT_GAMEPAD_RIGHT_SHOULDER);
		UpdateButton(XINPUT_BUTTON_LEFT_THUMB, state.wButtons, XINPUT_GAMEPAD_LEFT_THUMB);
		UpdateButton(XINPUT_BUTTON_RIGHT_THUMB, state.wButtons, XINPUT_GAMEPAD_RIGHT_THUMB);
		UpdateButton(XINPUT_BUTTON_DPAD_RIGHT, state.wButtons, XINPUT_GAMEPAD_DPAD_RIGHT);
		UpdateButton(XINPUT_BUTTON_DPAD_UP, state.wButtons, XINPUT_GAMEPAD_DPAD_UP);
		UpdateButton(XINPUT_BUTTON_DPAD_LEFT, state.wButtons, XINPUT_GAMEPAD_DPAD_LEFT);
		UpdateButton(XINPUT_BUTTON_DPAD_DOWN, state.wButtons, XINPUT_GAMEPAD_DPAD_DOWN);
		
	}
	else if (errorStatus == ERROR_DEVICE_NOT_CONNECTED) {
		m_isConnected = false;
		Reset();
	}
}


//-----------------------------------------------------------------------------------------------
void XboxController::Reset() {
	m_leftTrigger = 0.f;
	m_rightTrigger = 0.f;

	for (int buttonIndex=0; buttonIndex < NUM_XBOX_BUTTONS; buttonIndex++) {
		m_buttons[buttonIndex].m_isPressed = false;
		m_buttons[buttonIndex].m_wasPressedLastFrame = false;
	}

	m_leftStick.Reset();
	m_rightStick.Reset();
}


//-----------------------------------------------------------------------------------------------
void XboxController::UpdateJoystick(AnalogJoystick& out_joystick, short rawX, short rawY) {
	// normalize values then update position
	float normalizedRawX = RangeMapClamped(rawX, JOYSTICK_X_MIN, JOYSTICK_X_MAX, -1, 1);
	float normalizedRawY = RangeMapClamped(rawY, JOYSTICK_Y_MIN, JOYSTICK_Y_MAX, -1, 1);
	out_joystick.UpdatePosition(normalizedRawX, normalizedRawY);
}


//-----------------------------------------------------------------------------------------------
void XboxController::UpdateTrigger(float& out_triggerValue, unsigned char rawValue) {
	// first normalize the value
	float valueFraction = GetFractionWithin(rawValue, TRIGGER_MIN, TRIGGER_MAX);
	// deadzone corrected
	out_triggerValue = RangeMapClamped(valueFraction, TRIGGER_BOTTOM_DEADZONE,
		TRIGGER_UPPER_DEADZONE, 0.f, 1.f);
}


//-----------------------------------------------------------------------------------------------
void XboxController::UpdateButton(XboxButtonID buttonID, unsigned short buttonFlags, unsigned short buttonFlag) {
	// update the button state of the flag returned from xbox controller = the right button flag
	m_buttons[buttonID].m_isPressed = ((buttonFlags & buttonFlag) == buttonFlag);
}
