#pragma once
#include "Engine/Input/AnalogJoystick.hpp"
#include "Engine/Input/KeyButtonState.hpp"

enum XboxButtonID {
	XINPUT_BUTTON_DPAD_UP,
	XINPUT_BUTTON_DPAD_DOWN,
	XINPUT_BUTTON_DPAD_LEFT,
	XINPUT_BUTTON_DPAD_RIGHT,
	XINPUT_BUTTON_START,
	XINPUT_BUTTON_BACK,
	XINPUT_BUTTON_LEFT_THUMB,
	XINPUT_BUTTON_RIGHT_THUMB,
	XINPUT_BUTTON_LEFT_SHOULDER,
	XINPUT_BUTTON_RIGHT_SHOULDER,
	XINPUT_BUTTON_A,
	XINPUT_BUTTON_B,
	XINPUT_BUTTON_X,
	XINPUT_BUTTON_Y,
	NUM_XBOX_BUTTONS
};

constexpr short JOYSTICK_X_MAX =	32767;
constexpr short JOYSTICK_X_MIN = -32768;
constexpr short JOYSTICK_Y_MAX =	32767;
constexpr short JOYSTICK_Y_MIN = -32768;
constexpr float TRIGGER_MAX = 255.f;
constexpr float TRIGGER_MIN = 0.f;
constexpr float TRIGGER_UPPER_DEADZONE = 0.98f;
constexpr float TRIGGER_BOTTOM_DEADZONE = 0.12f;


class XboxController{

	friend class InputSystem;

public:
	XboxController();
	~XboxController();
	bool					IsConnected() const;
	int						GetControllerID() const;
	AnalogJoystick const&	GetLeftStick() const;
	AnalogJoystick const&	GetRightStick() const;
	float					GetLeftTrigger() const;
	float					GetRightTrigger() const;
	KeyButtonState const&	GetButton(XboxButtonID buttonID) const;
	bool					IsButtonDown(XboxButtonID buttonID) const;
	bool					WasButtonJustPressed(XboxButtonID buttonID) const;
	bool					WasButtonJustReleased(XboxButtonID buttonID) const;

private:
	void					Update();
	void					Reset();
	void					UpdateJoystick(AnalogJoystick& out_joystick, short rawX, short rawY);
	void					UpdateTrigger(float& out_triggerValue, unsigned char rawValue);
	void					UpdateButton(XboxButtonID buttonID, unsigned short buttonFlags, unsigned short buttonFlag);

private:
	int						m_id = -1;
	bool					m_isConnected = false;
	float					m_leftTrigger = 0.f;
	float					m_rightTrigger = 0.f;
	KeyButtonState			m_buttons[NUM_XBOX_BUTTONS];
	AnalogJoystick			m_leftStick;
	AnalogJoystick			m_rightStick;
};



