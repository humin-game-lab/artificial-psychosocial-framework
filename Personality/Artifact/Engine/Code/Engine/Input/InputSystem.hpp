#pragma once
#include "Engine/Input/KeyButtonState.hpp"
#include "Engine/Input/XboxController.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <vector>

class Window;

constexpr int NUM_KEY = 256;
constexpr int NUM_MAX_CONTROLLERS = 4;
extern const unsigned char KEYCODE_F1;
extern const unsigned char KEYCODE_F2;
extern const unsigned char KEYCODE_F3;
extern const unsigned char KEYCODE_F4;
extern const unsigned char KEYCODE_F5;
extern const unsigned char KEYCODE_F6;
extern const unsigned char KEYCODE_F7;
extern const unsigned char KEYCODE_F8;
extern const unsigned char KEYCODE_F9; 
extern const unsigned char KEYCODE_F10;
extern const unsigned char KEYCODE_F11;
extern const unsigned char KEYCODE_ESC;
extern const unsigned char KEYCODE_SPACE;
extern const unsigned char KEYCODE_ENTER;
extern const unsigned char KEYCODE_LEFT;
extern const unsigned char KEYCODE_RIGHT;
extern const unsigned char KEYCODE_UP;
extern const unsigned char KEYCODE_DOWN;
extern const unsigned char KEYCODE_HOME;
extern const unsigned char KEYCODE_END;
extern const unsigned char KEYCODE_BACKSPACE;
extern const unsigned char KEYCODE_DELETE;
extern const unsigned char KEYCODE_SHIFT; 
extern const unsigned char KEYCODE_INSERT;

enum MouseInput {
	LEFT_MOUSE_BUTTON = 0,
	MIDDLE_MOUSE_BUTTON,
	RIGHT_MOUSE_BUTTON,
	NUM_MOUSE_BUTTONS
};

//-----------------------------------------------------------------------------------------------
struct InputSystemConfig{
};

struct MouseConfig {
	int m_priority = 0;
	bool m_isHidden = false;
	bool m_isLocked = false;
	bool m_isRelative = false;
};

//-----------------------------------------------------------------------------------------------
class InputSystem {
public: 
	InputSystem(InputSystemConfig const& intputConfig);
	~InputSystem();

	void Startup();
	void BeginFrame();
	void EndFrame();
	void Shutdown();

	bool HandleKeyPressed(unsigned char keycode);
	bool HandleKeyReleased(unsigned char keycode);

	bool IsKeyDown(unsigned char keycode) const;
	bool WasKeyJustPressed(unsigned char keycode) const;
	bool WasKeyJustReleased(unsigned char keycode) const;

	bool HandleMouseButtonPressed(MouseInput mouseButton);
	bool HandleMouseButtonReleased(MouseInput mouseButton);

	bool IsMouseButtonDown(MouseInput mouseButton) const;
	bool WasMouseButtonJustPressed(MouseInput mouseButton) const;
	bool WasMouseButtonJustReleased(MouseInput mouseButton) const;


	XboxController const& GetController(int controllerID);
	
	void PushMouseConfig(MouseConfig const* config);
	void PopMouseConfig(MouseConfig const* config);
	void ApplyMouseConfig(MouseConfig const* config);

	IntVec2 GetMouseClientPosition() const;       
	IntVec2 GetMouseRelativeMovement() const;

public:
	InputSystemConfig				m_config;
	KeyButtonState					m_keyStates[NUM_KEY];
	KeyButtonState					m_mouseButtonStates[NUM_MOUSE_BUTTONS];
	XboxController					m_controllers[NUM_MAX_CONTROLLERS];


	MouseConfig const*				m_currentMouseConfig;
	std::vector<MouseConfig const*>	m_mouseConfigs = { m_currentMouseConfig };
	IntVec2							m_previousClientPosition = IntVec2(9999, 9999);
	IntVec2							m_relativeMovement = IntVec2(9999, 9999);

	Window*							m_windowContext = nullptr;

protected:
	void FindHighestPriorMouseConfig();
};



