#pragma once
#include "Camera.hpp"
#include "Engine/Core/Vertex_PCU.hpp"
#include <vector>


//-----------------------------------------------------------------------------------------------
class Window;
class Texture;
class BitmapFont;

//-----------------------------------------------------------------------------------------------
struct ID3D11Device;
struct ID3D11DeviceContext;
struct IDXGISwapChain;
struct ID3D11RenderTargetView;

//-----------------------------------------------------------------------------------------------
struct RenderContextConfig {
	Window* m_windowContext = nullptr;
};

//-----------------------------------------------------------------------------------------------
enum class BlendMode {
	ALPHA,
	ADDITIVE,
};


//-----------------------------------------------------------------------------------------------
class Renderer {
public:
	// Construction/Destruction
	~Renderer();												
	Renderer(RenderContextConfig const& renderConfig);			

	void Startup();
	void Shutdown();
	void DestroyRenderContext();

	void DrawVertexArray(int numVertexes, Vertex_PCU const* vertexes);
	void ClearScreen(Rgba8 const& clearColor);
	Texture* CreateOrGetTextureFromFile(const char* imageFilePath);
	void BindTexture(Texture const* texture);
	BitmapFont* CreateOrGetBitmapFont(const char* bitmapFontFilePathWithNoExtension);
	void SetBlendMode(BlendMode blendMode);
	

	void BeginFrame();
	void EndFrame();
	void BeginCamera(const Camera& camera );
	void EndCamera(const Camera& camera);

public:
	std::vector<Texture*>		m_loadedTexture;
	std::vector< BitmapFont*>	m_loadedFonts;
	Texture const*				m_currentTexture = nullptr;

protected:
	void CreateRC();
	void AquireBackBufferRTV();

protected:
	RenderContextConfig			m_config;

	//D3D11 Objects
	ID3D11Device*				m_device = nullptr;
	ID3D11DeviceContext*		m_context = nullptr;
	IDXGISwapChain*				m_swapChain = nullptr;
	ID3D11RenderTargetView*		m_backBufferRTV = nullptr;

private:
	BitmapFont* CreateBitmapFontFromFile(char const* fontFilePathNameWithNoExtension);
	Texture*	CreateTextureFromFile(const char* imageFilePath);
};


