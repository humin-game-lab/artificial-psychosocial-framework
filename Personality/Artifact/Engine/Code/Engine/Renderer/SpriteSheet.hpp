#pragma once
#include "Engine/Math/Vec2.hpp"
#include <vector>

class Texture;
class SpriteDefinition;
struct AABB2;
struct IntVec2;

class SpriteSheet {
public:
	explicit SpriteSheet(Texture& texture, IntVec2 const& simpleGridLayout);
	~SpriteSheet();

	// functions that get useful sprite sheet information
	Texture&			GetTexture() const;
	int						GetNumSprites() const;
	SpriteDefinition const& GetSpriteDefinition(int spriteIndex) const;
	void					GetSpriteUVs(Vec2& out_uvAtMins, Vec2& out_uvAtMaxs, int spriteIndex) const;
	AABB2					GetSpriteUVs(int spriteIndex) const;

protected:
	Texture& m_texture;
	std::vector<SpriteDefinition> m_spriteDefs;
};