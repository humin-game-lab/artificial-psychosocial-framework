#include "Engine/Renderer/SpriteAnimDefinition.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Math/AABB2.hpp"

//------------------------------------------------------------------------------------------------
SpriteAnimDefinition::SpriteAnimDefinition(const SpriteSheet& sheet, std::vector<int> animIndexes
    , float durationSeconds, SpriteAnimPlaybackType playbackType):
	m_spriteSheet(sheet){
	m_animIndexes = animIndexes;
	m_durationSeconds = durationSeconds;
	m_playbackType = playbackType;
}


//------------------------------------------------------------------------------------------------	
const SpriteDefinition& SpriteAnimDefinition::GetSpriteDefAtTime(float seconds) const
{
	int totalFrames = static_cast<int>(m_animIndexes.size());
	if(totalFrames <= 0){
		ERROR_AND_DIE("Animation has 0 frame");
	}
	if (seconds <= 0.f) {
		return m_spriteSheet.GetSpriteDefinition(m_animIndexes[0]);
	}
	switch (m_playbackType) {
	case SpriteAnimPlaybackType::ONCE: {
		// when playing once, return last sprite if time is greater than duration
		// else calculate sprite index based on time
		if (seconds > m_durationSeconds) {
			return m_spriteSheet.GetSpriteDefinition(m_animIndexes.back());
		}
		float timeForEachFrame = m_durationSeconds / totalFrames;
		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		return m_spriteSheet.GetSpriteDefinition(m_animIndexes[spriteIndex]);
		break;
	}
	case SpriteAnimPlaybackType::LOOP: {
		// when playing loop, calculate sprite index based on time
		float timeForEachFrame = m_durationSeconds / static_cast<float>(totalFrames);
		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		while (spriteIndex >= totalFrames) {
			spriteIndex -= totalFrames;
		}
		return m_spriteSheet.GetSpriteDefinition(m_animIndexes[spriteIndex]);
		break;
	}
	case SpriteAnimPlaybackType::PINGPONG: {
		// when playing pingpong, consider one complete pingpong animation as one complete animation
		int oneCompleteAnimationFrames = totalFrames;
		int totalPingPongFrames = oneCompleteAnimationFrames * 2 - 1;
		float timeForEachFrame = m_durationSeconds / static_cast<float>(totalPingPongFrames);

		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		while (spriteIndex >= totalPingPongFrames) {
			spriteIndex -= totalPingPongFrames;
		}
		int pinngpongLoopBackIndex = oneCompleteAnimationFrames - 1;
		for (int pingpongIndex = oneCompleteAnimationFrames - 1; pingpongIndex < totalPingPongFrames;
			pingpongIndex++) {
			if (pingpongIndex == spriteIndex) {
				spriteIndex = pinngpongLoopBackIndex;
				break;
			}
			pinngpongLoopBackIndex--;
		}
		return m_spriteSheet.GetSpriteDefinition(m_animIndexes[spriteIndex]);
		break;
	}
	}
	return m_spriteSheet.GetSpriteDefinition(m_animIndexes[0]);
}

//------------------------------------------------------------------------------------------------
const AABB2 SpriteAnimDefinition::GetUVsAtTime(float seconds) const{
	int totalFrames = static_cast<int>(m_animIndexes.size());
	if (totalFrames <= 0) {
		ERROR_AND_DIE("Animation has 0 frame");
	}
	if (seconds <= 0.f) {
		return m_spriteSheet.GetSpriteUVs(m_animIndexes[0]);
	}
	switch (m_playbackType) {
	case SpriteAnimPlaybackType::ONCE: {
		// when playing once, return last sprite if time is greater than duration
		// else calculate sprite index based on time
		if (seconds >= m_durationSeconds) {
			return m_spriteSheet.GetSpriteUVs(m_animIndexes.back());
		}
		float timeForEachFrame = m_durationSeconds / totalFrames;
		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		return m_spriteSheet.GetSpriteUVs(m_animIndexes[spriteIndex]);
		break;
	}
	case SpriteAnimPlaybackType::LOOP: {
		// when playing loop, calculate sprite index based on time
		float timeForEachFrame = m_durationSeconds / static_cast<float>(totalFrames);
		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		while (spriteIndex >= totalFrames) {
			spriteIndex -= totalFrames;
		}
		return m_spriteSheet.GetSpriteUVs(m_animIndexes[spriteIndex]);
		break;
	}
	case SpriteAnimPlaybackType::PINGPONG: {
		// when playing pingpong, consider one complete pingpong animation as one complete animation
		int oneCompleteAnimationFrames = totalFrames;
		int totalPingPongFrames = oneCompleteAnimationFrames * 2 - 1;
		float timeForEachFrame = m_durationSeconds / static_cast<float>(totalPingPongFrames);

		int spriteIndex = static_cast<int>(floor(seconds / timeForEachFrame));
		while (spriteIndex >= totalPingPongFrames) {
			spriteIndex -= totalPingPongFrames;
		}
		int pinngpongLoopBackIndex = oneCompleteAnimationFrames - 1;
		for (int pingpongIndex = oneCompleteAnimationFrames - 1; pingpongIndex < totalPingPongFrames;
			pingpongIndex++) {
			if (pingpongIndex == spriteIndex) {
				spriteIndex = pinngpongLoopBackIndex;
				break;
			}
			pinngpongLoopBackIndex--;
		}
		return m_spriteSheet.GetSpriteUVs(m_animIndexes[spriteIndex]);
		break;
	}
	}
	return m_spriteSheet.GetSpriteUVs(m_animIndexes[0]);
}


//------------------------------------------------------------------------------------------------
Texture& SpriteAnimDefinition::GetTexture() const{
	return m_spriteSheet.GetTexture();
}
