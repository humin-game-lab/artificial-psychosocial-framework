#include "Engine/Renderer/ConstantBuffer.hpp"
#include "Engine/Renderer/D3DInternal.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"


//-----------------------------------------------------------------------------------------------
bool ConstantBuffer::SetData(void const* data, size_t byteCount){
	D3D11_MAPPED_SUBRESOURCE mapping;
	HRESULT result = m_render->m_context->Map(
		m_gpuBuffer,
		0,
		D3D11_MAP_WRITE_DISCARD,
		0,
		&mapping
	);

	ASSERT_OR_DIE(SUCCEEDED(result), "Failed to map buffer for write");
	memcpy(mapping.pData, data, byteCount);

	m_render->m_context->Unmap(m_gpuBuffer, 0);
	return true;
}


//-----------------------------------------------------------------------------------------------
ConstantBuffer::ConstantBuffer(Renderer* source, size_t const maxSize){
	m_render = source;
	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = (UINT)maxSize;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.MiscFlags = 0;
	desc.StructureByteStride = 0;

	source->m_device->CreateBuffer(&desc, nullptr, &m_gpuBuffer);
	ASSERT_OR_DIE(nullptr != m_gpuBuffer, "Failed to create constant buffer");
}


//-----------------------------------------------------------------------------------------------
ConstantBuffer::~ConstantBuffer(){
}


//-----------------------------------------------------------------------------------------------
ID3D11Buffer* ConstantBuffer::GetHandle(){
	return m_gpuBuffer;
}
