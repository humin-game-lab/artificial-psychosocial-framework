#pragma once
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include <string>
#include <vector>

//-----------------------------------------------------------------------------------------------
// predefines so this compiles
struct ID3D11InputLayout;
struct ID3D11PixelShader; 
struct ID3D11VertexShader; 

//-----------------------------------------------------------------------------------------------
struct ShaderConfig {
	std::string m_name;// for now, file path without an extension
	std::string m_source;
	std::string m_vertexEntryPoint = "VertexMain"; 
	std::string m_pixelEntryPoint = "PixelMain"; 
};

//-----------------------------------------------------------------------------------------------
class Shader {
	friend class Renderer; // Only the Renderer can create new Texture objects!
public:
	std::string const& GetName() const; 

protected:
	ID3D11InputLayout* CreateOrGetInputLayoutFor_Vertex_PCU();
	
protected:
	ShaderConfig							m_config;					// options for this shader
	ID3D11VertexShader*						m_vertexStage = nullptr;    // Vertex Shader Compiled Object
	ID3D11PixelShader*						m_pixelStage  = nullptr;    // Pixel Shader Compiled Object
	ID3D11InputLayout*						m_inputLayoutFor_Vertex_PCU = nullptr;
	Renderer*								m_source = nullptr;

	std::vector<uint8_t>					m_vertexByteCode;           

private:
	Shader(Renderer* source); // can't instantiate directly; must ask Renderer to do it for you
	Shader(Shader const& copy) = delete; // No copying allowed! 
	~Shader();
};