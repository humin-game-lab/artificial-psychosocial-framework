#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Core/StringUtils.hpp"

constexpr int SPRITESHEET_LAYOUT = 16;
const Vec2 BitmapFont::ALIGN_CENTERED(0.5f,0.5f);

//------------------------------------------------------------------------------------------------
BitmapFont::BitmapFont(char const* fontFilePathNameWithNoExtension, Texture& fontTexture)
:m_fontGlyphsSpriteSheet(fontTexture, IntVec2(SPRITESHEET_LAYOUT, SPRITESHEET_LAYOUT)){
	m_fontFilePathNameWithNoExtension = fontFilePathNameWithNoExtension;

}


//------------------------------------------------------------------------------------------------
Texture& BitmapFont::GetTexture() const{
	return m_fontGlyphsSpriteSheet.GetTexture();
}


//------------------------------------------------------------------------------------------------
void BitmapFont::AddVertsForText2D(std::vector<Vertex_PCU>& vertexArray, Vec2 const& textMins, 
	float cellHeight, std::string const& text, Rgba8 const& m_isTint, float cellAspect){
	float cellWidth = cellHeight * cellAspect;

	// add verts for each character
	vertexArray.reserve(vertexArray.size() + 6 * text.size());
	for (int textIndex = 0; textIndex < static_cast<int>(text.size()); textIndex++) {
		Vec2 charMins = textMins + Vec2(cellWidth, 0.f) * static_cast<float>(textIndex);
		Vec2 charMaxs = Vec2(charMins.x + cellWidth, charMins.y + cellHeight);
		AABB2 characterAABBs(charMins, charMaxs);
		int characterInInt = static_cast<int>(text[textIndex]);
		// handle special case '\' //
		if (characterInInt == -110) {
			characterInInt = '\'';
		}
		AABB2 uv = m_fontGlyphsSpriteSheet.GetSpriteUVs(characterInInt);
		AddVertsForAABB2D(vertexArray, characterAABBs, m_isTint, uv.m_mins, uv.m_maxs);
	}
}


//------------------------------------------------------------------------------------------------
void BitmapFont::AddVertsForText3D(std::vector<Vertex_PCU>& vertexArray, Vec3 const& origin, 
	Vec2 alignment, float cellHeight, std::string const& text, Rgba8 const& m_isTint, float cellAspect){
	float cellWidth = cellHeight * cellAspect;
	float sizeOfText = static_cast<float>(text.size());
	float textLengthInGame = sizeOfText * cellWidth;
	Vec3 textMins = Vec3(origin.x - textLengthInGame * alignment.x, 
		origin.y - cellHeight * alignment.y, origin.z);
	// add verts for each character
	vertexArray.reserve(vertexArray.size() + 6 * text.size());
	for (int textIndex = 0; textIndex < static_cast<int>(text.size()); textIndex++) {
		Vec3 charMins = textMins + Vec3(cellWidth, 0.f, 0.f) * static_cast<float>(textIndex);
		Vec3 charMaxs = Vec3(charMins.x + cellWidth, charMins.y + cellHeight, origin.z);
		int characterInInt = static_cast<int>(text[textIndex]);
		// handle special case '\' //
		if (characterInInt == -110) {
			characterInInt = '\'';
		}
		AABB2 uv = m_fontGlyphsSpriteSheet.GetSpriteUVs(characterInInt);
		std::vector<Vec3> bounds = { charMins, Vec3(charMaxs.x, charMins.y, charMins.z),
		Vec3(charMins.x, charMaxs.y, charMins.z), charMaxs };
		AddVertsForAABB2In3D(vertexArray, bounds, m_isTint, uv);
	}
}


//------------------------------------------------------------------------------------------------
void BitmapFont::AddVertsForTextInBox2D(std::vector<Vertex_PCU>& vertexArray, const AABB2& box, 
	float cellHeight, const std::string& text, const Rgba8& isTint, float cellAspect, 
	const Vec2& alignment, TextBoxMode mode, int maxGlyphsToDraw){
	float cellWidth = cellHeight * cellAspect;
	Strings lines = SplitStringOnDelimiter(text, '\n');

	// find the max length in strings
	float maxStringLength = -1.f;
	float numOfLines = static_cast<float>(lines.size());
	for (int lineIndex = 0; lineIndex < static_cast<int>(numOfLines); lineIndex++) {
		if (static_cast<int>(lines[lineIndex].size()) > static_cast<int>(maxStringLength)) {
			maxStringLength = static_cast<float>(lines[lineIndex].size());
		}
	}

	// based on text box mode, change the character width and height
	Vec2 boxDimension = box.GetDimensions();
	float maxLineWidth = cellWidth * maxStringLength;
	float maxLinesHeight = cellHeight * numOfLines;
	if (mode == TEXT_BOX_MODE_SHRINK) {
		if (maxLineWidth > boxDimension.x) {
			cellWidth = boxDimension.x / maxStringLength;
			cellHeight = cellWidth / cellAspect;
		}
		maxLinesHeight = cellHeight * numOfLines;
		if (maxLinesHeight > boxDimension.y) {
			cellHeight = boxDimension.y / numOfLines;
			cellWidth = cellHeight * cellAspect;
		}
	}

	// calculate top and left empty room, then draw texts based on empty room and cell aspect
	maxLinesHeight = cellHeight * numOfLines;
	int remainingGlyphsToDraw = maxGlyphsToDraw;
	float topEmptyDistance = (boxDimension.y - maxLinesHeight) * (1 - alignment.y);
	for (int lineIndex = 0; lineIndex < static_cast<int>(numOfLines); lineIndex++) {
		float lineWidth = static_cast<float>(lines[lineIndex].size()) * cellWidth;
		float leftEmptyDistance = (boxDimension.x - lineWidth) * alignment.x;

		float stringYMinPosition = box.m_maxs.y - topEmptyDistance - cellHeight * (lineIndex + 1);
		float stringXMinPosition = box.m_mins.x + leftEmptyDistance;
		Vec2 textMins(stringXMinPosition, stringYMinPosition);

		std::string remainingString = lines[lineIndex];
		if (remainingGlyphsToDraw < static_cast<int>(remainingString.size())) {
			remainingString = lines[lineIndex].substr(0, remainingGlyphsToDraw);
		}
		AddVertsForText2D(vertexArray, textMins, cellHeight, remainingString, isTint, cellAspect);
		remainingGlyphsToDraw -= static_cast<int>(remainingString.size());
	}
}


//------------------------------------------------------------------------------------------------
float BitmapFont::GetGlyphAspect(int glyphUnicode) const{
	UNUSED(glyphUnicode);
	return 1.f;
}
