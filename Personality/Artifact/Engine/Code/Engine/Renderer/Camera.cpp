#include "Engine/Renderer/Camera.hpp"
#include "Engine/Math/Vec4.hpp"
#include "Engine/Math/EulerAngles.hpp"


//-----------------------------------------------------------------------------------------------
Camera::~Camera() {

}


//-----------------------------------------------------------------------------------------------
Camera::Camera() {

}		


//-----------------------------------------------------------------------------------------------
void Camera::Translate2D(const Vec2& translation2D) {
	m_bottomLeft +=  translation2D;
	m_topRight += translation2D;
}


//-----------------------------------------------------------------------------------------------
void Camera::SetOrthoView(Vec2 const& bottomLeft, Vec2 const& topRight) {
	m_bottomLeft = bottomLeft;
	m_topRight = topRight;
}


//-----------------------------------------------------------------------------------------------
Vec2 Camera::GetOrthoBottomLeft() const {
	return m_bottomLeft;
}


//-----------------------------------------------------------------------------------------------
Vec2 Camera::GetOrthoTopRight() const {
	return m_topRight;
}


//-----------------------------------------------------------------------------------------------
Vec3 const Camera::GetCameraViewPosition() const{
	return m_viewPosition;
}

//-----------------------------------------------------------------------------------------------
Vec3 const Camera::GetCameraForward() const{
	return m_viewEulerAngle.GetMatrix__XFwd_YLeft_ZUp().GetIBasis3D();
}


Mat44 const Camera::GetCameraBasis() const{
	return m_viewEulerAngle.GetMatrix__XFwd_YLeft_ZUp();
}

//-----------------------------------------------------------------------------------------------
void Camera::SetProjectionMatrix(Mat44 const& proj){
	m_projectionMatrix = proj;
	m_projectionMatrix.Append(m_gameToRendererMatrix.GetOrthonormalInverse());
}


//-----------------------------------------------------------------------------------------------
void Camera::SetTransform(Vec3 position, EulerAngles eulerAngles){
	m_viewPosition = position;
	m_viewEulerAngle = eulerAngles;
}


//-----------------------------------------------------------------------------------------------
void Camera::SetOrthographicMatrix(){
	m_projectionMatrix = Mat44::CreateOrthoProjection(m_bottomLeft.x, m_topRight.x,
		m_bottomLeft.y, m_topRight.y, 0.f, 1.f);
}


//-----------------------------------------------------------------------------------------------
void Camera::DefineTheGameSpace(Vec3 const& right, Vec3 const& up, Vec3 const& away){
	m_gameToRendererMatrix.SetIJK3D(right, up, away);
}


//-----------------------------------------------------------------------------------------------
Mat44 Camera::GetGameToRendererMatrix() const{
	return m_gameToRendererMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 Camera::GetProjectionMatrix() const{
	
	return m_projectionMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 Camera::GetViewMatrix() const{
	Mat44 viewMatrix = Mat44::CreateTranslation3D(m_viewPosition);
	viewMatrix.Append(m_viewEulerAngle.GetMatrix__XFwd_YLeft_ZUp());
	viewMatrix = viewMatrix.GetOrthonormalInverse();
	return viewMatrix;
}


//-----------------------------------------------------------------------------------------------
void Camera::SetNormalizedViewport(AABB2 const& viewport /*= AABB2::ZERO_TO_ONE*/){
	m_normalizedViewport = viewport;
}


//-----------------------------------------------------------------------------------------------
AABB2 Camera::GetNormalizedViewport() const{
	return m_normalizedViewport;
}


//-----------------------------------------------------------------------------------------------
AABB2 Camera::GetViewport(Vec2 const& outputSize) const{
	Vec2 newMins = m_normalizedViewport.m_mins * outputSize; // (x * x, y * y)
	Vec2 newMaxs = m_normalizedViewport.m_maxs * outputSize;

	// last bit, invert the y
	// 0, 0 SHOULD be (0, height)
	// 0, 1 SHOULD be (0, 0)

// 	newMins.y = outputSize.y - newMins.y;
// 	newMaxs.y = outputSize.y - newMaxs.y;

	return AABB2(newMins, newMaxs);
}


//-----------------------------------------------------------------------------------------------
Vec3 Camera::GetViewPosition() const{
	return m_viewPosition;
}


//-----------------------------------------------------------------------------------------------
EulerAngles Camera::GetViewOrientation() const{
	return m_viewEulerAngle;
}
