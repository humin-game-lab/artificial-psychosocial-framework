#include "Engine/Renderer/SpriteDefinition.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Renderer/Texture.hpp"


//-----------------------------------------------------------------------------------------------
SpriteDefinition::SpriteDefinition(SpriteSheet const& spriteSheet, int spriteIndex, Vec2 const& uvAtMins, 
	Vec2 const& uvAtMaxs):m_spriteSheet(spriteSheet){
	m_spriteIndex = spriteIndex;
	m_uvAtMins = uvAtMins;
	m_uvAtMaxs = uvAtMaxs;
}


//-----------------------------------------------------------------------------------------------
void SpriteDefinition::GetUVs(Vec2& out_uvAtMins, Vec2& out_uvAtMaxs) const{
	out_uvAtMins = m_uvAtMins;
	out_uvAtMaxs = m_uvAtMaxs;
}


//-----------------------------------------------------------------------------------------------
SpriteSheet const& SpriteDefinition::GetSpriteSheet() const{
	return m_spriteSheet;
}


//-----------------------------------------------------------------------------------------------
Texture& SpriteDefinition::GetTexture() const{
	return m_spriteSheet.GetTexture();
}


//-----------------------------------------------------------------------------------------------
float SpriteDefinition::GetAspect() const
{
	// calculate the sprite aspect based on the uv
	float widthU = m_uvAtMaxs.x - m_uvAtMins.x;
	float heightV = m_uvAtMaxs.y - m_uvAtMins.y;

	float widthPixels = widthU * m_spriteSheet.GetTexture().m_dimensions.x;
	float heightPixels = heightV * m_spriteSheet.GetTexture().m_dimensions.y;
	return widthPixels / heightPixels;
}
