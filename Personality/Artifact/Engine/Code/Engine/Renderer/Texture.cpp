#include "Engine/Renderer/Texture.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Core/Image.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"

#include "Engine/Renderer/D3DInternal.hpp"
#include <d3dcompiler.h>
#pragma comment(lib, "d3d11.lib")

#include "ThirdParty/stb/stb_image.h"
#define DX_SAFE_RELEASE(obj) if(obj!=nullptr) {(obj)->Release(); (obj) = nullptr;}


//-----------------------------------------------------------------------------------------------
Texture::Texture(){
	
}


//-----------------------------------------------------------------------------------------------
Texture::~Texture(){

}


//-----------------------------------------------------------------------------------------------
bool Texture::LoadFromFile(Renderer* source, char const* imageFilePath){
	Image image(imageFilePath);
	m_imageFilePath = imageFilePath;
	return CreateFromImage(source, image);
}


//-----------------------------------------------------------------------------------------------
bool Texture::CreateFromImage(Renderer* source, Image const& image){
	D3D11_TEXTURE2D_DESC desc = {};

	IntVec2 imageDimension = image.GetDimensions();
	m_dimensions = imageDimension;
	desc.Width = imageDimension.x;
	desc.Height = imageDimension.y;
	desc.MipLevels = 1; // mip mapping    
	desc.ArraySize = 1; // number of textures in the texture array    
	desc.Usage = D3D11_USAGE_IMMUTABLE;     
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // contents of my image class, SRGB    
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;    
	desc.CPUAccessFlags = 0; //     
	desc.MiscFlags = 0; // cube map     
	desc.SampleDesc.Count = 1;    
	desc.SampleDesc.Quality = 0; 

	D3D11_SUBRESOURCE_DATA initialData = {};
	initialData.pSysMem = image.GetRawData();
	initialData.SysMemPitch = image.GetBytesPerPixel() * image.GetDimensions().x;
	initialData.SysMemSlicePitch = 0;


	source->m_device->CreateTexture2D(&desc, &initialData, &m_handle);
	if (m_handle) {
		return true;
	}
	else {
		ERROR_AND_DIE("Cannot create texture from image");
		return false;
	}
}	


//-----------------------------------------------------------------------------------------------
ID3D11RenderTargetView* Texture::GetOrCreateRenderTargetView(Renderer* renderer){
	if (m_renderTargetView) {
		return m_renderTargetView;
	}

	HRESULT hr;

	hr = renderer->m_swapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(void**)&m_handle
	);

	if (!SUCCEEDED(hr)) ERROR_AND_DIE("Get not get swap chain buffer");

	hr = renderer->m_device->CreateRenderTargetView(
		m_handle,
		NULL,
		&m_renderTargetView
	);

	return m_renderTargetView;
}


//-----------------------------------------------------------------------------------------------
ID3D11ShaderResourceView* Texture::GetOrCreateShaderResourceView(Renderer* renderer){
	if (m_shaderResourceView) {
		return m_shaderResourceView;
	}
	renderer->m_device->CreateShaderResourceView(m_handle, nullptr, &m_shaderResourceView);

	ASSERT_OR_DIE(m_shaderResourceView != nullptr, "Failed to create shader resource view.");
	return m_shaderResourceView;
}


//-----------------------------------------------------------------------------------------------
bool Texture::CreateDepthStencilTarget(Renderer* source, Image const& image){
	D3D11_TEXTURE2D_DESC desc = {};

	IntVec2 imageDimension = image.GetDimensions();
	m_dimensions = imageDimension;
	desc.Width = imageDimension.x;
	desc.Height = imageDimension.y;
	desc.MipLevels = 1; // mip mapping    
	desc.ArraySize = 1; // number of textures in the texture array    
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; // contents of my image class, SRGB    
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	desc.CPUAccessFlags = 0; //     
	desc.MiscFlags = 0; // cube map     
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	D3D11_SUBRESOURCE_DATA initialData = {};
	initialData.pSysMem = image.GetRawData();
	initialData.SysMemPitch = image.GetBytesPerPixel() * image.GetDimensions().x;
	initialData.SysMemSlicePitch = 0;


	source->m_device->CreateTexture2D(&desc, &initialData, &m_handle);
	if (m_handle) {
		return true;
	}
	else {
		ERROR_AND_DIE("Cannot create depth texture from image");
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
ID3D11DepthStencilView* Texture::GetOrCreateDepthStencilView(Renderer* renderer){
	if (m_depthStencilView) {
		return m_depthStencilView;
	}
	ID3D11DepthStencilView* dsv = nullptr;     
	renderer->m_device->CreateDepthStencilView(m_handle, nullptr, &dsv);
	if (!dsv) {
		ERROR_AND_DIE("Cannot create depth stencil view");
	}
	m_depthStencilView = dsv;
	return dsv;
}


//-----------------------------------------------------------------------------------------------
void Texture::ReleaseResources(){
	DX_SAFE_RELEASE(m_handle);
	DX_SAFE_RELEASE(m_shaderResourceView);
	DX_SAFE_RELEASE(m_renderTargetView);
	DX_SAFE_RELEASE(m_depthStencilView);
}


//-----------------------------------------------------------------------------------------------
void Texture::WatchInternal(Renderer* renderer){
	ReleaseResources();   
	GetOrCreateRenderTargetView(renderer);
}
