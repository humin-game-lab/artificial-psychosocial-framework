#include "Engine/Renderer/Shader.hpp"
#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/StringUtils.hpp"

#include "Engine/Renderer/D3DInternal.hpp"
#include <d3dcompiler.h>
#pragma comment(lib, "d3d11.lib")

//-----------------------------------------------------------------------------------------------
std::string const& Shader::GetName() const{
	return m_config.m_name;
}


//-----------------------------------------------------------------------------------------------
ID3D11InputLayout* Shader::CreateOrGetInputLayoutFor_Vertex_PCU(){
	if (m_inputLayoutFor_Vertex_PCU != nullptr) {
		return m_inputLayoutFor_Vertex_PCU;
	}

	D3D11_INPUT_ELEMENT_DESC vertexDesc[3];
	vertexDesc[0].SemanticName = "POSITION";
	vertexDesc[0].SemanticIndex = 0;	
	vertexDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	vertexDesc[0].InputSlot = 0;
	vertexDesc[0].AlignedByteOffset = offsetof(Vertex_PCU, m_position);
	vertexDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	vertexDesc[0].InstanceDataStepRate = 0;

	vertexDesc[1].SemanticName = "TINT";
	vertexDesc[1].Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	vertexDesc[1].SemanticIndex = 0;
	vertexDesc[1].InputSlot = 0;
	vertexDesc[1].AlignedByteOffset = offsetof(Vertex_PCU, m_color);
	vertexDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	vertexDesc[1].InstanceDataStepRate = 0;

	vertexDesc[2].SemanticName = "TEXCOORD";
	vertexDesc[2].SemanticIndex = 0;
	vertexDesc[2].Format = DXGI_FORMAT_R32G32_FLOAT;
	vertexDesc[2].InputSlot = 0;
	vertexDesc[2].AlignedByteOffset = offsetof(Vertex_PCU, m_uvTexCoords);
	vertexDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	vertexDesc[2].InstanceDataStepRate = 0;
	
	 m_source->m_device->CreateInputLayout(vertexDesc, 3,     // input desc        
		&m_vertexByteCode[0], m_vertexByteCode.size(),  // byte code        
		&m_inputLayoutFor_Vertex_PCU);

	 ASSERT_OR_DIE(m_inputLayoutFor_Vertex_PCU != nullptr, 
		 Stringf("Failed to create input layout for shader"));
	 return m_inputLayoutFor_Vertex_PCU;
}

//-----------------------------------------------------------------------------------------------
Shader::Shader(Renderer* source){
	m_source = source;
}


//-----------------------------------------------------------------------------------------------
Shader::~Shader(){

}

