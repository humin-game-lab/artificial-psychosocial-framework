#pragma once
#include "Engine/Renderer/SpriteSheet.hpp"
#include <vector>

enum class SpriteAnimPlaybackType
{
	ONCE,		// for 5-frame anim, plays 0,1,2,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4...
	LOOP,		// for 5-frame anim, plays 0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0...
	PINGPONG,	// for 5-frame anim, plays 0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1,2,3,4,3,2,1,0,1...
	NUM_PLAYBACKTYPES
};



//------------------------------------------------------------------------------------------------
class SpriteAnimDefinition
{
public:
	SpriteAnimDefinition(const SpriteSheet& sheet, std::vector<int>	animIndexes,
		float durationSeconds, SpriteAnimPlaybackType playbackType = SpriteAnimPlaybackType::LOOP);

	const SpriteDefinition& GetSpriteDefAtTime(float seconds) const;
	const AABB2 GetUVsAtTime(float seconds) const;
	Texture& GetTexture() const;
	float GetDuration()const {return m_durationSeconds;} ;

private:
	const SpriteSheet&  m_spriteSheet;
	std::vector<int>	m_animIndexes;
	float				m_durationSeconds = 1.f;
	SpriteAnimPlaybackType	m_playbackType = SpriteAnimPlaybackType::LOOP;
};
