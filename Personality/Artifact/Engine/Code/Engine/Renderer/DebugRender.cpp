#include "Engine/Renderer/DebugRender.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/Camera.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Core/Rgba8.hpp"
#include "Engine/Core/Stopwatch.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Math/Mat44.hpp"
#include "Engine/Math/MathUtils.hpp"
#include <vector>

//-----------------------------------------------------------------------------------------------
struct DebugRenderStaticObjectWorld {
	std::vector<Vertex_PCU> m_verts;
	float m_duration = 0.0;
	Stopwatch m_timer;
	Rgba8 m_startColor;
	Rgba8 m_endColor;
	DebugRenderMode m_mode = DebugRenderMode::USE_DEPTH;
	FillMode m_fillMode = FillMode::SOLID;
	bool m_isTint = false;
	bool m_isText = false;
};


//-----------------------------------------------------------------------------------------------
struct DebugRenderBillBoardObject {
	Vec3 m_origin;
	std::string m_text = "";
	float m_textHeight = 0.0;
	Vec2 m_alignment;
	float m_duration = 0.0;
	Stopwatch m_timer;
	Rgba8 m_startColor;
	Rgba8 m_endColor;
	DebugRenderMode m_mode = DebugRenderMode::USE_DEPTH;
};


//-----------------------------------------------------------------------------------------------
struct DebugRenderObjectScreen {
	Vec2 m_origin;
	std::string m_text = "";
	float m_textHeight = 0.0;
	Vec2 m_alignment;
	float m_duration = 0.0;
	Stopwatch m_timer;
	Rgba8 m_startColor;
	Rgba8 m_endColor;
};

//-----------------------------------------------------------------------------------------------
struct DebugRenderObjectMessage {
	std::string m_text = "";
	float m_duration = 0.0;
	Stopwatch m_timer;
	Rgba8 m_startColor;
	Rgba8 m_endColor;
};



//-----------------------------------------------------------------------------------------------
struct DebugRenderer {
	std::vector<DebugRenderStaticObjectWorld> m_staticWorldObjects;
	std::vector<DebugRenderBillBoardObject> m_worldBillBoards;
	std::vector<DebugRenderObjectScreen> m_screenObjects;
	std::vector<DebugRenderObjectMessage> m_messageObjects;
	DebugRenderConfig m_debugRenderConfig;
	BitmapFont* m_font = nullptr;
	Clock* m_debugClock = nullptr;
};


//-----------------------------------------------------------------------------------------------
static DebugRenderer debugRenderer;


//-----------------------------------------------------------------------------------------------
void DebugRenderSystemStartup(DebugRenderConfig const& config){
	debugRenderer.m_debugRenderConfig = config;
	debugRenderer.m_debugClock = &config.m_renderer->m_debugClock;
	debugRenderer.m_font =
		config.m_renderer->CreateOrGetBitmapFont("Data/Fonts/SquirrelFixedFont");
}


//-----------------------------------------------------------------------------------------------
void DebugRenderSystemShutdown(){

}


//-----------------------------------------------------------------------------------------------
void DebugRenderSetVisible(){
	debugRenderer.m_debugRenderConfig.m_startHidden = false;
}


//-----------------------------------------------------------------------------------------------
void DebugRenderSetHidden(){
	debugRenderer.m_debugRenderConfig.m_startHidden = true;
}


//-----------------------------------------------------------------------------------------------
void DebugRenderClear(){
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.clear();
	debugRenderer.m_worldBillBoards.clear();
	debugRenderer.m_screenObjects.clear();
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugRenderBeginFrame(){

}


//-----------------------------------------------------------------------------------------------
void DebugRenderWorldToCamera(Camera const& camera){
	if (debugRenderer.m_debugRenderConfig.m_startHidden) {
		return;
	}
	g_debugDrawMutex.lock();
	Renderer*& renderer = debugRenderer.m_debugRenderConfig.m_renderer;
	renderer->BeginCamera(camera);
	// render static world objects
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_staticWorldObjects.size());
		objectIndex++) {
		DebugRenderStaticObjectWorld& object = debugRenderer.m_staticWorldObjects[objectIndex];

		// change color (set tint or set lerp value)
		if (object.m_isTint) {
			float elapsedTimeFraction = object.m_timer.GetElapsedFraction();
			elapsedTimeFraction = Clamp(elapsedTimeFraction, 0.f, 1.f);
			renderer->SetModelTint(Rgba8::Lerp(object.m_startColor,
				object.m_endColor, elapsedTimeFraction));
		}
		else {
			if (object.m_startColor != object.m_endColor) {
				float elapsedTimeFraction = object.m_timer.GetElapsedFraction();
				elapsedTimeFraction = Clamp(elapsedTimeFraction, 0.f, 1.f);
				for (int vertIndex = 0; vertIndex < static_cast<int>(object.m_verts.size()); vertIndex++) {
					object.m_verts[vertIndex].m_color = Rgba8::Lerp(object.m_startColor,
						object.m_endColor, elapsedTimeFraction);
				}
			}
		}

		// render object based on different mode
		if (object.m_mode == DebugRenderMode::USE_DEPTH) {
			renderer->SetDepthOptions(DepthTest::LESS_EQUAL, true);
		}
		else if (object.m_mode == DebugRenderMode::ALWAYS) {
			renderer->SetDepthOptions(DepthTest::ALWAYS, false);
		}
		else {
			renderer->SetDepthOptions(DepthTest::GREATER, false);
			renderer->SetModelTint(Rgba8(255,255,255,128));
			renderer->SetCullMode(CullMode::BACK);
			renderer->SetFillMode(object.m_fillMode);
			renderer->BindShader(nullptr);
			renderer->SetBlendMode(BlendMode::ALPHA);
			renderer->BindTexture(nullptr);
			renderer->DrawVertexArray(static_cast<int>(object.m_verts.size()), &object.m_verts[0]);

			renderer->SetDepthOptions(DepthTest::LESS_EQUAL, false);
			renderer->SetModelTint(Rgba8::WHITE);
		}
			renderer->SetFillMode(object.m_fillMode);
		

		renderer->BindShader(nullptr);
		renderer->SetBlendMode(BlendMode::ALPHA);
		// if it is text render text font
		if (object.m_isText) {
			renderer->BindTexture(&debugRenderer.m_font->GetTexture());
			renderer->SetCullMode(CullMode::NONE);
		}
		else {
			renderer->BindTexture(nullptr);
			renderer->SetCullMode(CullMode::BACK);
		}
		
		
		renderer->DrawVertexArray(static_cast<int>(object.m_verts.size()), &object.m_verts[0]);
	}

	// render billboard objects
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_worldBillBoards.size());
		objectIndex++) {
		DebugRenderBillBoardObject& object = debugRenderer.m_worldBillBoards[objectIndex];
		
		// calculate color
		Rgba8 textColor = object.m_startColor;
		if (object.m_startColor != object.m_endColor) {
			float elapsedTimeFraction = object.m_timer.GetElapsedFraction();
			textColor = Rgba8::Lerp(object.m_startColor, object.m_endColor, elapsedTimeFraction);
		}
		std::vector<Vertex_PCU> objectVerts;
		debugRenderer.m_font->AddVertsForText3D(objectVerts, Vec3::ZERO, object.m_alignment,
			object.m_textHeight, object.m_text, textColor);

		// using camera opposing xyz
		Mat44 cameraTransform = camera.GetCameraBasis();
		Mat44 transform(cameraTransform.GetJBasis3D() * -1.f, cameraTransform.GetKBasis3D(), 
			cameraTransform.GetIBasis3D() * -1.f, object.m_origin);
		//camera facing code
// 		Vec3 forward = camera.GetCameraViewPosition() - object.m_origin;
// 		Mat44 transform;
// 		transform.SetTranslation3D(object.m_origin);
// 		transform.SetIJK3D(forward.GetNormalized(), Vec3(0.f, 1.f, 0.f), Vec3(0.f, 0.f, 1.f));
// 		transform.Orthonormalize_XFwd_YLeft_ZUp();
// 		transform.SetIJK3D(transform.GetJBasis3D(), transform.GetKBasis3D(), transform.GetIBasis3D());

		for (int vertIndex = 0; vertIndex < static_cast<int>(objectVerts.size()); vertIndex++) {
			objectVerts[vertIndex].m_position =
				transform.TransformPosition3D(objectVerts[vertIndex].m_position);
		}
		// render object based on different mode
		if (object.m_mode == DebugRenderMode::USE_DEPTH) {
			renderer->SetDepthOptions(DepthTest::LESS_EQUAL, true);
		}
		else if (object.m_mode == DebugRenderMode::ALWAYS) {
			renderer->SetDepthOptions(DepthTest::ALWAYS, false);
		}
		else {
			renderer->SetDepthOptions(DepthTest::GREATER, false);
			renderer->SetModelTint(Rgba8::BLACK);
			renderer->SetCullMode(CullMode::BACK);
			renderer->SetFillMode(FillMode::SOLID);
			renderer->BindShader(nullptr);
			renderer->SetBlendMode(BlendMode::ALPHA);
			renderer->BindTexture(&debugRenderer.m_font->GetTexture());
			renderer->DrawVertexArray(static_cast<int>(objectVerts.size()), &objectVerts[0]);

			renderer->SetDepthOptions(DepthTest::LESS_EQUAL, true);
			renderer->SetModelTint(Rgba8::WHITE);
		}
		renderer->SetFillMode(FillMode::SOLID);
		renderer->SetModelTint(Rgba8::WHITE);
		renderer->BindShader(nullptr);
		renderer->SetBlendMode(BlendMode::ALPHA);
		renderer->BindTexture(&debugRenderer.m_font->GetTexture());
		renderer->SetCullMode(CullMode::BACK);
		renderer->DrawVertexArray(static_cast<int>(objectVerts.size()), &objectVerts[0]);
	}
	renderer->EndCamera(camera);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugRenderScreenToCamera(Camera const& camera){
	if (debugRenderer.m_debugRenderConfig.m_startHidden) {
		return;
	}
	g_debugDrawMutex.lock();
	Renderer*& renderer = debugRenderer.m_debugRenderConfig.m_renderer;
	renderer->BeginCamera(camera);
	// render screen objects
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_screenObjects.size());
		objectIndex++) {
		DebugRenderObjectScreen& object = debugRenderer.m_screenObjects[objectIndex];

		AABB2 textBox(Vec2(object.m_origin.x, object.m_origin.y - object.m_textHeight),
			Vec2(camera.GetOrthoTopRight().x, object.m_origin.y));

		// get text box
		Rgba8 textColor = object.m_startColor;
		if (object.m_startColor != object.m_endColor) {
			float elapsedTimeFraction = object.m_timer.GetElapsedFraction();
			textColor = Rgba8::Lerp(object.m_startColor, object.m_endColor, elapsedTimeFraction);
		}

		std::vector<Vertex_PCU> objectVerts;
		debugRenderer.m_font->AddVertsForTextInBox2D(objectVerts, textBox, object.m_textHeight, object.m_text,
			textColor, 1.f, object.m_alignment);
		renderer->BindShader(nullptr);
		renderer->SetBlendMode(BlendMode::ALPHA);
		renderer->BindTexture(&debugRenderer.m_font->GetTexture());
		renderer->DrawVertexArray(static_cast<int>(objectVerts.size()), &objectVerts[0]);
	}

	// render messages from newest to oldest
	std::vector<DebugRenderObjectMessage> reverseMessages = debugRenderer.m_messageObjects;
	reverse(reverseMessages.begin(), reverseMessages.end());
	for (int objectIndex = 0; objectIndex < static_cast<int>(reverseMessages.size());
		objectIndex++) {
		DebugRenderObjectMessage& object = reverseMessages[objectIndex];

		float textHeight = g_gameConfigBlackboard.GetValue("messageTextHeight", 0.f);
		Vec2 screenTopRight = camera.GetOrthoTopRight();
		AABB2 textBox(Vec2(camera.GetOrthoBottomLeft().x, screenTopRight.y -
			objectIndex * textHeight - textHeight), Vec2(screenTopRight.x, screenTopRight.y -
			objectIndex * textHeight));
		Vec2 alignment = g_gameConfigBlackboard.GetValue("messageAlignment", Vec2::ZERO);;

		Rgba8 textColor = object.m_startColor;
		if (object.m_startColor != object.m_endColor) {
			float elapsedTimeFraction = object.m_timer.GetElapsedFraction();
			textColor = Rgba8::Lerp(object.m_startColor, object.m_endColor, elapsedTimeFraction);
		}

		std::vector<Vertex_PCU> aabbVerts;
		Rgba8 boxColor = Rgba8::DARK_GREY;
		boxColor.a = 125;
		AddVertsForAABB2D(aabbVerts, textBox, boxColor);
		renderer->BindShader(nullptr);
		renderer->SetBlendMode(BlendMode::ALPHA);
		renderer->BindTexture(nullptr);
		renderer->DrawVertexArray(static_cast<int>(aabbVerts.size()), &aabbVerts[0]);

		std::vector<Vertex_PCU> objectVerts;
		debugRenderer.m_font->AddVertsForTextInBox2D(objectVerts, textBox, textHeight, object.m_text,
			textColor, 1.f, alignment);
		renderer->BindShader(nullptr);
		renderer->SetBlendMode(BlendMode::ALPHA);
		renderer->BindTexture(&debugRenderer.m_font->GetTexture());
		renderer->DrawVertexArray(static_cast<int>(objectVerts.size()), &objectVerts[0]);
	}
	renderer->EndCamera(camera);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugRenderEndFrame(){
	// clear all objects that are expired 
	g_debugDrawMutex.lock();
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_staticWorldObjects.size());) {
		DebugRenderStaticObjectWorld object = debugRenderer.m_staticWorldObjects[objectIndex];
		if (object.m_timer.HasElapsed() && object.m_duration >= 0.f) {
			debugRenderer.m_staticWorldObjects.erase(debugRenderer.m_staticWorldObjects.begin() + objectIndex);
		}
		else {
			objectIndex++;
		}
	}
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_worldBillBoards.size());) {
		DebugRenderBillBoardObject object = debugRenderer.m_worldBillBoards[objectIndex];
		if (object.m_timer.HasElapsed() && object.m_duration >= 0.f) {
			debugRenderer.m_worldBillBoards.erase(debugRenderer.m_worldBillBoards.begin() + objectIndex);
		}
		else {
			objectIndex++;
		}
	}
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_screenObjects.size());) {
		DebugRenderObjectScreen object = debugRenderer.m_screenObjects[objectIndex];
		if (object.m_timer.HasElapsed() && object.m_duration >= 0.f) {
			debugRenderer.m_screenObjects.erase(debugRenderer.m_screenObjects.begin() + objectIndex);
		}
		else {
			objectIndex++;
		}
	}
	for (int objectIndex = 0; objectIndex < static_cast<int>(debugRenderer.m_messageObjects.size());) {
		DebugRenderObjectMessage object = debugRenderer.m_messageObjects[objectIndex];
		if (object.m_timer.HasElapsed() && object.m_duration >= 0.f) {
			debugRenderer.m_messageObjects.erase(debugRenderer.m_messageObjects.begin() + objectIndex);
		}
		else {
			objectIndex++;
		}
	}
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldPoint(Vec3 pos, float size, Rgba8 startColor, Rgba8 endColor, float duration, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> pointVerts;
	AddVertsForCube3D_RH(pointVerts, startColor, pos, size);

	Stopwatch pointTimer;
	pointTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld pointObject = {pointVerts, duration, pointTimer, startColor, endColor, mode};
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(pointObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldPoint(Vec3 pos, float size, Rgba8 color, float duration, DebugRenderMode mode){
	std::vector<Vertex_PCU> pointVerts;
	AddVertsForCube3D_RH(pointVerts, color, pos, size);

	Stopwatch pointTimer;
	pointTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld pointObject = { pointVerts, duration, pointTimer, color, color, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(pointObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldPoint(Vec3 pos, Rgba8 color, float duration, DebugRenderMode mode){
	std::vector<Vertex_PCU> pointVerts;
	AddVertsForCube3D_RH(pointVerts, color, pos, 1.f);

	Stopwatch pointTimer;
	pointTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld pointObject = { pointVerts, duration, pointTimer, color, color, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(pointObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldLine(Vec3 start, Vec3 end, Rgba8 startColor, Rgba8 endColor, float thickness, 
	float duration, DebugRenderMode mode){
	std::vector<Vertex_PCU> lineVerts;
	AddVertsForLine3D(lineVerts, start, end, thickness * 0.5f, startColor);
	Stopwatch lineTimer;
	lineTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld lineObject = { lineVerts, duration, lineTimer, startColor, endColor, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(lineObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldLine(Vec3 start, Vec3 end, Rgba8 color, float thickness, float duration, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> lineVerts;
	AddVertsForLine3D(lineVerts, start, end, thickness * 0.5f, color);
	Stopwatch lineTimer;
	lineTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld lineObject = { lineVerts, duration, lineTimer, color, color, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(lineObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldWireCylinder(Vec3 base, Vec3 top, float radius, float duration, Rgba8 color, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> cylinderVerts;
	AddVertsForCylinder(cylinderVerts, base, top, radius, color, 12);
	Stopwatch cylinderTimer;
	cylinderTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld cylinderObject = { cylinderVerts, duration, cylinderTimer, color, 
		Rgba8::RED, mode, FillMode::WIREFRAME };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(cylinderObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldWireSphere(Vec3 center, float radius, float duration, Rgba8 color, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> wireSphereVerts;
	AddVertsForUVSphereZ3D(wireSphereVerts, center, radius, 12,12, color);
	Stopwatch wireSphereTimer;
	wireSphereTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld wireSphereObject = { wireSphereVerts, duration, wireSphereTimer, color, 
		Rgba8::RED, mode, FillMode::WIREFRAME };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(wireSphereObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldSphere(Vec3 center, float radius, float duration, Rgba8 color, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> sphereVerts;
	AddVertsForUVSphereZ3D(sphereVerts, center, radius, 32, 32, color);
	Stopwatch sphereTimer;
	sphereTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld sphereObject = { sphereVerts, duration, sphereTimer, color,
		color, mode, FillMode::SOLID };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(sphereObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldArrow(Vec3 start, Vec3 end, Rgba8 color, float thickness, float duration, 
	DebugRenderMode mode){
	std::vector<Vertex_PCU> arrowVerts;
	AddVertsForArrow3D(arrowVerts, start, end, thickness * 0.5f, color);
	Stopwatch arrowTimer;
	arrowTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld arrowObject = { arrowVerts, duration, arrowTimer, color, color, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(arrowObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldBasis(Mat44 basis, Rgba8 startTint, Rgba8 endTint, float duration, 
	DebugRenderMode mode){
	Vec3 location = basis.GetTranslation3D();
	Vec3 xFwd = basis.GetIBasis3D().GetNormalized();
	Vec3 yLft = basis.GetJBasis3D().GetNormalized();
	Vec3 zUp = basis.GetKBasis3D().GetNormalized();


	std::vector<Vertex_PCU> basisVerts;
	AddVertsForArrow3D(basisVerts, location, location + xFwd, 0.05f, Rgba8::RED);
	AddVertsForArrow3D(basisVerts, location, location + yLft, 0.05f, Rgba8::GREEN);
	AddVertsForArrow3D(basisVerts, location, location + zUp, 0.05f, Rgba8::BLUE);
	Stopwatch basisTimer;
	basisTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld basisObject = { basisVerts, duration, basisTimer, startTint,
		endTint, mode, FillMode::SOLID, true };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(basisObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldBasis(Mat44 basis, float duration, DebugRenderMode mode){
	Vec3 location = basis.GetTranslation3D();
	Vec3 xFwd = basis.GetIBasis3D().GetNormalized();
	Vec3 yLft = basis.GetJBasis3D().GetNormalized();
	Vec3 zUp = basis.GetKBasis3D().GetNormalized();


	std::vector<Vertex_PCU> basisVerts;
	AddVertsForArrow3D(basisVerts, location, location+xFwd, 0.05f, Rgba8::RED);
	AddVertsForArrow3D(basisVerts, location, location + yLft, 0.05f, Rgba8::GREEN);
	AddVertsForArrow3D(basisVerts, location, location + zUp, 0.05f, Rgba8::BLUE);
	Stopwatch basisTimer;
	basisTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld basisObject = { basisVerts, duration, basisTimer, Rgba8::WHITE, 
		Rgba8::WHITE, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(basisObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldText(std::string const& text, float textHeight, Vec2 alignment, Rgba8 startColor, 
	Rgba8 endColor, Mat44 const& transform, float duration, DebugRenderMode mode){
	std::vector<Vertex_PCU> textVerts;
	
	float lengthX = static_cast<float>(text.size()) * textHeight;
	float lengthY = textHeight;
	Vec2 textMin = Vec2(-lengthX * alignment.x, -lengthY * alignment.y);
	debugRenderer.m_font->AddVertsForText2D(textVerts, textMin, textHeight, text, startColor);
	for (int vertIndex = 0; vertIndex < static_cast<int>(textVerts.size()); vertIndex++) {
		textVerts[vertIndex].m_position =
			transform.TransformPosition3D(textVerts[vertIndex].m_position);
	}
	Stopwatch textTimer;
	textTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld textObject = { textVerts, duration, textTimer, startColor,
		endColor, mode, FillMode::SOLID,false,true };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(textObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldText(std::string const& text, float textHeight, Vec2 alignment, Rgba8 color, 
	Mat44 const& transform, float duration, DebugRenderMode mode){
	std::vector<Vertex_PCU> textVerts;

	float lengthX = static_cast<float>(text.size()) * textHeight;
	float lengthY = textHeight;
	Vec2 textMin = Vec2(-lengthX * alignment.x, -lengthY * alignment.y);
	debugRenderer.m_font->AddVertsForText2D(textVerts, textMin,textHeight, text, color);
	for (int vertIndex = 0; vertIndex < static_cast<int>(textVerts.size()); vertIndex++) {
		textVerts[vertIndex].m_position =
			transform.TransformPosition3D(textVerts[vertIndex].m_position);
	}
	Stopwatch textTimer;
	textTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderStaticObjectWorld textObject = { textVerts, duration, textTimer, color,
		color, mode, FillMode::SOLID,false,true };
	g_debugDrawMutex.lock();
	debugRenderer.m_staticWorldObjects.push_back(textObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldBillboardText(Vec3 origin, std::string const& text, float textHeight, 
	Vec2 alignment, Rgba8 startcolor, Rgba8 endColor, float duration, DebugRenderMode mode){
	Stopwatch billboardTimer;
	billboardTimer.Start(*debugRenderer.m_debugClock, duration);
	
	DebugRenderBillBoardObject billboardObject = { origin, text, textHeight, alignment, duration,
	billboardTimer, startcolor, endColor, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_worldBillBoards.push_back(billboardObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddWorldBillboardText(Vec3 origin, std::string const& text, float textHeight, 
	Vec2 alignment, Rgba8 color, float duration, DebugRenderMode mode){
	Stopwatch billboardTimer;
	billboardTimer.Start(*debugRenderer.m_debugClock, duration);

	DebugRenderBillBoardObject billboardObject = { origin, text, textHeight, alignment, duration,
	billboardTimer, color, color, mode };
	g_debugDrawMutex.lock();
	debugRenderer.m_worldBillBoards.push_back(billboardObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddScreenText(std::string const& text, Vec2 position, float duration, Vec2 pivot, 
	float size, Rgba8 startColor, Rgba8 endColor){
	Stopwatch screenTextTimer;
	screenTextTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderObjectScreen screenTextObject = { position,text, size, pivot, duration,
	screenTextTimer, startColor, endColor };
	g_debugDrawMutex.lock();
	debugRenderer.m_screenObjects.push_back(screenTextObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddScreenText(std::string const& text, Vec2 position, float duration, Vec2 pivot, 
	float size, Rgba8 color){
	Stopwatch screenTextTimer;
	screenTextTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderObjectScreen screenTextObject = { position,text, size, pivot, duration,
	screenTextTimer, color, color };
	g_debugDrawMutex.lock();
	debugRenderer.m_screenObjects.push_back(screenTextObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddMessage(std::string const& text, float duration, Rgba8 startColor, Rgba8 endColor){
	Stopwatch screenMessageTimer;
	screenMessageTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderObjectMessage screenMessageObject = {text, duration,
	screenMessageTimer, startColor, endColor };
	g_debugDrawMutex.lock();
	debugRenderer.m_messageObjects.push_back(screenMessageObject);
	g_debugDrawMutex.unlock();
}


//-----------------------------------------------------------------------------------------------
void DebugAddMessage(std::string const& text, float duration, Rgba8 color){
	Stopwatch screenMessageTimer;
	screenMessageTimer.Start(*debugRenderer.m_debugClock, duration);
	DebugRenderObjectMessage screenMessageObject = { text, duration,
	screenMessageTimer, color, color };
	g_debugDrawMutex.lock();
	debugRenderer.m_messageObjects.push_back(screenMessageObject);
	g_debugDrawMutex.unlock();
}
