#include "Engine/Renderer/RenderContext.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/Window.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/StringUtils.hpp"

#define WIN32_LEAN_AND_MEAN		// Always #define this before #including <windows.h>
#include <windows.h>			// #include this (massive, platform-specific) header in very few places
#include <math.h>

#include "Engine/Renderer/D3DInternal.hpp"
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#include "ThirdParty/stb/stb_image.h"

//-----------------------------------------------------------------------------------------------
Renderer::Renderer(RenderContextConfig const& renderConfig):
	m_config(renderConfig){

}


//-----------------------------------------------------------------------------------------------
Renderer::~Renderer() {
	for (int textureIndex = 0; textureIndex < static_cast<int>(m_loadedTexture.size()); textureIndex++) {
		Texture * texture = m_loadedTexture[textureIndex];
		delete texture;
		texture = nullptr;
	}
	m_loadedTexture.clear();

	for (int fontIndex = 0; fontIndex < static_cast<int>(m_loadedFonts.size()); fontIndex++) {
		BitmapFont* font = m_loadedFonts[fontIndex];
		delete font;
		font = nullptr;
	}
	m_loadedFonts.clear();
}


//-----------------------------------------------------------------------------------------------
void Renderer::DrawVertexArray(int numVertexes, Vertex_PCU const* vertexes) {
	UNUSED(numVertexes);
	UNUSED(vertexes);
}


//-----------------------------------------------------------------------------------------------
void Renderer::Startup() {
	CreateRC();
	AquireBackBufferRTV();
}


//-----------------------------------------------------------------------------------------------
void Renderer::Shutdown() {
	DestroyRenderContext();
}


//-----------------------------------------------------------------------------------------------
#define DX_SAFE_RELEASE(obj) if(obj!=nullptr) {(obj)->Release(); (obj) = nullptr;}
void Renderer::DestroyRenderContext()
{
	DX_SAFE_RELEASE(m_device);
	DX_SAFE_RELEASE(m_context);
	DX_SAFE_RELEASE(m_swapChain);
	DX_SAFE_RELEASE(m_backBufferRTV);
}

//-----------------------------------------------------------------------------------------------
void Renderer::ClearScreen(Rgba8 const& clearColor) {
	float clearColorAsFloats[4];
	clearColor.GetAsFloats(clearColorAsFloats);

	m_context->ClearRenderTargetView(m_backBufferRTV, clearColorAsFloats);
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::CreateOrGetTextureFromFile(const char* imageFilePath){
	// if the texture is already loaded, return the loaded texture
	std::string imageFilePathString(imageFilePath);
	for (int textureIndex = 0; textureIndex < static_cast<int> (m_loadedTexture.size()); 
		textureIndex++) {
		if (imageFilePathString == (m_loadedTexture[textureIndex]->m_imageFilePath)) {
			return m_loadedTexture[textureIndex];
		}
	}

	// if texture is not loaded, load texture
	Texture* texture = CreateTextureFromFile(imageFilePath);
	m_loadedTexture.push_back(texture);
	return texture;
}

//-----------------------------------------------------------------------------------------------
void Renderer::BindTexture(Texture const* texture){
	// if texture is not a nullptr, check if it is the current texture, then do nothing
	// else if the texture is a new texture, bind the texture and then set current texture
	if (texture)
	{
	}
	else
	{
	}
}


//-----------------------------------------------------------------------------------------------
BitmapFont* Renderer::CreateOrGetBitmapFont(const char* bitmapFontFilePathWithNoExtension){
	std::string bitmapFilePathString(bitmapFontFilePathWithNoExtension);
	for (int fontIndex = 0; fontIndex < static_cast<int> (m_loadedFonts.size());
		fontIndex++) {
		if (bitmapFilePathString == (m_loadedFonts[fontIndex]->m_fontFilePathNameWithNoExtension)) {
			return m_loadedFonts[fontIndex];
		}
	}

	// if texture is not loaded, load texture
	BitmapFont* newFont = CreateBitmapFontFromFile(bitmapFontFilePathWithNoExtension);
	m_loadedFonts.push_back(newFont);
	return newFont;
}


//-----------------------------------------------------------------------------------------------
void Renderer::SetBlendMode(BlendMode blendMode) {
	if (blendMode == BlendMode::ALPHA) {
	}
	else if (blendMode == BlendMode::ADDITIVE) {
	}
	else {
		ERROR_AND_DIE(Stringf("Unknown / unsupported blend mode #%i", blendMode));
	}

}


//-----------------------------------------------------------------------------------------------
BitmapFont* Renderer::CreateBitmapFontFromFile(char const* fontFilePathNameWithNoExtension){
	std::string imagePath = fontFilePathNameWithNoExtension;
	imagePath += ".png";
	Texture* fontTexture = CreateTextureFromFile(imagePath.c_str());
	BitmapFont* bitMapFont = new BitmapFont(fontFilePathNameWithNoExtension, *fontTexture);
	return bitMapFont;
}


//-----------------------------------------------------------------------------------------------
void Renderer::BeginFrame() {

}


//-----------------------------------------------------------------------------------------------
void Renderer::EndFrame() {
	//Sleep(1);

	m_swapChain->Present(0, 0);
}


//-----------------------------------------------------------------------------------------------
void Renderer::BeginCamera(const Camera& camera) {
	Vec2 bottomLeft = camera.GetOrthoBottomLeft();
	Vec2 topRight = camera.GetOrthoTopRight();
}


//-----------------------------------------------------------------------------------------------
void Renderer::EndCamera(const Camera& camera) {
	UNUSED(camera);

}


//-----------------------------------------------------------------------------------------------
void Renderer::CreateRC(){
	HRESULT hr;

	DWORD flags = 0;
	flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#if defined(_DEBUG)
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	IntVec2 dimension = m_config.m_windowContext->GetDimensions();

	DXGI_SWAP_CHAIN_DESC swapChainDesc = {0};
	swapChainDesc.BufferDesc.Width = dimension.x;
	swapChainDesc.BufferDesc.Height = dimension.y;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.OutputWindow = (HWND)(m_config.m_windowContext->GetHwnd());
	swapChainDesc.Windowed = true;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.Flags = 0;



	hr = ::D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		flags,
		nullptr,
		0,
		D3D11_SDK_VERSION,
		&swapChainDesc,
		&m_swapChain,
		&m_device,
		nullptr,
		&m_context);

	ASSERT_OR_DIE(SUCCEEDED(hr), "Failed to create D3D11 device");
}


//-----------------------------------------------------------------------------------------------
void Renderer::AquireBackBufferRTV(){
	HRESULT hr;

	ID3D11Texture2D* backBuffer;
	hr = m_swapChain->GetBuffer(
		0,
		__uuidof(ID3D11Texture2D),
		(void**)&backBuffer
	);

	if (!SUCCEEDED(hr)) ERROR_AND_DIE("Get not get swap chain buffer");

	hr = m_device->CreateRenderTargetView(
		backBuffer,
		NULL,
		&m_backBufferRTV
	);

	DX_SAFE_RELEASE(backBuffer);
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::CreateTextureFromFile(const char* imageFilePath){

	// Create a new Texture object 
	Texture* newTexture = new Texture();
	newTexture->m_imageFilePath = imageFilePath;
	newTexture->m_dimensions = IntVec2(512, 512);
	//newTexture->m_textureID = textureID;
	return newTexture;
}
