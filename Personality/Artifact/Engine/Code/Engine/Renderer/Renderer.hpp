#pragma once
#include "Engine/Renderer/Camera.hpp"
#include "Engine/Renderer/Shader.hpp"
#include "Engine/Renderer/DebugRender.hpp"
#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Math/Mat44.hpp"
#include <cstddef>
#include <vector>


//-----------------------------------------------------------------------------------------------
class Window;
class Texture;
class BitmapFont;
class ConstantBuffer;
class VertexBuffer;
class Shader;

//-----------------------------------------------------------------------------------------------
struct ID3D11Device;
struct ID3D11DeviceContext;
struct IDXGISwapChain;
struct ID3D11RenderTargetView;
struct ID3D11BlendState;
struct ID3D11DeviceChild;
struct ID3D11RasterizerState;
struct IDXGIDebug;
struct ID3D11SamplerState; 
struct ID3D11Texture2D;
struct ID3D11DepthStencilState;

//-----------------------------------------------------------------------------------------------
struct RendererConfig {
	Window* m_windowContext = nullptr;
};

//-----------------------------------------------------------------------------------------------
struct CameraConstants {
	Mat44 ViewMatrix;
	Mat44 ProjectionMatrix;
};

struct ModelConstants{
	Mat44 ModelMatrix;
	float m_isTint[4] = {};
};

//-----------------------------------------------------------------------------------------------
enum class BlendMode {
	OPAQUE_ = 0,
	ALPHA,
	ADDITIVE,
	COUNT,
};

//-----------------------------------------------------------------------------------------------
enum class WindingOrder {
	CounterClockwise = true,
	Clockwise = false,
};

//-----------------------------------------------------------------------------------------------
enum class CullMode: unsigned char {
	NONE = 1, 
	FRONT, 
	BACK, 
};

enum class FillMode {
	SOLID,
	WIREFRAME,
};

//-----------------------------------------------------------------------------------------------
enum class SamplerMode { 
	POINT_CLAMP = 0, 
	POINT_WRAP, 
	BILINEAR_CLAMP, 
	BILINEAR_WRAP, 
	COUNT, 
};

enum class DepthTest : unsigned char { 
	ALWAYS, 
	NEVER, 
	EQUAL, 
	NOT_EQUAL, 
	LESS, 
	LESS_EQUAL, 
	GREATER, 
	GREATER_EQUAL, 
};

//-----------------------------------------------------------------------------------------------
class Renderer {
public:
	// Construction/Destruction
	~Renderer();												
	Renderer(RendererConfig const& renderConfig);			

	void Startup();
	void Shutdown();
	void DestroyRenderContext();

	void DrawVertexArray(int numVertexes, Vertex_PCU const* vertexes);
	void ClearScreen(Rgba8 const& clearColor);
	Texture* CreateOrGetTextureFromFile(const char* imageFilePath);
	void BindTexture(Texture* texture, int slot = 0);
	void SetSamplerMode(SamplerMode mode, int slot = 0);
	BitmapFont* CreateOrGetBitmapFont(const char* bitmapFontFilePathWithNoExtension);
	void SetBlendMode(BlendMode blendMode);
	void SetCullMode(CullMode cullMode);
	void SetFillMode(FillMode m_fillMode);
	void SetModelMatrix(Mat44 const& model);
	void SetModelTint(Rgba8 const& m_isTint);
	void SetWindingOrder(WindingOrder frontFaceWindingOrder);
	

	void BeginFrame();
	void EndFrame();
	void BeginCamera(const Camera& camera );
	void EndCamera(const Camera& camera);

	void DrawVertexBuffer(VertexBuffer const* vbo, int vertexCount);
	VertexBuffer* CreateDynamicVertexBuffer(size_t const initialByteSize = 0);
	void DestroyVertexBuffer(VertexBuffer* vbo);


	void BindShader(Shader const* shader);
	void BindShaderByName(char const* shaderName);
	Shader* CreateOrGetShader(char const* fileNameWithoutExtension);

	Texture* CreateDepthStencilTexture(IntVec2 size);
	void SetDepthOptions(DepthTest test = DepthTest::ALWAYS, bool writeDepth = 1.f);
	void ClearDepth(float depthValue = 1.0f);


	ConstantBuffer* CreateConstantBuffer(size_t const size); 
	void DestroyConstantBuffer(ConstantBuffer* cbo);     
	void BindConstantBuffer(int slot, ConstantBuffer* constantBuffer);

	static ID3D11RasterizerState* CreateRasterState(ID3D11Device* d3dDevice, CullMode cullMode, 
		FillMode m_fillMode, WindingOrder winding);
	static bool DebugRenderClears(EventArgs& args);
	static bool DebugRenderToggle(EventArgs& args);
	static bool DebugRenderSetTimeScale(EventArgs& args);

public:
	std::vector<Texture*>		m_loadedTexture;
	std::vector<Shader*>		m_loadedShaders;
	std::vector< BitmapFont*>	m_loadedFonts;
	Texture const*				m_currentTexture = nullptr;
	Texture*					m_defaultDiffuse = nullptr;
	Shader*						m_currentShader = nullptr;
	Clock						m_debugClock;
	Camera const*				m_currentCamera = nullptr;

	//D3D11 Objects
	ID3D11Device* m_device = nullptr;
	ID3D11DeviceContext* m_context = nullptr;
	IDXGISwapChain* m_swapChain = nullptr;
	Texture* m_backBufferRTV = nullptr;

protected:
	void CreateRC();
	void AquireBackBufferRTV();
	void ComileShader(Shader* shader, char const* source, std::string const& entryPoint);
	void LoadAndCompileShader(Shader* shader, std::string& filename);

	void StartupDebugAPI();     
	void ShutdownDebugAPI();    
	void ReportLiveObjects();     
	bool IsRasterStateDirty();  // lazy update of raster states

	void CreateSamplerStates();
	void DestroySamplerStates();
	Texture* RegisterColorTexture( Rgba8 const& color);
	Texture* RegisterTexture( Texture* texture);

	void UpdateDepthStencilState();

protected:
	RendererConfig			m_config;
	ConstantBuffer*			m_cameraCBO = nullptr;
	ConstantBuffer*			m_modelCBO = nullptr;
	VertexBuffer*			m_immediateVBO = nullptr;
	Shader*					m_defaultShader = nullptr;

	ID3D11BlendState*		m_blendStateByMode[static_cast<int>(BlendMode::COUNT)] = {};

	ID3D11RasterizerState*	m_currentRasterState = nullptr;
	CullMode				m_currentCullMode = CullMode::NONE;
	CullMode				m_desiredCullMode = CullMode::NONE;
	FillMode				m_currentFillMode = FillMode::SOLID;
	FillMode				m_desiredFillMode = FillMode::SOLID;
	WindingOrder			m_currentWindingOrder = WindingOrder::CounterClockwise;
	ID3D11SamplerState*		m_samplerStates[static_cast<int>(SamplerMode::COUNT)] = {};

	ModelConstants			m_modelConstant;

	void*					m_debugModule = nullptr;
	IDXGIDebug*				m_debug = nullptr;
	
	Texture*				m_defaultDepthStencil;     
	DepthTest				m_depthTest = DepthTest::ALWAYS;     
	bool					m_writeDepth = false;     
	ID3D11DepthStencilState* m_depthStencilState = nullptr;
private:
	BitmapFont* CreateBitmapFontFromFile(char const* fontFilePathNameWithNoExtension);
};


