#pragma once
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include <string>

enum TextBoxMode {
	TEXT_BOX_MODE_SHRINK,
	TEXT_BOX_MODE_OVERRUN,
	NUM_TEXT_BOX_MODES
};

//------------------------------------------------------------------------------------------------
class BitmapFont
{
	friend class Renderer; // Only the RenderContext can create new BitmapFont objects!
public:
	static const Vec2 ALIGN_CENTERED;

public:
	Texture& GetTexture() const;

	void AddVertsForText2D(std::vector<Vertex_PCU>& vertexArray, Vec2 const& textMins,
		float cellHeight, std::string const& text, Rgba8 const& m_isTint = Rgba8::WHITE, float cellAspect = 1.f);
	void AddVertsForText3D(std::vector<Vertex_PCU>& vertexArray, Vec3 const& origin, Vec2 alignment,
		float cellHeight, std::string const& text, Rgba8 const& m_isTint = Rgba8::WHITE, float cellAspect = 1.f);
	void AddVertsForTextInBox2D(std::vector<Vertex_PCU>& vertexArray, const AABB2& box, float cellHeight,
		const std::string& text, const Rgba8& m_isTint = Rgba8::WHITE, float cellAspect = 1.f,
		const Vec2& alignment = ALIGN_CENTERED, TextBoxMode mode = TEXT_BOX_MODE_SHRINK, int maxGlyphsToDraw = 99999999);


protected:
	float GetGlyphAspect(int glyphUnicode) const; // For now this will always return 1.0f!!!

protected:
	std::string	m_fontFilePathNameWithNoExtension;
	SpriteSheet	m_fontGlyphsSpriteSheet;

private:
	BitmapFont(char const* fontFilePathNameWithNoExtension, Texture& fontTexture);
};
