#include "Engine/Renderer/SpriteDefinition.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Math/AABB2.hpp"

//-----------------------------------------------------------------------------------------------
SpriteSheet::SpriteSheet(Texture& texture, IntVec2 const& simpleGridLayout):
	m_texture(texture){
	// based on grid layout, cut the sheet into sprites
	// the order is from top left to bottom right
	float uForEachSprite = 1.f / static_cast<float>(simpleGridLayout.x);
	float vForEachSprite = 1.f / static_cast<float>(simpleGridLayout.y);
	for (int vIndex = simpleGridLayout.y; vIndex > 0; vIndex--) {
		for (int uIndex = 1; uIndex <= simpleGridLayout.x; uIndex++) {
			float uOffSet = 1.f / (static_cast<float>(simpleGridLayout.x) * 100.f);
			float vOffSet = 1.f / (static_cast<float>(simpleGridLayout.y) * 100.f);
			Vec2 uvMins(static_cast<float>(uIndex - 1) * uForEachSprite + uOffSet, 
				static_cast<float>(vIndex - 1) * vForEachSprite + vOffSet);
			Vec2 uvMaxs(static_cast<float>(uIndex) * uForEachSprite - uOffSet, 
				static_cast<float>(vIndex) * vForEachSprite - vOffSet);
			SpriteDefinition spritedef(*this, static_cast<int>(m_spriteDefs.size()),
				uvMins, uvMaxs);
			m_spriteDefs.push_back(spritedef);
		}
	}
}


//-----------------------------------------------------------------------------------------------
SpriteSheet::~SpriteSheet(){
	m_spriteDefs.clear();
}


//-----------------------------------------------------------------------------------------------
Texture& SpriteSheet::GetTexture() const{
	return m_texture;
}

//-----------------------------------------------------------------------------------------------
int SpriteSheet::GetNumSprites() const{
	return static_cast<int>(m_spriteDefs.size());
}

//-----------------------------------------------------------------------------------------------
SpriteDefinition const& SpriteSheet::GetSpriteDefinition(int spriteIndex) const{
	return m_spriteDefs[spriteIndex];
}


//-----------------------------------------------------------------------------------------------
void SpriteSheet::GetSpriteUVs(Vec2& out_uvAtMins, Vec2& out_uvAtMaxs, int spriteIndex) const{
	m_spriteDefs[spriteIndex].GetUVs(out_uvAtMins, out_uvAtMaxs);
}


//-----------------------------------------------------------------------------------------------
AABB2 SpriteSheet::GetSpriteUVs(int spriteIndex) const {
	Vec2 uvMins, uvMaxs;
	m_spriteDefs[spriteIndex].GetUVs(uvMins, uvMaxs);
	return AABB2(uvMins, uvMaxs);
}