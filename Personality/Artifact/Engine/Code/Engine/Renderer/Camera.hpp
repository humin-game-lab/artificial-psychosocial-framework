#pragma once
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math//Mat44.hpp"
#include "Engine/Math/EulerAngles.hpp"
#include "Engine/Math/AABB2.hpp"

//-----------------------------------------------------------------------------------------------
class Camera
{
public:
	// Construction/Destruction
	~Camera();											// destructor (do nothing)
	Camera();											// default constructor (do nothing)
	void Translate2D(const Vec2& translation2D);
	void SetOrthoView(Vec2 const& bottomLeft, Vec2 const& topRight);
	Vec2 GetOrthoBottomLeft() const;
	Vec2 GetOrthoTopRight() const;
	Vec3 const GetCameraViewPosition() const;
	Vec3 const GetCameraForward() const;
	Mat44 const GetCameraBasis() const;

	void SetProjectionMatrix(Mat44 const& proj);
	void SetTransform(Vec3 position, EulerAngles eulerAngles);
	void SetOrthographicMatrix();
	void DefineTheGameSpace(Vec3 const& right, Vec3 const& up, Vec3 const& away);

	Mat44 GetGameToRendererMatrix() const;
	Mat44 GetProjectionMatrix() const; 
	Mat44 GetViewMatrix() const;

	void SetNormalizedViewport(AABB2 const& viewport = AABB2::ZERO_TO_ONE);
	AABB2 GetNormalizedViewport() const;

	AABB2 GetViewport(Vec2 const& outputSize) const;

	Vec3 GetViewPosition() const;
	EulerAngles GetViewOrientation() const;
	
private:
	Vec2	m_bottomLeft;
	Vec2	m_topRight;
	float	m_screenShakeAmount = 0.f;
	Mat44	m_projectionMatrix;

	Vec3	m_viewPosition;
	EulerAngles	m_viewEulerAngle;
	Mat44	m_gameToRendererMatrix;

	AABB2 m_normalizedViewport = AABB2::ZERO_TO_ONE;
};


