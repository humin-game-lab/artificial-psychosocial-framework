#pragma once
#include "Engine/Math/IntVec2.hpp"
#include <string>

//------------------------------------------------------------------------------------------------
class Image;
class Renderer;

struct ID3D11Texture2D;
struct ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;
struct ID3D11DepthStencilView;


//------------------------------------------------------------------------------------------------
class Texture
{
	friend class Renderer; // Only the Renderer can create new Texture objects!

private:
	Texture(); // can't instantiate directly; must ask Renderer to do it for you
	Texture(Texture const& copy) = delete; // No copying allowed!  This represents GPU memory.
	~Texture();

	bool LoadFromFile(Renderer* source, char const* imageFilePath);						 // creates texture from file, will call the below `CreateFromImage`
	bool CreateFromImage(Renderer* source, Image const& image); // creates a texture from an image file.

	ID3D11RenderTargetView* GetOrCreateRenderTargetView(Renderer* renderer);		// optional: can move RTV creation here if we implement `WatchInternal`, standardizes texture usage
	ID3D11ShaderResourceView* GetOrCreateShaderResourceView(Renderer* renderer);	// create an SRV for our texture so we can bind it as a shader input
	bool CreateDepthStencilTarget(Renderer* source, Image const& image);
	ID3D11DepthStencilView* GetOrCreateDepthStencilView(Renderer* renderer);
public:
	void				ReleaseResources(); // releases held views and texture handles
	void				WatchInternal(Renderer* renderer); // optional: lets us make a texture for my backbuffer instead of handling that handle directly (standardizes our texture usage)

	IntVec2				GetDimensions() const { return m_dimensions; }
	std::string const& GetImageFilePath() const { return m_imageFilePath; }

	float				GetAspect() const { return (float)m_dimensions.x / (float)m_dimensions.y; }

public:
	std::string			m_imageFilePath;
	IntVec2				m_dimensions;

	ID3D11Texture2D* m_handle = nullptr;

	ID3D11RenderTargetView* m_renderTargetView = nullptr;		// A01 - specifying that the texture wants to be rendered to in a specific format
	ID3D11ShaderResourceView* m_shaderResourceView = nullptr;		// A04 - this texture can be read from in a specific format
	ID3D11DepthStencilView* m_depthStencilView = nullptr;
};
