#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/Renderer/ConstantBuffer.hpp"
#include "Engine/Renderer/DefaultShaderSource.hpp"
#include "Engine/Core/VertexBuffer.hpp"
#include "Engine/Core/FileUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/Window.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Core/Image.hpp"
#include "Engine/Math/Vec4.hpp"

#include "Game/EngineBuildPreferences.hpp"

#define WIN32_LEAN_AND_MEAN		// Always #define this before #including <windows.h>
#include <windows.h>			// #include this (massive, platform-specific) header in very few places
#include <math.h>

#include "Engine/Renderer/D3DInternal.hpp"
#include <d3dcompiler.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

#if defined(ENGINE_DEBUG_RENDERER)
#include <dxgidebug.h>
#endif
#pragma comment( lib, "dxguid.lib" )

#include "ThirdParty/stb/stb_image.h"
#define DX_SAFE_RELEASE(obj) if(obj!=nullptr) {(obj)->Release(); (obj) = nullptr;}

//-----------------------------------------------------------------------------------------------
Renderer::Renderer(RendererConfig const& renderConfig):
	m_config(renderConfig){
}


//-----------------------------------------------------------------------------------------------
Renderer::~Renderer() {

}

//-----------------------------------------------------------------------------------------------
ID3D11BlendState* CreateBlendStates(ID3D11Device* d3dDevice, D3D11_BLEND srcFactor,
	D3D11_BLEND dstFactor, D3D11_BLEND_OP op) {
	D3D11_BLEND_DESC desc = {};
	desc.AlphaToCoverageEnable = false;
	desc.IndependentBlendEnable = false;

	desc.RenderTarget[0].BlendEnable = true;
	desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	// color blend state    
	desc.RenderTarget[0].SrcBlend = srcFactor;
	desc.RenderTarget[0].DestBlend = dstFactor;
	desc.RenderTarget[0].BlendOp = op;

	// alpha blend state
	desc.RenderTarget[0].SrcBlendAlpha = srcFactor;
	desc.RenderTarget[0].DestBlendAlpha =dstFactor;
	desc.RenderTarget[0].BlendOpAlpha = op;

	ID3D11BlendState* blendState = nullptr;
	d3dDevice->CreateBlendState(&desc, &blendState);
	return blendState;
}


//-----------------------------------------------------------------------------------------------
void Renderer::DrawVertexArray(int numVertexes, Vertex_PCU const* vertexes) {
	if (m_currentShader == nullptr) {
		m_currentShader = m_defaultShader;
		BindShader(m_currentShader);
	}
	m_immediateVBO->CopyVertexData(vertexes, numVertexes * sizeof(Vertex_PCU));
	DrawVertexBuffer(m_immediateVBO, numVertexes);
}


//-----------------------------------------------------------------------------------------------
void Renderer::Startup() {
	StartupDebugAPI();

	CreateRC();
	AquireBackBufferRTV();
	m_cameraCBO = CreateConstantBuffer(sizeof(CameraConstants));
	m_modelCBO = CreateConstantBuffer(sizeof(ModelConstants));
	m_immediateVBO = CreateDynamicVertexBuffer(32);

	ShaderConfig config; 
	config.m_name = "Default"; 
	config.m_source = g_defaultShaderSource;
	m_defaultShader = new Shader(this);
	m_defaultShader->m_config = config;
	ComileShader(m_defaultShader, g_defaultShaderSource, m_defaultShader->m_config.m_vertexEntryPoint);
	ComileShader(m_defaultShader, g_defaultShaderSource, m_defaultShader->m_config.m_pixelEntryPoint);
	m_loadedShaders.push_back(m_defaultShader);


	m_blendStateByMode[static_cast<int>(BlendMode::OPAQUE_)] = 
		CreateBlendStates(m_device, D3D11_BLEND_ONE, D3D11_BLEND_ZERO, D3D11_BLEND_OP_ADD);
	m_blendStateByMode[static_cast<int>(BlendMode::ALPHA)] =
		CreateBlendStates(m_device, D3D11_BLEND_SRC_ALPHA, D3D11_BLEND_INV_SRC_ALPHA, D3D11_BLEND_OP_ADD);
	m_blendStateByMode[static_cast<int>(BlendMode::ADDITIVE)] =
		CreateBlendStates(m_device, D3D11_BLEND_ONE, D3D11_BLEND_ONE, D3D11_BLEND_OP_ADD);
	SetBlendMode(BlendMode::ALPHA);

	CreateSamplerStates();
	m_defaultDiffuse = RegisterColorTexture( Rgba8::WHITE);

	m_defaultDepthStencil = CreateDepthStencilTexture(m_config.m_windowContext->GetDimensions());

	DebugRenderConfig debugRenderConfig;
	debugRenderConfig.m_renderer = this;
	DebugRenderSystemStartup(debugRenderConfig);

	g_theEventSystem->SubscribeEventCallbackFunction("DebugRenderSetTimeScale", DebugRenderSetTimeScale);
	g_theEventSystem->SubscribeEventCallbackFunction("DebugRenderToggle", DebugRenderToggle);
	g_theEventSystem->SubscribeEventCallbackFunction("DebugRenderClear", DebugRenderClears);
}


//-----------------------------------------------------------------------------------------------
void Renderer::Shutdown() {
	DestroyRenderContext();
	DestroyConstantBuffer(m_cameraCBO);
	DestroyConstantBuffer(m_modelCBO);
	DestroyVertexBuffer(m_immediateVBO);
	DestroySamplerStates();

	ReportLiveObjects();
	ShutdownDebugAPI();

	DebugRenderSystemShutdown();
}


//-----------------------------------------------------------------------------------------------
void Renderer::DestroyRenderContext()
{
	DX_SAFE_RELEASE(m_device);
	DX_SAFE_RELEASE(m_context);
	DX_SAFE_RELEASE(m_swapChain);
	DX_SAFE_RELEASE(m_currentRasterState);
	DX_SAFE_RELEASE(m_depthStencilState);

	for (int blendIndex = 0; blendIndex < 3; blendIndex++) {
		ID3D11BlendState* blendState = m_blendStateByMode[blendIndex];
		DX_SAFE_RELEASE(blendState);
	}

	for (int shaderIndex = 0; shaderIndex < static_cast<int>(m_loadedShaders.size()); shaderIndex++) {
		Shader* shader = m_loadedShaders[shaderIndex];
		DX_SAFE_RELEASE(shader->m_pixelStage);
		DX_SAFE_RELEASE(shader->m_vertexStage);
		DX_SAFE_RELEASE(shader->m_inputLayoutFor_Vertex_PCU);
		delete shader;
		shader = nullptr;
	}
	m_loadedShaders.clear();

	for (int textureIndex = 0; textureIndex < static_cast<int>(m_loadedTexture.size()); textureIndex++) {
		Texture* texture = m_loadedTexture[textureIndex];
		texture->ReleaseResources();
		delete texture;
		texture = nullptr;
	}
	m_loadedTexture.clear();

	for (int fontIndex = 0; fontIndex < static_cast<int>(m_loadedFonts.size()); fontIndex++) {
		BitmapFont* font = m_loadedFonts[fontIndex];
		delete font;
		font = nullptr;
	}
	m_loadedFonts.clear();

	m_backBufferRTV->ReleaseResources();
	delete m_backBufferRTV;
	m_backBufferRTV = nullptr;
}

//-----------------------------------------------------------------------------------------------
void Renderer::ClearScreen(Rgba8 const& clearColor) {
	float clearColorAsFloats[4];
	clearColor.GetAsFloats(clearColorAsFloats);

	m_context->ClearRenderTargetView(m_backBufferRTV->m_renderTargetView, clearColorAsFloats);
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::CreateOrGetTextureFromFile(const char* imageFilePath){
	// if the texture is already loaded, return the loaded texture
	std::string imageFilePathString(imageFilePath);
	for (int textureIndex = 0; textureIndex < static_cast<int> (m_loadedTexture.size()); 
		textureIndex++) {
		if (imageFilePathString == (m_loadedTexture[textureIndex]->m_imageFilePath)) {
			return m_loadedTexture[textureIndex];
		}
	}

	// if texture is not loaded, load texture
	Texture* texture = new Texture();
	texture->LoadFromFile(this, imageFilePath);
	return RegisterTexture(texture);
}


//-----------------------------------------------------------------------------------------------
void Renderer::BindTexture(Texture* texture, int slot){
	Texture* bindedTexture = texture;
	if (!texture) {
		bindedTexture = m_defaultDiffuse;
	}
	
	ID3D11ShaderResourceView* srv = bindedTexture->GetOrCreateShaderResourceView(this);
	m_context->PSSetShaderResources(slot, 1, &srv);  // only setting to the pixelshader stage
}


//-----------------------------------------------------------------------------------------------
void Renderer::SetSamplerMode(SamplerMode mode, int slot){
	ID3D11SamplerState* state = m_samplerStates[(int)mode];
	m_context->PSSetSamplers(slot, 1, &state);
}


//-----------------------------------------------------------------------------------------------
BitmapFont* Renderer::CreateOrGetBitmapFont(const char* bitmapFontFilePathWithNoExtension){
	std::string bitmapFilePathString(bitmapFontFilePathWithNoExtension);
	for (int fontIndex = 0; fontIndex < static_cast<int> (m_loadedFonts.size());
		fontIndex++) {
		if (bitmapFilePathString == (m_loadedFonts[fontIndex]->m_fontFilePathNameWithNoExtension)) {
			return m_loadedFonts[fontIndex];
		}
	}

	// if texture is not loaded, load texture
	BitmapFont* newFont = CreateBitmapFontFromFile(bitmapFontFilePathWithNoExtension);
	m_loadedFonts.push_back(newFont);
	return newFont;
}


//-----------------------------------------------------------------------------------------------
void Renderer::SetBlendMode(BlendMode blendMode) {
	float const blendConstants[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	switch (blendMode) {
	case BlendMode::OPAQUE_:
		m_context->OMSetBlendState(m_blendStateByMode[0],
			blendConstants,
			0xffffffff);
		break;
	case BlendMode::ADDITIVE:
		m_context->OMSetBlendState(m_blendStateByMode[2],
			blendConstants,
			0xffffffff);
		break;
	case BlendMode::ALPHA:
		m_context->OMSetBlendState(m_blendStateByMode[1],
			blendConstants,
			0xffffffff);
		break;
	
	}

}


//-----------------------------------------------------------------------------------------------
void Renderer::SetCullMode(CullMode cullMode){
	m_desiredCullMode = cullMode;
}


//-----------------------------------------------------------------------------------------------
void Renderer::SetFillMode(FillMode m_fillMode){
	m_desiredFillMode = m_fillMode;
}


//-----------------------------------------------------------------------------------------------
void Renderer::SetModelMatrix(Mat44 const& model){
	if (m_modelConstant.ModelMatrix != model) {
		m_modelConstant.ModelMatrix = model;
		m_modelCBO->SetData(m_modelConstant);
		BindConstantBuffer(3, m_modelCBO);
	}
}


void Renderer::SetModelTint(Rgba8 const& m_isTint){
	Rgba8 constantTint = Rgba8::GetColorFromFloats(m_modelConstant.m_isTint);
	if (constantTint != m_isTint) {
		m_isTint.GetAsFloats(m_modelConstant.m_isTint);
		m_modelCBO->SetData(m_modelConstant);
		BindConstantBuffer(3, m_modelCBO);
	}
}

//-----------------------------------------------------------------------------------------------
void Renderer::SetWindingOrder(WindingOrder frontFaceWindingOrder){
	m_currentWindingOrder = frontFaceWindingOrder;
}

//-----------------------------------------------------------------------------------------------
BitmapFont* Renderer::CreateBitmapFontFromFile(char const* fontFilePathNameWithNoExtension){
	std::string imagePath = fontFilePathNameWithNoExtension;
	imagePath += ".png";
	Texture* fontTexture = CreateOrGetTextureFromFile(imagePath.c_str());
	BitmapFont* bitMapFont = new BitmapFont(fontFilePathNameWithNoExtension, *fontTexture);
	return bitMapFont;
}


//-----------------------------------------------------------------------------------------------
void Renderer::BeginFrame() {
	DebugRenderBeginFrame();
}


//-----------------------------------------------------------------------------------------------
void Renderer::EndFrame() {
	//Sleep(0);

	m_swapChain->Present(0, 0);
	DebugRenderEndFrame();
}


//-----------------------------------------------------------------------------------------------
void Renderer::BeginCamera(const Camera& camera) {
	m_currentCamera = &camera;
	Vec2 bottomLeft = camera.GetOrthoBottomLeft();
	Vec2 topRight = camera.GetOrthoTopRight();
	IntVec2 clientSizeInt = m_config.m_windowContext->GetDimensions();
	Vec2 clientSize = Vec2(static_cast<float>(clientSizeInt.x), static_cast<float>(clientSizeInt.y));
	AABB2 vpBounds = camera.GetViewport(clientSize);
	Vec2 boundDimension = vpBounds.GetDimensions();
	float width = boundDimension.x;
	float height = abs(boundDimension.y);

	m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	ID3D11RenderTargetView* rtv = m_backBufferRTV->GetOrCreateRenderTargetView(this);
	D3D11_VIEWPORT viewport = {};
	viewport.TopLeftX = vpBounds.m_mins.x;
	viewport.TopLeftY = vpBounds.m_mins.y;
	viewport.Width = width;
	viewport.Height = height;
	viewport.MinDepth = 0;
	viewport.MaxDepth = 1;
	m_context->RSSetViewports(1, &viewport);


	CameraConstants cameraData;
	cameraData.ViewMatrix = camera.GetViewMatrix();
	//cameraData.ProjectionMatrix = camera.GetProjectionMatrix();
	cameraData.ProjectionMatrix = camera.GetProjectionMatrix();
	m_cameraCBO->SetData(cameraData);
	BindConstantBuffer(2, m_cameraCBO);

	Mat44 identityMatrix;
	SetModelMatrix(identityMatrix);
	SetModelTint(Rgba8::WHITE);

	ID3D11DepthStencilView* dsv = m_defaultDepthStencil->GetOrCreateDepthStencilView(this);
	m_context->OMSetRenderTargets(1, &rtv, dsv);
	SetDepthOptions(DepthTest::ALWAYS, false);

	BindShader(nullptr);
	BindTexture(m_defaultDiffuse);
	SetSamplerMode(SamplerMode::POINT_CLAMP);
	SetCullMode(CullMode::NONE);
	SetFillMode(FillMode::SOLID);
	SetWindingOrder(WindingOrder::CounterClockwise);
}


//-----------------------------------------------------------------------------------------------
void Renderer::EndCamera(const Camera& camera) {
	UNUSED(camera);

}


//-----------------------------------------------------------------------------------------------
void Renderer::DrawVertexBuffer(VertexBuffer const* vbo, int vertexCount){
	UpdateDepthStencilState();
	ID3D11Buffer* vboHandler = vbo->GetHandle();
	UINT stride = (UINT)vbo->GetStride();
	UINT offset = 0;

	m_context->IASetVertexBuffers(0,
		1,
		&vboHandler,
		&stride,
		&offset);

	ID3D11InputLayout* inputLayout = m_currentShader->CreateOrGetInputLayoutFor_Vertex_PCU();
	m_context->IASetInputLayout(inputLayout);

	if (IsRasterStateDirty()) {
		m_currentCullMode = m_desiredCullMode;
		m_currentFillMode = m_desiredFillMode;
		DX_SAFE_RELEASE(m_currentRasterState);
		m_currentRasterState = CreateRasterState(m_device, m_currentCullMode,
			m_currentFillMode,m_currentWindingOrder);
		m_context->RSSetState(m_currentRasterState);
	}
	m_context->Draw(vertexCount, 0);
}


//-----------------------------------------------------------------------------------------------
VertexBuffer* Renderer::CreateDynamicVertexBuffer(size_t const initialByteSize){
	VertexBuffer* newBuffer = new VertexBuffer(this, initialByteSize);
	return newBuffer;
}


//-----------------------------------------------------------------------------------------------
void Renderer::DestroyVertexBuffer(VertexBuffer* vbo){
	DX_SAFE_RELEASE(vbo->m_gpuBuffer);
	delete vbo;
	vbo = nullptr;
}


//-----------------------------------------------------------------------------------------------
void Renderer::BindShader(Shader const* shader)
{
	if (shader == nullptr) {
		shader = m_defaultShader;
		m_currentShader = m_defaultShader;
	}
	m_context->VSSetShader(shader->m_vertexStage, nullptr, 0);
	m_context->PSSetShader(shader->m_pixelStage, nullptr, 0);
}


//-----------------------------------------------------------------------------------------------
void Renderer::BindShaderByName(char const* shaderName)
{
	Shader* shader = CreateOrGetShader(shaderName);
	m_currentShader = shader;
	BindShader(shader);
}


//-----------------------------------------------------------------------------------------------
Shader* Renderer::CreateOrGetShader(char const* fileNameWithoutExtension)
{
	std::string shaderPath = fileNameWithoutExtension;
	

	int numShaders = static_cast<int>(m_loadedShaders.size());
	for (int shaderIndex = 0; shaderIndex < numShaders; shaderIndex++) {
		if (m_loadedShaders[shaderIndex]->GetName() == shaderPath) {
			return m_loadedShaders[shaderIndex];
		}
	}

	ShaderConfig shaderConfig;
	shaderConfig.m_name = shaderPath;
	Shader* shader = new Shader(this);
	shader->m_config = shaderConfig;
	m_loadedShaders.push_back(shader);

	std::string shaderPathWithExtension = shader->GetName() + ".hlsl";
	LoadAndCompileShader(shader, shaderPathWithExtension);

	return shader;
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::CreateDepthStencilTexture(IntVec2 size){
	Texture* depthStencilTexture = new Texture();
	D3D11_TEXTURE2D_DESC desc = {};

	desc.Width = size.x;
	desc.Height = size.y;
	desc.MipLevels = 1; // mip mapping   
	desc.ArraySize = 1; // number of textures in the texture array    
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; // contents of my image class, SRGB    
	desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	desc.CPUAccessFlags = 0; //     
	desc.MiscFlags = 0; // cube map     
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	m_device->CreateTexture2D(&desc, nullptr, &depthStencilTexture->m_handle);
	if (!depthStencilTexture->m_handle) {
		ERROR_AND_DIE("Cannot create depth buffer handler");
	}

	m_loadedTexture.push_back(depthStencilTexture);
	return depthStencilTexture;
}

//-----------------------------------------------------------------------------------------------
void Renderer::SetDepthOptions(DepthTest test, bool writeDepth){
	m_depthTest = test;
	m_writeDepth = writeDepth;
}


//-----------------------------------------------------------------------------------------------
void Renderer::ClearDepth(float depthValue){
	ID3D11DepthStencilView* dsv = m_defaultDepthStencil->GetOrCreateDepthStencilView(this);
	m_context->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, depthValue, 
		(UINT8)m_depthTest);
}


//-----------------------------------------------------------------------------------------------
ConstantBuffer* Renderer::CreateConstantBuffer(size_t const size){
	ConstantBuffer* newBuffer = new ConstantBuffer(this, size);
	return newBuffer;
}

//-----------------------------------------------------------------------------------------------
void Renderer::DestroyConstantBuffer(ConstantBuffer* cbo){
	DX_SAFE_RELEASE(cbo->m_gpuBuffer);
	delete cbo;
	cbo = nullptr;
}

//-----------------------------------------------------------------------------------------------
void Renderer::BindConstantBuffer(int slot, ConstantBuffer* constantBuffer){
	ID3D11Buffer*& cboHandle = constantBuffer->m_gpuBuffer;

	m_context->VSSetConstantBuffers(slot, 1, &cboHandle);
	m_context->PSSetConstantBuffers(slot, 1, &cboHandle);

}


//-----------------------------------------------------------------------------------------------
ID3D11RasterizerState* Renderer::CreateRasterState(ID3D11Device* d3dDevice, CullMode cullMode, 
	FillMode m_fillMode, WindingOrder winding){
	D3D11_CULL_MODE cullModeD3D11 = static_cast<D3D11_CULL_MODE>(cullMode);

	D3D11_RASTERIZER_DESC rasterDesc = {};     
	rasterDesc.CullMode = cullModeD3D11;
	if (m_fillMode == FillMode::SOLID) {
		rasterDesc.FillMode = D3D11_FILL_SOLID;
	}
	else if(m_fillMode == FillMode::WIREFRAME){
		rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	}
	rasterDesc.FrontCounterClockwise = static_cast<bool>(winding);

	rasterDesc.DepthBias = 0;     
	rasterDesc.DepthBiasClamp = 0.0f;    
	rasterDesc.SlopeScaledDepthBias = 0.0f;    
	rasterDesc.DepthClipEnable = true; // discard pixels outside of a 0 to 1 range;     
	rasterDesc.ScissorEnable = false;  // use scissor regions (one second)    
	rasterDesc.MultisampleEnable = false;         
	rasterDesc.AntialiasedLineEnable = true; 

	ID3D11RasterizerState* rasterState = nullptr;  
	d3dDevice->CreateRasterizerState(&rasterDesc, &rasterState);

	return rasterState;
}


//-----------------------------------------------------------------------------------------------
bool Renderer::DebugRenderClears(EventArgs& args){
	UNUSED(args);
	DebugRenderClear();
	return true;
}


//-----------------------------------------------------------------------------------------------
bool Renderer::DebugRenderToggle(EventArgs& args){
	UNUSED(args);
	static bool s_isHidden = false;
	s_isHidden = !s_isHidden;
	if (s_isHidden) {
		DebugRenderSetHidden();
	}
	else {
		DebugRenderSetVisible();
	}
	return true;
}


//-----------------------------------------------------------------------------------------------
bool Renderer::DebugRenderSetTimeScale(EventArgs& args){
	std::string scaleString = args.GetValue("Scale", "");
	double scale = 1.0;
	try
	{
		scale = stod(scaleString);
	}
	catch (...)
	{
		g_theConsole->AddLine(DevConsole::ERRORS, "Invalid time scale number");
	}
	if (g_theConsole->GetRenderer()) {
		g_theConsole->GetRenderer()->m_debugClock.SetTimeDilation(scale);

		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
void Renderer::CreateRC(){
	HRESULT hr;

	DWORD flags = 0;
	flags = D3D11_CREATE_DEVICE_SINGLETHREADED;
#if defined(_DEBUG)
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	IntVec2 dimension = m_config.m_windowContext->GetDimensions();

	DXGI_SWAP_CHAIN_DESC swapChainDesc = {0};
	swapChainDesc.BufferDesc.Width = dimension.x;
	swapChainDesc.BufferDesc.Height = dimension.y;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.OutputWindow = (HWND)(m_config.m_windowContext->GetHwnd());
	swapChainDesc.Windowed = true;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.Flags = 0;



	hr = ::D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		flags,
		nullptr,
		0,
		D3D11_SDK_VERSION,
		&swapChainDesc,
		&m_swapChain,
		&m_device,
		nullptr,
		&m_context);

	ASSERT_OR_DIE(SUCCEEDED(hr), "Failed to create D3D11 device");
}


//-----------------------------------------------------------------------------------------------
void Renderer::AquireBackBufferRTV(){
	m_backBufferRTV = new Texture();
	m_backBufferRTV->WatchInternal(this);
}


void Renderer::ComileShader(Shader* shader, char const* source, std::string const& entryPoint){
	std::string modelTarget;
	if (entryPoint == shader->m_config.m_vertexEntryPoint) {
		modelTarget = "vs_5_0";
	}
	else if (entryPoint == shader->m_config.m_pixelEntryPoint) {
		modelTarget = "ps_5_0";
	}

	DWORD compileFlags = 0;
#if defined(ENGINE_DEBUG_RENDERER)
	compileFlags |= D3DCOMPILE_WARNINGS_ARE_ERRORS;
#else
	compileFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

	ID3DBlob* byteCode = nullptr;
	ID3DBlob* errorBuffer = nullptr;
	HRESULT result = D3DCompile(
		source,
		strlen(source),
		"Default",
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		entryPoint.c_str(),
		modelTarget.c_str(),
		compileFlags,
		0,
		&byteCode,
		&errorBuffer
	);

	if (SUCCEEDED(result) && (byteCode != nullptr)) {
		if (entryPoint == shader->m_config.m_vertexEntryPoint) {
			m_device->CreateVertexShader(
				byteCode->GetBufferPointer(),
				byteCode->GetBufferSize(),
				nullptr,
				&shader->m_vertexStage
			);
		}
		else if (entryPoint == shader->m_config.m_pixelEntryPoint) {
			m_device->CreatePixelShader(
				byteCode->GetBufferPointer(),
				byteCode->GetBufferSize(),
				nullptr,
				&shader->m_pixelStage
			);
		}

	}
	else {
		char const* errorString = "";
		if (errorBuffer != nullptr) {
			errorString = (char const*)errorBuffer->GetBufferPointer();
		}
		DebuggerPrintf(errorString);
		ERROR_AND_DIE(errorString);
	}
	if (entryPoint == shader->m_config.m_vertexEntryPoint) {
		shader->m_vertexByteCode.resize(byteCode->GetBufferSize());
		memcpy(shader->m_vertexByteCode.data(), byteCode->GetBufferPointer(), byteCode->GetBufferSize());
	}

	DX_SAFE_RELEASE(byteCode);
	DX_SAFE_RELEASE(errorBuffer);
}

//-----------------------------------------------------------------------------------------------
void Renderer::LoadAndCompileShader(Shader* shader, std::string& filename){
	std::string source;
	FileReadToString(source, filename);
	if (source.size() == 0) {
		ERROR_AND_DIE("Shader File is empty!");
	}
	ComileShader(shader, source.c_str(), shader->m_config.m_vertexEntryPoint);
	ComileShader(shader, source.c_str(), shader->m_config.m_pixelEntryPoint);
}


//-----------------------------------------------------------------------------------------------
void Renderer::StartupDebugAPI() {
#if defined(ENGINE_DEBUG_RENDERER)
	HMODULE modHandle = ::LoadLibraryA("Dxgidebug.dll");
	if (nullptr == modHandle) {
		return;
	}
	typedef HRESULT(WINAPI* GetDebugModuleFunc)(REFIID, void**);

	GetDebugModuleFunc getModuleFunc =
		(GetDebugModuleFunc) ::GetProcAddress(modHandle, "DXGIGetDebugInterface");
	ASSERT_OR_DIE(getModuleFunc != nullptr, "Failed to load get debug interface.");
	HRESULT result = getModuleFunc(__uuidof(IDXGIDebug), (void**)&m_debug);
	ASSERT_OR_DIE(SUCCEEDED(result), "...");

	m_debugModule = modHandle;
	
#endif

}



//-----------------------------------------------------------------------------------------------
void Renderer::ShutdownDebugAPI(){
#if defined(ENGINE_DEBUG_RENDERER)
	if (m_debugModule) {
		DX_SAFE_RELEASE(m_debug);
		::FreeLibrary((HMODULE)m_debugModule);
		m_debugModule = nullptr;
	}

#endif
}


//-----------------------------------------------------------------------------------------------
void Renderer::ReportLiveObjects(){
#if defined(ENGINE_DEBUG_RENDERER)
	if (nullptr != m_debug) {
		DXGI_DEBUG_RLO_FLAGS const flags = 
			(DXGI_DEBUG_RLO_FLAGS)(DXGI_DEBUG_RLO_DETAIL | DXGI_DEBUG_RLO_IGNORE_INTERNAL);
		m_debug->ReportLiveObjects(DXGI_DEBUG_ALL, flags);
	}
#endif
}


//-----------------------------------------------------------------------------------------------
bool Renderer::IsRasterStateDirty(){
	return (m_currentRasterState == nullptr) || (m_desiredCullMode != m_currentCullMode
		|| (m_desiredFillMode != m_currentFillMode));
}


//-----------------------------------------------------------------------------------------------
void Renderer::CreateSamplerStates() {
	for (int samplerIndex = 0; samplerIndex < static_cast<int>(SamplerMode::COUNT); samplerIndex++) {
		SamplerMode samplerMode = SamplerMode(samplerIndex);
		D3D11_FILTER filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		D3D11_TEXTURE_ADDRESS_MODE mode = D3D11_TEXTURE_ADDRESS_CLAMP;

		switch (samplerMode) {
			case SamplerMode::BILINEAR_CLAMP:
				filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
				mode = D3D11_TEXTURE_ADDRESS_CLAMP;
				break;
			case SamplerMode::BILINEAR_WRAP:
				filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
				mode = D3D11_TEXTURE_ADDRESS_WRAP;
				break;
			case SamplerMode::POINT_CLAMP:
				filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
				mode = D3D11_TEXTURE_ADDRESS_CLAMP;
			case SamplerMode::POINT_WRAP:
				filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
				mode = D3D11_TEXTURE_ADDRESS_WRAP;
		}

		D3D11_SAMPLER_DESC desc = {};
		desc.Filter = filter;
		desc.AddressU = mode;
		desc.AddressV = mode;
		desc.AddressW = mode;

		desc.MipLODBias = 0.0f;
		desc.MinLOD = 0;
		desc.MaxLOD = 0;
		desc.MaxAnisotropy = 0;
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;

		ID3D11SamplerState* state = nullptr;
		m_device->CreateSamplerState(&desc, &state);
		ASSERT_OR_DIE(state != nullptr, "Failed to create sampler state.");

		m_samplerStates[samplerIndex] = state;
	}

}


//-----------------------------------------------------------------------------------------------
void Renderer::DestroySamplerStates(){
	for (int samplerIndex = static_cast<int>(SamplerMode::COUNT) -1; samplerIndex >=0 ; 
		samplerIndex--) {
		DX_SAFE_RELEASE(m_samplerStates[samplerIndex]);
	}
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::RegisterColorTexture( Rgba8 const& color){
	Image image;
	image.SetupAsSolidColor(1, 1, color);

	Texture* texture = new Texture();
	texture->CreateFromImage(this, image);

	RegisterTexture(texture);

	return texture;
}


//-----------------------------------------------------------------------------------------------
Texture* Renderer::RegisterTexture( Texture* texture){

	if (texture->m_handle) {
		m_loadedTexture.push_back(texture);
	}
	else {
		delete texture;
		texture = nullptr;
	}

	return texture;
}


//-----------------------------------------------------------------------------------------------
void Renderer::UpdateDepthStencilState(){
	D3D11_DEPTH_STENCIL_DESC desc = {};     
	desc.DepthEnable = TRUE; // disabled if test:ALWAYS and write:false    
	desc.DepthWriteMask = m_writeDepth ? D3D11_DEPTH_WRITE_MASK_ALL  : D3D11_DEPTH_WRITE_MASK_ZERO; 

	switch (m_depthTest)
	{
	case DepthTest::ALWAYS:
		desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
		break;
	case DepthTest::NEVER:
		desc.DepthFunc = D3D11_COMPARISON_NEVER;
		break;
	case DepthTest::EQUAL:
		desc.DepthFunc = D3D11_COMPARISON_EQUAL;
		break;
	case DepthTest::NOT_EQUAL:
		desc.DepthFunc = D3D11_COMPARISON_NOT_EQUAL;
		break;
	case DepthTest::LESS:
		desc.DepthFunc = D3D11_COMPARISON_LESS;
		break;
	case DepthTest::LESS_EQUAL:
		desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
		break;
	case DepthTest::GREATER:
		desc.DepthFunc = D3D11_COMPARISON_GREATER;
		break;
	case DepthTest::GREATER_EQUAL:
		desc.DepthFunc = D3D11_COMPARISON_GREATER_EQUAL;
		break;
	default:
		break;
	}

	desc.StencilEnable = false; 
	DX_SAFE_RELEASE(m_depthStencilState);     
	m_device->CreateDepthStencilState(&desc, &m_depthStencilState);     
	m_context->OMSetDepthStencilState(m_depthStencilState, 0);
}

