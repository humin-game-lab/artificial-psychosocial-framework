#include "Engine/Math/Vec4.hpp"
#include "Engine/Math/Vec3.hpp"

//-----------------------------------------------------------------------------------------------
Vec4::~Vec4(){

}


//-----------------------------------------------------------------------------------------------
Vec4::Vec4(){

}


//-----------------------------------------------------------------------------------------------
Vec4::Vec4(const Vec4& copyFrom){
	x = copyFrom.x; 
	y = copyFrom.y;
	w = copyFrom.w;
	z = copyFrom.z;
}


//-----------------------------------------------------------------------------------------------
Vec4::Vec4(float initialX, float initialY, float initialZ, float initialW){
	x = initialX;
	y = initialY;
	w = initialW;
	z = initialZ;
}


//-----------------------------------------------------------------------------------------------
Vec4::Vec4(Vec3 vector3D, float initialW){
	x = vector3D.x;
	y = vector3D.y;
	z = vector3D.z;
	w = initialW;
}

//-----------------------------------------------------------------------------------------------
const Vec4 Vec4::operator-(const Vec4& vecToSubtract) const{
	Vec4 temp;
	temp.x = x - vecToSubtract.x;
	temp.y = y - vecToSubtract.y;
	temp.z = z - vecToSubtract.z;
	temp.w = w - vecToSubtract.w;
	return temp;
}


//-----------------------------------------------------------------------------------------------
void Vec4::operator*=(const float uniformScale){
	x *= uniformScale;
	y *= uniformScale;
	z *= uniformScale;
	w *= uniformScale;
}
