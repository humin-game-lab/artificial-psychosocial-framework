#pragma once
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"

// forward type declarations
struct Vec4;
struct Vertex_PCU;
struct AABB2;
struct AABB3;
struct IntVec2;
struct Capsule2;
struct OBB2;
struct LineSegment2;
struct FloatRange;

struct RaycastResult2D
{
	Vec2 m_start;
	Vec2 m_direction;
	float m_distance;
	bool m_didImpact = false;
	Vec2 m_impactPosition;
	int m_impactEntityIndex = -1;
	float m_impactFraction;
	float m_impactDistance = 9999.f;
	Vec2 m_impactSurfaceNormal;
};

struct RaycastResult3D
{
	Vec3 m_start;
	Vec3 m_direction;
	float m_distance;
	bool m_didImpact = false;
	Vec3 m_impactPosition;
	int m_impactEntityIndex = -1;
	float m_impactFraction;
	float m_impactDistance;
	Vec3 m_impactSurfaceNormal;
};


// angle utilities
float ConvertDegreesToRadians(float degrees);
float ConvertRadiansToDegrees(float radians);
float CosDegrees(float degrees);
float SinDegrees(float degrees);
float Atan2Degrees(float y, float x);
float TanDegrees(float degrees);
float ArcCosDegrees(float cosValue);

// basic 2d & 3d utilities
float GetDistance2D(Vec2 const& positionA, Vec2 const& positionB);
float GetDistanceSquared2D(Vec2 const& positionA, Vec2 const& positionB);
float GetDistance3D(Vec3 const& positionA, Vec3 const& positionB);
float GetDistanceSquared3D(Vec3 const& positionA, Vec3 const& positionB);
float GetDistanceXY3D(Vec3 const& positionA, Vec3 const& positionB);
float GetDistanceXYSquared3D(Vec3 const& positionA, Vec3 const& positionB);

// Geometric query utilities
bool DoDiscsOverlap(Vec2 const& centerA, float radiusA, Vec2 const& centerB, float radiusB);
bool DoSpheresOverlap(Vec3 const& centerA, float radiusA, Vec3 const& centerB, float radiusB);
bool DoSphereOverlapAABB3(Vec3 const& centerA, float radiusA, AABB3 const& box);
bool DoSphereOverlapCylinderZ(Vec3 const& centerA, float radiusA, Vec3 const& cylinderPos, FloatRange cylinderZ, float cylinderRadius);
bool DoAABB3OverlapAABB3(AABB3 const& boxA, AABB3 const& boxB);
bool DoAABB3OverlapCylinderZ(AABB3 const& box, Vec3 const& cylinderPos, FloatRange cylinderZ, float cylinderRadius);
bool DoCylinderZOverlapCylinderZ(Vec3 const& cylinderAPos, FloatRange cylinderAZ, float cylinderARadius, Vec3 const& cylinderBPos, FloatRange cylinderBZ, float cylinderBRadius);
bool IsPointInsideDisc2D(Vec2 const& point, Vec2 const& discCenter, float discRadius);
bool IsPointInsideSphere(Vec3 const& point, Vec3 const& sphereCenter, float sphereRadius);
bool IsPointInsideAABB2D(Vec2 const& point, AABB2 const& box);
bool IsPointInsideAABB3D(Vec3 const& point, AABB3 const& box);
bool IsPointInsideCapsule2D(Vec2 const& point, Capsule2 const& capsule);
bool IsPointInsideCapsule2D(Vec2 const& point, Vec2 const& boneStart, Vec2 const& boneEnd, float radius);
bool IsPointInsideOBB2D(Vec2 const& point, OBB2 const& orientedBox);
bool IsPointInsideOrientedSector2D(Vec2 const& point, Vec2 const& sectorTip,
	float sectorForwardDegrees, float sectorApertureDegrees, float sectorRadius);
bool IsPointInsideDirectedSector2D(Vec2 const& point, Vec2 const& sectorTip,
	Vec2 const& sectorForwardNormal, float sectorApertureDegrees, float sectorRadius);
Vec2 const GetNearestPointOnDisc2D(Vec2 const& referencePos, Vec2 const& discCenter, 
	float discRadius);
Vec2 const GetNearestPointOnAABB2D(Vec2 const& referencePos, AABB2& box);
Vec2 const GetNearestPointOnInfiniteLine2D(Vec2 const& referencePos, LineSegment2 const& infiniteLine);
Vec2 const GetNearestPointOnInfiniteLine2D(Vec2 const& referencePos, Vec2 const& pointOnLine, 
	Vec2 const& anotherPointOnLine);
Vec2 const GetNearestPointOnLineSegment2D(Vec2 const& referencePos, LineSegment2 const& lineSegment);
Vec2 const GetNearestPointOnLineSegment2D(Vec2 const& referencePos, Vec2 const& lineSegStart, 
	Vec2 const& lineSegEnd);
Vec2 const GetNearestPointOnCapsule2D(Vec2 const& referencePos, Capsule2 const& capsule);
Vec2 const GetNearestPointOnCapsule2D(Vec2 const& referencePos, Vec2 const& boneStart, Vec2 const& boneEnd,
	float radius);
Vec2 const GetNearestPointOnOBB2D(Vec2 const& referencePos, OBB2 const& orientedBox);
Vec3 const GetNearestPointOnSphere(Vec3 const& referencePos, Vec3 const& sphereCenter, float radius);
Vec3 const GetNearestPointOnAABB3(Vec3 const& referencePos, Vec3 const& aabb3Mins, Vec3 const& aabb3Maxs);
Vec3 const GetNearestPointOnCylinder(Vec3 const& referencePos, Vec2 const& cylinderCenter, float cylinderRadius, FloatRange zRange);

bool PushDiscOutOfPoint2D(Vec2& mobileDiscCenter, float discRadius, Vec2 const& fixedPoint);
bool PushDiscOutOfDisc2D(Vec2& mobileDiscCenter, float mobileRadius, Vec2 const& fixedDiscCenter,
	float fixedRadius, float pushFraction = 1.f);
bool PushDiscsOutOfEachOther2D(Vec2& aCenter, float aRadius, Vec2& bCenter, float bRadius, float pushFraction = 1.f);
bool PushDiscOutOfAABB2D(Vec2& mobileDiscCenter, float discRadius, AABB2 const& fixedBox);
Vec3 const GetPointOnSphere(float yaw, float pitch);
Vec3 const GetRandomPointInCircle3D(Vec3 const& center, float radius, Vec3 const& ibasis, Vec3 const& jbasis);


// Transform utilities
RaycastResult2D RaycastDiscs(Vec2 const& start, Vec2 const& forwardNormal, float distance, Vec2 discCenter, float discRadius);
RaycastResult2D RaycastAABB2D(Vec2 const& start, Vec2 const& forwardNormal, float distance, AABB2 const& box);
RaycastResult2D RaycastLineSegment2D(Vec2 const& start, Vec2 const& forwardNormal, float distance, LineSegment2 const& lineSeg);

RaycastResult3D RaycastSphere(Vec3 const& start, Vec3 const& forwardNormal, float distance, Vec3 sphereCenter, float radius) ;
RaycastResult3D RaycastAABB3D(Vec3 const& start, Vec3 const& forwardNormal, float distance, AABB3 const& box);
RaycastResult3D RaycastCylinder(Vec3 const& start, Vec3 const& forwardNormal, float distance, Vec3 cylinderPos, FloatRange cylinderRangeZ, float radius);


// elasticity
void ReflectFixedObject2D(Vec2& velocity, Vec2 const& surfaceNormal, float eA, float eB);
void BounceDiscOffDisc2D(Vec2& aCenter, float aRadius, Vec2& velocityA, Vec2& bCenter, float bRadius, Vec2& velocityB, float eA, float eB);
void ChangeMomentumDiscs2D(Vec2& velocityA, Vec2& velocityB, Vec2 const& surface, float eA, float eB);
void BounceDiscOffPoint2D(Vec2& discCenter, float bounceDistance, Vec2& velocity, Vec2 const& bouncePosition, float eA, float eB);

// Transform utilities
void TransformPosition2D(Vec2& posToTransform, float uniformScaleXY,
	float rotationDegrees, Vec2 const& translation);
void TransformPosition2D(Vec2& posToTransform, Vec2 const& iBasis, Vec2 const& jBasis,
	Vec2 const& translation);
void TransformPositionXY3D(Vec3& posToTransform, float uniformScaleXY,
	float rotationDegrees, Vec2 const& translationXY);
void TransformPositionXY3D(Vec3& posToTransform, Vec2 const& iBasis, Vec2 const& jBasis,
	Vec2 const& translationXY);
void TransformVertexArrayXY3D(int numVerts, Vertex_PCU* verts, float uniformScaleXY,
	float rotationDegreesAboutZ, Vec2 const& translationXY);

// Rangemap functions
float Interpolate(float start, float end, float fractionValue);
float GetFractionWithin(float value, float start, float end);
float RangeMap(float value, float start, float end, float outputStart, float outputEnd);
float RangeMapClamped(float value, float start, float end, float outputStart, float outputEnd);
float Clamp(float value, float start, float end);
double Clamp(double value, double start, double end);
int Clamp(int value, int start, int end);
float ClampZeroToOne(float value);
int RoundDownToInt(float value);
float SmoothStep3(float inputZeroToOne);

// Angular displacement
float GetAngularDispDegrees(float startDegree, float endDegree);
float GetShortestAngularDispDegrees(float startDegree, float endDegree);
float GetTurnedTowardDegrees(float startDegree, float endDegree, float maxDeltaDegrees);

// Dot product
int GetTaxicabDistance2D(IntVec2 const& pointA, IntVec2 const& pointB);
float DotProduct2D(Vec2 const& pointA, Vec2 const& pointB);
float DotProduct3D(Vec3 const& pointA, Vec3 const& pointB);
float DotProduct4D(Vec4 const& a, Vec4 const& b);
float CrossProduct2D(Vec2 const& pointA, Vec2 const& pointB);
Vec3 CrossProduct3D(Vec3 const& pointA, Vec3 const& pointB);
float GetProjectedLength2D(Vec2 const& vectorToProject, Vec2 const& vectorToProjectOnto);
Vec3 const GetProjectedOnto3D(Vec3 const& vectorToProject, Vec3 const& vectorToProjectOnto);
Vec2 const GetProjectedOnto2D(Vec2 const& vectorToProject, Vec2 const& vectorToProjectOnto);
float GetAngleDegreesBetweenVectors2D(Vec2 const& a, Vec2 const& b);
float GetAngleDegreesBetweenVectors3D(Vec3 const& a, Vec3 const& b);

// Normalization
float NormalizeByte(unsigned char byteValue);
unsigned char DenormalizeByte(float zeroToOne);


int Log2Int(int number);