#include "RandomNumberGenerator.hpp"
#include <stdlib.h>

RandomNumberGenerator* g_rng = nullptr;

//-----------------------------------------------------------------------------------------------
// return random integer between [0,max number)
int RandomNumberGenerator::GetRandomIntLessThan(int maxNotInclusive) {
	return (rand() % maxNotInclusive);
}


//-----------------------------------------------------------------------------------------------
// return random int between [min, max]
int RandomNumberGenerator::GetRandomIntInRange(int minInclusive, int maxInclusive) {
	return (rand() % ((maxInclusive+1)-minInclusive) + minInclusive);
}


//-----------------------------------------------------------------------------------------------
// return random float between [0,1]
float RandomNumberGenerator::GetRandomFloatZeroToOne() { 
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}


//-----------------------------------------------------------------------------------------------
// return random float between [min, max]
float RandomNumberGenerator::GetRandomFloatInRange(float minInclusive, float maxInclusive) {
	return (minInclusive + static_cast <float> (rand())
		/ (static_cast <float> (RAND_MAX / (maxInclusive - minInclusive))));
}

//-----------------------------------------------------------------------------------------------
// return random float between (min, max)
float RandomNumberGenerator::GetRandomFloatInRangeNotMaxValue(float minExclusive, float maxExclusive){
	float returnValue = minExclusive;
	while(returnValue <= minExclusive || returnValue >= maxExclusive){
		returnValue = (minExclusive + static_cast <float> (rand())
			/ (static_cast <float> (RAND_MAX / (maxExclusive - minExclusive))));
	}
	return returnValue;
}
