#pragma once
#include "Engine/Math/Vec2.hpp"

//-----------------------------------------------------------------------------------------------
struct Capsule2
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec2 m_start;
	Vec2 m_end;
	float m_radius = 0.f;

public:
	Capsule2();
	explicit Capsule2(Vec2 const& start, Vec2 const& end, float radius);

	void Translate(Vec2 const& translation);
	void SetCenter(Vec2 const& newCenter);
	void RotateAboutCenter(float rotationDeltaDegrees);
};


