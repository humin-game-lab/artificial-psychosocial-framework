#include "Engine/Math/FloatRange.hpp"

const FloatRange FloatRange::ZERO(0.f, 0.f);
const FloatRange FloatRange::ONE(1.f, 1.f);
const FloatRange FloatRange::ZERO_TO_ONE(0.f, 1.f);

//-----------------------------------------------------------------------------------------------
FloatRange::FloatRange(){
	m_min = 0.f;
	m_max = 0.f;
}


//-----------------------------------------------------------------------------------------------
FloatRange::FloatRange(float min, float max){
	m_min = min;
	m_max = max;
}


//-----------------------------------------------------------------------------------------------
bool FloatRange::IsOnRange(float value) const{
	return (value >= m_min && value <= m_max);
}


//-----------------------------------------------------------------------------------------------
bool FloatRange::IsOverlappingWith(FloatRange const& floatRange) const{
	return (m_max >= floatRange.m_min || m_min <= floatRange.m_max);
}


//-----------------------------------------------------------------------------------------------
FloatRange  FloatRange::GetOverlappingPart(FloatRange const& floatRange) const{
	FloatRange returnedRange = ZERO;
	if(floatRange.m_min <= floatRange.m_max){
		if (floatRange.m_min <= m_min && floatRange.m_max >= m_min) {
			returnedRange.m_min = m_min;
		}
		else if (floatRange.m_min >= m_min && floatRange.m_min <= m_max) {
			returnedRange.m_min = floatRange.m_min;
		}
		if (floatRange.m_max >= m_max && floatRange.m_min <= m_max) {
			returnedRange.m_max = m_max;
		}
		else if (floatRange.m_max <= m_max && floatRange.m_max >= m_min) {
			returnedRange.m_max = floatRange.m_max;
		}
	}
	else{
		if (floatRange.m_min >= m_max && floatRange.m_max <= m_max) {
			returnedRange.m_min = m_max;
		}
		else if (floatRange.m_min >= m_min && floatRange.m_min <= m_max) {
			returnedRange.m_min = floatRange.m_min;
		}
		if (floatRange.m_max <= m_min && floatRange.m_min >= m_min) {
			returnedRange.m_max = m_min;
		}
		else if (floatRange.m_max <= m_max && floatRange.m_max >= m_min) {
			returnedRange.m_max = floatRange.m_max;
		}
	}
	return returnedRange;
}


//-----------------------------------------------------------------------------------------------
bool FloatRange::operator!=(FloatRange const& compare) const{
	return (m_min != compare.m_min || m_max != compare.m_max);
}


//-----------------------------------------------------------------------------------------------
bool FloatRange::operator==(FloatRange const& compare) const{
	return (m_min == compare.m_min && m_max == compare.m_max);
}


//-----------------------------------------------------------------------------------------------
void FloatRange::operator=(FloatRange const& copyFrom){
	m_min = copyFrom.m_min;
	m_max = copyFrom.m_max;
}
