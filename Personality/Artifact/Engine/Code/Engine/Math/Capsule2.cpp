#include "Engine/Math/Capsule2.hpp"

//-----------------------------------------------------------------------------------------------
Capsule2::Capsule2() {

}


//-----------------------------------------------------------------------------------------------
Capsule2::Capsule2(Vec2 const& start, Vec2 const& end, float radius) {
	m_start = start;
	m_end = end;
	m_radius = radius;
}


//-----------------------------------------------------------------------------------------------
void Capsule2::Translate(Vec2 const& translation) {
	m_start += translation;
	m_end += translation;
}


//-----------------------------------------------------------------------------------------------
void Capsule2::SetCenter(Vec2 const& newCenter) {
	Vec2 currentCenter = (m_start + m_end) * 0.5f;
	Vec2 centerToStart = m_start - currentCenter;
	Vec2 centerToEnd = m_end - currentCenter;

	m_start = newCenter + centerToStart;
	m_end = newCenter + centerToEnd;
}


//-----------------------------------------------------------------------------------------------
void Capsule2::RotateAboutCenter(float rotationDeltaDegrees) {
	Vec2 currentCenter = (m_start + m_end) * 0.5f;
	Vec2 centerToStart = m_start - currentCenter;
	Vec2 centerToEnd = m_end - currentCenter;

	centerToStart = Vec2::MakeFromPolarDegrees(centerToStart.GetOrientationDegrees() + rotationDeltaDegrees,
		centerToStart.GetLength());
	centerToEnd = Vec2::MakeFromPolarDegrees(centerToEnd.GetOrientationDegrees() + rotationDeltaDegrees,
		centerToEnd.GetLength());
	m_start = currentCenter + centerToStart;
	m_end = currentCenter + centerToEnd;
}
