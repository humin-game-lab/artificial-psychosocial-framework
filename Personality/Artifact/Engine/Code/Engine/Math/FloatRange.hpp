#pragma once

//-----------------------------------------------------------------------------------------------
struct FloatRange
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	float m_min;
	float m_max;
	static const FloatRange ZERO;
	static const FloatRange ONE;
	static const FloatRange ZERO_TO_ONE;
public:
	FloatRange();
	explicit FloatRange(float min, float max);
	bool IsOnRange(float value) const;
	bool IsOverlappingWith(FloatRange const& floatRange) const; 
	FloatRange GetOverlappingPart(FloatRange const& floatRange) const;

	void operator=(FloatRange const& copyFrom);
	bool operator==(FloatRange const& compare) const;
	bool operator!=(FloatRange const& compare) const;
};


