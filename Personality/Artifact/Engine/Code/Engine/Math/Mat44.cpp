#include "Engine/Math/Mat44.hpp"
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/Vec4.hpp"
#include "Engine/Math/MathUtils.hpp"
#include <math.h>


//-----------------------------------------------------------------------------------------------
Mat44::Mat44(){
	m_values[Ix] = 1.f;
	m_values[Jy] = 1.f;
	m_values[Kz] = 1.f;
	m_values[Tw] = 1.f;
}


//-----------------------------------------------------------------------------------------------
Mat44::Mat44(Vec2 const& iBasis2D, Vec2 const& jBasis2D, Vec2 const& translation2D){
	m_values[Ix] = iBasis2D.x;
	m_values[Iy] = iBasis2D.y;
	m_values[Jx] = jBasis2D.x;
	m_values[Jy] = jBasis2D.y;
	m_values[Tx] = translation2D.x;
	m_values[Ty] = translation2D.y;
	m_values[Kz] = 1.f;
	m_values[Tw] = 1.f;
}


//-----------------------------------------------------------------------------------------------
Mat44::Mat44(Vec3 const& iBasis3D, Vec3 const& jBasis3D, Vec3 const& kBasis3D, Vec3 const& translation3D){
	m_values[Ix] = iBasis3D.x;
	m_values[Iy] = iBasis3D.y;
	m_values[Iz] = iBasis3D.z;
	m_values[Jx] = jBasis3D.x;
	m_values[Jy] = jBasis3D.y;
	m_values[Jz] = jBasis3D.z;
	m_values[Kx] = kBasis3D.x;
	m_values[Ky] = kBasis3D.y;
	m_values[Kz] = kBasis3D.z;
	m_values[Tx] = translation3D.x;
	m_values[Ty] = translation3D.y;
	m_values[Tz] = translation3D.z;
	m_values[Tw] = 1.f;
}


//-----------------------------------------------------------------------------------------------
Mat44::Mat44(Vec4 const& iBasis4D, Vec4 const& jBasis4D, Vec4 const& kBasis4D, Vec4 const& translation4D){
	m_values[Ix] = iBasis4D.x;
	m_values[Iy] = iBasis4D.y;
	m_values[Iz] = iBasis4D.z;
	m_values[Iw] = iBasis4D.w;
	m_values[Jx] = jBasis4D.x;
	m_values[Jy] = jBasis4D.y;
	m_values[Jz] = jBasis4D.z;
	m_values[Jw] = jBasis4D.w;
	m_values[Kx] = kBasis4D.x;
	m_values[Ky] = kBasis4D.y;
	m_values[Kz] = kBasis4D.z;
	m_values[Kw] = kBasis4D.w;
	m_values[Tx] = translation4D.x;
	m_values[Ty] = translation4D.y;
	m_values[Tz] = translation4D.z;
	m_values[Tw] = translation4D.w;
}


//-----------------------------------------------------------------------------------------------
Mat44::Mat44(float const* sixteenValuesBasisMajor){
	for (int matrixIndex = 0; matrixIndex < 16; matrixIndex++) {
		m_values[matrixIndex] = sixteenValuesBasisMajor[matrixIndex];
	}
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateTranslation2D(Vec2 const& translationXY){
	Mat44 translationMatrix;
	translationMatrix.m_values[Tx] = translationXY.x;
	translationMatrix.m_values[Ty] = translationXY.y;
	return translationMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateTranslation3D(Vec3 const& translationXYZ){
	Mat44 translationMatrix;
	translationMatrix.m_values[Tx] = translationXYZ.x;
	translationMatrix.m_values[Ty] = translationXYZ.y;
	translationMatrix.m_values[Tz] = translationXYZ.z;
	return translationMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateUniformScale2D(float uniformScaleXY){
	Mat44 uniformScaleMatrix;
	uniformScaleMatrix.m_values[Ix] = uniformScaleXY;
	uniformScaleMatrix.m_values[Jy] = uniformScaleXY;
	return uniformScaleMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateUniformScale3D(float uniformScaleXYZ){
	Mat44 uniformScaleMatrix;
	uniformScaleMatrix.m_values[Ix] = uniformScaleXYZ;
	uniformScaleMatrix.m_values[Jy] = uniformScaleXYZ;
	uniformScaleMatrix.m_values[Kz] = uniformScaleXYZ;
	return uniformScaleMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateNonUniformScale2D(Vec2 nonUniformScaleXY){
	Mat44 nonUniformScaleMatrix;
	nonUniformScaleMatrix.m_values[Ix] = nonUniformScaleXY.x;
	nonUniformScaleMatrix.m_values[Jy] = nonUniformScaleXY.y;
	return nonUniformScaleMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateNonUniformScale3D(Vec3 nonUniformScaleXYZ){
	Mat44 nonUniformScaleMatrix;
	nonUniformScaleMatrix.m_values[Ix] = nonUniformScaleXYZ.x;
	nonUniformScaleMatrix.m_values[Jy] = nonUniformScaleXYZ.y;
	nonUniformScaleMatrix.m_values[Kz] = nonUniformScaleXYZ.z;
	return nonUniformScaleMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateZRotationDegrees(float rotationDegreesAboutZ){
	Mat44 rotationZMatrix;
	rotationZMatrix.m_values[Ix] = CosDegrees(rotationDegreesAboutZ);
	rotationZMatrix.m_values[Iy] = SinDegrees(rotationDegreesAboutZ);
	rotationZMatrix.m_values[Jx] = -SinDegrees(rotationDegreesAboutZ);
	rotationZMatrix.m_values[Jy] = CosDegrees(rotationDegreesAboutZ);
	return rotationZMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateYRotationDegrees(float rotationDegreesAboutY){
	Mat44 rotationYMatrix;
	rotationYMatrix.m_values[Kx] = SinDegrees(rotationDegreesAboutY);
	rotationYMatrix.m_values[Kz] = CosDegrees(rotationDegreesAboutY);
	rotationYMatrix.m_values[Ix] = CosDegrees(rotationDegreesAboutY);
	rotationYMatrix.m_values[Iz] = -SinDegrees(rotationDegreesAboutY);
	return rotationYMatrix;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateXRotationDegrees(float rotationDegreesAboutX){
	Mat44 rotationXMatrix;
	rotationXMatrix.m_values[Jy] = CosDegrees(rotationDegreesAboutX);
	rotationXMatrix.m_values[Jz] = SinDegrees(rotationDegreesAboutX);
	rotationXMatrix.m_values[Ky] = -SinDegrees(rotationDegreesAboutX);
	rotationXMatrix.m_values[Kz] = CosDegrees(rotationDegreesAboutX);
	return rotationXMatrix;
}



//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreatePerspectiveProjection(float fovYDegrees, float aspect, float zNear, float zFar){
	Mat44 perspective;
	float thetaRadians = 0.5f * ConvertDegreesToRadians(fovYDegrees);
	float scaleY = cosf(thetaRadians) / sinf(thetaRadians);
	float scaleX = scaleY / aspect;
	float scaleZ = zFar / (zFar - zNear);
	float translationZ = (zNear * zFar) / (zNear - zFar);

	perspective.m_values[Ix] = scaleX;
	perspective.m_values[Jy] = scaleY;
	perspective.m_values[Kz] = scaleZ;
	perspective.m_values[Tz] = translationZ;
	perspective.m_values[Tw] = 0.f;
	perspective.m_values[Kw] = 1.f;

	return perspective;
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::CreateOrthoProjection(float left, float right, float bottom, float top, float zNear, float zFar){
	Vec4 iBasis(2.f / (right - left), 0.f, 0.f, 0.f);
	Vec4 jBasis(0.f, 2.f / (top - bottom), 0.f, 0.f);
	Vec4 kBasis(0.f, 0.f, 1.f/ (zFar - zNear), 0.f);
	Vec4 translation((right + left) / (left - right),
		(top + bottom) / (bottom - top), zNear/(zNear - zFar), 1.f);
	Mat44 orthoProjection(iBasis, jBasis, kBasis, translation);
	return orthoProjection;
}


//-----------------------------------------------------------------------------------------------
Vec2 const Mat44::TransformVectorQuantity2D(Vec2 const& vectorQuantityXY) const{
	Mat44 copyOfThis = *this;
	float const* matrixValues = &copyOfThis.m_values[0];
	Vec2 quantity2D;
	quantity2D.x = matrixValues[Ix] * vectorQuantityXY.x + matrixValues[Jx] * vectorQuantityXY.y;
	quantity2D.y = matrixValues[Iy] * vectorQuantityXY.x + matrixValues[Jy] * vectorQuantityXY.y;
	return quantity2D;
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::TransformVectorQuantity3D(Vec3 const& vectorQuantityXYZ) const{
	Mat44 copyOfThis = *this;
	float const* matrixValues = &copyOfThis.m_values[0];
	Vec3 quantity3D;
	quantity3D.x = matrixValues[Ix] * vectorQuantityXYZ.x + matrixValues[Jx] * vectorQuantityXYZ.y + matrixValues[Kx] * vectorQuantityXYZ.z;
	quantity3D.y = matrixValues[Iy] * vectorQuantityXYZ.x + matrixValues[Jy] * vectorQuantityXYZ.y + matrixValues[Ky] * vectorQuantityXYZ.z;
	quantity3D.z = matrixValues[Iz] * vectorQuantityXYZ.x + matrixValues[Jz] * vectorQuantityXYZ.y + matrixValues[Kz] * vectorQuantityXYZ.z;
	return quantity3D;
}


//-----------------------------------------------------------------------------------------------
Vec2 const Mat44::TransformPosition2D(Vec2 const& positionXY) const{
	Mat44 copyOfThis = *this;
	float const* matrixValues = &copyOfThis.m_values[0];
	Vec2 transform2D;
	transform2D.x = matrixValues[Ix] * positionXY.x + matrixValues[Jx] * positionXY.y + matrixValues[Tx];
	transform2D.y = matrixValues[Iy] * positionXY.x + matrixValues[Jy] * positionXY.y + matrixValues[Ty];
	return transform2D;
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::TransformPosition3D(Vec3 const& positionXYZ) const{
	Mat44 copyOfThis = *this;
	float const* matrixValues = &copyOfThis.m_values[0];
	Vec3 transform3D;
	transform3D.x = matrixValues[Ix] * positionXYZ.x + matrixValues[Jx] * positionXYZ.y + matrixValues[Kx] * positionXYZ.z + matrixValues[Tx];
	transform3D.y = matrixValues[Iy] * positionXYZ.x + matrixValues[Jy] * positionXYZ.y + matrixValues[Ky] * positionXYZ.z + matrixValues[Ty];
	transform3D.z = matrixValues[Iz] * positionXYZ.x + matrixValues[Jz] * positionXYZ.y + matrixValues[Kz] * positionXYZ.z + matrixValues[Tz];
	return transform3D;
}


//-----------------------------------------------------------------------------------------------
Vec4 const Mat44::TransformHomogeneous3D(Vec4 const& homogeneousPoint3D) const{
	Mat44 copyOfThis = *this;
	float const* matrixValues = &copyOfThis.m_values[0];
	Vec4 homogeneous3D;
	homogeneous3D.x = matrixValues[Ix] * homogeneousPoint3D.x + matrixValues[Jx] * homogeneousPoint3D.y +
		matrixValues[Kx] * homogeneousPoint3D.z + matrixValues[Tx] * homogeneousPoint3D.w;
	homogeneous3D.y = matrixValues[Iy] * homogeneousPoint3D.x + matrixValues[Jy] * homogeneousPoint3D.y +
		matrixValues[Ky] * homogeneousPoint3D.z + matrixValues[Ty] * homogeneousPoint3D.w;
	homogeneous3D.z = matrixValues[Iz] * homogeneousPoint3D.x + matrixValues[Jz] * homogeneousPoint3D.y +
		matrixValues[Kz] * homogeneousPoint3D.z + matrixValues[Tz] * homogeneousPoint3D.w;
	homogeneous3D.w = matrixValues[Iw] * homogeneousPoint3D.x + matrixValues[Jw] * homogeneousPoint3D.y +
		matrixValues[Kw] * homogeneousPoint3D.z + matrixValues[Tw] * homogeneousPoint3D.w;
	return homogeneous3D;
}


//-----------------------------------------------------------------------------------------------
float* Mat44::GetAsFloatArray(){
	return m_values;
}


//-----------------------------------------------------------------------------------------------
float const* Mat44::GetAsFloatArray() const{
	return m_values;
}


//-----------------------------------------------------------------------------------------------
Vec2 const Mat44::GetIBasis2D() const{
	return Vec2(m_values[Ix], m_values[Iy]);
}


//-----------------------------------------------------------------------------------------------
Vec2 const Mat44::GetJBasis2D() const{
	return Vec2(m_values[Jx], m_values[Jy]);
}


//-----------------------------------------------------------------------------------------------
Vec2 const Mat44::GetTranslation2D() const{
	return Vec2(m_values[Tx], m_values[Ty]);
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::GetIBasis3D() const{
	return Vec3(m_values[Ix], m_values[Iy], m_values[Iz]);
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::GetJBasis3D() const{
	return Vec3(m_values[Jx], m_values[Jy], m_values[Jz]);
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::GetKBasis3D() const{
	return Vec3(m_values[Kx], m_values[Ky], m_values[Kz]);
}


//-----------------------------------------------------------------------------------------------
Vec3 const Mat44::GetTranslation3D() const{
	return Vec3(m_values[Tx], m_values[Ty], m_values[Tz]);
}


//-----------------------------------------------------------------------------------------------
Vec4 const Mat44::GetIBasis4D() const{
	return Vec4(m_values[Ix], m_values[Iy], m_values[Iz], m_values[Iw]);
}


//-----------------------------------------------------------------------------------------------
Vec4 const Mat44::GetJBasis4D() const{
	return Vec4(m_values[Jx], m_values[Jy], m_values[Jz], m_values[Jw]);
}


//-----------------------------------------------------------------------------------------------
Vec4 const Mat44::GetKBasis4D() const{
	return Vec4(m_values[Kx], m_values[Ky], m_values[Kz], m_values[Kw]);
}


//-----------------------------------------------------------------------------------------------
Vec4 const Mat44::GetTranslation4D() const{
	return Vec4(m_values[Tx], m_values[Ty], m_values[Tz], m_values[Tw]);
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetTranslation2D(Vec2 const& translationXY){
	m_values[Tx] = translationXY.x;
	m_values[Ty] = translationXY.y;
	m_values[Tz] = 0.f;
	m_values[Tw] = 1.f;
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetTranslation3D(Vec3 const& trnaslationXYZ){
	m_values[Tx] = trnaslationXYZ.x;
	m_values[Ty] = trnaslationXYZ.y;
	m_values[Tz] = trnaslationXYZ.z;
	m_values[Tw] = 1.f;
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetIJ2D(Vec2 const& iBasis2D, Vec2 const& jBasis2D){
	m_values[Ix] = iBasis2D.x;
	m_values[Iy] = iBasis2D.y;
	m_values[Iz] = 0.f;
	m_values[Iw] = 0.f;
	m_values[Jx] = jBasis2D.x;
	m_values[Jy] = jBasis2D.y;
	m_values[Jz] = 0.f;
	m_values[Jw] = 0.f;
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetIJT2D(Vec2 const& iBasis2D, Vec2 const& jBasis2D, Vec2 const& translationXY){
	SetTranslation2D(translationXY);
	SetIJ2D(iBasis2D, jBasis2D);
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetIJK3D(Vec3 const& iBasis3D, Vec3 const& jBasis3D, Vec3 const& kBasis3D){
	m_values[Ix] = iBasis3D.x;
	m_values[Iy] = iBasis3D.y;
	m_values[Iz] = iBasis3D.z;
	m_values[Iw] = 0.f;
	m_values[Jx] = jBasis3D.x;
	m_values[Jy] = jBasis3D.y;
	m_values[Jz] = jBasis3D.z;
	m_values[Jw] = 0.f;
	m_values[Kx] = kBasis3D.x;
	m_values[Ky] = kBasis3D.y;
	m_values[Kz] = kBasis3D.z;
	m_values[Kw] = 0.f;
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetIJKT3D(Vec3 const& iBasis3D, Vec3 const& jBasis3D, Vec3 const& kBasis3D, Vec3 const& translationXYZ){
	SetIJK3D(iBasis3D, jBasis3D, kBasis3D);
	SetTranslation3D(translationXYZ);
}


//-----------------------------------------------------------------------------------------------
void Mat44::SetIJKT4D(Vec4 const& iBasis4D, Vec4 const& jBasis4D, Vec4 const& kBasis4D, Vec4 const& translation4D){
	m_values[Ix] = iBasis4D.x;
	m_values[Iy] = iBasis4D.y;
	m_values[Iz] = iBasis4D.z;
	m_values[Iw] = iBasis4D.w;
	m_values[Jx] = jBasis4D.x;
	m_values[Jy] = jBasis4D.y;
	m_values[Jz] = jBasis4D.z;
	m_values[Jw] = jBasis4D.w;
	m_values[Kx] = kBasis4D.x;
	m_values[Ky] = kBasis4D.y;
	m_values[Kz] = kBasis4D.z;
	m_values[Kw] = kBasis4D.w;
	m_values[Tx] = translation4D.x;
	m_values[Ty] = translation4D.y;
	m_values[Tz] = translation4D.z;
	m_values[Tw] = translation4D.w;
}


//-----------------------------------------------------------------------------------------------
void Mat44::Append(Mat44 const& appendThis){
	Mat44 copyOfThis = *this;
	float const* l = &copyOfThis.m_values[0];
	float const* r = &appendThis.m_values[0];

	m_values[Ix] = (l[Ix] * r[Ix]) + (l[Jx] * r[Iy]) + (l[Kx] * r[Iz]) + (l[Tx] * r[Iw]);
	m_values[Iy] = (l[Iy] * r[Ix]) + (l[Jy] * r[Iy]) + (l[Ky] * r[Iz]) + (l[Ty] * r[Iw]);
	m_values[Iz] = (l[Iz] * r[Ix]) + (l[Jz] * r[Iy]) + (l[Kz] * r[Iz]) + (l[Tz] * r[Iw]);
	m_values[Iw] = (l[Iw] * r[Ix]) + (l[Jw] * r[Iy]) + (l[Kw] * r[Iz]) + (l[Tw] * r[Iw]);
	m_values[Jx] = (l[Ix] * r[Jx]) + (l[Jx] * r[Jy]) + (l[Kx] * r[Jz]) + (l[Tx] * r[Jw]);
	m_values[Jy] = (l[Iy] * r[Jx]) + (l[Jy] * r[Jy]) + (l[Ky] * r[Jz]) + (l[Ty] * r[Jw]);
	m_values[Jz] = (l[Iz] * r[Jx]) + (l[Jz] * r[Jy]) + (l[Kz] * r[Jz]) + (l[Tz] * r[Jw]);
	m_values[Jw] = (l[Iw] * r[Jx]) + (l[Jw] * r[Jy]) + (l[Kw] * r[Jz]) + (l[Tw] * r[Jw]);
	m_values[Kx] = (l[Ix] * r[Kx]) + (l[Jx] * r[Ky]) + (l[Kx] * r[Kz]) + (l[Tx] * r[Kw]);
	m_values[Ky] = (l[Iy] * r[Kx]) + (l[Jy] * r[Ky]) + (l[Ky] * r[Kz]) + (l[Ty] * r[Kw]);
	m_values[Kz] = (l[Iz] * r[Kx]) + (l[Jz] * r[Ky]) + (l[Kz] * r[Kz]) + (l[Tz] * r[Kw]);
	m_values[Kw] = (l[Iw] * r[Kx]) + (l[Jw] * r[Ky]) + (l[Kw] * r[Kz]) + (l[Tw] * r[Kw]);
	m_values[Tx] = (l[Ix] * r[Tx]) + (l[Jx] * r[Ty]) + (l[Kx] * r[Tz]) + (l[Tx] * r[Tw]);
	m_values[Ty] = (l[Iy] * r[Tx]) + (l[Jy] * r[Ty]) + (l[Ky] * r[Tz]) + (l[Ty] * r[Tw]);
	m_values[Tz] = (l[Iz] * r[Tx]) + (l[Jz] * r[Ty]) + (l[Kz] * r[Tz]) + (l[Tz] * r[Tw]);
	m_values[Tw] = (l[Iw] * r[Tx]) + (l[Jw] * r[Ty]) + (l[Kw] * r[Tz]) + (l[Tw] * r[Tw]);

}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendZRotation(float degreesRotationAboutZ){
	Mat44 rotationZMatrix = CreateZRotationDegrees(degreesRotationAboutZ);
	Append(rotationZMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendYRotation(float degreesRotationAboutY){
	Mat44 rotationYMatrix = CreateYRotationDegrees(degreesRotationAboutY);
	Append(rotationYMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendXRotation(float degreesRotationAboutX){
	Mat44 rotationXMatrix = CreateXRotationDegrees(degreesRotationAboutX);
	Append(rotationXMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendTranslation2D(Vec2 const& translationXY){
	Mat44 translation2DMatrix = CreateTranslation2D(translationXY);
	Append(translation2DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendTranslation3D(Vec3 const& translationXYZ){
	Mat44 translation3DMatrix = CreateTranslation3D(translationXYZ);
	Append(translation3DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendScaleUniform2D(float uniformScaleXY){
	Mat44 uniformScale2DMatrix = CreateUniformScale2D(uniformScaleXY);
	Append(uniformScale2DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendScaleUniform3D(float uniformScaleXYZ){
	Mat44 uniformScale3DMatrix = CreateUniformScale3D(uniformScaleXYZ);
	Append(uniformScale3DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendScaleNonUniform2D(Vec2 const& nonUniformScaleXY){
	Mat44 nonUniScale2DMatrix = CreateNonUniformScale2D(nonUniformScaleXY);
	Append(nonUniScale2DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::AppendScaleNonUniform3D(Vec3 const& nonUniformScaleXYZ){
	Mat44 nonUniScale3DMatrix = CreateNonUniformScale3D(nonUniformScaleXYZ);
	Append(nonUniScale3DMatrix);
}


//-----------------------------------------------------------------------------------------------
void Mat44::Transpose(){
	Vec4 iBasis = GetIBasis4D();
	Vec4 jBasis = GetJBasis4D();
	Vec4 kBasis = GetKBasis4D();
	Vec4 translation = GetTranslation4D();
	m_values[Ix] = iBasis.x;
	m_values[Jx] = iBasis.y;
	m_values[Kx] = iBasis.z;
	m_values[Tx] = iBasis.w;
	m_values[Iy] = jBasis.x;
	m_values[Jy] = jBasis.y;
	m_values[Ky] = jBasis.z;
	m_values[Ty] = jBasis.w;
	m_values[Iz] = kBasis.x;
	m_values[Jz] = kBasis.y;
	m_values[Kz] = kBasis.z;
	m_values[Tz] = kBasis.w;
	m_values[Iw] = translation.x;
	m_values[Jw] = translation.y;
	m_values[Kw] = translation.z;
	m_values[Tw] = translation.w;
}


//-----------------------------------------------------------------------------------------------
void Mat44::Orthonormalize_XFwd_YLeft_ZUp(){
	Vec3 iBasis = GetIBasis3D();
	Vec3 kBasis = GetKBasis3D();

	if (iBasis == Vec3(0.f, 0.f, 1.f)) {
		kBasis = Vec3(1.f, 0.f, 0.f);
	}
	else if (iBasis == Vec3(0.f, 0.f, -1.f)) {
		kBasis = Vec3(-1.f, 0.f, 0.f);
	}
	Vec3 newJBasis = CrossProduct3D(kBasis, iBasis);
	Vec3 newKBasis = CrossProduct3D(iBasis, newJBasis);

	SetIJK3D(iBasis.GetNormalized(), newJBasis.GetNormalized(), newKBasis.GetNormalized());
}


//-----------------------------------------------------------------------------------------------
Mat44 const Mat44::GetOrthonormalInverse() const{
	Vec3 iBasis = GetIBasis3D();
	Vec3 jBasis = GetJBasis3D();
	Vec3 kBasis = GetKBasis3D();
	Vec3 translation = GetTranslation3D();

	Mat44 invertedMatrix(iBasis, jBasis, kBasis, translation);

	// transpose Rotation part
	invertedMatrix.m_values[Ix] = iBasis.x;
	invertedMatrix.m_values[Jx] = iBasis.y;
	invertedMatrix.m_values[Kx] = iBasis.z;
	invertedMatrix.m_values[Iy] = jBasis.x;
	invertedMatrix.m_values[Jy] = jBasis.y;
	invertedMatrix.m_values[Ky] = jBasis.z;
	invertedMatrix.m_values[Iz] = kBasis.x;
	invertedMatrix.m_values[Jz] = kBasis.y;
	invertedMatrix.m_values[Kz] = kBasis.z;

	// calculate tranlastion part which is -t * transposed rotation
	translation *= -1.f;
	invertedMatrix.m_values[Tx] = translation.x * invertedMatrix.m_values[Ix] +
		translation.y * invertedMatrix.m_values[Jx] + translation.z * invertedMatrix.m_values[Kx];
	invertedMatrix.m_values[Ty] = translation.x * invertedMatrix.m_values[Iy] +
		translation.y * invertedMatrix.m_values[Jy] + translation.z * invertedMatrix.m_values[Ky];
	invertedMatrix.m_values[Tz] = translation.x * invertedMatrix.m_values[Iz] +
		translation.y * invertedMatrix.m_values[Jz] + translation.z * invertedMatrix.m_values[Kz];

	return invertedMatrix;
}


//-----------------------------------------------------------------------------------------------
bool Mat44::operator!=(const Mat44& compare) const{
	for (int valueIndex = 0; valueIndex < 16; valueIndex++) {
		if (m_values[valueIndex] != compare.m_values[valueIndex]) {
			return true;
		}
	}
	return false;
}
