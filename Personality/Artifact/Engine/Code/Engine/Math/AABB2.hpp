#pragma once
#include "Engine/Math/Vec2.hpp"

//-----------------------------------------------------------------------------------------------
struct AABB2
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec2 m_mins;
	Vec2 m_maxs;

	static const AABB2 ZERO_TO_ONE;

public:
	~AABB2();													// destructor (do nothing)
	AABB2();													// default constructor (do nothing)
	AABB2(const AABB2& copyFrom);								// copy constructor (from another vec2)
	AABB2(Vec2 const& initialMins, Vec2 const& initialMaxs);	// constructor passes two vectors: min and max
	AABB2(float minX, float minY, float maxX, float maxY);		// constructor passes four values of four corners

	// Accessors (const methods)
	bool IsPointInside(Vec2 const& point) const;
	Vec2 const GetCenter() const;
	Vec2 const GetDimensions() const;
	Vec2 const GetNearestPoint(Vec2 const& point) const;
	Vec2 const GetPointAtUV(Vec2 const& uv) const;
	Vec2 const GetUVForPoint(Vec2 const& point) const;

	//Mutators (non-const methods)
	void Translate(Vec2 const& displacement);
	void SetCenter(Vec2 const& newCenter);
	void SetDimensions(Vec2 const& newDimension);
	void StretchToIncludePoint(Vec2 const& point);

};


