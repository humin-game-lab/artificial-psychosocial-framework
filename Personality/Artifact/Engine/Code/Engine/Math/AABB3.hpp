#pragma once
#include "Engine/Math/Vec3.hpp"

//-----------------------------------------------------------------------------------------------
struct AABB3
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec3 m_mins;
	Vec3 m_maxs;

public:
	~AABB3();													// destructor (do nothing)
	AABB3();													// default constructor (do nothing)
	AABB3(const AABB3& copyFrom);								// copy constructor (from another vec3)
	AABB3(Vec3 const& initialMins, Vec3 const& initialMaxs);	// constructor passes two vectors: min and max
	AABB3(float minX, float minY, float minZ, float maxX, float maxY, float maxZ);

	// Accessors (const methods)
	bool IsPointInside(Vec3 const& point) const;
	Vec3 const GetCenter() const;
	Vec3 const GetDimensions() const;
	Vec3 const GetNearestPoint(Vec3 const& point) const;

	//Mutators (non-const methods)
	void Translate(Vec3 const& displacement);
	void SetCenter(Vec3 const& newCenter);
	void SetDimensions(Vec3 const& newDimension);
	void StretchToIncludePoint(Vec3 const& point);

};


