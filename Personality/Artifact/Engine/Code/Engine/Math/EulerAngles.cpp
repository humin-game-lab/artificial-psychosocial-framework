#include "Engine/Math/EulerAngles.hpp"
#include "Engine//Math/Vec3.hpp"
#include "Engine/Math/Mat44.hpp"


//-----------------------------------------------------------------------------------------------
EulerAngles::EulerAngles(float yawDegrees, float pitchDegrees, float rollDegrees){
	m_yawDegrees = yawDegrees;
	m_pitchDegrees = pitchDegrees;
	m_rollDegrees = rollDegrees;
}


//-----------------------------------------------------------------------------------------------
EulerAngles::EulerAngles(Vec3 const anglesYallPitchRoll){
	m_yawDegrees = anglesYallPitchRoll.x;
	m_pitchDegrees = anglesYallPitchRoll.y;
	m_rollDegrees = anglesYallPitchRoll.z;
}


//-----------------------------------------------------------------------------------------------
void EulerAngles::GetVectors_XFwd_YLeft_ZUp(Vec3& out_forwardIBasis, Vec3& out_leftJBasis, Vec3& out_upKBasis) const{
	Mat44 ypr;
	ypr.AppendZRotation(m_yawDegrees);
	ypr.AppendYRotation(m_pitchDegrees);
	ypr.AppendXRotation(m_rollDegrees);

	out_forwardIBasis = ypr.GetIBasis3D();
	out_leftJBasis = ypr.GetJBasis3D();
	out_upKBasis = ypr.GetKBasis3D();
}


//-----------------------------------------------------------------------------------------------
Mat44 EulerAngles::GetMatrix__XFwd_YLeft_ZUp() const{
	Mat44 ypr;
	ypr.AppendZRotation(m_yawDegrees);
	ypr.AppendYRotation(m_pitchDegrees);
	ypr.AppendXRotation(m_rollDegrees);

	return ypr;
}


//-----------------------------------------------------------------------------------------------
Vec3 EulerAngles::GetAngles() const{
	return Vec3(m_yawDegrees, m_pitchDegrees, m_rollDegrees);
}


//-----------------------------------------------------------------------------------------------
Vec3 EulerAngles::GetAnglesPYR() const{
	return Vec3(m_rollDegrees, m_pitchDegrees, m_yawDegrees);
}
