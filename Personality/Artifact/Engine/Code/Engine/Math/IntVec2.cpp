#include "Engine/Math/IntVec2.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Core/StringUtils.hpp"
#include <math.h>

//-----------------------------------------------------------------------------------------------
IntVec2::~IntVec2() {

}


//-----------------------------------------------------------------------------------------------
IntVec2::IntVec2() {

}


//-----------------------------------------------------------------------------------------------
IntVec2::IntVec2(const IntVec2& copyFrom) {
	x = copyFrom.x;
	y = copyFrom.y;
}


//-----------------------------------------------------------------------------------------------
IntVec2::IntVec2(int initialX, int initialY) {
	x = initialX;
	y = initialY;
}


//-----------------------------------------------------------------------------------------------
float IntVec2::GetLength() const {
	double double_x = static_cast<double>(x);
	double double_y = static_cast<double>(y);
	return static_cast<float>(sqrt(double_x * double_x + double_y * double_y));
}


//-----------------------------------------------------------------------------------------------
int IntVec2::GetLengthSquared() const {
	return (x * x + y * y);
}


//-----------------------------------------------------------------------------------------------
int IntVec2::GetTaxicabLength() const {
	return abs(x) + abs(y);
}


//-----------------------------------------------------------------------------------------------
float IntVec2::GetOrientationRadians() const {
	return static_cast<float>(atan2(static_cast<float>(y), static_cast<float>(x)));
}


//-----------------------------------------------------------------------------------------------
float IntVec2::GetOrientationDegrees() const {
	return Atan2Degrees(static_cast<float>(y), static_cast<float>(x));
}


//-----------------------------------------------------------------------------------------------
IntVec2 const IntVec2::GetRotated90Degrees() const {
	return IntVec2(-y, x);
}


//-----------------------------------------------------------------------------------------------
IntVec2 const IntVec2::GetRotatedMinus90Degrees() const {
	return IntVec2(y, -x);
}


//-----------------------------------------------------------------------------------------------
void IntVec2::Rotate90Degrees() {
	int temp = x;
	x = -y;
	y = temp;
}


//-----------------------------------------------------------------------------------------------
void IntVec2::RotateMinus90Degrees() {
	int temp = x;
	x = y;
	y = -temp;
}


//-----------------------------------------------------------------------------------------------
void IntVec2::SetFromText(const char* text){
	Strings floats = SplitStringOnDelimiter(text, ',');
	x = static_cast<int>(atoi(floats[0].c_str()));
	y = static_cast<int>(atoi(floats[1].c_str()));
}


//-----------------------------------------------------------------------------------------------
void IntVec2::operator-=(IntVec2 const& intVecToSubtract){
	x -= intVecToSubtract.x;
	y -= intVecToSubtract.y;
}


//-----------------------------------------------------------------------------------------------
IntVec2 const IntVec2::operator-(IntVec2 const& intVecToSubtract){
	return IntVec2(x - intVecToSubtract.x, y - intVecToSubtract.y);
}


//-----------------------------------------------------------------------------------------------
void IntVec2::operator+=(IntVec2 const& intVecToAdd) {
	x += intVecToAdd.x;
	y += intVecToAdd.y;
}

//-----------------------------------------------------------------------------------------------
IntVec2 const IntVec2::operator+(IntVec2 const& intVecToAdd){
	return IntVec2(x + intVecToAdd.x, y + intVecToAdd.y);
}


//-----------------------------------------------------------------------------------------------
bool IntVec2::operator==(IntVec2 const& compare) const {
	if (x == compare.x && y == compare.y) {
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool IntVec2::operator!=(IntVec2 const& compare) const {
	if (x != compare.x || y != compare.y) {
		return true;
	}
	return false;
}