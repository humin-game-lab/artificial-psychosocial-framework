#pragma once

//-----------------------------------------------------------------------------------------------
struct IntVec2
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	int x = 0;
	int y = 0;

public:
	// Construction/Destruction
	~IntVec2();											// destructor (do nothing)
	IntVec2();											// default constructor (do nothing)
	IntVec2( const IntVec2& copyFrom );					// copy constructor (from another vec2)
	explicit IntVec2( int initialX, int initialY );		// explicit constructor (from x, y)

	// Accessors (const methods)
	float GetLength() const;
	int GetLengthSquared() const;
	int GetTaxicabLength() const;
	float GetOrientationRadians() const;
	float GetOrientationDegrees() const;
	IntVec2 const GetRotated90Degrees() const;
	IntVec2 const GetRotatedMinus90Degrees() const;

	//Mutators (non-const methods)
	void Rotate90Degrees();
	void RotateMinus90Degrees();
	void SetFromText(const char* text);

	void operator+=(IntVec2 const& intVecToAdd);
	IntVec2 const operator+(IntVec2 const& intVecToAdd);
	IntVec2 const operator-(IntVec2 const& intVecToSubtract);
	void operator-=(IntVec2 const& intVecToSubtract);
	bool operator==(IntVec2 const& compare) const;
	bool operator!=(IntVec2 const& compare) const;
};


