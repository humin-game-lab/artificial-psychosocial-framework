#pragma once

//-----------------------------------------------------------------------------------------------
class RandomNumberGenerator {
public:
	~RandomNumberGenerator() {};
	RandomNumberGenerator() {};
	int GetRandomIntLessThan(int maxNotInclusive);
	int GetRandomIntInRange(int minInclusive, int maxInclusive);
	float GetRandomFloatZeroToOne();
	float GetRandomFloatInRange(float minInclusive, float maxInclusive);
	float GetRandomFloatInRangeNotMaxValue(float minExclusive, float maxExclusive);
private:
	//unsigned int m_speed = 0;
	//int m_position = 0;
};

