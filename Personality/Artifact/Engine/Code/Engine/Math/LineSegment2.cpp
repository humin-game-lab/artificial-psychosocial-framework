#include "Engine/Math/LineSegment2.hpp"


//-----------------------------------------------------------------------------------------------
LineSegment2::LineSegment2(){

}


//-----------------------------------------------------------------------------------------------
LineSegment2::LineSegment2(Vec2 const& start, Vec2 const& end){
	m_start = start;
	m_end = end;
}


//-----------------------------------------------------------------------------------------------
void LineSegment2::Translate(Vec2 const& translation){
	m_start += translation;
	m_end += translation;
}


//-----------------------------------------------------------------------------------------------
void LineSegment2::SetCenter(Vec2 const& newCenter){
	Vec2 currentCenter = (m_start + m_end) * 0.5f;
	Vec2 centerToStart = m_start - currentCenter;
	Vec2 centerToEnd = m_end - currentCenter;

	m_start = newCenter + centerToStart;
	m_end = newCenter + centerToEnd;
}


//-----------------------------------------------------------------------------------------------
void LineSegment2::RotateAboutCenter(float rotationDeltaDegrees) {
	// calculate cs, ce
	Vec2 currentCenter = (m_start + m_end) * 0.5f;
	Vec2 centerToStart = m_start - currentCenter;
	Vec2 centerToEnd = m_end - currentCenter;

	// calculate orientation degrees of cs and ce, then add rotate degrees to rotate
	centerToStart.RotateDegrees(rotationDeltaDegrees);
	centerToEnd.RotateDegrees(rotationDeltaDegrees);
	m_start = currentCenter + centerToStart;
	m_end = currentCenter + centerToEnd;
}
