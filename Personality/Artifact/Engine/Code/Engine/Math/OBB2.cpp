#include "Engine/Math/OBB2.hpp"
#include "Engine/Math/MathUtils.hpp"


//-----------------------------------------------------------------------------------------------
OBB2::OBB2(){

}


//-----------------------------------------------------------------------------------------------
OBB2::OBB2(Vec2 const& iBasisNormal, Vec2 const& halfDimensions, Vec2 const& center){
	m_iBasisNormal = iBasisNormal;
	m_halfDimensions = halfDimensions;
	m_center = center;
}


//-----------------------------------------------------------------------------------------------
void OBB2::GetCornerPoints(Vec2* out_fourCornerWorldPositions) const{
	Vec2 jBasisNormal = m_iBasisNormal.GetRotated90Degrees();

	// calculate four corners in order of BL, BR, TR, TL
	out_fourCornerWorldPositions[0] = m_center - m_iBasisNormal * m_halfDimensions.x
		- jBasisNormal * m_halfDimensions.y;
	out_fourCornerWorldPositions[1] = m_center + m_iBasisNormal * m_halfDimensions.x
		- jBasisNormal * m_halfDimensions.y;
	out_fourCornerWorldPositions[2] = m_center + m_iBasisNormal * m_halfDimensions.x
		+ jBasisNormal * m_halfDimensions.y;
	out_fourCornerWorldPositions[3] = m_center - m_iBasisNormal * m_halfDimensions.x
	 	+ jBasisNormal * m_halfDimensions.y;
}


//-----------------------------------------------------------------------------------------------
Vec2 const OBB2::GetLocalPosForWorldPos(Vec2 const& worldPos) const{
	Vec2 jBasisNormal = m_iBasisNormal.GetRotated90Degrees();
	Vec2 centerToPoint = worldPos - m_center;

	// return local position in I J basis
	return Vec2(DotProduct2D(centerToPoint, m_iBasisNormal), DotProduct2D(centerToPoint, jBasisNormal));
}


//-----------------------------------------------------------------------------------------------
Vec2 const OBB2::GetWorldPosForLocalPos(Vec2 const& localPos) const{
	// return world position based on locla i j basis
	Vec2 jBasisNormal = m_iBasisNormal.GetRotated90Degrees();
	return (m_center + localPos.x * m_iBasisNormal + localPos.y * jBasisNormal);
}


//-----------------------------------------------------------------------------------------------
void OBB2::RotateAboutCenter(float rotationDeltaDegrees){
	m_iBasisNormal = Vec2::MakeFromPolarDegrees(m_iBasisNormal.GetOrientationDegrees() + rotationDeltaDegrees,
		m_iBasisNormal.GetLength());
}
