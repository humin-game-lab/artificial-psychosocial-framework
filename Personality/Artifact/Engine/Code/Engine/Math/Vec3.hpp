#pragma once

//-----------------------------------------------------------------------------------------------
struct Vec3
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;

	static const Vec3 ZERO;
	static const Vec3 ONE;

public:
	// Construction/Destruction
	~Vec3() {}														// destructor (do nothing)
	Vec3() = default;												// default constructor (do nothing)
	explicit Vec3(float x1, float y1, float z1);

	// Accessors(const methods)
	float GetLength() const;
	float GetLengthXY() const;
	float GetLengthSquared() const;
	float GetLengthXYSquared() const;
	float GetAngleAboutZRadians() const;
	float GetAngleAboutZDegrees() const;
	float GetAngleAboutYDegrees() const;
	Vec3 const GetRotatedAboutZRadians(float deltaRadians) const;
	Vec3 const GetRotatedAboutZDegrees(float deltaDegrees) const;
	Vec3 const GetRotatedAboutYDegrees(float deltaDegrees) const;
	Vec3 const GetClamped(float maxLength) const;
	Vec3 const GetNormalized() const;

	void SetFromText(const char* text);

	// Operators (const)
	bool		operator==(const Vec3& compare) const;		// Vec3 == Vec3
	bool		operator!=(const Vec3& compare) const;		// Vec3 != Vec3
	const Vec3	operator+(const Vec3& vecToAdd) const;		// Vec3 + Vec3
	const Vec3	operator+(float valueToAdd) const;		// Vec3 + float
	const Vec3	operator-(const Vec3& vecToSubtract) const;	// Vec3 - Vec3
	const Vec3	operator-(float valueSubtract) const;	// Vec3 - float
	const Vec3	operator*(float uniformScale) const;		// Vec3 * float
	const Vec3	operator/(float inverseScale) const;		// Vec3 / float

	// Operators (self-mutating / non-const)
	void		operator+=(const Vec3& vecToAdd);			// Vec3 += Vec3
	void		operator-=(const Vec3& vecToSubtract);		// Vec3 -= Vec3
	void		operator*=(const float uniformScale);		// Vec3 *= float
	void		operator/=(const float uniformDivisor);		// Vec3 /= float
	void		operator=(const Vec3& copyFrom);			// Vec3 = Vec3

	// Standalone "friend" functions that are conceptually, but not actually, part of Vec3::
	friend const Vec3 operator*(float uniformScale, const Vec3& vecToScale);
};


