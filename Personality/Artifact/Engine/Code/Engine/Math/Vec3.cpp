#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Core/StringUtils.hpp"
#include <math.h>
//#include "Engine/Core/EngineCommon.hpp"

const Vec3 Vec3::ZERO(0.f, 0.f, 0.f);
const Vec3 Vec3::ONE(1.f, 1.f, 1.f);

//-----------------------------------------------------------------------------------------------
Vec3::Vec3(float x1, float y1, float z1) {
	x = x1;
	y = y1;
	z = z1;
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetLength() const {
	return sqrtf(x * x + y * y + z * z);
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetLengthXY() const {
	return sqrtf(x * x + y * y);
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetLengthSquared() const {
	return (x * x + y * y + z * z);
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetLengthXYSquared() const {
	return (x * x + y * y);
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetAngleAboutZRadians() const {
	return static_cast<float>(atan2(y, x));
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetAngleAboutZDegrees() const {
	return Atan2Degrees(y, x);
}

//-----------------------------------------------------------------------------------------------
float Vec3::GetAngleAboutYDegrees() const{
	return Atan2Degrees(z, x);
}


//-----------------------------------------------------------------------------------------------
Vec3 const Vec3::GetRotatedAboutZRadians(float deltaRadians) const {
	float r = GetLengthXY();
	float angleAboutZRadians = GetAngleAboutZRadians();

	// add degrees
	angleAboutZRadians += deltaRadians;

	float cosine = static_cast<float>(cos(angleAboutZRadians));
	float sine = static_cast<float>(sin(angleAboutZRadians));
	
	// convert radians to Cartesian for rotation
	return Vec3(r * cosine , r * sine,z);
}

//-----------------------------------------------------------------------------------------------
Vec3 const Vec3::GetRotatedAboutZDegrees(float deltaDegrees) const {
	float r = GetLengthXY();
	float angleAboutZDegrees = GetAngleAboutZDegrees();


	// add degrees
	angleAboutZDegrees += deltaDegrees;

	// convert radians to Cartesian for rotation
	return Vec3(r * CosDegrees(angleAboutZDegrees), r * SinDegrees(angleAboutZDegrees),z);

}


//-----------------------------------------------------------------------------------------------
Vec3 const Vec3::GetRotatedAboutYDegrees(float deltaDegrees) const{
	float r = GetLengthXY();
	float angleAboutYDegrees = GetAngleAboutYDegrees();


	// add degrees
	angleAboutYDegrees += deltaDegrees;

	// convert radians to Cartesian for rotation
	return Vec3(r * CosDegrees(angleAboutYDegrees), y, r * -SinDegrees(angleAboutYDegrees));
}


//-----------------------------------------------------------------------------------------------
Vec3 const Vec3::GetClamped(float maxLength) const {
	float length = GetLength();
	if (length <= 0.f) {
		return Vec3(0.f,0.f,0.f);
	}
	float length_radio = maxLength / length;

	if (length_radio <= 1) {
		return Vec3(x * length_radio, y * length_radio, z* length_radio);
	}
	return Vec3(x, y,z);
}

//-----------------------------------------------------------------------------------------------
Vec3 const Vec3::GetNormalized() const {
	float length = GetLength();
	if (length <= 0.f) {
		return Vec3(0.f,0.f,0.f);
	}
	float divided_length = 1.f / length;
	return Vec3(x * divided_length, y * divided_length, z * divided_length);
}



//-----------------------------------------------------------------------------------------------
void Vec3::SetFromText(const char* text){
	Strings floats = SplitStringOnDelimiter(text, ',');
	x = static_cast<float>(atof(floats[0].c_str()));
	y = static_cast<float>(atof(floats[1].c_str()));
	z = static_cast<float>(atof(floats[2].c_str()));
}


//-----------------------------------------------------------------------------------------------
const Vec3 Vec3::operator + (const Vec3& vecToAdd) const {
	return Vec3(x + vecToAdd.x, y + vecToAdd.y, z+vecToAdd.z);
}


//-----------------------------------------------------------------------------------------------
const Vec3 Vec3::operator-(const Vec3& vecToSubtract) const {
	return Vec3(x - vecToSubtract.x, y - vecToSubtract.y, z - vecToSubtract.z);
}


//-----------------------------------------------------------------------------------------------
const Vec3 Vec3::operator-(float valueSubtract) const{
	return Vec3(x - valueSubtract, y - valueSubtract, z - valueSubtract);
}


//-----------------------------------------------------------------------------------------------
const Vec3 Vec3::operator*(float uniformScale) const {
	return Vec3(x * uniformScale, y * uniformScale, z*uniformScale);
}



//------------------------------------	-----------------------------------------------------------
const Vec3 Vec3::operator/(float inverseScale) const {
	if (inverseScale == 0.f) {
		return Vec3(0.f, 0.f, 0.f);
	}
	float divided_inverseScale = 1.f / inverseScale;
	return Vec3(x * divided_inverseScale, y * divided_inverseScale, z* divided_inverseScale);
}


//-----------------------------------------------------------------------------------------------
void Vec3::operator+=(const Vec3& vecToAdd) {
	x += vecToAdd.x;
	y += vecToAdd.y;
	z += vecToAdd.z;
}



//-----------------------------------------------------------------------------------------------
const Vec3 Vec3::operator+(float valueToAdd) const{
	return Vec3(x + valueToAdd, y + valueToAdd, z + valueToAdd);
}


//-----------------------------------------------------------------------------------------------
void Vec3::operator-=(const Vec3& vecToSubtract) {
	x -= vecToSubtract.x;
	y -= vecToSubtract.y;
	z -= vecToSubtract.z;
}


//-----------------------------------------------------------------------------------------------
void Vec3::operator*=(const float uniformScale) {
	x *= uniformScale;
	y *= uniformScale;
	z *= uniformScale;
}


//-----------------------------------------------------------------------------------------------
void Vec3::operator/=(const float uniformDivisor) {
	if (uniformDivisor == 0.f) {
		return;
	}
	float devided_divisor = 1.f / uniformDivisor;
	x *= devided_divisor;
	y *= devided_divisor;
	z *= devided_divisor;
}


//-----------------------------------------------------------------------------------------------
void Vec3::operator=(const Vec3& copyFrom) {
	x = copyFrom.x;
	y = copyFrom.y;
	z = copyFrom.z;
}


//-----------------------------------------------------------------------------------------------
const Vec3 operator*(float uniformScale, const Vec3& vecToScale) {
	return Vec3(vecToScale.x * uniformScale, vecToScale.y * uniformScale, 
		vecToScale.z * uniformScale);
}


//-----------------------------------------------------------------------------------------------
bool Vec3::operator==(const Vec3& compare) const {
	if (x == compare.x && y == compare.y && z == compare.z) {
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Vec3::operator!=(const Vec3& compare) const
{
	if (x != compare.x || y != compare.y || z != compare.z) {
		return true;
	}
	return false;
}
