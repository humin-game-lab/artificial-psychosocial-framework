#include "Engine/Math/AABB3.hpp"
#include "Engine/Math/MathUtils.hpp"

//-----------------------------------------------------------------------------------------------
AABB3::~AABB3() {

}


//-----------------------------------------------------------------------------------------------
AABB3::AABB3() {

}


//-----------------------------------------------------------------------------------------------
AABB3::AABB3(const AABB3& copyFrom) {
	m_mins = copyFrom.m_mins;
	m_maxs = copyFrom.m_maxs;
}


//-----------------------------------------------------------------------------------------------
AABB3::AABB3(Vec3 const& initialMins, Vec3 const& initialMaxs) {
	m_mins = initialMins;
	m_maxs = initialMaxs;
}


//-----------------------------------------------------------------------------------------------
AABB3::AABB3(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
	m_mins = Vec3(minX, minY, minZ);
	m_maxs = Vec3(maxX, maxY, maxZ);
}


//-----------------------------------------------------------------------------------------------
bool AABB3::IsPointInside(Vec3 const& point) const {
	if ((point.x > m_mins.x && point.y > m_mins.y && point.z > m_mins.z) 
		&& (point.x < m_maxs.x && point.y < m_maxs.y && point.z < m_maxs.z)) {
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
Vec3 const AABB3::GetCenter() const {
	float centerX = (m_mins.x + m_maxs.x) * 0.5f;
	float centerY = (m_mins.y + m_maxs.y) * 0.5f;
	float centerZ = (m_mins.z + m_maxs.z) * 0.5f;
	return Vec3(centerX, centerY, centerZ);
}


//-----------------------------------------------------------------------------------------------
Vec3 const AABB3::GetDimensions() const {
	return Vec3(m_maxs.x - m_mins.x, m_maxs.y - m_mins.y, m_maxs.z - m_mins.z);
}


//-----------------------------------------------------------------------------------------------
Vec3 const AABB3::GetNearestPoint(Vec3 const& point) const {
	// using clamp to get the point
	// if point is inside, clamp function will return original point values
	// else if point is outside, the clamp will return clamped values based on the min and max
	Vec3 returnPoint(0.f,0.f, 0.f);
	returnPoint.x = Clamp(point.x, m_mins.x, m_maxs.x);
	returnPoint.y = Clamp(point.y, m_mins.y, m_maxs.y);
	returnPoint.z = Clamp(point.z, m_mins.z, m_maxs.z);

	return returnPoint;
}


//-----------------------------------------------------------------------------------------------
void AABB3::Translate(Vec3 const& displacement) {
	m_mins += displacement;
	m_maxs += displacement;
	
}


//-----------------------------------------------------------------------------------------------
void AABB3::SetCenter(Vec3 const& newCenter) {
	Vec3 center = GetCenter();
	Vec3 centerToMinDisplacement = center - m_mins;
	Vec3 centerToMaxDispalcement = center - m_maxs;

	m_mins = newCenter - centerToMinDisplacement;
	m_maxs = newCenter - centerToMaxDispalcement;
}


//-----------------------------------------------------------------------------------------------
void AABB3::SetDimensions(Vec3 const& newDimension) {
	// get the center
	Vec3 center = GetCenter();

	// get half lengths of new dimension
	float halfXDimension = newDimension.x * 0.5f;
	float halfYDimension = newDimension.y * 0.5f;
	float halfZDimension = newDimension.z * 0.5f;

	// calculate new min and max
	m_mins = Vec3(center.x - halfXDimension, center.y - halfYDimension, center.z - halfZDimension);
	m_maxs = Vec3(center.x + halfXDimension, center.y + halfYDimension, center.z + halfZDimension);

}


//-----------------------------------------------------------------------------------------------
void AABB3::StretchToIncludePoint(Vec3 const& point) {
	if (!IsPointInside(point)) {
		if (point.x < m_mins.x) {
			m_mins.x = point.x;
		}
		else if (point.x > m_maxs.x) {
			m_maxs.x = point.x;
		}

		if (point.y < m_mins.y) {
			m_mins.y = point.y;
		}
		else if (point.y > m_maxs.y) {
			m_maxs.y = point.y;
		}

		if (point.z < m_mins.z) {
			m_mins.z = point.z;
		}
		else if (point.z > m_maxs.z) {
			m_maxs.z = point.z;
		}
	}
}

