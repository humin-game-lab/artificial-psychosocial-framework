#pragma once

struct Vec3;

//-----------------------------------------------------------------------------------------------
struct Vec4
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
	float w = 0.f;

public:
	// Construction/Destruction
	~Vec4();
	Vec4();
	Vec4( const Vec4& copyFrom );
	explicit Vec4( float initialX, float initialY, float initialZ, float initialW);
	explicit Vec4(Vec3 vector3D, float initialW);
	

	const Vec4	operator-(const Vec4& vecToSubtract) const;	// Vec4 - Vec4
	void		operator*=(const float uniformScale);		// Vec4 *= float
};


