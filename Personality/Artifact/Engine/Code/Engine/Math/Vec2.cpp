#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Core/StringUtils.hpp"
#include <math.h>
//#include "Engine/Core/EngineCommon.hpp"

const Vec2 Vec2::ZERO( 0.f, 0.f);
const Vec2 Vec2::ONE(1.f, 1.f);

//-----------------------------------------------------------------------------------------------
Vec2::Vec2( const Vec2& copy )
	: x( copy.x )
	, y( copy.y ) {
}


//-----------------------------------------------------------------------------------------------
Vec2::Vec2( float initialX, float initialY )
	: x(initialX)
	, y(initialY) {
}


//-----------------------------------------------------------------------------------------------
const Vec2 Vec2::operator + ( const Vec2& vecToAdd ) const {
	return Vec2( x+vecToAdd.x, y+vecToAdd.y );
}


//-----------------------------------------------------------------------------------------------
const Vec2 Vec2::operator-( const Vec2& vecToSubtract ) const {
	return Vec2(x - vecToSubtract.x, y - vecToSubtract.y);
}


//------------------------------------------------------------------------------------------------
const Vec2 Vec2::operator-() const {
	return Vec2( -x,-y );
}


//-----------------------------------------------------------------------------------------------
const Vec2 Vec2::operator*( float uniformScale ) const {
	return Vec2( x*uniformScale, y*uniformScale );
}


//------------------------------------------------------------------------------------------------
const Vec2 Vec2::operator*( const Vec2& vecToMultiply ) const {
	return Vec2( x*vecToMultiply.x, y* vecToMultiply.y );
}


//------------------------------------	-----------------------------------------------------------
const Vec2 Vec2::operator/( float inverseScale ) const {
	return Vec2( x/inverseScale,y/inverseScale );
}


//-----------------------------------------------------------------------------------------------
bool Vec2::operator<=(const Vec2& compare) const {
	return GetLength() <= compare.GetLength();
}


//-----------------------------------------------------------------------------------------------
bool Vec2::operator>=(const Vec2& compare) const {
	return GetLength() >= compare.GetLength();
}


//-----------------------------------------------------------------------------------------------
void Vec2::operator+=( const Vec2& vecToAdd ) {
	x += vecToAdd.x;
	y += vecToAdd.y;
}


//-----------------------------------------------------------------------------------------------
void Vec2::operator-=( const Vec2& vecToSubtract ) {
	x -= vecToSubtract.x;
	y -= vecToSubtract.y;
}


//-----------------------------------------------------------------------------------------------
void Vec2::operator*=( const float uniformScale ) {
	x *= uniformScale;
	y *= uniformScale;
}


//-----------------------------------------------------------------------------------------------
void Vec2::operator/=( const float uniformDivisor ) {
	float divided_divisor = 1.f / uniformDivisor;
	x *= divided_divisor;
	y *= divided_divisor;
}


//-----------------------------------------------------------------------------------------------
void Vec2::operator=( const Vec2& copyFrom ) {
	x = copyFrom.x;
	y = copyFrom.y;
}


//-----------------------------------------------------------------------------------------------
const Vec2 operator*( float uniformScale, const Vec2& vecToScale ) {
	return Vec2( vecToScale.x*uniformScale, vecToScale.y * uniformScale );
}


//-----------------------------------------------------------------------------------------------
bool Vec2::operator==( const Vec2& compare ) const {
	if (x == compare.x && y == compare.y) {
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Vec2::operator!=( const Vec2& compare ) const {
	if (x != compare.x || y != compare.y) {
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------
// static method convert radians polar orientation to (x,y) form 
// using formula x = r* cos (theta) and y = r* sin(theta)
Vec2 const Vec2::MakeFromPolarRadians(float orientationRadians, float length) {
	float x = length * static_cast<float>(cos(orientationRadians));
	float y = length * static_cast<float>(sin(orientationRadians));
	return Vec2(x, y);
}

//-----------------------------------------------------------------------------------------------
// convert degree polar orientation to (x,y) form 
// using formula x = r* cos (theta) and y = r* sin(theta)
Vec2 const Vec2::MakeFromPolarDegrees(float orientationDegrees, float length) {
	float x = length * CosDegrees(orientationDegrees);
	float y = length * SinDegrees(orientationDegrees);
	return Vec2(x, y);
}

//-----------------------------------------------------------------------------------------------
float Vec2::GetLength() const {
	return sqrtf(x * x + y * y);
}

//-----------------------------------------------------------------------------------------------
float Vec2::GetLengthSquared() const {
	return (x * x + y * y);
}

//-----------------------------------------------------------------------------------------------
float Vec2::GetOrientationRadians() const {
	return static_cast<float>(atan2(y, x));
}

//-----------------------------------------------------------------------------------------------
float Vec2::GetOrientationDegrees() const {
	return Atan2Degrees(y, x);
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetRotated90Degrees() const {
	return Vec2(-y, x);
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetRotatedMinus90Degrees() const {
	return Vec2(y, -x);
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetRotatedRadians(float deltaRadians) const {
	float r = GetLength();
	float orientationRadians = GetOrientationRadians();


	// add degrees
	orientationRadians += deltaRadians;

	// convert radians to Cartesian for rotation
	return Vec2(r * static_cast<float>(cos(orientationRadians)), r * 
		static_cast<float>(sin(orientationRadians)));
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetRotatedDegrees(float deltaDegrees) const {
	float r = GetLength();
	float orientationDegrees = GetOrientationDegrees();


	// add degrees
	orientationDegrees += deltaDegrees;

	// convert radians to Cartesian for rotation
	return Vec2(r * CosDegrees(orientationDegrees), r * SinDegrees(orientationDegrees));
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetClamped(float maxLength) const {
	float length = GetLength();
	if (length > 0.f) {
		float length_radio = maxLength / length;

		if (length_radio <= 1) {
			return Vec2(x * length_radio, y * length_radio);
		}
	}
	
	return Vec2(x,y);
}

//-----------------------------------------------------------------------------------------------
Vec2 const Vec2::GetNormalized() const {
	float length = GetLength();
	if (length <= 0.f) {
		return Vec2(0.f, 0.f);
	}
	float divided_length = 1.f / length;
	return Vec2(x* divided_length, y * divided_length);
}


//---------------------------------------------------------------------------------------------
Vec2 const Vec2::GetReflected(Vec2 const& impactSurfaceNormal) const
{
	Vec2 copy = Vec2(x, y);
	Vec2 projectedSurfaceSide = DotProduct2D(copy,impactSurfaceNormal)*impactSurfaceNormal;
	Vec2 projectedT = copy - projectedSurfaceSide;
	return projectedT - projectedSurfaceSide;
}

//-----------------------------------------------------------------------------------------------
void Vec2::SetOrientationRadians(float newOrientationRadians) {
	float length = GetLength();
	x = length * static_cast<float>(cos(newOrientationRadians));
	y = length * static_cast<float>(sin(newOrientationRadians));
}

//-----------------------------------------------------------------------------------------------
void Vec2::SetOrientationDegrees(float newOrientationDegrees) {
	float length = GetLength();
	x = length * CosDegrees(newOrientationDegrees);
	y = length * SinDegrees(newOrientationDegrees);
}

//-----------------------------------------------------------------------------------------------
void Vec2::SetPolarRadians(float newOrientationRadians, float newLength) {
	x = newLength * static_cast<float>(cos(newOrientationRadians));
	y = newLength * static_cast<float>(sin(newOrientationRadians));
}

//-----------------------------------------------------------------------------------------------
void Vec2::SetPolarDegrees(float newOrientationDegrees, float newLength) {
	x = newLength * CosDegrees(newOrientationDegrees);
	y = newLength * SinDegrees(newOrientationDegrees);
}

//-----------------------------------------------------------------------------------------------
void Vec2::Rotate90Degrees() {
	float temp = x;
	x = -y;
	y = temp;
}

//-----------------------------------------------------------------------------------------------
void Vec2::RotateMinus90Degrees() {
	float temp = x;
	x = y;
	y = -temp;
}

//-----------------------------------------------------------------------------------------------
void Vec2::RotateRadians(float deltaRadians) {
	float r = GetLength();
	float orientationRadians = GetOrientationRadians();


	// add degrees
	orientationRadians += deltaRadians;

	// convert radians to Cartesian for rotation
	x = r * static_cast<float>(cos(orientationRadians));
	y = r * static_cast<float>(sin(orientationRadians));
}

//-----------------------------------------------------------------------------------------------
void Vec2::RotateDegrees(float deltaDegrees) {
	float r = GetLength();
	float orientationDegrees = GetOrientationDegrees();


	// add degrees
	orientationDegrees += deltaDegrees;

	// convert radians to Cartesian for rotation
	x = r * CosDegrees(orientationDegrees);
	y = r * SinDegrees(orientationDegrees);
}

//-----------------------------------------------------------------------------------------------
void Vec2::ClampLength(float maxLength) {
	float length = GetLength();
	if (length <= 0.f) {
		return;
	}
	float size_radio = maxLength / length;

	if (size_radio <= 1) {
		x *= size_radio;
		y *= size_radio;
	}
}

//-----------------------------------------------------------------------------------------------
void Vec2::SetLength(float newLength) {
	float length = GetLength();
	if (length <= 0.f) {
		return;
	}
	float length_radio = newLength / length;
	x *= length_radio;
	y *= length_radio;
}

//-----------------------------------------------------------------------------------------------
void Vec2::Normalize() {
	float length = GetLength();
	if (length <= 0.f) {
		return;
	}
	float divided_length = 1.f / length;
	x *= divided_length;
	y *= divided_length;
}

//-----------------------------------------------------------------------------------------------
float Vec2::NormalizeAndGetPreviousLength() {
	float length = GetLength();
	Normalize();

	return length;
}

//-----------------------------------------------------------------------------------------------
void Vec2::Reflect(Vec2 const& impactSurfaceNormal)
{
	Vec2 copy = Vec2(x, y);
	Vec2 projectedSurfaceSide = DotProduct2D(copy,impactSurfaceNormal)*impactSurfaceNormal;
	Vec2 projectedT = copy - projectedSurfaceSide;
	x = projectedT.x - projectedSurfaceSide.x;
	y = projectedT.y - projectedSurfaceSide.y;
}


//-----------------------------------------------------------------------------------------------
void Vec2::SetFromText(const char* text){
	Strings floats = SplitStringOnDelimiter(text, ',');
	x = static_cast<float>(atof(floats[0].c_str()));
	y = static_cast<float>(atof(floats[1].c_str()));
}
