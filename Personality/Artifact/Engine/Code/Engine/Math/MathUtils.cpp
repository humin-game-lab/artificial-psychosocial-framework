#include "Engine/Core/Vertex_PCU.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/AABB3.hpp"
#include "Engine/Math/OBB2.hpp"
#include "Engine/Math/Vec4.hpp"
#include "Engine/Math/LineSegment2.hpp"
#include "Engine/Math/Capsule2.hpp"
#include "Engine/Math/FloatRange.hpp"
#include "Engine/Math/RandomNumberGenerator.hpp"
#include <math.h>
#include <cmath>

//-----------------------------------------------------------------------------------------------
// convert degrees to radians
float ConvertDegreesToRadians(float degrees) {
	return degrees * static_cast<float>(3.1415926535897 / 180.f);
}


//-----------------------------------------------------------------------------------------------
// convert radians to degrees
float ConvertRadiansToDegrees(float radians) {
	return radians * static_cast<float>(180.f / 3.1415926535897);
}


//-----------------------------------------------------------------------------------------------
// return cosine value passing degrees instead of radians
float CosDegrees(float degrees) {
	float radians = ConvertDegreesToRadians(degrees);
	return static_cast<float>(cos(radians));
}


//-----------------------------------------------------------------------------------------------
// return sine value passing degrees instead of radians
float SinDegrees(float degrees) {
	float radians = ConvertDegreesToRadians(degrees);
	return static_cast<float>(sin(radians));
}


//-----------------------------------------------------------------------------------------------
// calculate degrees using position y and x : atan2 (y,x) function
float Atan2Degrees(float y, float x) {
	float atan2Radians = static_cast<float>(atan2(y, x));
	return ConvertRadiansToDegrees(atan2Radians);
}

//-----------------------------------------------------------------------------------------------
float TanDegrees(float degrees) {
	float radians = ConvertDegreesToRadians(degrees);
	return static_cast<float>(tan(radians));
}

//-----------------------------------------------------------------------------------------------
float ArcCosDegrees(float cosValue)
{
	float radians = static_cast<float>(acos(cosValue));
	return ConvertRadiansToDegrees(radians);
}

//-----------------------------------------------------------------------------------------------
// using Pythagorean theorem to calculate distance 
// between A & B:distance^2 =  (x1-x2)^2 + (y1-y2)^2
float GetDistance2D(Vec2 const& positionA, Vec2 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	return sqrtf(dx * dx + dy * dy);
}


//-----------------------------------------------------------------------------------------------
// Get the squared distance value between A & B
float GetDistanceSquared2D(Vec2 const& positionA, Vec2 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	return (dx * dx + dy * dy);
}


//-----------------------------------------------------------------------------------------------
// using Pythagorean theorem to calculate distance 
// between A & B in 3D:distance^2 =  (x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2
float GetDistance3D(Vec3 const& positionA, Vec3 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	float dz = positionA.z - positionB.z;
	return sqrtf(dx * dx + dy * dy + dz*dz);
}


//-----------------------------------------------------------------------------------------------
// Get the squared distance value between A & B in 3D
float GetDistanceSquared3D(Vec3 const& positionA, Vec3 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	float dz = positionA.z - positionB.z;
	return (dx * dx + dy * dy + dz * dz);
}


//-----------------------------------------------------------------------------------------------
// only calculate the xy distance between 3D points A and B
float GetDistanceXY3D(Vec3 const& positionA, Vec3 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	return sqrtf(dx * dx + dy * dy);
}


//-----------------------------------------------------------------------------------------------
// Get the squared value 
float GetDistanceXYSquared3D(Vec3 const& positionA, Vec3 const& positionB) {
	float dx = positionA.x - positionB.x;
	float dy = positionA.y - positionB.y;
	return (dx * dx + dy * dy);
}

//-----------------------------------------------------------------------------------------------
// get the distance only on x and y-axis, ignore the z-axis
// calculation equation is d = sqrt (x^2 + y^2)
float GetDistanceXY3D(Vec3 const& position) {
	return sqrtf(position.x * position.x + position.y * position.y);
}


//-----------------------------------------------------------------------------------------------
// check if disc A and B overlap based on distance between A & B VS Radius A + Radius B
bool DoDiscsOverlap(Vec2 const& centerA, float radiusA, Vec2 const& centerB, float radiusB) {

	float distance2DSquared = (centerA - centerB).GetLengthSquared();
	float maxDistanceSquared = (radiusA + radiusB) * (radiusA + radiusB);
	if (maxDistanceSquared >= distance2DSquared) {
		return true;
	}

	return false;
}


//-----------------------------------------------------------------------------------------------
// check if sphere A and B overlap based on distance between A & B VS Radius A + Radius B
bool DoSpheresOverlap(Vec3 const& centerA, float radiusA, Vec3 const& centerB, float radiusB) {
	float distance3D = GetDistance3D(centerA, centerB);
	float maxDistance = radiusA + radiusB;
	if (maxDistance >= distance3D) {
		return true;
	}

	return false;
}


//-----------------------------------------------------------------------------------------------
bool DoSphereOverlapAABB3(Vec3 const& centerA, float radiusA, AABB3 const& box){
	Vec3 nearestPointOnSphere = GetNearestPointOnSphere(box.GetCenter(), centerA, radiusA);
	return box.IsPointInside(nearestPointOnSphere);
}


//-----------------------------------------------------------------------------------------------
bool DoSphereOverlapCylinderZ(Vec3 const& centerA, float radiusA, Vec3 const& cylinderPos, 
	FloatRange cylinderZ, float cylinderRadius){
	Vec2 cynlinderPos2D = Vec2(cylinderPos.x, cylinderPos.y);
	Vec2 sphereCenter2D = Vec2(centerA.x, centerA.y);
	if(DoDiscsOverlap(cynlinderPos2D, cylinderRadius, sphereCenter2D, radiusA)){
		FloatRange sphereZRange = FloatRange(centerA.z - radiusA, centerA.z + radiusA);
		FloatRange objectOverlapping = sphereZRange.GetOverlappingPart(cylinderZ);
		if(objectOverlapping != FloatRange::ZERO){
			return true;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool DoAABB3OverlapAABB3(AABB3 const& boxA, AABB3 const& boxB){
	if(boxA.m_mins.x > boxB.m_maxs.x || boxA.m_maxs.x < boxB.m_mins.x ||
		boxA.m_mins.y > boxB.m_maxs.y || boxA.m_maxs.y < boxB.m_mins.y || 
		boxA.m_mins.z > boxB.m_maxs.z || boxA.m_maxs.z < boxB.m_mins.z){
			return false;
		}
	return true;
}


//-----------------------------------------------------------------------------------------------
bool DoAABB3OverlapCylinderZ(AABB3 const& box, Vec3 const& cylinderPos, FloatRange cylinderZ, 
	float cylinderRadius){
	Vec2 cynlinderPos2D = Vec2(cylinderPos.x, cylinderPos.y);
	Vec2 boxPos2D = Vec2(box.GetCenter().x, box.GetCenter().y);
	AABB2 boxTopView(Vec2(box.m_mins.x, box.m_mins.y), Vec2(box.m_maxs.x, box.m_maxs.y));
	Vec2 nearestPointOnDisc = GetNearestPointOnDisc2D(boxPos2D, cynlinderPos2D, cylinderRadius);
	if(boxTopView.IsPointInside(nearestPointOnDisc)){
		if(box.m_mins.z > cylinderZ.m_max || box.m_maxs.z < cylinderZ.m_min){
		return false;
		}
		else{
			return true;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool DoCylinderZOverlapCylinderZ(Vec3 const& cylinderAPos, FloatRange cylinderAZ, float cylinderARadius, 
	Vec3 const& cylinderBPos, FloatRange cylinderBZ, float cylinderBRadius){
	Vec2 cynlinderAPos2D = Vec2(cylinderAPos.x, cylinderAPos.y);
	Vec2 cynlinderBPos2D = Vec2(cylinderBPos.x, cylinderBPos.y);
	if (DoDiscsOverlap(cynlinderAPos2D, cylinderARadius, cynlinderBPos2D, cylinderBRadius)) {
		if (cylinderAZ.m_min > cylinderBZ.m_max || cylinderAZ.m_max < cylinderBZ.m_min) {
			return false;
		}
		else {
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------------------
// check if the distance between disc and point is less than the radius
// if distance < radius, it is inside the disc
// if distance >= tadius, it is not inside the disc
bool IsPointInsideDisc2D(Vec2 const& point, Vec2 const& discCenter, float discRadius)
{
	float distance2D = GetDistance2D(discCenter, point);
	if(distance2D < discRadius){
		return true;
	}
	else {
		return false;
	}
}



bool IsPointInsideSphere(Vec3 const& point, Vec3 const& sphereCenter, float sphereRadius){
	float distance3D = GetDistance3D(sphereCenter, point);
	if (distance3D < sphereRadius) {
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
bool IsPointInsideAABB2D(Vec2 const& point, AABB2 const& box){
	return box.IsPointInside(point);
}



bool IsPointInsideAABB3D(Vec3 const& point, AABB3 const& box){
	return box.IsPointInside(point);
}


//-----------------------------------------------------------------------------------------------
bool IsPointInsideCapsule2D(Vec2 const& point, Capsule2 const& capsule){
	// find nearest point on capsule bone, and then see if the point distance to bone is 
	// greater or smaller than the capsule radius
	Vec2 nearestPoint = GetNearestPointOnLineSegment2D(point, capsule.m_start, capsule.m_end);
	return GetDistance2D(point, nearestPoint) < capsule.m_radius;
}


//-----------------------------------------------------------------------------------------------
bool IsPointInsideCapsule2D(Vec2 const& point, Vec2 const& boneStart, Vec2 const& boneEnd, float radius){
	Vec2 nearestPoint = GetNearestPointOnLineSegment2D(point, boneStart, boneEnd);
	return GetDistance2D(point, nearestPoint) < radius;
}


//-----------------------------------------------------------------------------------------------
bool IsPointInsideOBB2D(Vec2 const& point, OBB2 const& orientedBox){
	// convert the point position to local position and then compare it to obb dimensions
	Vec2 centerToPoint = point - orientedBox.m_center;
	Vec2 jBasisNormal = orientedBox.m_iBasisNormal.GetRotated90Degrees();
	
	float pointIBasis = DotProduct2D(centerToPoint, orientedBox.m_iBasisNormal);
	float pointJBasis = DotProduct2D(centerToPoint, jBasisNormal);

	return (pointIBasis > -orientedBox.m_halfDimensions.x && pointIBasis < orientedBox.m_halfDimensions.x
		&& pointJBasis > -orientedBox.m_halfDimensions.y && pointJBasis < orientedBox.m_halfDimensions.y);
}

//-----------------------------------------------------------------------------------------------
// check if point is inside, if it is, return the point
// else get the normalize vector * radius + center to get nearest point
Vec2 const GetNearestPointOnDisc2D(Vec2 const& referencePos, Vec2 const& discCenter, float discRadius)
{
	Vec2 displacement = referencePos - discCenter;
	float displacementLength = displacement.GetLength();
	float distanceBetweenDiscAndPoint = displacementLength - (discRadius);
	if (distanceBetweenDiscAndPoint <= 0.f) {
		return referencePos;
	}
	else {
		return displacement.GetNormalized() * discRadius + discCenter;
	}
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnAABB2D(Vec2 const& referencePos, AABB2& box){
	return box.GetNearestPoint(referencePos);
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnInfiniteLine2D(Vec2 const& referencePos, LineSegment2 const& infiniteLine){
	Vec2 startToEnd = infiniteLine.m_end - infiniteLine.m_start;
	Vec2 startToPosition = referencePos - infiniteLine.m_start;

	Vec2 startToNearestPoint = GetProjectedOnto2D(startToPosition, startToEnd);
	return startToNearestPoint + infiniteLine.m_start;
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnInfiniteLine2D(Vec2 const& referencePos, Vec2 const& pointOnLine, 
	Vec2 const& anotherPointOnLine){
	Vec2 startToEnd = anotherPointOnLine - pointOnLine;
	Vec2 startToPosition = referencePos - pointOnLine;

	Vec2 startToNearestPoint = GetProjectedOnto2D(startToPosition, startToEnd);
	return startToNearestPoint + pointOnLine;
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnLineSegment2D(Vec2 const& referencePos, LineSegment2 const& lineSegment){
	// determine if the point is close to start point or end point
	// if so, return start or end point
	Vec2 startToEnd = lineSegment.m_end - lineSegment.m_start;
	Vec2 endToPosition = referencePos - lineSegment.m_end;
	if (DotProduct2D(startToEnd, endToPosition) >= 0.f) {
		return lineSegment.m_end;
	}

	Vec2 startToPosition = referencePos - lineSegment.m_start;
	if (DotProduct2D(startToEnd, startToPosition) <= 0.f) {
		return lineSegment.m_start;
	}

	// find the point on the line
	return GetNearestPointOnInfiniteLine2D(referencePos, lineSegment.m_start, lineSegment.m_end);
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnLineSegment2D(Vec2 const& referencePos, Vec2 const& lineSegStart, 
	Vec2 const& lineSegEnd){
	Vec2 startToEnd = lineSegEnd - lineSegStart;
	Vec2 endToPosition = referencePos - lineSegEnd;
	if (DotProduct2D(startToEnd, endToPosition) >= 0.f) {
		return lineSegEnd;
	}

	Vec2 startToPosition = referencePos - lineSegStart;
	if (DotProduct2D(startToEnd, startToPosition) <= 0.f) {
		return lineSegStart;
	}
	return GetNearestPointOnInfiniteLine2D(referencePos, lineSegStart, lineSegEnd);
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnCapsule2D(Vec2 const& referencePos, Capsule2 const& capsule){
	// first find the vector between point and nearest point on the bone,
	Vec2 nearestPointOnBone = GetNearestPointOnLineSegment2D(referencePos, capsule.m_start, 
		capsule.m_end);
	Vec2 nearestPointOnBoneToPoint = referencePos - nearestPointOnBone;

	// clamp vector based on capsule radius
	float clampedLength = Clamp(nearestPointOnBoneToPoint.GetLength(), 0.f, capsule.m_radius);
	return nearestPointOnBone + nearestPointOnBoneToPoint.GetNormalized() * clampedLength;
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnCapsule2D(Vec2 const& referencePos, Vec2 const& boneStart, 
	Vec2 const& boneEnd, float radius){
	Vec2 nearestPointOnBone = GetNearestPointOnLineSegment2D(referencePos, boneStart,
		boneEnd);
	Vec2 nearestPointOnBoneToPoint = referencePos - nearestPointOnBone;

	float clampedLength = Clamp(nearestPointOnBoneToPoint.GetLength(), 0.f, radius);
	return nearestPointOnBoneToPoint.GetNormalized() * clampedLength;
}


//-----------------------------------------------------------------------------------------------
Vec2 const GetNearestPointOnOBB2D(Vec2 const& referencePos, OBB2 const& orientedBox){
	// first convert the point position to local position
	Vec2 jBasisNormal = orientedBox.m_iBasisNormal.GetRotated90Degrees();
	Vec2 localPosition = orientedBox.GetLocalPosForWorldPos(referencePos);

	// clamp local position based on obb dimension
	float NearestPointOnI = Clamp(localPosition.x, -orientedBox.m_halfDimensions.x,
		orientedBox.m_halfDimensions.x);
	float NearestPointOnJ = Clamp(localPosition.y, -orientedBox.m_halfDimensions.y,
		orientedBox.m_halfDimensions.y);
	// convert the local position back to world position and return the nearest point
	return orientedBox.GetWorldPosForLocalPos(Vec2(NearestPointOnI, NearestPointOnJ));
}


//-----------------------------------------------------------------------------------------------
Vec3 const GetNearestPointOnSphere(Vec3 const& referencePos, Vec3 const& sphereCenter, float radius){
	Vec3 centerToPosition = referencePos - sphereCenter;
	float clampedDistance = Clamp(centerToPosition.GetLength(), 0.f, radius);
	return centerToPosition.GetNormalized() * clampedDistance + sphereCenter;
}


//-----------------------------------------------------------------------------------------------
Vec3 const GetNearestPointOnAABB3(Vec3 const& referencePos, Vec3 const& aabb3Mins, Vec3 const& aabb3Maxs){
	Vec3 nearestPoint;
	nearestPoint.x = Clamp(referencePos.x, aabb3Mins.x, aabb3Maxs.x);
	nearestPoint.y = Clamp(referencePos.y, aabb3Mins.y, aabb3Maxs.y);
	nearestPoint.z = Clamp(referencePos.z, aabb3Mins.z, aabb3Maxs.z);
	return nearestPoint;
}


//-----------------------------------------------------------------------------------------------
Vec3 const GetNearestPointOnCylinder(Vec3 const& referencePos, Vec2 const& cylinderCenter, 
	float cylinderRadius, FloatRange zRange){
	Vec2 nearestPointDisc = GetNearestPointOnDisc2D(Vec2(referencePos.x, referencePos.y), cylinderCenter,
	cylinderRadius);
	Vec3 nearestPoint;
	nearestPoint.x = nearestPointDisc.x;
	nearestPoint.y = nearestPointDisc.y;
	nearestPoint.z = Clamp(referencePos.z, zRange.m_min, zRange.m_max);
	return nearestPoint;
}


//-----------------------------------------------------------------------------------------------
// first check if the point is in a disc with the same radius
// if not, then check if the angle between point and sector forward
bool IsPointInsideOrientedSector2D(Vec2 const& point, Vec2 const& sectorTip, 
	float sectorForwardDegrees, float sectorApertureDegrees, float sectorRadius){
	if (!IsPointInsideDisc2D(point, sectorTip, sectorRadius)) {
		return false;
	}
	if (sectorForwardDegrees < 0.f) {
		sectorForwardDegrees += 360.f;
	}
	Vec2 forwardVector = Vec2::MakeFromPolarDegrees(sectorForwardDegrees);
	Vec2 centerToPoint = point - sectorTip;
	float angleToPoint = GetAngleDegreesBetweenVectors2D(forwardVector, centerToPoint);
	return angleToPoint < (sectorApertureDegrees * 0.5f);
}

//-----------------------------------------------------------------------------------------------
// first check if the point is in a disc with the same radius
// if not, then check if the angle between point and sector forward
bool IsPointInsideDirectedSector2D(Vec2 const& point, Vec2 const& sectorTip, 
	Vec2 const& sectorForwardNormal, float sectorApertureDegrees, float sectorRadius){
	if (!IsPointInsideDisc2D(point, sectorTip, sectorRadius)) {
		return false;
	}
	Vec2 centerToPoint = point - sectorTip;
	float angleToPoint = GetAngleDegreesBetweenVectors2D(sectorForwardNormal, centerToPoint);
	return angleToPoint < (sectorApertureDegrees * 0.5f);
}

//-----------------------------------------------------------------------------------------------
// first check if point is inside of disc (if disc really overlaps with the point)
// then calculate moving direction and distance
bool PushDiscOutOfPoint2D(Vec2& mobileDiscCenter, float discRadius, Vec2 const& fixedPoint){
	if (!IsPointInsideDisc2D(fixedPoint, mobileDiscCenter, discRadius)) {
		return false;
	};
	
	Vec2 displacement = mobileDiscCenter - fixedPoint;
	float displacementLength = displacement.GetLength();
	float distanceBetweenDiscAndPoint = displacementLength - (discRadius);
	if (distanceBetweenDiscAndPoint < -0.001f) {
		Vec2 pushNormal = displacement.GetNormalized();
		mobileDiscCenter += pushNormal * fabsf(distanceBetweenDiscAndPoint);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------
// calculate moving direction and distance
// push disc if discs overlap
bool PushDiscOutOfDisc2D(Vec2& mobileDiscCenter, float mobileRadius, 
	Vec2 const& fixedDiscCenter, float fixedRadius, float pushFraction){
	if (!DoDiscsOverlap(mobileDiscCenter, mobileRadius,
		fixedDiscCenter, fixedRadius)){
		return false;
	}
	Vec2 displacement = mobileDiscCenter - fixedDiscCenter;
	float displacementLength = displacement.GetLength();
	float distanceBetweenTwoDisc = displacementLength - (mobileRadius + fixedRadius);
	if (distanceBetweenTwoDisc < -0.001f) {
		Vec2 pushNormal = displacement.GetNormalized();
		mobileDiscCenter += pushNormal * fabsf(distanceBetweenTwoDisc) * pushFraction;
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------
// calculate moving direction and distance
// push both disc half way
bool PushDiscsOutOfEachOther2D(Vec2& aCenter, float aRadius, Vec2& bCenter, float bRadius, float pushFraction){
	if (!DoDiscsOverlap(aCenter, aRadius,
		bCenter, bRadius)) {
		return false;
	}
	Vec2 displacement = aCenter - bCenter;
	float displacementLength = displacement.GetLength();
	float distanceBetweenTwoDisc = displacementLength - (aRadius + bRadius);
	if (displacementLength < (aRadius + bRadius)) {
		Vec2 pushNormal = displacement.GetNormalized();
		aCenter += pushNormal * fabsf(distanceBetweenTwoDisc * 0.5f * pushFraction);
		bCenter -= pushNormal * fabsf(distanceBetweenTwoDisc * 0.5f * pushFraction);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------
// calculate moving direction and distance
// push disc if disc and AABB2 overlap
bool PushDiscOutOfAABB2D(Vec2& mobileDiscCenter, float discRadius, AABB2 const& fixedBox){
	Vec2 aabb2Point = fixedBox.GetNearestPoint(mobileDiscCenter);
	if (IsPointInsideDisc2D(aabb2Point, mobileDiscCenter, discRadius)) {
		return PushDiscOutOfPoint2D(mobileDiscCenter, discRadius, aabb2Point);
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
Vec3 const GetPointOnSphere(float yaw, float pitch){
	return Vec3(CosDegrees(yaw) * CosDegrees(pitch), SinDegrees(yaw) * CosDegrees(pitch),
		-SinDegrees(pitch));
}



//-----------------------------------------------------------------------------------------------
Vec3 const GetRandomPointInCircle3D(Vec3 const& center, float radius, Vec3 const& ibasis, Vec3 const& jbasis){
	RandomNumberGenerator rng;
	Vec3 iVector = ibasis * radius * rng.GetRandomFloatInRange(-1.f, 1.f);
	Vec3 jVector = jbasis * radius * rng.GetRandomFloatInRange(-1.f, 1.f);
	Vec3 finalPoint = iVector + jVector;
	while(finalPoint.GetLengthSquared() > radius*radius){
		iVector = ibasis * radius * rng.GetRandomFloatInRange(-1.f, 1.f);
		jVector = jbasis * radius * rng.GetRandomFloatInRange(-1.f, 1.f);
		finalPoint = iVector + jVector;
	}
	return finalPoint+center;
}


//-----------------------------------------------------------------------------------------------
RaycastResult2D RaycastDiscs(Vec2 const& start, Vec2 const& forwardNormal, float distance
	, Vec2 discCenter, float discRadius) {
	
	RaycastResult2D result;
	Vec2 i = forwardNormal;
	Vec2 j = i.GetRotated90Degrees();
	Vec2 startToCenter = discCenter - start;

	float startToCenterJ = DotProduct2D(startToCenter, j);
	if (startToCenterJ > discRadius || startToCenterJ < -discRadius) {
		return result;
	}
	float startToCenterI = DotProduct2D(startToCenter, i);
	if (startToCenterI > (distance + discRadius) || startToCenterI < -discRadius) {
		return result;
	}

		
	result.m_start = start;
	result.m_direction = i;
	result.m_distance = distance;
	if (IsPointInsideDisc2D(start, discCenter, discRadius)) {
		result.m_didImpact = true;
		result.m_impactDistance = 0.f;
		result.m_impactPosition = start;
		result.m_impactFraction = 0.f;
		result.m_impactSurfaceNormal = i * -1.f;
	}

	float halfDistanceInSideEntity = sqrtf(discRadius * discRadius - startToCenterJ * startToCenterJ);
	float impactDistance = startToCenterI - halfDistanceInSideEntity;
	if (impactDistance < 0.f) {
		return result;
	}

	else {
		result.m_impactFraction = impactDistance / distance;
		result.m_didImpact = true;
		result.m_impactDistance = impactDistance;
		result.m_impactPosition = start + i * result.m_impactDistance;
		result.m_impactSurfaceNormal = (result.m_impactPosition - discCenter).GetNormalized();
	}

	return result;
}



//-----------------------------------------------------------------------------------------------
RaycastResult2D RaycastAABB2D(Vec2 const& start, Vec2 const& forwardNormal, float distance
	, AABB2 const& box) {
	Vec2 end = start + forwardNormal * distance;

	RaycastResult2D result;

	//handle special condition
	if (start == end && box.IsPointInside(start)) {
		result.m_start = start;
		result.m_direction = forwardNormal;
		result.m_distance = distance;
		result.m_didImpact = true;
		result.m_impactDistance = 0.f;
		result.m_impactPosition = start;
		result.m_impactFraction = 0.f;
		result.m_impactSurfaceNormal = forwardNormal * -1.f;
		return result;
	}

	FloatRange rayXRange(start.x, end.x);
	FloatRange rayYRange(start.y, end.y);

	FloatRange aabbXRange = FloatRange(box.m_mins.x, box.m_maxs.x);
	FloatRange aabbYRange = FloatRange(box.m_mins.y, box.m_maxs.y);
	FloatRange xOverlapping = aabbXRange.GetOverlappingPart(rayXRange);
	FloatRange yOverlapping = aabbYRange.GetOverlappingPart(rayYRange);
	if (xOverlapping == FloatRange::ZERO || yOverlapping == FloatRange::ZERO) {
		return result;
	}
	xOverlapping.m_min = (xOverlapping.m_min - start.x) / (end.x - start.x);
	xOverlapping.m_max = (xOverlapping.m_max - start.x) / (end.x - start.x);
	yOverlapping.m_min = (yOverlapping.m_min - start.y) / (end.y - start.y);
	yOverlapping.m_max = (yOverlapping.m_max - start.y) / (end.y - start.y);

	FloatRange xyOverlapping = xOverlapping.GetOverlappingPart(yOverlapping);
	if (xyOverlapping == FloatRange::ZERO) {
		return result;
	}
	result.m_start = start;
	result.m_direction = forwardNormal;
	result.m_distance = distance;
	result.m_didImpact = true;
	result.m_impactDistance = xyOverlapping.m_min * distance;
	result.m_impactFraction = xyOverlapping.m_min;
	result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
	if (box.IsPointInside(start)) {
		result.m_impactSurfaceNormal = forwardNormal * -1.f;
	}
	else if (abs(result.m_impactPosition.x - aabbXRange.m_min) < 0.001f) {
		result.m_impactSurfaceNormal = Vec2(-1.f, 0.f);
	}
	else if (abs(result.m_impactPosition.x - aabbXRange.m_max) < 0.001f) {
		result.m_impactSurfaceNormal = Vec2(1.f, 0.f);
	}
	else if (abs(result.m_impactPosition.y - aabbYRange.m_min) < 0.001f) {
		result.m_impactSurfaceNormal = Vec2(0.f, -1.f);
	}
	else if (abs(result.m_impactPosition.y - aabbYRange.m_max) < 0.001f) {
		result.m_impactSurfaceNormal = Vec2(0.f, 1.f);
	}
	return result;

}


//-----------------------------------------------------------------------------------------------
RaycastResult3D RaycastSphere(Vec3 const& start, Vec3 const& forwardNormal, float distance,
	Vec3 sphereCenter, float radius) {

	RaycastResult3D result;
	Vec3 i = forwardNormal;

	Vec3 startToCenter = sphereCenter - start;
	Vec3 startToCenterI = GetProjectedOnto3D(startToCenter, i);
	float startToCenterILegnth = DotProduct3D(startToCenter, i);
	if (startToCenterILegnth > (distance + radius) || startToCenterILegnth < -radius) {
		return result;
	}

	Vec3 startToCenterJK = startToCenter - startToCenterI;
	if (startToCenterJK.GetLengthSquared() > radius * radius) {
		return result;
	}

	result.m_start = start;
	result.m_direction = i;
	result.m_distance = distance;
	if (IsPointInsideSphere(start, sphereCenter, radius)) {
		result.m_didImpact = true;
		result.m_impactDistance = 0.f;
		result.m_impactPosition = start;
		result.m_impactFraction = 0.f;
		result.m_impactSurfaceNormal = i * -1.f;
		return result;
	}

	float halfDistanceInSideEntity = sqrtf(radius * radius - startToCenterJK.GetLengthSquared());
	float impactDistance = startToCenterI.GetLength() - halfDistanceInSideEntity;
	if (impactDistance < 0.f || impactDistance > distance) {
		return result;
	}

	else {
		result.m_impactFraction = impactDistance / distance;
		result.m_didImpact = true;
		result.m_impactDistance = impactDistance;
		result.m_impactPosition = start + i * result.m_impactDistance;
		result.m_impactSurfaceNormal = (result.m_impactPosition - sphereCenter).GetNormalized();
		return result;
	}
}



RaycastResult3D RaycastAABB3D(Vec3 const& start, Vec3 const& forwardNormal, float distance, AABB3 const& box) {
	Vec3 end = start + forwardNormal * distance;
	
	RaycastResult3D result;
	if (start == end && IsPointInsideAABB3D(start, box)) {
			
		result.m_start = start;
		result.m_direction = forwardNormal;
		result.m_distance = distance;
		result.m_didImpact = true;
		result.m_impactDistance = 0.f;
		result.m_impactPosition = start;
		result.m_impactFraction = 0.f;
		result.m_impactSurfaceNormal = forwardNormal * -1.f;
		return result;
	}

	FloatRange rayXRange(start.x, end.x);
	FloatRange rayYRange(start.y, end.y);
	FloatRange rayZRange(start.z, end.z);

	FloatRange aabbXRange = FloatRange(box.m_mins.x, box.m_maxs.x);
	FloatRange aabbYRange = FloatRange(box.m_mins.y, box.m_maxs.y);
	FloatRange aabbZRange = FloatRange(box.m_mins.z, box.m_maxs.z);
	FloatRange xOverlapping = aabbXRange.GetOverlappingPart(rayXRange);
	FloatRange yOverlapping = aabbYRange.GetOverlappingPart(rayYRange);
	FloatRange zOverlapping = aabbZRange.GetOverlappingPart(rayZRange);
	if (xOverlapping == FloatRange::ZERO || yOverlapping == FloatRange::ZERO || yOverlapping == FloatRange::ZERO) {
		return result;
	}
	xOverlapping.m_min = (xOverlapping.m_min - start.x) / (end.x - start.x);
	xOverlapping.m_max = (xOverlapping.m_max - start.x) / (end.x - start.x);
	yOverlapping.m_min = (yOverlapping.m_min - start.y) / (end.y - start.y);
	yOverlapping.m_max = (yOverlapping.m_max - start.y) / (end.y - start.y);
	zOverlapping.m_min = (zOverlapping.m_min - start.z) / (end.z - start.z);
	zOverlapping.m_max = (zOverlapping.m_max - start.z) / (end.z - start.z);

	FloatRange xyOverlapping = xOverlapping.GetOverlappingPart(yOverlapping);
	if (xyOverlapping == FloatRange::ZERO) {
		return result;
	}
	FloatRange xyzOverlapping = xyOverlapping.GetOverlappingPart(zOverlapping);
	if (xyzOverlapping == FloatRange::ZERO) {
		return result;
	}
	result.m_start = start;
	result.m_direction = forwardNormal;
	result.m_distance = distance;
	result.m_didImpact = true;
	result.m_impactDistance = xyzOverlapping.m_min * distance;
	result.m_impactFraction = xyzOverlapping.m_min;
	result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
	if (IsPointInsideAABB3D(start, box)) {
		result.m_impactSurfaceNormal = forwardNormal * -1.f;
	}
	else if (abs(result.m_impactPosition.x - aabbXRange.m_min) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(-1.f, 0.f, 0.f);
	}
	else if (abs(result.m_impactPosition.x - aabbXRange.m_max) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(1.f, 0.f, 0.f);
	}
	else if (abs(result.m_impactPosition.y - aabbYRange.m_min) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(0.f, -1.f, 0.f);
	}
	else if (abs(result.m_impactPosition.y - aabbYRange.m_max) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(0.f, 1.f, 0.f);
	}
	else if (abs(result.m_impactPosition.z - aabbZRange.m_min) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(0.f, 0.f, -1.f);
	}
	else if (abs(result.m_impactPosition.z - aabbZRange.m_max) < 0.001f) {
		result.m_impactSurfaceNormal = Vec3(0.f, 0.f, 1.f);
	}
	return result;
}


//-----------------------------------------------------------------------------------------------
RaycastResult3D RaycastCylinder(Vec3 const& start, Vec3 const& forwardNormal, float distance
	, Vec3 cylinderPos, FloatRange cylinderRangeZ, float radius){
	RaycastResult3D result;

	Vec2 start2D(start.x, start.y);
	Vec2 i(forwardNormal.x, forwardNormal.y);
	Vec2 j = i.GetRotated90Degrees();
	float xyDistance = (i * distance).GetLength();

	Vec2 center(cylinderPos.x, cylinderPos.y);
	Vec2 startToCenter = center - start2D;

	float startToCenterJ = GetProjectedLength2D(startToCenter, j);
	if (startToCenterJ > radius || startToCenterJ < -radius) {
		return result;
	}
	float startToCenterI = GetProjectedLength2D(startToCenter, i);
	if (startToCenterI > (distance + radius) || startToCenterI < -radius) {
		return result;
	}

	result.m_start = start;
	result.m_direction = forwardNormal;
	result.m_distance = distance;
	if (IsPointInsideDisc2D(start2D, center, radius)) {
		if (cylinderRangeZ.IsOnRange(start.z)) {
			result.m_didImpact = true;
			result.m_impactDistance = 0.f;
			result.m_impactPosition = start;
			result.m_impactFraction = 0.f;
			result.m_impactSurfaceNormal = forwardNormal * -1.f;
			return result;
		}
		else if (start.z > cylinderRangeZ.m_max && forwardNormal.z <= 0.f) {
			float tMaxZ = (cylinderRangeZ.m_max - start.z) / forwardNormal.z;
			Vec2 positionMaxZ = start2D + tMaxZ * i;
			if (IsPointInsideDisc2D(positionMaxZ, center, radius)) {
				result.m_impactFraction = tMaxZ / distance;
				result.m_didImpact = true;
				result.m_impactDistance = result.m_impactFraction * distance;
				result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
				result.m_impactSurfaceNormal = Vec3(0.f, 0.f, 1.f);
				return result;
			}
		}
		else if (start.z < cylinderRangeZ.m_min && forwardNormal.z >= 0.f) {
			float tMinZ = (cylinderRangeZ.m_min - start.z) / forwardNormal.z;
			Vec2 positionMinZ = start2D + tMinZ * i;
			if (IsPointInsideDisc2D(positionMinZ, center, radius)) {
				result.m_impactFraction = tMinZ / distance;
				result.m_didImpact = true;
				result.m_impactDistance = result.m_impactFraction * distance;
				result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
				result.m_impactSurfaceNormal = Vec3(0.f, 0.f, -1.f);
				return result;
			}
		}
	}

	float halfDistanceInSideEntity = sqrtf(radius * radius - startToCenterJ * startToCenterJ);
	float impactDistance = startToCenterI - halfDistanceInSideEntity;
	if (impactDistance < 0.f) {
		return result;
	}

	else {
		Vec3 impactPosition = start + forwardNormal * distance * impactDistance / xyDistance;
		if (impactPosition.z > cylinderRangeZ.m_max) {
			if (forwardNormal.z < 0.f) {
				float tMaxZ = (cylinderRangeZ.m_max - start.z) / forwardNormal.z;
				Vec2 positionMaxZ = start2D + tMaxZ * i;
				if (IsPointInsideDisc2D(positionMaxZ, center, radius)) {
					result.m_impactFraction = tMaxZ / distance;
					result.m_didImpact = true;
					result.m_impactDistance = result.m_impactFraction * distance;
					result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
					result.m_impactSurfaceNormal = Vec3(0.f, 0.f, 1.f);
				}
			}
			return result;
		}
		if (impactPosition.z < cylinderRangeZ.m_min) {
			if (forwardNormal.z > 0.f) {
				float tMinZ = (cylinderRangeZ.m_min - start.z) / forwardNormal.z;
				Vec2 positionMinZ = start2D + tMinZ * i;
				if (IsPointInsideDisc2D(positionMinZ, center, radius)) {
					result.m_impactFraction = tMinZ / distance;
					result.m_didImpact = true;
					result.m_impactDistance = result.m_impactFraction * distance;
					result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
					result.m_impactSurfaceNormal = Vec3(0.f, 0.f, -1.f);
				}
			}
			return result;
		}
		result.m_impactFraction = impactDistance / xyDistance;
		result.m_didImpact = true;
		result.m_impactDistance = result.m_impactFraction * distance;
		result.m_impactPosition = start + forwardNormal * result.m_impactDistance;
		Vec3 impactNormal3D = result.m_impactPosition - cylinderPos;
		result.m_impactSurfaceNormal = Vec3(impactNormal3D.x, impactNormal3D.y,
			0.f).GetNormalized();
		return result;
	}
}


//-----------------------------------------------------------------------------------------------
void ReflectFixedObject2D(Vec2& velocity, Vec2 const& surfaceNormal, float eA, float eB){
	float lengthOnN = DotProduct2D(velocity, surfaceNormal);
	if(lengthOnN < 0.f){
		Vec2 projectedN = lengthOnN *surfaceNormal;
		Vec2 projectedT = velocity - projectedN;
		projectedN *= eA * eB;
		velocity = Vec2(projectedT.x - projectedN.x, projectedT.y - projectedN.y);
	}
}


//-----------------------------------------------------------------------------------------------
void BounceDiscOffDisc2D(Vec2& aCenter, float aRadius, Vec2& velocityA, Vec2& bCenter, 
	float bRadius, Vec2& velocityB, float eA, float eB){
	Vec2 displacement = bCenter - aCenter;
	float displacementLength = displacement.GetLength();
	float distanceBetweenTwoDisc = displacementLength - (aRadius + bRadius);
	if (displacementLength < (aRadius + bRadius)) {
		Vec2 pushNormal = displacement.GetNormalized();
		if (DotProduct2D(velocityA, displacement) > DotProduct2D(velocityB, displacement)) {
			Vec2 projectedAToN = DotProduct2D(velocityA, pushNormal) * pushNormal;
			Vec2 projectedBToN = DotProduct2D(velocityB, pushNormal) * pushNormal;

			Vec2 projectedAToT = velocityA - projectedAToN;
			Vec2 projectedBToT = velocityB - projectedBToN;

			velocityA = projectedAToT + projectedBToN * eA * eB;
			velocityB = projectedBToT + projectedAToN * eA * eB;
		}
		
		aCenter -= pushNormal * fabsf(distanceBetweenTwoDisc * 0.5f);
		bCenter += pushNormal * fabsf(distanceBetweenTwoDisc * 0.5f);
	}
}


//-----------------------------------------------------------------------------------------------
void ChangeMomentumDiscs2D(Vec2& velocityA, Vec2& velocityB, Vec2 const& surface, 
	float eA, float eB){
	if(DotProduct2D(velocityA, surface) > DotProduct2D(velocityB, surface)){
		Vec2 surfaceNormal = surface.GetNormalized();
		Vec2 projectedAToN = DotProduct2D(velocityA, surfaceNormal) * surfaceNormal;
		Vec2 projectedBToN = DotProduct2D(velocityB, surfaceNormal) * surfaceNormal;
		
		Vec2 projectedAToT = velocityA - projectedAToN;
		Vec2 projectedBToT = velocityB - projectedBToN;

		velocityA = projectedAToT + projectedBToN * eA * eB;
		velocityB = projectedBToT + projectedAToN * eA * eB;
	}
}


//-----------------------------------------------------------------------------------------------
void BounceDiscOffPoint2D(Vec2& discCenter, float bounceDistance, Vec2& velocity, 
	Vec2 const& bouncePosition, float eA, float eB){
	Vec2 displacement = discCenter - bouncePosition;
	if(displacement.GetLengthSquared() > bounceDistance * bounceDistance){
		return;
	}
	Vec2 bounceNormal = displacement.GetNormalized();
	discCenter = bouncePosition + bounceNormal * bounceDistance;
	float lengthOnN = DotProduct2D(velocity, bounceNormal);
	if (lengthOnN < 0.f) {
		Vec2 projectedN = lengthOnN * bounceNormal;
		Vec2 projectedT = velocity - projectedN;
		projectedN *= eA * eB;
		velocity = Vec2(projectedT.x - projectedN.x, projectedT.y - projectedN.y);
	}
}


//-----------------------------------------------------------------------------------------------
RaycastResult2D RaycastLineSegment2D(Vec2 const& start, Vec2 const& forwardNormal,
	float distance, LineSegment2 const& lineSeg) {
	RaycastResult2D result;
	if (distance == 0.f) {
		return result;
	}

	Vec2 i = forwardNormal;
	Vec2 j = i.GetRotated90Degrees();
	
	Vec2 startToA = lineSeg.m_start - start;
	Vec2 startToB = lineSeg.m_end - start;

	float Aj = DotProduct2D(startToA, j);
	float Bj = DotProduct2D(startToB, j);
	if ((Aj > 0.f && Bj > 0.f) || (Aj < 0.f && Bj < 0.f)) {
		return result;
	}

	float fraction = Aj / (Aj - Bj);
	float Ai = DotProduct2D(startToA, i);
	float Bi = DotProduct2D(startToB, i);
	float hitPointI = Ai + fraction * (Bi - Ai);
	if (hitPointI < 0.f || hitPointI > distance) {
		return result;
	}

	result.m_start = start;
	result.m_direction = forwardNormal;
	result.m_distance = distance;
	result.m_didImpact = true;
	result.m_impactDistance = hitPointI;
	result.m_impactFraction = hitPointI / distance;
	result.m_impactPosition = start + hitPointI * forwardNormal;
	Vec2 aToB = lineSeg.m_end - lineSeg.m_start;
	result.m_impactSurfaceNormal = aToB.GetRotated90Degrees().GetNormalized();
	if (DotProduct2D(result.m_impactSurfaceNormal, i) > 0.f) {
		result.m_impactSurfaceNormal *= -1.f;
	}
	return result;

}


//-----------------------------------------------------------------------------------------------
// transforms are applied in scale, rotate, and translate order
void TransformPosition2D(Vec2& posToTransform, float uniformScaleXY,
	float rotationDegrees, Vec2 const& translation) {
	// calculate radian coordinates
	float r = posToTransform.GetLength();
	float thetaDegrees = Atan2Degrees(posToTransform.y, posToTransform.x);

	// scale first
	r *= uniformScaleXY;

	// add degrees
	thetaDegrees += rotationDegrees;

	// convert radians to Cartesian for rotation
	posToTransform.x = r * CosDegrees(thetaDegrees);
	posToTransform.y = r * SinDegrees(thetaDegrees);

	//translation
	posToTransform.x += translation.x;
	posToTransform.y += translation.y;
	
}

//-----------------------------------------------------------------------------------------------
void TransformPosition2D(Vec2& posToTransform, Vec2 const& iBasis, Vec2 const& jBasis, 
	Vec2 const& translation){
	Vec2 pointIJ = posToTransform.x * iBasis + posToTransform.y * jBasis;
	posToTransform = pointIJ + translation;
}

//-----------------------------------------------------------------------------------------------
// transforms applied for 3D vectors
void TransformPositionXY3D(Vec3& posToTransform, float uniformScaleXY,
	float rotationDegrees, Vec2 const& translationXY) {
	// calculate radian coordinates
	float r = sqrtf(posToTransform.x * posToTransform.x + posToTransform.y * posToTransform.y);
	float thetaDegrees = Atan2Degrees(posToTransform.y, posToTransform.x);

	// scale first
	r *= uniformScaleXY;

	// add degrees
	thetaDegrees += rotationDegrees;

	// convert radians to Cartesian for rotation
	posToTransform.x = r * CosDegrees(thetaDegrees);
	posToTransform.y = r * SinDegrees(thetaDegrees);

	//translation
	posToTransform.x += translationXY.x;
	posToTransform.y += translationXY.y;
}


//-----------------------------------------------------------------------------------------------
void TransformPositionXY3D(Vec3& posToTransform, Vec2 const& iBasis, Vec2 const& jBasis, 
	Vec2 const& translationXY){
	Vec2 pointIJ = posToTransform.x * iBasis + posToTransform.y * jBasis;
	posToTransform = Vec3(pointIJ.x + translationXY.x, pointIJ.y + translationXY.y, posToTransform.z);
}


//-----------------------------------------------------------------------------------------------
void TransformVertexArrayXY3D(int numVerts, Vertex_PCU* verts, float uniformScaleXY,
	float rotationDegreesAboutZ, Vec2 const& translationXY) {
	// loop the entire array
	for (int vertIndex = 0; vertIndex < numVerts; vertIndex++) {
		Vec3& pos = verts[vertIndex].m_position;
		TransformPositionXY3D(pos, uniformScaleXY, rotationDegreesAboutZ, translationXY);
	}
}


//-----------------------------------------------------------------------------------------------
// calculate value in range[start,end] using equation (start + fraction*( end - start))
float Interpolate(float start, float end, float fractionValue) {
	return (start + fractionValue * (end - start));
}


//-----------------------------------------------------------------------------------------------
// calculate fraction of value in this range using the equation (value - start) / (end - start)
float GetFractionWithin(float value, float start, float end) {
	float range = end - start;

	// handle condition of 0 range
	if (range == 0) {
		if (value < start) {
			return 0.f;
		}
		else {
			return 1.f;
		}
	}

	return (value - start) / range;
}


//-----------------------------------------------------------------------------------------------
// calculate related value in different range using interpolate and getfractionwithin functions
float RangeMap(float value, float start, float end, float outputStart, float outputEnd) {
	// first get the fraction of value in its range
	float fraction = GetFractionWithin(value, start, end);
	// based on the fraction, calculate corresponding value in another range using the fraction value
	return Interpolate(outputStart, outputEnd, fraction);
}


//-----------------------------------------------------------------------------------------------
// calculate related clamped value in different range using interpolate and getfractionwithin functions
float RangeMapClamped(float value, float start, float end, float outputStart, float outputEnd) {
	// first get the fraction of value in its range
	float fraction = GetFractionWithin(value, start, end);

	// if the value is out of range, return clamped values instead
	if (fraction <= 0.f) {
		return outputStart;
	}
	else if (fraction >= 1.f) {
		return outputEnd;
	}
	// based on the fraction, calculate corresponding value in another range using the fraction value
	return Interpolate(outputStart, outputEnd, fraction);
}


//-----------------------------------------------------------------------------------------------
float Clamp(float value, float start, float end) {
	// if the value is out of range, return clamped values
	if (value < start) {
		return start;
	}
	else if (value > end) {
		return end;
	}
	// else just return the original value
	else {
		return value;
	}
}


//-----------------------------------------------------------------------------------------------
int Clamp(int value, int start, int end){
	// if the value is out of range, return clamped values
	if (value < start) {
		return start;
	}
	else if (value > end) {
		return end;
	}
	// else just return the original value
	else {
		return value;
	}
}


double Clamp(double value, double start, double end)
{
	// if the value is out of range, return clamped values
	if (value < start) {
		return start;
	}
	else if (value > end) {
		return end;
	}
	// else just return the original value
	else {
		return value;
	}
}

//-----------------------------------------------------------------------------------------------
float ClampZeroToOne(float value) {
	if (value < 0.f) {
		return 0.f;
	}
	else if (value > 1.f) {
		return 1.f;
	}
	else {
		return value;
	}
}


//-----------------------------------------------------------------------------------------------
int RoundDownToInt(float value) {
	return static_cast<int>(floor(value));
}


//-----------------------------------------------------------------------------------------------
float SmoothStep3(float inputZeroToOne){
	return 3 * (inputZeroToOne* inputZeroToOne) - 2 * (inputZeroToOne* inputZeroToOne* inputZeroToOne);
}


//-----------------------------------------------------------------------------------------------
float GetAngularDispDegrees(float startDegree, float endDegree) {
	return endDegree - startDegree;
}


//-----------------------------------------------------------------------------------------------
float GetShortestAngularDispDegrees(float startDegree, float endDegree) {
	float angularDispDegrees = endDegree - startDegree;

	// if degree is larger than 180 or smaller than -180, rotate it 360
	while (angularDispDegrees > 180.f) {
		angularDispDegrees -= 360.f;
	}
	while (angularDispDegrees < -180.f) {
		angularDispDegrees += 360.f;
	}

	return angularDispDegrees;
}


//-----------------------------------------------------------------------------------------------
// Turn to end degress based on max delta degrees
float GetTurnedTowardDegrees(float startDegree, float endDegree, float maxDeltaDegrees) {
	// first get the shortest angular degrees
	float shortest_angular_disp_degrees = GetShortestAngularDispDegrees(startDegree, endDegree);

	// if it is close to end degrees and next move will exceed end degree, just return end degree
	if (shortest_angular_disp_degrees <= maxDeltaDegrees 
		&& shortest_angular_disp_degrees >= -maxDeltaDegrees) {
		return endDegree;
	}
	// move angle
	if (shortest_angular_disp_degrees > 0.f) {
		return startDegree + maxDeltaDegrees;
	}
	else {
		return startDegree - maxDeltaDegrees;
	}

}


//-----------------------------------------------------------------------------------------------
int GetTaxicabDistance2D(IntVec2 const& pointA, IntVec2 const& pointB)
{
	return abs(pointB.x - pointA.x) + abs(pointB.y - pointA.y);
}


//-----------------------------------------------------------------------------------------------
float DotProduct2D(Vec2 const& pointA, Vec2 const& pointB) {
	return (pointA.x * pointB.x) + (pointA.y * pointB.y);
}


//-----------------------------------------------------------------------------------------------
float DotProduct3D(Vec3 const& pointA, Vec3 const& pointB){
	return (pointA.x * pointB.x) + (pointA.y * pointB.y) + (pointA.z * pointB.z);
}


//-----------------------------------------------------------------------------------------------
float DotProduct4D(Vec4 const& a, Vec4 const& b){
	return (a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w);
}


//-----------------------------------------------------------------------------------------------
float CrossProduct2D(Vec2 const& pointA, Vec2 const& pointB){
	return pointA.x* pointB.y - pointA.y * pointB.x;
}


//-----------------------------------------------------------------------------------------------
Vec3 CrossProduct3D(Vec3 const& pointA, Vec3 const& pointB){
	Vec3 result;
	result.x = pointA.y * pointB.z - pointA.z * pointB.y;
	result.y = pointA.z * pointB.x - pointA.x * pointB.z;
	result.z = pointA.x * pointB.y - pointA.y * pointB.x;
	return result;
}


//-----------------------------------------------------------------------------------------------
float GetProjectedLength2D(Vec2 const& vectorToProject, Vec2 const& vectorToProjectOnto){
	return DotProduct2D(vectorToProject, vectorToProjectOnto.GetNormalized());
}


//-----------------------------------------------------------------------------------------------
Vec3 const GetProjectedOnto3D(Vec3 const& vectorToProject, Vec3 const& vectorToProjectOnto){
	Vec3 unnormalizedProjectedVector =
		DotProduct3D(vectorToProject, vectorToProjectOnto) * vectorToProjectOnto;
	return  unnormalizedProjectedVector / vectorToProjectOnto.GetLengthSquared();
}


//-----------------------------------------------------------------------------------------------
// calculate the projected vector using better method: (A ` B) * B/ |B|^2
// where B is not normalized vector
Vec2 const GetProjectedOnto2D(Vec2 const& vectorToProject, Vec2 const& vectorToProjectOnto)
{
	Vec2 unnormalizedProjectedVector = 
		DotProduct2D(vectorToProject, vectorToProjectOnto) * vectorToProjectOnto;
	return  unnormalizedProjectedVector / vectorToProjectOnto.GetLengthSquared();
}

//-----------------------------------------------------------------------------------------------
// calculate angles between two vectors using dot product equation: a`b = |a||b| cos(theta)
// where theta = arccos(a`b/|a||b|)
float GetAngleDegreesBetweenVectors2D(Vec2 const& a, Vec2 const& b)
{
	float dotProductAB = DotProduct2D(a, b);
	float cosTheta = dotProductAB / (a.GetLength() * b.GetLength());
	cosTheta = Clamp(cosTheta, -1.f, 1.f);
	return ArcCosDegrees(cosTheta);
}


//-----------------------------------------------------------------------------------------------
float GetAngleDegreesBetweenVectors3D(Vec3 const& a, Vec3 const& b){
	float dotProductAB = DotProduct3D(a, b);
	float cosTheta = dotProductAB / (a.GetLength() * b.GetLength());
	cosTheta = Clamp(cosTheta, -1.f, 1.f);
	return ArcCosDegrees(cosTheta);
}


//-----------------------------------------------------------------------------------------------
float NormalizeByte(unsigned char byteValue){
	float byteValueFloat = static_cast<float>(byteValue);
	return RangeMapClamped(byteValueFloat, 0.f, 255.f, 0.f, 1.f);
}


//-----------------------------------------------------------------------------------------------
unsigned char DenormalizeByte(float zeroToOne){
	if (zeroToOne == 1.f) {
		return 255;
	}
	float floatValueByte = RangeMapClamped(zeroToOne, 0.f, 1.f, 0.f, 255.f);
	return static_cast<unsigned char>(floatValueByte * 256.f / 255.f);
}

//-----------------------------------------------------------------------------------------------
int Log2Int(int number) {
	int result = 0;
	while (number >>= 1) {
		result++;
	}
	return result;
}