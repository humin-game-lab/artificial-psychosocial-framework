#include "Engine/Math/IntVec3.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Core/StringUtils.hpp"
#include <math.h>

//-----------------------------------------------------------------------------------------------
IntVec3::~IntVec3() {

}


//-----------------------------------------------------------------------------------------------
IntVec3::IntVec3() {

}


//-----------------------------------------------------------------------------------------------
IntVec3::IntVec3(const IntVec3& copyFrom) {
	x = copyFrom.x;
	y = copyFrom.y;
	z = copyFrom.z;
}


//-----------------------------------------------------------------------------------------------
IntVec3::IntVec3(int initialX, int initialY, int initialZ) {
	x = initialX;
	y = initialY;
	z = initialZ;
}


//-----------------------------------------------------------------------------------------------
float IntVec3::GetLength() const {
	double double_x = static_cast<double>(x);
	double double_y = static_cast<double>(y);
	double double_z = static_cast<double>(z);
	return static_cast<float>(sqrt(double_x * double_x + double_y * double_y + double_z*double_z));
}


//-----------------------------------------------------------------------------------------------
int IntVec3::GetLengthSquared() const {
	return (x * x + y * y + z*z);
}


//-----------------------------------------------------------------------------------------------
int IntVec3::GetTaxicabLength() const {
	return abs(x) + abs(y) + abs(z);
}


//-----------------------------------------------------------------------------------------------
void IntVec3::operator-=(IntVec3 const& intVecToSubtract){
	x -= intVecToSubtract.x;
	y -= intVecToSubtract.y;
	z -= intVecToSubtract.z;
}


//-----------------------------------------------------------------------------------------------
IntVec3 const IntVec3::operator-(IntVec3 const& intVecToSubtract){
	return IntVec3(x - intVecToSubtract.x, y - intVecToSubtract.y, z - intVecToSubtract.z);
}


//-----------------------------------------------------------------------------------------------
void IntVec3::operator+=(IntVec3 const& intVecToAdd) {
	x += intVecToAdd.x;
	y += intVecToAdd.y;
	z += intVecToAdd.z;
}

//-----------------------------------------------------------------------------------------------
IntVec3 const IntVec3::operator+(IntVec3 const& intVecToAdd){
	return IntVec3(x + intVecToAdd.x, y + intVecToAdd.y, z + intVecToAdd.z);
}


//-----------------------------------------------------------------------------------------------
bool IntVec3::operator==(IntVec3 const& compare) const {
	if (x == compare.x && y == compare.y && z == compare.z) {
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool IntVec3::operator!=(IntVec3 const& compare) const {
	if (x != compare.x || y != compare.y || z != compare.z) {
		return true;
	}
	return false;
}