#pragma once
#include "Engine/Math/Vec2.hpp"

//-----------------------------------------------------------------------------------------------
struct OBB2
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec2 m_iBasisNormal;
	Vec2 m_halfDimensions;
	Vec2 m_center;

public:
	OBB2();
	explicit OBB2(Vec2 const& iBasisNormal, Vec2 const& halfDimensions, Vec2 const& center);

	void GetCornerPoints(Vec2* out_fourCornerWorldPositions) const;
	Vec2 const GetLocalPosForWorldPos(Vec2 const& worldPos) const;
	Vec2 const GetWorldPosForLocalPos(Vec2 const& localPos) const;
	void RotateAboutCenter(float rotationDeltaDegrees);
};


