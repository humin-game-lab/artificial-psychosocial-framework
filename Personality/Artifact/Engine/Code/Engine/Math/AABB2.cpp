#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/MathUtils.hpp"


const AABB2 AABB2::ZERO_TO_ONE(Vec2::ZERO, Vec2::ONE);

//-----------------------------------------------------------------------------------------------
AABB2::~AABB2() {

}


//-----------------------------------------------------------------------------------------------
AABB2::AABB2() {

}


//-----------------------------------------------------------------------------------------------
AABB2::AABB2(const AABB2& copyFrom) {
	m_mins = copyFrom.m_mins;
	m_maxs = copyFrom.m_maxs;
}


//-----------------------------------------------------------------------------------------------
AABB2::AABB2(Vec2 const& initialMins, Vec2 const& initialMaxs) {
	m_mins = initialMins;
	m_maxs = initialMaxs;
}


//-----------------------------------------------------------------------------------------------
AABB2::AABB2(float minX, float minY, float maxX, float maxY) {
	m_mins = Vec2(minX, minY);
	m_maxs = Vec2(maxX, maxY);
}


//-----------------------------------------------------------------------------------------------
bool AABB2::IsPointInside(Vec2 const& point) const {
	if ((point.x > m_mins.x && point.y > m_mins.y) 
		&& (point.x < m_maxs.x && point.y < m_maxs.y)) {
		return true;
	}
	else {
		return false;
	}
}


//-----------------------------------------------------------------------------------------------
Vec2 const AABB2::GetCenter() const {
	float centerX = (m_mins.x + m_maxs.x) * 0.5f;
	float centerY = (m_mins.y + m_maxs.y) * 0.5f;
	return Vec2(centerX, centerY);
}


//-----------------------------------------------------------------------------------------------
Vec2 const AABB2::GetDimensions() const {
	return Vec2(m_maxs.x - m_mins.x, m_maxs.y - m_mins.y);
}


//-----------------------------------------------------------------------------------------------
Vec2 const AABB2::GetNearestPoint(Vec2 const& point) const {
	// using clamp to get the point
	// if point is inside, clamp function will return original point values
	// else if point is outside, the clamp will return clamped values based on the min and max
	Vec2 returnPoint(0.f,0.f);
	returnPoint.x = Clamp(point.x, m_mins.x, m_maxs.x);
	returnPoint.y = Clamp(point.y, m_mins.y, m_maxs.y);

	return returnPoint;
}


//-----------------------------------------------------------------------------------------------
Vec2 const AABB2::GetPointAtUV(Vec2 const& uv) const {
	float x = Interpolate(m_mins.x, m_maxs.x, uv.x);
	float y = Interpolate(m_mins.y, m_maxs.y, uv.y);
	return Vec2(x, y);
}


//-----------------------------------------------------------------------------------------------
Vec2 const AABB2::GetUVForPoint(Vec2 const& point) const {
	float u = GetFractionWithin(point.x, m_mins.x, m_maxs.x);
	float v = GetFractionWithin(point.y, m_mins.y, m_maxs.y);
	return Vec2(u, v);
}


//-----------------------------------------------------------------------------------------------
void AABB2::Translate(Vec2 const& displacement) {
	m_mins += displacement;
	m_maxs += displacement;
}


//-----------------------------------------------------------------------------------------------
void AABB2::SetCenter(Vec2 const& newCenter) {
	Vec2 center = GetCenter();
	Vec2 centerToMinDisplacement = center - m_mins;
	Vec2 centerToMaxDispalcement = center - m_maxs;

	m_mins = newCenter - centerToMinDisplacement;
	m_maxs = newCenter - centerToMaxDispalcement;
}


//-----------------------------------------------------------------------------------------------
void AABB2::SetDimensions(Vec2 const& newDimension) {
	// get the center
	Vec2 center = GetCenter();

	// get half lengths of new dimension
	float halfXDimension = newDimension.x * 0.5f;
	float halfYDimension = newDimension.y * 0.5f;

	// calculate new min and max
	m_mins = Vec2(center.x - halfXDimension, center.y - halfYDimension);
	m_maxs = Vec2(center.x + halfXDimension, center.y + halfYDimension);

}


//-----------------------------------------------------------------------------------------------
void AABB2::StretchToIncludePoint(Vec2 const& point) {
	if (!IsPointInside(point)) {
		if (point.x < m_mins.x) {
			m_mins.x = point.x;
		}
		else if (point.x > m_maxs.x) {
			m_maxs.x = point.x;
		}

		if (point.y < m_mins.y) {
			m_mins.y = point.y;
		}
		else if (point.y > m_maxs.y) {
			m_maxs.y = point.y;
		}
	}
}

