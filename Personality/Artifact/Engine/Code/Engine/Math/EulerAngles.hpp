#pragma once

//-----------------------------------------------------------------------------------------------
struct Vec3;
struct Mat44;

//-----------------------------------------------------------------------------------------------
struct EulerAngles
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	EulerAngles() = default;
	EulerAngles(float yawDegrees, float pitchDegrees, float rollDegrees);
	EulerAngles(Vec3 const anglesYallPitchRoll);
	void GetVectors_XFwd_YLeft_ZUp(Vec3& out_forwardIBasis, Vec3& out_leftJBasis, Vec3& out_upKBasis) const;
	Mat44 GetMatrix__XFwd_YLeft_ZUp() const;
	Vec3 GetAngles() const;
	Vec3 GetAnglesPYR() const;

public:
	float m_rollDegrees = 0.f;
	float m_pitchDegrees = 0.f;
	float m_yawDegrees = 0.f;
};


