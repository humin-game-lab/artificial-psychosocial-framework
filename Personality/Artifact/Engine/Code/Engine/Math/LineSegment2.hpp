#pragma once
#include "Engine/Math/Vec2.hpp"

//-----------------------------------------------------------------------------------------------
struct LineSegment2
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	Vec2 m_start;
	Vec2 m_end;

public:
	LineSegment2();
	explicit LineSegment2(Vec2 const& start, Vec2 const& end);

	void Translate(Vec2 const& translation);
	void SetCenter(Vec2 const& newCenter);
	void RotateAboutCenter(float rotationDeltaDegrees);
};


