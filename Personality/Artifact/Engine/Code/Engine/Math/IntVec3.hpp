#pragma once

//-----------------------------------------------------------------------------------------------
struct IntVec3
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	int x = 0;
	int y = 0;
	int z = 0;

public:
	// Construction/Destruction
	~IntVec3();											// destructor (do nothing)
	IntVec3();											// default constructor (do nothing)
	IntVec3( const IntVec3& copyFrom );					// copy constructor (from another vec3)
	explicit IntVec3( int initialX, int initialY, int initialZ);		// explicit constructor (from x, y,z)

	// Accessors (const methods)
	float GetLength() const;
	int GetLengthSquared() const;
	int GetTaxicabLength() const;

	void operator+=(IntVec3 const& intVecToAdd);
	IntVec3 const operator+(IntVec3 const& intVecToAdd);
	IntVec3 const operator-(IntVec3 const& intVecToSubtract);
	void operator-=(IntVec3 const& intVecToSubtract);
	bool operator==(IntVec3 const& compare) const;
	bool operator!=(IntVec3 const& compare) const;
};


