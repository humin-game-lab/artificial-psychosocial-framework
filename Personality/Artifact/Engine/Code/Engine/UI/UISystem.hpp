#pragma once
#include "Engine/Renderer/Camera.hpp"

class Renderer;
class Window;

//-----------------------------------------------------------------------------------------------
struct UISystemConfig {
	Window* m_windowContext = nullptr;
	Renderer* m_renderer = nullptr;
};

//-----------------------------------------------------------------------------------------------
class UISystem{
public:
	UISystem(UISystemConfig const& uiConfig);
	~UISystem();

	void Startup();
	void BeginFrame();
	void Render(Camera const& Camera) const;
	void EndFrame();
	void Shutdown();
public:
	UISystemConfig	m_config;
};



