#include "Engine/UI/UISystem.hpp"
#include "Engine/Core/Window.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "ThirdParty/imgui/imgui.h"
#include "ThirdParty/imgui/imgui_impl_win32.h"
#include "ThirdParty/imgui/imgui_impl_dx11.h"
#include "ThirdParty/imgui/implot.h"



//-----------------------------------------------------------------------------------------------
UISystem::UISystem(UISystemConfig const& uiConfig){
	m_config = uiConfig;
}


//-----------------------------------------------------------------------------------------------
UISystem::~UISystem(){

}


//-----------------------------------------------------------------------------------------------
void UISystem::Startup(){
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGui_ImplWin32_Init(m_config.m_windowContext->GetHwnd());
	ImGui_ImplDX11_Init(m_config.m_renderer->m_device, m_config.m_renderer->m_context);
}


//-----------------------------------------------------------------------------------------------
void UISystem::BeginFrame(){
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
}


//-----------------------------------------------------------------------------------------------
void UISystem::Render(Camera const& camera) const{
	m_config.m_renderer->BeginCamera(camera);
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	m_config.m_renderer->EndCamera(camera);
}


//-----------------------------------------------------------------------------------------------
void UISystem::EndFrame(){
	ImGui::EndFrame();
}


//-----------------------------------------------------------------------------------------------
void UISystem::Shutdown(){
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImPlot::DestroyContext();
	ImGui::DestroyContext();
}
