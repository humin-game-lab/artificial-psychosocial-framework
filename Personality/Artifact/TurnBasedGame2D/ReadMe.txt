----------------------------------------------READ ME--------------------------------------------
------------------------------------------1. How to Use----------------------------------------
This is protogame2D, a protogame for creating new 2D game


----------------------------------------1.1 Control-------------------------------------
"N" or "Space": Press N or space to leave the attract mode and enter the game mode
"P": press P to pause and unpause the game
"ESC": press ESC when game is paused will quit to the attract mode, or quit the game when in the attract mode
"Enter": press Enter to toggle the development console

"Start": pressing start button in the xbox controller to leave the attract mode and enter the game mode

-----------------------------------------1.2 Developer Instruction-------------------------------------
1.	How to build this game?
	Get the completed game on Perforce using the changelist written in the word document. The game is competed and fully tested, which allows any developers to directly open and build it.

2.	How to run this game?
	One way to run this game is to open the game solution on Visual Studio and run it by pressing F5 or Ctrl + F5. Another way is to open the ‘.exe’ executable file in the directory ‘protogame2D/run’.

3.	How to change/rewrite/import the game code?
	The game code includes several parts/structs/classes (engine excluded):
	•	App: handles app-level actions (start, render, general frame, quit, key pressing handle)
	•	Game: owns all game worlds
	•	Entity: abstract base class which has virtual classes of update, render, die and etc.
	•	GameCommon: holds common variables and debug drawing functions
	Developers can open the game folder in Visual Studio to see the details of each class/struct



-----------------------------------------2. Known Issues-------------------------------------
N/A