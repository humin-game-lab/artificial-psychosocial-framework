struct vs_input_t
{
	float3 position : POSITION;
	float4 color	: TINT;
	float2 uv		: TEXCOORD;
};

// struct of data that I'm passing from the vertex stage for the fragment stage.  `v2f_t` is the name used in Unity for this, 
// but you can call it whatever you want, such as `VertexToPixel` or `VertexToFragment` if you prefer a different naming scheme
struct v2p_t // vertex to fragment
{
	float4 position : SV_Position; // SV_Position is always requried for a vertex shader, and denotes the renering location of this vertex.  As an input to the pixel shader, it gives the pixel location of the fragment
	float4 color : VertexColor;    // This semantic is NOT `SV_`, and is just whatever name we want to call it.  If a pixel stage has an input marked as `VertexColor`, it will link the two up.
	float2 uv	:	TEXCOORD;
};


cbuffer CameraConstants : register(b2) {
	float4x4 ViewMatrix;
	float4x4 ProjectionMatrix;
}

cbuffer ModelConstants : register(b3) {
	float4x4 ModelMatrix;
	float4 tint;
}

cbuffer GameConstants : register(b8) {
	float4 b_camWorldPos; // used for fog thickness calculation, specular lighting
	float4 b_skyColor;	// also used as fog color
	float4 b_outdoorColor;
	float4 b_indoorColor;
	float b_fogStartDist;	// zero opacity at before this distance
	float b_fogEndDist;
	float b_fogMaxAlpha;	// beyond fog end ist, gets this much opacity
	float PADDING;
}

Texture2D<float4> SurfaceColorTexture : register(t0);
SamplerState SurfaceSampler : register(s0);

//------------------------------------------------------------------------------------------------
// Main Entry Point for the vertex stage
// which for graphical shaders is usually a main entry point
// and will get information from the game
v2p_t VertexMain(vs_input_t input)
{
	float4 localPosition = float4(input.position, 1);
	float4 worldPosition = mul(ModelMatrix, localPosition);
	float4 cameraPosition = mul(ViewMatrix, worldPosition);
	float4 clipPosition = mul(ProjectionMatrix, cameraPosition);

	v2p_t v2f;
	// we defined the position as a 3 dimensional coordinate, but SV_Position expects a clip/perspective space coordinate (4D).  More on this later.  For now, just pass 1 for w; 
	v2f.position = clipPosition;
	v2f.color = input.color * tint;
	v2f.uv = input.uv;

	return v2f; // pass it on to the raster stage
}



//------------------------------------------------------------------------------------------------
// Main Entry Point for the Pixel Stage
// This returns only one value (the output color)
// so instead of using a struct, we'll just label the return value
//
// This determines the color of a single pixel or fragment in the output
// the input may vary certain variables in the raster stage, and we'll get those varied 
// inputs passed to us. 
//
// Note, system variables such as `SV_Position` have special rules, and the one output may
// have no or very little relation to the one you got into the pixel shader; 
float4 PixelMain(v2p_t input) : SV_Target0
{
	float2 textCoords = input.uv;
	float4 surfaceColor = SurfaceColorTexture.Sample(SurfaceSampler, textCoords);
	// output is RGBA, and input is RGB, so again.  (for now, this will have no visible change no matter what value you pass for alpha)

	float4 tint = input.color;
	tint.a = surfaceColor.a;
	if (tint.a <= 0.0) {
		discard;
	}
	return tint;
}


