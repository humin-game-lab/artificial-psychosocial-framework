#include "RL/SimulationUtils.hpp"
#include "Game/GameCommon.hpp"
#include "Engine/Math/MathUtils.hpp"


//-----------------------------------------------------------------------------------------------
float GetTankScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats
	, bool battleResult){
	UNUSED(battleResult);
	float score = 0.5f;
	float damageBlockedPercentage = 0.f;
	if(totalStats.m_damageBlocked > 0.f){
		damageBlockedPercentage = stats.m_damageBlocked / totalStats.m_damageBlocked;
	}

	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float redZonePercentage = (float)stats.m_redZoneCount / (float)totalZoneCount;
	score += RangeMapClamped(redZonePercentage, 0.f, 0.95f, -0.60f, 0.00f);
	score += RangeMapClamped(redZonePercentage, 0.9f, 1.0f, 0.00f, 0.30f);

	float defenseActionCounter = 0.f;
	float moveBackwardActionCounter = 0.f;
	for(std::string action : actions){
		if(action == "Defense"){
			defenseActionCounter += 1.f;
		}
		else if(action == "Move backward"){
			moveBackwardActionCounter += 1.f;
		}
	}

	//score += RangeMapClamped(damageTakenPercentage, 0.f, 0.5f, -0.20f, 0.20f);

	score += RangeMapClamped(damageBlockedPercentage, 0.f, 0.7f, -0.2f, 0.0f);
	score += RangeMapClamped(damageBlockedPercentage, 0.7f, 0.1f, 0.f, 0.5f);
	if(defenseActionCounter > 0.f){
		score += RangeMapClamped(defenseActionCounter, 0.f, 8.f, -0.3f, 0.3f);
	}
	else{
		score -= 0.4f;
	}
	if(moveBackwardActionCounter > 0.f){
		score -= 0.4f;
	}

	score = RangeMapClamped(score, -1.1f, 1.6f, 0.f, 1.0f);

// 	if(damageDealtPercentage > 0.1f){
// 		score += 0.05f;
// 	}

// 	if(moveForwardActionCounter > 0.f){
// 		score += 0.04f;
// 	}
// 
// 	score -= moveBackwardActionCounter * 0.05f;
	return score;
}


//-----------------------------------------------------------------------------------------------
float GetRangerScore(std::vector<std::string> const& actions, CharacterStats stats, 
	CharacterStats totalStats, bool battleResult){
	if(battleResult){
		int i = 0;
	}
	UNUSED(battleResult);
	float score = 0.5f;
	float damageDealtPercentage = stats.m_damageDealt / totalStats.m_damageDealt;
	score += RangeMapClamped(damageDealtPercentage, 0.f, 0.3f, -0.3f, 0.0f);
	score += RangeMapClamped(damageDealtPercentage, 0.3f, 0.5f, 0.0f, 0.5f);

	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float yellowZoneCount = (float)stats.m_yellowZoneCount / (float)totalZoneCount;
	score += RangeMapClamped(yellowZoneCount, 0.f, 0.8f, -0.60f, 0.00f);
	score += RangeMapClamped(yellowZoneCount, 0.8f, 1.0f, 0.00f, 0.40f);

	float shortAttackCounter = 0.f;
	for (std::string action : actions) {
		if (action == "Short attack") {
			shortAttackCounter += 1.f;
		}
	}

	if(shortAttackCounter > 0.f){
		score += RangeMapClamped(shortAttackCounter, 1.f, 5.f, -0.10f, -0.50f);
	}

	score = RangeMapClamped(score, -1.9f, 1.4f, 0.f, 1.0f);

	return score;
}


//-----------------------------------------------------------------------------------------------
float GetHealerScore(std::vector<std::string> const& actions, CharacterStats stats, 
	CharacterStats totalStats, bool battleResult){
	UNUSED(actions);
	UNUSED(battleResult);
	float score = 0.5f;
	float damageHealedPercentage = stats.m_damageHealed / totalStats.m_damageHealed;
	score += RangeMapClamped(damageHealedPercentage, 0.f, 0.4f, -0.4f, 0.0f);
	score += RangeMapClamped(damageHealedPercentage, 0.4f, 0.7f, 0.0f, 0.6f);

	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float redZoneCount = (float)stats.m_redZoneCount / (float)totalZoneCount;
	//float greenZoneCount = (float)stats.m_greenZoneCount / (float)totalZoneCount;
	score += RangeMapClamped(redZoneCount, 0.f, 0.5f, 0.20f, -0.30f);

// 	float shortAttackCounter = 0.f;
// 	for (std::string action : actions) {
// 		if (action == "Short attack") {
// 			shortAttackCounter += 1.f;
// 		}
// 	}
// 
// 	if (shortAttackCounter > 0.f) {
// 		score += RangeMapClamped(shortAttackCounter, 1.f, 5.f, -0.10f, -0.50f);
// 	}

	score = RangeMapClamped(score, -1.2f, 1.3f, 0.f, 1.0f);

	return score;
}


//-----------------------------------------------------------------------------------------------
float GetSylvanasScore(std::vector<std::string> const& actions, CharacterStats stats, 
	CharacterStats totalStats, bool battleResult){
	float score = 0.5f;

	// win/lose condition
	if(battleResult == false){
		score -= 0.5f;
	}
	else{
		score += 0.1f;
	}
	
	// no heals
	float healCount = 0.f;
	for (std::string action : actions) {
		if (action == "Heal") {
			healCount += 1.f;
		}
	}
	if (healCount > 0.f) {
		score += RangeMapClamped(healCount, 1.f, 5.f, -0.20f, -0.50f);
	}
	else{
		score += 0.1f;
	}

	// never retreat
	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float redZonePercentage = (float)stats.m_redZoneCount / (float)totalZoneCount;
	float greenZonePercentage = (float)stats.m_greenZoneCount / (float)totalZoneCount;
	if(redZonePercentage > 0.8f){
		score += 0.2f;
	}
	else{
		score -= 0.2f;
	}
	if(greenZonePercentage > 0.f){
		score -= 0.5f;
	}

	// damage percentage
	float damageDealtPercentage = stats.m_damageDealt / totalStats.m_damageDealt;
	score += RangeMapClamped(damageDealtPercentage, 0.f, 0.3f, -0.3f, 0.0f);
	score += RangeMapClamped(damageDealtPercentage, 0.3f, 0.8f, 0.0f, 0.6f);

	score = RangeMapClamped(score, -2.5f, 1.3f, 0.f, 1.0f);

	return score;
}


//-----------------------------------------------------------------------------------------------
float GetSylvanasReverseScore(std::vector<std::string> const& actions, CharacterStats stats, 
	CharacterStats totalStats, bool battleResult){
	float score = 0.5f;

	// no heals
	float healCount = 0.f;
	for (std::string action : actions) {
		if (action == "Heal") {
			healCount += 1.f;
		}
	}
	if (healCount > 0.f) {
		score += RangeMapClamped(healCount, 1.f, 10.f, 0.05f, 0.30f);
	}
	else {
		score -= 0.5f;
	}

	// never go to red zone
	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float redZoneCount = (float)stats.m_redZoneCount / (float)totalZoneCount;
	//float greenZoneCount = (float)stats.m_greenZoneCount / (float)totalZoneCount;
	score += RangeMapClamped(redZoneCount, 0.f, 0.5f, 0.20f, -0.30f);


	score = RangeMapClamped(score, -1.3f, 1.f, 0.f, 1.0f);

	return score;
}


//-----------------------------------------------------------------------------------------------
float GetSylvanasCompletelyReverseScore(std::vector<std::string> const& actions,
	CharacterStats stats, CharacterStats totalStats, bool battleResult){
	float score = 0.5f;

	// win/lose condition
	if (battleResult == false) {
		score += 0.1f;
	}
	else {
		score -= 0.5f;
	}

	// no heals
	float healCount = 0.f;
	for (std::string action : actions) {
		if (action == "Heal") {
			healCount += 1.f;
		}
	}
	if (healCount > 0.f) {
		score += RangeMapClamped(healCount, 1.f, 10.f, 0.05f, 0.30f);
	}
	else {
		score -= 0.5f;
	}

	// never go to green zone
	int totalZoneCount = stats.m_redZoneCount + stats.m_yellowZoneCount + stats.m_greenZoneCount;
	float redZoneCount = (float)stats.m_redZoneCount / (float)totalZoneCount;
	//float greenZoneCount = (float)stats.m_greenZoneCount / (float)totalZoneCount;
	score += RangeMapClamped(redZoneCount, 0.f, 0.5f, 0.20f, -0.30f);

	// damage percentage
	float damageDealtPercentage = stats.m_damageDealt / totalStats.m_damageDealt;
	score += RangeMapClamped(damageDealtPercentage, 0.3f, 0.6f, -0.3f, 0.0f);
	score += RangeMapClamped(damageDealtPercentage, 0.3f, 0.0f, 0.0f, 0.4f);

	return score;
}


//-----------------------------------------------------------------------------------------------
float GetReward(float state){
	return state - 0.5f;
}


//-----------------------------------------------------------------------------------------------
float GetPraiseDifference(std::vector<float> const& beginPraises, std::vector<float> const& endPraises){
	float difference = 0.f;
	for(int index = 0; index < (int)beginPraises.size();index++){
		difference += abs(endPraises[index] - beginPraises[index]);
	}

	float score = RangeMapClamped(difference, 0.f, 1.5f, 1.f, 0.f);
	return score;
}


//-----------------------------------------------------------------------------------------------
bool DoPraisesSimilar(std::vector<float> const& beginPraises, std::vector<float> const& endPraises){
	int traitCount = 0;
	for (int index = 0; index < (int)beginPraises.size(); index++) {
		if(abs(beginPraises[index] - endPraises[index]) <= 0.02f){
			traitCount++;
		}
	}

	if(traitCount == (int)beginPraises.size()){
		return true;
	}

	return false;
}


//-----------------------------------------------------------------------------------------------
bool IsNewPraiseRecorded(std::vector<std::vector<float>> const& praises, std::vector<float> newPraise){
	for(int index = 0; index < (int)praises.size(); index++){
		if(DoPraisesSimilar(newPraise, praises[index])){
			return true;
		}
	}
	return false;
}
