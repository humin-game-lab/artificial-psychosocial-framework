#include "RL/PSOWindow.hpp"
#include "RL/RLBrain.hpp"
#include "RL/PCA.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/APFUtils.hpp"
#include "ThirdParty/imgui/imgui.h"
#include "ThirdParty/imgui/implot.h"
#include <string>


//-----------------------------------------------------------------------------------------------
PSOWindow::PSOWindow(PSOWithValence* valence, PSOWithPersonality* personality){
	m_psoValence = valence;
	m_psoPersonality = personality;
}


//-----------------------------------------------------------------------------------------------
void PSOWindow::Update(){
	if(g_theGame->m_trainState == PSO_VALENCE){
		UpdateValenceTraining();
	}
	else if(g_theGame->m_trainState == PSO_PERSONALITY){
		UpdatePersonalityTraining();
	}
	
}


//-----------------------------------------------------------------------------------------------
void PSOWindow::UpdateValenceTraining(){
	ImGui::Begin("PSO Settings");

	if(!m_isHeuristicSelected){
		std::vector<const char*> heuristicNames;
		for(auto iter = g_heuristicFunctions.begin(); iter != g_heuristicFunctions.end(); iter++){
			heuristicNames.push_back(iter->first.c_str());
		}
		ImGui::ListBox("Available Heuristic Functions", &m_selectedHeuritiscIndex, heuristicNames.data(),
			(int)heuristicNames.size(), 8);
	}
	ImGui::SliderInt("Number Of Generations", &m_generation, 20, 500);
	ImGui::SliderInt("Number Of Particles", &m_swarms, 5, 20);
	ImGui::NewLine();
	ImGui::SliderFloat("C1", &m_c1, 0.f, 5.f);
	ImGui::SliderFloat("C2", &m_c2, 0.f, 5.f);
	ImGui::SliderFloat("Current Velocity Weight", &m_weight, 0.f, 1.f);
	ImGui::NewLine();
	ImGui::SliderFloat("Min Velocity", &m_minVelocity, -0.5f, m_maxVelocity);
	ImGui::SliderFloat("Max Velocity", &m_maxVelocity, m_minVelocity, 0.5f);
	ImGui::SliderFloat("Min Valence Value", &m_minValence, 0.f, m_maxValence);
	ImGui::SliderFloat("Max Valence Value", &m_maxValence, m_minValence, 1.f);
	ImGui::Checkbox("Record High Score Values", &m_recordHighScoreValues);
	if(m_isHeuristicSelected){
		if (ImGui::Button("Apply Settings And Restart")) {
			delete g_theGame->m_psoValence;
			g_theGame->m_psoValence = new PSOWithValence(m_swarms);
			g_theGame->m_psoValence->m_maxGenerations = m_generation;
			g_theGame->m_psoValence->m_c1 = m_c1;
			g_theGame->m_psoValence->m_c2 = m_c2;
			g_theGame->m_psoValence->m_weight = m_weight;
			g_theGame->m_psoValence->m_minVelocity = m_minVelocity;
			g_theGame->m_psoValence->m_maxVelocity = m_maxVelocity;
			g_theGame->m_psoValence->m_minValence = m_minValence;
			g_theGame->m_psoValence->m_maxValence = m_maxValence;
			if(m_recordHighScoreValues){
				g_theGame->m_psoValence->m_recordHighPraises = true;
			}
			m_psoValence = g_theGame->m_psoValence;

			int heutisticIndex = 0;
			for (auto iter = g_heuristicFunctions.begin(); iter != g_heuristicFunctions.end(); iter++) {
				if (heutisticIndex == m_selectedHeuritiscIndex) {
					g_theGame->m_psoValence->m_heuristicFunction = iter->second;
					break;
				}
				heutisticIndex++;
			}

			m_psoValence->m_isPaused = false;
		}
		if (m_psoValence->m_isPaused) {
			if (ImGui::Button("Resume")) {
				m_psoValence->m_isPaused = false;
			}
		}
		if (!m_psoValence->m_isPaused) {
			if (ImGui::Button("Pause")) {
				m_psoValence->m_isPaused = true;
			}
		}
		if (ImGui::Button("Apply Global Best And Train Personality")) {
			if(m_psoValence->m_recordHighPraises){
				m_psoValence->SaveHighScorePraises();
			}
			g_theGame->ApplyValenceAndTrainPersonality();
		}
	}
	else{
		if (ImGui::Button("Start Simulating")) {
			m_isHeuristicSelected = true;
			delete g_theGame->m_psoValence;
			g_theGame->m_psoValence = new PSOWithValence(m_swarms);
			g_theGame->m_psoValence->m_maxGenerations = m_generation;
			g_theGame->m_psoValence->m_c1 = m_c1;
			g_theGame->m_psoValence->m_c2 = m_c2;
			g_theGame->m_psoValence->m_weight = m_weight;
			g_theGame->m_psoValence->m_minVelocity = m_minVelocity;
			g_theGame->m_psoValence->m_maxVelocity = m_maxVelocity;
			g_theGame->m_psoValence->m_minValence = m_minValence;
			g_theGame->m_psoValence->m_maxValence = m_maxValence;
			if (m_recordHighScoreValues) {
				g_theGame->m_psoValence->m_recordHighPraises = true;
			}
			m_psoValence = g_theGame->m_psoValence;

			int heutisticIndex = 0;
			for (auto iter = g_heuristicFunctions.begin(); iter != g_heuristicFunctions.end(); iter++) {
				if (heutisticIndex == m_selectedHeuritiscIndex) {
					g_theGame->m_psoValence->m_heuristicFunction = iter->second;
					break;
				}
				heutisticIndex++;
			}

			m_psoValence->m_isPaused = false;


		}
	}
	ImGui::NewLine();

	std::string scoreString = std::to_string(m_psoValence->m_globalScore);
	std::string bestScore = "Global Best Score:" + scoreString.substr(0,
		scoreString.find('.') + 4);
	if (ImGui::TreeNode(bestScore.c_str())) {
		int actionIndex = 0;
		for (auto iter = s_actions.begin(); iter != s_actions.end(); iter++) {
			std::string valenceString = std::to_string(m_psoValence->m_globalValence.m_praises[actionIndex]);
			std::string actionString = iter->second->actionName + ": " + valenceString.substr(0,
				valenceString.find('.') + 4);
			if (ImGui::TreeNode(actionString.c_str())) {
				ImGui::TreePop();
			};
			actionIndex++;
		}
		ImGui::TreePop();
	}

	ImGui::NewLine();

	ImPlot::SetNextPlotLimitsX(-1, 1);
	ImPlot::SetNextPlotLimitsY(-2, 2);
	Eigen::MatrixXd pcaResult = PCA2Dim(m_psoValence);
	std::vector<float> xValues, yValues;
	for (int rowIndex = 0; rowIndex < (int)pcaResult.rows(); rowIndex++) {
		xValues.push_back((float)pcaResult(rowIndex, 0));
		yValues.push_back((float)pcaResult(rowIndex, 1));
	}
	if (ImPlot::BeginPlot("Valence Data Changes (PCA Dimension: 2)")) {
		ImPlot::PlotScatter("Valence Data", xValues.data(), yValues.data(), (int)xValues.size());
		ImPlot::EndPlot();
	}
	ImGui::End();
}


//-----------------------------------------------------------------------------------------------
void PSOWindow::UpdatePersonalityTraining(){
	ImGui::Begin("PSO Settings");

	ImGui::SliderInt("Number Of Generations", &m_generation, 20, 500);
	ImGui::SliderInt("Number Of Swarms", &m_swarms, 5, 20);
	ImGui::NewLine();
	ImGui::SliderFloat("C1", &m_c1, 0.f, 5.f);
	ImGui::SliderFloat("C2", &m_c2, 0.f, 5.f);
	ImGui::SliderFloat("Current Velocity Weight", &m_weight, 0.f, 1.f);
	ImGui::NewLine();
	ImGui::SliderFloat("Min Velocity", &m_minVelocity, -0.5f, m_maxVelocity);
	ImGui::SliderFloat("Max Velocity", &m_maxVelocity, m_minVelocity, 0.5f);
	ImGui::SliderFloat("Min Valence Value", &m_minValence, 0.f, m_maxValence);
	ImGui::SliderFloat("Max Valence Value", &m_maxValence, m_minValence, 1.f);
	if (ImGui::Button("Apply Settings And Restart")) {
		delete g_theGame->m_psoPersonality;
		g_theGame->m_psoPersonality = new PSOWithPersonality(m_swarms);
		g_theGame->m_psoPersonality->m_maxGenerations = m_generation;
		g_theGame->m_psoPersonality->m_c1 = m_c1;
		g_theGame->m_psoPersonality->m_c2 = m_c2;
		g_theGame->m_psoPersonality->m_weight = m_weight;
		g_theGame->m_psoPersonality->m_minVelocity = m_minVelocity;
		g_theGame->m_psoPersonality->m_maxVelocity = m_maxVelocity;
		g_theGame->m_psoPersonality->m_minTrait = m_minValence;
		g_theGame->m_psoPersonality->m_maxTrait = m_maxValence;
		m_psoPersonality = g_theGame->m_psoPersonality;
	}
	if (m_psoPersonality->m_isPaused) {
		if (ImGui::Button("Resume")) {
			m_psoPersonality->m_isPaused = false;
		}
	}
	if (!m_psoPersonality->m_isPaused) {
		if (ImGui::Button("Pause")) {
			m_psoPersonality->m_isPaused = true;
		}
	}
	if (ImGui::Button("Apply Global Best And Simulate Game")) {
		g_theGame->SimulateLevelFromPSOBest();
	}
	ImGui::NewLine();

	std::string scoreString = std::to_string(m_psoPersonality->m_globalScore);
	std::string bestScore = "Global Best Score:" + scoreString.substr(0,
		scoreString.find('.') + 4);
	if (ImGui::TreeNode(bestScore.c_str())) {
		int actionIndex = 0;
		for (int trainIndex = 0; trainIndex < 5; trainIndex++) {
			std::string traitString = std::to_string(m_psoPersonality->m_globalPersonality[actionIndex]);
			std::string actionString = m_psoPersonality->m_globalPersonality.GetTraitName(trainIndex) +
				": " + traitString.substr(0, traitString.find('.') + 4);
			if (ImGui::TreeNode(actionString.c_str())) {
				ImGui::TreePop();
			};
			actionIndex++;
		}
		ImGui::TreePop();
	}

	ImGui::NewLine();

	ImPlot::SetNextPlotLimitsX(-1, 1);
	ImPlot::SetNextPlotLimitsY(-2, 2);
	Eigen::MatrixXd pcaResult = PCA2Dim(m_psoPersonality);
	std::vector<float> xValues, yValues;
	for (int rowIndex = 0; rowIndex < (int)pcaResult.rows(); rowIndex++) {
		xValues.push_back((float)pcaResult(rowIndex, 0));
		yValues.push_back((float)pcaResult(rowIndex, 1));
	}
	if (ImPlot::BeginPlot("Personality Data Changes (PCA Dimension: 2)")) {
		ImPlot::PlotScatter("Personality Data", xValues.data(), yValues.data(), (int)xValues.size());
		ImPlot::EndPlot();
	}
	ImGui::End();
}

