#pragma once
#include "Game/Character.hpp"


//-----------------------------------------------------------------------------------------------
float GetTankScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);
float GetRangerScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);
float GetHealerScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);
float GetSylvanasScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);
float GetSylvanasReverseScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);
float GetSylvanasCompletelyReverseScore(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);

float GetReward(float state);
float GetPraiseDifference(std::vector<float> const& beginPraises, std::vector<float> const& endPraises);
bool DoPraisesSimilar(std::vector<float> const& beginPraises, std::vector<float> const& endPraises);
bool IsNewPraiseRecorded(std::vector<std::vector<float>> const& praises, std::vector<float> newPraise);