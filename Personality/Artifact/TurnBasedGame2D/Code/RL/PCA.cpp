#include "RL/PCA.hpp"
#include "RL/RLBrain.hpp"


//-----------------------------------------------------------------------------------------------
Eigen::MatrixXd PCA2Dim(PSOWithValence* pso){
	Eigen::MatrixXd originalData(pso->m_numOfParticles, pso->m_particleDimension);
	for(int rowIndex = 0; rowIndex < pso->m_numOfParticles; rowIndex++){
		for(int colIndex = 0; colIndex < pso->m_particleDimension; colIndex++){
			originalData(rowIndex, colIndex) = pso->m_particles[rowIndex].m_currentValence.m_praises[colIndex];
		}
	}
	double correlation = 0.95f;
	while ((int)originalData.cols() > 2){
		CalculatePCA(originalData, correlation);
		correlation -= 0.05f;
	}
	return originalData;
}


//-----------------------------------------------------------------------------------------------
Eigen::MatrixXd PCA2Dim(PSOWithPersonality* pso){
	Eigen::MatrixXd originalData(pso->m_numOfParticles, pso->m_particleDimension);
	for (int rowIndex = 0; rowIndex < pso->m_numOfParticles; rowIndex++) {
		for (int colIndex = 0; colIndex < pso->m_particleDimension; colIndex++) {
			originalData(rowIndex, colIndex) = pso->m_particles[rowIndex].m_currentPersonality[colIndex];
		}
	}
	double correlation = 0.95f;
	while ((int)originalData.cols() > 2) {
		CalculatePCA(originalData, correlation);
		correlation -= 0.05f;
	}
	return originalData;
}


//-----------------------------------------------------------------------------------------------
void CalculatePCA(Eigen::MatrixXd& data, double dimCorrelation){
	Normalize(data);

	Eigen::MatrixXd covariance(data.cols(), data.cols());
	GetCovariance(data, covariance);


	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigen(covariance);
	Eigen::MatrixXd eigenVectors = eigen.eigenvectors();
	Eigen::MatrixXd eigenValues = eigen.eigenvalues();
	int newDimension = GetNewDimension(eigenValues, dimCorrelation);
	if(newDimension < 2){
		newDimension = 2;
	}
	data = data * eigenVectors.rightCols(newDimension);
}


//-----------------------------------------------------------------------------------------------
void Normalize(Eigen::MatrixXd& data){
	Eigen::MatrixXd mean = data.colwise().mean();
	Eigen::RowVectorXd meanRow = mean;
	data.rowwise() -= meanRow;
}


//-----------------------------------------------------------------------------------------------
void GetCovariance(Eigen::MatrixXd& data, Eigen::MatrixXd& covariance){
	covariance = data.adjoint() * data;
	covariance = covariance.array() / (data.rows() - 1);
}


//-----------------------------------------------------------------------------------------------
int GetNewDimension(Eigen::MatrixXd const& eigenValues, double dimCorrelation){
	int dimension = 0;
	double sum = 0.0;
	for(int rowIndex = (int)eigenValues.rows() - 1; rowIndex >= 0; rowIndex--){
		sum += eigenValues(rowIndex, 0);
		dimension = rowIndex;

		if(sum / eigenValues.sum() >= dimCorrelation){
			break;
		}
	}
	return (int)eigenValues.rows() - dimension;
}
