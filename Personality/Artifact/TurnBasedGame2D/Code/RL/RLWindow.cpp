#include "RL/RLWindow.hpp"
#include "ThirdParty/imgui/imgui.h"
#include "ThirdParty/imgui/implot.h"


//-----------------------------------------------------------------------------------------------
void RLWindow::Update(){
	ImPlot::SetNextPlotLimitsX(0, 20, ImGuiCond_Always);
	ImPlot::SetNextPlotLimitsY(0.0, 1.0);

	ImGui::Begin("Reinforcement Learning Optimization Curve");
	if (ImPlot::BeginPlot("Simulation", "Score", "Score")) {
		ImPlot::PlotLine("Q Learning", &m_simulations.data()[0], &m_scores.data()[0], 
			(int)m_simulations.size());
		ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle);
		ImPlot::EndPlot();
	}
	ImGui::End();
}


//-----------------------------------------------------------------------------------------------
void RLWindow::AddScore(float score){
	m_simulations.push_back((float)m_simulations.size() + 1.f);
	m_scores.push_back(score);
}


//-----------------------------------------------------------------------------------------------
void RLWindow::Clear(){
	m_simulations.clear();
	m_scores.clear();
}

