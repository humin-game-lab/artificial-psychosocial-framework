#pragma once
#include "Game/Character.hpp"
#include <map>
#include <vector>
#include <string>

struct Level;

//-----------------------------------------------------------------------------------------------
typedef float (*HeuristicFunctionPointer)(std::vector<std::string> const& actions, CharacterStats stats, CharacterStats totalStats, bool battleResult);

//-----------------------------------------------------------------------------------------------
struct PersonalityAction {
public:
	PersonalityAction(float o = 0.f, float c = 0.f, float e = 0.f, float a = 0.f, float n = 0.f);
	PersonalityAction(PersonalityAction const& copy);
	float GetActionScore() const;
	void Reset();
	bool IsValid() const;

	bool operator < (const PersonalityAction& rhs) const;
	void operator += (const PersonalityAction& action);
public:
	float m_openness = 0.f;
	float m_conscientiousness = 0.f;
	float m_extraversion = 0.f;
	float m_agreeableness = 0.f;
	float m_neuroticism = 0.f;

	
};


//-----------------------------------------------------------------------------------------------
struct ValenceAction {
public:
	ValenceAction(int index = -1, float valance = 0.f);
	ValenceAction(ValenceAction const& copy);
	float GetActionScore() const;
	void Reset();

	bool operator < (const ValenceAction& rhs) const;
public:
	int m_actionIndex = -1;
	float m_valenceChange = 0.f;
};



//-----------------------------------------------------------------------------------------------
struct Personality {
public:
	Personality(float o = 0.5f, float c = 0.5f, float e = 0.5f, float a = 0.5f, float n = 0.5f):
		m_openness(o),m_conscientiousness(c),
		m_extraversion(e), m_agreeableness(a), m_neuroticism(n){};
	Personality(Personality const& copy);
	void AddAction(PersonalityAction action);
	void Reset();
	float GetPersonalityScore() const;
	bool operator < (const Personality& rhs) const;
	float& operator[] (int index);
	std::string GetTraitName(int index) const;
	PersonalityAction ConvertToAction() const;
public:
	float m_openness = 0.5f;
	float m_conscientiousness = 0.5f;
	float m_extraversion = 0.5f;
	float m_agreeableness = 0.5f;
	float m_neuroticism = 0.5f;
};


//-----------------------------------------------------------------------------------------------
struct Valence {
public:
	Valence();
	Valence(Valence const& copy);
	void GenerateData();
	void AddAction(ValenceAction action);
	void Reset();
	float GetValenceScore() const;
	bool IsValid() const;
	bool operator < (Valence const& rhs) const;
public:
	std::vector<float> m_praises;
};


//-----------------------------------------------------------------------------------------------
class QLearningWithPersonality {
public:
	QLearningWithPersonality(std::vector<PersonalityAction> actions, float learningRate = 0.01f, 
		float rewardDecay = 0.9f, float greedy = 0.8f);
	static std::vector<PersonalityAction> InitializeAllPossiblePeronalityActions();

	PersonalityAction ChooseAction(Personality state);
	void Learn(Personality state, Personality newState, PersonalityAction action, float reward);
public:
	std::vector<PersonalityAction> m_actions;
	float m_learningRate = 0.f;
	float m_gamma = 0.f;
	float m_epsilon = 0.f;

	std::map<Personality, std::map<PersonalityAction, float>> m_qTable;

	float m_highestScore = 0.f;
	Personality m_highestScoreState;
private:
	bool CheckStateExist(Personality state);
	std::map<PersonalityAction, float> InitializeEmptyStateTable() const;
};


//-----------------------------------------------------------------------------------------------
class QLearningWithValence {
public:
	QLearningWithValence(std::vector<ValenceAction> actions, float learningRate = 0.01f,
		float rewardDecay = 0.9f, float greedy = 0.6f);
	static std::vector<ValenceAction> InitializeAllPossibleValenceActions();

	ValenceAction ChooseAction(Valence state);
	void Learn(Valence state, Valence newState, ValenceAction action, float reward);
public:
	std::vector<ValenceAction> m_actions;
	float m_learningRate = 0.f;
	float m_gamma = 0.f;
	float m_epsilon = 0.f;

	std::map<Valence, std::map<ValenceAction, float>> m_qTable;

	float m_highestScore = 0.f;
	Valence m_highestScoreState;
private:
	bool CheckStateExist(Valence state);
	std::map<ValenceAction, float> InitializeEmptyStateTable() const;
};


//-----------------------------------------------------------------------------------------------
class PSOWithValence;
struct ValenceParticle {
public:
	ValenceParticle(PSOWithValence* owner, int index);
	void Simulate(float deltaSeconds);
	void PaticleMove(float deltaSeconds);
	void InitializeRandomly();
public:
	PSOWithValence* m_owner = nullptr;
	int m_index = -1;

	Level* m_level = nullptr;

	Valence m_currentValence;
	Valence m_bestValence;

	float m_currentScore = 0.f;
	float m_bestScore = 0.f;
	
	std::vector<float> m_currentVelocity;
};


//-----------------------------------------------------------------------------------------------
class PSOWithPersonality;
struct PersonalityParticle {
public:
	PersonalityParticle(PSOWithPersonality* owner, int index);
	void Simulate(float deltaSeconds);
	void PaticleMove(float deltaSeconds);
	void InitializeRandomly();
public:
	PSOWithPersonality* m_owner = nullptr;
	int m_index = -1;

	Level* m_level = nullptr;

	Personality m_currentPersonality;
	Personality m_bestPersonality;

	float m_currentScore = 0.f;
	float m_bestScore = 0.f;

	std::vector<float> m_currentVelocity;
};

//-----------------------------------------------------------------------------------------------
class PSOWithValence {
public:
	PSOWithValence(int numOfParticles = 20);
	void Update(float deltaSeconds);
	Level* GetParticleLevel(int swarmIndex) const;
	Level* GetRunningLevel() const;
	void SaveHighScorePraises() const;

public:
	int m_numberOfThreads = 0;

	float m_c1 = 2.f;
	float m_c2 = 2.f;
	float m_weight = 0.5f;

	int m_maxGenerations = 50;
	int m_currentGeneration = 0;
	int m_numOfParticles = 20;
	int m_particleDimension = 0;
	int m_numOfSimulationsEachFrame = 5;

	float m_minVelocity = -0.1f;
	float m_maxVelocity = 0.1f;
	float m_minValence = 0.f;
	float m_maxValence = 1.f;

	Valence m_globalValence;
	float m_globalScore = 0.f;

	bool m_simulatomComplete = false;
	bool m_isPaused = true;

	std::vector<ValenceParticle> m_particles;
	int m_currentParticleIndex = 0;

	HeuristicFunctionPointer m_heuristicFunction = nullptr;

	bool m_recordHighPraises = false;
	std::vector<std::vector<float>> m_highPraises;
protected:
	void InitializeRandomSwarms();
	void RandomRestart();
};


//-----------------------------------------------------------------------------------------------
class PSOWithPersonality {
public:
	PSOWithPersonality(int numOfParticles = 20);
	void Update(float deltaSeconds);
	Level* GetParticleLevel(int swarmIndex) const;
	Level* GetRunningLevel() const;
	Personality GetCurrentPersonality() const;
public:
	float m_c1 = 2.f;
	float m_c2 = 2.f;
	float m_weight = 0.5f;

	int m_maxGenerations = 50;
	int m_currentGeneration = 0;
	int m_numOfParticles = 20;
	int m_particleDimension = 0;
	int m_numOfSimulationsEachFrame = 5;

	float m_minVelocity = -0.1f;
	float m_maxVelocity = 0.1f;
	float m_minTrait = 0.0f;
	float m_maxTrait = 1.f;

	Personality m_globalPersonality;
	float m_globalScore = 0.f;

	bool m_simulatomComplete = false;
	bool m_isPaused = false;

	std::vector<PersonalityParticle> m_particles;
	int m_currentParticleIndex = 0;
protected:
	void InitializeRandomSwarms();
	void RandomRestart();
};


