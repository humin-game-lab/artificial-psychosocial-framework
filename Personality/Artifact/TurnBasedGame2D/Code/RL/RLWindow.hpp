#pragma once
#include <vector>

class RLWindow{
public:
	RLWindow(){}
	~RLWindow(){}
	void Update();
	void AddScore(float score);
	void Clear();
public:	
	std::vector<float> m_simulations;
	std::vector<float> m_scores;
};

