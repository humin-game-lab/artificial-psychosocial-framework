#pragma once
#include "ThirdParty/Eigen/Dense"
#include <vector>

class PSOWithValence;
class PSOWithPersonality;

//-----------------------------------------------------------------------------------------------
Eigen::MatrixXd PCA2Dim(PSOWithValence* pso);
Eigen::MatrixXd PCA2Dim(PSOWithPersonality* pso);
void CalculatePCA(Eigen::MatrixXd& data, double dimCorrelation);
void Normalize(Eigen::MatrixXd& data);
void GetCovariance(Eigen::MatrixXd& data, Eigen::MatrixXd& covariance);
int GetNewDimension(Eigen::MatrixXd const& eigenValues, double dimCorrelation);
