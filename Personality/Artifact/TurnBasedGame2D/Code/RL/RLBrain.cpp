#include "RL/RLBrain.hpp"
#include "RL/SimulationUtils.hpp"
#include "Game/APFUtils.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"
#include "Game/GameCommon.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "ThirdParty/APF/APF/Trait.hpp"
#include <math.h>
#include <iostream>
#include <fstream>


//-----------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------
PersonalityAction::PersonalityAction(float o, float c, float e, float a, float n){
	m_openness = o;
	m_conscientiousness = c;
	m_extraversion = e;
	m_agreeableness = a;
	m_neuroticism = n;
}


//-----------------------------------------------------------------------------------------------
PersonalityAction::PersonalityAction(PersonalityAction const& copy){
	m_openness = copy.m_openness;
	m_conscientiousness = copy.m_conscientiousness;
	m_extraversion = copy.m_extraversion;
	m_agreeableness = copy.m_agreeableness;
	m_neuroticism = copy.m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
float PersonalityAction::GetActionScore() const{
	return m_openness * 10000.f + m_conscientiousness * 1000.f
		+ m_extraversion * 100.f + m_agreeableness * 10.f + m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
void PersonalityAction::Reset(){
	m_openness = 0.f;
	m_conscientiousness = 0.f;
	m_extraversion = 0.f;
	m_agreeableness = 0.f;
	m_neuroticism = 0.f;
}


//-----------------------------------------------------------------------------------------------
bool PersonalityAction::IsValid() const{
	if(m_openness >= 1.f || m_conscientiousness >= 1.f || m_extraversion >= 1.f || m_agreeableness >= 1.f ||
		m_neuroticism >= 1.f || m_openness <= 0.f || m_conscientiousness <= 0.f || m_extraversion <= 0.f ||
		m_agreeableness <= 0.f || m_neuroticism <= 0.f){
		return false;	
	}
	return true;
}


//-----------------------------------------------------------------------------------------------
bool PersonalityAction::operator<(const PersonalityAction& rhs) const{
	return GetActionScore() < rhs.GetActionScore();
}


//-----------------------------------------------------------------------------------------------
void PersonalityAction::operator+=(const PersonalityAction& action){
	m_openness += action.m_openness;
	m_conscientiousness += action.m_conscientiousness;
	m_extraversion += action.m_extraversion;
	m_agreeableness += action.m_agreeableness;
	m_neuroticism += action.m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
ValenceAction::ValenceAction(int index /*= -1*/, float valance /*= 0.f*/){
	m_actionIndex = index;
	m_valenceChange = valance;
}


//-----------------------------------------------------------------------------------------------
ValenceAction::ValenceAction(ValenceAction const& copy){
	m_actionIndex = copy.m_actionIndex;
	m_valenceChange = copy.m_valenceChange;
}


//-----------------------------------------------------------------------------------------------
float ValenceAction::GetActionScore() const{
	return pow(1.f , m_actionIndex) + m_valenceChange;
}


//-----------------------------------------------------------------------------------------------
void ValenceAction::Reset(){
	m_actionIndex = -1;
	m_valenceChange = 0.f;
}


//-----------------------------------------------------------------------------------------------
bool ValenceAction::operator<(const ValenceAction& rhs) const{
	return GetActionScore() < rhs.GetActionScore();
}


//-----------------------------------------------------------------------------------------------
Valence::Valence() {
	if ((int)m_praises.size() <= 0) {
		GenerateData();
	}
}


//-----------------------------------------------------------------------------------------------
Valence::Valence(Valence const& copy) {
	m_praises = copy.m_praises;
}


//-----------------------------------------------------------------------------------------------
void Valence::GenerateData(){
	if((int)s_actions.size() > 0){
		m_praises.clear();
		for(int index = 0; index < (int)s_actions.size(); index++){
			m_praises.push_back(0.5f);
		}
	}
}


//-----------------------------------------------------------------------------------------------
void Valence::AddAction(ValenceAction action) {
	if((int)m_praises.size() <= 0){
		GenerateData();
	}
	m_praises[action.m_actionIndex] += action.m_valenceChange;
}


//-----------------------------------------------------------------------------------------------
void Valence::Reset() {
	m_praises.clear();
	for (int index = 0; index < (int)s_actions.size(); index++) {
		m_praises.push_back(0.5f);
	}
}


//-----------------------------------------------------------------------------------------------
float Valence::GetValenceScore() const {
	float score = 0.f;
	for (int index = 0; index < (int)m_praises.size(); index++) {
		score += pow(m_praises[index], (float)index+1.f);
	}
	return score;
}


//-----------------------------------------------------------------------------------------------
bool Valence::IsValid() const{
	for (int index = 0; index < (int)m_praises.size(); index++) {
		if(m_praises[index] < 0.f || m_praises[index] > 1.f){
			return false;
		}
	}

	return true;
}


//-----------------------------------------------------------------------------------------------
bool Valence::operator<( Valence const& rhs) const {
	return GetValenceScore() < rhs.GetValenceScore();
}


//-----------------------------------------------------------------------------------------------
Personality::Personality(Personality const& copy) {
	m_openness = copy.m_openness;
	m_conscientiousness = copy.m_conscientiousness;
	m_extraversion = copy.m_extraversion;
	m_agreeableness = copy.m_agreeableness;
	m_neuroticism = copy.m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
void Personality::AddAction(PersonalityAction action) {
	m_openness += action.m_openness;
	m_conscientiousness += action.m_conscientiousness;
	m_extraversion += action.m_extraversion;
	m_agreeableness += action.m_agreeableness;
	m_neuroticism += action.m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
void Personality::Reset() {
	m_openness = 0.5f;
	m_conscientiousness = 0.5f;
	m_extraversion = 0.5f;
	m_agreeableness = 0.5f;
	m_neuroticism = 0.5f;
}


//-----------------------------------------------------------------------------------------------
float Personality::GetPersonalityScore() const {
	return m_openness * 10000.f + m_conscientiousness * 1000.f
		+ m_extraversion * 100.f + m_agreeableness * 10.f + m_neuroticism;
}


//-----------------------------------------------------------------------------------------------
float& Personality::operator[](int index){
	switch (index){
		case 0:
			return m_openness;
		case 1:
			return m_conscientiousness;
		case 2:
			return m_extraversion;
		case 3:
			return m_agreeableness;
		case 4:
			return m_neuroticism;
		default:
			return m_openness;
	}
}


//-----------------------------------------------------------------------------------------------
std::string Personality::GetTraitName(int index) const{
	switch (index) {
	case 0:
		return "Openness";
	case 1:
		return "Conscientiousness";
	case 2:
		return "Extraversion";
	case 3:
		return "Agreeableness";
	case 4:
		return "Neuroticism";
	}
	return "";
}


//-----------------------------------------------------------------------------------------------
PersonalityAction Personality::ConvertToAction() const {
	return PersonalityAction(m_openness, m_conscientiousness,
		m_extraversion, m_agreeableness, m_neuroticism);
}


//-----------------------------------------------------------------------------------------------
bool Personality::operator<(const Personality& rhs) const {
	return GetPersonalityScore() < rhs.GetPersonalityScore();
}


//-----------------------------------------------------------------------------------------------
QLearningWithPersonality::QLearningWithPersonality(std::vector<PersonalityAction> actions, float learningRate, float rewardDecay, 
	float greedy){
	m_actions = actions;
	m_learningRate = learningRate;
	m_gamma = rewardDecay;
	m_epsilon = greedy;
}


//-----------------------------------------------------------------------------------------------
std::vector<PersonalityAction> QLearningWithPersonality::InitializeAllPossiblePeronalityActions(){
	std::vector<PersonalityAction> actions;
	for(float valueChange = -0.1f; valueChange <= 0.1f; valueChange += 0.1f){
		if(valueChange == 0.f){
			continue;
		}
		actions.push_back(PersonalityAction(valueChange, 0.f, 0.f, 0.f, 0.f));
		actions.push_back(PersonalityAction(0.f, valueChange, 0.f, 0.f, 0.f));
		actions.push_back(PersonalityAction(0.f, 0.f, valueChange, 0.f, 0.f));
		actions.push_back(PersonalityAction(0.f, 0.f, 0.f, valueChange, 0.f));
		actions.push_back(PersonalityAction(0.f, 0.f, 0.f, 0.f, valueChange));
	}
	return actions;
}



//-----------------------------------------------------------------------------------------------
PersonalityAction QLearningWithPersonality::ChooseAction(Personality state){
	float greeyPosibility = g_rng->GetRandomFloatInRange(0.f, 1.f);
	if(!CheckStateExist(state) || greeyPosibility >= m_epsilon){
		int randomIndex = g_rng->GetRandomIntInRange(0, (int)m_actions.size() - 1);
		return m_actions[randomIndex];
	}
	std::map<PersonalityAction, float>& stateAction = m_qTable[state];
	float maxValue = -1;
	auto maxAction = stateAction.begin();
	for(auto actionValue = stateAction.begin(); actionValue != stateAction.end(); actionValue++){
		if(actionValue->second > maxValue){
			PersonalityAction action = g_theGame->m_currentPersonalityState.ConvertToAction();
			action += actionValue->first;
			if(!action.IsValid()){
				continue;
			}
			maxValue = actionValue->second;
			maxAction = actionValue;
		}
		else if(actionValue->second == maxValue){
			float randomPickAnotherOne = g_rng->GetRandomFloatInRange(0.f, 1.f);
			if(randomPickAnotherOne >= 0.5f){
				PersonalityAction action = g_theGame->m_currentPersonalityState.ConvertToAction();
				action += actionValue->first;
				if (!action.IsValid()) {
					continue;
				}
				maxValue = actionValue->second;
				maxAction = actionValue;

			}
		}
	}
	return maxAction->first;
}


//-----------------------------------------------------------------------------------------------
void QLearningWithPersonality::Learn(Personality state, Personality newState, PersonalityAction action, float reward){
	CheckStateExist(newState);
	float predict = m_qTable[state][action];
	float actual = reward + m_gamma * 0.5f;
	m_qTable[state][action] += m_learningRate * (actual - predict);

	if(actual > m_highestScore){
		m_highestScore = actual;
		m_highestScoreState = newState;
	}
}


//-----------------------------------------------------------------------------------------------
bool QLearningWithPersonality::CheckStateExist(Personality state){
	if(m_qTable.find(state) == m_qTable.end()){
		m_qTable[state] = InitializeEmptyStateTable();
		return false;
	}
	return true;
}


//-----------------------------------------------------------------------------------------------
std::map<PersonalityAction, float> QLearningWithPersonality::InitializeEmptyStateTable() const{
	std::map<PersonalityAction, float> emptyTable;
	for(PersonalityAction action: m_actions){
		emptyTable[action] = 0.5f;
	}
	return emptyTable;
}


//-----------------------------------------------------------------------------------------------
QLearningWithValence::QLearningWithValence(std::vector<ValenceAction> actions, 
	float learningRate, float rewardDecay, float greedy){
	m_actions = actions;
	m_learningRate = learningRate;
	m_gamma = rewardDecay;
	m_epsilon = greedy;
}


//-----------------------------------------------------------------------------------------------
std::vector<ValenceAction> QLearningWithValence::InitializeAllPossibleValenceActions(){
	std::vector<ValenceAction> actions;
	for (float valueChange = -0.1f; valueChange <= 0.1f; valueChange += 0.1f) {
		if (valueChange == 0.f) {
			continue;
		}
		for(int actionIndex = 0; actionIndex < (int)s_actions.size(); actionIndex++){
			actions.push_back(ValenceAction(actionIndex, valueChange));
		}
	}
	return actions;
}


//-----------------------------------------------------------------------------------------------
ValenceAction QLearningWithValence::ChooseAction(Valence state){
	float greeyPosibility = g_rng->GetRandomFloatInRange(0.f, 1.f);
	if (!CheckStateExist(state) || greeyPosibility >= m_epsilon) {
		int randomIndex = g_rng->GetRandomIntInRange(0, (int)m_actions.size() - 1);
		return m_actions[randomIndex];
	}
	std::map<ValenceAction, float>& stateAction = m_qTable[state];
	float maxValue = -1;
	auto maxAction = stateAction.begin();
	for (auto actionValue = stateAction.begin(); actionValue != stateAction.end(); actionValue++) {
		if (actionValue->second > maxValue) {
			Valence currentState = g_theGame->m_currentValenceState;
			currentState.AddAction(actionValue->first);
			if (!currentState.IsValid()) {
				continue;
			}
			maxValue = actionValue->second;
			maxAction = actionValue;
		}
		else if (actionValue->second == maxValue) {
			float randomPickAnotherOne = g_rng->GetRandomFloatInRange(0.f, 1.f);
			if (randomPickAnotherOne >= 0.5f) {
				Valence currentState = g_theGame->m_currentValenceState;
				currentState.AddAction(actionValue->first);
				if (!currentState.IsValid()) {
					continue;
				}
				maxValue = actionValue->second;
				maxAction = actionValue;

			}
		}
	}
	return maxAction->first;
}


//-----------------------------------------------------------------------------------------------
void QLearningWithValence::Learn(Valence state, Valence newState, ValenceAction action, 
	float reward){
	CheckStateExist(newState);
	float predict = m_qTable[state][action];
	float actual = reward + m_gamma * 0.5f;
	m_qTable[state][action] += m_learningRate * (actual - predict);

	if (actual > m_highestScore) {
		m_highestScore = actual;
		m_highestScoreState = newState;
	}
}


//-----------------------------------------------------------------------------------------------
bool QLearningWithValence::CheckStateExist(Valence state){
	if (m_qTable.find(state) == m_qTable.end()) {
		m_qTable[state] = InitializeEmptyStateTable();
		return false;
	}
	return true;
}


//-----------------------------------------------------------------------------------------------
std::map<ValenceAction, float> QLearningWithValence::InitializeEmptyStateTable() const{
	std::map<ValenceAction, float> emptyTable;
	for (ValenceAction action : m_actions) {
		emptyTable[action] = 0.5f;
	}
	return emptyTable;
}


//-----------------------------------------------------------------------------------------------
ValenceParticle::ValenceParticle(PSOWithValence* owner, int index) {
	m_owner = owner;
	m_index = index;
	InitializeRandomly();
}


//-----------------------------------------------------------------------------------------------
void ValenceParticle::Simulate(float deltaSeconds){
	g_APF->m_anpcs.clear();
	g_APF->m_praises.clear();
	g_APF->m_socials.clear();
	g_APF->m_anpcsMentalState.clear();
	if (m_level) {
		delete m_level;
		m_level = nullptr;
	}

	m_level = new Level(g_theGame, m_currentValence.m_praises, m_index);
	while(!m_level->m_gameComplete){
		m_level->Update(deltaSeconds);
	}

	float characterScore = m_owner->m_heuristicFunction(m_level->m_anpcCharacter->m_anpcActions,
		m_level->m_anpcCharacter->m_stats, m_level->GetTotalAllyStats(), m_level->m_doesAIWinTheBattle);

	m_currentScore = characterScore;
	if(m_bestScore < m_currentScore){
		m_bestScore = m_currentScore;
		m_bestValence = m_currentValence;
	}
	if (m_level) {
		delete m_level;
		m_level = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void ValenceParticle::PaticleMove(float deltaSeconds){
	for (int index = 0; index < m_owner->m_particleDimension; index++) {
		float randomFloat1 = g_rng->GetRandomFloatZeroToOne();
		float randomFloat2 = g_rng->GetRandomFloatZeroToOne();
		m_currentVelocity[index] = (m_currentVelocity[index] * m_owner->m_weight
			+ m_owner->m_c1 * randomFloat1 * (m_bestValence.m_praises[index] - m_currentValence.m_praises[index])
			+ m_owner->m_c1 * randomFloat2 * (m_owner->m_globalValence.m_praises[index] - m_currentValence.m_praises[index]))
			* deltaSeconds;

		m_currentVelocity[index] = Clamp(m_currentVelocity[index], m_owner->m_minVelocity,
			m_owner->m_maxVelocity);
		m_currentValence.m_praises[index] = Clamp(m_currentValence.m_praises[index] + 
			m_currentVelocity[index], m_owner->m_minValence, m_owner->m_maxValence);
	}
}


//-----------------------------------------------------------------------------------------------
void ValenceParticle::InitializeRandomly(){
	for(int index = 0; index< m_owner->m_particleDimension; index++){
		float praise = g_rng->GetRandomFloatInRange(m_owner->m_minValence,
			m_owner->m_maxValence);
		m_currentValence.m_praises[index] = praise;
		float velocity = g_rng->GetRandomFloatInRange(m_owner->m_minVelocity,
			m_owner->m_maxVelocity);
		m_currentVelocity.push_back(velocity);
	}
	m_bestValence = m_currentValence;
}


//-----------------------------------------------------------------------------------------------
PersonalityParticle::PersonalityParticle(PSOWithPersonality* owner, int index){
	m_owner = owner;
	m_index = index;
	InitializeRandomly();
}


//-----------------------------------------------------------------------------------------------
void PersonalityParticle::Simulate(float deltaSeconds){
	g_APF->m_anpcs.clear();
	g_APF->m_praises.clear();
	g_APF->m_socials.clear();
	g_APF->m_anpcsMentalState.clear();
	m_level = new Level(g_theGame, g_theGame->m_psoValence->m_globalValence.m_praises, m_index);
	while (!m_level->m_gameComplete) {
		m_level->Update(deltaSeconds);
	}

// 	float valenceChange = GetPraiseDifference(g_theGame->m_psoValence->m_globalValence.m_praises,
// 		g_APF->GetPraises(m_level->m_anpcCharacter->m_name.c_str()));
	float totalValenceChange = m_level->m_anpcCharacter->m_totalValenceChange;
	g_personalityActionConnection->GetIdealPersonality(g_theGame->m_psoValence->m_globalValence.m_praises);

	m_currentScore = RangeMapClamped(totalValenceChange, 0.f, 1.f, 1.f, 0.f);
	if (m_bestScore < m_currentScore) {
		m_bestScore = m_currentScore;
		m_bestPersonality = m_currentPersonality;
	}
	if (m_level) {
		delete m_level;
	}
}


//-----------------------------------------------------------------------------------------------
void PersonalityParticle::PaticleMove(float deltaSeconds){
	for (int index = 0; index < m_owner->m_particleDimension; index++) {
		float randomFloat1 = g_rng->GetRandomFloatZeroToOne();
		float randomFloat2 = g_rng->GetRandomFloatZeroToOne();
		m_currentVelocity[index] = (m_currentVelocity[index] * m_owner->m_weight
			+ m_owner->m_c1 * randomFloat1 * (m_bestPersonality[index] - m_currentPersonality[index])
			+ m_owner->m_c1 * randomFloat2 * (m_owner->m_globalPersonality[index] - m_currentPersonality[index]))
			* deltaSeconds;

		m_currentVelocity[index] = Clamp(m_currentVelocity[index], m_owner->m_minVelocity,
			m_owner->m_maxVelocity);
		m_currentPersonality[index] = Clamp(m_currentPersonality[index] +
			m_currentVelocity[index], m_owner->m_minTrait, m_owner->m_maxTrait);
	}
}


//-----------------------------------------------------------------------------------------------
void PersonalityParticle::InitializeRandomly(){
	for(int index = 0; index < 5; index++){
		m_currentPersonality[index] = g_rng->GetRandomFloatInRange(m_owner->m_minTrait,
			m_owner->m_maxTrait);
		float velocity = g_rng->GetRandomFloatInRange(m_owner->m_minVelocity,
			m_owner->m_maxVelocity);
		m_currentVelocity.push_back(velocity);
	}

	m_bestPersonality = m_bestScore;
}


//-----------------------------------------------------------------------------------------------
PSOWithValence::PSOWithValence(int numOfParticles){
	m_numOfParticles = numOfParticles;
	m_particleDimension = (int)s_actions.size();
	InitializeRandomSwarms();
}


//-----------------------------------------------------------------------------------------------
void PSOWithValence::Update(float deltaSeconds){
	if( m_isPaused){
		return;
	}
	else if(m_simulatomComplete){
		RandomRestart();
	}

	m_currentParticleIndex = (m_currentParticleIndex + 1) == m_numOfParticles ? 0 : m_currentParticleIndex;
	int startIndex = m_currentParticleIndex;
	int endIndex = (m_currentParticleIndex + m_numOfSimulationsEachFrame) < m_numOfParticles ?
		(m_currentParticleIndex + m_numOfSimulationsEachFrame) : m_numOfParticles;

	for (int swarmIndex = startIndex; swarmIndex < endIndex; swarmIndex++) {
		m_currentParticleIndex = swarmIndex;
		m_particles[swarmIndex].Simulate(deltaSeconds);
		if (m_globalScore < m_particles[swarmIndex].m_bestScore) {
			m_globalScore = m_particles[swarmIndex].m_bestScore;
			m_globalValence = m_particles[swarmIndex].m_bestValence;
		}
		if(m_particles[swarmIndex].m_bestScore > 0.75f && 
			!IsNewPraiseRecorded(m_highPraises, m_particles[swarmIndex].m_bestValence.m_praises)){
			m_highPraises.push_back(m_particles[swarmIndex].m_bestValence.m_praises);
		}
	}
	if(endIndex == m_numOfParticles){
		for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
			m_particles[swarmIndex].PaticleMove(deltaSeconds);
		}
		m_currentGeneration++;
		if (m_currentGeneration >= m_maxGenerations) {
			m_simulatomComplete = true;
		}
	}
}


//-----------------------------------------------------------------------------------------------
Level* PSOWithValence::GetParticleLevel(int swarmIndex) const{
	return m_particles[swarmIndex].m_level;
}


//-----------------------------------------------------------------------------------------------
Level* PSOWithValence::GetRunningLevel() const{
	return m_particles[m_currentParticleIndex].m_level;
}


//-----------------------------------------------------------------------------------------------
void PSOWithValence::SaveHighScorePraises() const{
	std::string fileName = "Saves/ValenceData.csv";
	std::ofstream savedData;
	savedData.open(fileName);


	std::string allActions = "";
	for (auto it = s_actions.begin(); it != s_actions.end(); it++) {
		allActions += it->first + ",";
	}
	allActions += '\n';
	savedData<<allActions;

	for (int index = 0; index < (int)m_highPraises.size(); index++){
		std::vector<float> praises = m_highPraises[index];
		std::string valenceString = "";
		for (float praise : praises) {
			valenceString += std::to_string(praise).substr(0, std::to_string(praise).find('.') + 4) + ',' ;
		}
		valenceString += '\n';
		savedData << valenceString;
	}

	savedData.close();
}


//-----------------------------------------------------------------------------------------------
void PSOWithValence::InitializeRandomSwarms(){
	for(int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++){
		ValenceParticle newSwarm(this, swarmIndex);
		m_particles.push_back(newSwarm);
	}
}


//-----------------------------------------------------------------------------------------------
void PSOWithValence::RandomRestart(){
	m_simulatomComplete = false;
	m_currentGeneration = 0;
	m_currentParticleIndex = 0;

	// random restart with best particle 
// 	for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
// 		if(m_particles[swarmIndex].m_bestScore == m_globalScore){
// 			ValenceParticle remainingParticle = m_particles[swarmIndex];
// 			remainingParticle.m_index = 0;
// 			m_particles.clear();
// 			m_particles.push_back(remainingParticle);
// 			break;
// 		}
// 	}
// 	for (int swarmIndex = 1; swarmIndex < m_numOfParticles; swarmIndex++) {
// 		ValenceParticle newSwarm(this, swarmIndex);
// 		m_particles.push_back(newSwarm);
// 	}

	// random restart all particles
	m_particles.clear();
	for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
		ValenceParticle newSwarm(this, swarmIndex);
		m_particles.push_back(newSwarm);
	}
}


//-----------------------------------------------------------------------------------------------
PSOWithPersonality::PSOWithPersonality(int numOfParticles){
	m_numOfParticles = numOfParticles;
	m_particleDimension = 5;
	InitializeRandomSwarms();
}


//-----------------------------------------------------------------------------------------------
void PSOWithPersonality::Update(float deltaSeconds){
	if (m_isPaused) {
		return;
	}
	else if (m_simulatomComplete) {
		RandomRestart();
	}

	m_currentParticleIndex = (m_currentParticleIndex + 1) == m_numOfParticles ? 0 : m_currentParticleIndex;
	int startIndex = m_currentParticleIndex;
	int endIndex = (m_currentParticleIndex + m_numOfSimulationsEachFrame) < m_numOfParticles ?
		(m_currentParticleIndex + m_numOfSimulationsEachFrame) : m_numOfParticles;

	for (int swarmIndex = startIndex; swarmIndex < endIndex; swarmIndex++) {
		m_currentParticleIndex = swarmIndex;
		m_particles[swarmIndex].Simulate(deltaSeconds);
		if (m_globalScore < m_particles[swarmIndex].m_bestScore) {
			m_globalScore = m_particles[swarmIndex].m_bestScore;
			m_globalPersonality = m_particles[swarmIndex].m_bestPersonality;
		}
	}
	if(endIndex == m_numOfParticles){
		for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
			m_particles[swarmIndex].PaticleMove(deltaSeconds);
		}
		m_currentGeneration++;
		if (m_currentGeneration >= m_maxGenerations) {
			m_simulatomComplete = true;
		}
	}
}


//-----------------------------------------------------------------------------------------------
Level* PSOWithPersonality::GetParticleLevel(int swarmIndex) const{
	return m_particles[swarmIndex].m_level;
}


//-----------------------------------------------------------------------------------------------
Level* PSOWithPersonality::GetRunningLevel() const{
	return m_particles[m_currentParticleIndex].m_level;
}


//-----------------------------------------------------------------------------------------------
Personality PSOWithPersonality::GetCurrentPersonality() const{
	return m_particles[m_currentParticleIndex].m_currentPersonality;
}


//-----------------------------------------------------------------------------------------------
void PSOWithPersonality::InitializeRandomSwarms(){
	for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
		PersonalityParticle newSwarm(this, swarmIndex);
		m_particles.push_back(newSwarm);
	}
}


//-----------------------------------------------------------------------------------------------
void PSOWithPersonality::RandomRestart(){
	m_simulatomComplete = false;
	m_currentGeneration = 0;
	m_currentParticleIndex = 0;

	for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
		if (m_particles[swarmIndex].m_bestScore == m_globalScore) {
			PersonalityParticle remainingParticle = m_particles[swarmIndex];
			remainingParticle.m_index = 0;
			m_particles.clear();
			m_particles.push_back(remainingParticle);
			break;
		}
	}
	for (int swarmIndex = 1; swarmIndex < m_numOfParticles; swarmIndex++) {
		PersonalityParticle newSwarm(this, swarmIndex);
		m_particles.push_back(newSwarm);
	}

// 	m_particles.clear();
// 	for (int swarmIndex = 0; swarmIndex < m_numOfParticles; swarmIndex++) {
// 		PersonalityParticle newSwarm(this, swarmIndex);
// 		m_particles.push_back(newSwarm);
// 	}
}
