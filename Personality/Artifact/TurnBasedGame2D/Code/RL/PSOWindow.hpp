#pragma once
#include <vector>

class PSOWithValence;
class PSOWithPersonality;

class PSOWindow{
public:
	PSOWindow(PSOWithValence* valence, PSOWithPersonality* personality);
	~PSOWindow(){}
	void Update();
public:	
	PSOWithValence* m_psoValence = nullptr;
	PSOWithPersonality* m_psoPersonality = nullptr;

	int m_generation = 50;
	int m_swarms = 20;

	float m_c1 = 2.f;
	float m_c2 = 2.f;
	float m_weight = 0.5f;

	float m_minVelocity = -0.1f;
	float m_maxVelocity = 0.1f;
	float m_minValence = 0.0f;
	float m_maxValence = 1.f;

	bool m_isHeuristicSelected = false;
	bool m_recordHighScoreValues = false;
	int m_selectedHeuritiscIndex = 0;
protected:
	void UpdateValenceTraining();
	void UpdatePersonalityTraining();
};

