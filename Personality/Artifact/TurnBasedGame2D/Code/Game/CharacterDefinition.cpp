#include "Game/CharacterDefinition.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Engine/Core/XmlUtils.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/StringUtils.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Renderer/SpriteAnimDefinition.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"


//------------------------------------------------------------------------------------------------
std::map<std::string, CharacterDefinition> CharacterDefinition::s_charDefinitions;

//------------------------------------------------------------------------------------------------
CharacterDefinition::CharacterDefinition(){
}

//------------------------------------------------------------------------------------------------
CharacterDefinition::~CharacterDefinition(){
	
}


//------------------------------------------------------------------------------------------------
void CharacterDefinition::InitializeCharacterDefinitions(){
	s_charDefinitions.clear();
	tinyxml2::XMLDocument charDefinition;
	std::string unitDefinitionXML = g_gameConfigBlackboard.GetValue("characterDefinition", "None");

	ASSERT_OR_DIE(unitDefinitionXML != "None", "Missing or invalid character definition XML name");

	charDefinition.LoadFile(unitDefinitionXML.c_str());
	tinyxml2::XMLNode* rootNode = charDefinition.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	while (element != nullptr)
	{
		ASSERT_OR_DIE(LoadFromXmlElement(*element),
			"Unable to load char definition");
		element = element->NextSiblingElement();
	}

}


//------------------------------------------------------------------------------------------------
void CharacterDefinition::ClearCharacterDefinitions(){
	for(auto iter = s_charDefinitions.begin(); iter != s_charDefinitions.end(); iter++){
		for(auto anim = iter->second.m_animations.begin(); anim != iter->second.m_animations.end();
			anim++){
			delete anim->second;	
		}
		iter->second.m_animations.clear();
	}
}

//------------------------------------------------------------------------------------------------
bool CharacterDefinition::LoadFromXmlElement(tinyxml2::XMLElement& elem){
	CharacterDefinition charDefinition;
	std::string name = ParseXmlAttribute(elem, "name", std::string("None"));
	// check duplicates
	ASSERT_OR_DIE(s_charDefinitions.find(name) == s_charDefinitions.end(),
		"Duplicate character definition names");
	ASSERT_OR_DIE(name != "None", "Missing or invalid character XML name");
	charDefinition.m_name = name;
	std::string behaviorTree = ParseXmlAttribute(elem, "behaviorTree", std::string("None"));
	ASSERT_OR_DIE(behaviorTree != "None", "Missing or invalid behavior tree");
	charDefinition.m_behaviorTree = behaviorTree;
	charDefinition.m_health = ParseXmlAttribute(elem, "health", 0.f);
	charDefinition.m_speed = ParseXmlAttribute(elem, "speed", 0.f);
	charDefinition.m_attackDamage = ParseXmlAttribute(elem, "attackDamage", 0.f);
	charDefinition.m_rangeAttackDamage = ParseXmlAttribute(elem, "rangeAttackDamage", 0.f);
	charDefinition.m_longAttackDamage = ParseXmlAttribute(elem, "longAttackDamage", 0.f);
	charDefinition.m_blockingDamagePercentage = ParseXmlAttribute(elem, "blockingDamagePercentage", 0.f);
	charDefinition.m_healDamage = ParseXmlAttribute(elem, "healDamage", 0.f);

	tinyxml2::XMLElement* unitElement = elem.FirstChildElement();
	
	while (unitElement) {
		std::string elementName = unitElement->Name();
		if (elementName == "Animation") {
			charDefinition.LoadAnimationElement(*unitElement);
		}
		unitElement = unitElement->NextSiblingElement();
	}

	s_charDefinitions[name] = charDefinition;
	return true;
}


//------------------------------------------------------------------------------------------------
void CharacterDefinition::LoadAnimationElement(tinyxml2::XMLElement& elem){
	std::string animationName = ParseXmlAttribute(elem, "name", std::string("None"));
	ASSERT_OR_DIE(animationName != "None", "Missing or invalid animation XML name");
	std::string animationType = ParseXmlAttribute(elem, "type", std::string("None"));
	SpriteAnimPlaybackType playType = SpriteAnimPlaybackType::ONCE;
	if(animationType == "loop"){
		playType = SpriteAnimPlaybackType::LOOP;
	}
	else if(animationType == "once"){
		playType = SpriteAnimPlaybackType::ONCE;
	}

	std::string animationIndexString = ParseXmlAttribute(elem, "index", std::string("None"));
	ASSERT_OR_DIE(animationIndexString != "None", "Missing or invalid animation index");
	Strings animationIndexes = SplitStringOnDelimiter(animationIndexString, ',');
	std::vector<int> indexes;
	for(std::string index:animationIndexes){
		indexes.push_back(stoi(index));
	}
	float duration = ParseXmlAttribute(elem, "duration", 0.f);
	SpriteAnimDefinition* anim = new SpriteAnimDefinition(*g_theGame->m_characterSpritesheet,
		indexes, duration, playType);
	m_animations[animationName] = anim;
}

