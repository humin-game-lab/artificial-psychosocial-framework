#pragma once
#include "Game/Character.hpp"

//-----------------------------------------------------------------------------------------------
class Tank : public Character
{
public:
	Tank(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,FacingDirection direction,
		Faction faction, bool isANPC, int speedVariance);
	~Tank();
	static void RegisterTankBehaviors();
	static bool FindTargetInRedZone(EventArgs& args);
public:
};


