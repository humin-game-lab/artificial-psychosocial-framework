#pragma once

struct Level;

//-----------------------------------------------------------------------------------------------
class DebugWindow
{
public:
	DebugWindow(Level* level);
	void Update(float deltaSeconds);
	void UpdateSettingWindow(float deltaSeconds);
	void UpdateStatsWindow(float deltaSeconds);
public:
	Level* m_level = nullptr;

	// game setting window
	float m_currentGameSpeed = 10.f;

	// stats window
	int m_currentSelectedCharInStatsWindow = 0;
};


