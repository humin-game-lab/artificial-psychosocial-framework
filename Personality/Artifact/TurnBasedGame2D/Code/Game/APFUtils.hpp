#pragma once
#include "ThirdParty/APF/APF/Relations.hpp"
#include "ThirdParty/APF/Memory/GameEvent.hpp"
#include "ThirdParty/APF/Memory/Memory.hpp"
#include "Memory/Connection.hpp"
#include "ThirdParty/TinyXML2/tinyxml2.h"
#include <vector>
#include <map>

//-----------------------------------------------------------------------------------------------
extern std::map<std::string, APF_Lib::TraitSocialRole> s_socials;
extern std::map<std::string, APF_Lib::MM::ActionInfo*> s_actions;
extern APF_Lib::PersonalityActionConnection* g_personalityActionConnection;

void LoadRelationsFromXML(char const*);
void LoadActionsFromXML(char const*);
void LoadConnectionsFromXML(char const*);
void LoadPraisesFromXML(char const*, int actorIndex);
void LoadConnection(tinyxml2::XMLElement* element);
std::vector<APF_Lib::MM::LTMConstNodeList> RankTracesByMentalValance(std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, 
	APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float const (&weights)[2]);
void GetTraceMentalValence(APF_Lib::MM::LTMConstNodeList const& trace, 
	APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float(&valences)[APF_Lib::NUM_VALENCE_FEATURES]);
void GenerateMentalSalientProspectMemoryFromTraces(APF_Lib::MM::Memory& result, 
	std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, APF_Lib::MentalState const& mental, 
	APF_Lib::MM::GetProspectMultiplierFunction funcPtr);

