#pragma once
#include "Game/GameCommon.hpp"

#include <vector>



//------------------------------------------------------------------------------------------------
struct CharacterUID
{
public:
	CharacterUID();
	CharacterUID(int index, int salt);

	void Invalidate();

	bool IsValid() const;
	int GetIndex() const;

	inline bool operator==(CharacterUID const& other) const { return m_data == other.m_data; }
	inline bool operator==(int data) const { return m_data == data; }
public:
	static int GetNextSalt(int curSalt);
	static CharacterUID const INVALID;

private:
	int m_data;
};


