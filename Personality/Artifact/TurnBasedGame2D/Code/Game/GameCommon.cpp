#include "Game/GameCommon.hpp"
#include "Engine/Math/MathUtils.hpp"

bool		g_debugDraw = false;
bool		g_noClip = false;
SoundID		g_gameSoundIDs[NUM_GAME_SOUNDS];
Texture*	g_gameTextures[NUM_GAME_TEXTURES];
BitmapFont* g_gameFonts[NUM_GAME_FONTS];



//-----------------------------------------------------------------------------------------------
void DebugDrawLine(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color) {
	// compute four corners of the line
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepFront + stepLeft;
	Vec2 frontRight = end + stepFront - stepLeft;
	Vec2 backLeft = start - stepFront + stepLeft;
	Vec2 backRight = start - stepFront - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	Vertex_PCU verts[NUM_VERTS];

	verts[0].m_position = Vec3(backRight.x, backRight.y, 0.f);
	verts[1].m_position = Vec3(backLeft.x, backLeft.y, 0.f);
	verts[2].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[3].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[4].m_position = Vec3(frontLeft.x, frontLeft.y, 0.f);
	verts[5].m_position = Vec3(backLeft.x, backLeft.y, 0.f);

	for (int vertIndex = 0; vertIndex < NUM_VERTS; vertIndex++) {
		verts[vertIndex].m_color = color;
	}
	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindTexture(nullptr);
	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);
}


//-----------------------------------------------------------------------------------------------
void DebugDrawDisc(Vec2 const& center, float radius, Rgba8 const& color) {
	constexpr int NUM_SIDES = 12;
	constexpr int NUM_TRIS = NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	// calculate outer positions 
	Vec2 outerPosition[NUM_SIDES];
	for (int sideIndex = 0; sideIndex < NUM_SIDES; sideIndex++) {
		float thetaDegree = DEGREE_PER_SIDE * sideIndex;
		outerPosition[sideIndex].x = center.x + radius * CosDegrees(thetaDegree);
		outerPosition[sideIndex].y = center.y + radius * SinDegrees(thetaDegree);
	}

	Vertex_PCU verts[NUM_VERTS];
	// draw triangles, ABC, A is the center, BC are outer points
	for (int vertIndex = 0; vertIndex < NUM_TRIS; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % NUM_SIDES;
		int firstVertIndex = vertIndex * 3 + 0;
		int secondVertIndex = vertIndex * 3 + 1;
		int thirdVertIndex = vertIndex * 3 + 2;

		verts[firstVertIndex].m_position = Vec3(center.x, center.y, 0);
		verts[firstVertIndex].m_color = color;

		verts[secondVertIndex].m_position = Vec3(outerPosition[startRadiusIndex].x, outerPosition[startRadiusIndex].y, 0);
		verts[secondVertIndex].m_color = color;

		verts[thirdVertIndex].m_position = Vec3(outerPosition[endRadiusIndex].x, outerPosition[endRadiusIndex].y, 0);
		verts[thirdVertIndex].m_color = color;
	}
	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindTexture(nullptr);
	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);
}


//-----------------------------------------------------------------------------------------------
void DebugDrawRing(Vec2 const& center, float radius, float thickness, Rgba8 const& color) {
	float halfTickness = 0.5f * thickness;
	float innerRadius = radius - halfTickness;
	float outerRadius = radius + halfTickness;

	constexpr int NUM_SIDES = 20;
	constexpr int NUM_TRIS = 2 * NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	Vertex_PCU verts[NUM_VERTS];

	for (int sideNum = 0; sideNum < NUM_SIDES; sideNum++) {
		// compute angle-related terms
		float startDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum);
		float endDegrees = DEGREE_PER_SIDE * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute inner and outer position
		Vec3 innerStartPos = Vec3(center.x + innerRadius * cosStart,
			center.y + innerRadius * sinStart, 0.f);
		Vec3 outerStartPos = Vec3(center.x + outerRadius * cosStart,
			center.y + outerRadius * sinStart, 0.f);
		Vec3 innerEndPos = Vec3(center.x + innerRadius * cosEnd,
			center.y + innerRadius * sinEnd, 0.f);
		Vec3 outerEndPos = Vec3(center.x + outerRadius * cosEnd,
			center.y + outerRadius * sinEnd, 0.f);

		// trapezoid is made of two triangles: ABC and DEF;
		// where A is inner end, B is inner start, C is outer start
		// D is inner end, E is outer start, F is outer end
		int vertIndexA = 6 * sideNum;
		int vertIndexB = 6 * sideNum + 1;
		int vertIndexC = 6 * sideNum + 2;
		int vertIndexD = 6 * sideNum + 3;
		int vertIndexE = 6 * sideNum + 4;
		int vertIndexF = 6 * sideNum + 5;

		// Set vertex positions and color for two triangles
		verts[vertIndexA].m_position = innerEndPos;
		verts[vertIndexB].m_position = innerStartPos;
		verts[vertIndexC].m_position = outerStartPos;
		verts[vertIndexA].m_color = color;
		verts[vertIndexB].m_color = color;
		verts[vertIndexC].m_color = color;

		verts[vertIndexD].m_position = innerEndPos;
		verts[vertIndexE].m_position = outerStartPos;
		verts[vertIndexF].m_position = outerEndPos;
		verts[vertIndexD].m_color = color;
		verts[vertIndexE].m_color = color;
		verts[vertIndexF].m_color = color;
	}

	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindTexture(nullptr);
	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);

}


//-----------------------------------------------------------------------------------------------
void DrawSlash(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color) {
	// compute four corners of the line
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float directionDegrees = displacement.GetOrientationDegrees();

	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepFront + stepLeft;
	Vec2 frontRight = end + stepFront - stepLeft;
	Vec2 backLeft = start - stepFront + stepLeft;
	Vec2 backRight = start - stepFront - stepLeft;

	Vec2 reversedStepFront = Vec2(stepFront.y, stepFront.x);

	// if one corner is higher than another, let another corner.y = higher corner.y.
	// then based on line's angle, calculate difference between x
	// which is 1/ sin(theta)
	if (frontLeft.y > frontRight.y) {
		float y_offset = frontLeft.y - frontRight.y;
		frontLeft.y = frontRight.y;
		backLeft.y -= y_offset;

		float sinValue = 1.f / SinDegrees(directionDegrees);
		float x_offset = y_offset * sinValue;
		backLeft.x -= x_offset;
		frontLeft.x -= x_offset;
	}
	else {
		float y_offset = frontRight.y - frontLeft.y;
		frontRight.y = frontLeft.y;
		backRight.y -= y_offset;

		float sinValue = 1.f / SinDegrees(directionDegrees);
		float x_offset = y_offset * sinValue;
		backRight.x += x_offset;
		frontRight.x += x_offset;
	}

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	Vertex_PCU verts[NUM_VERTS];

	verts[0].m_position = Vec3(backRight.x, backRight.y, 0.f);
	verts[1].m_position = Vec3(backLeft.x, backLeft.y, 0.f);
	verts[2].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[3].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[4].m_position = Vec3(frontLeft.x, frontLeft.y, 0.f);
	verts[5].m_position = Vec3(backLeft.x, backLeft.y, 0.f);

	for (int vertIndex = 0; vertIndex < NUM_VERTS; vertIndex++) {
		verts[vertIndex].m_color = color;
	}
	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);
}


//-----------------------------------------------------------------------------------------------
// Draw a shorter line without adding/subtracting stepfront to match circles (avoid overlappings)
void DrawShorterLine(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color) {
	// compute four corners of the line,
	// but now it doesn't add stepfront
	Vec2 displacement = end - start;
	Vec2 forwardDirection = displacement.GetNormalized();
	float halfTickness = 0.5f * thickness;
	Vec2 stepFront = forwardDirection * halfTickness;
	Vec2 stepLeft = stepFront.GetRotated90Degrees();
	Vec2 frontLeft = end + stepLeft;
	Vec2 frontRight = end - stepLeft;
	Vec2 backLeft = start + stepLeft;
	Vec2 backRight = start - stepLeft;

	// draw two triangles (br, bl, fr) (fr, fl, bl)
	constexpr int NUM_TRIS = 2;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	Vertex_PCU verts[NUM_VERTS];

	verts[0].m_position = Vec3(backRight.x, backRight.y, 0.f);
	verts[1].m_position = Vec3(backLeft.x, backLeft.y, 0.f);
	verts[2].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[3].m_position = Vec3(frontRight.x, frontRight.y, 0.f);
	verts[4].m_position = Vec3(frontLeft.x, frontLeft.y, 0.f);
	verts[5].m_position = Vec3(backLeft.x, backLeft.y, 0.f);

	for (int vertIndex = 0; vertIndex < NUM_VERTS; vertIndex++) {
		verts[vertIndex].m_color = color;
	}

	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);
}


//-----------------------------------------------------------------------------------------------
void DrawHalfRing(Vec2 const& center, float startAngleDegree, float radius,
	float thickness, Rgba8 const& color) {
	float halfTickness = 0.5f * thickness;
	float innerRadius = radius - halfTickness;
	float outerRadius = radius + halfTickness;

	constexpr int NUM_SIDES = 20;
	constexpr int NUM_TRIS = 2 * NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 180.f / (float)NUM_SIDES;

	Vertex_PCU verts[NUM_VERTS];

	for (int sideNum = 0; sideNum < NUM_SIDES; sideNum++) {
		// compute angle-related terms
		float startDegrees = startAngleDegree + DEGREE_PER_SIDE * static_cast<float>(sideNum);
		float endDegrees = startAngleDegree + DEGREE_PER_SIDE * static_cast<float>(sideNum + 1);
		float cosStart = CosDegrees(startDegrees);
		float sinStart = SinDegrees(startDegrees);
		float cosEnd = CosDegrees(endDegrees);
		float sinEnd = SinDegrees(endDegrees);

		// compute inner and outer position
		Vec3 innerStartPos = Vec3(center.x + innerRadius * cosStart,
			center.y + innerRadius * sinStart, 0.f);
		Vec3 outerStartPos = Vec3(center.x + outerRadius * cosStart,
			center.y + outerRadius * sinStart, 0.f);
		Vec3 innerEndPos = Vec3(center.x + innerRadius * cosEnd,
			center.y + innerRadius * sinEnd, 0.f);
		Vec3 outerEndPos = Vec3(center.x + outerRadius * cosEnd,
			center.y + outerRadius * sinEnd, 0.f);

		// trapezoid is made of two triangles: ABC and DEF;
		// where A is inner end, B is inner start, C is outer start
		// D is inner end, E is outer start, F is outer end
		int vertIndexA = 6 * sideNum;
		int vertIndexB = 6 * sideNum + 1;
		int vertIndexC = 6 * sideNum + 2;
		int vertIndexD = 6 * sideNum + 3;
		int vertIndexE = 6 * sideNum + 4;
		int vertIndexF = 6 * sideNum + 5;

		// Set vertex positions and color for two triangles
		verts[vertIndexA].m_position = innerEndPos;
		verts[vertIndexB].m_position = innerStartPos;
		verts[vertIndexC].m_position = outerStartPos;
		verts[vertIndexA].m_color = color;
		verts[vertIndexB].m_color = color;
		verts[vertIndexC].m_color = color;

		verts[vertIndexD].m_position = innerEndPos;
		verts[vertIndexE].m_position = outerStartPos;
		verts[vertIndexF].m_position = outerEndPos;
		verts[vertIndexD].m_color = color;
		verts[vertIndexE].m_color = color;
		verts[vertIndexF].m_color = color;
	}

	g_theRenderer->DrawVertexArray(NUM_VERTS, verts);
}


//-----------------------------------------------------------------------------------------------
void DrawCharacterS(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {

	if (thickness >= height || thickness >= width) {
		return;
	}

	float circleDiameter = height * 0.5f;
	float circleRadius = circleDiameter * 0.5f;
	float lineHalfLength = (width - circleDiameter) * 0.5f;
	float longerLineEdge = (width - circleDiameter * 0.33f) * 0.5f;

	/* S has five parts: three horizontal lines, two half circles
	   each half circle has radius height/4, diameter of height/2,
	   so two circles = 2 * diameter = height.
	   Since half circle also takes space horizontally(total half height),
	   each line only has length = width - halfheight
	   the top line has right side longer and bottom line has left side longer */
	DrawShorterLine(Vec2((center.x - lineHalfLength), (center.y + circleDiameter)),
		Vec2(center.x + longerLineEdge, (center.y + circleDiameter)), thickness, color);
	DrawShorterLine(Vec2((center.x - lineHalfLength), center.y),
		Vec2((center.x + lineHalfLength), center.y), thickness, color);
	DrawShorterLine(Vec2(center.x - longerLineEdge, (center.y - circleDiameter)),
		Vec2(center.x + lineHalfLength, (center.y - circleDiameter)), thickness, color);

	DrawHalfRing(Vec2((center.x - lineHalfLength), (center.y + circleRadius)),
		90.f, circleRadius, thickness, color);
	DrawHalfRing(Vec2((center.x + lineHalfLength), (center.y - circleRadius)),
		270.f, circleRadius, thickness, color);
}


//-----------------------------------------------------------------------------------------------
void DrawCharacterT(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}

	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;

	// draw upper horizontal line and then middle vertical line
	DrawShorterLine(Vec2(center.x - halfwidth, center.y + halfheight),
		Vec2(center.x + halfwidth, center.y + halfheight), thickness, color);
	DebugDrawLine(Vec2(center.x, center.y - halfheight),
		Vec2(center.x, center.y + halfheight - thickness), thickness, color);

}


//-----------------------------------------------------------------------------------------------
void DrawCharacterA(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}

	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;
	float halfthickness = thickness * 0.5f;
	float thicknessOffset = thickness * 0.20f;
	float verticalThicknessOffset = thickness * 0.3f;

	//calculate A slashes degree using one slash x,y positions
	Vec2 slashDisplacement = Vec2(center.x, center.y + halfheight)
		- Vec2(center.x - halfwidth + halfthickness, center.y - halfheight);
	float slashDegrees = slashDisplacement.GetOrientationDegrees();
	float topTriangleHeight = halfheight + thickness;
	float bottomHeight = height - topTriangleHeight;
	float bottomSlashOffset = bottomHeight * (1.f / TanDegrees(slashDegrees));

	// Draw outer triangle passed color with and inner triangle with black color
	constexpr int TOP_TRIANGLE_VERTEXES = 6;
	Vertex_PCU topTriangle[TOP_TRIANGLE_VERTEXES] = {};
	topTriangle[0].m_position = Vec3(center.x, center.y + halfheight + halfthickness, 0.f);
	topTriangle[1].m_position = Vec3(center.x - halfwidth + bottomSlashOffset,
		center.y - thickness, 0.f);
	topTriangle[2].m_position = Vec3(center.x + halfwidth - bottomSlashOffset,
		center.y - thickness, 0.f);
	topTriangle[3].m_position = Vec3(center.x,
		center.y + (halfheight + halfthickness) * 0.3f, 0.f);
	topTriangle[4].m_position = Vec3(center.x - (halfwidth - bottomSlashOffset) * 0.3f,
		center.y, 0.f);
	topTriangle[5].m_position = Vec3(center.x + (halfwidth - bottomSlashOffset) * 0.3f,
		center.y, 0.f);
	topTriangle[0].m_color = color;
	topTriangle[1].m_color = color;
	topTriangle[2].m_color = color;
	topTriangle[3].m_color = Rgba8::BLACK;
	topTriangle[4].m_color = Rgba8::BLACK;
	topTriangle[5].m_color = Rgba8::BLACK;
	g_theRenderer->DrawVertexArray(TOP_TRIANGLE_VERTEXES, topTriangle);

	// draw two bottom slashes
	DrawSlash(Vec2(center.x - halfwidth + halfthickness + thicknessOffset, center.y - halfheight),
		Vec2(center.x - halfwidth + halfthickness + bottomSlashOffset + thicknessOffset,
			center.y - thickness - verticalThicknessOffset), thickness, color);
	DrawSlash(Vec2(center.x + halfwidth - halfthickness - thicknessOffset, center.y - halfheight),
		Vec2(center.x + halfwidth - halfthickness - bottomSlashOffset - thicknessOffset,
			center.y - thickness - verticalThicknessOffset), thickness, color);
}


//-----------------------------------------------------------------------------------------------
void DrawCharacterR(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}
	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;
	float halfThickness = thickness * 0.5f;

	float circleRadius = halfheight * 0.5f;
	float linesNearCircleLength = width - halfThickness - circleRadius;
	float leftLineXPosition = center.x - halfwidth + halfThickness;
	float slashOffset = linesNearCircleLength * 0.20f;
	float thicknessOffset = halfThickness * 0.4f;

	// draw the left side vertical line
	DebugDrawLine(Vec2(leftLineXPosition, center.y - halfheight),
		Vec2(leftLineXPosition, center.y + halfheight), thickness, color);

	// then draw top half ring with two extend lines
	DrawHalfRing(Vec2(leftLineXPosition + linesNearCircleLength, center.y + circleRadius),
		270.f, circleRadius, thickness, color);
	DrawShorterLine(Vec2(leftLineXPosition + halfThickness, center.y + halfheight),
		Vec2(leftLineXPosition + linesNearCircleLength, center.y + halfheight), thickness, color);
	DrawShorterLine(Vec2(leftLineXPosition + halfThickness, center.y),
		Vec2(leftLineXPosition + linesNearCircleLength, center.y), thickness, color);

	// draw bottom slash, add thickness offset for slash's bottom to match the actual height
	// add slash offset for x axis to move little bit to the left
	DrawSlash(Vec2(center.x + halfwidth - slashOffset, center.y - halfheight + thicknessOffset),
		Vec2(leftLineXPosition + linesNearCircleLength - slashOffset,
			center.y - halfThickness - thicknessOffset), thickness, color);
}


//-----------------------------------------------------------------------------------------------
void DrawCharacterH(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}
	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;
	float halfThickness = thickness * 0.5f;

	float leftLineXPosition = center.x - halfwidth + halfThickness;
	float rightLineXPosition = center.x + halfwidth - halfThickness;
	// draw left and right vertical lines, then draw the middle horizontal line
	DebugDrawLine(Vec2(leftLineXPosition, center.y - halfheight),
		Vec2(leftLineXPosition, center.y + halfheight), thickness, color);
	DebugDrawLine(Vec2(rightLineXPosition, center.y - halfheight),
		Vec2(rightLineXPosition, center.y + halfheight), thickness, color);
	DrawShorterLine(Vec2(leftLineXPosition + halfThickness, center.y),
		Vec2(rightLineXPosition - halfThickness, center.y), thickness, color);


}


//-----------------------------------------------------------------------------------------------
void DrawCharacterI(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}

	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;
	float halfthickness = thickness * 0.5f;

	// draw top and bottom horizontal lines, then draw the middle vertical line
	DebugDrawLine(Vec2(center.x - halfwidth, center.y + halfheight),
		Vec2(center.x + halfwidth, center.y + halfheight), thickness, color);
	DebugDrawLine(Vec2(center.x - halfwidth, center.y - halfheight),
		Vec2(center.x + halfwidth, center.y - halfheight), thickness, color);
	DrawShorterLine(Vec2(center.x, center.y - halfheight + halfthickness),
		Vec2(center.x, center.y + halfheight - halfthickness), thickness, color);
}


//-----------------------------------------------------------------------------------------------
void DrawCharacterP(Vec2 const& center, float height, float width,
	float thickness, Rgba8 const& color) {
	if (thickness >= height || thickness >= width) {
		return;
	}
	float halfwidth = width * 0.5f;
	float halfheight = height * 0.5f;
	float halfThickness = thickness * 0.5f;

	float circleRadius = halfheight * 0.5f;
	float linesNearCircleLength = width - halfThickness - circleRadius;
	float leftLineXPosition = center.x - halfwidth + halfThickness;

	// same procedure like R but no bottom slash
	// draw the left side vertical line
	DebugDrawLine(Vec2(leftLineXPosition, center.y - halfheight),
		Vec2(leftLineXPosition, center.y + halfheight), thickness, color);

	// then draw top half ring with two extend lines
	DrawHalfRing(Vec2(leftLineXPosition + linesNearCircleLength, center.y + circleRadius),
		270.f, circleRadius, thickness, color);
	DrawShorterLine(Vec2(leftLineXPosition + halfThickness, center.y + halfheight),
		Vec2(leftLineXPosition + linesNearCircleLength, center.y + halfheight), thickness, color);
	DrawShorterLine(Vec2(leftLineXPosition + halfThickness, center.y),
		Vec2(leftLineXPosition + linesNearCircleLength, center.y), thickness, color);

}


//-----------------------------------------------------------------------------------------------
void DrawDiscForLoaclVertexes(Vertex_PCU* verts, Vec2 const& center,
	float radius, Rgba8 const& color) {
	constexpr int NUM_SIDES = 16;
	constexpr int NUM_TRIS = NUM_SIDES;
	constexpr int NUM_VERTS = 3 * NUM_TRIS;
	constexpr float DEGREE_PER_SIDE = 360.f / (float)NUM_SIDES;

	Vec2 outerPosition[NUM_SIDES];
	for (int traingleIndex = 0; traingleIndex < NUM_TRIS; traingleIndex++) {
		float angleDegrees = traingleIndex * DEGREE_PER_SIDE;
		outerPosition[traingleIndex] = Vec2::MakeFromPolarDegrees(angleDegrees, radius);
	}
	for (int vertIndex = 0; vertIndex < NUM_TRIS; vertIndex++) {
		int startRadiusIndex = vertIndex;
		int endRadiusIndex = (vertIndex + 1) % NUM_TRIS;
		int firstVertIndex = vertIndex * 3 + 0;
		int secondVertIndex = vertIndex * 3 + 1;
		int thirdVertIndex = vertIndex * 3 + 2;

		verts[firstVertIndex].m_position = Vec3(center.x, center.y, 0);
		verts[firstVertIndex].m_color = color;

		verts[secondVertIndex].m_position = Vec3(outerPosition[startRadiusIndex].x + center.x,
			outerPosition[startRadiusIndex].y + center.y, 0);
		verts[secondVertIndex].m_color = color;

		verts[thirdVertIndex].m_position = Vec3(outerPosition[endRadiusIndex].x + center.x,
			outerPosition[endRadiusIndex].y + center.y, 0);
		verts[thirdVertIndex].m_color = color;
	}
}
