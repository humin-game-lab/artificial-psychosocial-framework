#pragma once
#include "Engine/Core/EventSystem.hpp"

struct Game;
class QLearningWithPersonality;
class QLearningWithValence;
class PSOWithValence;
class RLWindow;

//-----------------------------------------------------------------------------------------------
struct App
{

public:
	// Construction/Destruction
	~App();												// destructor (do nothing)
	App();												// default constructor (do nothing)

	void Startup();
	void Run();
	void RunFrame();
	void Shutdown();

	bool IsQuitting() const;

	void HandleQuitRequest();
	static bool EventQuit(EventArgs& args);

public:
	

private:
	void BeginFrame();
	void Update();
	void EndFrame();
	void Render() const;

	void HandleKeyActivities();
	void HandleControllerActivities();

private:
	bool	m_isQuitting = false;
};
