#pragma once

struct Level;

//-----------------------------------------------------------------------------------------------
class GameSettingWindow
{
public:
	GameSettingWindow(Level* level);
	void Update(float deltaSeconds);

public:
	Level* m_level = nullptr;

	float m_attackDamage = 1.f;
	float m_rangeAttackDamage = 1.f;
	float m_longAttackDamage = 1.f;
	float m_blockingDamagePercentage = 50.f;
	float m_healDamage = 1.f;
};


