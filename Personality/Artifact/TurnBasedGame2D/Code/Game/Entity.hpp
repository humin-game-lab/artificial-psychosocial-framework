#pragma once
#include "Engine/Math/Vec2.hpp"

struct Game;


//-----------------------------------------------------------------------------------------------
struct Entity
{
public:
	Entity(Game* const& game, Vec2 const& startingPosition);

	// virtual functions
	virtual void Render() const = 0;
	virtual void Update(float deltaSeconds) =0;
	virtual void Die();

	// Accessors (const methods)
	bool IsAlive() const;
	Vec2 const GetForwardNormal() const;
	void DebugRender() const;

public:
	Vec2	m_position;
	Vec2	m_velocity;
	float	m_orientationDegrees;
	float	m_angularVelocity;
	float	m_physicsRadius;
	float	m_cosmeticRadius;
	int		m_health;
	bool	m_isDead;
	bool	m_isGarbage;
	Game*	m_game;

};


