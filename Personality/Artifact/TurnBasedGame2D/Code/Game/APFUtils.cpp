#include "Game/APFUtils.hpp"
#include "Game/GameCommon.hpp"
#include "Engine/Core/XmlUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "ThirdParty/APF/APF/Trait.hpp"
#include "ThirdParty/APF/Memory/LongTermMemory.hpp"
#include "ThirdParty/TinyXML2/tinyxml2.h"

//-----------------------------------------------------------------------------------------------
std::map<std::string, APF_Lib::TraitSocialRole> s_socials;
std::map<std::string, APF_Lib::MM::ActionInfo*> s_actions;
APF_Lib::PersonalityActionConnection* g_personalityActionConnection;

//-----------------------------------------------------------------------------------------------
void LoadRelationsFromXML(char const* xmlFile){
	s_socials.clear();
	tinyxml2::XMLDocument socialDefinition;

	socialDefinition.LoadFile(xmlFile);
	tinyxml2::XMLNode* rootNode = socialDefinition.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	while (element != nullptr){
		std::string name = ParseXmlAttribute(*element, "name", std::string("None"));
		float liking = ParseXmlAttribute(*element, "liking", 0.f);
		float dominance = ParseXmlAttribute(*element, "dominance", 0.f);
		float solidarity = ParseXmlAttribute(*element, "solidarity", 0.f);
		float familarity = ParseXmlAttribute(*element, "familarity", 0.f);
		s_socials[name] = APF_Lib::TraitSocialRole(liking, dominance,solidarity, familarity);
		element = element->NextSiblingElement();
	}
}


//-----------------------------------------------------------------------------------------------
void LoadActionsFromXML(char const* xmlFile){
	s_actions.clear();
	tinyxml2::XMLDocument actionDefinition;

	actionDefinition.LoadFile(xmlFile);
	tinyxml2::XMLNode* rootNode = actionDefinition.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	while (element != nullptr) {
		std::string name = ParseXmlAttribute(*element, "name", std::string("None"));
		float effect = ParseXmlAttribute(*element, "effect", 0.f);
		APF_Lib::MM::ActionInfo* actionInfo = new APF_Lib::MM::ActionInfo(name,effect);
		s_actions[name] = actionInfo;
		element = element->NextSiblingElement();
	}
}


//-----------------------------------------------------------------------------------------------
void LoadConnectionsFromXML(char const* xmlFile){
	tinyxml2::XMLDocument connectionDefinition;

	connectionDefinition.LoadFile(xmlFile);
	tinyxml2::XMLNode* rootNode = connectionDefinition.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	while (element != nullptr) {
		LoadConnection(element);
		element = element->NextSiblingElement();
	}
}


//-----------------------------------------------------------------------------------------------
void LoadPraisesFromXML(char const* xmlFile, int actorIndex){
	tinyxml2::XMLDocument praiseDefinition;

	praiseDefinition.LoadFile(xmlFile);
	tinyxml2::XMLNode* rootNode = praiseDefinition.FirstChild();
	tinyxml2::XMLElement* element = rootNode->FirstChildElement();
	while (element != nullptr) {
		std::string name = ParseXmlAttribute(*element, "name", std::string("None"));
		float valence = ParseXmlAttribute(*element, "valence", 0.f);
		int actionIndex = g_APF->GetActionIndex(name.c_str());
		g_APF->AddPraise(actorIndex, actionIndex, valence);
		element = element->NextSiblingElement();
	}
}


//-----------------------------------------------------------------------------------------------
void LoadConnection(tinyxml2::XMLElement* element){
	std::string actionName = ParseXmlAttribute(*element, "actionName", std::string("None"));
	int actionIndex = s_actions[actionName]->apfIdx;

	tinyxml2::XMLElement* subElement = element->FirstChildElement();
	while (subElement != nullptr) {
		std::string traitName = ParseXmlAttribute(*subElement, "name", std::string("None"));
		float triatThreshold = ParseXmlAttribute(*subElement, "value", 0.f);
		float weight = ParseXmlAttribute(*subElement, "weight", 0.f);
		g_personalityActionConnection->AddConnection(traitName, actionName, actionIndex, triatThreshold,
			weight);
		subElement = subElement->NextSiblingElement();
	}
}


//-----------------------------------------------------------------------------------------------
std::vector<APF_Lib::MM::LTMConstNodeList> RankTracesByMentalValance(std::vector<APF_Lib::MM::LTMConstNodeList> 
	const& traces, APF_Lib::MM::GetProspectMultiplierFunction funcPtr, float const (&weights)[2]){
	std::vector<APF_Lib::MM::LTMConstNodeList> result;
	std::vector<float> valences;

	for (APF_Lib::MM::LTMConstNodeList const& trace : traces) {
		float vals[APF_Lib::NUM_VALENCE_FEATURES];
		GetTraceMentalValence(trace, funcPtr, vals);

		float val = vals[0] * weights[0] + vals[1] * weights[1];

		bool inserted = false;
		for (size_t i = 0; i < valences.size(); i++) {
			if (val > valences[i]) {
				valences.insert(valences.begin() + i, val);
				result.insert(result.begin() + i, trace);
				inserted = true;
				break;
			}
		}
		if (!inserted) {
			valences.push_back(val);
			result.push_back(trace);
		}
	}

	return result;
}



//-----------------------------------------------------------------------------------------------
void GetTraceMentalValence(APF_Lib::MM::LTMConstNodeList const& trace, 
	APF_Lib::MM::GetProspectMultiplierFunction funcPtr, 
	float(&valences)[APF_Lib::NUM_VALENCE_FEATURES]){
	if (trace.empty()) {
		valences[0] = valences[1] = 0.f;
		return;
	}
	else if (trace.size() == 1) {
		APF_Lib::MentalState const& ms = trace[0]->GetAverageMentalState();
		ms.GetValence(valences);
		return;
	}

	size_t totalNum = trace.size();
	float* multipliers = new float[(int)totalNum];
	for (size_t i = 0; i < totalNum; i++) {
		multipliers[i] = funcPtr(i, totalNum);
	}

	float accumulateMultiplier = multipliers[0] + multipliers[1];
	APF_Lib::MentalState::CombineMentalValences(trace[0]->GetAverageMentalState(), trace[1]->GetAverageMentalState(), 
		multipliers[0] / accumulateMultiplier, valences);
	for (size_t i = 2; i < totalNum; i++) {
		float newMultiplier = multipliers[i];
		APF_Lib::MentalState tempValence;
		tempValence.SetValence(valences);
		APF_Lib::MentalState::CombineMentalValences(tempValence, trace[i]->GetAverageMentalState(), 
			accumulateMultiplier / (accumulateMultiplier + newMultiplier), valences);
		accumulateMultiplier += newMultiplier;
	}

	delete[] multipliers;
}


//-----------------------------------------------------------------------------------------------
void GenerateMentalSalientProspectMemoryFromTraces(APF_Lib::MM::Memory& result, 
	std::vector<APF_Lib::MM::LTMConstNodeList> const& traces, 
	APF_Lib::MentalState const& mental, 
	APF_Lib::MM::GetProspectMultiplierFunction funcPtr){
	UNUSED(mental);
	float const (&weights)[2] = POSITIVE_WEIGHTS;
	std::vector<APF_Lib::MM::LTMConstNodeList> ranked = RankTracesByMentalValance(traces, funcPtr, weights);
	constexpr float rankMultiplier[3] = { .5f,.3f,.2f };
	if (traces.empty()) {
		return;
	}

	APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(result, ranked[0], funcPtr);
	if (traces.size() == 1) {
		return;
	}

	APF_Lib::MM::Memory rank1;
	APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(rank1, ranked[1], funcPtr);
	APF_Lib::MM::Memory::GetCombinedMemory(result, result, rank1, rankMultiplier[0] / (rankMultiplier[1] + rankMultiplier[0]));
	if (traces.size() == 2) {
		return;
	}

	APF_Lib::MM::Memory rank2;
	APF_Lib::MM::Memory::GenerateProspectMemoryFromTrace(rank2, ranked[2], funcPtr);
	APF_Lib::MM::Memory::GetCombinedMemory(result, rank2, result, rankMultiplier[2]);

}
