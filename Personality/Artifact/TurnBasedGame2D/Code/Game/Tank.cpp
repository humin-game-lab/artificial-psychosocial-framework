#include "Game/Tank.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"
#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "ThirdParty/Squirrel/RawNoise.hpp"

//-----------------------------------------------------------------------------------------------
Tank::Tank(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
	FacingDirection direction,Faction faction, bool isANPC, int speedVariance):Character(level, 
	gridIndex, position, unitDef, direction, faction, isANPC, speedVariance) {

}


//-----------------------------------------------------------------------------------------------
Tank::~Tank(){
	if (m_behaviorTree) {
		delete m_behaviorTree;
		m_behaviorTree = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Tank::RegisterTankBehaviors(){
	ActionNode::SubscribeActionEventFunction("FindTargetInRedZone", FindTargetInRedZone);
}


//-----------------------------------------------------------------------------------------------
bool Tank::FindTargetInRedZone(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	bool targetFound = false;
	if (character) {
		for(Character* randomChar : level->m_characters){
			if(character->m_faction != randomChar->m_faction && !randomChar->m_isDead
				&& !randomChar->m_isGarbage && level->IsCharacterInRedZone(randomChar)){
				character->m_target = randomChar;
				targetFound = true;
				float chanceToFindOtherTarget = Get1dNoiseZeroToOne(level->m_totalTurnCount,
					3);
				if(chanceToFindOtherTarget >= 0.5f){
					continue;
				}
				else{
					break;
				}
			}
		}
	}
	return targetFound;
}


