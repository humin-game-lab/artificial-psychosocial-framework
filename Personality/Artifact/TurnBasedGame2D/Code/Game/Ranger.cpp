#include "Game/Ranger.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"
#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"

//-----------------------------------------------------------------------------------------------
Ranger::Ranger(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
	FacingDirection direction,Faction faction, bool isANPC, int speedVariance):Character(level, 
	gridIndex, position, unitDef, direction, faction, isANPC, speedVariance) {

}


//-----------------------------------------------------------------------------------------------
Ranger::~Ranger(){
	if (m_behaviorTree) {
		delete m_behaviorTree;
		m_behaviorTree = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Ranger::RegisterRangerBehaviors(){
	ActionNode::SubscribeActionEventFunction("DetermineWhichAttackToUse", DetermineWhichAttackToUse);
}





//-----------------------------------------------------------------------------------------------
bool Ranger::DetermineWhichAttackToUse(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if(!character->m_target){
			return false;
		}
		int targetColumn = character->m_target->m_gridIndex % level->m_numOfGridRows;
		int startingIndex = targetColumn * level->m_numOfGridRows;
		int enemyCount = 0;
		for(int charIndex = startingIndex; charIndex < startingIndex + 4; charIndex++){
			if(level->FindCharacterByIndex(charIndex, character->m_target->m_faction)
				!= nullptr){
				enemyCount++;	
			}
		}
		if(enemyCount >= level->m_numOfGridRows-1){
			if (character->m_chosenAction == nullptr) {
				character->m_chosenAction = Character::RangeAttackAction;
			}
		}
		else{
			if (character->m_chosenAction == nullptr) {
				character->m_chosenAction = Character::LongAttackAction;
			}
		}
		return true;
	}
	return false;
}
