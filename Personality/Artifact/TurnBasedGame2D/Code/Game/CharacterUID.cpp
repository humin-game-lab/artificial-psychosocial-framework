#include "Game/CharacterUID.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"

//------------------------------------------------------------------------------------------------
CharacterUID const CharacterUID::INVALID;

//------------------------------------------------------------------------------------------------
CharacterUID::CharacterUID() {
	m_data = 0xffffffff;
}


//------------------------------------------------------------------------------------------------
CharacterUID::CharacterUID(int index, int salt) {
	ASSERT_OR_DIE(salt < 0xffff, "Invalid salt");
	ASSERT_OR_DIE(index <= 0xffff, "Invalid index");
	m_data = (salt << 16) | index;
}


//------------------------------------------------------------------------------------------------
void CharacterUID::Invalidate() {
	m_data = 0xffffffff;
}


//------------------------------------------------------------------------------------------------
bool CharacterUID::IsValid() const {
	return (*this == CharacterUID::INVALID);
}


//------------------------------------------------------------------------------------------------
int CharacterUID::GetIndex() const {
	return m_data & 0xffff;
}


//------------------------------------------------------------------------------------------------
int CharacterUID::GetNextSalt(int curSalt) {
	return	++curSalt;
}
