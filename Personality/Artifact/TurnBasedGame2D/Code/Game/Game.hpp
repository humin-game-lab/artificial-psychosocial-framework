#pragma once
#include "Game/GameCommon.hpp"
#include "RL/RLBrain.hpp"
#include "Engine/Core/Clock.hpp"

struct App;
struct Level;
class Character;
class SpriteSheet;
class DebugWindow;
class PSOWindow;
class GameSettingWindow;

//-----------------------------------------------------------------------------------------------
extern std::map<std::string, HeuristicFunctionPointer> g_heuristicFunctions;


//-----------------------------------------------------------------------------------------------
enum TRAIN_STATE {
	Q_LEARNING_PERSONALITY,
	Q_LEARNING_VALENCE,
	PSO_VALENCE,
	PSO_PERSONALITY,
	PSO_RENDER_SIMULATION,
	NORMAL_GAME,
	NUM_GAME_STATES
};


//-----------------------------------------------------------------------------------------------
struct Game
{
public:
	App* m_app = nullptr;
	TRAIN_STATE		m_trainState = PSO_VALENCE;
	Clock	m_gameClock;
	Clock	m_stateClock;
	Camera	m_screenCamera;
	Level* m_currentLevel = nullptr;
	SpriteSheet* m_characterSpritesheet = nullptr;

	QLearningWithPersonality* m_qLearningPersonality = nullptr;
	QLearningWithValence* m_qLearningValence = nullptr;
	PSOWithValence* m_psoValence = nullptr;
	PSOWithPersonality* m_psoPersonality = nullptr;
	RLWindow* m_learningWindow = nullptr;
	PSOWindow* m_psoWindow = nullptr;
	GameSettingWindow* m_gameSettingWindow = nullptr;

	bool m_renderGameSettingWindow = true;

	
	Personality m_currentPersonalityState;
	Personality m_previousPersonalityState;
	PersonalityAction m_currentPersonalityAction;
	Valence m_currentValenceState;
	Valence m_previousValenceState;
	ValenceAction m_currentValenceAction;
	float m_currentLearningScore = -1.f;
	int m_learningCount = 0;
public:
	// Construction/Destruction
	~Game();
	Game(App* const& owner);

	void Startup();
	void Render() const;
	void Update();
	void Shutdown();

	void EnterNewLevel();
	void SimulateLevelFromPSOBest();
	void ApplyValenceAndTrainPersonality();

	Level* GetSimulatingLevel(int index);
	Level* GetSimulatingLevel();
	Personality GetANPCPersonality() const;
private:
	// initialize game functions
	void InitializeAssets();
	void InitializeAPF();
	void InitializeHeuristicFunctions();

	// update functions
	void HandleKeyAcvities();

	// UI Render functions
	void RenderConsole() const;

	// Camera functions
	void UpdateCamera(float deltaSeconds);

private:
	bool	m_openDebugCamera = false;

	DebugWindow* m_debugWindow = nullptr;
	bool	m_renderDebugWindow = true;
	bool	m_renderRLWindow = false;
	bool	m_renderPSOWindow = true;
};




