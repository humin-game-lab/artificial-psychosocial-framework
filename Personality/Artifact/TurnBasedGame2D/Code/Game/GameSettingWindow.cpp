#include "Game/GameSettingWindow.hpp"
#include "Game/Level.hpp"
#include "Game/Character.hpp"
#include "Game/Game.hpp"
#include "ThirdParty/imgui/imgui.h"
#include "Engine/Core/EngineCommon.hpp"
#include <vector>

//-----------------------------------------------------------------------------------------------
GameSettingWindow::GameSettingWindow(Level* level){
	m_level = level;
}


//-----------------------------------------------------------------------------------------------
void GameSettingWindow::Update(float deltaSeconds){
	UNUSED(deltaSeconds);
	ImGui::Begin("Game Setting");
	ImGui::SliderFloat("Attack Damage Radio", &m_attackDamage, 0.f, 2.f);
	ImGui::SliderFloat("Range Attack Damage Radio", &m_rangeAttackDamage, 0.f, 2.f);
	ImGui::SliderFloat("Long Attack Damage Radio", &m_longAttackDamage, 0.f, 2.f);
	ImGui::SliderFloat("Heal Damage Radio", &m_healDamage, 0.f, 2.f);
	ImGui::SliderFloat("Blocking Percentage", &m_blockingDamagePercentage, 0.f, 100.f);

	std::vector<float> praises = g_APF->GetPraises(m_level->m_anpcCharacter->m_name.c_str());
	for(float praise : praises){
		std::string praiseString = std::to_string(praise);
		std::string praiseStringCut = praiseString.substr(praiseString.find('.'), 4);
		if(ImGui::TreeNode(praiseStringCut.c_str())){
			ImGui::TreePop();
		}
	}
	ImGui::End();
}
