#pragma once
#include "Game/App.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/Texture.hpp"
#include "Engine/Math/RandomNumberGenerator.hpp"
#include "Engine/Core/Window.hpp"
#include "Engine/Input/InputSystem.hpp"
#include "Engine/Audio/AudioSystem.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Renderer/BitmapFont.hpp"
#include "Engine/UI/UISystem.hpp"
#include "ThirdParty/APF/APF/APF_Lib.hpp"

// global game common assets
enum GameSound {
	GAME_ATTRACT_MODE_BACKGROUND_MUSIC,
	GAME_BACKGROUND_MUSIC,
	GAME_PAUSE_SOUND,
	GAME_UNPAUSE_SOUND,
	NUM_GAME_SOUNDS
};

enum GameTexture {
	GAME_ATTACT_SCREEN_TEXTURE,
	GAME_BACKGROUND_TEXTURE,
	GAME_CHARACTER_SPRITESHEET,
	NUM_GAME_TEXTURES
};

enum GameFont {
	GAME_FONT,
	NUM_GAME_FONTS
};


// global objects
extern App*						g_theApp;
extern Game*					g_theGame;
extern Renderer*			g_theRenderer;
extern RandomNumberGenerator*	g_rng;
extern InputSystem*				g_theInputSystem;
extern Window*					g_theWindowContext;
extern AudioSystem*				g_theAudioSystem;
extern UISystem*				g_theUISystem;
extern APF_Lib::APF*			g_APF;
extern bool						g_debugDraw;
extern bool						g_noClip;
extern SoundID					g_gameSoundIDs[NUM_GAME_SOUNDS];
extern Texture*					g_gameTextures[NUM_GAME_TEXTURES];
extern BitmapFont*				g_gameFonts[NUM_GAME_FONTS];
constexpr float POSITIVE_WEIGHTS[APF_Lib::NUM_VALENCE_FEATURES] = { .6f,.4f };
constexpr float NEGATIVE_WEIGHTS[APF_Lib::NUM_VALENCE_FEATURES] = { .4f,.6f };

void DebugDrawLine(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color);
void DebugDrawDisc(Vec2 const& center, float radius, Rgba8 const& color);
void DebugDrawRing(Vec2 const& center, float radius, float thickness, Rgba8 const& color);

void DrawSlash(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color);
void DrawShorterLine(Vec2 const& start, Vec2 const& end, float thickness, Rgba8 const& color);
void DrawHalfRing(Vec2 const& center, float startAngleDegree, float radius, float thickness,
	Rgba8 const& color);

void DrawCharacterS(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterT(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterA(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterR(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterH(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterI(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);
void DrawCharacterP(Vec2 const& center, float height, float width, float thickness,
	Rgba8 const& color);

void DrawDiscForLoaclVertexes(Vertex_PCU* verts, Vec2 const& center, float radius,
	Rgba8 const& color);


