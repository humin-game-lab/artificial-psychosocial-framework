#pragma once
#include "Game/Character.hpp"

//-----------------------------------------------------------------------------------------------
class Healer : public Character
{
public:
	Healer(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
		FacingDirection direction, Faction faction, bool isANPC, int speedVariance);
	~Healer();
	static void RegisterHealerBehaviors();
	static bool IsInRedZone(EventArgs& args);
	static bool CanRetreat(EventArgs& args);
	static bool IsAllyHealthBelow(EventArgs& args);
public:
};


