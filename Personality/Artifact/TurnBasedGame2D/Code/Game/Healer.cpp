#include "Game/Healer.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/DecoratorNode.hpp"

//-----------------------------------------------------------------------------------------------
Healer::Healer(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
	FacingDirection direction,Faction faction, bool isANPC, int speedVariance):Character(level, 
	gridIndex, position, unitDef, direction, faction, isANPC, speedVariance) {

}


//-----------------------------------------------------------------------------------------------
Healer::~Healer(){
	if (m_behaviorTree) {
		delete m_behaviorTree;
		m_behaviorTree = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Healer::RegisterHealerBehaviors(){
	//ActionNode::SubscribeActionEventFunction("FindTargetInRedZone", FindTargetInRedZone);
	DecoratorNode::SubscribeDecoratorEventFunction("IsInRedZone", IsInRedZone);
	DecoratorNode::SubscribeDecoratorEventFunction("CanRetreat", CanRetreat);
	DecoratorNode::SubscribeDecoratorEventFunction("IsAllyHealthBelow", IsAllyHealthBelow);
}


//-----------------------------------------------------------------------------------------------
bool Healer::IsInRedZone(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		return level->IsCharacterInRedZone(character);
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Healer::CanRetreat(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		return (character->GetMoveBackwardIndex() != -1);
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Healer::IsAllyHealthBelow(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	float belowHealthPercentage = args.GetValue(std::string("health"), 0.f);
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		float minHealthPercentage = 9999.f;
		int minIndex = -1;
		for(int charIndex = 0; charIndex < (int)level->m_characters.size();
			charIndex++){
			Character* allyChar = level->m_characters[charIndex];
			if(allyChar && !allyChar->m_isDead && !allyChar->m_isGarbage && allyChar->m_faction
				== character->m_faction && allyChar->GetHealthPercentage() < minHealthPercentage){
					minHealthPercentage = allyChar->GetHealthPercentage();
					minIndex = charIndex;
			}
		}
		if(minIndex != -1 && minHealthPercentage <= belowHealthPercentage){
			character->m_target = level->m_characters[minIndex];
			return true;
		}
		return false;
	}
	return false;
}

