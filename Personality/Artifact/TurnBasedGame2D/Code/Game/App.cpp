#include "Game/App.hpp"
#include "Game/Game.hpp"
#include "Game/GameCommon.hpp"
#include "RL/RLBrain.hpp"
#include "RL/RLWindow.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "ThirdParty/TinyXML2/tinyxml2.h"
#include "ThirdParty/APF/APF/APF_Lib.hpp"
#include "ThirdParty/APF/Memory/MemorySystem.hpp"

App* g_theApp = nullptr;
APF_Lib::APF* g_APF = nullptr;
InputSystem* g_theInputSystem = nullptr;
Window* g_theWindowContext = nullptr;
Renderer* g_theRenderer = nullptr;
AudioSystem* g_theAudioSystem = nullptr;
UISystem* g_theUISystem = nullptr;

//-----------------------------------------------------------------------------------------------
App::App() {}


//-----------------------------------------------------------------------------------------------
App::~App() {
}


//-----------------------------------------------------------------------------------------------
void App::Startup() {
	// dynamic allocate memory for global pointers
	// also set up engine configs
	tinyxml2::XMLDocument gameConfig;
	gameConfig.LoadFile("Data/GameConfig.xml");
	tinyxml2::XMLElement* rootElement = gameConfig.RootElement();
	g_gameConfigBlackboard.PopulateFromXmlElementAttributes(*rootElement);

	g_rng = new RandomNumberGenerator();

	InputSystemConfig inputConfig;
	g_theInputSystem = new InputSystem(inputConfig);

	WindowConfig windowConfig;
	windowConfig.m_windowTitle = "ProtoGame2D";
	windowConfig.m_clientAspect = g_gameConfigBlackboard.GetValue("clientAspect", 0.f);
	windowConfig.m_inputSystem = g_theInputSystem;
	g_theWindowContext = new Window(windowConfig);


	RendererConfig renderConfig;
	renderConfig.m_windowContext = g_theWindowContext;
	g_theRenderer = new Renderer(renderConfig);

	EventSystemConfig eventSystemConfig;
	g_theEventSystem = new EventSystem(eventSystemConfig);

	DevConsoleConfig devConsoleConfig;
	devConsoleConfig.m_fontName = "SquirrelFixedFont";
	devConsoleConfig.m_fontSize = 1.f;
	devConsoleConfig.m_renderder = g_theRenderer;
	devConsoleConfig.m_eventSystem = g_theEventSystem;
	devConsoleConfig.m_inputSystem = g_theInputSystem;
	g_theConsole = new DevConsole(devConsoleConfig);


	AudioSystemConfig audioConfig;
	g_theAudioSystem = new AudioSystem(audioConfig);

	UISystemConfig uiConfig;
	uiConfig.m_renderer = g_theRenderer;
	uiConfig.m_windowContext = g_theWindowContext;
	g_theUISystem = new UISystem(uiConfig);

	g_theInputSystem->Startup();
	g_theWindowContext->Startup();
	g_theRenderer->Startup();
	g_theEventSystem->Startup();
	g_theConsole->Startup();
	g_theAudioSystem->Startup();
	g_theUISystem->Startup();

	g_theEventSystem->SubscribeEventCallbackFunction("Quit", EventQuit);
	g_APF = new APF_Lib::APF();
	APF_Lib::MM::MemorySystem::gMemoryAPF = g_APF; 

	
	g_theGame = new Game(this);
	g_theGame->Startup();
}


void App::Run()
{
	while (!m_isQuitting) {
		RunFrame();
	}
}

//-----------------------------------------------------------------------------------------------
bool App::IsQuitting() const {
	return m_isQuitting;
}


//-----------------------------------------------------------------------------------------------
void App::HandleQuitRequest() {
	m_isQuitting = true;
}


//-----------------------------------------------------------------------------------------------
bool App::EventQuit(EventArgs& args) {
	UNUSED(args);
	if (g_theApp) {
		g_theApp->HandleQuitRequest();
	}
	return true;
}


//-----------------------------------------------------------------------------------------------
void App::RunFrame() {


	BeginFrame();
	HandleKeyActivities();
	HandleControllerActivities();
	Update();
	Render();
	EndFrame();

}


//-----------------------------------------------------------------------------------------------
void App::Shutdown() {
	// delete pointers when shutdown the app

	g_theGame->Shutdown();
	delete g_theGame;
	g_theGame = nullptr;


	g_theUISystem->Shutdown();
	delete g_theUISystem;
	g_theUISystem = nullptr;

	g_theAudioSystem->Shutdown();
	delete g_theAudioSystem;
	g_theAudioSystem = nullptr;

	g_theRenderer->Shutdown();
	delete g_theRenderer;
	g_theRenderer = nullptr;

	g_theWindowContext->Shutdown();
	delete g_theWindowContext;
	g_theWindowContext = nullptr;

	g_theInputSystem->Shutdown();
	delete g_theInputSystem;
	g_theInputSystem = nullptr;

	g_theConsole->Shutdown();
	delete g_theConsole;
	g_theConsole = nullptr;

	g_theEventSystem->Shutdown();
	delete g_theEventSystem;
	g_theEventSystem = nullptr;

	delete g_APF;
	g_APF = nullptr;

	delete g_rng;
	g_rng = nullptr;
}


//-----------------------------------------------------------------------------------------------
void App::BeginFrame() {
	Clock::SystemBeginFrame();

	g_theConsole->BeginFrame();
	g_theEventSystem->BeginFrame();
	g_theInputSystem->BeginFrame();
	g_theWindowContext->BeginFrame();
	g_theRenderer->BeginFrame();
	g_theAudioSystem->BeginFrame();
	g_theUISystem->BeginFrame();
}


//-----------------------------------------------------------------------------------------------
void App::Update() {
	if((g_theGame->m_learningCount > 10 && g_theGame->m_trainState == Q_LEARNING_PERSONALITY) ||
		(g_theGame->m_learningCount > 20 && g_theGame->m_trainState == Q_LEARNING_VALENCE)){
		g_theGame->Shutdown();
		g_theGame->Startup();
		g_theGame->m_learningCount = 0;
	}
	g_theGame->Update();
}


//-----------------------------------------------------------------------------------------------
void App::EndFrame() {
	g_theUISystem->EndFrame();
	g_theAudioSystem->EndFrame();
	g_theRenderer->EndFrame();
	g_theWindowContext->EndFrame();
	g_theEventSystem->EndFrame();
	g_theConsole->EndFrame();
	g_theInputSystem->EndFrame();

}


//-----------------------------------------------------------------------------------------------
void App::Render() const {

	g_theGame->Render();
}


//-----------------------------------------------------------------------------------------------
void App::HandleKeyActivities() {
	if (g_theConsole->GetMode() == OPEN) {
		return;
	}
	// Press F8 to hardset the game
	if (g_theInputSystem->WasKeyJustPressed(KEYCODE_F8)) {
		if (g_theGame) {
			g_theGame->Shutdown();
			delete g_theGame;
			g_theGame = new Game(this);
			g_theGame->Startup();
		}
	}

	// Press ESC, if game is in attract mode, quit the game
	if (g_theInputSystem->WasKeyJustPressed(KEYCODE_ESC)) {
		HandleQuitRequest();
	}
}


//-----------------------------------------------------------------------------------------------
void App::HandleControllerActivities() {
	if (g_theConsole->GetMode() == OPEN) {
		return;
	}

	XboxController const& controller = g_theInputSystem->GetController(0);

	// Press back, if game is in attract mode, quit the game
	if (controller.WasButtonJustPressed(XboxButtonID::XINPUT_BUTTON_BACK)) {
		HandleQuitRequest();
	}
}

