#include "Game/DebugWindow.hpp"
#include "Game/Level.hpp"
#include "Game/Character.hpp"
#include "Game/Game.hpp"
#include "ThirdParty/imgui/imgui.h"
#include "Engine/Core/EngineCommon.hpp"
#include <vector>

//-----------------------------------------------------------------------------------------------
DebugWindow::DebugWindow(Level* level){
	m_level = level;
}


//-----------------------------------------------------------------------------------------------
void DebugWindow::Update(float deltaSeconds){
	UpdateStatsWindow(deltaSeconds);
	UpdateSettingWindow(deltaSeconds);
}


//-----------------------------------------------------------------------------------------------
void DebugWindow::UpdateSettingWindow(float deltaSeconds){
	UNUSED(deltaSeconds);
	ImGui::Begin("Debug Setting");
	if(ImGui::SliderFloat("Simulation Speed", &m_currentGameSpeed, 0.5, 20.f)){
		g_theGame->m_stateClock.SetTimeDilation(m_currentGameSpeed);
	}
	if (ImGui::Button("Render Grid")) {
		m_level->m_renderGrid = !m_level->m_renderGrid;
	}
	ImGui::SameLine();
	if (ImGui::Button("Render Grid Safety")) {
		m_level->m_renderSafety = !m_level->m_renderSafety;
	}
	ImGui::End();
}


//-----------------------------------------------------------------------------------------------
void DebugWindow::UpdateStatsWindow(float deltaSeconds){
	UNUSED(deltaSeconds);
	ImGui::Begin("Character Stats");
	std::vector<const char*> characterNames;
	for(Character* character : m_level->m_allCharacters){
		characterNames.push_back(character->m_name.c_str());
	}
	ImGui::ListBox("Characters:\nSelect any character to see the character \
statistics.", &m_currentSelectedCharInStatsWindow, characterNames.data(),
		(int)characterNames.size(), 8);
	Character* selectedCharacter = m_level->m_allCharacters[m_currentSelectedCharInStatsWindow];
	std::string characterStats = "Character Name: " + selectedCharacter->m_name + "\n  Faction: ";
	if(selectedCharacter->m_faction == FACTION_GOOD){
		characterStats += "Good";
	} 
	else{
		characterStats += "Evil";
	}
	std::string healthString = std::to_string(selectedCharacter->m_health);
	characterStats += "\n  Current Health:" + healthString.substr( 0, healthString.find('.')) + 
		"\n  Character Stats: \n  " + selectedCharacter->GetStatsDescription();
	ImGui::Text(characterStats.c_str());
	ImGui::NewLine();

	if(selectedCharacter->m_isANPC){
		if(ImGui::TreeNode("ANPC Actions")){
			for(int actionIndex = 0; actionIndex < (int)selectedCharacter->m_anpcActions.size();
				actionIndex++){
				if (ImGui::TreeNode(selectedCharacter->m_anpcActions[actionIndex].c_str())) {
					ImGui::TreePop();
				}
			}
			ImGui::TreePop();
		}
	}
	ImGui::End();
}
