#pragma once
#include "Game/CharacterUID.hpp"
#include "Game/CharacterDefinition.hpp"
#include "Engine/Core/Stopwatch.hpp"
#include "Engine/Math/Vec2.hpp"
#include "ThirdParty/APF/Memory/GameActor.hpp"
#include <string>

struct Game;
struct Level;
class BehaviorTree;
class SpriteAnimDefinition;


//-----------------------------------------------------------------------------------------------
enum FacingDirection {
	FACING_LEFT,
	FACING_RIGHT
};


//-----------------------------------------------------------------------------------------------
enum Faction {
	FACTION_GOOD,
	FACTION_EVIL,
	FACTION_NEUTRAL
};


//-----------------------------------------------------------------------------------------------
struct CharacterStats{
	float m_damageDealt = 0.f;
	float m_damageTaken = 0.f;
	float m_damageBlocked = 0.f;
	float m_damageHealed = 0.f;
	float m_allyHealing = 0.f;
	//float m_allyDamageTaken = 0.f;

	int m_redZoneCount = 0;
	int m_yellowZoneCount = 0;
	int m_greenZoneCount = 0;
	void operator+=(const CharacterStats& statsToAdd);
	void operator/=(float valueToDivide);
	void operator*=(float valueToMultiply);
	void operator=(const CharacterStats& copy);
	CharacterStats operator-(const CharacterStats& statsToSubtract);
};

struct Expectation {
	int m_actionIndex = -1;
	CharacterStats m_statsChange;
	float m_actionScore = 0.f;
	float m_idealScore = 0.f;
	bool m_isEmpty = true;
};


//-----------------------------------------------------------------------------------------------
typedef bool (*CharacterAction)(EventArgs& args);
constexpr int SKIP_TURN_AFTER_NUM_TRYS_FAILED = 10;



//-----------------------------------------------------------------------------------------------
class Character
{
public:
	Character(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
		FacingDirection direction, Faction faction, bool isANPC = false, int speedVariance = -1);
	Character(Character* character);
	virtual ~Character();

	// virtual functions
	virtual void Render() const;
	virtual void Update(float deltaSeconds);
	void RenderHealthBar() const;

	virtual void UpdateAnimation();
	virtual void PlayAnimation(std::string animationName, float animationSpeed = 1.f);

	void SetCharUID(CharacterUID uid, std::string unitName);
	void ResetSpeed();
	std::string GetFactionString() const;
	std::string GetStatsDescription() const;
	float GetHealthPercentage() const;
	void DebugRender() const;

	// APF
	void InitializeAPFACtor();
	void TriggerEvent(APF_Lib::MM::ActionInfo* info, APF_Lib::MM::GameObject* target);
	void DetermineANPCAction();
	void ChooseRandomAction();
	void RecordStats();
	void UpdatePraiseByPersonality(APF_Lib::MM::GameEvent const& event);
	void UpdateImiediateActionPraiseByExpectation(APF_Lib::MM::GameEvent& event);
	bool UpdateContinuousActionPraiseByExpectation(APF_Lib::MM::MemoryAndEvent* memory);
	void UpdateMemory(APF_Lib::MM::GameEvent& event, CharacterStats statsChange);

	void ShortRangeNormalAttack(Character* target);
	void LongRangeNormalAttack(Character* target);
	void LongSingleNormalAttack(Character* target);
	void Defense();
	void Heal(Character* target);
	void MoveForward();
	void MoveBackward();

	virtual float TakeDamage(Character* killer,float damage);
	virtual void Die();
	virtual float GetHealed(float damage);
	Faction GetOppositeFaction() const;

	int GetMoveForwardIndex() const;
	int GetMoveBackwardIndex() const;

	float GetAttackDamage() const;
	float GetRangeAttackDamage() const;
	float GetLongAttackDamage() const;
	float GetHealDamage() const;
	float GetBlockingDamagePercentage() const;

	// behavior nodes
	static void RegisterBehaviorNodes();
	static bool IsHealthAbove(EventArgs& args);
	static bool ShortAttackAction(EventArgs& args);
	static bool LongAttackAction(EventArgs& args);
	static bool RangeAttackAction(EventArgs& args);
	static bool DefenseAction(EventArgs& args);
	static bool HealAction(EventArgs& args);
	static bool MoveForwardAction(EventArgs& args);
	static bool MoveBackwardAction(EventArgs& args);
	static bool FindRandomTarget(EventArgs& args);
public:
	// general character information
	std::string m_name;
	bool	m_isANPC = false;
	Vec2	m_position;
	Vec2	m_velocity;
	float	m_health;
	bool	m_isDead;
	bool	m_isGarbage;
	Faction	m_faction;
	FacingDirection m_facingDirection;
	Level*	m_level = nullptr;
	BehaviorTree* m_behaviorTree = nullptr;
	CharacterDefinition m_definition;
	CharacterUID m_uid;

	// APF
	APF_Lib::MM::GameActor* m_memoryActor = nullptr;
	APF_Lib::MM::MemorySystem* m_memorySystem = nullptr;

	// character info
	CharacterStats m_stats;
	Expectation m_currentExpectation;
	bool m_currentExpectationEmpty = true;
	std::vector<CharacterStats> m_allStats = { m_stats };
	CharacterAction m_chosenAction = nullptr;
	EventArgs m_defaultArguments;
	float m_totalValenceChange = 0.f;

	// animations
	std::string m_currentAnimationName;
	SpriteAnimDefinition* m_currentAnimation = nullptr;
	Clock m_animationClock;
	Stopwatch m_attackAnimTime;
	Stopwatch m_painAnimTime;
	Stopwatch m_healAnimTime;
	Stopwatch m_dieFadeOutTime;

	// render and ingame logic
	Vec2 m_renderScale = Vec2::ONE;
	int m_gridIndex = -1;
	float m_currentSpeed = 0.f;
	float m_defaultSpeed = 0.f;
	bool m_isMyTurn = false;
	bool m_renderCharTextureBound = false;

	// actions
	Character* m_target = nullptr;
	Stopwatch m_fadeInTimer;
	Stopwatch m_fadeOutTimer;
	Stopwatch m_actionTimer;
	bool m_isActionEffectReady = false;
	int m_blockDamageCounter = 0;
	int m_skipCounter = 0;
	std::vector<std::string> m_anpcActions;

private:
	void ChooseActionByName(std::string actionName);
	void ChooseTargetByName(std::string actionName);
	CharacterStats EvaluateAction(std::string actionName, Character* target);
	float GetIdealActionResult(std::string actionName);
	float GetExpectionScore(CharacterStats expection, std::string actionName);
};


