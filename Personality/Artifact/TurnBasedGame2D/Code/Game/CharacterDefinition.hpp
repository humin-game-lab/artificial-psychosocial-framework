#pragma once
#include "Game/GameCommon.hpp"
#include "Engine/Math/Vec3.hpp"
#include "ThirdParty/TinyXML2/tinyxml2.h"
#include <string>
#include <map>
#include <vector>

class SpriteAnimDefinition;


//------------------------------------------------------------------------------------------------
class CharacterDefinition
{
public:
	CharacterDefinition();
	~CharacterDefinition();

	static void InitializeCharacterDefinitions();
	static void ClearCharacterDefinitions();
	static std::map<std::string, CharacterDefinition> s_charDefinitions;
	static bool LoadFromXmlElement(tinyxml2::XMLElement& elem);



protected:
	void LoadAnimationElement(tinyxml2::XMLElement& elem);
public:
	
	// options
	std::string m_name; 
	std::string m_behaviorTree;
	float m_health = 0.f;
	float m_speed = 0.f;
	float m_attackDamage = 0.f;
	float m_rangeAttackDamage = 0.f;
	float m_longAttackDamage = 0.f;
	float m_blockingDamagePercentage = 0.f;
	float m_healDamage = 0.f;

	std::map<std::string, SpriteAnimDefinition*> m_animations;
};

