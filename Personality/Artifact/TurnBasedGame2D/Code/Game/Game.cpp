#include "Game/Game.hpp"
#include "Game/Character.hpp"
#include "Game/Tank.hpp"
#include "Game/Ranger.hpp"
#include "Game/Healer.hpp"
#include "Game/Level.hpp"
#include "Game/DebugWindow.hpp"
#include "Game/GameSettingWindow.hpp"
#include "Game/APFUtils.hpp"
#include "RL/SimulationUtils.hpp"
#include "RL/RLWindow.hpp"
#include "RL/PSOWindow.hpp"
#include "Engine/Math/AABB2.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/Core/ErrorWarningAssert.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "ThirdParty/APF/APF/APF_Lib.hpp"

//-----------------------------------------------------------------------------------------------
Game* g_theGame = nullptr;
std::map<std::string, HeuristicFunctionPointer> g_heuristicFunctions;

//-----------------------------------------------------------------------------------------------
Game::Game(App* const& owner) {
	m_app = owner;
}


//-----------------------------------------------------------------------------------------------
Game::~Game() {
}


//-----------------------------------------------------------------------------------------------
void Game::Startup() {

	// initial game assets and attract mode vertexes
	InitializeAssets();
	InitializeHeuristicFunctions();

	m_stateClock.SetParent(m_gameClock);
	m_stateClock.Reset();
	CharacterDefinition::InitializeCharacterDefinitions();
	Character::RegisterBehaviorNodes();
	Tank::RegisterTankBehaviors();
	Ranger::RegisterRangerBehaviors();
	Healer::RegisterHealerBehaviors();
	InitializeAPF();

	m_stateClock.SetTimeDilation(10.f);
	m_qLearningPersonality = new QLearningWithPersonality(
		QLearningWithPersonality::InitializeAllPossiblePeronalityActions());
	m_learningWindow = new RLWindow();
	m_qLearningValence = new QLearningWithValence(
		QLearningWithValence::InitializeAllPossibleValenceActions());
	m_psoValence = new PSOWithValence();
	m_psoPersonality = new PSOWithPersonality();
	m_psoWindow = new PSOWindow(m_psoValence, m_psoPersonality);


	m_learningWindow->Clear();
	m_currentPersonalityState.Reset();
	m_previousPersonalityState.Reset();
	m_currentValenceState.Reset();
	m_previousValenceState.Reset();
	m_currentLearningScore = -1.f;
	if(m_trainState != PSO_VALENCE){
		EnterNewLevel();
	}

}


//-----------------------------------------------------------------------------------------------
void Game::Render() const {
	g_theRenderer->ClearScreen(Rgba8(0, 0, 0, 255));

	if (m_trainState == PSO_VALENCE || m_trainState == PSO_PERSONALITY) {
		g_theUISystem->Render(m_screenCamera);
		return;
	}

	g_theRenderer->BeginCamera(m_screenCamera);
	m_currentLevel->Render();
	RenderConsole();
	g_theRenderer->EndCamera(m_screenCamera);
	g_theUISystem->Render(m_screenCamera);
}


//-----------------------------------------------------------------------------------------------
void Game::Update() {
	float deltaSeconds = static_cast<float>(m_stateClock.GetFrameDeltaSeconds());

	if(m_currentLevel && m_currentLevel->m_gameComplete){
 		float characterScore = GetTankScore(m_currentLevel->m_anpcCharacter->m_anpcActions,
			m_currentLevel->m_anpcCharacter->m_stats, m_currentLevel->GetTotalAllyStats(),
				m_currentLevel->m_doesAIWinTheBattle); 
		if (m_trainState == PSO_RENDER_SIMULATION) {
			return;
		}
		m_currentLearningScore = characterScore;
		float reward = GetReward(characterScore);
		if(m_trainState == Q_LEARNING_PERSONALITY){
			m_qLearningPersonality->Learn(m_previousPersonalityState, m_currentPersonalityState,
				m_currentPersonalityAction, reward);
		}
		else if (m_trainState == Q_LEARNING_VALENCE){
			m_qLearningValence->Learn(m_previousValenceState, m_currentValenceState,
				m_currentValenceAction, reward);
		}
		EnterNewLevel();
	}
	// handle input activities
	if (g_theConsole->GetMode() != OPEN) {
		if(m_currentLevel){
			m_currentLevel->Update(deltaSeconds);
		}
		if(m_trainState == PSO_VALENCE){
			m_psoValence->Update(deltaSeconds);
		}
		else if(m_trainState == PSO_PERSONALITY){
			m_psoPersonality->Update(deltaSeconds);
		}
		HandleKeyAcvities();
	}
	if(m_debugWindow && m_renderDebugWindow){
		m_debugWindow->Update(deltaSeconds);
	}
	if(m_gameSettingWindow && m_renderGameSettingWindow){
		m_gameSettingWindow->Update(deltaSeconds);
	}

	if (m_renderRLWindow) {
		m_learningWindow->Update();
	}

	if(m_renderPSOWindow){
		m_psoWindow->Update();
	}
	
	g_theConsole->Update();
	UpdateCamera(deltaSeconds);
}


//-----------------------------------------------------------------------------------------------
void Game::Shutdown() {
	CharacterDefinition::ClearCharacterDefinitions();
	if(m_characterSpritesheet){
		delete m_characterSpritesheet;
		m_characterSpritesheet = nullptr;
	}
	if(m_currentLevel){
		delete m_currentLevel;
		m_currentLevel = nullptr;
	}
	if(m_debugWindow){
		delete m_debugWindow;
		m_debugWindow = nullptr;
	}
	if(m_psoValence){
		delete m_psoValence;
		m_psoValence = nullptr;
	}
	if (m_qLearningPersonality) {
		delete m_qLearningPersonality;
		m_qLearningPersonality = nullptr;
	}
	if (m_qLearningValence) {
		delete m_qLearningValence;
		m_qLearningValence = nullptr;
	}
	if(g_personalityActionConnection){
		delete g_personalityActionConnection;
		g_personalityActionConnection = nullptr;
	}
	if(m_psoValence){
		delete m_psoValence;
		m_psoValence = nullptr;
	}
	if (m_psoPersonality) {
		delete m_psoPersonality;
		m_psoPersonality = nullptr;
	}
	if (m_psoWindow) {
		delete m_psoWindow;
		m_psoWindow = nullptr;
	}
	if (m_learningWindow) {
		delete m_learningWindow;
		m_learningWindow = nullptr;
	}
}



//-----------------------------------------------------------------------------------------------
void Game::EnterNewLevel(){
	g_APF->m_anpcs.clear();
	g_APF->m_praises.clear();
	g_APF->m_socials.clear();
	g_APF->m_anpcsMentalState.clear();
	if(m_trainState == Q_LEARNING_PERSONALITY){
		m_currentPersonalityAction = 
			m_qLearningPersonality->ChooseAction(m_currentPersonalityState);
		if (m_currentLearningScore != -1.f) {
			m_learningWindow->AddScore(m_currentLearningScore);
		}
		m_previousPersonalityState = m_currentPersonalityState;
		m_currentPersonalityState.AddAction(m_currentPersonalityAction);
	}
	else if(m_trainState == Q_LEARNING_VALENCE){
		m_currentValenceAction = m_qLearningValence->ChooseAction(m_currentValenceState);
		if (m_currentLearningScore != -1.f) {
			m_learningWindow->AddScore(m_currentLearningScore);
		}
		m_previousValenceState = m_currentValenceState;
		m_currentValenceState.AddAction(m_currentValenceAction);
	}
	m_learningCount++;
	if(m_currentLevel){
		delete m_currentLevel;
		m_currentLevel = nullptr;
	}
	m_currentLevel = new Level(this);
	if(m_debugWindow){
		delete m_debugWindow;
		m_debugWindow = nullptr;
	}
	m_debugWindow = new DebugWindow(m_currentLevel);
	
	if (m_gameSettingWindow) {
		delete m_gameSettingWindow;
		m_gameSettingWindow = nullptr;
	}
	m_gameSettingWindow = new GameSettingWindow(m_currentLevel);
}


//-----------------------------------------------------------------------------------------------
void Game::SimulateLevelFromPSOBest() {
	g_theGame->m_renderPSOWindow = false;
	m_trainState = PSO_RENDER_SIMULATION;
	m_stateClock.SetTimeDilation(5.f);
	if(m_currentLevel){
		delete m_currentLevel;
	}
	g_APF->m_anpcs.clear();
	g_APF->m_praises.clear();
	g_APF->m_socials.clear();
	g_APF->m_anpcsMentalState.clear();
	m_currentValenceState = m_psoValence->m_globalValence;
	m_currentLevel = new Level(this);

	if (m_debugWindow) {
		delete m_debugWindow;
		m_debugWindow = nullptr;
	}
	m_debugWindow = new DebugWindow(m_currentLevel);

	if (m_gameSettingWindow) {
		delete m_gameSettingWindow;
		m_gameSettingWindow = nullptr;
	}
	m_gameSettingWindow = new GameSettingWindow(m_currentLevel);
}


//-----------------------------------------------------------------------------------------------
void Game::ApplyValenceAndTrainPersonality(){
	m_trainState = PSO_PERSONALITY;
	g_APF->m_anpcs.clear();
	g_APF->m_praises.clear();
	g_APF->m_socials.clear();
	g_APF->m_anpcsMentalState.clear();
}


//-----------------------------------------------------------------------------------------------
Level* Game::GetSimulatingLevel(int index){
	if(m_trainState == PSO_VALENCE){
		return m_psoValence->GetParticleLevel(index);
	}
	else if(m_trainState == PSO_PERSONALITY){
		return m_psoPersonality->GetParticleLevel(index);
	}

	return nullptr;
}


//-----------------------------------------------------------------------------------------------
Level* Game::GetSimulatingLevel(){
	if(m_currentLevel){
		return m_currentLevel;
	}
	else if (m_trainState == PSO_VALENCE) {
		return m_psoValence->GetRunningLevel();
	}
	else if (m_trainState == PSO_PERSONALITY) {
		return m_psoPersonality->GetRunningLevel();
	}
	return nullptr;
}


//-----------------------------------------------------------------------------------------------
Personality Game::GetANPCPersonality() const{
	if(m_trainState == PSO_PERSONALITY){
		return m_psoPersonality->GetCurrentPersonality();
	}
	else if(m_trainState == PSO_RENDER_SIMULATION){
		return m_psoPersonality->m_globalPersonality;
	}
	return m_currentPersonalityState;
}


//-----------------------------------------------------------------------------------------------
void Game::InitializeAssets() {
	// load the base and turret texture
	g_gameTextures[GAME_ATTACT_SCREEN_TEXTURE] =
		g_theRenderer->CreateOrGetTextureFromFile("Data/Images/AttractScreen.png");
	g_gameTextures[GAME_BACKGROUND_TEXTURE] =
		g_theRenderer->CreateOrGetTextureFromFile("Data/Images/Bg_plains_01.png");
	g_gameTextures[GAME_CHARACTER_SPRITESHEET] =
		g_theRenderer->CreateOrGetTextureFromFile("Data/Images/CharacterMegasheet.png");
	m_characterSpritesheet = new SpriteSheet(*g_gameTextures[GAME_CHARACTER_SPRITESHEET]
		, IntVec2(8,15));

	g_gameFonts[GAME_FONT] =
		g_theRenderer->CreateOrGetBitmapFont("Data/Fonts/SquirrelFixedFont");
}


//-----------------------------------------------------------------------------------------------
void Game::InitializeAPF(){
	LoadActionsFromXML("Data/Definitions/Actions.xml");
	LoadRelationsFromXML("Data/Definitions/Relations.xml");
	g_APF->InitializeVocabulary();
	for (auto it = s_actions.begin(); it != s_actions.end(); it++){
		g_APF->AddAction(it->first.c_str(), it->second->effect);
		it->second->apfIdx = g_APF->GetActionIndex(it->first.c_str());
	}

	g_personalityActionConnection = new APF_Lib::PersonalityActionConnection();
	LoadConnectionsFromXML("Data/Definitions/ActionPersonalityConnection.xml");

}


//-----------------------------------------------------------------------------------------------
void Game::InitializeHeuristicFunctions(){
	g_heuristicFunctions["Defensive Tank"] = GetTankScore;
	g_heuristicFunctions["AD Carry Ranger"] = GetRangerScore;
	g_heuristicFunctions["Only Heal Healer"] = GetHealerScore;
	g_heuristicFunctions["Sylvanas"] = GetSylvanasScore;
}


//-----------------------------------------------------------------------------------------------
void Game::HandleKeyAcvities() {
	if (g_theInputSystem->WasKeyJustPressed(KEYCODE_ENTER)) {
		g_theConsole->Toggle(OPEN);
	}
	if (g_theInputSystem->WasKeyJustPressed('K')) {
		m_renderDebugWindow = !m_renderDebugWindow;
		m_renderGameSettingWindow = !m_renderGameSettingWindow;
	}

	if (g_theInputSystem->WasKeyJustPressed('L')) {
		m_renderRLWindow = !m_renderRLWindow;
	}
	if (g_theInputSystem->WasKeyJustPressed('J')) {
		m_renderPSOWindow = !m_renderPSOWindow;
	}
}


//-----------------------------------------------------------------------------------------------
void Game::RenderConsole() const {
	AABB2 cameraBounds(m_screenCamera.GetOrthoBottomLeft(), m_screenCamera.GetOrthoTopRight());
	g_theConsole->Render(cameraBounds, g_theRenderer);
}


//-----------------------------------------------------------------------------------------------
void Game::UpdateCamera(float deltaSeconds) {
	UNUSED(deltaSeconds);
	Vec2 screenSize = g_gameConfigBlackboard.GetValue("screenSize", Vec2(0.f, 0.f));
	m_screenCamera.SetOrthoView(Vec2(0.f,0.f), 
		screenSize);
	m_screenCamera.SetOrthographicMatrix();
}

