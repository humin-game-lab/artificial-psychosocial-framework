#include "Game/Entity.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Engine/Core/EngineCommon.hpp"

//-----------------------------------------------------------------------------------------------
Entity::Entity(Game* const& game, Vec2 const& startingPosition) {
	 m_game = game;
	 m_position = startingPosition;

	 m_velocity = Vec2(0.f,0.f);
	 m_orientationDegrees = 0.f;
	 m_angularVelocity = 0.f;
	 m_physicsRadius = 0.f;
	 m_cosmeticRadius = 0.f;
	 m_health = 0;
	 m_isDead = false;
	 m_isGarbage = false;
}


//-----------------------------------------------------------------------------------------------
void Entity::Update(float deltaTime) {
	UNUSED(deltaTime);
}


//-----------------------------------------------------------------------------------------------
void Entity::Render()const {
	
}


//-----------------------------------------------------------------------------------------------
void Entity::Die() {
	m_isDead = true;
	m_isGarbage = true;
}


//-----------------------------------------------------------------------------------------------
Vec2 const Entity::GetForwardNormal()const {
	return Vec2::MakeFromPolarDegrees(m_orientationDegrees);
}


//-----------------------------------------------------------------------------------------------
bool Entity::IsAlive()const {
	return !m_isDead;
}


//-----------------------------------------------------------------------------------------------
void Entity::DebugRender() const {
	// debug mode draw an inner ring and an outer ring, also a line as the velocity
	float lineThickness = 0.2f;
	// draw the x-axis and y-axis with the length = cosmetic radius
	Vec2 forward_normal_vector = GetForwardNormal();
	Vec2 forward_vector_rotate_90degrees = forward_normal_vector.GetRotated90Degrees();
	Vec2 x_direction = forward_normal_vector * m_cosmeticRadius;
	Vec2 y_direction = forward_vector_rotate_90degrees * m_cosmeticRadius;

	DebugDrawRing(m_position, m_physicsRadius, lineThickness, Rgba8::CYAN);
	DebugDrawRing(m_position, m_cosmeticRadius, lineThickness, Rgba8::MAGENTA);
	DebugDrawLine(m_position, m_position + m_velocity, lineThickness, Rgba8::YELLOW);
	DebugDrawLine(m_position, m_position + x_direction, lineThickness, Rgba8::RED);
	DebugDrawLine(m_position, m_position + y_direction, lineThickness, Rgba8::GREEN);
}