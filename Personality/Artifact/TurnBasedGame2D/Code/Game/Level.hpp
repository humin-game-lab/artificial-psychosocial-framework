#pragma once
#include "Game/Character.hpp"
#include "ThirdParty/APF/Memory/GameEvent.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
struct Game;


//-----------------------------------------------------------------------------------------------
struct Level
{
public:
	Level(Game* const& game);
	Level(Game* const& game, std::vector<float> const& praises, int swarmIndex);
	~Level();

	void Render() const;
	void RenderBackground() const;
	void RenderGrid() const;
	void RenderColumnSafety() const;
	void Update(float deltaSeconds);

	void DeleteGarbageCharacters();
	void HandleKeyAcvities();
	Vec2 GetGridPositions(int index, Faction faction);

	Character* SpawnCharacters(std::string name, FacingDirection direction, Faction faction, int gridIndex);
	Character* FindCharacterByIndex(int index, Faction faction);
	Character* FindCharacterByName(std::string);
	CharacterStats GetTotalAllyStats() const;
	CharacterStats GetStatsChange(int startTurn, int endTurn, int actorIndex) const;
	std::string GetCharacterRoleByIndex(int actorIndex) const;
	bool UpdateContinuousAction(APF_Lib::MM::MemoryAndEvent* memory);

	bool IsCharacterInRedZone(Character* character);
	bool IsCharacterInYellowZone(Character* character);
	bool IsCharacterInGreenZone(Character* character);
	bool IsEventContinuous(int eventIdx);

	void ProcessNewEvent(APF_Lib::MM::GameEvent& event);
public:
	Game* m_game;
	int m_swarmIndex = -1;
	std::ofstream m_valenceData;
	std::vector<Character*> m_characters;
	std::vector<Character*> m_allCharacters;
	int m_numOfGridRows = 0;
	bool m_renderGrid = false;
	bool m_renderSafety = false;
	bool m_gameComplete = false;
	bool m_doesAIWinTheBattle = false;
	int m_totalTurnCount = 0;

	Character* m_anpcCharacter = nullptr;
	std::vector<APF_Lib::MentalState> m_mentalStates;

protected:
	void AddCharacter(Character* character, std::string role);
	int GetFreeCharacterSlot() const;

	void UpdateGridSafetyness();
	void UpdateBattleProgress();
	void CalculateGridPositions();
	void UpdateCharacterTurn();
	void UpdateAPFAfterActorInitialization();
	void UpdateAPFAfterActorInitialization(std::vector<float> const& praises);
	void UpdateANPCZoneInfo();
	void InitializeANPCs();
private:
	int m_charSalt = 0;
	std::vector<Vec2> m_playerSidePositions;
	std::vector<Vec2> m_enemySidePositions;

	int m_playerSideRedColumn = -1;
	int m_enemySideRedColumn = -1;

	Character* m_currentTurnCharacter = nullptr;
};


