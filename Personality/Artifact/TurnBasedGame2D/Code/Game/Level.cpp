#include "Game/Level.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/CharacterUID.hpp"
#include "Game/Tank.hpp"
#include "Game/Ranger.hpp"
#include "Game/Healer.hpp"
#include "Game/APFUtils.hpp"
#include "Engine/Renderer/Camera.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/Time.hpp"
#include "Engine/Math/MathUtils.hpp"


//-----------------------------------------------------------------------------------------------
Level::Level(Game* const& game){
	m_game = game;

	double currentTime = GetCurrentTimeSeconds();
	std::string timeString = std::to_string(currentTime);
	std::string fileName = "Saves/" + timeString.substr(0, timeString.find('.') + 2) + ".csv";
	m_valenceData.open(fileName);
	std::string allActions = "Action,";
	for (auto it = s_actions.begin(); it != s_actions.end(); it++) {
		allActions += it->first + ",";
	}
	allActions += ",Positive,Negative,Pleased,Displeased,Liking,Disliking,Approving,Disapproving,Hope,Fear,Joy,\
Distress,Love,Hate,Interest,Pride,Shame,Admiration,Reproach,Satisfaction,Frears_confirmed,\
Relief,Disappointment,Happy_For,Resentment,Gloating,Pity,Gratification,Remorse,Gratitude,Anger\n";
	m_valenceData << allActions;

	float gridBoundTop = g_gameConfigBlackboard.GetValue("gridBoundTop", 0.f);
	float gridBoundBottom = g_gameConfigBlackboard.GetValue("gridBoundBottom", 0.f);
	float gridBoundHeight = g_gameConfigBlackboard.GetValue("gridBoundHeight", 0.f);
	m_numOfGridRows = static_cast<int>((gridBoundTop - gridBoundBottom) / gridBoundHeight);
	CalculateGridPositions();
	SpawnCharacters("Ranger", FACING_RIGHT, FACTION_GOOD, 5);
	SpawnCharacters("Tank", FACING_RIGHT, FACTION_GOOD, 0);
	SpawnCharacters("Character", FACING_RIGHT, FACTION_GOOD, 1);
	SpawnCharacters("Healer", FACING_RIGHT, FACTION_GOOD, 6);
	SpawnCharacters("Tank", FACING_LEFT, FACTION_EVIL,0);
	SpawnCharacters("Tank", FACING_LEFT, FACTION_EVIL, 1); 
	SpawnCharacters("Ranger", FACING_LEFT, FACTION_EVIL, 4);
	SpawnCharacters("Healer", FACING_LEFT, FACTION_EVIL, 6);
	InitializeANPCs();
	UpdateAPFAfterActorInitialization();
}


//-----------------------------------------------------------------------------------------------
Level::Level(Game* const& game, std::vector<float> const& praises, int swarmIndex){
	m_game = game;
	m_swarmIndex = swarmIndex;

	if (g_theGame->m_trainState != PSO_VALENCE && g_theGame->m_trainState != PSO_PERSONALITY) {
	double currentTime = GetCurrentTimeSeconds();
	std::string timeString = std::to_string(currentTime);
	std::string fileName = "Saves/" + timeString.substr(0, timeString.find('.') + 2) + ".csv";
	m_valenceData.open(fileName);
	std::string allActions = "Action,";
	for (auto it = s_actions.begin(); it != s_actions.end(); it++) {
		allActions += it->first + ",";
	}
	allActions += ",Positive,Negative,Pleased,Displeased,Liking,Disliking,Approving,Disapproving,Hope,Fear,Joy,\
Distress,Love,Hate,Interest,Pride,Shame,Admiration,Reproach,Satisfaction,Frears_confirmed,\
Relief,Disappointment,Happy_For,Resentment,Gloating,Pity,Gratification,Remorse,Gratitude,Anger\n";
	m_valenceData << allActions;
	}

	float gridBoundTop = g_gameConfigBlackboard.GetValue("gridBoundTop", 0.f);
	float gridBoundBottom = g_gameConfigBlackboard.GetValue("gridBoundBottom", 0.f);
	float gridBoundHeight = g_gameConfigBlackboard.GetValue("gridBoundHeight", 0.f);
	m_numOfGridRows = static_cast<int>((gridBoundTop - gridBoundBottom) / gridBoundHeight);
	CalculateGridPositions();
	SpawnCharacters("Ranger", FACING_RIGHT, FACTION_GOOD, 5);
	SpawnCharacters("Tank", FACING_RIGHT, FACTION_GOOD, 0);
	SpawnCharacters("Character", FACING_RIGHT, FACTION_GOOD, 1);
	SpawnCharacters("Healer", FACING_RIGHT, FACTION_GOOD, 6);
	SpawnCharacters("Tank", FACING_LEFT, FACTION_EVIL, 0);
	SpawnCharacters("Tank", FACING_LEFT, FACTION_EVIL, 1);
	SpawnCharacters("Ranger", FACING_LEFT, FACTION_EVIL, 4);
	SpawnCharacters("Healer", FACING_LEFT, FACTION_EVIL, 6);
	InitializeANPCs();
	UpdateAPFAfterActorInitialization(praises);
}


//-----------------------------------------------------------------------------------------------
Level::~Level(){
	for(Character* character:m_allCharacters){
		if(character){
			delete character;
		}
	}
	m_allCharacters.clear();
	m_valenceData.close();
}


//-----------------------------------------------------------------------------------------------
void Level::Render() const{
	RenderBackground();
	if(m_renderSafety){
		RenderColumnSafety();
	}
	if(m_renderGrid){
		RenderGrid();
	}

	std::vector<Character*> tempCharVector = m_characters;
	while (static_cast<int>(tempCharVector.size()) > 0) {
		float highestYValue = -1.f;
		int charIndexNeedToRender = -1;
		for (int charIndex = 0; charIndex < static_cast<int>(tempCharVector.size()); charIndex++) {
			if (tempCharVector[charIndex] && tempCharVector[charIndex]->m_position.y > highestYValue) {
				highestYValue = tempCharVector[charIndex]->m_position.y;
				charIndexNeedToRender = charIndex;
			}
		}
		if (charIndexNeedToRender != -1) {
			tempCharVector[charIndexNeedToRender]->Render();
			tempCharVector.erase(tempCharVector.begin() + charIndexNeedToRender);
		}
	}

	for(Character* character:m_characters){
		if(character && !character->m_isDead){
			character->RenderHealthBar();
		}
	}
}


//-----------------------------------------------------------------------------------------------
void Level::RenderBackground() const{
	std::vector<Vertex_PCU> backgroundVerts;
	backgroundVerts.reserve(6);
	AABB2 backgroundBox(m_game->m_screenCamera.GetOrthoBottomLeft(),
		m_game->m_screenCamera.GetOrthoTopRight());

	AddVertsForAABB2D(backgroundVerts, backgroundBox, Rgba8::WHITE);

	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindTexture(g_gameTextures[GAME_BACKGROUND_TEXTURE]);
	g_theRenderer->BindShader(nullptr);
	g_theRenderer->SetModelTint(Rgba8::WHITE);
	g_theRenderer->DrawVertexArray(static_cast<int>(backgroundVerts.size()), &backgroundVerts[0]);
}


//-----------------------------------------------------------------------------------------------
void Level::RenderGrid() const{
	float lineThickness = 5.f;
	Vec2 screenSize = g_gameConfigBlackboard.GetValue("screenSize", Vec2(0.f, 0.f));
	float gridBoundTop = g_gameConfigBlackboard.GetValue("gridBoundTop", 0.f);
	float gridBoundBottom = g_gameConfigBlackboard.GetValue("gridBoundBottom", 0.f);
	float gridBoundHeight = g_gameConfigBlackboard.GetValue("gridBoundHeight", 0.f);
	float gridBoundWidth = g_gameConfigBlackboard.GetValue("gridBoundWidth", 0.f);
	float gridBoundInline = g_gameConfigBlackboard.GetValue("gridBoundInline", 0.f);
	int numOfLinesOnX = g_gameConfigBlackboard.GetValue("numOfLinesOnX", 0);
	std::vector<Vertex_PCU> gridVerts;
	gridVerts.reserve(6 * static_cast<int>((gridBoundTop - gridBoundBottom)/ gridBoundHeight)
		* numOfLinesOnX * 2);

	float gridHeight = gridBoundTop;
	while(gridHeight >= gridBoundBottom){
		AddVertsForLine(gridVerts, Vec2(0.f, gridHeight), Vec2(screenSize.x, gridHeight), 
			lineThickness, Rgba8::WHITE);
		gridHeight -= gridBoundHeight;
	}

	for(int gridIndex = 0;gridIndex < (numOfLinesOnX+1); gridIndex++){
		float leftStartPoint = 0.f + gridBoundWidth * static_cast<float>(gridIndex);
		AddVertsForLine(gridVerts, Vec2(leftStartPoint, gridBoundBottom),
			Vec2(leftStartPoint + gridBoundInline, gridBoundTop), lineThickness, Rgba8::WHITE);

		float rightStartPoint = screenSize.x - gridBoundWidth * static_cast<float>(gridIndex);
		AddVertsForLine(gridVerts, Vec2(rightStartPoint, gridBoundBottom),
			Vec2(rightStartPoint - gridBoundInline, gridBoundTop), lineThickness, Rgba8::WHITE);
	}
	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindTexture(nullptr);
	g_theRenderer->BindShader(nullptr);
	g_theRenderer->SetModelTint(Rgba8::WHITE);
	g_theRenderer->DrawVertexArray(static_cast<int>(gridVerts.size()), &gridVerts[0]);
}


//-----------------------------------------------------------------------------------------------
void Level::RenderColumnSafety() const{
	Vec2 screenSize = g_gameConfigBlackboard.GetValue("screenSize", Vec2(0.f, 0.f));
	float gridBoundTop = g_gameConfigBlackboard.GetValue("gridBoundTop", 0.f);
	float gridBoundBottom = g_gameConfigBlackboard.GetValue("gridBoundBottom", 0.f);
	float gridBoundWidth = g_gameConfigBlackboard.GetValue("gridBoundWidth", 0.f);
	float gridBoundInline = g_gameConfigBlackboard.GetValue("gridBoundInline", 0.f);
	int numOfLinesOnX = g_gameConfigBlackboard.GetValue("numOfLinesOnX", 0);

	std::vector<Vertex_PCU> safetyVerts;
	if(m_playerSideRedColumn != -1){
		float numOfColumnCrossed = RangeMap(static_cast<float>(m_playerSideRedColumn),
			0.f, static_cast<float>(numOfLinesOnX - 1), static_cast<float>(numOfLinesOnX), 1.f);
		float leftSideTopXEndPosition = 0.f + gridBoundWidth * numOfColumnCrossed
			+ gridBoundInline;
		AddVertsForQuadrangle2D(safetyVerts, Vec2(leftSideTopXEndPosition - gridBoundWidth - gridBoundInline,
			gridBoundBottom), Vec2(leftSideTopXEndPosition - gridBoundInline, gridBoundBottom),
			Vec2(leftSideTopXEndPosition - gridBoundWidth, gridBoundTop),
			Vec2(leftSideTopXEndPosition, gridBoundTop), Rgba8::RED);
		float boundWidth = 0.f;
		int playerColumn = m_playerSideRedColumn + 1;
		if(playerColumn <= (numOfLinesOnX-1)){
			numOfColumnCrossed = RangeMap(static_cast<float>(playerColumn),
				0.f, static_cast<float>(numOfLinesOnX-1), static_cast<float>(numOfLinesOnX), 1.f);
			if(((numOfLinesOnX - 1) - playerColumn) > 0){
				boundWidth = 2.f * gridBoundWidth;
				playerColumn += 2;
			}
			else{
				boundWidth = gridBoundWidth;
				playerColumn += 1;
			}
			
			leftSideTopXEndPosition = 0.f + gridBoundWidth * numOfColumnCrossed
				+ gridBoundInline;
			AddVertsForQuadrangle2D(safetyVerts, Vec2(leftSideTopXEndPosition - boundWidth - 
				gridBoundInline, gridBoundBottom), Vec2(leftSideTopXEndPosition - gridBoundInline, 
				gridBoundBottom), Vec2(leftSideTopXEndPosition - boundWidth, gridBoundTop),
				Vec2(leftSideTopXEndPosition, gridBoundTop), Rgba8::YELLOW);
		}
		if (playerColumn <= (numOfLinesOnX - 1)) {
			numOfColumnCrossed = RangeMap(static_cast<float>(playerColumn),
				0.f, static_cast<float>(numOfLinesOnX - 1), static_cast<float>(numOfLinesOnX), 1.f);
			if (((numOfLinesOnX - 1) - playerColumn) > 0) {
				boundWidth = 2.f * gridBoundWidth;
			}
			else {
				boundWidth = gridBoundWidth;
			}

			leftSideTopXEndPosition = 0.f + gridBoundWidth * numOfColumnCrossed
				+ gridBoundInline;
			AddVertsForQuadrangle2D(safetyVerts, Vec2(leftSideTopXEndPosition - boundWidth -
				gridBoundInline, gridBoundBottom), Vec2(leftSideTopXEndPosition - gridBoundInline,
					gridBoundBottom), Vec2(leftSideTopXEndPosition - boundWidth, gridBoundTop),
				Vec2(leftSideTopXEndPosition, gridBoundTop), Rgba8::GREEN);
		}
	}
	if (m_enemySideRedColumn != -1) {
		float numOfColumnCrossed = RangeMap(static_cast<float>(m_enemySideRedColumn),
			0.f, static_cast<float>(numOfLinesOnX - 1), static_cast<float>(numOfLinesOnX), 1.f);
		float rightSideTopXEndPosition = screenSize.x - gridBoundWidth * numOfColumnCrossed
			- gridBoundInline;
		AddVertsForQuadrangle2D(safetyVerts, Vec2(rightSideTopXEndPosition + gridBoundWidth + gridBoundInline,
			gridBoundBottom), Vec2(rightSideTopXEndPosition + gridBoundInline, gridBoundBottom),
			Vec2(rightSideTopXEndPosition + gridBoundWidth, gridBoundTop),
			Vec2(rightSideTopXEndPosition, gridBoundTop), Rgba8::RED);
		float boundWidth = 0.f;
		int enemyColumn = m_enemySideRedColumn + 1;
		if (enemyColumn <= (numOfLinesOnX - 1)) {
			numOfColumnCrossed = RangeMap(static_cast<float>(enemyColumn),
				0.f, static_cast<float>(numOfLinesOnX - 1), static_cast<float>(numOfLinesOnX), 1.f);
			if (((numOfLinesOnX - 1) - enemyColumn) > 0) {
				boundWidth = 2.f * gridBoundWidth;
				enemyColumn += 2;
			}
			else {
				boundWidth = gridBoundWidth;
				enemyColumn += 1;
			}

			rightSideTopXEndPosition = screenSize.x - gridBoundWidth * numOfColumnCrossed
				- gridBoundInline;
			AddVertsForQuadrangle2D(safetyVerts, Vec2(rightSideTopXEndPosition + boundWidth +
				gridBoundInline, gridBoundBottom), Vec2(rightSideTopXEndPosition + gridBoundInline,
					gridBoundBottom), Vec2(rightSideTopXEndPosition + boundWidth, gridBoundTop),
				Vec2(rightSideTopXEndPosition, gridBoundTop), Rgba8::YELLOW);
		}
		if (enemyColumn <= (numOfLinesOnX - 1)) {
			numOfColumnCrossed = RangeMap(static_cast<float>(enemyColumn),
				0.f, static_cast<float>(numOfLinesOnX - 1), static_cast<float>(numOfLinesOnX), 1.f);
			if (((numOfLinesOnX - 1) - enemyColumn) > 0) {
				boundWidth = 2.f * gridBoundWidth;
			}
			else {
				boundWidth = gridBoundWidth;
			}

			rightSideTopXEndPosition = screenSize.x - gridBoundWidth * numOfColumnCrossed
				- gridBoundInline;
			AddVertsForQuadrangle2D(safetyVerts, Vec2(rightSideTopXEndPosition + boundWidth +
				gridBoundInline, gridBoundBottom), Vec2(rightSideTopXEndPosition + gridBoundInline,
					gridBoundBottom), Vec2(rightSideTopXEndPosition + boundWidth, gridBoundTop),
				Vec2(rightSideTopXEndPosition, gridBoundTop), Rgba8::GREEN);
		}
	}
	if(safetyVerts.size() > 0){
		g_theRenderer->SetBlendMode(BlendMode::ALPHA);
		g_theRenderer->BindTexture(nullptr);
		g_theRenderer->BindShader(nullptr);
		g_theRenderer->SetModelTint(Rgba8::WHITE);
		g_theRenderer->DrawVertexArray(static_cast<int>(safetyVerts.size()), &safetyVerts[0]);
	}
}


//-----------------------------------------------------------------------------------------------
void Level::Update(float deltaSeconds) {
// 	APF_Lib::MM::MemoryClock::SetGlobalTimeScale(1.f);
// 	APF_Lib::MM::MemoryClock::BeginFrame(1000.f * deltaSeconds);
	UpdateBattleProgress();
	if(!m_gameComplete){
		
		UpdateGridSafetyness();
		UpdateCharacterTurn();
		for (int charIndex = 0; charIndex < static_cast<int>(m_characters.size()); charIndex++) {
			if (m_characters[charIndex]) {
				m_characters[charIndex]->Update(deltaSeconds);
			}
		}
		DeleteGarbageCharacters();
	}
	HandleKeyAcvities();
}


//-----------------------------------------------------------------------------------------------
void Level::DeleteGarbageCharacters(){
	for (int charIndex = 0; charIndex < static_cast<int>(m_characters.size()); ) {
		if (m_characters[charIndex] && m_characters[charIndex]->m_isGarbage) {
			m_characters.erase(m_characters.begin() + charIndex);
		}
		else {
			charIndex++;
		}
	}
}


//-----------------------------------------------------------------------------------------------
void Level::HandleKeyAcvities(){
	if(g_theInputSystem->WasKeyJustPressed('Q')){
		for(Character* character:m_characters){
			if(character && !character->m_isGarbage){
				character->m_renderCharTextureBound = !character->m_renderCharTextureBound;
			}
		}
	}
}


//-----------------------------------------------------------------------------------------------
Vec2 Level::GetGridPositions(int index, Faction faction){
	if(faction == FACTION_GOOD){
		return m_playerSidePositions[index];
	}
	else if(faction == FACTION_EVIL){
		return m_enemySidePositions[index];
	}
	return Vec2::ZERO;
}	


//-----------------------------------------------------------------------------------------------
Character* Level::SpawnCharacters(std::string name, FacingDirection direction, Faction faction,
	int gridIndex){
	Vec2 position;
	if(faction == FACTION_GOOD){
		position = m_playerSidePositions[gridIndex];
	}
	else if(faction == FACTION_EVIL){
		position = m_enemySidePositions[gridIndex];
	}
	if(name == "Tank"){
		Tank* tank = new Tank(this, gridIndex, position, name, direction,faction, false,
			(int)m_allCharacters.size());
		AddCharacter(tank,std::string("Tank"));
		return tank;
	}
	else if(name == "Ranger") {
		Ranger* ranger = new Ranger(this, gridIndex, position, name, direction, faction, false,
			(int)m_allCharacters.size());
		AddCharacter(ranger, std::string("Ranger"));
		return ranger;
	}
	else if (name == "Healer") {
		Healer* healer = new Healer(this, gridIndex, position, name, direction, faction, false,
			(int)m_allCharacters.size());
		AddCharacter(healer, std::string("Healer"));
		return healer;
	}
	else if (name == "Character") {
		Character* anpc = new Character(this, gridIndex, position, name, direction, faction,true,
			(int)m_allCharacters.size());
		AddCharacter(anpc, std::string("Healer"));
		return anpc;
	}
	return nullptr;
}


//-----------------------------------------------------------------------------------------------
Character* Level::FindCharacterByIndex(int index, Faction faction){
	for(Character* character : m_characters){
		if(character->m_faction == faction && character->m_gridIndex == index){
			return character;
		}
	}
	return nullptr;
}


//-----------------------------------------------------------------------------------------------
Character* Level::FindCharacterByName(std::string name){
	for (int charIndex = 0; charIndex < static_cast<int>(m_characters.size()); charIndex++) {
		if (m_characters[charIndex] && m_characters[charIndex]->m_name == name) {
			return m_characters[charIndex];
		}
	}
	return nullptr;
}


//-----------------------------------------------------------------------------------------------
CharacterStats Level::GetTotalAllyStats() const{
	CharacterStats totalStats;
	for(Character* character : m_allCharacters){
		if(character->m_faction == FACTION_GOOD){
			totalStats += character->m_stats;
		}
	}
	return totalStats;
}


//-----------------------------------------------------------------------------------------------
CharacterStats Level::GetStatsChange(int startTurn, int endTurn, int actorIndex) const {
	CharacterStats emptyStats;
	for(Character* character: m_allCharacters){
		if (character->m_memoryActor->GetIndex() == actorIndex) {
			if (endTurn >= (int)character->m_allStats.size()) {
				return emptyStats;
			}
			else if(startTurn == (int)character->m_allStats.size()){
				return character->m_stats - character->m_allStats[endTurn];
			}
			else if(startTurn > (int)character->m_allStats.size()){
				return character->m_allStats.back() - character->m_allStats[endTurn];
			}
			return character->m_allStats[startTurn] - character->m_allStats[endTurn];
		}
	}
	return emptyStats;
}


//-----------------------------------------------------------------------------------------------
std::string Level::GetCharacterRoleByIndex(int actorIndex) const{
	Character* targetChar = nullptr;
	for(Character* character : m_allCharacters){
		if(character && character->m_memoryActor->GetIndex() == actorIndex){
			targetChar = character;
		}
	}
	if(dynamic_cast<Tank*>(targetChar)){
		return "Tank";
	}
	else if (dynamic_cast<Healer*>(targetChar)) {
		return "Healer";
	}
	else if (dynamic_cast<Ranger*>(targetChar)) {
		return "Ranger";
	}
	return "";
}


//-----------------------------------------------------------------------------------------------
bool Level::UpdateContinuousAction(APF_Lib::MM::MemoryAndEvent* memory){
	return m_anpcCharacter->UpdateContinuousActionPraiseByExpectation(memory);
}


//-----------------------------------------------------------------------------------------------
bool Level::IsCharacterInRedZone(Character* character){
	int characterColumn = character->m_gridIndex / m_numOfGridRows;
	if(character->m_faction == FACTION_GOOD){
		if(characterColumn == m_playerSideRedColumn){
			return true;
		}
	}
	else if (character->m_faction == FACTION_EVIL) {
		if (characterColumn == m_enemySideRedColumn) {
			return true;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Level::IsCharacterInYellowZone(Character* character){
	int characterColumn = character->m_gridIndex / m_numOfGridRows;
	if (character->m_faction == FACTION_GOOD) {
		if (characterColumn <= (m_playerSideRedColumn + 2) && characterColumn > m_playerSideRedColumn) {
			return true;
		}
	}
	else if (character->m_faction == FACTION_EVIL) {
		if (characterColumn >= (m_enemySideRedColumn-2) && characterColumn < m_playerSideRedColumn) {
			return true;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Level::IsCharacterInGreenZone(Character* character){
	int characterColumn = character->m_gridIndex / m_numOfGridRows;
	if (character->m_faction == FACTION_GOOD) {
		if (characterColumn >= (m_playerSideRedColumn + 3)) {
			return true;
		}

	}
	else if (character->m_faction == FACTION_EVIL) {
		if (characterColumn >= (m_enemySideRedColumn - 3) && characterColumn <
			(m_enemySideRedColumn - 2)) {
			return true;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Level::IsEventContinuous(int eventIdx){
	for(auto iter= s_actions.begin(); iter != s_actions.end(); iter++){
		if(iter->second->apfIdx == eventIdx){
			if(iter->first == "Defense" || iter->first == "Move forward" || iter->first == "Move backward"){
				return true;
			}
			return false;
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
void Level::ProcessNewEvent(APF_Lib::MM::GameEvent& event){
	Character* actor = nullptr;
	for (Character* character : m_characters) {
		if (character->m_memoryActor->GetIndex() == event.m_actor) {
			actor = character;
		}
	}
	event.m_owner = m_anpcCharacter->m_memoryActor->GetIndex();
	g_APF->AddEventWithANPC(event.m_owner, event.m_actor, event.m_action, event.m_patient, event.m_certainty);
	g_APF->ProcessEventWithoutMemory();

	if(g_theGame->m_trainState != PSO_VALENCE){
		if (!IsEventContinuous(event.m_action) && event.m_actor == event.m_owner) {
			m_anpcCharacter->UpdateImiediateActionPraiseByExpectation(event);
		}
		CharacterStats actorStatsChange = actor->m_stats - actor->m_allStats.back();
		m_anpcCharacter->UpdateMemory(event, actorStatsChange);
	}

	if(g_theGame->m_trainState != PSO_VALENCE && g_theGame->m_trainState != PSO_PERSONALITY){
		std::vector<float> praises = g_APF->GetPraises(m_anpcCharacter->m_name.c_str());
		std::string valenceString = "";
		if (actor->m_isANPC) {
			valenceString += event.m_actionInfo->actionName + " (ANPC) " + std::to_string(m_totalTurnCount) + ",";
		}
		else {
			valenceString += event.m_actionInfo->actionName + ",";
		}
// 		for (float praise : praises) {
// 			valenceString += std::to_string(praise).substr(0, std::to_string(praise).find('.') + 4) + ',';
// 		}
		for(Character* character : m_allCharacters){
			valenceString += std::to_string(character->m_health).substr(0, std::to_string(character->m_health).find('.') + 4) + ' ';
			valenceString += std::to_string(character->m_currentSpeed).substr(0, std::to_string(character->m_currentSpeed).find('.') + 10) + ',';
		}
		valenceString += actor->m_name + '\n';
		m_valenceData << valenceString;
	}

	UpdateANPCZoneInfo();
	for(Character* character: m_characters){
		character->RecordStats();
	}
}


//-----------------------------------------------------------------------------------------------
void Level::AddCharacter(Character* character, std::string role){
	m_charSalt = CharacterUID::GetNextSalt(m_charSalt);
	int freeIndex = GetFreeCharacterSlot();

	CharacterUID newUID(freeIndex, m_charSalt);
	int uidData = (m_charSalt << 16) | freeIndex;
	if(m_game->m_trainState == PSO_VALENCE || m_game->m_trainState == PSO_PERSONALITY){
		uidData = uidData * 100 + m_swarmIndex;
		character->SetCharUID(newUID, std::to_string(uidData));
	}
	else{
		if(character->m_isANPC){
			role = "ANPC";
		}
		character->SetCharUID(newUID, role + std::to_string(uidData));
	}

	if (freeIndex >= static_cast<int>(m_characters.size())) {
		m_characters.push_back(character);
		m_allCharacters.push_back(character);
	}
	else {
		m_characters[freeIndex] = character;
		m_allCharacters.push_back(character);
	}
}


//-----------------------------------------------------------------------------------------------
int Level::GetFreeCharacterSlot() const{
	int freeSlot = static_cast<int>(m_characters.size());
	for (int unitIndex = 0; unitIndex < static_cast<int>(m_characters.size()); unitIndex++) {
		if (!m_characters[unitIndex]) {
			freeSlot = unitIndex;
		}
	}
	return freeSlot;
}


//-----------------------------------------------------------------------------------------------
void Level::UpdateGridSafetyness(){
	int minGridIndex = 9999;
	for (Character* character : m_characters) {
		if (character && !character->m_isDead && !character->m_isGarbage) {
			if (character->m_faction == FACTION_GOOD) {
				for (int gridIndex = 0; gridIndex < static_cast<int>(m_playerSidePositions.size());
					gridIndex++) {
					if (character->m_position == m_playerSidePositions[gridIndex]
						&& gridIndex < minGridIndex) {
						minGridIndex = gridIndex;
						break;
					}
				}
			}
		}
	}
	if (minGridIndex != 9999) {
		int playerColumn = minGridIndex / m_numOfGridRows;
		m_playerSideRedColumn = playerColumn;
	}
	else {
		m_playerSideRedColumn = -1;
	}
	minGridIndex = 9999;
	for (Character* character : m_characters) {
		if (character && !character->m_isDead && !character->m_isGarbage) {
			if (character->m_faction == FACTION_EVIL) {
				for (int gridIndex = 0; gridIndex < static_cast<int>(m_enemySidePositions.size());
					gridIndex++) {
					if (character->m_position == m_enemySidePositions[gridIndex]
						&& gridIndex < minGridIndex) {
						minGridIndex = gridIndex;
						break;
					}
				}
			}
		}
	}
	if (minGridIndex != 9999) {
		int enemyColumn = minGridIndex / m_numOfGridRows;
		m_enemySideRedColumn = enemyColumn;
	}
	else {
		m_enemySideRedColumn = -1;
	}
}


//-----------------------------------------------------------------------------------------------
void Level::UpdateBattleProgress(){
	int totalGoodCharacters = 0;
	int totalEvilCharacters = 0;
	for(Character* character : m_characters){
		if(character->m_faction == FACTION_GOOD){
			totalGoodCharacters++;
		}
		else if(character->m_faction == FACTION_EVIL){
			totalEvilCharacters++;
		}
	}
	if(totalEvilCharacters == 0){

		m_gameComplete = true;
		m_doesAIWinTheBattle = true;
	}
	else if(totalGoodCharacters == 0){
		m_gameComplete = true;
		m_doesAIWinTheBattle = false;
	}
}


//-----------------------------------------------------------------------------------------------
void Level::CalculateGridPositions(){
	Vec2 screenSize = g_gameConfigBlackboard.GetValue("screenSize", Vec2(0.f, 0.f));
	float gridBoundTop = g_gameConfigBlackboard.GetValue("gridBoundTop", 0.f);
	float gridBoundBottom = g_gameConfigBlackboard.GetValue("gridBoundBottom", 0.f);
	float gridBoundHeight = g_gameConfigBlackboard.GetValue("gridBoundHeight", 0.f);
	float gridBoundWidth = g_gameConfigBlackboard.GetValue("gridBoundWidth", 0.f);
	float gridBoundInline = g_gameConfigBlackboard.GetValue("gridBoundInline", 0.f);
	int numOfLinesOnX = g_gameConfigBlackboard.GetValue("numOfLinesOnX", 0);

	float inlineRatio = (gridBoundTop - gridBoundBottom) / gridBoundInline;
	float leftSideTopXEndPosition = 0.f + gridBoundWidth * static_cast<float>(numOfLinesOnX)
		+ gridBoundInline;
	float rightSideTopXEndPosition = screenSize.x - gridBoundWidth * static_cast<float>(numOfLinesOnX)
		- gridBoundInline;
	std::vector<float> leftSideIntersectXValues;
	std::vector<float> rightSideIntersectXValues;
	for(int index = 1; index <= m_numOfGridRows; index++){
		float height = index * gridBoundHeight;
		float xDistance = height / inlineRatio;
		leftSideIntersectXValues.push_back(leftSideTopXEndPosition - xDistance);
		rightSideIntersectXValues.push_back(rightSideTopXEndPosition + xDistance);
	}

	float halfWidth = gridBoundWidth * 0.5f;
	float halfHeight = gridBoundHeight * 0.5f;
	for(int xLineIndex = 1; xLineIndex <= numOfLinesOnX; xLineIndex++){
		float gridHeight = gridBoundTop;
		for(int yLineIndex = 0; yLineIndex < m_numOfGridRows; yLineIndex++){
			Vec2 gridCenter(leftSideIntersectXValues[yLineIndex] - halfWidth,
				gridHeight - halfHeight);
			m_playerSidePositions.push_back(gridCenter);

			gridCenter = Vec2(rightSideIntersectXValues[yLineIndex] + halfWidth,
				gridHeight - halfHeight);
			m_enemySidePositions.push_back(gridCenter);
			leftSideIntersectXValues[yLineIndex] -= gridBoundWidth;
			rightSideIntersectXValues[yLineIndex] += gridBoundWidth;
			gridHeight -= gridBoundHeight;
		}
	}
}


//-----------------------------------------------------------------------------------------------
void Level::UpdateCharacterTurn(){
	if(!m_currentTurnCharacter || !m_currentTurnCharacter->m_isMyTurn){
		float minSpeed = 99999.f;
		int minSpeedIndex = -1;
// 		if (m_game->m_trainState == PSO_RENDER_SIMULATION && m_totalTurnCount == 97) {
// 			int i = 0;
// 		}
		for(int charIndex = 0; charIndex < (int)(m_characters.size()); charIndex++){
			if(m_characters[charIndex] && !m_characters[charIndex]->m_isDead &&
				!m_characters[charIndex]->m_isGarbage && m_characters[charIndex]->m_currentSpeed < minSpeed){
				minSpeed = m_characters[charIndex]->m_currentSpeed;
				minSpeedIndex = charIndex;
			}
		}
		if(minSpeedIndex != -1){
			m_currentTurnCharacter = m_characters[minSpeedIndex];
			m_currentTurnCharacter->ResetSpeed();
			m_currentTurnCharacter->m_isMyTurn = true;

			for (Character* character : m_characters) {
				if (character && !character->m_isDead && !character->m_isGarbage
					&& character != m_currentTurnCharacter) {
					character->m_currentSpeed -= minSpeed;
				}
			}
			m_totalTurnCount++;
		}
		
	}
}


//-----------------------------------------------------------------------------------------------
void Level::UpdateAPFAfterActorInitialization(){
	// update indexes
	for(Character* character : m_characters){
		character->m_memoryActor->SetAPFIndex(
			g_APF->GetANPCIndex(character->m_memoryActor->GetName().c_str()));
		if(character->m_isANPC){
			m_anpcCharacter = character;
		}
	}

	// initialize mental states
	g_APF->InitializeANPCsMentalStates();
	if(m_anpcCharacter){
		APF_Lib::MM::GameActor* gameActor = m_anpcCharacter->m_memoryActor;
		APF_Lib::MentalState new_mental_state;
		g_APF->SetANPCsMentalState(gameActor->GetIndex(), new_mental_state);
		gameActor->GetMemorySystem()->m_getProspectFromTraces = 
			GenerateMentalSalientProspectMemoryFromTraces;
	}
	
	// initialize relations
	g_APF->InitializeRelations();
	if(m_game->m_trainState != Q_LEARNING_VALENCE && g_theGame->m_trainState != PSO_RENDER_SIMULATION){
		if ((int)(g_APF->GetPraises(m_anpcCharacter->m_name.c_str()).size()) <= 0) {
			LoadPraisesFromXML("Data/Definitions/Praises.xml", m_anpcCharacter->m_memoryActor->GetIndex());
		}
	}
	else{
		if ((int)(g_APF->GetPraises(m_anpcCharacter->m_name.c_str()).size()) <= 0) {
			int index = 0;
			for(auto iter = s_actions.begin(); iter!= s_actions.end(); iter++){
				g_APF->AddPraise(m_anpcCharacter->m_memoryActor->GetIndex(), iter->second->apfIdx, 
					m_game->m_currentValenceState.m_praises[index]);
				index++;
			}
		}
	}
	Faction anpcEnemyFaction = m_anpcCharacter->GetOppositeFaction();
	for (Character* character : m_characters) {
		if(character->m_faction == m_anpcCharacter->m_faction){
			g_APF->AddSocial(m_anpcCharacter->m_memoryActor->GetIndex(),
				character->m_memoryActor->GetIndex(), s_socials["Ally"].m_traits[0],
				s_socials["Ally"].m_traits[1], s_socials["Ally"].m_traits[2],
				s_socials["Ally"].m_traits[3]);
		}
		else if(character->m_faction == anpcEnemyFaction){
			g_APF->AddSocial(m_anpcCharacter->m_memoryActor->GetIndex(),
				character->m_memoryActor->GetIndex(), s_socials["Enemy"].m_traits[0],
				s_socials["Enemy"].m_traits[1], s_socials["Enemy"].m_traits[2],
				s_socials["Enemy"].m_traits[3]);
		}
	}

	g_APF->InitializeEventHandler();


	APF_Lib::MM::ObjectState state;
	m_anpcCharacter->m_memoryActor->SetNewState(state);
	APF_Lib::MM::GameState gameState;
	gameState.UpdateStateForActor(m_anpcCharacter->m_memoryActor);
	APF_Lib::MM::Memory curMemory;
	m_anpcCharacter->m_memoryActor->GenerateMemory(gameState, curMemory);
	m_anpcCharacter->m_memoryActor->InitializeSTM(curMemory);
}



//-----------------------------------------------------------------------------------------------
void Level::UpdateAPFAfterActorInitialization(std::vector<float> const& praises){
	// update indexes
	for (Character* character : m_characters) {
		character->m_memoryActor->SetAPFIndex(
			g_APF->GetANPCIndex(character->m_memoryActor->GetName().c_str()));
		if (character->m_isANPC) {
			m_anpcCharacter = character;
		}
	}

	// initialize mental states
	g_APF->InitializeANPCsMentalStates();
	if (m_anpcCharacter) {
		APF_Lib::MM::GameActor* gameActor = m_anpcCharacter->m_memoryActor;
		APF_Lib::MentalState new_mental_state;
		g_APF->SetANPCsMentalState(gameActor->GetIndex(), new_mental_state);
		gameActor->GetMemorySystem()->m_getProspectFromTraces =
			GenerateMentalSalientProspectMemoryFromTraces;
	}

	// initialize relations
	g_APF->InitializeRelations();
	if ((int)(g_APF->GetPraises(m_anpcCharacter->m_name.c_str()).size()) <= 0) {
		int index = 0;
		for (auto iter = s_actions.begin(); iter != s_actions.end(); iter++) {
			g_APF->AddPraise(m_anpcCharacter->m_memoryActor->GetIndex(), iter->second->apfIdx,
				praises[index]);
			index++;
		}
	}

	Faction anpcEnemyFaction = m_anpcCharacter->GetOppositeFaction();
	for (Character* character : m_characters) {
		if (character->m_faction == m_anpcCharacter->m_faction) {
			g_APF->AddSocial(m_anpcCharacter->m_memoryActor->GetIndex(),
				character->m_memoryActor->GetIndex(), s_socials["Ally"].m_traits[0],
				s_socials["Ally"].m_traits[1], s_socials["Ally"].m_traits[2],
				s_socials["Ally"].m_traits[3]);
		}
		else if (character->m_faction == anpcEnemyFaction) {
			g_APF->AddSocial(m_anpcCharacter->m_memoryActor->GetIndex(),
				character->m_memoryActor->GetIndex(), s_socials["Enemy"].m_traits[0],
				s_socials["Enemy"].m_traits[1], s_socials["Enemy"].m_traits[2],
				s_socials["Enemy"].m_traits[3]);
		}
	}

	g_APF->InitializeEventHandler();


	APF_Lib::MM::ObjectState state;
	m_anpcCharacter->m_memoryActor->SetNewState(state);
	APF_Lib::MM::GameState gameState;
	gameState.UpdateStateForActor(m_anpcCharacter->m_memoryActor);
	APF_Lib::MM::Memory curMemory;
	m_anpcCharacter->m_memoryActor->GenerateMemory(gameState, curMemory);
	m_anpcCharacter->m_memoryActor->InitializeSTM(curMemory);
}


//-----------------------------------------------------------------------------------------------
void Level::UpdateANPCZoneInfo(){
	if(!m_anpcCharacter || m_anpcCharacter->m_isDead || m_anpcCharacter->m_isGarbage){
		return;
	}
	if(IsCharacterInRedZone(m_anpcCharacter)){
		m_anpcCharacter->m_stats.m_redZoneCount++;
	}
	else if (IsCharacterInYellowZone(m_anpcCharacter)) {
		m_anpcCharacter->m_stats.m_yellowZoneCount++;
	}
	else{
		m_anpcCharacter->m_stats.m_greenZoneCount++;
	}
}


//-----------------------------------------------------------------------------------------------
void Level::InitializeANPCs(){
	std::map<std::string, Character*> sortedCharacters;
	for(Character* character : m_characters){
		sortedCharacters[character->m_name] = character;
	}
	for(auto it = sortedCharacters.begin(); it!=sortedCharacters.end(); it++){
		it->second->InitializeAPFACtor();
	}
}

