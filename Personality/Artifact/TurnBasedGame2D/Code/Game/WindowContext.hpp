#define WIN32_LEAN_AND_MEAN		// Always #define this before #including <windows.h>
#include <windows.h>			// #include this (massive, platform-specific) header in very few places
#include <math.h>
#include <cassert>
#include <crtdbg.h>

//-----------------------------------------------------------------------------------------------
struct WindowContext
{
public: // NOTE: this is one of the few cases where we break both the "m_" naming rule AND the avoid-public-members rule
	HWND m_windowHandle = nullptr;
	HDC m_displayContext = nullptr;

public:
	// Construction/Destruction
	~WindowContext() {}												// destructor (do nothing)
	WindowContext() {}												// default constructor (do nothing)
};


