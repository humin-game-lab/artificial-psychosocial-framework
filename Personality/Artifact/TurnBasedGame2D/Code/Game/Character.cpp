#include "Game/Character.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"
#include "Game/APFUtils.hpp"
#include "Game/GameSettingWindow.hpp"
#include "RL/RLBrain.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Memory/ShortTermMemory.hpp"
#include "Memory/Memory.hpp"
#include "Engine/BehaviorTree/BehaviorTree.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/VertexUtils.hpp"
#include "Engine/Renderer/SpriteAnimDefinition.hpp"
#include "Engine/Math/MathUtils.hpp"
#include "Engine/BehaviorTree/ActionNode.hpp"
#include "Engine/BehaviorTree/DecoratorNode.hpp"
#include "ThirdParty/APF/Memory/MemorySystem.hpp"
#include "ThirdParty/Squirrel/RawNoise.hpp"


//-----------------------------------------------------------------------------------------------
void CharacterStats::operator+=(const CharacterStats& vecToAdd) {
	m_damageDealt += vecToAdd.m_damageDealt;
	m_damageTaken += vecToAdd.m_damageTaken;
	m_damageBlocked += vecToAdd.m_damageBlocked;
	m_damageHealed += vecToAdd.m_damageHealed;
	m_allyHealing += vecToAdd.m_allyHealing;
}


//-----------------------------------------------------------------------------------------------
void CharacterStats::operator/=(float valueToDivide){
	m_damageDealt /= valueToDivide;
	m_damageTaken /= valueToDivide;
	m_damageBlocked /= valueToDivide;
	m_damageHealed /= valueToDivide;
	m_allyHealing /= valueToDivide;
}


//-----------------------------------------------------------------------------------------------
void CharacterStats::operator*=(float valueToMultiply){
	m_damageDealt *= valueToMultiply;
	m_damageTaken *= valueToMultiply;
	m_damageBlocked *= valueToMultiply;
	m_damageHealed *= valueToMultiply;
	m_allyHealing *= valueToMultiply;
}


//-----------------------------------------------------------------------------------------------
void CharacterStats::operator=(const CharacterStats& copy){
	m_damageDealt = copy.m_damageDealt;
	m_damageTaken = copy.m_damageTaken;
	m_damageBlocked = copy.m_damageBlocked;
	m_damageHealed = copy.m_damageHealed;
	m_allyHealing = copy.m_allyHealing;
}


//-----------------------------------------------------------------------------------------------
CharacterStats CharacterStats::operator-(const CharacterStats& statsToSubtract){
	CharacterStats statsChange;
	statsChange.m_damageDealt = m_damageDealt - statsToSubtract.m_damageDealt;
	statsChange.m_damageTaken = m_damageTaken - statsToSubtract.m_damageTaken;
	statsChange.m_damageBlocked = m_damageBlocked - statsToSubtract.m_damageBlocked;
	statsChange.m_damageHealed = m_damageHealed - statsToSubtract.m_damageHealed;
	statsChange.m_allyHealing = m_allyHealing - statsToSubtract.m_allyHealing;
	return statsChange;
}


//-----------------------------------------------------------------------------------------------
Character::Character(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,
	FacingDirection direction, Faction faction, bool isANPC, int speedVariance) {
	 m_level = level;
	 m_gridIndex = gridIndex;
	 m_position = position;
	 m_definition = CharacterDefinition::s_charDefinitions[unitDef];
	 m_faction = faction;
	 m_facingDirection = direction;
	 m_isANPC = isANPC;

	 m_velocity = Vec2(0.f,0.f);
	 m_health = m_definition.m_health;
	 m_defaultSpeed = m_definition.m_speed;
	 m_defaultSpeed += 0.001f * Get1dNoiseZeroToOne(speedVariance, 10);
	 m_defaultSpeed += 0.0001f * Get1dNoiseZeroToOne(speedVariance, 11);
	 m_currentSpeed = m_defaultSpeed;
	 m_isDead = false;
	 m_isGarbage = false;
	 m_currentAnimation = m_definition.m_animations["idle"];
	 m_animationClock.SetParent(g_theGame->m_stateClock);
	 m_attackAnimTime.SetClock(g_theGame->m_stateClock);
	 m_painAnimTime.SetClock(g_theGame->m_stateClock);
	 m_healAnimTime.SetClock(g_theGame->m_stateClock);
	 m_fadeInTimer.SetClock(g_theGame->m_stateClock);
	 m_fadeOutTimer.SetClock(g_theGame->m_stateClock);
	 m_actionTimer.SetClock(g_theGame->m_stateClock);
	 m_dieFadeOutTime.SetClock(g_theGame->m_stateClock);
}


//-----------------------------------------------------------------------------------------------
Character::Character(Character* character){
	m_health = character->m_health;
	m_name = character->m_name;
	m_faction = character->m_faction;
	m_stats = character->m_stats;
}


//-----------------------------------------------------------------------------------------------
Character::~Character(){
	if (m_memoryActor) {
		delete m_memoryActor;
		m_memoryActor = nullptr;
	}
	if(m_behaviorTree){
		delete m_behaviorTree;
		m_behaviorTree = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::Render() const{
	std::vector<Vertex_PCU> charVerts;
	AABB2 charBound(Vec2::ZERO, Vec2(m_renderScale.x * 200.f,
		 m_renderScale.y * 200.f));
	
	charBound.SetCenter(Vec2(m_position.x, m_position.y + m_renderScale.y * 80.f));

	AABB2 uvs = 
		m_currentAnimation->GetUVsAtTime(static_cast<float>(m_animationClock.GetTotalSeconds()));
	if (m_facingDirection == FACING_LEFT) {
		float tempX = uvs.m_mins.x;
		uvs.m_mins.x = uvs.m_maxs.x;
		uvs.m_maxs.x = tempX;
	}

	Rgba8 renderColor = Rgba8::WHITE;
	if (m_fadeOutTimer.IsRunning()) {
		float fadingOutTime = static_cast<float>(m_fadeOutTimer.GetElapsedSeconds());
		renderColor.a = static_cast<uchar>(RangeMapClamped(fadingOutTime, 0.f, 0.5f, 255.f, 0.f));
	}
	AddVertsForAABB2D(charVerts, charBound, renderColor, uvs.m_mins,
		uvs.m_maxs);
	g_theRenderer->SetFillMode(FillMode::SOLID);
	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindShader(nullptr);
	g_theRenderer->SetModelTint(Rgba8::WHITE);
	g_theRenderer->BindTexture(&m_currentAnimation->GetTexture());
	g_theRenderer->DrawVertexArray(static_cast<int>(charVerts.size()), &charVerts[0]);


	if(m_renderCharTextureBound){
		std::vector<Vertex_PCU> boundVerts;
		AddVertsForLine(boundVerts, charBound.m_mins, Vec2(charBound.m_maxs.x, charBound.m_mins.y), 2.f, Rgba8::BLUE);
		AddVertsForLine(boundVerts, charBound.m_mins, Vec2(charBound.m_mins.x, charBound.m_maxs.y), 2.f, Rgba8::BLUE);
		AddVertsForLine(boundVerts, Vec2(charBound.m_maxs.x, charBound.m_mins.y), charBound.m_maxs, 2.f, Rgba8::BLUE);
		AddVertsForLine(boundVerts, Vec2(charBound.m_mins.x, charBound.m_maxs.y), charBound.m_maxs, 2.f, Rgba8::BLUE);
		g_theRenderer->SetFillMode(FillMode::SOLID);
		g_theRenderer->SetBlendMode(BlendMode::ALPHA);
		g_theRenderer->BindShader(nullptr);
		g_theRenderer->BindTexture(nullptr);
		g_theRenderer->SetModelTint(Rgba8::WHITE);
		g_theRenderer->DrawVertexArray(static_cast<int>(boundVerts.size()), &boundVerts[0]);
	}

}


//-----------------------------------------------------------------------------------------------
void Character::Update(float deltaTime) {
	if(m_isMyTurn){
		if(m_isANPC){
			if(!m_chosenAction){
				if(g_theGame->m_trainState == PSO_PERSONALITY){
					ChooseRandomAction();
				}
				else{
					DetermineANPCAction();
				}
			}
		}
		if(m_skipCounter >= SKIP_TURN_AFTER_NUM_TRYS_FAILED){
			m_isMyTurn = false;
			m_skipCounter = 0;
		}
		else if(m_chosenAction){
			m_chosenAction(m_defaultArguments);
			m_skipCounter = 0;
		}
		else if (m_behaviorTree && !m_behaviorTree->IsBehaviorTreePaused()) {
			m_behaviorTree->Run(deltaTime);
			m_skipCounter++;
		}
	}
	UpdateAnimation();
	if(m_dieFadeOutTime.IsRunning() && m_dieFadeOutTime.HasElapsed()){
		m_isGarbage = true;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::RenderHealthBar() const{
	std::vector<Vertex_PCU> healthVerts;
	AABB2 healthBarBound(Vec2::ZERO, Vec2(100.f,
		10.f));

	healthBarBound.SetCenter(Vec2(m_position.x, m_position.y + m_renderScale.y * 120.f));
	float healthBackgroundWidth = healthBarBound.GetDimensions().x;
	float healthFillWidth = healthBackgroundWidth * (m_health / m_definition.m_health);
	AABB2 healthFillBound(healthBarBound.m_mins, Vec2(healthBarBound.m_mins.x + healthFillWidth,
		healthBarBound.m_maxs.y));

	AddVertsForAABB2D(healthVerts, healthBarBound, Rgba8::DARK_GREY);
	Rgba8 healthBarColor;
	if (m_faction == FACTION_GOOD) {
		healthBarColor = Rgba8::GREEN;
	}
	else if (m_faction == FACTION_EVIL) {
		healthBarColor = Rgba8::RED;
	}
	AddVertsForAABB2D(healthVerts, healthFillBound, healthBarColor);
	g_theRenderer->SetFillMode(FillMode::SOLID);
	g_theRenderer->SetBlendMode(BlendMode::ALPHA);
	g_theRenderer->BindShader(nullptr);
	g_theRenderer->BindTexture(nullptr);
	g_theRenderer->SetModelTint(Rgba8::WHITE);
	g_theRenderer->DrawVertexArray(static_cast<int>(healthVerts.size()), &healthVerts[0]);
}


//-----------------------------------------------------------------------------------------------
void Character::UpdateAnimation(){
	if (m_isDead) {
		PlayAnimation("pain");
	}
	else {
		if (m_attackAnimTime.IsRunning() && !m_attackAnimTime.HasElapsed()) {

		}
		else if (m_painAnimTime.IsRunning() && !m_painAnimTime.HasElapsed()) {
			// don't do anything - continue playing pain
		}
		else if (m_healAnimTime.IsRunning() && !m_healAnimTime.HasElapsed()) {
			// don't do anything - continue playing heal
		}
		else {
			PlayAnimation("idle");
		}
	}
}


//-----------------------------------------------------------------------------------------------
void Character::PlayAnimation(std::string animationName, float animationSpeed /*= 1.f*/){
	UNUSED(animationSpeed);
	if (animationName != m_currentAnimationName) {
		m_currentAnimationName = animationName;
		m_currentAnimation = m_definition.m_animations[animationName];
		m_animationClock.Reset();
	}

	if (animationName == "attack") {
		float attackAnimDuration = m_definition.m_animations[animationName]->GetDuration();
		m_attackAnimTime.Start(attackAnimDuration);
		m_animationClock.Reset();
	}
	else if (animationName == "pain") {
		float painAnimDuration = m_definition.m_animations[animationName]->GetDuration();
		m_painAnimTime.Start(painAnimDuration);
	}
	else if (animationName == "heal") {
		float healAnimDuration = m_definition.m_animations[animationName]->GetDuration();
		m_healAnimTime.Start(healAnimDuration);
		m_animationClock.Reset();
	}
}


//-----------------------------------------------------------------------------------------------
void Character::SetCharUID(CharacterUID uid, std::string unitName){
	m_uid = uid;
	m_name = unitName;
	if(!m_isANPC){
		m_behaviorTree = new BehaviorTree(m_definition.m_behaviorTree, m_name);
	}
	m_defaultArguments.SetValue("owner", m_name);
}



//-----------------------------------------------------------------------------------------------
void Character::ResetSpeed(){
	m_currentSpeed = m_defaultSpeed;
}


//-----------------------------------------------------------------------------------------------
std::string Character::GetFactionString() const{
	if(m_faction == FACTION_GOOD){
		return std::string("Good");
	}
	else{
		return std::string("Evil");
	}
}	


//-----------------------------------------------------------------------------------------------
std::string Character::GetStatsDescription() const{
	std::string dmgDealtString = std::to_string(m_stats.m_damageDealt);
	std::string dmgTakenString = std::to_string(m_stats.m_damageTaken);
	std::string dmgBlockedString = std::to_string(m_stats.m_damageBlocked);
	std::string dmgHealedString = std::to_string(m_stats.m_damageHealed);
	std::string allyHealingString = std::to_string(m_stats.m_allyHealing);
	std::string desciption = "Total Damage Dealt: " + dmgDealtString.substr(0, dmgDealtString.find('.'))
		+ "\n  Total Damage Taken: " + dmgTakenString.substr(0, dmgTakenString.find('.') )
		+ "\n  Total Damage Blocked: " + dmgBlockedString.substr(0, dmgBlockedString.find('.') )
		+ "\n  Total Damage Healed: " + dmgHealedString.substr(0, dmgHealedString.find('.'))
		+ "\n  Ally Healing: " + allyHealingString.substr(0, allyHealingString.find('.'));
	return desciption;
}


//-----------------------------------------------------------------------------------------------
float Character::GetHealthPercentage() const{
	return m_health * 100.f / m_definition.m_health;
}


//-----------------------------------------------------------------------------------------------
void Character::DebugRender() const {

}


//-----------------------------------------------------------------------------------------------
void Character::InitializeAPFACtor(){
	m_memoryActor = new APF_Lib::MM::GameActor(APF_Lib::MM::GameTime());
	m_memoryActor->SetName(m_name);
	if(m_isANPC){
		m_memorySystem = m_memoryActor->GetMemorySystem();
		m_memorySystem->Initialize();
		Personality currentPersonality = g_theGame->GetANPCPersonality();
		g_APF->AddANPC(m_name.c_str(), currentPersonality.m_openness,
			currentPersonality.m_conscientiousness,
			currentPersonality.m_extraversion,
			currentPersonality.m_agreeableness,
			currentPersonality.m_neuroticism);
		
		g_personalityActionConnection->m_personality.m_traits[0] = currentPersonality.m_openness;
		g_personalityActionConnection->m_personality.m_traits[1] = currentPersonality.m_conscientiousness;
		g_personalityActionConnection->m_personality.m_traits[2] = currentPersonality.m_extraversion;
		g_personalityActionConnection->m_personality.m_traits[3] = currentPersonality.m_agreeableness;
		g_personalityActionConnection->m_personality.m_traits[4] = currentPersonality.m_neuroticism;
	}
	else{
		g_APF->AddANPC(m_name.c_str(), 0.5f, 0.5f, 0.5f, 0.5f, 0.5f);
	}
}


//-----------------------------------------------------------------------------------------------
void Character::TriggerEvent(APF_Lib::MM::ActionInfo* info, APF_Lib::MM::GameObject* target){
	APF_Lib::MM::GameEvent event;
	event.m_actionInfo = info;
	event.m_target = target;
	event.m_isGarbage = false;
	event.m_action = info->apfIdx;
	event.m_actor = m_memoryActor->GetIndex();
	event.m_patient = target->GetIndex();
	event.m_patientType = APF_Lib::PATIENT_ANPC;
	event.m_certainty = 1.f;
	m_level->ProcessNewEvent(event);
}




//-----------------------------------------------------------------------------------------------
void Character::DetermineANPCAction(){
	std::vector<float> praises = g_APF->GetPraises(m_name.c_str());

	std::vector<float> actionProbs;
	float praiseTotal = 0.f;
	for (int praiseIndex = 0; praiseIndex < (int)praises.size(); praiseIndex++) {
		float newPraiseCount = SmoothStep3(praises[praiseIndex]);
		praiseTotal += (newPraiseCount);
	}
	actionProbs.push_back(100.f *(praises[0])/praiseTotal);
	for (int praiseIndex = 1; praiseIndex < (int)praises.size(); praiseIndex++) {
		actionProbs.push_back(actionProbs.back() + 100.f * (praises[praiseIndex]) / praiseTotal);
	}

	auto action = s_actions.begin();
	bool actionFound = false;
	int numOfTrys = 0;

	Valence inputPraise;
	inputPraise.m_praises = praises;
	int praiseScore = (int)inputPraise.GetValenceScore();
	while(!actionFound){
		numOfTrys++;
		float randomProb = Get2dNoiseZeroToOne(numOfTrys, praiseScore, (int)m_anpcActions.size()) * 100;
		int chosenActionIndex = -1;
		for (int probIndex = 0; probIndex < (int)actionProbs.size(); probIndex++) {
			if(randomProb <= actionProbs[probIndex]){
				chosenActionIndex = probIndex;
				break;
			}
		}

		action = s_actions.begin();
		while (chosenActionIndex > 0) {
			action++;
			chosenActionIndex--;
		}
		if((action->first == "Move forward" && GetMoveForwardIndex() == -1) ||
			(action->first == "Move backward" && GetMoveBackwardIndex() == -1)){
			
		}
		else if (m_level->IsCharacterInGreenZone(this) && (action->first == "Short attack"
			|| action->first == "Long attack" || action->first == "Range attack")) {
			
		}
		else{
			actionFound = true;
		}
	}
	ChooseActionByName(action->first);
	ChooseTargetByName(action->first);
	m_anpcActions.push_back(action->first);
}



//-----------------------------------------------------------------------------------------------
void Character::ChooseRandomAction(){
	std::vector<float> praises = g_APF->GetPraises(m_name.c_str());
	bool actionFound = false;
	int numOfTrys = 0;
	Valence inputPraise;
	inputPraise.m_praises = praises;
	int praiseScore = (int)inputPraise.GetValenceScore();

	auto action = s_actions.begin();
	float actionProb = 100.f / (float)praises.size();
	while (!actionFound) {
		numOfTrys++;
		float randomProb = Get2dNoiseZeroToOne(numOfTrys, praiseScore, (int)m_anpcActions.size()) * 100;
		int chosenActionIndex = (int) (randomProb / actionProb);
		chosenActionIndex = Clamp(chosenActionIndex, 0, (int)praises.size() - 1);

		action = s_actions.begin();
		while (chosenActionIndex > 0) {
			action++;
			chosenActionIndex--;
		}
		if ((action->first == "Move forward" && GetMoveForwardIndex() == -1) ||
			(action->first == "Move backward" && GetMoveBackwardIndex() == -1)) {

		}
		else if(m_level->IsCharacterInGreenZone(this) && (action->first == "Short Attack"
			|| action->first == "Long Attack" || action->first == "Range Attack")){
			
		}
		else {
			actionFound = true;
		}
	}

	ChooseActionByName(action->first);
	ChooseTargetByName(action->first);
	m_anpcActions.push_back(action->first);
}


//-----------------------------------------------------------------------------------------------
void Character::RecordStats(){
	m_allStats.push_back(m_stats);
}


//-----------------------------------------------------------------------------------------------
void Character::UpdatePraiseByPersonality(APF_Lib::MM::GameEvent const& event){
	std::vector<float> praises = g_APF->GetPraises(m_name.c_str());
	float trueValence = g_personalityActionConnection->GetTrueValenceData(event.m_actionInfo->actionName,
		praises);

	if(praises[event.m_actionInfo->apfIdx] >= trueValence * 0.9f &&
		praises[event.m_actionInfo->apfIdx] <= trueValence * 1.1f){
		return;
	}

	float valenceDifference = trueValence - praises[event.m_actionInfo->apfIdx];
	if(abs(valenceDifference) >= 0.001f){
		float valenceChange = Clamp(valenceDifference, APF_Lib::MIN_DELTA_PRAISE, 
			APF_Lib::MAX_DELTA_PRAISE);
		float newValence = praises[event.m_actionInfo->apfIdx] + valenceChange;
		newValence = Clamp(newValence, 0.f, 1.f);
		g_APF->OverwritePraise(m_memoryActor->GetIndex(), event.m_actionInfo->apfIdx, 
			newValence);
		m_totalValenceChange += abs(valenceChange);
	}
}


//-----------------------------------------------------------------------------------------------
void Character::UpdateImiediateActionPraiseByExpectation(APF_Lib::MM::GameEvent& event){
	if(m_currentExpectation.m_isEmpty){
		return;
	}
	APF_Lib::MentalState anpcMental;
	g_APF->GetANPCsMentalState(anpcMental, m_memoryActor->GetIndex());

	float mentalStatesChangedByResult[4] = {0.f, 0.f, 0.f, 0.f};
	CharacterStats statsChange = m_stats - m_allStats.back();
	float actualScore = 0.f;
	actualScore += statsChange.m_damageDealt;
	actualScore += statsChange.m_damageHealed;
	float expectationLevel = m_currentExpectation.m_actionScore / m_currentExpectation.m_idealScore;
	float actualResult = 0.f;
	if(m_currentExpectation.m_actionScore != 0.f){
		actualResult = actualScore / m_currentExpectation.m_actionScore;
	}
	else if(actualScore != 0.f){
		actualResult = 2.f;
	}


	if(actualResult < 2.2f && actualResult > 0.3f){
		UpdatePraiseByPersonality(event);
		return;
	}

	if(expectationLevel >= 0.7f){
		if(actualResult >= 0.7f){
			mentalStatesChangedByResult[0] = RangeMapClamped(actualResult, 0.7f, 1.3f, 0.6f, 1.f);
		}
		else{
			mentalStatesChangedByResult[3] = RangeMapClamped(actualResult, 0.0f, 0.7f, 0.6f, 1.f);
			
		}
	}
	else{
		if (actualResult >= 0.7f) {
			mentalStatesChangedByResult[2] = RangeMapClamped(actualResult, 0.7f, 1.3f, 0.6f, 1.f);
		}
		else {
			mentalStatesChangedByResult[1] = RangeMapClamped(actualResult, 0.0f, 0.7f, 0.6f, 1.f);
		}
	}
	// agreeableness: not used
	// conscientiousness: things confirmed: positive -> positive, negative -> negative
	// extraversion: positive result
	// openness/neuroticism: negative result
	float anpcPersonality[5];
	g_APF->GetANPCsPersonalities(anpcPersonality, m_memoryActor->GetIndex());
	anpcMental[APF_Lib::MENTAL_FEATURE_SATISFACTION] = mentalStatesChangedByResult[0] * 
		anpcPersonality[APF_Lib::PERSONALITY_CONSCIENTIOUSNESS] *
		anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION];

	anpcMental[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED] = mentalStatesChangedByResult[1] *
		anpcPersonality[APF_Lib::PERSONALITY_CONSCIENTIOUSNESS] *
		(anpcPersonality[APF_Lib::PERSONALITY_NEUROTICISM] * 
		(1.f - anpcPersonality[APF_Lib::PERSONALITY_OPENNESS]));

	anpcMental[APF_Lib::FEELING_FEATURE_RELIEF] = mentalStatesChangedByResult[2] *
		anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION];

	anpcMental[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT] = mentalStatesChangedByResult[3] *
		(anpcPersonality[APF_Lib::PERSONALITY_NEUROTICISM] *
			(1.f - anpcPersonality[APF_Lib::PERSONALITY_OPENNESS]));

	float valence = g_APF->GetPraise(m_memoryActor->GetIndex(), m_currentExpectation.m_actionIndex);
	valence += anpcMental[APF_Lib::MENTAL_FEATURE_SATISFACTION] * 0.1f + 
		anpcMental[APF_Lib::FEELING_FEATURE_RELIEF] * 0.05f -
		anpcMental[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED] * 0.1f -
		anpcMental[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT] * 0.05f;
	valence = Clamp(valence, 0.f, 1.f);
	g_APF->OverwritePraise(m_memoryActor->GetIndex(), m_currentExpectation.m_actionIndex, valence);
	g_APF->SetANPCsMentalState(m_memoryActor->GetIndex(), anpcMental);


	// get any difference from the ideal result and actual result
	// update it to any holding memories
	CharacterStats statesChangedByUnknownEvent;
	if(statsChange.m_damageDealt > 0.f){
		float damageReduced = GetAttackDamage() - statsChange.m_damageDealt;
		if (damageReduced > 0.f) {
			statesChangedByUnknownEvent.m_damageBlocked = damageReduced;
			(dynamic_cast<APF_Lib::MM::TBGShortTermMemory*>(m_memorySystem->GetSTM()))->
				UpdateHoldingMemory(statesChangedByUnknownEvent, event.m_patient);
		}
	}
}


//-----------------------------------------------------------------------------------------------
bool Character::UpdateContinuousActionPraiseByExpectation(APF_Lib::MM::MemoryAndEvent* memory){
	APF_Lib::MM::ContinuingMemory* actualMemory = 
		dynamic_cast<APF_Lib::MM::ContinuingMemory*>(memory);
	if(actualMemory->m_actionExpectation.m_actionIndex == -1){
		return false;
	}
	APF_Lib::MentalState anpcMental = actualMemory->memory.m_mentalState;

	float mentalStatesChangedByResult[4] = { 0.f, 0.f, 0.f, 0.f };

	float actualScore = 0.f;
	if(memory->selfEvent.m_actionInfo->actionName == "Move forward" ||
		memory->selfEvent.m_actionInfo->actionName == "Move backward"){
		CharacterStats statsChange = actualMemory->m_avgStateChange;
		actualScore += statsChange.m_damageTaken;
	}
	else if(memory->selfEvent.m_actionInfo->actionName == "Defense"){
		if(!actualMemory->m_effectFound){
			return false;
		}
		CharacterStats statsChange = actualMemory->m_statsChange;
		actualScore += statsChange.m_damageBlocked;
	}

	
	
	float expectationLevel = 0.f;
	float actualResult = 0.f;
	if(actualMemory->m_actionExpectation.m_actionScore == 0.f){
		expectationLevel = 1.f;
		actualResult = actualScore / actualMemory->m_actionExpectation.m_idealScore;
	}
	else{
		expectationLevel = actualMemory->m_actionExpectation.m_actionScore /
			actualMemory->m_actionExpectation.m_idealScore;
		actualResult = actualScore / actualMemory->m_actionExpectation.m_actionScore;
	}

	if (actualResult < 2.2f || actualResult > 0.3f) {
		UpdatePraiseByPersonality(memory->selfEvent);
		return true; 
	}

	if (expectationLevel >= 0.7f) {
		if (actualResult >= 0.7f) {
			mentalStatesChangedByResult[0] = RangeMapClamped(actualResult, 0.7f, 1.3f, 0.6f, 1.f);
		}
		else {
			mentalStatesChangedByResult[3] = RangeMapClamped(actualResult, 0.0f, 0.7f, 0.6f, 1.f);

		}
	}
	else {
		if (actualResult >= 0.7f) {
			mentalStatesChangedByResult[2] = RangeMapClamped(actualResult, 0.7f, 1.3f, 0.6f, 1.f);
		}
		else {
			mentalStatesChangedByResult[1] = RangeMapClamped(actualResult, 0.0f, 0.7f, 0.6f, 1.f);
		}
	}
	// agreeableness: not used
	// conscientiousness: things confirmed: positive -> positive, negative -> negative
	// extraversion: positive result
	// openness/neuroticism: negative result
	float anpcPersonality[5];
	g_APF->GetANPCsPersonalities(anpcPersonality, m_memoryActor->GetIndex());
	anpcMental[APF_Lib::MENTAL_FEATURE_SATISFACTION] = mentalStatesChangedByResult[0] *
		anpcPersonality[APF_Lib::PERSONALITY_CONSCIENTIOUSNESS] *
		anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION];
	anpcMental[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED] = mentalStatesChangedByResult[1] *
		anpcPersonality[APF_Lib::PERSONALITY_CONSCIENTIOUSNESS] *
		(anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION] *
			(1.f - anpcPersonality[APF_Lib::PERSONALITY_OPENNESS]));
	anpcMental[APF_Lib::FEELING_FEATURE_RELIEF] = mentalStatesChangedByResult[2] *
		anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION];
	anpcMental[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT] = mentalStatesChangedByResult[3] *
		(anpcPersonality[APF_Lib::PERSONALITY_EXTROVERSION] *
			(1.f - anpcPersonality[APF_Lib::PERSONALITY_OPENNESS]));

	float valence = g_APF->GetPraise(m_memoryActor->GetIndex(), actualMemory->m_actionExpectation.m_actionIndex);
	valence += anpcMental[APF_Lib::MENTAL_FEATURE_SATISFACTION] * 0.1f +
		anpcMental[APF_Lib::FEELING_FEATURE_RELIEF] * 0.05f -
		anpcMental[APF_Lib::MENTAL_FEATURE_FEARS_CONFIRMED] * 0.1f -
		anpcMental[APF_Lib::MENTAL_FEATURE_DISAPPOINTMENT] * 0.05f;
	valence = Clamp(valence, 0.f, 1.f);
	g_APF->OverwritePraise(m_memoryActor->GetIndex(), actualMemory->m_actionExpectation.m_actionIndex, valence);
	actualMemory->memory.m_mentalState = anpcMental;

	return true;
}


//-----------------------------------------------------------------------------------------------
void Character::UpdateMemory(APF_Lib::MM::GameEvent& event, CharacterStats statsChange){
	APF_Lib::MentalState anpcMental;
	g_APF->GetANPCsMentalState(anpcMental, m_memoryActor->GetIndex());
	if(event.m_actionInfo->actionName == "Move forward" || 
		event.m_actionInfo->actionName == "Move backward" ||
		event.m_actionInfo->actionName == "Defense"){
		APF_Lib::MM::ContinuingMemory* newMemory = new
			APF_Lib::MM::ContinuingMemory(m_level->m_totalTurnCount-1);
		newMemory->memory.m_mentalState = anpcMental;
		newMemory->selfEvent = event;
		newMemory->m_actionExpectation = m_currentExpectation;
		(dynamic_cast<APF_Lib::MM::TBGShortTermMemory*>(m_memorySystem->GetSTM()))->AddNewMemory(newMemory);
	}
	else{
		APF_Lib::MM::ImiediateMemory* newMemory = 
			new APF_Lib::MM::ImiediateMemory(m_level->m_totalTurnCount-1);
		newMemory->memory.m_mentalState = anpcMental;
		newMemory->selfEvent = event;
		newMemory->UpdateStatsChange(statsChange);
		(dynamic_cast<APF_Lib::MM::TBGShortTermMemory*>(m_memorySystem->GetSTM()))->AddNewMemory(newMemory);
	}
}


//-----------------------------------------------------------------------------------------------
void Character::ShortRangeNormalAttack(Character* target){
	if (!m_target) {
		m_isMyTurn = false;
	}
	if(g_theGame->m_stateClock.GetTimeDilation() >= 10.0){
		m_stats.m_damageDealt += target->TakeDamage(this, GetAttackDamage());
		TriggerEvent(s_actions["Short attack"], m_target->m_memoryActor);
		m_isMyTurn = false;
		m_chosenAction = nullptr;
		return;
	}
	if (!m_fadeOutTimer.IsRunning() && !m_actionTimer.IsRunning() && !m_fadeInTimer.IsRunning()) {
		m_fadeOutTimer.Start(0.25f);
		m_isActionEffectReady = true;
	}
	else if(m_fadeOutTimer.HasElapsed() && !m_actionTimer.IsRunning() && !m_fadeInTimer.IsRunning()){
		m_position = target->m_position;
		if(m_faction == FACTION_GOOD){
			m_position.x -= 120.f;
		}
		else if (m_faction == FACTION_EVIL) {
			m_position.x += 120.f;
		}
		m_fadeOutTimer.Stop();
		m_fadeInTimer.Start(0.25f);
	}
	else if(!m_fadeOutTimer.IsRunning() && !m_actionTimer.IsRunning() && m_fadeInTimer.HasElapsed()){
		m_fadeInTimer.Stop();
		m_actionTimer.Start(1.f);
		PlayAnimation("attack");
	}
	else if (m_actionTimer.GetElapsedFraction() < 0.6f && m_actionTimer.GetElapsedFraction() > 0.4f
		&& m_isActionEffectReady) {
		m_stats.m_damageDealt += target->TakeDamage(this, GetAttackDamage());
		TriggerEvent(s_actions["Short attack"], m_target->m_memoryActor);
		m_isActionEffectReady = false;
	}
	else if (!m_fadeOutTimer.IsRunning() && m_actionTimer.HasElapsed() && !m_fadeInTimer.IsRunning()) {
		m_fadeOutTimer.Start(0.25f);
	}
	else if (m_fadeOutTimer.HasElapsed() && m_actionTimer.HasElapsed() && !m_fadeInTimer.IsRunning()) {
		m_position = m_level->GetGridPositions(m_gridIndex, m_faction);
		m_fadeOutTimer.Stop();
		m_fadeInTimer.Start(0.25f);
	}
	else if (!m_fadeOutTimer.IsRunning() && m_actionTimer.HasElapsed() && m_fadeInTimer.HasElapsed()) {
		m_fadeInTimer.Stop();
		m_actionTimer.Stop();
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::LongRangeNormalAttack(Character* target){
	if(!m_target){
		m_isMyTurn = false;
	}
	if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
		int firstIndexInTargetRow = target->m_gridIndex -
			target->m_gridIndex % m_level->m_numOfGridRows;
		for (int charIndex = firstIndexInTargetRow; charIndex < firstIndexInTargetRow + 4; charIndex++) {
			Character* enemyCharacter = m_level->FindCharacterByIndex(charIndex,
				GetOppositeFaction());
			if (enemyCharacter) {
				m_stats.m_damageDealt += enemyCharacter->TakeDamage(this, GetRangeAttackDamage());
				TriggerEvent(s_actions["Range attack"], enemyCharacter->m_memoryActor);
			}

		}
		m_isMyTurn = false;
		m_chosenAction = nullptr;
		return;
	}

	if(!m_actionTimer.IsRunning()){
		m_isActionEffectReady = true;
		m_actionTimer.Start(1.f);
		PlayAnimation("attack");
	}
	else if (m_actionTimer.GetElapsedFraction() < 0.6f && m_actionTimer.GetElapsedFraction() > 0.4f
		&& m_isActionEffectReady) {
		int firstIndexInTargetRow = target->m_gridIndex - 
			target->m_gridIndex % m_level->m_numOfGridRows;
		for(int charIndex = firstIndexInTargetRow; charIndex < firstIndexInTargetRow + 4; charIndex++){
			Character* enemyCharacter = m_level->FindCharacterByIndex(charIndex, 
				GetOppositeFaction());
			if(enemyCharacter){
				m_stats.m_damageDealt += enemyCharacter->TakeDamage(this, GetRangeAttackDamage());
				TriggerEvent(s_actions["Range attack"], enemyCharacter->m_memoryActor);
			}
		}
		
		m_isActionEffectReady = false;
	}
	else if(m_actionTimer.HasElapsed()){
		m_actionTimer.Stop();
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::LongSingleNormalAttack(Character* target){
	if (!m_target) {
		m_isMyTurn = false;
	}
	if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
		m_stats.m_damageDealt += target->TakeDamage(this, GetLongAttackDamage());
		TriggerEvent(s_actions["Long attack"], m_target->m_memoryActor);
		m_isMyTurn = false;
		m_chosenAction = nullptr;
		return;
	}
	if (!m_actionTimer.IsRunning()) {
		m_isActionEffectReady = true;
		m_actionTimer.Start(1.f);
		PlayAnimation("attack");
	}
	else if (m_actionTimer.GetElapsedFraction() < 0.6f && m_actionTimer.GetElapsedFraction() > 0.4f
		&& m_isActionEffectReady) {
		m_stats.m_damageDealt += target->TakeDamage(this, GetLongAttackDamage());
		TriggerEvent(s_actions["Long attack"], m_target->m_memoryActor);
		m_isActionEffectReady = false;
	}
	else if (m_actionTimer.HasElapsed()) {
		m_actionTimer.Stop();
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::Defense(){
	if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
		TriggerEvent(s_actions["Defense"], m_memoryActor);
		m_blockDamageCounter += 1;
		m_isMyTurn = false;
		m_chosenAction = nullptr;
		return;
	}
	if(!m_actionTimer.IsRunning()){
		m_actionTimer.Start(1.f);
		PlayAnimation("attack");
	}
	else if (m_actionTimer.HasElapsed()) {
		TriggerEvent(s_actions["Defense"], m_memoryActor);
		m_actionTimer.Stop();
		m_blockDamageCounter += 1;
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
	
}


//-----------------------------------------------------------------------------------------------
void Character::Heal(Character* target){
	if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
		float healedValue = target->GetHealed(GetHealDamage());
		m_stats.m_damageHealed += healedValue;
		if (target != this) {
			m_stats.m_allyHealing += healedValue;
			TriggerEvent(s_actions["Heal"], m_target->m_memoryActor);
		}
		else {
			TriggerEvent(s_actions["Heal"], m_memoryActor);
		}
		m_isMyTurn = false;
		m_chosenAction = nullptr;
		return;
	}
	if (!m_actionTimer.IsRunning()) {
		m_actionTimer.Start(1.f);
		PlayAnimation("heal");
		m_isActionEffectReady = true;
	}
	else if (m_actionTimer.GetElapsedFraction() < 0.6f && m_actionTimer.GetElapsedFraction() > 0.4f
		&& m_isActionEffectReady) {
		float healedValue = target->GetHealed(GetHealDamage());
		m_stats.m_damageHealed += healedValue;
		if(target != this){
			m_stats.m_allyHealing += healedValue;
			TriggerEvent(s_actions["Heal"], m_target->m_memoryActor);
		}
		else{
			TriggerEvent(s_actions["Heal"], m_memoryActor);
		}
		m_isActionEffectReady = false;
	}
	else if (m_actionTimer.HasElapsed()) {
		m_actionTimer.Stop();
		m_blockDamageCounter += 1;
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::MoveForward(){
	int forwardIndex = GetMoveForwardIndex();
	if(forwardIndex  != -1){
		if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
			m_gridIndex = forwardIndex;
			m_position = m_level->GetGridPositions(m_gridIndex, m_faction);
			TriggerEvent(s_actions["Move forward"], m_memoryActor);
			m_isMyTurn = false;
			m_chosenAction = nullptr;
			return;
		}
		if (!m_fadeOutTimer.IsRunning() && !m_fadeInTimer.IsRunning()) {
			m_fadeOutTimer.Start(0.25f);
			m_isActionEffectReady = true;
		}
		else if (m_fadeOutTimer.HasElapsed() && !m_fadeInTimer.IsRunning() && m_isActionEffectReady) {
			m_gridIndex = forwardIndex;
			m_position = m_level->GetGridPositions(m_gridIndex, m_faction);
			m_isActionEffectReady = false;
			TriggerEvent(s_actions["Move forward"], m_memoryActor);
			m_fadeOutTimer.Stop();
			m_fadeInTimer.Start(0.25f);
		}
		else if (!m_fadeOutTimer.IsRunning()  && m_fadeInTimer.HasElapsed()) {
			m_fadeInTimer.Stop();
			m_isMyTurn = false;
			m_chosenAction = nullptr;
		}
	}
	else{
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::MoveBackward(){
	int backwardIndex = GetMoveBackwardIndex();
	if (backwardIndex != -1) {
		if (g_theGame->m_stateClock.GetTimeDilation() >= 10.0) {
			m_gridIndex = backwardIndex;
			m_position = m_level->GetGridPositions(m_gridIndex, m_faction);
			TriggerEvent(s_actions["Move backward"], m_memoryActor);
			m_isMyTurn = false;
			m_chosenAction = nullptr;
			return;
		}
		if (!m_fadeOutTimer.IsRunning() && !m_fadeInTimer.IsRunning()) {
			m_fadeOutTimer.Start(0.25f);
			m_isActionEffectReady = true;
		}
		else if (m_fadeOutTimer.HasElapsed() && !m_fadeInTimer.IsRunning() && m_isActionEffectReady) {
			m_gridIndex = backwardIndex;
			m_position = m_level->GetGridPositions(m_gridIndex, m_faction);
			m_isActionEffectReady = false;
			TriggerEvent(s_actions["Move backward"], m_memoryActor);
			m_fadeOutTimer.Stop();
			m_fadeInTimer.Start(0.25f);
		}
		else if (!m_fadeOutTimer.IsRunning() && m_fadeInTimer.HasElapsed()) {
			m_fadeInTimer.Stop();
			m_isMyTurn = false;
			m_chosenAction = nullptr;
		}
	}
	else {
		m_isMyTurn = false;
		m_chosenAction = nullptr;
	}
}


//-----------------------------------------------------------------------------------------------
float Character::TakeDamage(Character* killer, float damage){
	UNUSED(killer);
	if(m_blockDamageCounter > 0){
		m_blockDamageCounter--;
		damage *= (GetBlockingDamagePercentage()/ 100.f);
		m_stats.m_damageBlocked += damage;
	}
	float previousHealth = m_health;
	m_health -= damage;
	m_stats.m_damageTaken += damage;
	PlayAnimation("pain");
	if(m_health <= 0.f){
		//killer->TriggetKillEvent();
		Die();
		return damage;
	}
	return (previousHealth - m_health);
}


//-----------------------------------------------------------------------------------------------
void Character::Die(){
	if(m_isDead){
		return;
	}

	m_isDead = true;
	if(g_theGame->m_stateClock.GetTimeDilation() >= 10.0){
		m_isGarbage = true;
	}
	else{
		m_dieFadeOutTime.Start(2.f);
	}
}


//-----------------------------------------------------------------------------------------------
float Character::GetHealed(float damage){
	if (m_isDead || m_isGarbage) {
		return 0.f;
	}
	float previousHealth = m_health;
	m_health += damage;
	Clamp(m_health, 0.f, m_definition.m_health);
	return (m_health - previousHealth);
}


//-----------------------------------------------------------------------------------------------
Faction Character::GetOppositeFaction() const{
	return m_faction == FACTION_EVIL ? FACTION_GOOD : FACTION_EVIL;
}


//-----------------------------------------------------------------------------------------------
int Character::GetMoveForwardIndex() const{
	int numOfRows = m_level->m_numOfGridRows;
	int currentColumn = m_gridIndex / numOfRows;
	if (currentColumn == 0) {
		return -1 ;
	}
	else{
		if (m_level->FindCharacterByIndex(m_gridIndex-4, m_faction) == nullptr) {
			return m_gridIndex - 4;
		}
		int forwardedColumn = currentColumn - 1;
		for(int index = forwardedColumn * numOfRows; index < forwardedColumn * numOfRows + 4;
			index++){
			if (m_level->FindCharacterByIndex(index, m_faction) == nullptr){
				return index;
			}
		}
	}
	return -1;
}


//-----------------------------------------------------------------------------------------------
int Character::GetMoveBackwardIndex() const{
	int numOfRows = m_level->m_numOfGridRows;
	int currentColumn = m_gridIndex / numOfRows;
	int numOfLinesOnX = g_gameConfigBlackboard.GetValue("numOfLinesOnX", 0);
	int totalGrids = numOfLinesOnX * m_level->m_numOfGridRows;
	if((m_gridIndex + m_level->m_numOfGridRows) >= totalGrids){
		return -1;
	}
	else{
		if (m_level->FindCharacterByIndex(m_gridIndex + 4, m_faction) == nullptr) {
			return m_gridIndex + 4;
		}
		int backwardedColumn = currentColumn + 1;
		for (int index = backwardedColumn * numOfRows; index < backwardedColumn * numOfRows + 4;
			index++) {
			if (m_level->FindCharacterByIndex(index, m_faction) == nullptr) {
				return index;
			}
		}
	}
	return -1;
}


//------------------------------------------------------------------------------------------------
float Character::GetAttackDamage() const {
	if(g_theGame->m_gameSettingWindow){
		return m_definition.m_attackDamage * g_theGame->m_gameSettingWindow->m_attackDamage;
	}
	return m_definition.m_attackDamage;
}


//------------------------------------------------------------------------------------------------
float Character::GetRangeAttackDamage() const {
	if (g_theGame->m_gameSettingWindow) {
		return m_definition.m_rangeAttackDamage * g_theGame->m_gameSettingWindow->m_rangeAttackDamage;
	}
	return m_definition.m_rangeAttackDamage;
}


//------------------------------------------------------------------------------------------------
float Character::GetLongAttackDamage() const {
	if (g_theGame->m_gameSettingWindow) {
		return m_definition.m_longAttackDamage * g_theGame->m_gameSettingWindow->m_longAttackDamage;
	}
	return m_definition.m_longAttackDamage;
}


//------------------------------------------------------------------------------------------------
float Character::GetHealDamage() const {
	if (g_theGame->m_gameSettingWindow) {
		return m_definition.m_healDamage * g_theGame->m_gameSettingWindow->m_healDamage;
	}
	return m_definition.m_healDamage;
}


//------------------------------------------------------------------------------------------------
float Character::GetBlockingDamagePercentage() const {
	if (g_theGame->m_gameSettingWindow) {
		return g_theGame->m_gameSettingWindow->m_blockingDamagePercentage;
	}
	return m_definition.m_blockingDamagePercentage;
}


//-----------------------------------------------------------------------------------------------
void Character::RegisterBehaviorNodes(){
	DecoratorNode::SubscribeDecoratorEventFunction("IsHealthAbove", IsHealthAbove);
	ActionNode::SubscribeActionEventFunction("ShortAttack", ShortAttackAction);
	ActionNode::SubscribeActionEventFunction("LongAttack", LongAttackAction);
	ActionNode::SubscribeActionEventFunction("RangeAttack", RangeAttackAction);
	ActionNode::SubscribeActionEventFunction("Defense", DefenseAction);
	ActionNode::SubscribeActionEventFunction("Heal", HealAction);
	ActionNode::SubscribeActionEventFunction("MoveForward", MoveForwardAction);
	ActionNode::SubscribeActionEventFunction("MoveBackward", MoveBackwardAction);
	ActionNode::SubscribeActionEventFunction("FindRandomTarget", FindRandomTarget);
}


//-----------------------------------------------------------------------------------------------
bool Character::IsHealthAbove(EventArgs& args) {
	std::string charName = args.GetValue(std::string("owner"), "None");
	float health = args.GetValue(std::string("health"), 0.f);
	Level* level = g_theGame->m_currentLevel;
	if(!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)){
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		return character->m_health >= health;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::ShortAttackAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if(character->m_chosenAction == nullptr){
			character->m_chosenAction = Character::ShortAttackAction;
		}
		character->ShortRangeNormalAttack(character->m_target);
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::LongAttackAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::LongAttackAction;
		}
		character->LongSingleNormalAttack(character->m_target);
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::RangeAttackAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::RangeAttackAction;
		}
		character->LongRangeNormalAttack(character->m_target);
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::DefenseAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::DefenseAction;
		}
		character->Defense();
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::HealAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::HealAction;
		}
		character->Heal(character->m_target);
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::MoveForwardAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::MoveForwardAction;
		}
		character->MoveForward();
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::MoveBackwardAction(EventArgs& args){
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		if (character->m_chosenAction == nullptr) {
			character->m_chosenAction = Character::MoveBackwardAction;
		}
		character->MoveBackward();
		return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
bool Character::FindRandomTarget(EventArgs& args) {
	float chanceToTargetRedZoneTarget = 0.8f;
	std::string charName = args.GetValue(std::string("owner"), "None");
	Level* level = g_theGame->m_currentLevel;
	if (!level && (g_theGame->m_trainState == PSO_VALENCE || g_theGame->m_trainState == PSO_PERSONALITY)) {
		int charIndex = stoi(charName);
		int levelIndex = charIndex % 100;
		level = g_theGame->GetSimulatingLevel(levelIndex);
	}
	Character* character = level->FindCharacterByName(charName);
	if (character) {
		Faction oppositeFaction = character->GetOppositeFaction();
		int findTargetCounter = 0;
		
		while (findTargetCounter++ < 1000) {
			float targetProbability = Get1dNoiseZeroToOne(level->m_totalTurnCount);
			if(targetProbability < chanceToTargetRedZoneTarget){
				std::vector<Character*> redEnemies;
				for(Character* redChar : level->m_characters){
					if (redChar && redChar->m_faction == oppositeFaction &&
						!redChar->m_isDead && !redChar->m_isGarbage &&
						level->IsCharacterInRedZone(redChar)){
							redEnemies.push_back(redChar);
						}
				}
				if(redEnemies.size() > 0){
					float randomProbability = 
						Get1dNoiseZeroToOne(level->m_totalTurnCount, 1);
					float mappedProb = RangeMapClamped(randomProbability, 0.f, 1.f,
						0.f, (float)redEnemies.size());
					int randomIndex = (int)mappedProb;
					character->m_target = redEnemies[randomIndex];
					return true;
				}
			}
			else{
				float randomIndexFloat = Get1dNoiseZeroToOne(level->m_totalTurnCount, findTargetCounter);
				int randomIndex = (int)RangeMapClamped(randomIndexFloat, 0.f, 1.f, 0.f,
					(float)(level->m_characters.size() - 1));
				Character* enemyChar = level->m_characters[randomIndex];
				if (enemyChar && !level->IsCharacterInGreenZone(enemyChar) &&
					enemyChar->m_faction == oppositeFaction && !enemyChar->m_isDead
					&& !enemyChar->m_isGarbage) {
					character->m_target = enemyChar;
					return true;
				}
			}
			
		}
	}
	return false;
}


//-----------------------------------------------------------------------------------------------
void Character::ChooseActionByName(std::string actionName){
	if(actionName == "Short attack"){
		m_chosenAction = Character::ShortAttackAction;
	}
	else if(actionName == "Long attack") {
		m_chosenAction = Character::LongAttackAction;
	}
	else if (actionName == "Range attack") {
		m_chosenAction = Character::RangeAttackAction;
	}
	else if (actionName == "Defense") {
		m_chosenAction = Character::DefenseAction;
	}
	else if (actionName == "Heal") {
		m_chosenAction = Character::HealAction;
	}
	else if (actionName == "Move forward") {
		m_chosenAction = Character::MoveForwardAction;
	}
	else if (actionName == "Move backward") {
		m_chosenAction = Character::MoveBackwardAction;
	}
}


//-----------------------------------------------------------------------------------------------
void Character::ChooseTargetByName(std::string actionName){
	float highestScore = -1.f;
	if (actionName == "Short attack" || actionName == "Long attack" || 
		actionName == "Range attack") {
		Faction enemyFaction = GetOppositeFaction();
		int loopCount = 0;
		for(Character* character : m_level->m_characters){
			if(!character->m_isDead && !character->m_isGarbage && character->m_faction
				== enemyFaction){
				CharacterStats expection = EvaluateAction(actionName, character);
				float score = GetExpectionScore(expection, actionName);
				if(score > highestScore){
					highestScore = score;
					m_target = character;
					m_currentExpectation.m_statsChange = expection;
					m_currentExpectation.m_idealScore = GetIdealActionResult(actionName);
					m_currentExpectation.m_actionIndex = s_actions[actionName]->apfIdx;
					m_currentExpectation.m_actionScore = score;
				}
				else if(g_theGame->m_trainState == PSO_VALENCE) {
					float randomChance = Get1dNoiseZeroToOne(loopCount, m_level->m_totalTurnCount);
					if (randomChance >= 0.5f){
						m_target = character;
						m_currentExpectation.m_statsChange = expection;
						m_currentExpectation.m_idealScore = GetIdealActionResult(actionName);
						m_currentExpectation.m_actionIndex = s_actions[actionName]->apfIdx;
						m_currentExpectation.m_actionScore = score;
					}
				}
			}
			loopCount++;
		}
	}
	else if (actionName == "Heal") {
		int loopCount = 0;
		for (Character* character : m_level->m_characters) {
			if (!character->m_isDead && !character->m_isGarbage && character->m_faction
				== m_faction) {
				CharacterStats expection = EvaluateAction(actionName, character);
				float score = GetExpectionScore(expection, actionName);
				if (score > highestScore) {
					highestScore = score;
					m_target = character;
					m_currentExpectation.m_statsChange = expection;
					m_currentExpectation.m_idealScore = GetIdealActionResult(actionName);
					m_currentExpectation.m_actionIndex = s_actions[actionName]->apfIdx;
					m_currentExpectation.m_actionScore = score;
					m_currentExpectation.m_isEmpty = m_currentExpectationEmpty;
				}
				else if (g_theGame->m_trainState == PSO_VALENCE) {
					float randomChance = Get1dNoiseZeroToOne(loopCount, m_level->m_totalTurnCount);
					if (randomChance >= 0.5f) {
						m_target = character;
						m_currentExpectation.m_statsChange = expection;
						m_currentExpectation.m_idealScore = GetIdealActionResult(actionName);
						m_currentExpectation.m_actionIndex = s_actions[actionName]->apfIdx;
						m_currentExpectation.m_actionScore = score;
					}
				}
			}
			loopCount++;
		}
	}
	else{
		CharacterStats expection = EvaluateAction(actionName, this);
		m_currentExpectation.m_statsChange = expection;
		m_currentExpectation.m_idealScore = GetIdealActionResult(actionName);
		m_currentExpectation.m_actionIndex = s_actions[actionName]->apfIdx;
		m_currentExpectation.m_actionScore = GetExpectionScore(expection, actionName);
	}
}


//-----------------------------------------------------------------------------------------------
CharacterStats Character::EvaluateAction(std::string actionName, Character* target){
	if(g_theGame->m_trainState == PSO_VALENCE){
		CharacterStats emptyStats;
		return emptyStats;
	}
	APF_Lib::MM::MemorySystem* memSys = m_memoryActor->GetMemorySystem();
	
	int actionIndex = -1;
	for(auto iter = s_actions.begin(); iter != s_actions.end(); iter++){
		if(iter->first == actionName){
			actionIndex = iter->second->apfIdx;
			break;
		}
	}
	int targetIndex = target->m_memoryActor->GetIndex();
	CharacterStats expectation;
	
	bool result = (dynamic_cast<APF_Lib::MM::TBGLongTermMemory*>(memSys->GetLTM()))->
		GetExpectationOfAction(expectation, actionIndex, targetIndex);
	if(result){
		m_currentExpectationEmpty = false;
	}
	return expectation;
}


//-----------------------------------------------------------------------------------------------
float Character::GetIdealActionResult(std::string actionName){
	float score = 0.f;
	if (actionName == "Short attack") {
		score += m_definition.m_attackDamage;
	}
	else if (actionName == "Long attack") {
		score += m_definition.m_longAttackDamage;
	}
	else if (actionName == "Range attack") {
		score += m_definition.m_rangeAttackDamage;
	}
	else if (actionName == "Defense") {
		score += m_definition.m_attackDamage * 0.5f;
	}
	else if (actionName == "Heal") {
		score += m_definition.m_healDamage;
	}
	else if (actionName == "Move forward") {
		score += m_definition.m_attackDamage;
	}
	else if (actionName == "Move backward") {
		score -= m_definition.m_attackDamage;
	}
	return score;
}


//-----------------------------------------------------------------------------------------------
float Character::GetExpectionScore(CharacterStats expection, std::string actionName){
	float score = 0.f;
	if (actionName == "Defense" || actionName == "Move backward" || actionName == "Move forward") {
		score += expection.m_damageBlocked;
	}
	score += expection.m_damageDealt;
	score += expection.m_damageHealed;

	return score;
}

