#pragma once
#include "Game/Character.hpp"

//-----------------------------------------------------------------------------------------------
class Ranger : public Character
{
public:
	Ranger(Level* const& level, int gridIndex, Vec2 const& position, std::string unitDef,FacingDirection direction,
		Faction faction, bool isANPC, int speedVariance);
	~Ranger();
	static void RegisterRangerBehaviors();
	static bool DetermineWhichAttackToUse(EventArgs& args);
public:
};


