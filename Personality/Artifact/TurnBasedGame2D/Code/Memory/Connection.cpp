#include "Memory/Connection.hpp"
#include "Engine/Math/MathUtils.hpp"


//-----------------------------------------------------------------------------------------------
APF_Lib::ActionTraitConnection::ActionTraitConnection(std::string name, int index, float element, 
	float weight){
	m_actionName = name;
	m_actionIndex = index;
	m_influenceElement = element;
	m_weight = weight;
}


//-----------------------------------------------------------------------------------------------
float APF_Lib::ActionTraitConnection::GetTraitByValence(float valence) const{
	if(m_influenceElement >= 0.f){
		if(valence >= 0.5f){
			float trait = RangeMapClamped(valence, 0.5f, 1.f, m_influenceElement, 1.f);
			return trait * m_weight;
		}
		else{
			float trait = RangeMapClamped(valence, 0.0f, 0.5f, 0.f, m_influenceElement);
			return trait * m_weight;
		}
	}
	else{
		float absoluteElement = abs(m_influenceElement);
		if (valence >= 0.5f) {
			float trait = RangeMapClamped(valence, 0.5f, 1.f, absoluteElement, 0.f);
			return trait * m_weight;
		}
		else {
			float trait = RangeMapClamped(valence, 0.0f, 0.5f, 1.f, absoluteElement);
			return trait * m_weight;
		}
		
	}
}


//-----------------------------------------------------------------------------------------------
float APF_Lib::ActionTraitConnection::GetValenceByTrait(float traitValue) const{
	if (m_influenceElement >= 0.f) {
		if (traitValue >= m_influenceElement) {
			float valence = RangeMapClamped(traitValue, m_influenceElement, 1.f, 0.5f, 1.f);
			return valence;
		}
		else {
			float valence = RangeMapClamped(traitValue, 0.f, m_influenceElement, 0.f, 0.5f);
			return valence;
		}
	}
	else {
		float absoluteElement = abs(m_influenceElement);
		if (traitValue >= absoluteElement) {
			float valence = RangeMapClamped(traitValue, absoluteElement, 1.f, 0.5f, 0.f);
			return valence;
		}
		else {
			float valence = RangeMapClamped(traitValue, 0.f, absoluteElement, 1.f, 0.5f);
			return valence;
		}

	}
}


//-----------------------------------------------------------------------------------------------
APF_Lib::PersonalityActionConnection::PersonalityActionConnection(){
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::PersonalityActionConnection::AddConnection(std::string personalityTrait, 
	std::string actionName, int actionIndex, float element, float weight){
	ActionTraitConnection connection(actionName, actionIndex, element, weight);
	connection.m_trait = m_personality[GetTraitIndexByName(personalityTrait)];
	m_connections[personalityTrait].push_back(connection);
}


//-----------------------------------------------------------------------------------------------
float APF_Lib::PersonalityActionConnection::GetTrueValenceData(std::string actionName,
	std::vector<float> const& praises){
	std::vector<float> assumedValenceValues;
	for(auto iter = m_connections.begin(); iter != m_connections.end(); iter++){
		float traitValue = m_personality[GetTraitIndexByName(iter->first)];
		int actionConnnectionIndex = -1;

		for (int connectionIndex = 0; connectionIndex < (int)iter->second.size(); connectionIndex++) {
			if (actionName == iter->second[connectionIndex].m_actionName) {
				actionConnnectionIndex = connectionIndex;
			}
		}
		if(actionConnnectionIndex == -1){
			continue;
		}

		for(int connectionIndex = 0; connectionIndex < (int)iter->second.size(); connectionIndex++){
			if(actionName != iter->second[connectionIndex].m_actionName){
				traitValue -= iter->second[connectionIndex].GetTraitByValence(
					praises[iter->second[connectionIndex].m_actionIndex]);
			}
		}

		if(traitValue > 0.f && actionConnnectionIndex != -1){
			float traitForthisValence = traitValue / iter->second[actionConnnectionIndex].m_weight;
			traitForthisValence = Clamp(traitForthisValence, 0.f, 1.f);
			float trueValenceForThisTrait = iter->second[actionConnnectionIndex].GetValenceByTrait(traitForthisValence);
			assumedValenceValues.push_back(trueValenceForThisTrait);
		}
		else if(traitValue < 0.f){
			assumedValenceValues.push_back(0.f);
		}
	}

	float totalValenceValues = 0.f;
	for(float trueValence : assumedValenceValues){
		totalValenceValues += trueValence;
	}
	return totalValenceValues / (float)assumedValenceValues.size();
}


//-----------------------------------------------------------------------------------------------
int APF_Lib::PersonalityActionConnection::GetTraitIndexByName(std::string traitName){
	if(traitName == "Openness"){
		return 0;
	}
	else if (traitName == "Conscientiousness") {
		return 1;
	}
	else if (traitName == "Extraversion") {
		return 2;
	}
	else if (traitName == "Agreeableness") {
		return 3;
	}
	else if (traitName == "Neuroticism") {
		return 4;
	}
	return -1;
}


//-----------------------------------------------------------------------------------------------
APF_Lib::TraitPersonality APF_Lib::PersonalityActionConnection::GetIdealPersonality(std::vector<float> 
	const& praises){
	TraitPersonality idealPersonality;
	for (auto iter = m_connections.begin(); iter != m_connections.end(); iter++) {
		float trait = 0.f;
		for (int connectionIndex = 0; connectionIndex < (int)iter->second.size(); connectionIndex++) {
			trait += iter->second[connectionIndex].GetTraitByValence(
				praises[iter->second[connectionIndex].m_actionIndex]);
		}
		if (iter->first == "Openness") {
			idealPersonality[0] = trait;
		}
		else if (iter->first == "Conscientiousness") {
			idealPersonality[1] = trait;
		}
		else if (iter->first == "Extraversion") {
			idealPersonality[2] = trait;
		}
		else if (iter->first == "Agreeableness") {
			idealPersonality[3] = trait;
		}
		else if (iter->first == "Neuroticism") {
			idealPersonality[4] = trait;
		}
	}
	return idealPersonality;
}
