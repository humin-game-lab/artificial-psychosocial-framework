#include "Memory/ShortTermMemory.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::TBGShortTermMemory::TBGShortTermMemory(MemorySystem* memSys):ShortTermMemory(memSys){

}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::TBGShortTermMemory::~TBGShortTermMemory(){
	for (MemoryAndEvent* memory : m_memories) {
		delete memory;
	}
	m_memories.clear();
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGShortTermMemory::AddNewMemory(MemoryAndEvent* newMem){
	m_memories.push_back(newMem);
	if((int)m_memories.size() > MAX_MEMORY_CAPACITY){
		ConsolidateOldestMemoryToLTM();
	}
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGShortTermMemory::UpdateHoldingMemory(CharacterStats statsChange,
	int patientIndex){
	for(MemoryAndEvent* memory: m_memories){
		ContinuingMemory* continueMem = dynamic_cast<ContinuingMemory*>(memory);
		if(continueMem && continueMem->m_onHold && continueMem->selfEvent.m_patient == patientIndex){
			continueMem->UpdateStatsChange(statsChange);
			break;
		}
	}
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGShortTermMemory::GetRelatedMemory(std::vector<MemoryAndEvent*>& memories,
	int actionIndex, std::string targetRole){
	Level* currentLevel = g_theGame->GetSimulatingLevel();
	for (MemoryAndEvent* memory : m_memories) {
		if(dynamic_cast<ContinuingMemory*>(memory)){
			continue;
		}
		if (memory->selfEvent.m_action == actionIndex) {
			std::string patientRole =
				currentLevel->GetCharacterRoleByIndex(memory->selfEvent.m_patient);
			if (patientRole == targetRole) {
				memories.push_back(memory);
			}
		}
	}
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGShortTermMemory::ConsolidateOldestMemoryToLTM(){
	MemoryAndEvent* oldestMemory = m_memories[0];
	ContinuingMemory* continueMem = dynamic_cast<ContinuingMemory*>(oldestMemory);
	if(continueMem){
		continueMem->CalculateStatsChange();
		Level* currentLevel = g_theGame->GetSimulatingLevel();
		bool updateResult = currentLevel->UpdateContinuousAction(continueMem);
		if(!updateResult){
			m_memories.erase(m_memories.begin());
			return;
		}
	}
	m_memories.erase(m_memories.begin());
	(dynamic_cast<TBGLongTermMemory*>(m_LTM))->AddNewMemory(oldestMemory);
}

