#pragma once

#include "ThirdParty/APF/Memory/MemoryClock.hpp"
#include "ThirdParty/APF/Memory/GameState.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"
#include "ThirdParty/APF/Memory/ShortTermMemory.hpp"
#include "ThirdParty/APF/APF/MentalState.hpp"
#include "Game/Character.hpp"

namespace APF_Lib
{
	namespace MM
	{
		constexpr int CONTINUING_MEMORY_HOLD_DURATION = 9;
		struct ImiediateMemory: public MemoryAndEvent {
			public:
				ImiediateMemory(int currentTurn);
				~ImiediateMemory();
				ImiediateMemory& operator=(ImiediateMemory const& source);
				void UpdateStatsChange(CharacterStats stateChange);


			public:
				CharacterStats m_statsChange;
				int m_startTurn = -1;
		};

		struct ContinuingMemory : public MemoryAndEvent {
		public:
			ContinuingMemory(int currentTurn);
			~ContinuingMemory();
			ContinuingMemory& operator=(ContinuingMemory const& source);
			void UpdateStatsChange(CharacterStats stateChange);
			void CalculateStatsChange();


		public:
			CharacterStats m_statsChange;
			CharacterStats m_avgStateChange;
			Expectation m_actionExpectation;
			int m_startTurn = -1;
			bool m_onHold = false;
			bool m_effectFound = false;
		};

	}
}
