#include "Memory/LongTermMemory.hpp"
#include "Memory/Memory.hpp"
#include "Memory/ShortTermMemory.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::TBGLongTermMemory::TBGLongTermMemory(MemorySystem* sys):LongTermMemory(sys){

}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::TBGLongTermMemory::~TBGLongTermMemory(){
	for(MemoryAndEvent* memory: m_memories){
		delete memory;
	}
	m_memories.clear();
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGLongTermMemory::AddNewMemory(MemoryAndEvent* newMem){
	m_memories.push_back(newMem);
}


//-----------------------------------------------------------------------------------------------
bool APF_Lib::MM::TBGLongTermMemory::GetExpectationOfAction(CharacterStats& expectation, 
	int actionIndex, int patientIndex){
	Level* currentLevel = g_theGame->GetSimulatingLevel();
	std::string patientRole = currentLevel->GetCharacterRoleByIndex(patientIndex);
	std::vector<MemoryAndEvent*> relatedMemories = GetRelatedMemory(actionIndex, patientRole);
	if((int)relatedMemories.size() == 0){
		(dynamic_cast<TBGShortTermMemory*>(m_STM))->GetRelatedMemory(relatedMemories,
			actionIndex, patientRole);
		if((int)relatedMemories.size() == 0){
			return false;
		}
	}
	std::vector<float> memoryWeights = { 0.1f };
	for (int index = 1; index < (int)relatedMemories.size(); index++) {
		memoryWeights.push_back(memoryWeights.back() * 2.f);
	}
	UpdateWeightsByMemoryType(relatedMemories, memoryWeights, patientIndex);
	float totalWeights = 0.f;
	for(float weight: memoryWeights){
		totalWeights += weight;
	}

	for(int memIndex = 0; memIndex < (int)relatedMemories.size(); memIndex++){
		
		ImiediateMemory* imediateMem = dynamic_cast<ImiediateMemory*>(relatedMemories[memIndex]);
		ContinuingMemory* continueMem = dynamic_cast<ContinuingMemory*>(relatedMemories[memIndex]);
		if(imediateMem){
			CharacterStats statsPortion = imediateMem->m_statsChange;
			statsPortion *= memoryWeights[memIndex] / totalWeights;
			expectation += statsPortion;
		}
		else if(continueMem){
			CharacterStats statsPortion = continueMem->m_statsChange;
			statsPortion *= memoryWeights[memIndex] / totalWeights;
			expectation += statsPortion;
		}
	}
	return true;
	
}


//-----------------------------------------------------------------------------------------------
std::vector<APF_Lib::MM::MemoryAndEvent*> APF_Lib::MM::TBGLongTermMemory::GetRelatedMemory(int actionIndex,
	std::string targetRole){
	Level* currentLevel = g_theGame->GetSimulatingLevel();
	std::vector<MemoryAndEvent*> relatedMemories;
	for(int memIndex = 0; memIndex < (int)m_memories.size(); memIndex++){
		if(m_memories[memIndex]->selfEvent.m_action == actionIndex){
			std::string patientRole = 
				currentLevel->GetCharacterRoleByIndex(m_memories[memIndex]->selfEvent.m_patient);
			if(patientRole == targetRole){
				relatedMemories.push_back(m_memories[memIndex]);
			}
		}
	}
	return relatedMemories;
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::TBGLongTermMemory::UpdateWeightsByMemoryType(std::vector<MemoryAndEvent*> const& memories,
	std::vector<float>& weights, int patientIndex){
	int ownerIndex = m_memSystem->GetOwnerIndex();
	for(int memIndex = 0; memIndex < (int)memories.size(); memIndex++){
		if(memories[memIndex]->selfEvent.m_actor == ownerIndex){
			weights[memIndex] *= 5.f;
		}
		else if(memories[memIndex]->selfEvent.m_patient == patientIndex){
			weights[memIndex] *= 1.5f;
		}
	}
}
