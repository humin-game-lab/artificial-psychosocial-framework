#pragma once

#include "ThirdParty/APF/APF/MentalState.hpp"
#include "ThirdParty/APF/APF/Trait.hpp"
#include <vector>
#include <map>

namespace APF_Lib
{
	constexpr float MAX_DELTA_PRAISE = 0.1f;
	constexpr float MIN_DELTA_PRAISE = -0.1f;
	struct ActionTraitConnection {
		public:
			ActionTraitConnection(std::string name, int index, float element, float weight);
			float GetTraitByValence(float valence) const;
			float GetValenceByTrait(float traitValue) const;
		public:
			std::string m_actionName;
			int m_actionIndex;
			float m_trait = 0.f;
			float m_influenceElement = 0.f;
			float m_weight = 0.f;
	};

	struct PersonalityActionConnection {
		public:
			PersonalityActionConnection();
			void AddConnection(std::string personalityTrait, std::string actionName, int actionIndex, float element, float weight);
			float GetTrueValenceData(std::string actionName, std::vector<float> const& praises);
			int GetTraitIndexByName(std::string traitName);
			TraitPersonality GetIdealPersonality(std::vector<float> const& praises);
		public:
			std::map<std::string, std::vector<ActionTraitConnection>> m_connections;
			TraitPersonality m_personality;
	};
}
