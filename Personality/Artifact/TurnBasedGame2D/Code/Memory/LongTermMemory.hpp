#pragma once

#include "ThirdParty/APF/Memory/MemoryClock.hpp"
#include "ThirdParty/APF/Memory/GameState.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"
#include "ThirdParty/APF/Memory/LongTermMemory.hpp"
#include "ThirdParty/APF/APF/MentalState.hpp"
#include "Game/Character.hpp"
#include "Memory/Memory.hpp"

namespace APF_Lib
{
	namespace MM
	{
		class TBGLongTermMemory: public LongTermMemory {
			public:
				TBGLongTermMemory(MemorySystem* sys);
				~TBGLongTermMemory();
				void SetShortTermMemory(ShortTermMemory* stm) { m_STM = stm; }
				void AddNewMemory(MemoryAndEvent * newMem);
				bool GetExpectationOfAction(CharacterStats& expectation, int actionIndex, int patientIndex);

			public:
				ShortTermMemory* m_STM = nullptr;

			protected:
				std::vector<MemoryAndEvent*> GetRelatedMemory(int actionIndex, std::string targetRole);
				void UpdateWeightsByMemoryType(std::vector<MemoryAndEvent*>const& memories, std::vector<float> & weights
					, int patientIndex);
			protected:
				std::vector<MemoryAndEvent*> m_memories;


		};


	}
}
