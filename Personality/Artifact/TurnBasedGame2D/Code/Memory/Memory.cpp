#include "Memory/Memory.hpp"
#include "Game/GameCommon.hpp"
#include "Game/Game.hpp"
#include "Game/Level.hpp"


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ImiediateMemory::ImiediateMemory(int currentTurn):m_startTurn(currentTurn){

}

//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ImiediateMemory::~ImiediateMemory()
{

}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ImiediateMemory& APF_Lib::MM::ImiediateMemory::operator=(ImiediateMemory const& source){
	memory = source.memory;
	selfEvent = source.selfEvent;
	repeatedTimes = source.repeatedTimes;
	m_statsChange = source.m_statsChange;
	m_startTurn = source.m_startTurn;
	return *this;
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::ImiediateMemory::UpdateStatsChange(CharacterStats stateChange){
	m_statsChange = stateChange;
}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ContinuingMemory::ContinuingMemory(int currentTurn){
	m_startTurn = currentTurn;
	m_onHold = true;
}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ContinuingMemory::~ContinuingMemory(){

}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::ContinuingMemory::UpdateStatsChange(CharacterStats stateChange){
	m_statsChange += stateChange;
	m_effectFound = true;
}


//-----------------------------------------------------------------------------------------------
void APF_Lib::MM::ContinuingMemory::CalculateStatsChange(){
	if(m_startTurn <= (CONTINUING_MEMORY_HOLD_DURATION + 1)){
		return;
	}
	Level* currentLevel = g_theGame->GetSimulatingLevel();
	CharacterStats beforeStatsChange = currentLevel->GetStatsChange(m_startTurn - 1,
		m_startTurn - (CONTINUING_MEMORY_HOLD_DURATION + 1), selfEvent.m_patient);
	CharacterStats afterStatsChange = currentLevel->GetStatsChange(m_startTurn +
		CONTINUING_MEMORY_HOLD_DURATION, m_startTurn, selfEvent.m_patient);
	m_avgStateChange = afterStatsChange - beforeStatsChange;
}


//-----------------------------------------------------------------------------------------------
APF_Lib::MM::ContinuingMemory& APF_Lib::MM::ContinuingMemory::operator=(ContinuingMemory const& source){
	memory = source.memory;
	selfEvent = source.selfEvent;
	repeatedTimes = source.repeatedTimes;
	m_statsChange = source.m_statsChange;
	m_avgStateChange = source.m_avgStateChange;
	m_startTurn = source.m_startTurn;
	m_onHold = source.m_onHold;
	return *this;
}
