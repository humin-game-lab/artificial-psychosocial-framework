#pragma once

#include "ThirdParty/APF/Memory/MemoryClock.hpp"
#include "ThirdParty/APF/Memory/GameState.hpp"
#include "ThirdParty/APF/Memory/MemCommon.hpp"
#include "ThirdParty/APF/Memory/ShortTermMemory.hpp"
#include "ThirdParty/APF/APF/MentalState.hpp"
#include "Memory/LongTermMemory.hpp"
#include "Game/Character.hpp"

namespace APF_Lib
{
	namespace MM
	{
		constexpr int MAX_MEMORY_CAPACITY = 10;
		class TBGShortTermMemory: public ShortTermMemory {
			public:
				TBGShortTermMemory(MemorySystem* memSys);
				~TBGShortTermMemory();
				void SetLongTermMemory(LongTermMemory* ltm){m_LTM = ltm;}
				void AddNewMemory(MemoryAndEvent* newMem);
				void UpdateHoldingMemory(CharacterStats statsChange, int patientIndex);
				void GetRelatedMemory(std::vector<MemoryAndEvent*>& memories, int actionIndex, std::string targetRole);
			public:
				LongTermMemory* m_LTM = nullptr;

			protected:
				void ConsolidateOldestMemoryToLTM();
			protected:
				std::vector<MemoryAndEvent*> m_memories;
		};

	}
}
