import fall_23.scripts.TraitBasedPersonalityTest as tbpt
import fall_23.scripts.PersonalityTestResponses as ptr
import fall_23.scripts.notebook_commons as nc
from fall_23.scripts.Centroids import Centroids
import fall_23.scripts.personality_tools as pt
import fall_23.scripts.LangchainInterface as LCI
import fall_23.scripts.LangchainLlm as LCL
import fall_23.scripts.SyntheticPopulation as SP

import random
import time
import numpy as np
import pandas as pd
import os

import holoviews as hv
from holoviews import opts
from sctriangulate.colors import build_custom_continuous_cmap
hv.extension('matplotlib')
hv.renderer('matplotlib')
from sklearn.model_selection import train_test_split

from IPython.display import display_svg
renderer = hv.plotting.mpl.MPLRenderer.instance(dpi=120)

# Code to test LLM model
# ------------------------------
test_llm_model = False
if test_llm_model:
    llm_implementation = LCI.LC_OpenAi()
    llm_model = LCL.OPENAI_GPT_3_5(llm_implementation)
    llm_model.template = "Question: {question} Answer: Let's think step by step."
    llm_model.prompt = ['question']
    print(llm_model.run("How many 2 Dollar bills can you make from a 100 Yuan bill?"))


# Code to test wrapper classes for response data
# ------------------------------
test_personality_stats = False
if test_personality_stats:
    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_NEO_60
    implementation_ipipNeo60= tbpt.IpipNeo60()
    profile_centroids = Centroids()

    responses_ipipneo60 = ptr.PersonalityFactorResponseSet(implementation_ipipNeo60)
    loaded_key = responses_ipipneo60.init_test_key(nc.IPIP_NEO_60_TEST_KEY_DIR)

    try:
        responses_ipipneo60.load(nc.IPIP_NEO_60_QUICKLOAD_DIR, "test_set")
    except ValueError:
        print("Cannot locate quickload. Loading original file")
        df_responses_ipipNeo60 = responses_ipipneo60.get_dataframe(responses=True, evaluation=False, cscf=False, labels=None, columns=np.array([1, 91, 6, 126, 11, 41, 76, 16, 171, 231, 176, 296, 2, 92, 7, 247, 12, 42, 17, 47, 22, 52, 57, 147, 3, 63, 8, 158, 13, 193, 138, 198, 203, 263, 28, 118, 4, 34, 159, 249, 74, 104, 229, 259, 144, 174, 29, 59, 65, 155, 40, 190, 105, 195, 50, 170, 145, 265, 150, 270], dtype=np.uint16))
        responses_ipipneo60.init_raw_responses_pd(df_responses_ipipNeo60)
        responses_ipipneo60.eval_test_responses()
        responses_ipipneo60.eval_centroid_similarity(profile_centroids)
        responses_ipipneo60.save(nc.IPIP_NEO_60_QUICKLOAD_DIR, "test_set")

    #correlations_matrix  = responses_ipipneo60.get_dataframe(responses=True, evaluation=False, cscf=False, labels=None, columns=np.array([24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35])).corr()

    #responses_ipipneo60.get_dataframe(responses=True, evaluation=False, cscf=False, labels=None).to_csv(nc.IPIP_NEO_60_TEST_RESPONSES_DIR)

    print_reliability = False
    if print_reliability == True:
        print("Cronbach Alpha -------------------------")
        print(responses_ipipneo60.cronbach_alpha_breakdown())

        print("Guttman's Lambda-6 -------------------------")
        print(responses_ipipneo60.lambda_6_breakdown())

        print("McDonalds Hierarchical Omega -------------------------")
        print(responses_ipipneo60.hierarchical_omega_breakdown())




    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_50
    implementation_ipip50= tbpt.Ipip50()
    profile_centroids = Centroids()

    # Load test key and test responses
    responses_ipip50 = ptr.PersonalityFactorResponseSet(implementation_ipip50)
    loaded_key = responses_ipip50.init_test_key(nc.IPIP_50_TEST_KEY_DIR)

    try:
        responses_ipip50.load(nc.IPIP_50_QUICKLOAD_DIR, "test_set")
    except ValueError:
        print("Cannot locate quickload. Loading original file")
        df_test_response = pd.read_csv(
            r"C:\Users\38977332\OneDrive - Southern Methodist University\Documents\School\APF\Repos\apf\LLM_Personality\fall_23\resources\datasets\ipip_50\data-final-commas.csv",
            delimiter=',', header=0)

        df_test_response.dropna(inplace=True)

        for col_name in df_test_response.columns:
            if col_name[-1].isdigit():  # Checks the question is answered correctly
                index_incorect_answer = df_test_response[(df_test_response[col_name] == 0)].index
                df_test_response.drop(index_incorect_answer, inplace=True)
            elif col_name.endswith("_E"):  # Checks the question response time
                index_fast_react = df_test_response[(df_test_response[col_name] <= 1000)].index
                df_test_response.drop(index_fast_react, inplace=True)
            elif col_name == "IPC":  # IPC counts the number of times this IP adress was recorded
                index_multiple_responses = df_test_response[(df_test_response[col_name] != 1)].index
                df_test_response.drop(index_multiple_responses, inplace=True)

        responses_ipip50.init_raw_responses_pd(df_test_response)
        responses_ipip50.eval_test_responses()
        responses_ipip50.eval_centroid_similarity(profile_centroids)
        responses_ipip50.save(nc.IPIP_50_QUICKLOAD_DIR, "test_set")

    if print_reliability == True:
        print("Cronbach Alpha -------------------------")
        print(responses_ipip50.cronbach_alpha_breakdown())

        print("Guttman's Lambda-6 -------------------------")
        print(responses_ipip50.lambda_6_breakdown())

        print("McDonalds Hierarchical Omega -------------------------")
        print(responses_ipip50.hierarchical_omega_breakdown())


    responses_ipipneo60_eval_df = responses_ipipneo60.get_dataframe(responses = False, evaluation = True , cscf = False, labels=None, columns=None)
    responses_ipip50_eval_df = responses_ipip50.get_dataframe(responses = False, evaluation = True , cscf = False, labels=None, columns=None)


    print("pearson product moment correlation -------------------------")
    product_moment_correlation_matrix = pt.calculate_pearson_product_moment_correlation(responses_ipip50_eval_df.to_numpy(), responses_ipip50_eval_df.to_numpy())
    print(product_moment_correlation_matrix)


# Code to test AWS Bedrock
# ------------------------------
test_bedrock = False
if test_bedrock:
    import json
    import os
    import sys

    import boto3
    import botocore

    model_path = ".."
    sys.path.append(os.path.abspath(model_path))
    from utils import bedrock, print_ww


    bedrock_runtime = boto3.client(
        service_name = "bedrock-runtime",
        aws_access_key_id = os.getenv("AWS_ACCESS_KEY_ID"),
        aws_secret_access_key = os.getenv("AWS_SECRET_ACCESS_KEY"),
        region_name = os.getenv("AWS_DEFAULT_REGION")
    )


    scenario_description = "You are about to take a personality test"

    request_body = json.dumps({
        "inputText": scenario_description,
        "textGenerationConfig":{
            "maxTokenCount":4096,
            "stopSequences":[],
            "temperature":0.5,
            "topP":0.9
            }
        })

    modelID = "amazon.titan-text-express-v1"
    accept = "application/json"
    contentType = "application/json"
    generatedText = "\n"

    model_response = bedrock_runtime.invoke_model(body=request_body, modelId=modelID, accept=accept, contentType=contentType)
    response_contents = json.loads(model_response.get('body').read())

    generatedText = response_contents.get('results')[0].get('outputText')

    email_content = generatedText[generatedText.index('\n')+1:]
    print_ww(email_content)



# Code to generate synthetic populations given an LLM
# --------------------------
test_synthetic_population_generation = False
if test_synthetic_population_generation:
    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_NEO_300
    personality_test_directory_name = "population_aws_claud_old"
    profile_centroids = Centroids()

    llm_company_name = nc.LLM_COMPANY.AWS
    llm_model_name = nc.LLM_MODEL.AWS_CLAUDE
    multiples = 10
    num_questions_per_batch = 50

    key_directory = None
    test_response_directory = None
    quick_load_directory = None
    implementation_personality_test = None

    match PRIMARY_PERSONALITY_TEST:
        case nc.PERSONALITY_TEST.IPIP_NEO_60:
            key_directory = nc.IPIP_NEO_60_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_60_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_60_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo60()

        case nc.PERSONALITY_TEST.IPIP_NEO_120:
            key_directory = nc.IPIP_NEO_120_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_120_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_120_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo120()

        case nc.PERSONALITY_TEST.IPIP_NEO_300:
            key_directory = nc.IPIP_NEO_300_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_300_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_300_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo300()

        case nc.PERSONALITY_TEST.IPIP_50:
            key_directory = nc.IPIP_50_TEST_KEY_DIR
            test_response_directory = nc.IPIP_50_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_50_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Ipip50()

        case nc.PERSONALITY_TEST.PANAS_20:
            key_directory = nc.PANAS_20_TEST_KEY_DIR
            test_response_directory = nc.PANAS_20_TEST_RESPONSES_DIR
            quick_load_directory = nc.PANAS_20_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Panas20()

        case nc.PERSONALITY_TEST.BPAQ_29:
            key_directory = nc.BPAQ_29_TEST_KEY_DIR
            test_response_directory = nc.BPAQ_29_TEST_RESPONSES_DIR
            quick_load_directory = nc.BPAQ_29_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Bpaq29()

        case nc.PERSONALITY_TEST.SSCS_11:
            key_directory = nc.SSCS_11_TEST_KEY_DIR
            test_response_directory = nc.SSCS_11_TEST_RESPONSES_DIR
            quick_load_directory = nc.SSCS_11_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Sscs11()

        case nc.PERSONALITY_TEST.PVQ_RR_57:
            key_directory = nc.PVQRR_57_TEST_KEY_DIR
            test_response_directory = nc.PVQRR_57_TEST_RESPONSES_DIR
            quick_load_directory = nc.PVQRR_57_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Pvqrr57()

        case _:
            raise Exception("Could not find personality test. Please check PRIMARY_PERSONALITY_TEST.")

    # Load test key and test responses
    responses_set = ptr.PersonalityFactorResponseSet(implementation_personality_test)
    keys_loaded = responses_set.init_test_key(key_directory)

    synthetic_population = SP.SyntheticPopulation()
    synthetic_population.SetPopulationPrompts(personality_profile_centers=profile_centroids.GetCenterList(), replicas=multiples, question_items=responses_set.get_item_list(), likert_question=responses_set.implementation.get_likert_question(), likert_responses=responses_set.implementation.get_likert_responses())
    response_index = synthetic_population.GetListOfIndexPairs()
    random.shuffle(response_index)

    input_text = []
    for r_idx in response_index:
        input_text.append(synthetic_population.GetPrompt(r_idx[0], r_idx[1]))

    all_text_input = [input_text[i:i+num_questions_per_batch] for i in range(0, len(input_text), num_questions_per_batch)]

    llm_implementation = None
    llm_model = None

    match llm_company_name:
        case nc.LLM_COMPANY.GPT4All:
            llm_implementation = LCI.LC_GPT4All()
            match llm_model_name:
                case nc.LLM_MODEL.GPT4A_MISTRAL_OPENORCA:
                    llm_model = LCL.GPT4A_Mistral_OpenOrca(llm_implementation)
                case nc.LLM_MODEL.GPT4A_FALCON:
                    llm_model = LCL.GPT4A_Falcon(llm_implementation)
                case nc.LLM_MODEL.GPT4A_ORCA_2:
                    llm_model = LCL.GPT4A_ORCA_2(llm_implementation)

        case nc.LLM_COMPANY.OPENAI:
            llm_implementation = LCI.LC_OpenAi()
            match llm_model_name:
                case nc.LLM_MODEL.OPENAI_GPT_3_5:
                    llm_model = LCL.OPENAI_GPT_3_5(llm_implementation)
                case nc.LLM_MODEL.OPENAI_DAVINCI:
                    llm_model = LCL.OPENAI_DAVINCI(llm_implementation)

        case nc.LLM_COMPANY.AWS:
            llm_implementation = LCI.LC_AWS()
            match llm_model_name:
                case nc.LLM_MODEL.AWS_CLAUDE:
                    llm_model = LCL.AWS_Anthropic_Claude_v1(llm_implementation)
                case nc.LLM_MODEL.AWS_TITAN:
                    llm_model = LCL.AWS_Amazon_Titan_Text_G1_Lite(llm_implementation)

    llm_model.template = "{question}"
    llm_model.prompt = ['question']

    synthetic_responses = ptr.PersonalityFactorResponseSet(implementation_personality_test)
    loaded_key = synthetic_responses.init_test_key(key_directory)

    if not os.path.exists(f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}"):
        os.mkdir(f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}")
        os.mkdir(
            f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}\\{personality_test_directory_name}")

    batch_idx = 0
    response_idx = 0
    entire_process_start_time = time.time()
    batch_delta_times = np.zeros(len(all_text_input))
    likert_responses = responses_set.implementation.get_likert_responses()

    for batch_input in all_text_input:
        batch_start_time = time.time()
        print(f"Batch: {batch_idx} out of {len(all_text_input)}")
        #print(f"{batch_input}")

        # Either we for loop each prompt and ask the LLM one at a time
        for input in batch_input:
            #print(f"input: {input}")
            generated_output = llm_model.run(input)
            generated_output = generated_output.lower()
            #print(f"generated output: {generated_output}")

            found_1 = generated_output.find(f"{likert_responses[0]}")
            found_2 = generated_output.find(f"{likert_responses[1]}")
            found_3 = generated_output.find(f"{likert_responses[2]}")
            found_4 = generated_output.find(f"{likert_responses[3]}")
            found_5 = generated_output.find(f"{likert_responses[4]}")

            found_result = 0
            if found_1 != -1:
                found_result = 1
            elif found_2 != -1:
                found_result = 2
            elif found_3 != -1:
                found_result = 3
            elif found_4 != -1:
                found_result = 4
            elif found_5 != -1:
                found_result = 5

            synthetic_population.SaveResponse(response_index[response_idx][0], response_index[response_idx][1], found_result)
            response_idx += 1

        synthetic_responses.init_raw_responses_pd(synthetic_population.GetResponseDf())
        synthetic_responses.eval_test_responses()
        #synthetic_responses.eval_centroid_similarity(profile_centroids)
        synthetic_responses.save(quick_load_directory, personality_test_directory_name)

        batch_end_time = time.time()
        delta_time = batch_end_time - batch_start_time
        batch_delta_times[batch_idx] = delta_time
        print(f"Batch: {batch_idx} finished in {delta_time}, avg batch time {np.sum(batch_delta_times)/(batch_idx + 1)}")
        batch_idx += 1



    print(f"Total Batch time: {(time.time() - entire_process_start_time)} seconds ---")

    print(synthetic_population.GetResponseDf())





test_google_pipline = False
test_internal_consistency = False
test_external_consistency = False
if test_google_pipline:
    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_NEO_300
    personality_test_directory_name = "population_aws_claude"
    profile_centroids = Centroids()

    key_directory = None
    test_response_directory = None
    quick_load_directory = None
    implementation_personality_test = None

    match PRIMARY_PERSONALITY_TEST:
        case nc.PERSONALITY_TEST.IPIP_NEO_60:
            key_directory = nc.IPIP_NEO_60_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_60_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_60_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo60()


        case nc.PERSONALITY_TEST.IPIP_NEO_120:
            key_directory = nc.IPIP_NEO_120_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_120_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_120_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo120()

        case nc.PERSONALITY_TEST.IPIP_NEO_300:
            key_directory = nc.IPIP_NEO_300_TEST_KEY_DIR
            test_response_directory = nc.IPIP_NEO_300_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_NEO_300_QUICKLOAD_DIR
            implementation_personality_test = tbpt.IpipNeo300()

        case nc.PERSONALITY_TEST.IPIP_50:
            key_directory = nc.IPIP_50_TEST_KEY_DIR
            test_response_directory = nc.IPIP_50_TEST_RESPONSES_DIR
            quick_load_directory = nc.IPIP_50_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Ipip50()

        case nc.PERSONALITY_TEST.PANAS_20:
            key_directory = nc.PANAS_20_TEST_KEY_DIR
            test_response_directory = nc.PANAS_20_TEST_RESPONSES_DIR
            quick_load_directory = nc.PANAS_20_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Panas20()

        case nc.PERSONALITY_TEST.BPAQ_29:
            key_directory = nc.BPAQ_29_TEST_KEY_DIR
            test_response_directory = nc.BPAQ_29_TEST_RESPONSES_DIR
            quick_load_directory = nc.BPAQ_29_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Bpaq29()

        case nc.PERSONALITY_TEST.SSCS_11:
            key_directory = nc.SSCS_11_TEST_KEY_DIR
            test_response_directory = nc.SSCS_11_TEST_RESPONSES_DIR
            quick_load_directory = nc.SSCS_11_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Sscs11()

        case nc.PERSONALITY_TEST.PVQ_RR_57:
            key_directory = nc.PVQRR_57_TEST_KEY_DIR
            test_response_directory = nc.PVQRR_57_TEST_RESPONSES_DIR
            quick_load_directory = nc.PVQRR_57_QUICKLOAD_DIR
            implementation_personality_test = tbpt.Pvqrr57()

        case _:
            raise Exception("Could not find personality test. Please check PRIMARY_PERSONALITY_TEST.")

    # Load test key and test responses
    responses_set = ptr.PersonalityFactorResponseSet(implementation_personality_test)

    keys_loaded = responses_set.init_test_key(key_directory)
    if not keys_loaded:
        raise Exception("Could not load the key for the personality test, check to make sure that key_directory is the correcct directory path.")

    try:
        # load the numpy files used earlier.
        test_directory = f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}\\{personality_test_directory_name}"
        if os.path.exists(test_directory): # Does the directory exist
            has_test_evaluation = os.path.isfile(f"{test_directory}/test_evaluation.npy")
            has_test_response = os.path.isfile(f"{test_directory}/test_responses.npy")
            has_test_similarity = os.path.isfile(f"{test_directory}/test_responses.npy")
            if has_test_evaluation and has_test_response and has_test_similarity:
                responses_set.load(quick_load_directory, personality_test_directory_name)
        else:
            raise ValueError()
    except ValueError:
        print("Cannot locate quickload. Loading original file")
        os.mkdir(f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}")
        os.mkdir(f"{quick_load_directory}" + f"\\PersonalityTest.{PRIMARY_PERSONALITY_TEST.name}\\{personality_test_directory_name}")

        responses_set.init_raw_responses_dir(test_response_directory)
        responses_set.eval_test_responses()
        responses_set.eval_centroid_similarity(profile_centroids)
        responses_set.save(quick_load_directory, personality_test_directory_name)

    if test_internal_consistency:
        # create dataframe of original data
        df_test_responses = responses_set.get_dataframe(responses=True, evaluation=False, cscf=False)

        np_o = df_test_responses.loc[:,
               ["I2", "I7", "I12", "I17", "I22", "I27", "I32", "I37", "I42", "I47", "I52",
               "I57", "I62", "I67", "I72", "I77", "I82", "I87", "I92", "I97", "I102", "I107", "I112", "I117", "I122",
               "I127", "I132", "I137", "I142", "I147", "I152", "I157", "I162", "I167", "I172", "I177", "I182", "I187",
               "I192", "I197", "I202", "I207", "I212", "I217", "I222", "I227", "I232", "I237", "I242", "I247", "I252",
               "I257", "I262", "I267", "I272", "I277", "I282", "I287", "I292", "I297"]]

        np_c = df_test_responses.loc[:,
               ["I4", "I9", "I14", "I19", "I24", "I29", "I34", "I39", "I44", "I49", "I54", "I59", "I64", "I69", "I74",
                "I79", "I84", "I89", "I94", "I99", "I104", "I109", "I114", "I119", "I124", "I129", "I134", "I139",
                "I144", "I149", "I154", "I159", "I164", "I169", "I174", "I179", "I184", "I189", "I194", "I199", "I204",
                "I209", "I214", "I219", "I224", "I229", "I234", "I239", "I244", "I249", "I254", "I259", "I264", "I269",
                "I274", "I279", "I284", "I289", "I294", "I299"]]

        np_e = df_test_responses.loc[:,
               ["I1", "I6", "I11", "I16", "I21", "I26", "I31", "I36", "I41", "I46", "I51", "I56", "I61", "I66", "I71",
                "I76", "I81", "I86", "I91", "I96", "I101", "I106", "I111", "I116", "I121", "I126", "I131", "I136",
                "I141", "I146", "I151", "I156", "I161", "I166", "I171", "I176", "I181", "I186", "I191", "I196", "I201",
                "I206", "I211", "I216", "I221", "I226", "I231", "I236", "I241", "I246", "I251", "I256", "I261", "I266",
                "I271", "I276", "I281", "I286", "I291", "I296"]]

        np_a = df_test_responses.loc[:,
               ["I3", "I8", "I13", "I18", "I23", "I28", "I33", "I38", "I43", "I48", "I53", "I58", "I63", "I68", "I73",
                "I78", "I83", "I88", "I93", "I98", "I103", "I108", "I113", "I118", "I123", "I128", "I133", "I138",
                "I143", "I148", "I153", "I158", "I163", "I168", "I173", "I178", "I183", "I188", "I193", "I198", "I203",
                "I208", "I213", "I218", "I223", "I228", "I233", "I238", "I243", "I248", "I253", "I258", "I263", "I268",
                "I273", "I278", "I283", "I288", "I293", "I298"]]

        np_n = df_test_responses.loc[:,
               ["I0", "I5", "I10", "I15", "I20", "I25", "I30", "I35", "I40", "I45", "I50", "I55", "I60", "I65", "I70",
                "I75", "I80", "I85", "I90", "I95", "I100", "I105", "I110", "I115", "I120", "I125", "I130", "I135",
                "I140", "I145", "I150", "I155", "I160", "I165", "I170", "I175", "I180", "I185", "I190", "I195", "I200",
                "I205", "I210", "I215", "I220", "I225", "I230", "I235", "I240", "I245", "I250", "I255", "I260", "I265",
                "I270", "I275", "I280", "I285", "I290", "I295"]]

        for enum_value in nc.PersonalityFactor:
            if enum_value.value >= 0 and enum_value.value < nc.PersonalityFactor.LENGTH.value:
                np_temp = np.zeros(1)
                match enum_value.value:
                    case 0: np_temp = np_o
                    case 1: np_temp = np_c
                    case 2: np_temp = np_e
                    case 3: np_temp = np_a
                    case 4: np_temp = np_n

                print(f"{enum_value.name}: ")
                print(f"min: {np.mean(np_temp, axis=1).min()}")
                print(f"med: {np.mean(np_temp, axis=1).median()}")
                print(f"max: {np.mean(np_temp, axis=1).max()}")
                print(f"std: {np.mean(np_temp, axis=1).std()}")

        print("Internal Validity Check")

        rm_o_interpret = None
        rm_c_interpret = None
        rm_e_interpret = None
        rm_a_interpret = None
        rm_n_interpret = None

        print("Cronbach Alpha -------------------------")
        cronbach_alpha_values = responses_set.cronbach_alpha_breakdown()
        cronbach_alpha_interpret = ["", "", "", "", ""]

        for rm_idx in range(len(cronbach_alpha_values)):
            if cronbach_alpha_values[rm_idx] < 0.5:
                cronbach_alpha_interpret[rm_idx] = "unacceptable"
            elif 0.5 <= cronbach_alpha_values[rm_idx] < 0.6:
                cronbach_alpha_interpret[rm_idx] = "poor"
            elif 0.6 <= cronbach_alpha_values[rm_idx] < 0.7:
                cronbach_alpha_interpret[rm_idx] = "questionable"
            elif 0.7 <= cronbach_alpha_values[rm_idx] < 0.8:
                cronbach_alpha_interpret[rm_idx] = "acceptable"
            elif 0.8 <= cronbach_alpha_values[rm_idx] < 0.9:
                cronbach_alpha_interpret[rm_idx] = "good"
            elif 0.9 <= cronbach_alpha_values[rm_idx] < 1.0:
                cronbach_alpha_interpret[rm_idx] = "excellent"

        print(f"Cronbach values: {cronbach_alpha_values}")
        print(f"Cronbach Interpret: {cronbach_alpha_interpret}")

        print("Guttman's Lambda-6 -------------------------")
        lambda_6_values = responses_set.lambda_6_breakdown()
        lambda_6_interpret = ["", "", "", "", ""]

        for rm_idx in range(len(lambda_6_values)):
            if lambda_6_values[rm_idx] < 0.5:
                lambda_6_interpret[rm_idx] = "unacceptable"
            elif 0.5 <= lambda_6_values[rm_idx] < 0.6:
                lambda_6_interpret[rm_idx] = "poor"
            elif 0.6 <= lambda_6_values[rm_idx] < 0.7:
                lambda_6_interpret[rm_idx] = "questionable"
            elif 0.7 <= lambda_6_values[rm_idx] < 0.8:
                lambda_6_interpret[rm_idx] = "acceptable"
            elif 0.8 <= lambda_6_values[rm_idx] < 0.9:
                lambda_6_interpret[rm_idx] = "good"
            elif 0.9 <= lambda_6_values[rm_idx] < 1.0:
                lambda_6_interpret[rm_idx] = "excellent"

        print(f"Guttman's Lambda-6 values: {lambda_6_values}")
        print(f"Cronbach Interpret: {lambda_6_interpret}")

        print("McDonalds Hierarchical Omega -------------------------")
        hierarchical_omega_values = responses_set.hierarchical_omega_breakdown()
        hierarchical_omega_interpret = ["", "", "", "", ""]

        for rm_idx in range(len(hierarchical_omega_values)):
            if hierarchical_omega_values[rm_idx] < 0.5:
                hierarchical_omega_interpret[rm_idx] = "unacceptable"
            elif 0.5 <= hierarchical_omega_values[rm_idx] < 0.6:
                hierarchical_omega_interpret[rm_idx] = "poor"
            elif 0.6 <= hierarchical_omega_values[rm_idx] < 0.7:
                hierarchical_omega_interpret[rm_idx] = "questionable"
            elif 0.7 <= hierarchical_omega_values[rm_idx] < 0.8:
                hierarchical_omega_interpret[rm_idx] = "acceptable"
            elif 0.8 <= hierarchical_omega_values[rm_idx] < 0.9:
                hierarchical_omega_interpret[rm_idx] = "good"
            elif 0.9 <= hierarchical_omega_values[rm_idx] < 1.0:
                hierarchical_omega_interpret[rm_idx] = "excellent"

        print(f"Guttman's Lambda-6 values: {hierarchical_omega_values}")
        print(f"Cronbach Interpret: {hierarchical_omega_interpret}")

    if test_external_consistency:
        REDUNDANT_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_50
        redundant_personality_test_directory_name = "clean_set"
        redundant_key_directory = nc.IPIP_50_TEST_KEY_DIR
        redundant_test_response_directory = nc.IPIP_50_TEST_RESPONSES_DIR
        redundant_quick_load_directory = nc.IPIP_50_QUICKLOAD_DIR
        redundant_implementation_personality_test = tbpt.Ipip50()

        # Load test key and test responses
        redundant_responses_set = ptr.PersonalityFactorResponseSet(redundant_implementation_personality_test)
        keys_loaded = redundant_responses_set.init_test_key(redundant_key_directory)
        if not keys_loaded:
            raise Exception(
                "Could not load the key for the personality test, check to make sure that redundant_key_directory is the correcct directory path.")

        try:
            # load the numpy files used earlier.
            test_directory = f"{redundant_quick_load_directory}" + f"\\PersonalityTest.{REDUNDANT_PERSONALITY_TEST.name}\\{redundant_personality_test_directory_name}"
            if os.path.exists(test_directory):  # Does the directory exist
                has_test_evaluation = os.path.isfile(f"{test_directory}/test_evaluation.npy")
                has_test_response = os.path.isfile(f"{test_directory}/test_responses.npy")
                has_test_similarity = os.path.isfile(f"{test_directory}/test_responses.npy")
                if has_test_evaluation and has_test_response and has_test_similarity:
                    redundant_responses_set.load(redundant_quick_load_directory, redundant_personality_test_directory_name)
            else:
                raise ValueError()
        except ValueError:
            print("Cannot locate quickload. Loading original file")
            os.mkdir(f"{redundant_quick_load_directory}" + f"\\PersonalityTest.{REDUNDANT_PERSONALITY_TEST.name}")
            os.mkdir(
                f"{redundant_quick_load_directory}" + f"\\PersonalityTest.{REDUNDANT_PERSONALITY_TEST.name}\\{redundant_personality_test_directory_name}")

            redundant_responses_set.init_raw_responses_dir(redundant_test_response_directory)
            redundant_responses_set.eval_test_responses()
            redundant_responses_set.eval_centroid_similarity(profile_centroids)
            redundant_responses_set.save(redundant_quick_load_directory, redundant_personality_test_directory_name)

        primary_test_responses = responses_set.get_dataframe(responses=False, evaluation=True, cscf=False, labels=None, columns=None).to_numpy()
        redundant_test_responses = redundant_responses_set.get_dataframe(responses=False, evaluation=True, cscf=False, labels=None, columns=None).to_numpy()

        mtmm = np.zeros((15, 15))

        # perform primary with first method
        primary_with_first_method = pt.calculate_pearson_product_moment_correlation(
            primary_evaluated_test_array= primary_test_responses,
            redundant_evaluated_test_array= primary_test_responses,
            sample_size= primary_test_responses.shape[0],
            randomize=False)

        for x_idx in range(5):
            for y_idx in range(5):
                mtmm[x_idx][y_idx] = primary_with_first_method[x_idx][y_idx]

        # perform primary with second method
        primary_with_second_method = pt.calculate_pearson_product_moment_correlation(
            primary_evaluated_test_array=primary_test_responses,
            redundant_evaluated_test_array=redundant_test_responses,
            sample_size= min(primary_test_responses.shape[0], redundant_test_responses.shape[0]),
            randomize=False)

        for x_idx in range(5):
            for y_idx in range(5):
                mtmm[5 + x_idx][y_idx] = primary_with_second_method[x_idx][y_idx]


        #personality_test_directory_name = "clean_set"
        external_key_directory = nc.PANAS_20_TEST_KEY_DIR
        external_test_response_directory = nc.PANAS_20_TEST_RESPONSES_DIR
        external_quick_load_directory = nc.PANAS_20_QUICKLOAD_DIR
        external_implementation_personality_test = tbpt.Panas20()
        PANS_responses_set = ptr.PersonalityFactorResponseSet(external_implementation_personality_test)
        keys_loaded = PANS_responses_set.init_test_key(external_key_directory)
        PANS_responses_set.load(external_quick_load_directory, personality_test_directory_name)
        PANS_responses_set.eval_test_responses()
        PANS_dataframe = PANS_responses_set.get_dataframe(responses=False, evaluation=True,cscf=False,labels=None,columns=None)

        external_key_directory = nc.BPAQ_29_TEST_KEY_DIR
        external_test_response_directory = nc.BPAQ_29_TEST_RESPONSES_DIR
        external_quick_load_directory = nc.BPAQ_29_QUICKLOAD_DIR
        external_implementation_personality_test = tbpt.Bpaq29()
        BPAQ_responses_set = ptr.PersonalityFactorResponseSet(external_implementation_personality_test)
        keys_loaded = BPAQ_responses_set.init_test_key(external_key_directory)
        BPAQ_responses_set.load(external_quick_load_directory, personality_test_directory_name)
        BPAQ_responses_set.eval_test_responses()
        BPAQ_dataframe = BPAQ_responses_set.get_dataframe(responses=False, evaluation=True,cscf=False,labels=None,columns=None)

        external_key_directory = nc.SSCS_11_TEST_KEY_DIR
        external_test_response_directory = nc.SSCS_11_TEST_RESPONSES_DIR
        external_quick_load_directory = nc.SSCS_11_QUICKLOAD_DIR
        external_implementation_personality_test = tbpt.Sscs11()
        SSCS_responses_set = ptr.PersonalityFactorResponseSet(external_implementation_personality_test)
        keys_loaded = SSCS_responses_set.init_test_key(external_key_directory)
        SSCS_responses_set.load(external_quick_load_directory, personality_test_directory_name)
        SSCS_responses_set.eval_test_responses()
        SSCS_dataframe = SSCS_responses_set.get_dataframe(responses=False, evaluation=True,cscf=False,labels=None,columns=None)

        external_key_directory = nc.PVQRR_57_TEST_KEY_DIR
        external_test_response_directory = nc.PVQRR_57_TEST_RESPONSES_DIR
        external_quick_load_directory = nc.PVQRR_57_QUICKLOAD_DIR
        external_implementation_personality_test = tbpt.Pvqrr57()
        PVQRR_responses_set = ptr.PersonalityFactorResponseSet(external_implementation_personality_test)
        keys_loaded = PVQRR_responses_set.init_test_key(external_key_directory)
        PVQRR_responses_set.load(external_quick_load_directory, personality_test_directory_name)
        PVQRR_responses_set.eval_test_responses()
        PVQRR_dataframe = PVQRR_responses_set.get_dataframe(responses=False, evaluation=True,cscf=False,labels=None,columns=None)

        response_dataframe = responses_set.get_dataframe(responses=False, evaluation=True,cscf=False,labels=None,columns=None)

        large_primary_response_array = response_dataframe["OPENNESS"].to_numpy()
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["OPENNESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["CONSCIENTIOUSNESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["CONSCIENTIOUSNESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["CONSCIENTIOUSNESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["EXTRAVERSION"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["EXTRAVERSION"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["AGREEABLENESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["AGREEABLENESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["AGREEABLENESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["AGREEABLENESS"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["NEUROTICISM"].to_numpy()])
        large_primary_response_array = np.vstack([large_primary_response_array, response_dataframe["NEUROTICISM"].to_numpy()])
        large_primary_response_array = np.transpose(large_primary_response_array)

        large_external_response_array = SSCS_dataframe["SELF_EFFICACY"].to_numpy()
        large_external_response_array = np.vstack([large_external_response_array, SSCS_dataframe["PERSONAL_IDENTITY"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PVQRR_dataframe["ACHIEVEMENT"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PVQRR_dataframe["CONFORMITY"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PVQRR_dataframe["SECURITY"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PANS_dataframe["POSITIVE"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PANS_dataframe["NEGATIVE"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, BPAQ_dataframe["PHYSICAL"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, BPAQ_dataframe["VERBAL"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, BPAQ_dataframe["ANGER"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, BPAQ_dataframe["HOSTILITY"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PANS_dataframe["NEGATIVE"].to_numpy()])
        large_external_response_array = np.vstack([large_external_response_array, PANS_dataframe["POSITIVE"].to_numpy()])
        large_external_response_array = np.transpose(large_external_response_array)

        # perform redundant with second method
        external_with_primary_method = pt.calculate_pearson_product_moment_correlation(
            primary_evaluated_test_array=large_primary_response_array,
            redundant_evaluated_test_array=large_external_response_array,
            sample_size=min(large_primary_response_array.shape[0], large_external_response_array.shape[0]),
            randomize=False)

        #for x_idx in range(external_with_primary_method.shape[0]):
            #for y_idx in range(external_with_primary_method.shape[1]):
                #mtmm[5 + x_idx][5 + y_idx] = external_with_primary_method[x_idx][y_idx]
        print(external_with_primary_method)
        print("-----------------------")

        # perform redundant with self
        redundant_with_second_method = pt.calculate_pearson_product_moment_correlation(
            primary_evaluated_test_array=redundant_test_responses,
            redundant_evaluated_test_array=redundant_test_responses,
            sample_size=redundant_test_responses.shape[0],
            randomize=False)

        for x_idx in range(5):
            for y_idx in range(5):
                mtmm[5 + x_idx][5 + y_idx] = redundant_with_second_method[x_idx][y_idx]

        # TODO: perform redundant with third method

        # perform external with first method, not needed

        # perform external with second method, not needed

        # TODO: perform external with third method

        print(mtmm)

add_labels = True
if add_labels:
    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_50
    implementation_Ipip50 = tbpt.Ipip50()
    profile_centroids = Centroids()

    # Load test key and test responses
    responses_Ipip50 = ptr.PersonalityFactorResponseSet(implementation_Ipip50)
    loaded_key = responses_Ipip50.init_test_key(nc.IPIP_50_TEST_KEY_DIR)

    population_set = "population_flan_xxl"

    responses_Ipip50.load(nc.IPIP_50_QUICKLOAD_DIR, population_set)

    #create dataframe of original data
    df_test_responses = responses_Ipip50.get_dataframe(responses=True, evaluation=False, cscf=False, labels=responses_Ipip50.get_top_k_closest_centroid_list(1))
    df_test_evaluation = responses_Ipip50.get_dataframe(responses=False, evaluation=True, cscf=False, labels=responses_Ipip50.get_top_k_closest_centroid_list(1))


    with open(file=f"C:\\Users\\38977332\\OneDrive - Southern Methodist University\\Documents\\School\\APF\\Repos\\personal\\apf\\LLM_Personality\\fall_23\\notebooks\\results\\PersonalityTest.IPIP_50\\{population_set}\\test_responses_labels.npy",mode="wb") as f:
        np.save(file=f, arr=df_test_responses.to_numpy(), allow_pickle=True)

    with open(file=f"C:\\Users\\38977332\\OneDrive - Southern Methodist University\\Documents\\School\\APF\\Repos\\personal\\apf\\LLM_Personality\\fall_23\\notebooks\\results\\PersonalityTest.IPIP_50\\{population_set}\\test_evaluation_labels.npy",mode="wb") as f:
        np.save(file=f, arr=df_test_evaluation.to_numpy(), allow_pickle=True)




def draw_graph_with_labels(output_layout, encoding):
    profile_names = [e.name for e in nc.PersonalityProfile]
    profile_names = np.delete(profile_names, 0)
    profile_names = np.delete(profile_names, profile_names.shape[0] - 1)

    np_profile_centers = np.array(nc.OCEAN_PROFILES)
    np_profile_encoding = encoding.transform(np_profile_centers)
    np_profile_names = np.array([e for e in range(0, 20)])
    np_profile_names = np.reshape(np_profile_names, (20, 1))

    np_profile = np.append(np_profile_encoding, np_profile_names, axis=1)
    df_profiles = pd.DataFrame(np_profile,
                               columns=["x", "y", "label"])

    profile_eval_imgs = [hv.Scatter(
        data=df_profiles[df_profiles.label == label_idx], kdims=["x", "y"], vdims=["label"]).opts(
        color='label',
        cmap=[nc.LABEL_COLORS_HEX[10]],
        s=300,
        alpha=1.0,
        marker='X',
        fontsize={'title': 50,
                  'labels': 25,
                  'xticks': 20,
                  'yticks': 20}) for label_idx in [e for e in range(0, 20)]]

    label_eval_imgs = [
        hv.Labels(df_profiles[df_profiles.label == label_idx], kdims=["x", "y"], vdims=["label"]).opts(
            cmap=[nc.LABEL_COLORS_HEX[10]]) for label_idx in [e for e in range(0, 20)]]

    output_layout *= (profile_eval_imgs[0] * label_eval_imgs[0])
    for idx in range(1, 20):
        output_layout *= profile_eval_imgs[idx] * label_eval_imgs[idx]

    output = output_layout.opts(opts.Labels(color="label", cmap=build_custom_continuous_cmap(nc.LABEL_COLORS_RGP[10],
                                                                                    nc.LABEL_COLORS_RGP[10]),
                                   xoffset=0.0, yoffset=0.025, size=24, padding=0.025))

    svg, info = renderer(output.opts(fig_inches=10, fig_bounds=(0, 0, 1, 1)), fmt='svg')
    display_svg(svg, raw=False)



test_visualization = False
old_visual = False
if test_visualization:
    PRIMARY_PERSONALITY_TEST = nc.PERSONALITY_TEST.IPIP_50
    profile_centroids = Centroids()

    implementation_ipip50 = tbpt.Ipip50()
    clean_responses_ipip50 = ptr.PersonalityFactorResponseSet(implementation_ipip50)
    loaded_key = clean_responses_ipip50.init_test_key(nc.IPIP_50_TEST_KEY_DIR)

    try:
        clean_responses_ipip50.load(nc.IPIP_NEO_300_QUICKLOAD_DIR, "clean_set")
    except ValueError:
        print("Cannot locate quickload. Loading original file")
        clean_responses_ipip50.init_raw_responses_pd(df_test_responses)
        clean_responses_ipip50.eval_test_responses()
        clean_responses_ipip50.eval_centroid_similarity(profile_centroids)
        clean_responses_ipip50.save(nc.IPIP_NEO_300_QUICKLOAD_DIR, "clean_set")

    hv.output(clean_responses_ipip50.scatter_plot_cscf(show_profile_centers=True))


    ipip_neo_population = clean_responses_ipip50.get_dataframe(responses=False, evaluation=True, cscf=False,
                                                               labels=clean_responses_ipip50.get_top_k_closest_centroid_list(
                                                                   1))

    x = ipip_neo_population[['OPENNESS', 'CONSCIENTIOUSNESS', 'EXTRAVERSION', 'AGREEABLENESS', 'NEUROTICISM']]
    y = ipip_neo_population[['l0']]

    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.1, stratify=y)

    from sklearn.decomposition import PCA

    PCA_encoding = PCA(n_components=2, svd_solver="full", random_state=0)
    PCA_encoding.fit(X_train, y_train.to_numpy().flatten())
    results = PCA_encoding.transform(X_test)
    # results = PCA_encoding.fit_transform(X_test, y_test)
    results = np.hstack((results, y_test.to_numpy()))



    cscf_img = hv.Scatter(
        data=results, kdims=["0", "1"], vdims=["2"]
    ).opts(color="2",
           cmap=nc.LABEL_COLORS_HEX,
           alpha=0.05
           )

    draw_graph_with_labels(cscf_img, PCA_encoding)

