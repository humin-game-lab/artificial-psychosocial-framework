# commons file to hold globale values that the notebook or any other script can use
import os
from enum import Enum

# Types of Large Language Models that are available
# ----------------------------------------------------------------------------------------------------------------------
class LargeLanguageModel(Enum):
    UNKNOWN_MODEL = -1
    BEDROCK_ANTHROPIC_CLAUDE = 0
    BEDROCK_ANTHROPIC_CLAUDE_INSTANT = 1
    BEDROCK_AWS_TITAN_TEXT_G1_EXPRESS = 2
    BEDROCK_AWS_TITAN_TEXT_G1_LITE = 3
    BEDROCK_META_LLAMA_2_13B = 4
    BEDROCK_META_LLAMA_2_70B = 5
    BEDROCK_META_LLAMA_2_CHAT_13B = 6
    BEDROCK_META_LLAMA_2_CHAT_70B = 7
    GPT4ALL_ALPACA_7B = 8
    GPT4ALL_ALPACA_LORA_7B = 9
    GPT4ALL_DOLLY_12B = 10
    GPT4ALL_DOLLY_6B = 11
    GPT4ALL_FALCON_7B = 12
    GPT4ALL_FALCON_7B_INSTRUCT = 13
    GPT4ALL_FASTCHAT_T5 = 14
    GPT4ALL_FASTCHAT_VICUNA_13B = 15
    GPT4ALL_FASTCHAT_VICUNA_7B = 16
    GPT4ALL_GPT_J_6_7B = 17
    GPT4ALL_GPT4_VICUNA_13B = 18
    GPT4ALL_GPT4ALL_13B_SNOOZY = 19
    GPT4ALL_GPT4ALL_FALCON = 20
    GPT4ALL_GPT4ALL_J_6B_V1_0 = 21
    GPT4ALL_GPT4ALL_J_LORA_6B = 22
    GPT4ALL_GPT4ALL_J_V1_1_BREEZY = 23
    GPT4ALL_GPT4ALL_J_V1_2_JAZZY = 24
    GPT4ALL_GPT4ALL_J_V1_3_GROOVY = 25
    GPT4ALL_GPT4ALL_LLAMA_LORA_7B = 26
    GPT4ALL_KOALA_13B = 27
    GPT4ALL_LLAMA_13B = 28
    GPT4ALL_LLAMA_7B = 29
    GPT4ALL_MOSAIC_MPT_CHAT = 30
    GPT4ALL_MOSAIC_MPT_INSTRUCT = 31
    GPT4ALL_MOSAIC_MPT7B = 32
    GPT4ALL_NOUS_HERMES = 33
    GPT4ALL_NOUS_HERMES2 = 34
    GPT4ALL_NOUS_PUFFIN = 35
    GPT4ALL_OPEN_ASSISTANT_PYTHIA_12B = 36
    GPT4ALL_PYTHIA_12B = 37
    GPT4ALL_PYTHIA_6_7B = 38
    GPT4ALL_STABLELM_BASE = 39
    GPT4ALL_STABLELM_TUNED = 40
    GPT4ALL_STABLEVICUNA_RLHF = 41
    GPT4ALL_TEXT_DAVINCI_003 = 42
    GPT4ALL_WIZARD_13B_UNCENSORED = 43
    GPT4ALL_WIZARD_7B = 44
    GPT4ALL_WIZARD_7B_UNCENSORED = 45
    HUGGINGFACE_GOOGLE_FLAN_T5_BASE = 46
    HUGGINGFACE_GOOGLE_FLAN_T5_LARGE = 47
    HUGGINGFACE_GOOGLE_FLAN_T5_SMALL = 48
    HUGGINGFACE_GOOGLE_FLAN_T5_UL2 = 49
    HUGGINGFACE_GOOGLE_FLAN_T5_XL = 50
    HUGGINGFACE_GOOGLE_FLAN_T5_XXL = 51
    OPENAI_GPT_3_5_TURBO_0301 = 52
    OPENAI_GPT_3_5_TURBO_0613 = 53
    OPENAI_GPT_3_5_TURBO_1106 = 54
    OPENAI_GPT_3_5_TURBO_16K_0613 = 55
    OPENAI_GPT_4_0314 = 56
    OPENAI_GPT_4_0613 = 57
    OPENAI_GPT_4_1106_PREVIEW = 58
    OPENAI_GPT_4_32K_0314 = 59
    OPENAI_GPT_4_32K_0613 = 60
    OPENAI_TEXT_DAVINCI_003 = 61
    LENGTH = 62


LARGE_LANGUAGE_MODEL = Enum('LargeLanguageModel',
                            ["UNKNOWN_MODEL", "BEDROCK_ANTHROPIC_CLAUDE", "BEDROCK_ANTHROPIC_CLAUDE_INSTANT",
                             "BEDROCK_AWS_TITAN_TEXT_G1_EXPRESS", "BEDROCK_AWS_TITAN_TEXT_G1_LITE",
                             "BEDROCK_META_LLAMA_2_13B", "BEDROCK_META_LLAMA_2_70B", "BEDROCK_META_LLAMA_2_CHAT_13B",
                             "BEDROCK_META_LLAMA_2_CHAT_70B", "GPT4ALL_ALPACA_7B", "GPT4ALL_ALPACA_LORA_7B",
                             "GPT4ALL_DOLLY_12B", "GPT4ALL_DOLLY_6B", "GPT4ALL_FALCON_7B", "GPT4ALL_FALCON_7B_INSTRUCT",
                             "GPT4ALL_FASTCHAT_T5", "GPT4ALL_FASTCHAT_VICUNA_13B", "GPT4ALL_FASTCHAT_VICUNA_7B",
                             "GPT4ALL_GPT_J_6_7B", "GPT4ALL_GPT4_VICUNA_13B", "GPT4ALL_GPT4ALL_13B_SNOOZY",
                             "GPT4ALL_GPT4ALL_FALCON", "GPT4ALL_GPT4ALL_J_6B_V1_0", "GPT4ALL_GPT4ALL_J_LORA_6B",
                             "GPT4ALL_GPT4ALL_J_V1_1_BREEZY", "GPT4ALL_GPT4ALL_J_V1_2_JAZZY",
                             "GPT4ALL_GPT4ALL_J_V1_3_GROOVY", "GPT4ALL_GPT4ALL_LLAMA_LORA_7B", "GPT4ALL_KOALA_13B",
                             "GPT4ALL_LLAMA_13B", "GPT4ALL_LLAMA_7B", "GPT4ALL_MOSAIC_MPT_CHAT",
                             "GPT4ALL_MOSAIC_MPT_INSTRUCT", "GPT4ALL_MOSAIC_MPT7B", "GPT4ALL_NOUS_HERMES",
                             "GPT4ALL_NOUS_HERMES2", "GPT4ALL_NOUS_PUFFIN", "GPT4ALL_OPEN_ASSISTANT_PYTHIA_12B",
                             "GPT4ALL_PYTHIA_12B", "GPT4ALL_PYTHIA_6_7B", "GPT4ALL_STABLELM_BASE",
                             "GPT4ALL_STABLELM_TUNED", "GPT4ALL_STABLEVICUNA_RLHF", "GPT4ALL_TEXT_DAVINCI_003",
                             "GPT4ALL_WIZARD_13B_UNCENSORED", "GPT4ALL_WIZARD_7B", "GPT4ALL_WIZARD_7B_UNCENSORED",
                             "HUGGINGFACE_GOOGLE_FLAN_T5_BASE", "HUGGINGFACE_GOOGLE_FLAN_T5_LARGE",
                             "HUGGINGFACE_GOOGLE_FLAN_T5_SMALL", "HUGGINGFACE_GOOGLE_FLAN_T5_UL2",
                             "HUGGINGFACE_GOOGLE_FLAN_T5_XL", "HUGGINGFACE_GOOGLE_FLAN_T5_XXL",
                             "OPENAI_GPT_3_5_TURBO_0301", "OPENAI_GPT_3_5_TURBO_0613", "OPENAI_GPT_3_5_TURBO_1106",
                             "OPENAI_GPT_3_5_TURBO_16K_0613", "OPENAI_GPT_4_0314", "OPENAI_GPT_4_0613",
                             "OPENAI_GPT_4_1106_PREVIEW", "OPENAI_GPT_4_32K_0314", "OPENAI_GPT_4_32K_0613",
                             "OPENAI_TEXT_DAVINCI_003", "LENGTH"])

# Types of personality tests that are available
# ----------------------------------------------------------------------------------------------------------------------
class PersonalityTest(Enum):
    UNKNOWN_TEST = -1
    IPIP_NEO_60 = 0
    IPIP_NEO_120 = 1
    IPIP_NEO_300 = 2
    IPIP_50 = 3,
    PANAS_20 = 4,
    BPAQ_29 = 5,
    SSCS_11 = 6,
    PVQ_RR_57 = 7
    LENGTH = 8


PERSONALITY_TEST = Enum('PersonalityTest',
                        ['UNKNOWN_TEST', 'IPIP_NEO_60', 'IPIP_NEO_120', 'IPIP_NEO_300', 'IPIP_50', 'PANAS_20',
                         'BPAQ_29', 'SSCS_11', 'PVQ_RR_57', 'LENGTH'])

# OCEAN Personality Setup
# ----------------------------------------------------------------------------------------------------------------------
class PersonalityFactor(Enum):
    UNKNOWN_FACTOR = -1
    OPENNESS = 0
    CONSCIENTIOUSNESS = 1
    EXTRAVERSION = 2
    AGREEABLENESS = 3
    NEUROTICISM = 4
    LENGTH = 5


PERSONALITY_FACTOR = Enum('PersonalityFactor',
                                   ['UNKNOWN_FACTOR', 'OPENNESS', 'CONSCIENTIOUSNESS', 'EXTRAVERSION',
                                    'AGREEABLENESS', 'NEUROTICISM', 'LENGTH'])


class PersonalitySubFactor(Enum):
    UNKNOWN_SUB_FACTOR = -1
    O1_IMAGINATION = 0
    O2_ARTISTIC_INTERESTS = 1
    O3_EMOTIONALITY = 2
    O4_ADVENTUROUSNESS = 3
    O5_INTELLECT = 4
    O6_LIBERALISM = 5
    C1_SELF_EFFICACY = 6
    C2_ORDERLINESS = 7
    C3_DUTIFULNESS = 8
    C4_ACHIEVEMENT_STRIVING = 9
    C5_SELF_DISCIPLINE = 10
    C6_CAUTIOUSNESS = 11
    E1_FRIENDLINESS = 12
    E2_GREGARIOUSNESS = 13
    E3_ASSERTIVENESS = 14
    E4_ACTIVITY_LEVEL = 15
    E5_EXCITEMENT_SEEKING = 16
    E6_CHEERFULNESS = 17
    A1_TRUST = 18
    A2_MORALITY = 19
    A3_ALTRUISM = 20
    A4_COOPERATION = 21
    A5_MODESTY = 22
    A6_SYMPATHY = 23
    N1_ANXIETY = 24
    N2_ANGER = 25
    N3_DEPRESSION = 26
    N4_SELF_CONSCIOUSNESS = 27
    N5_IMMODERATION = 28
    N6_VULNERABILITY = 29
    LENGTH = 30


PERSONALITY_SUB_FACTOR = Enum('PersonalitySubFactor',
                                       ["UNKNOWN_SUB_FACTOR", "O1_IMAGINATION", "O2_ARTISTIC_INTERESTS",
                                        "O3_EMOTIONALITY", "O4_ADVENTUROUSNESS", "O5_INTELLECT", "O6_LIBERALISM",
                                        "C1_SELF_EFFICACY", "C2_ORDERLINESS", "C3_DUTIFULNESS",
                                        "C4_ACHIEVEMENT_STRIVING", "C5_SELF_DISCIPLINE", "C6_CAUTIOUSNESS",
                                        "E1_FRIENDLINESS", "E2_GREGARIOUSNESS", "E3_ASSERTIVENESS", "E4_ACTIVITY_LEVEL",
                                        "E5_EXCITEMENT_SEEKING", "E6_CHEERFULNESS", "A1_TRUST", "A2_MORALITY",
                                        "A3_ALTRUISM", "A4_COOPERATION", "A5_MODESTY", "A6_SYMPATHY", "N1_ANXIETY",
                                        "N2_ANGER", "N3_DEPRESSION", "N4_SELF_CONSCIOUSNESS", "N5_IMMODERATION",
                                        "N6_VULNERABILITY", "LENGTH"])

#Need this for string comparisons
PERSONALITY_FACTOR_DICTIONARY = {
    "O1": PersonalityFactor.OPENNESS.value,
    "O2": PersonalityFactor.OPENNESS.value,
    "O3": PersonalityFactor.OPENNESS.value,
    "O4": PersonalityFactor.OPENNESS.value,
    "O5": PersonalityFactor.OPENNESS.value,
    "O6": PersonalityFactor.OPENNESS.value,
    "C1": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "C2": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "C3": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "C4": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "C5": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "C6": PersonalityFactor.CONSCIENTIOUSNESS.value,
    "E1": PersonalityFactor.EXTRAVERSION.value,
    "E2": PersonalityFactor.EXTRAVERSION.value,
    "E3": PersonalityFactor.EXTRAVERSION.value,
    "E4": PersonalityFactor.EXTRAVERSION.value,
    "E5": PersonalityFactor.EXTRAVERSION.value,
    "E6": PersonalityFactor.EXTRAVERSION.value,
    "A1": PersonalityFactor.AGREEABLENESS.value,
    "A2": PersonalityFactor.AGREEABLENESS.value,
    "A3": PersonalityFactor.AGREEABLENESS.value,
    "A4": PersonalityFactor.AGREEABLENESS.value,
    "A5": PersonalityFactor.AGREEABLENESS.value,
    "A6": PersonalityFactor.AGREEABLENESS.value,
    "N1": PersonalityFactor.NEUROTICISM.value,
    "N2": PersonalityFactor.NEUROTICISM.value,
    "N3": PersonalityFactor.NEUROTICISM.value,
    "N4": PersonalityFactor.NEUROTICISM.value,
    "N5": PersonalityFactor.NEUROTICISM.value,
    "N6": PersonalityFactor.NEUROTICISM.value
}

PERSONALITY_SUB_FACTOR_DICTIONARY = {
    "O1": PersonalitySubFactor.O1_IMAGINATION.value,
    "O2": PersonalitySubFactor.O2_ARTISTIC_INTERESTS.value,
    "O3": PersonalitySubFactor.O3_EMOTIONALITY.value,
    "O4": PersonalitySubFactor.O4_ADVENTUROUSNESS.value,
    "O5": PersonalitySubFactor.O5_INTELLECT.value,
    "O6": PersonalitySubFactor.O6_LIBERALISM.value,
    "C1": PersonalitySubFactor.C1_SELF_EFFICACY.value,
    "C2": PersonalitySubFactor.C2_ORDERLINESS.value,
    "C3": PersonalitySubFactor.C3_DUTIFULNESS.value,
    "C4": PersonalitySubFactor.C4_ACHIEVEMENT_STRIVING.value,
    "C5": PersonalitySubFactor.C5_SELF_DISCIPLINE.value,
    "C6": PersonalitySubFactor.C6_CAUTIOUSNESS.value,
    "E1": PersonalitySubFactor.E1_FRIENDLINESS.value,
    "E2": PersonalitySubFactor.E2_GREGARIOUSNESS.value,
    "E3": PersonalitySubFactor.E3_ASSERTIVENESS.value,
    "E4": PersonalitySubFactor.E4_ACTIVITY_LEVEL.value,
    "E5": PersonalitySubFactor.E5_EXCITEMENT_SEEKING.value,
    "E6": PersonalitySubFactor.E6_CHEERFULNESS.value,
    "A1": PersonalitySubFactor.A1_TRUST.value,
    "A2": PersonalitySubFactor.A2_MORALITY.value,
    "A3": PersonalitySubFactor.A3_ALTRUISM.value,
    "A4": PersonalitySubFactor.A4_COOPERATION.value,
    "A5": PersonalitySubFactor.A5_MODESTY.value,
    "A6": PersonalitySubFactor.A6_SYMPATHY.value,
    "N1": PersonalitySubFactor.N1_ANXIETY.value,
    "N2": PersonalitySubFactor.N2_ANGER.value,
    "N3": PersonalitySubFactor.N3_DEPRESSION.value,
    "N4": PersonalitySubFactor.N4_SELF_CONSCIOUSNESS.value,
    "N5": PersonalitySubFactor.N5_IMMODERATION.value,
    "N6": PersonalitySubFactor.N6_VULNERABILITY.value
}


class PersonalityAffect(Enum):
    UNKNOWN_FACTOR = -1
    POSITIVE = 0
    NEGATIVE = 1
    LENGTH = 2


PERSONALITY_AFFECT = Enum('PersonalityAffect',
                          ['UNKNOWN_AFFECT', 'POSITIVE', 'NEGATIVE', 'LENGTH'])

PERSONALITY_AFFECT_DICTIONARY = {
    "P": PersonalityAffect.POSITIVE.value,
    "N": PersonalityAffect.NEGATIVE.value
}


class PersonalityAggression(Enum):
    UNKNOWN_AGGRESSION = -1
    PHYSICAL = 0
    VERBAL = 1
    ANGER = 2
    HOSTILITY = 3
    LENGTH = 4


PERSONALITY_AGGRESSION = Enum('PersonalityAggression',
                              ['UNKNOWN_AGGRESSION', 'PHYSICAL', 'VERBAL', 'ANGER', 'HOSTILITY',
                               'LENGTH'])


PERSONALITY_AGGRESSION_DICTIONARY = {
    "PAS": PersonalityAggression.PHYSICAL.value,
    "VAS": PersonalityAggression.VERBAL.value,
    "AAS": PersonalityAggression.ANGER.value,
    "HAS": PersonalityAggression.HOSTILITY.value,
}


class PersonalityCreative(Enum):
    UNKNOWN_CREATIVE = -1
    SELF_EFFICACY = 0
    PERSONAL_IDENTITY = 1
    LENGTH = 2


PERSONALITY_CREATIVE = Enum('PersonalityCreative',
                            ['UNKNOWN_CREATIVE', 'SELF_EFFICACY', 'PERSONAL_IDENTITY', 'LENGTH'])

PERSONALITY_CREATIVE_DICTIONARY = {
    "CSE": PersonalityCreative.SELF_EFFICACY.value,
    "CPI": PersonalityCreative.PERSONAL_IDENTITY.value,
}

class PersonalityValue(Enum):
    UNKNOWN_VALUE = -1
    SELF_DIRECTION = 0
    SECURITY = 1
    STIMULATION = 2
    CONFORMITY = 3
    HEDONISM = 4
    TRADITION = 5
    ACHIEVEMENT = 6
    BENEVOLENCE = 7
    POWER = 8
    UNIVERSALISM = 9
    LENGTH = 10


PERSONALITY_VALUE = Enum('PersonalityValue',
                            ['UNKNOWN_VALUE', "SELF_DIRECTION", "SECURITY", "STIMULATION", "CONFORMITY",
                             "HEDONISM", "TRADITION", "ACHIEVEMENT", "BENEVOLENCE", "POWER", "UNIVERSALISM",
                             'LENGTH'])

PERSONALITY_VALUE_DICTIONARY = {
    "UNKNOWN": PersonalityValue.UNKNOWN_VALUE.value,
    "Self-Direction": PersonalityValue.SELF_DIRECTION.value,
    "Security": PersonalityValue.SECURITY.value,
    "Stimulation": PersonalityValue.STIMULATION.value,
    "Conformity": PersonalityValue.CONFORMITY.value,
    "Hedonism": PersonalityValue.HEDONISM.value,
    "Tradition": PersonalityValue.TRADITION.value,
    "Achievement": PersonalityValue.ACHIEVEMENT.value,
    "Benevolence": PersonalityValue.BENEVOLENCE.value,
    "Power": PersonalityValue.POWER.value,
    "Universalism": PersonalityValue.UNIVERSALISM.value,
}

# The 20 Personality Profiles we will be monitoring for this experiment
# ----------------------------------------------------------------------------------------------------------------------
class PersonalityProfile(Enum):
    UNKNOWN_PROFILE = -1
    PARANOID = 0
    SCHIZOID = 1
    SCHIZOTYPAL = 2
    ANTISOCIAL = 3
    BORDERLINE = 4
    HISTRIONIC = 5
    NARCISSISTIC = 6
    AVOIDANT = 7
    DEPENDENT = 8
    OBSESSIVE_COMPULSIVE = 9
    PRONOID = 10
    PEOPLE_PERSON = 11
    SENSIBLE = 12
    PROSOCIAL = 13
    STRAIGHTFORWARD = 14
    NON_THEATRICAL = 15
    UNPRETENTIOUS = 16
    ACCOMMODATING = 17
    INDEPENDENT = 18
    LAISSEZ_FAIRE = 19
    LENGTH = 20


PERSONALITY_PROFILE = Enum('PersonalityProfile',
                           ["UNKNOWN_PROFILE", "PARANOID", "SCHIZOID", "SCHIZOTYPAL", "ANTISOCIAL", "BORDERLINE",
                            "HISTRIONIC", "NARCISSISTIC", "AVOIDANT", "DEPENDENT", "OBSESSIVE_COMPULSIVE", "PRONOID",
                            "PEOPLE_PERSON", "SENSIBLE", "PROSOCIAL", "STRAIGHTFORWARD", "NON_THEATRICAL",
                            "UNPRETENTIOUS", "ACCOMMODATING", "INDEPENDENT", "LAISSEZ_FAIRE", "LENGTH"])


# Parallel lists for each personality profile
# Need to assign each personality test a label based on the 20 personality profiles
OCEAN_PROFILES = [
    [0.3825, 0.6045, 0.3260, 0.2090, 0.6345],  # 0
    [0.3600, 0.4875, 0.0850, 0.4275, 0.4425],  # 1
    [0.5415, 0.4050, 0.2265, 0.4215, 0.6570],  # 2
    [0.5390, 0.3200, 0.5250, 0.2785, 0.5790],  # 3
    [0.5770, 0.3430, 0.5050, 0.3350, 0.7940],  # 4
    [0.6455, 0.3730, 0.7485, 0.4410, 0.5705],  # 5
    [0.5505, 0.4490, 0.6200, 0.2230, 0.4985],  # 6
    [0.4170, 0.4965, 0.1575, 0.4685, 0.8600],  # 7
    [0.4415, 0.4645, 0.3845, 0.6380, 0.7170],  # 8
    [0.2685, 0.8545, 0.3695, 0.4705, 0.5625],  # 9
    [0.6175, 0.3955, 0.6740, 0.7910, 0.3655],  # 10
    [0.6400, 0.5125, 0.9150, 0.5725, 0.5575],  # 11
    [0.4585, 0.5950, 0.7735, 0.5785, 0.3430],  # 12
    [0.4610, 0.6800, 0.4750, 0.7215, 0.4210],  # 13
    [0.4230, 0.6570, 0.4950, 0.6650, 0.2060],  # 14
    [0.3545, 0.6270, 0.2515, 0.5590, 0.4295],  # 15
    [0.4495, 0.5510, 0.3800, 0.7770, 0.5015],  # 16
    [0.5830, 0.5035, 0.8425, 0.5315, 0.1400],  # 17
    [0.5585, 0.5355, 0.6155, 0.3620, 0.2830],  # 18
    [0.7315, 0.1455, 0.6305, 0.5295, 0.4375]  # 19
]

LABEL_COLORS_HEX = ["#e71d43",
                    "#ff0000",
                    "#ff3700",
                    "#ff6e00",
                    "#ffa500",
                    "#ffc300",
                    "#ffe100",
                    "#ffff00",
                    "#aad500",
                    "#55aa00",
                    "#008000",
                    "#005555",
                    "#002baa",
                    "#0000ff",
                    "#1900d5",
                    "#3200ac",
                    "#4b0082",
                    "#812ba6",
                    "#b857ca",
                    "#d03a87"]

LABEL_COLORS_RGP = [[231, 29, 67],
                    [255, 0, 0],
                    [255, 55, 0],
                    [255, 110, 0],
                    [255, 165, 0],
                    [255, 195, 0],
                    [255, 225, 0],
                    [255, 255, 0],
                    [170, 213, 0],
                    [85, 170, 0],
                    [0, 128, 0],
                    [0, 85, 85],
                    [0, 43, 120],
                    [0, 0, 255],
                    [25, 0, 213],
                    [50, 0, 172],
                    [75, 0, 130],
                    [129, 43, 166],
                    [184, 87, 202],
                    [208, 58, 135]]



# Important Directories
# ----------------------------------------------------------------------------------------------------------------------
DIR_PATH = os.path.dirname(os.path.realpath(__file__))
ACR_PRC_IMG_DIR = DIR_PATH + r"\..\resources\images\Accuracy-And-Precision.png"
SMR_RES_IMG_DIR = DIR_PATH + r"\..\resources\images\eval_examples.png"

IPIP_NEO_300_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_300\IPIPNEOitemresponses2023.csv"
IPIP_NEO_300_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_300\IPIPitemkey_300.csv"
IPIP_NEO_300_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"

IPIP_NEO_120_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_120\final_merged_IPIP120.csv"
IPIP_NEO_120_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
IPIP_NEO_120_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_120\IPIPitemkey_120.csv"

IPIP_NEO_60_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
IPIP_NEO_60_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_60\IPIPitemkey_60.csv"
IPIP_NEO_60_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\ipip_neo_60\data-final-commas.csv"

IPIP_50_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\ipip_50\data-final-commas.csv"
IPIP_50_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
IPIP_50_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\ipip_50\IPIPitemkey_50.csv"


BPAQ_29_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\bpaq_29\randomized_item_responses.csv"
BPAQ_29_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
BPAQ_29_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\bpaq_29\BPAQitemkey_29.csv"

PANAS_20_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\panas_20\randomized_item_responses.csv"
PANAS_20_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
PANAS_20_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\panas_20\PANASitemkey_20.csv"

PVQRR_57_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\pvq_rr_57\randomized_item_responses.csv"
PVQRR_57_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
PVQRR_57_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\pvq_rr_57\PVQRRitemkey_57.csv"

SSCS_11_TEST_RESPONSES_DIR = DIR_PATH + r"\..\resources\datasets\sscs_11\randomized_item_responses.csv"
SSCS_11_QUICKLOAD_DIR = DIR_PATH + r"\..\notebooks\results"
SSCS_11_TEST_KEY_DIR = DIR_PATH + r"\..\resources\datasets\sscs_11\SSCSitemkey_11.csv"



# LLM Directories (These models are only saved locally, need to update this if the project was moved)
# ----------------------------------------------------------------------------------------------------------------------
GPT4ALL_MISTRAL_OPEN_ORCA_MODEL_DIR = "D:\Other\GPT4All\mistral-7b-openorca.Q4_0.gguf"
GPT4ALL_FALCON_DIR = "D:\Other\GPT4All\gpt4all-falcon-q4_0.gguf"
GPT4ALL_ORCA_2_DIR = "D:\Other\GPT4All\orca-2-7b.Q4_0.gguf"

GPT_3_5_TEST_RESPONSES_DIR = DIR_PATH + r"\..\notebooks\results\PersonalityTest.IPIP_50\population_gpt_3_5\PersonalityTestTurbo.csv"
GPT_DAVINCI_TEST_RESPONSES_DIR = DIR_PATH + r"\..\notebooks\results\PersonalityTest.IPIP_50\population_davinci\PersonalityTestDavinci.csv"
GPT_4_TEST_RESPONSES_DIR = DIR_PATH + r"\..\notebooks\results\PersonalityTest.IPIP_50\population_gpt_4\PersonalityTestGPT4.csv"


os.environ["OPENAI_API_KEY"] = "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"
os.environ["OPENAI_ORGANIZATION"] = "org-JLSNwUcEd3r2tW3zWb10y414"
# ----------------------------------------------------------------------------------------------------------------------

class LlmCompany(Enum):
    UNKNOWN_COMPANY = -1
    GPT4All = 0,
    OPENAI = 1,
    AWS = 2,
    LENGTH = 3


LLM_COMPANY = Enum('LlmCompany',
                            ['UNKNOWN_COMPANY', 'GPT4All', 'OPENAI', "AWS", 'LENGTH'])

class LlmModel(Enum):
    UNKNOWN_MODEL = -1
    GPT4A_MISTRAL_OPENORCA = 0,
    GPT4A_FALCON = 1,
    GPT4A_ORCA_2 = 2,
    OPENAI_GPT_3_5 = 3,
    OPENAI_DAVINCI = 4,
    AWS_CLAUDE = 5,
    AWS_TITAN = 6,
    LENGTH = 7


LLM_MODEL = Enum('LlmModel',
                            ['UNKNOWN_MODEL', 'GPT4A_MISTRAL_OPENORCA', 'GPT4A_FALCON', 'GPT4A_ORCA_2',
                             'OPENAI_GPT_3_5', 'OPENAI_DAVINCI', 'AWS_CLAUDE', 'AWS_TITAN', 'LENGTH'])




# using this for editing