import abc
from enum import Enum
from typing import Any

import numpy as np

import fall_23.scripts.notebook_commons as nc


class TraitBasedPersonalityTest(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'evaluate_personality_trait') and
                callable(subclass.evaluate_personality_trait) and

                hasattr(subclass, 'get_column_names') and
                callable(subclass.get_column_names) and

                hasattr(subclass, 'to_string') and
                callable(subclass.to_string) and

                hasattr(subclass, 'get_likert_responses') and
                callable(subclass.to_string) and

                hasattr(subclass, 'get_likert_question') and
                callable(subclass.to_string)
                )

    @abc.abstractmethod
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = -1
        self._test_type = nc.PERSONALITY_TEST.UNKNOWN_TEST
        self._personality_makeup = None
        self._likert_range = 5

    @property
    def item(self) -> list[Any]:
        return self._item

    @item.setter
    def item(self, value: list[Any]):
        self._item = value

    @item.deleter
    def item(self):
        del self._item

    @property
    def direction(self) -> list[Any]:
        return self._direction

    @direction.setter
    def direction(self, value: list[Any]):
        self._direction = value

    @direction.deleter
    def direction(self):
        del self._direction

    @property
    @abc.abstractmethod
    def trait(self) -> list[Any]:
        return self._trait

    @trait.setter
    @abc.abstractmethod
    def trait(self, value: list[Any]):
        self._trait = value

    @trait.deleter
    @abc.abstractmethod
    def trait(self):
        del self._trait

    @property
    def num_items(self) -> int:
        return self._num_items

    @num_items.setter
    def num_items(self, value: int):
        self._num_items = value

    @num_items.deleter
    def num_items(self):
        del self._num_items

    @property
    def test_type(self) -> nc.PersonalityTest:
        return self._test_type

    @test_type.setter
    def test_type(self, value: nc.PersonalityTest):
        self._test_type = value

    @test_type.deleter
    def test_type(self):
        del self._test_type

    @property
    def personality_makeup(self) -> Enum:
        return self._personality_makeup

    @personality_makeup.setter
    def personality_makeup(self, value: Enum):
        self._personality_makeup = value

    @personality_makeup.deleter
    def personality_makeup(self):
        del self._personality_makeup

    @property
    def likert_range(self) -> int:
        return self._likert_range

    @likert_range.setter
    def likert_range(self, value: int):
        self._likert_range = value

    @likert_range.deleter
    def likert_range(self):
        del self._likert_range

    @abc.abstractmethod
    def evaluate_personality_trait(self, likert_test_responses: list) -> list:
        if len(likert_test_responses) != self._num_items:
            return [-1.0]

        # Performing a summation of Likert Test responses to specific traits and then converting the traits from an arbitrary integer to a percentage
        num_traits = self._personality_makeup.LENGTH.value
        total_trait_value = np.array([0] * num_traits, dtype=np.int32)
        for item_idx in range(self._num_items):
            trait_value = self._trait[item_idx]
            base_value = ((num_traits + 1)/2.0) - ((num_traits + 1)/2.0) * self._direction[item_idx]
            response_inverse = base_value - likert_test_responses[item_idx]
            response = response_inverse * self._direction[item_idx] * -1.0
            total_trait_value[trait_value] += response

        return np.interp(total_trait_value,
                         xp=[(self._num_items / num_traits) * (1.0/5.0 * self._likert_range),
                             (self._num_items / num_traits) * (2.0/5.0 * self._likert_range),
                             (self._num_items / num_traits) * (3.0/5.0 * self._likert_range),
                             (self._num_items / num_traits) * (4.0/5.0 * self._likert_range),
                             (self._num_items / num_traits) * (5.0/5.0 * self._likert_range)],
                         fp=[0.0, 0.25, 0.50, 0.75, 1.0])

    @abc.abstractmethod
    def get_column_names(self) -> list:
        return ['I' + str(i) for i in range(self._num_items)]

    @abc.abstractmethod
    def to_string(self):
        return f"-------------------------------- \n Items: {self._item} \n direction: {self._direction} \n trait: {self._trait} \n -------------------------------- \n"

    @abc.abstractmethod
    def get_likert_responses(self):
        return ['very inaccurate', 'moderately inaccurate','neither accurate nor inaccurate','moderately accurate', 'very accurate']

    @abc.abstractmethod
    def get_likert_question(self):
        return "please rate how accurately this describes you"

# -----------------------------------------------------------------------------------------------------------------------


class IpipNeo60(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 60
        self._test_type = nc.PERSONALITY_TEST.IPIP_NEO_60
        self._personality_makeup = nc.PersonalityFactor
        self._likert_range = 5

        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) != self._num_items:
            error_message = "The length of value list does not match with the number of items."
            raise ValueError(error_message)

        self._trait = [nc.PERSONALITY_FACTOR_DICTIONARY[item_value] for item_value in value]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list) -> list:
        return super().evaluate_personality_trait(likert_test_responses)

    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return super().get_likert_responses()

    def get_likert_question(self):
        return super().get_likert_question()

# -----------------------------------------------------------------------------------------------------------------------


class IpipNeo120(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 120
        self._test_type = nc.PERSONALITY_TEST.IPIP_NEO_120
        self._personality_makeup = nc.PersonalityFactor
        self._likert_range = 5

        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) != self._num_items:
            error_message = "The length of value list does not match with the number of items."
            raise ValueError(error_message)

        self._trait = [nc.PERSONALITY_FACTOR_DICTIONARY[item_value] for item_value in value]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list) -> list:
        return super().evaluate_personality_trait(likert_test_responses)

    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return super().get_likert_responses()

    def get_likert_question(self):
        return super().get_likert_question()

# -----------------------------------------------------------------------------------------------------------------------


class IpipNeo300(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 300
        self._test_type = nc.PERSONALITY_TEST.IPIP_NEO_300
        self._personality_makeup = nc.PersonalityFactor
        self._likert_range = 5

        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) != self._num_items:
            error_message = "The length of value list does not match with the number of items."
            raise ValueError(error_message)

        self._trait = [nc.PERSONALITY_FACTOR_DICTIONARY[item_value] for item_value in value]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)

    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return super().get_likert_responses()

    def get_likert_question(self):
        return super().get_likert_question()

# -----------------------------------------------------------------------------------------------------------------------


class Ipip50(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 50
        self._test_type = nc.PERSONALITY_TEST.IPIP_50
        self._personality_makeup = nc.PersonalityFactor
        self._likert_range = 5

        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) == self._num_items:
            self._trait = [nc.PERSONALITY_SUB_FACTOR.UNKNOWN_SUB_FACTOR.value] * self._num_items
            for item_idx in range(self._num_items):
                self._trait[item_idx] = nc.PERSONALITY_FACTOR_DICTIONARY[value[item_idx]]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)


    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return super().get_likert_responses()

    def get_likert_question(self):
        return super().get_likert_question()

# -----------------------------------------------------------------------------------------------------------------------


class Bpaq29(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 29
        self._test_type = nc.PERSONALITY_TEST.BPAQ_29
        self._personality_makeup = nc.PersonalityAggression
        self._likert_range = 5

        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) == self._num_items:
            self._trait = [nc.PERSONALITY_AGGRESSION.UNKNOWN_AGGRESSION.value] * self._num_items
            for item_idx in range(self._num_items):
                self._trait[item_idx] = nc.PERSONALITY_AGGRESSION_DICTIONARY[value[item_idx]]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)


    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return ['extremely uncharacteristic of me', 'uncharacteristic of me','neither characteristic nor uncharacteristic of me','characteristic of me', 'extremely characteristic of me']

    def get_likert_question(self):
        return "rate how characteristic this is your character"
# -----------------------------------------------------------------------------------------------------------------------


class Panas20(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 20
        self._test_type = nc.PERSONALITY_TEST.PANAS_20
        self._personality_makeup = nc.PersonalityAffect
        self._likert_range = 5
        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) == self._num_items:
            self._trait = [nc.PERSONALITY_AFFECT.UNKNOWN_AFFECT.value] * self._num_items
            for item_idx in range(self._num_items):
                self._trait[item_idx] = nc.PERSONALITY_AFFECT_DICTIONARY[value[item_idx]]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)


    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return ['very slightly or not at all', 'a little','moderately','quite a bit', 'extremely']

    def get_likert_question(self):
        return "to what extent does your character feels this way during the week"

# -----------------------------------------------------------------------------------------------------------------------


class Pvqrr57(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 57
        self._test_type = nc.PERSONALITY_TEST.PVQ_RR_57
        self._personality_makeup = nc.PersonalityValue
        self._likert_range = 6
        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) == self._num_items:
            self._trait = [nc.PERSONALITY_VALUE.UNKNOWN_VALUE.value] * self._num_items
            for item_idx in range(self._num_items):
                self._trait[item_idx] = nc.PERSONALITY_VALUE_DICTIONARY[value[item_idx]]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)


    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return ["not like me at all", "not like me", "a little like me", "moderatley like me", "like me", "very much like me"]

    def get_likert_question(self):
        return "The statment briefly describe a different character. Based on that statment of the diffrent character, think about how much that character is or is not like you."

# -----------------------------------------------------------------------------------------------------------------------


class Sscs11(TraitBasedPersonalityTest):
    def __init__(self):
        self._item = []
        self._direction = []
        self._trait = []
        self._num_items = 11
        self._test_type = nc.PERSONALITY_TEST.SSCS_11
        self._personality_makeup = nc.PersonalityCreative
        self._likert_range = 5
        return

    @property
    def trait(self) -> list:
        return self._trait

    @trait.setter
    def trait(self, value: list):
        if len(value) == self._num_items:
            self._trait = [nc.PERSONALITY_CREATIVE.UNKNOWN_CREATIVE.value] * self._num_items
            for item_idx in range(self._num_items):
                self._trait[item_idx] = nc.PERSONALITY_CREATIVE_DICTIONARY[value[item_idx]]

    @trait.deleter
    def trait(self):
        del self._trait

    def evaluate_personality_trait(self, likert_test_responses: list):
        return super().evaluate_personality_trait(likert_test_responses)


    def get_column_names(self) -> list:
        return super().get_column_names()

    def to_string(self):
        return super().to_string()

    def get_likert_responses(self):
        return ['definitely not', 'somewhat not','neither yes or no','somewhat yes', 'definitely yes']

    def get_likert_question(self):
        return "Decide to what extent the statement describes you"
