import abc
import os

import fall_23.scripts.LangchainInterface as LCI
import fall_23.scripts.notebook_commons as nc

from langchain.llms import GPT4All, OpenAIChat, OpenAI
from langchain.prompts import PromptTemplate
from langchain.llms.bedrock import Bedrock


class LangchainLlm(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'run') and
                callable(subclass.evaluate_personality_trait)
                )

    @abc.abstractmethod
    def __init__(self, implementation: LCI.LangchainInterface):
        self._implementation = implementation
        self._template = None
        self._prompt = None
        self._llm = None

    @property
    def implementation(self) -> LCI.LangchainInterface:
        return self._implementation

    @implementation.deleter
    def implementation(self):
        del self._implementation

    @property
    def template(self) -> str:
        return self._template

    @template.setter
    def template(self, value: str):
        self._template = value

    @template.deleter
    def template(self):
        del self._template

    @property
    def prompt(self) -> PromptTemplate:
        return self._prompt

    @prompt.setter
    def prompt(self, input_variable: list):
        self._prompt = PromptTemplate(template=self._template, input_variables=input_variable)
        self._implementation.setup_chain(self._llm, self._prompt)

    @prompt.deleter
    def prompt(self):
        del self._prompt


# -----------------------------------------------------------------------------------------------------------------------


class GPT4A_Mistral_OpenOrca(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = GPT4All(model=nc.GPT4ALL_MISTRAL_OPEN_ORCA_MODEL_DIR,
                            callbacks=self.implementation.callback,
                            verbose=False,
                            max_tokens=256)

        # If you want to use a custom model add the backend parameter
        # Check https://docs.gpt4all.io/gpt4all_python.html for supported backends
        # llm = GPT4All(model=local_path, backend="gptj", callbacks=callbacks, verbose=True)

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------


class GPT4A_Falcon(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = GPT4All(model=nc.GPT4ALL_MISTRAL_OPEN_ORCA_MODEL_DIR,
                            callbacks=self.implementation.callback,
                            verbose=False,
                            max_tokens=256)

        # If you want to use a custom model add the backend parameter
        # Check https://docs.gpt4all.io/gpt4all_python.html for supported backends
        # llm = GPT4All(model=local_path, backend="gptj", callbacks=callbacks, verbose=True)

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------


class GPT4A_ORCA_2(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = GPT4All(model=nc.GPT4ALL_ORCA_2_DIR,
                            callbacks=self.implementation.callback,
                            verbose=False,
                            max_tokens=256)

        # If you want to use a custom model add the backend parameter
        # Check https://docs.gpt4all.io/gpt4all_python.html for supported backends
        # llm = GPT4All(model=local_path, backend="gptj", callbacks=callbacks, verbose=True)

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------


class OPENAI_GPT_3_5(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = OpenAIChat(model="gpt-3.5-turbo-0613", max_tokens=256)

        # If you want to use a custom model add the backend parameter
        # Check https://docs.gpt4all.io/gpt4all_python.html for supported backends
        # llm = GPT4All(model=local_path, backend="gptj", callbacks=callbacks, verbose=True)

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------


class OPENAI_DAVINCI(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = OpenAI(model="text-davinci-003", max_tokens=256)

        # If you want to use a custom model add the backend parameter
        # Check https://docs.gpt4all.io/gpt4all_python.html for supported backends
        # llm = GPT4All(model=local_path, backend="gptj", callbacks=callbacks, verbose=True)

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------

class AWS_Anthropic_Claude_v1(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = Bedrock(
            model_id="anthropic.claude-v2",
            client=self._implementation.callback,
            model_kwargs={
                "temperature": 0.7,
                "max_tokens_to_sample": 256,
                "top_p": 0.8,
                "top_k": 250
            },
        )

    def run(self, input_string: str):
        return self._implementation.run(input_string)


# -----------------------------------------------------------------------------------------------------------------------


class AWS_Amazon_Titan_Text_G1_Lite(LangchainLlm):
    def __init__(self, implementation: LCI):
        self._implementation = implementation
        self._llm = Bedrock(
            model_id="amazon.titan-text-lite-v1",
            client=self._implementation.callback,
            model_kwargs={
                "temperature": 0.7,
                "max_tokens_to_sample": 256,
                "top_p": 0.8,
                "top_k": 250
            },
        )

    def run(self, input_string: str):
        return self._implementation.run(input_string)
