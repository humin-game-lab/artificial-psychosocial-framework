import random

import numpy as np
import pandas as pd


class SyntheticPopulation:

    def __init__(self):
        self.profile_list = None
        self.replicate = None
        self.response_prompts = None
        self.response_values = None
        pass

    def SetPopulationPrompts(self, personality_profile_centers: np.ndarray, replicas: int, question_items: list, likert_responses: list, likert_question: str):
        self.profile_list = personality_profile_centers
        self.replicate = replicas

        test_instructions = ["Considering the statement, ", "Thinking about the statement, ",
                             "Reflecting on the statement, ", "Evaluating the statement, ", "Regarding the statement, "]

        total_population_size = self.replicate * len(self.profile_list)
        self.response_prompts = [[""] * len(question_items) for profile in range(total_population_size)]
        self.response_values = [[0] * len(question_items) for profile in range(total_population_size)]

        for response_idx in range(total_population_size):
            profile_idx = response_idx // self.replicate
            for item_idx in range(len(question_items)):
                personality_instruction = "For the following task, I want you to pretend to be human who response in a way that matches this Five Factor Personality: "
                personality_description = f"(O:{self.profile_list[profile_idx][0]}, C:{self.profile_list[profile_idx][1]}, E:{self.profile_list[profile_idx][2]}, A:{self.profile_list[profile_idx][3]}, N:{self.profile_list[profile_idx][4]})."
                test_instruction = random.choice(test_instructions)
                item = f"'{question_items[item_idx]}',"
                item_postamble = f"{likert_question} on a scale from 1 to 5 (where 1 = '{likert_responses[0]}', 2 = '{likert_responses[1]}', 3 = '{likert_responses[2]}', 4 = '{likert_responses[3]}', and 5 = '{likert_responses[4]}'). Respond with 'i would rate this as X (likert rating description).'"
                self.response_prompts[response_idx][
                    item_idx] = personality_instruction + personality_description + test_instruction + item + item_postamble

    def GetPrompt(self, response_idx: int, question_idx: int):
        return self.response_prompts[response_idx][question_idx]

    def GetNumResponses(self):
        return len(self.response_prompts)

    def GetNumQuestions(self):
        return len(self.response_prompts[0])

    def GetListOfIndexPairs(self):
        pair_index = []
        for response_idx in range(len(self.response_prompts)):
            for question_idx in range(len(self.response_prompts[0])):
                pair_index.append((response_idx, question_idx))
        return pair_index

    def SaveResponse(self, response_idx: int, question_idx: int, response: int):
        self.response_values[response_idx][question_idx] = response

    def GetResponseDf(self):
        temp_list = self.response_values.copy()
        index_list = [f"I{str(x)}" for x in range(0, len(self.response_values[0]))]

        return pd.DataFrame(temp_list, columns=index_list, copy=True)
