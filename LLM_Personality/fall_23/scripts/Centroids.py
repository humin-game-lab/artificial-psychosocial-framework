import pandas as pd
import numpy as np

import fall_23.scripts.notebook_commons as nc
class Centroids:

    def __init__(self):
        self.centers = np.zeros((nc.PersonalityProfile.LENGTH.value,
                                 nc.PersonalityFactor.LENGTH.value),
                                dtype=float)
        self.rgb = np.zeros((nc.PersonalityProfile.LENGTH.value, 3), dtype=float)
        self.names = [None] * nc.PersonalityProfile.LENGTH.value
        self.hex = [None] * nc.PersonalityProfile.LENGTH.value
        self.profile_type = [None] * nc.PersonalityProfile.LENGTH.value

        for profile_idx in range(0, nc.PersonalityProfile.LENGTH.value):
            self.names[profile_idx] = nc.PersonalityProfile(profile_idx).name
            self.centers[profile_idx] = nc.OCEAN_PROFILES[profile_idx]
            self.hex[profile_idx] = nc.LABEL_COLORS_HEX[profile_idx]
            self.rgb[profile_idx] = nc.LABEL_COLORS_RGP[profile_idx]
            self.profile_type[profile_idx] = nc.PersonalityProfile(profile_idx)

    def GetCenter(self, personality_profile: nc.PersonalityProfile):
        return self.centers[personality_profile.value]

    def GetCenterList(self):
        return self.centers

    def GetName(self, personality_profile: nc.PersonalityProfile):
        return self.names[personality_profile.value]

    def GetHexCode(self,personality_profile: nc.PersonalityProfile):
        return self.hex[personality_profile.value]

    def GetRGBCode(self, personality_profile: nc.PersonalityProfile):
        return self.rgb[personality_profile.value]

    def GetNumCentroids(self):
        return len(self.names)

    def GetDataframe(self):
        return pd.DataFrame(self.centers,
                            index=self.names,
                            columns=["center"])






