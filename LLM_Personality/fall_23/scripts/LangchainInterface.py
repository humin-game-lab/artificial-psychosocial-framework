import abc
import os
import json
import os
import sys
import boto3
from typing import Any

from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.chains import LLMChain
from langchain.prompts import PromptTemplate
from botocore.config import Config

module_path = ".."
sys.path.append(os.path.abspath(module_path))


class LangchainInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'setup_chain') and
                callable(subclass.evaluate_personality_trait) and

                hasattr(subclass, 'run') and
                callable(subclass.evaluate_personality_trait)
                )

    @abc.abstractmethod
    def __init__(self):
        self._api_key = None
        self._org_key = None
        self._llm_chain = None
        self._callback = None


    @property
    def api_key(self) -> str:
        return self._api_key

    @api_key.deleter
    def api_key(self):
        del self._api_key

    @property
    def org_key(self) -> str:
        return self._org_key

    @org_key.deleter
    def org_key(self):
        del self._org_key

    @property
    def callback(self):
        return self._callback

    @abc.abstractmethod
    def setup_chain(self, llm, prompt: PromptTemplate):
        self._llm_chain = LLMChain(prompt=prompt, llm=llm)

    @abc.abstractmethod
    def run(self, input_variables: list) -> str:
        return self._llm_chain.run(input_variables)


# -----------------------------------------------------------------------------------------------------------------------

class LC_GPT4All(LangchainInterface):
    def __init__(self):
        self._api_key = None
        self._org_key = None
        self._llm_chain = None
        self._callback = [StreamingStdOutCallbackHandler()]

    def setup_chain(self, llm, prompt: PromptTemplate):
        self._llm_chain = LLMChain(prompt=prompt, llm=llm)

    def run(self, input_variables: list) -> str:
        return self._llm_chain.run(input_variables)


# -----------------------------------------------------------------------------------------------------------------------


class LC_OpenAi(LangchainInterface):
    def __init__(self):
        self._api_key = os.environ.get("OPENAI_API_KEY")
        self._org_key = os.environ.get("OPENAI_ORGANIZATION")
        self._llm_chain = None
        self._callback = [StreamingStdOutCallbackHandler()]

    def setup_chain(self, llm, prompt: PromptTemplate):
        self._llm_chain = LLMChain(prompt=prompt, llm=llm)

    def run(self, input_variables: list) -> str:
        return self._llm_chain.run(input_variables)

# -----------------------------------------------------------------------------------------------------------------------

class LC_AWS(LangchainInterface):
    def __init__(self):
        self._api_key = os.getenv("AWS_ACCESS_KEY_ID")
        self._org_key = os.getenv("AWS_SECRET_ACCESS_KEY")
        self._llm_chain = None
        #self._callback = [StreamingStdOutCallbackHandler()]
        self._region_name = os.environ.get("AWS_DEFAULT_REGION")
        self._profile_name = "JakeKlinkert"

        self._session = boto3.session.Session(
            aws_access_key_id=self._api_key,
            aws_secret_access_key=self._org_key,
            region_name=self._region_name
        )

        retry_config = Config(
            retries={
                'max_attempts': 10,
                'mode': 'standard'
            }
        )

        self._callback = self._session.client("bedrock-runtime", config=retry_config)

    def setup_chain(self, llm, prompt: PromptTemplate):
        self._llm_chain = LLMChain(prompt=prompt, llm=llm)

    def run(self, input_variables: list) -> str:
        return self._llm_chain.run(input_variables)