import numpy as np
import pandas as pd
import statsmodels.api as sm
from factor_analyzer import FactorAnalyzer
from scipy.stats import pearsonr

import fall_23.scripts.notebook_commons as nc
from reliabilipy import reliability_analysis

import os
os.environ['R_HOME'] = r'C:\Users\38977332\AppData\Local\Programs\R\R-4.3.2'

import rpy2
import rpy2.robjects.packages as rpackages
import rpy2.robjects.numpy2ri
import rpy2.robjects.pandas2ri
from rpy2.robjects.packages import data
utils = rpackages.importr('utils')
utils.chooseCRANmirror(ind=1)

utils.install_packages('base')
utils.install_packages('psych')
utils.install_packages('GPArotation')


def EvaluateOceanTestResults(np_matrix_with_300_questions):
    # Each question has a direction, 1 or -1. If the direction is positive (1) then that means each response is
    # evaluated as is (1, 2, 3, 4, 5). However, if the direction is negative (-1), then each response is reversed
    # from the initial order (5, 4, 3, 2, 1). Thus, if a negative question was responded with a 5, the evaluation
    # response is 1.

    # To perform this calculation, we can simply do intercept - response, which will provide us with the correct
    # negative evaluated response, but then all positive evaluated responses are negative. Given the list of
    # directions, we can multiply that with -1, and multiply the evaluated list thus far with that to get the
    # proper evaluated responses.

    # question_direction = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    #                      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    # question_subtract = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # np_responses_eval = np.subtract(question_subtract, np_matrix_with_300_questions)
    # np_responses_eval = np.multiply(np_responses_eval, np.multiply(question_direction, -1))

    np_responses_eval = np_matrix_with_300_questions

    np_ocean_eval = np.zeros((np_responses_eval.shape[0], 5), dtype=int)

    # print(np_responses_eval)

    # Each row in the np_response is one entire test we need to evaluate
    row_id = 0
    for row_list in np_responses_eval:
        col_id = 0
        factor = -1
        NEOAC_eval_int = [0, 0, 0, 0, 0]

        # Each column assosiates to a question response from the test. There are 10 questsions per factor,
        # and there are 5 factors. To evaluate one factor, it is the summation of all questions. Thus the smallest
        # a single factor can be is 10, and the largest it can be is 50.
        # To calculate this, we will keep track of 10 questions that have been answered. All evaluations will be
        # added to it's associated factor. Once the 10th question has been answered, we will increment the factor
        # and continue the process so that the next 10 questions will be added to the next factor.

        for col_element in row_list:
            factor = col_id % 5
            val = col_element
            NEOAC_eval_int[factor] += val
            col_id += 1

        # Reorganize ENACO to OCEAN
        OCEAN_eval_int = [NEOAC_eval_int[2],
                          NEOAC_eval_int[4],
                          NEOAC_eval_int[1],
                          NEOAC_eval_int[3],
                          NEOAC_eval_int[0]
                          ]

        for factor_idx in range(0, 5):
            np_ocean_eval[row_id, factor_idx] = OCEAN_eval_int[factor_idx]
        row_id += 1

    return np.interp(np_ocean_eval, xp=[60, 120, 180, 240, 300], fp=[0.0, 0.25, 0.50, 0.75, 1.0])


def calc_CSCF(ocean_matrix):
    # To graph these values in the same way as the study, we will need to calculate the Cognitive Stability
    # (ALPHA = Agreeableness + Conscientiousness + Emotional Stability) and Cognitive Flexability (BETA = Extraversion
    # + Openness)

    np_cscf = np.zeros((ocean_matrix.shape[0], 2), dtype=float)

    row_idx = 0
    for row_list in ocean_matrix:
        O = row_list[0]
        C = row_list[1]
        E = row_list[2]
        A = row_list[3]
        N = row_list[4]

        ALPHA = A + C + (1.0 - N)
        BETA = E + O

        np_cscf[row_idx, 0] = ALPHA
        np_cscf[row_idx, 1] = BETA

        row_idx += 1

    return np_cscf


def GetPersonalityProfileLabels(ocean_matrix):
    np_distance_from_eval_to_profile = np.zeros((ocean_matrix.shape[0], 20), dtype=float)
    np_label = np.zeros(ocean_matrix.shape[0], dtype=float)

    row_idx = 0
    for row_OCEAN in ocean_matrix:
        col_idx = 0
        for profile_OCEAN in nc.OCEAN_PROFILES:
            distance = np.linalg.norm(profile_OCEAN - row_OCEAN)
            np_distance_from_eval_to_profile[row_idx, col_idx] = distance
            col_idx += 1
        row_idx += 1

    row_idx = 0
    for row_distances in np_distance_from_eval_to_profile:
        np_label[row_idx] = np.argmin(row_distances)
        row_idx += 1

    return np_label


def CreateFullInfoDf(np_matrix_with_50_questions, np_ocean=None, np_cscf=None, np_label=None):
    # Combine np_responses_clean, np_OCEAN, np_CSCF, and np_label

    if np.any(np_ocean) == False:
        np_ocean = EvaluateOceanTestResults(np_matrix_with_50_questions)

    if np.any(np_cscf) == False:
        np_cscf = calc_CSCF(np_ocean)

    if np.any(np_label) == False:
        np_label = GetPersonalityProfileLabels(np_ocean)

    np_all_infromation = np.concatenate(
        (np_matrix_with_50_questions, np_ocean, np_cscf, np.reshape(np_label, (np_label.shape[0], 1))), axis=1)

    df_all_infromation = pd.DataFrame(np_all_infromation, columns=[
        "E1",
        "E2",
        "E3",
        "E4",
        "E5",
        "E6",
        "E7",
        "E8",
        "E9",
        "E10",
        "N1",
        "N2",
        "N3",
        "N4",
        "N5",
        "N6",
        "N7",
        "N8",
        "N9",
        "N10",
        "A1",
        "A2",
        "A3",
        "A4",
        "A5",
        "A6",
        "A7",
        "A8",
        "A9",
        "A10",
        "C1",
        "C2",
        "C3",
        "C4",
        "C5",
        "C6",
        "C7",
        "C8",
        "C9",
        "C10",
        "O1",
        "O2",
        "O3",
        "O4",
        "O5",
        "O6",
        "O7",
        "O8",
        "O9",
        "O10",
        "Openness",
        "Conscientiousness",
        "Extraversion",
        "Agreeableness",
        "Neuroticism",
        "Alpha",
        "Beta",
        "Label"
    ])

    data_types = {
        "E1": "int",
        "E2": "int",
        "E3": "int",
        "E4": "int",
        "E5": "int",
        "E6": "int",
        "E7": "int",
        "E8": "int",
        "E9": "int",
        "E10": "int",
        "N1": "int",
        "N2": "int",
        "N3": "int",
        "N4": "int",
        "N5": "int",
        "N6": "int",
        "N7": "int",
        "N8": "int",
        "N9": "int",
        "N10": "int",
        "A1": "int",
        "A2": "int",
        "A3": "int",
        "A4": "int",
        "A5": "int",
        "A6": "int",
        "A7": "int",
        "A8": "int",
        "A9": "int",
        "A10": "int",
        "C1": "int",
        "C2": "int",
        "C3": "int",
        "C4": "int",
        "C5": "int",
        "C6": "int",
        "C7": "int",
        "C8": "int",
        "C9": "int",
        "C10": "int",
        "O1": "int",
        "O2": "int",
        "O3": "int",
        "O4": "int",
        "O5": "int",
        "O6": "int",
        "O7": "int",
        "O8": "int",
        "O9": "int",
        "O10": "int",
        "Openness": "float",
        "Conscientiousness": "float",
        "Extraversion": "float",
        "Agreeableness": "float",
        "Neuroticism": "float",
        "Alpha": "float",
        "Beta": "float",
        "Label": "int"
    }

    for col_name, dt in data_types.items():
        df_all_infromation[col_name] = df_all_infromation[col_name].astype(dt)

    return df_all_infromation

def linear_mapping(input_value, input_start, input_end, output_start, output_end):
    scale = (output_end - output_start) / (input_end - input_start)
    offset = -input_start * (output_end - output_start) / (input_end - input_start) + output_start
    q = input_value * scale + offset
    return q

def calculate_pearson_product_moment_correlation(primary_evaluated_test_array: np.ndarray, redundant_evaluated_test_array: np.ndarray, sample_size=100000, randomize = True):
    primary_test = primary_evaluated_test_array
    redundant_test = redundant_evaluated_test_array

    if randomize:
        primary_test = primary_evaluated_test_array[np.random.choice(primary_evaluated_test_array.shape[0], sample_size, replace=False), :]
        redundant_test = primary_evaluated_test_array[np.random.choice(primary_evaluated_test_array.shape[0], sample_size, replace=False), :]
    else:
        primary_test = primary_evaluated_test_array[:sample_size, :]
        redundant_test = redundant_evaluated_test_array[:sample_size, :]

    primary_test = primary_test.transpose()
    redundant_test = redundant_test.transpose()

    correlation_matrix = [[-1.0]]

    if np.shape(primary_test)[0] == np.shape(redundant_test)[0]:
        correlation_matrix = np.empty([np.shape(primary_test)[0], np.shape(primary_test)[0]])

    for x_idx in range(np.shape(primary_test)[0]):
        for y_idx in range(np.shape(primary_test)[0]):
            x_mean = np.mean(primary_test[x_idx])
            y_mean = np.mean(redundant_test[y_idx])

            sample_std_x_subtract = np.subtract(primary_test[x_idx], x_mean)
            sample_std_x_pow = np.power(sample_std_x_subtract, 2)
            sample_std_x_pow_sum = np.sum(sample_std_x_pow)

            sample_std_y_subtract = np.subtract(redundant_test[y_idx], y_mean)
            sample_std_y_pow = np.power(sample_std_y_subtract, 2)
            sample_std_y_pow_sum = np.sum(sample_std_y_pow)

            numerator = np.sum(np.multiply(sample_std_x_subtract, sample_std_y_subtract))
            denominator = np.sqrt(sample_std_x_pow_sum * sample_std_y_pow_sum)

            correlation_matrix[x_idx][y_idx] = numerator/denominator

    return correlation_matrix

class BasicStatistics:
    def __init__(self, data: pd.DataFrame, reliability_report):
        self.data = data
        self.n_items = self.data.shape[1] if self.data.ndim != 1 else 0
        self.correlations_matrix = np.corrcoef(self.data, rowvar=False)
        self.reliability_report = reliability_analysis(correlations_matrix=self.correlations_matrix)
        self.reliability_report.fit()

        # Defining the R script and loading the instance in Python

        rpy2.robjects.numpy2ri.activate()
        rpy2.robjects.pandas2ri.activate()
        self.r = rpy2.robjects.r
        self.r['source'](
            r'C:\Users\38977332\OneDrive - Southern Methodist University\Documents\School\APF\Repos\apf\LLM_Personality\fall_23\r\r_llm_personality\sub_psych.R')
        # Loading the function we have defined in R.



    def calculate_cronbach_alpha(self):
        item_coefficient_float = self._calculate_item_coefficient()
        response_variance = self._calculate_response_variance()
        my_result = item_coefficient_float * response_variance

        omega_cronbach_function_r = rpy2.robjects.globalenv['omega_cronbach']
        df_r = rpy2.robjects.pandas2ri.py2rpy(self.data)
        result_r = omega_cronbach_function_r(df_r)

        return result_r[0], my_result


    def calculate_guttmans_lambda_6(self):
        data = self.data.to_numpy()
        sum_squared_errors = 0.0
        for item_idx in range(self.data.shape[1]):
            print(f"index: {item_idx} of {self.data.shape[1]}")
            dependent_var = data[:, item_idx]
            #print(f"dependent_var: {dependent_var}")
            independent_vars = np.delete(data, obj=item_idx, axis=1)
            #print(f"independent_vars: {independent_vars}")
            independent_vars = sm.add_constant(independent_vars)
            #print(f"independent_vars: {independent_vars}")
            model = sm.OLS(dependent_var, independent_vars).fit()
            #print(f"model: {model}")
            sum_squared_errors += (1.0 - model.rsquared)
            #print(f"sum_squared_errors: {sum_squared_errors}")

        total_score_variance = self._calculate_total_variance()
        lambda_6 = 1.0 - (sum_squared_errors / total_score_variance)
        #print(f"lambda_6: {lambda_6}")

        omega_guttmans_function_r = rpy2.robjects.globalenv['omega_guttmans']
        df_r = rpy2.robjects.pandas2ri.py2rpy(self.data)
        result_r = omega_guttmans_function_r(df_r)

        return result_r[0], lambda_6

    def calculate_mcdonalds_hierarchical_omega(self):
        raw_responses = self.data.to_numpy()
        k = raw_responses.shape[1]  # Number of columns (items) in the data
        fa = FactorAnalyzer(rotation=None, n_factors=k)
        fa.fit(raw_responses)
        loadings = fa.loadings_
        item_variances = np.var(raw_responses, ddof=1, axis=0)  # Variance of each item
        total_variance = np.sum(item_variances)
        r_tt_1 = (k / (k - 1.0))
        r_tt_2 = (1.0 - np.sum(item_variances) / total_variance)
        r_tt = r_tt_1 * r_tt_2

        omega_numerator = np.sum((loadings ** 2) / item_variances) / k
        omega_denominator = (np.sum((loadings ** 2) / item_variances) / (k - 1)) - (1 / k) * (1 / (1 - r_tt ** 2))
        omega_h = omega_numerator / omega_denominator

        omega_mcdonalds_function_r = rpy2.robjects.globalenv['omega_mcdonalds']
        df_r = rpy2.robjects.pandas2ri.py2rpy(self.data)
        result_r = omega_mcdonalds_function_r(df_r)

        return result_r[0], omega_h

    def _calculate_item_variances(self):
        return np.var(self.data, axis=1)

    def _calculate_total_item_variance(self):
        variance = np.var(self.data, axis=0)
        return np.sum(variance)

    def _calculate_total_variance(self):
        summation = np.sum(self.data, axis=1)
        return np.var(summation)

    def _calculate_response_variance(self):
        summation_item_variance_float = self._calculate_total_item_variance()
        total_variance_float = self._calculate_total_variance()
        return (total_variance_float - summation_item_variance_float)/ total_variance_float

    def _calculate_item_coefficient(self):
        return (float(self.n_items))/(float(self.n_items) - 1.0)

    def _calculate_squared_multiple_correlations(self):
        r_smc_squared = []
        for i in range(self.n_items):
            reduced_matrix = np.delete(np.delete(self.correlations_matrix, i, axis=0), i, axis=1)
            r_smc = np.sum(reduced_matrix) / (self.n_items - 1)
            r_smc_squared.append(r_smc ** 2)

        return np.array(r_smc_squared)

