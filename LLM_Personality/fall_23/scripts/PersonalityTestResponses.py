from abc import ABC, abstractmethod

from typing import Any

import fall_23.scripts.TraitBasedPersonalityTest as TraitBasedPersonalityTest
import fall_23.scripts.notebook_commons as nc
import fall_23.scripts.personality_tools as personality_tools
from fall_23.scripts.Centroids import Centroids

import pandas as pd
import numpy as np

import holoviews as hv
hv.extension('matplotlib')

from sctriangulate.colors import build_custom_continuous_cmap
from holoviews import opts, dim
from reliabilipy import reliability_analysis

class PersonalityTestResponses(ABC):
    _implementation = None
    _test_responses = None
    _test_evaluation = None
    _test_similarity = None

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'init_test_key') and
                callable(subclass.init_test_key) and

                hasattr(subclass, 'init_raw_responses_dir') and
                callable(subclass.init_raw_responses_dir) and

                hasattr(subclass, 'init_raw_responses_pd') and
                callable(subclass.init_raw_responses_pd) and

                hasattr(subclass, 'eval_test_responses') and
                callable(subclass.eval_test_responses) and

                hasattr(subclass, 'get_index_list') and
                callable(subclass.get_index_list) and

                hasattr(subclass, 'get_item_list') and
                callable(subclass.get_item_list) and

                hasattr(subclass, 'get_dataframe') and
                callable(subclass.get_dataframe) and

                hasattr(subclass, 'calc_interrater_reliability') and
                callable(subclass.calc_interrater_reliability) and

                hasattr(subclass, 'root_mean_squared_error_point') and
                callable(subclass.root_mean_squared_error_point) and

                hasattr(subclass, 'root_mean_squared_error_vector') and
                callable(subclass.root_mean_squared_error_vector) and

                hasattr(subclass, 'root_mean_squared_error_cosine') and
                callable(subclass.root_mean_squared_error_cosine) and

                hasattr(subclass, 'save') and
                callable(subclass.save) and

                hasattr(subclass, 'load') and
                callable(subclass.load)
                )

    @abstractmethod
    def __init__(self, implementation: TraitBasedPersonalityTest):
        self._implementation = implementation

    @property
    def implementation(self) -> TraitBasedPersonalityTest:
        return self._implementation

    @implementation.setter
    def implementation(self, value: TraitBasedPersonalityTest):
        self._implementation = value

    @implementation.deleter
    def implementation(self):
        del self._implementation

    @property
    def test_responses(self) -> list[Any]:
        return self._test_responses

    @test_responses.setter
    def test_responses(self, value: list[Any]):
        self._test_responses = value

    @test_responses.deleter
    def test_responses(self):
        del self._test_responses

    @property
    def test_evaluation(self) -> list[Any]:
        return self._test_evaluation

    @test_evaluation.setter
    def test_evaluation(self, value: list[Any]):
        self._test_evaluation = value

    @test_evaluation.deleter
    def test_evaluation(self):
        del self._test_evaluation

    @property
    def test_similarity(self) -> list[Any]:
        return self._test_similarity

    @test_similarity.setter
    def test_similarity(self, value: list[Any]):
        self._test_similarity = value

    @test_similarity.deleter
    def test_similarity(self):
        del self._test_similarity

    @abstractmethod
    def init_test_key(self, string_key_dir: str, delimiter=',') -> bool:
        df_test_key = pd.read_csv(string_key_dir, delimiter=delimiter, header=0, dtype={'idx':'int8', 'Sign':'int8', 'Key':'str', 'Facet':'str', 'Item':'str'})
        #print(df_test_key)

        row_count = len(df_test_key)

        if row_count != self._implementation.num_items:
            return False

        self._implementation.item = df_test_key['Item'].tolist().copy()
        self._implementation.direction = df_test_key['Sign'].tolist().copy()
        self._implementation.trait = df_test_key['Key'].tolist().copy()
        return True

    @abstractmethod
    def init_raw_responses_dir(self, string_raw_responses_dir: str, delimiter=',') -> bool:
        df_test_responses = pd.read_csv(string_raw_responses_dir, delimiter=delimiter, header=0)
        self._test_responses = df_test_responses[self._implementation.get_column_names()].to_numpy(dtype=np.int32)
        return True

    @abstractmethod
    def init_raw_responses_pd(self, dataframe: pd.DataFrame) -> bool:
        self._test_responses = dataframe[self._implementation.get_column_names()].to_numpy(dtype=np.int32)
        return True

    @abstractmethod
    def eval_test_responses(self) -> bool:
        print("Evaluating test responses:")
        self._test_evaluation = np.zeros((len(self._test_responses), self.implementation.personality_makeup.LENGTH.value))
        for response_idx in range(len(self._test_responses)):
            self._test_evaluation[response_idx] = self._implementation.evaluate_personality_trait(
                self._test_responses[response_idx])
        return True

    @abstractmethod
    def get_index_list(self) -> list:
        return [*range(self._test_responses.shape[0])]

    @abstractmethod
    def get_item_list(self):
        return self._implementation.item

    @abstractmethod
    def get_dataframe(self, responses: bool, evaluation: bool, cscf: bool, labels=None) -> pd.DataFrame:
        pass

    @abstractmethod
    def calc_interrater_reliability(self, profile: nc.PersonalityProfile) -> float:
        '''
        total_similar_responses = 0
        total_responses_reviewed = 0
        label_list = self.get_top_k_closest_centroid_list(1)

        for x_response_idx in range(len(self._test_responses)):
            if label_list[x_response_idx] != profile.value:
                continue

            np_r1 = self._test_responses[x_response_idx]
            for y_response_id in range(x_response_idx + 1, len(self._test_responses)):
                np_r2 = self._test_responses[y_response_id]

                test_similarity = (np_r1 == np_r2).astype(int)
                num_similar = np.sum(test_similarity)

                total_similar_responses += num_similar
                total_responses_reviewed += self._implementation.num_items

        return total_similar_responses / total_responses_reviewed
        '''
        return -1.0


    @abstractmethod
    def root_mean_squared_error_point(self, profiles: Centroids, profile_type: nc.PersonalityProfile) -> float:
        ''''
        label_list = self.get_top_k_closest_centroid_list(1)

        total_error = []
        for x_response_idx in range(len(self._test_evaluation)):
            if label_list[x_response_idx] != profile_type.value:
                continue
            element_difference = np.subtract(self._test_evaluation[x_response_idx], profiles.GetCenter(profile_type))
            element_sq = element_difference ** 2.0
            total_error.append(element_sq)

        print(total_error)
        MSPE = np.mean(total_error, axis=0)
        RMSPE = np.sqrt(MSPE)
        return RMSPE
        '''
        return -1.0

    @abstractmethod
    def root_mean_squared_error_vector(self) -> list:
        return []

    @abstractmethod
    def root_mean_squared_error_cosine(self) -> list:
        return []

    @abstractmethod
    def save(self, full_file_dir: str, response_set_name: str):
        np.save(f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_responses",
                self._test_responses, allow_pickle=True)
        np.save(f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_evaluation",
                self._test_evaluation, allow_pickle=True)
        np.save(f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_similarity",
                self._test_similarity, allow_pickle=True)

    @abstractmethod
    def load(self, full_file_dir: str, response_set_name: str):
        self._test_responses = np.load(
            f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_responses.npy",
            allow_pickle=True)
        self._test_evaluation = np.load(
            f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_evaluation.npy",
            allow_pickle=True)
        self._test_similarity = np.load(
            f"{full_file_dir}\\{self._implementation.test_type}\\{response_set_name}\\test_similarity.npy",
            allow_pickle=True)

# -----------------------------------------------------------------------------------------------------------------------


class PersonalityFactorResponseSet(PersonalityTestResponses):

    def __init__(self, implementation: TraitBasedPersonalityTest):
        self._implementation = implementation

    def init_test_key(self, string_key_dir: str, delimiter=',') -> bool:
        return super().init_test_key(string_key_dir, delimiter)

    def init_raw_responses_dir(self, string_raw_responses_dir: str, delimiter=',') -> bool:
        return super().init_raw_responses_dir(string_raw_responses_dir, delimiter)

    def init_raw_responses_pd(self, dataframe: pd.DataFrame) -> bool:
        return super().init_raw_responses_pd(dataframe)

    def eval_test_responses(self) -> bool:
        return super().eval_test_responses()

    def get_index_list(self) -> list:
        return super().get_index_list()


    def get_item_list(self):
        return super().get_item_list()

    def calc_interrater_reliability(self, profile: nc.PersonalityProfile) -> float:
        return super().calc_interrater_reliability(profile)

    def root_mean_squared_error_point(self, profiles: Centroids, profile_type: nc.PersonalityProfile) -> float:
        return super().root_mean_squared_error_point(profiles, profile_type)

    def root_mean_squared_error_vector(self) -> list:
        return super().root_mean_squared_error_vector()

    def root_mean_squared_error_cosine(self) -> list:
        return super().root_mean_squared_error_cosine()

    def get_dataframe(self, responses: bool, evaluation: bool, cscf: bool, labels=None, columns=None) -> pd.DataFrame:
        temp_list = None
        index_list = None

        if responses:
            if isinstance(columns, np.ndarray):
                spliced_list = columns.tolist()
                temp_list = self._test_responses[:, spliced_list].copy()
                index_list = [f"I{str(x)}" for x in range(0, len(spliced_list))]
            else:
                temp_list = self._test_responses.copy()
                index_list = [f"I{str(x)}" for x in range(0, self._implementation.num_items)]

        if evaluation:
            if not isinstance(temp_list, np.ndarray):
                temp_list = self._test_evaluation.copy()
                index_list = [e.name for e in self._implementation._personality_makeup]
                index_list.pop(0)
                index_list.pop(len(index_list) - 1)
            else:
                temp_list = np.append(temp_list, self._test_evaluation.copy(), axis=1)
                traits_list = [e.name for e in self._implementation._personality_makeup]
                traits_list.pop(0)
                traits_list.pop(len(traits_list) - 1)
                index_list = np.append(index_list, np.array(traits_list), axis=0)

        if cscf:
           cscf_list = self.get_cscf_list()
           if not isinstance(temp_list, np.ndarray):
               temp_list = cscf_list.copy()
               index_list = ["Alpha", "Beta"]
           else:
               temp_list = np.append(temp_list, cscf_list.copy(), axis=1)
               index_list = np.append(index_list, np.array(["Alpha", "Beta"]), axis=0)

        if isinstance(labels, np.ndarray):
            if not isinstance(temp_list, np.ndarray):
                temp_list = labels.copy()
                index_list = [f"l{str(x)}" for x in range(labels.shape[1])]
            else:
                temp_list = np.append(temp_list, labels.copy(), axis=1)
                index_list = np.append(index_list, np.array([f"l{str(x)}" for x in range(labels.shape[1])]), axis=0)

        return pd.DataFrame(temp_list, columns=index_list, copy=True)

    def save(self, full_file_dir: str, response_set_name: str):
        return super().save(full_file_dir, response_set_name)

    def load(self, full_file_dir: str, response_set_name: str):
        return super().load(full_file_dir, response_set_name)

    def get_cscf_list(self) -> list:
        if not self._test_evaluation.any():
            return []

        responses_cscf = np.zeros((len(self._test_evaluation), 2))
        for row_idx in range(0, len(self._test_evaluation)):
            O = self._test_evaluation[row_idx][0]
            C = self._test_evaluation[row_idx][1]
            E = self._test_evaluation[row_idx][2]
            A = self._test_evaluation[row_idx][3]
            N = self._test_evaluation[row_idx][4]

            ALPHA = A + C + (1.0 - N)
            BETA = E + O

            responses_cscf[row_idx, 0] = ALPHA
            responses_cscf[row_idx, 1] = BETA

            row_idx += 1

        return responses_cscf

    def eval_centroid_similarity(self, profiles: Centroids):
        self._test_similarity = np.zeros((len(self._test_responses), profiles.GetNumCentroids()))

        for evaluation_idx in range(len(self._test_evaluation)):
            evaluation_loc = self._test_evaluation[evaluation_idx]
            for profile_idx in range(nc.PersonalityProfile.LENGTH.value):
                profile_loc = profiles.GetCenter(nc.PersonalityProfile(profile_idx))
                distance = np.linalg.norm(profile_loc - evaluation_loc)
                self._test_similarity[evaluation_idx][profile_idx] = distance

    def get_top_k_closest_centroid_list(self, k: int) -> list:
        top_k_closest = np.zeros((len(self._test_responses), k))

        for test_idx in range(len(self._test_similarity)):
            similarity_sorted = list(self._test_similarity[test_idx].copy())
            similarity_temp = list(self._test_similarity[test_idx].copy())
            similarity_sorted.sort()

            k_idx = 0
            for value in similarity_sorted:
                if k_idx >= k:
                    break
                top_k_closest[test_idx][k_idx] = similarity_temp.index(value)
                similarity_temp[similarity_temp.index(value)] = -1.0
                k_idx += 1
        return top_k_closest

    def get_weighted_centroids_list(self) -> list:
        return []

    def scatter_plot_cscf(self, show_profile_centers: bool):

        df_cscf = self.get_dataframe(False, False, True, self.get_top_k_closest_centroid_list(1))
        cscf_img = hv.Scatter(
            data=df_cscf, kdims=["Beta"], vdims=["Alpha", "l0"]
        ).opts(color='l0',
               cmap=nc.LABEL_COLORS_HEX,
               s=10.0,
               alpha=0.1,
               marker='o')

        output_layout = cscf_img

        if show_profile_centers:
            profile_names = [e.name for e in nc.PersonalityProfile]
            profile_names = np.delete(profile_names, 0)
            profile_names = np.delete(profile_names, profile_names.shape[0] - 1)

            np_profile_centers = np.array(nc.OCEAN_PROFILES)
            np_profile_cscf = personality_tools.calc_CSCF(np_profile_centers)
            np_profile_names = np.array([e for e in range(0, 20)])
            np_profile_names = np.reshape(np_profile_names, (20, 1))

            np_profile = np.append(np_profile_cscf, np_profile_names, axis=1)
            df_profiles = pd.DataFrame(np_profile,
                                       columns=["Alpha", "Beta", "Label"])

            profile_eval_imgs = [hv.Scatter(
                data=df_profiles[df_profiles.Label == label_idx], kdims=["Beta"], vdims=["Alpha", "Label"]).opts(
                color='Label',
                cmap=[nc.LABEL_COLORS_HEX[10]],
                s=300,
                alpha=1.0,
                marker='X',
                fontsize={'title': 50,
                          'labels': 25,
                          'xticks': 20,
                          'yticks': 20}) for label_idx in [e for e in range(0, 20)]]

            label_eval_imgs = [
                hv.Labels(df_profiles[df_profiles.Label == label_idx], ["Beta", "Alpha"], ["Label"]).opts(
                    cmap=[nc.LABEL_COLORS_HEX[10]]) for label_idx in [e for e in range(0, 20)]]

            output_layout *= (profile_eval_imgs[0] * label_eval_imgs[0])
            for idx in range(1, 20):
                output_layout *= profile_eval_imgs[idx] * label_eval_imgs[idx]

            output_layout.opts(opts.Labels(color="Label", cmap=build_custom_continuous_cmap(nc.LABEL_COLORS_RGP[10],
                                                                                            nc.LABEL_COLORS_RGP[10]),
                                           xoffset=0.0, yoffset=0.025, size=24, padding=0.025))

        return output_layout.opts(fig_inches=10, fig_bounds=(0, 0, 1, 1))

    def scatter_plot_lda(self, DisplayCentroids: bool, SeperateByCentroids: bool):
        return

    def cronbach_alpha_breakdown(self):
        #Generate the sub_traits
        max_category = max(self.implementation.trait)
        sub_trait_index = [[] for _ in range(max_category + 1)]
        for index, category in enumerate(self.implementation.trait):
            sub_trait_index[category].append(index)
        values = [[0.0] for _ in range(max_category + 1)]
        for sub_trait_value in range(len(sub_trait_index)):
            data = self.get_dataframe(responses=True, evaluation=False,cscf=False,
                                      columns=np.array(sub_trait_index[sub_trait_value]))
            stats = personality_tools.BasicStatistics(data= data, reliability_report=None)
            module_result, my_result = stats.calculate_cronbach_alpha()
            values[sub_trait_value] = module_result
            print(f"module_result: {module_result}, my_result: {my_result}")
        return values

    def lambda_6_breakdown(self):
        #Generate the sub_traits
        max_category = max(self.implementation.trait)
        sub_trait_index = [[] for _ in range(max_category + 1)]
        for index, category in enumerate(self.implementation.trait):
            sub_trait_index[category].append(index)
        values = [[0.0] for _ in range(max_category + 1)]
        for sub_trait_value in range(len(sub_trait_index)):
            stats = personality_tools.BasicStatistics(data=self.get_dataframe(responses=True, evaluation=False, cscf=False,
                                                                         columns=np.array(sub_trait_index[sub_trait_value])), reliability_report=None)
            module_result, my_result = stats.calculate_guttmans_lambda_6()
            values[sub_trait_value] = module_result
            print(f"module_result: {module_result}, my_result: {my_result}")
        return values

    def hierarchical_omega_breakdown(self):
        # Generate the sub_traits
        max_category = max(self.implementation.trait)
        sub_trait_index = [[] for _ in range(max_category + 1)]
        for index, category in enumerate(self.implementation.trait):
            sub_trait_index[category].append(index)

        values = [[0.0] for _ in range(max_category + 1)]
        for sub_trait_value in range(len(sub_trait_index)):
            stats = personality_tools.BasicStatistics(
                data=self.get_dataframe(responses=True, evaluation=False, cscf=False, columns=np.array(sub_trait_index[sub_trait_value])),
                reliability_report=None
            )
            module_result, my_result = stats.calculate_mcdonalds_hierarchical_omega()
            values[sub_trait_value] = module_result
            print(f"module_result: {module_result}, my_result: {my_result}")
        return values
