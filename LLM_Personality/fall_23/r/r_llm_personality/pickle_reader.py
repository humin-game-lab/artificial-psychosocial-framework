import numpy as np

def read_pickle_file(file):
    pickle_data = np.load(file, allow_pickle=True)
    return pickle_data