library(psych)
library(GPArotation)

omega_cronbach <- function(df){
  omega_report <- omega(df)
  return(omega_report[["alpha"]])
}

omega_guttmans <- function(df){
  omega_report <- omega(df)
  return(omega_report[["G6"]])
}

omega_mcdonalds <- function(df){
  omega_report <- omega(df)
  return(omega_report[["omega_h"]])
}