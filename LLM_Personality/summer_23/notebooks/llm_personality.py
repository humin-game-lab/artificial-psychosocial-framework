import io
import os
import numpy as np
import pandas as pd


from langchain.llms import OpenAI, HuggingFaceHub, GPT4All, HuggingFacePipeline, AI21, CerebriumAI, GPT4All
from langchain import PromptTemplate, LLMChain
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler


#os.environ["OPENAI_API_KEY"] = 
#os.environ["AI21_API_KEY"] = 
#os.environ["HUGGINGFACEHUB_API_TOKEN"] = 
#os.environ["HUGGINGFACEHUB_API_TOKEN"] = 
#os.environ["CEREBRIUMAI_API_KEY"] = 

callbacks = [StreamingStdOutCallbackHandler()]

model_name = ["gpt-3.5-turbo", "text-davinci-003", "gpt-4"]

llm_models = {
    "gpt-3.5-turbo" : 0,
    "text-davinci-003": 1,
    "text-davinci-002": 2,
    "gpt-4": 3,
    "j2-mid": 4,
    "HuggingFace" : 5,
    "flan-t5-xl": 6
    
    #"ggml-gpt4all-j-v1.3-groovy": 6,
    #"ggml-gpt4all-l13b-snoozy": 7,
    #"ggml-mpt-7b-base": 8,
    #"ggml-mpt-7b-chat": 9,
    #"ggml-mpt-7b-instruct": 10,
    #"ggml-nous-gpt4-vicuna-13b": 11,
    #"ggml-stable-vicuna-13B.q4_2": 12,
    #"ggml-v3-13b-hermes-q5_1": 13,
    #"ggml-vicuna-7b-1.1-q4_2": 14,
    #"ggml-vicuna-13b-1.1-q4_2": 15,
    #"ggml-wizard-13b-uncensored": 16,
    #"ggml-wizardLM-7B.q4_2": 17

}



def LangChainModel(api_id):
    match api_id:
        case 0: #OpenAI
            return OpenAI(temperature=0.7, model_name='gpt-3.5-turbo-0613', max_retries=256)
        case 1: #OpenAI
            return OpenAI(temperature=0.7, model_name='text-davinci-003', max_retries=256)
        case 2: #OpenAI
            return OpenAI(temperature=0.7, model_name='text-davinci-002', max_retries=256)
        case 3:
            return OpenAI(temperature=0.7, model_name='gpt-4-0613', max_retries=256)
        
        case 4: #AI21
            return AI21(ai21_api_key= os.environ["AI21_API_KEY"])
        case 5: #HuggingFace
            return HuggingFacePipeline.from_model_id(model_id="bigscience/bloom-1b7", task="text-generation",
                                                     model_kwargs={"temperature": 70, "max_length": 256})
        case 6:
            return CerebriumAI(endpoint_url="https://run.cerebrium.ai/flan-t5-xl-webhook/predict")

        
        #case 6:
        #    return GPT4All(model=r"D:\Other\GPT4All\ggml-gpt4all-j-v1.3-groovy.bin", callbacks=callbacks, verbose=True)
        #
        #case 7:
        #    return GPT4All(model=r"D:\Other\GPT4All\ggml-gpt4all-l13b-snoozy.bin", callbacks=callbacks, verbose=True)
       # 
        #case 8:
        #    return GPT4All(model=r"D:\Other\GPT4All\ggml-mpt-7b-base.bin", callbacks=callbacks, verbose=True)
   # 
   #     case 9:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-mpt-7b-chat.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 10:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-mpt-7b-instruct.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 11:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-nous-gpt4-vicuna-13b.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 12:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-stable-vicuna-13B.q4_2.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 13:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-v3-13b-hermes-q5_1.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 14:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-vicuna-7b-1.1-q4_2.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 15:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-vicuna-13b-1.1-q4_2.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 16:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-wizard-13b-uncensored.bin", callbacks=callbacks, verbose=True)
   #     
   #     case 16:
   #         return GPT4All(model=r"D:\Other\GPT4All\ggml-wizardLM-7B.q4_2.bin", callbacks=callbacks, verbose=True)
   
        case _:
            print("Could not find LLM by that name")



def TemplatePrompt():
    # Define the template prompt we will ask the model
    template = """I want you to roleplay as a character that has the OCEAN personality of the following:
    [Openness = {o}%,
    Conscientiousness = {c}%,	
    Extraversion = {e}%,
    Agreeableness = {a}%,	
    Neuroticism =	{n}%]
                    
    Given the following Likert scale and associated statement: 
    1 = Very inaccurate
    2 = Moderately inaccurate
    3 = Neither inaccurate nor accurate
    4 = Moderately accurate
    5 = Very accurate
    
    I expect as output a csv file, the first column is Index numbering the questions, and then the second column is your response as the character. Here is an example:
    Index	Response
    0, 1
    1, 2
    2, 1
    3, 5
    4, 3
    5, 4
    
    Here is the Test:
    Index, Question
    {questions}
    """

    personality_prompt = PromptTemplate(
        template=template,
        input_variables=['o', 'c', 'e', 'a', 'n', 'questions']
    )

    return personality_prompt



def GetPersonalityProfile():
    profiles_df = pd.read_csv(filepath_or_buffer=r"C:\Users\Jake Klinkert\Documents\School\HumIn Game Lab\APF\Papers\LLM\personality\notebooks\personality\profiles\personality_profiles_percent.csv", encoding='utf-8')
    #celebs_df = pd.read_csv(filepath_or_buffer=r"C:\Users\Jake Klinkert\Documents\School\HumIn Game Lab\APF\Papers\LLM\personality\notebooks\personality\profiles\personality_celebs.csv", encoding='utf-8')

    full_profile_df = pd.DataFrame(columns=['Name', "Openness", "Conscientiousness", "Extraversion", "Agreeableness", "Neuroticism"])

    # Concate Names
    for index, row in profiles_df.iterrows():
        full_profile_df.loc[len(full_profile_df.index)] = [row["Name"], row["Openness"], row["Conscientiousness"], row["Extraversion"], row["Agreeableness"], row["Neuroticism"]]

    #for index, row in celebs_df.iterrows():
    #    full_profile_df.loc[len(full_profile_df.index)] = [row["Name"], row["Openness"], row["Conscientiousness"], row["Extraversion"], row["Agreeableness"], row["Neuroticism"]]

    return full_profile_df


def GenResponseColumns(num_columns):
    col_names = []
    for i in range(0, num_columns):
        str_num = str(i).zfill(2)
        col_names.append(f"r{str_num}")
    return col_names



# Get Personality Profiles and Test
personalities_df = GetPersonalityProfile()
questions_df = pd.read_csv(filepath_or_buffer=r"C:\Users\Jake Klinkert\Documents\School\HumIn Game Lab\APF\Papers\LLM\personality\notebooks\personality\tests\BIG5.csv", encoding='utf-8')

for model in model_name:
    if model != "text-davinci-003":
        continue
    # Set up the LLM
    llm = OpenAI(temperature=0.9, model_name=model, max_retries=256)
    prompt = TemplatePrompt()
    chain = LLMChain(llm=llm, prompt=prompt)




    # For each personality profile, take the test\



    # test the data frame that we got the personality profile infromation.
    profile_id = 0
    personality_name = personalities_df["Name"][profile_id]
    personality_profile = [personalities_df["Openness"][profile_id],
                        personalities_df["Conscientiousness"][profile_id],
                        personalities_df["Extraversion"][profile_id],
                        personalities_df["Agreeableness"][profile_id],
                        personalities_df["Neuroticism"][profile_id]]
    #print(f"personality_name: {personality_name}")
    #print(f"personality_profile: O = {personality_profile[0]}, C = {personality_profile[1]}, E = {personality_profile[2]}, A = {personality_profile[3]}, N = {personality_profile[4]},")


    #Get Sets of questions to ask the LLM
    question_2d_list = [[], [], [], [], []]
    question_list = []
    q_id = 0
    f_id = -1
    # The LLM will need to answer each question from the personality test
    for question_index, question_row in questions_df.iterrows():
        if q_id % 10 == 0:
            f_id += 1
        question = question_row["text"]
        question_2d_list[f_id].append(question)
        q_id += 1

    # For a personality profile, take the test sameple_size number of times
    sample_size = 64


    # LLM start taking the test
    for personality_index, personality_row in personalities_df.iterrows():
        table_name = personality_row["Name"]
        df_llm_response = pd.DataFrame(columns=["E", "N", "A", "C", "O"])
        
        for response_idx in range(0, sample_size):
            # Run LLM
            facet_response = []
            for factor_id in range(0, 5):
                result = chain.run(o = personality_row["Openness"],
                                c = personality_row["Conscientiousness"],
                                e = personality_row["Extraversion"],
                                a = personality_row["Agreeableness"],
                                n = personality_row["Neuroticism"],
                                questions = question_2d_list[factor_id])
                facet_response.append(result)
            df_llm_response.loc[len(df_llm_response)] = facet_response
            print(f"{model} - {table_name} - {response_idx} out of {sample_size}")

        
        df_llm_response.to_csv(f"C:/Users/Jake Klinkert/Documents/School/HumIn Game Lab/APF/Papers/LLM/personality/notebooks/personality/results/model_{model}_64/{table_name}_64.csv")


