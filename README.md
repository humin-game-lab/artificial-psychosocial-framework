# Artificial Psychosocial Framework

Author: Lawrence (Jake) Klinkert
Date: 04 May 2020

Diagrams - show the high overview of APF, how the emotions are generated using OCC, and how the Social Relation is updated from the generated emotions.

Experiment - The application that implements APF to test simple scenarios.  You perform actions onto an ANPC and see the emotional and social relations histories based on the personality that the ANPC has.

Static Lib - The static library that is used as a plugin for the Unreal Engine.

Unreal Game - An unreal project that uses the static lib and has example functions that call APF functions in blueprint scripting.

Video - My thesis presentation explaining what APF, how it works, and what it intends to provide for game developers.

Synopsis of my thesis:
My thesis focuses on the creation of human-like characters using affective computing. This type of computing involves designing algorithms in computer sciences that are based on concepts from psychology. Affective Non-Player Characters (ANPC) have the same functions as a Non-Player Character (NPC). However, ANPCs can simulate the process of generating emotions like humans. ANPCs are socially aware of other ANPCs, have intrinsic motivations, and can empathize with the player. 

This research intrigued me because I specialize in Artificial Intelligence (AI) and have an interest in Psychology. Games such as “Left 4 Dead” and “This war of mine” have had a lasting impact on me. These games, at their core, combine elements of psychology and AI to influence the gameplay. When developing a game, designers may wish to personify NPCs to react more like a human. However, programmers create NPC behavior based solely on logic. This research provides programmers tools to represent NPCs with a set of affective responses.

I researched this topic for six months and created a library in three months that is integrated into the Unreal Engine. From this project, I had a crash course in affective computing, learned the process of modelers creating a cognitive system, and able to connect a static library to an API. This changes the perspective of AI design from solving a problem from strict logic, to react and reason. Developing with ANPCs can lead to procedurally populating entities in environments, managing ANPCs through a traumatic event, choosing non-violent actions for exciting scenarios, animating facial micro-expression, and producing emotional prosody.


 