#pragma once
#include "Vocabulary.hpp"


namespace APF_Lib
{
	enum PatientType
	{
		PATIENT_UNKNOWN = -1,

		PATIENT_OBJECT,
		PATIENT_ANPC,

		NUM_PATIENT_TYPES
	};
	
	struct Event
	{
	public:
		int		m_owner = -1;
		int		m_actor = -1;
		int		m_action = -1;
		int		m_patient = -1; // This needs to be an Entity (ANPC or Object)
		float	m_certainty = 0.0f;

		PatientType m_patientType = PATIENT_UNKNOWN;

	public:
		Event() = default;
		~Event() = default;

		Event(const int owner, const int actor, const int action, const int patient, const PatientType patient_type,  const float certainty);

		std::string ToString();
	};
}
