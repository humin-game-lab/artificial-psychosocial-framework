#pragma once
#include "LibCommon.hpp"

namespace APF_Lib
{
	//----------------------------------------------------------------------------------------------
	enum PersonalityIndex
	{
		PERSONALITY_UNKNOWN = -1,
		PERSONALITY_OPENNESS,
		PERSONALITY_CONSCIENTIOUSNESS,
		PERSONALITY_EXTROVERSION,
		PERSONALITY_AGREEABLENESS,
		PERSONALITY_NEUROTICISM,
		NUM_PERSONALITY_VALS
	};

	
	//----------------------------------------------------------------------------------------------
	struct TraitPersonality
	{
		public:
			float m_traits[NUM_PERSONALITY_VALS] = { MIN_UNIT_VALUE };

		public:
			TraitPersonality();
			~TraitPersonality();
			TraitPersonality(const TraitPersonality& copy);
			TraitPersonality(float o, float c, float e, float a, float n);

			float &operator[] (int idx);
	};

	
	//----------------------------------------------------------------------------------------------
	enum SocialRoleIndex
	{
		SOCIAL_ROLE_UNKNOWN = -1,
		SOCIAL_ROLE_LIKING,
		SOCIAL_ROLE_DOMINANCE,
		SOCIAL_ROLE_SOLIDARITY,
		SOCIAL_ROLE_FAMILIARITY,
		NUM_SOCIAL_ROLE_VALS
	};

	//----------------------------------------------------------------------------------------------
	struct TraitSocialRole
	{
		public:
			float m_traits[NUM_SOCIAL_ROLE_VALS] = { MIN_UNIT_VALUE };
			 
		public:
			TraitSocialRole();
			~TraitSocialRole();
			TraitSocialRole(const TraitSocialRole& copy);
			TraitSocialRole(float l, float d, float s, float f);

			float &operator[] (int idx);
	};


	//----------------------------------------------------------------------------------------------
	enum AttitudeIndex
	{
		ATTITUDE_UNKNOWN = -1,
		ATTITUDE_LIKING,
		ATTITUDE_FAMILIARITY,
		NUM_ATTITUDE_VALS
	};


	//----------------------------------------------------------------------------------------------
	struct TraitAttitude
	{
		public:
			float m_traits[NUM_ATTITUDE_VALS] = { MIN_UNIT_VALUE };

	public:
		TraitAttitude();
		~TraitAttitude();
		TraitAttitude(const TraitAttitude& copy);
		TraitAttitude(float l, float f);

		float &operator[] (int idx);
	};
	
}