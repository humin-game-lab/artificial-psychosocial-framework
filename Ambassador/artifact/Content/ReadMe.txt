Author: Lawrence (Jake) Klinkert
Date: 04 May 2020

First Time build:
	- Right-click on PsychSim.uproject and select, "Generate Visual Studio Project Files."
	- Double Click on PsychSim.uproject
	- If you are prompted with, "The following modules are missing or built with a different engine version: PsychSim, Would you like to rebuild them now?" Select yes.
	- Otherwise, unreal should now open.

Example Data Set Files:
	- These example csv files are used for the vocabulary and relations.
	- These files follow the same data struct used in unreal to define the different vocabs and relations.

Update a Data Set file
	- Best to create the csv in excel, follow the example data set as a template. 
	- Export the file as a csv, and reimport the file into unreal.
	- The file should be placed in Content/RawData. 
		- If you are overwriting the file, and the file is the same name with the same layout, then nothing else is needed.
		- If this is the first time you are importing the file, or it is in a new location, then a prompt will ask you the import type (DataTable) and DataTable row type (RelSocial, VocObject, etc.).
	- Additionally, in Content/APF/APF_test_1.uasset you will need to change the default values of the respective DataTable to your new DataTable.

Updating the APF static library
	- Gather the .h/hpp and .lib files and place them in ThirdParty/APF_Lib/Includes and ThirdParty/APF_Lib/Libraries, respectively
	- Open PsychSim.sln, and rebuild the code with the new files.

Updating source code in unreal with new code from the static library
	- Ensure that the .h/.hpp and .lib files are in ThirdParty/APF_Lib/Includes and ThirdParty/APF_Lib/Libraries, respectively
	- In visual studios, ensure that the relative path for the includes is set. 
		- Right-click PsychSim, and select properties at the very bottom.
		- Go to Configuration Properties->VC++ Directories
		- Go to General->Include Directories
		- Make sure the relative path is ..\..\ThirdParty\APF_Lib\Includes
	- Now you can include the header files and call functions in the unreal scripts.







 