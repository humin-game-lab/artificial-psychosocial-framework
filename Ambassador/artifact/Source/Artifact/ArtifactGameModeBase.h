// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArtifactGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ARTIFACT_API AArtifactGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
