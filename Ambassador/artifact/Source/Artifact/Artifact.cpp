// Copyright Epic Games, Inc. All Rights Reserved.

#include "Artifact.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Artifact, "Artifact" );
