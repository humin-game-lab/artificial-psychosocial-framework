Core Values - Cultural and Personal Ideals
============================================

The 54 Univeral Value Cards
--------------------------------------------
>> Freedom | Creativity | Independence | Choosing own goals | Curiosity | Self-respect
>> -------|----------|-----------|--------------|-------------------|-----------------
>> An exciting life | A varied life | Daring | Pleasure | Enjoying life | Ambition
>> Influence | Capability | Success | Social Power | Wealth | Authority
>> Public image preservation | Social Recognition | National Security | Family Security | Sense of Belongning | Social Order
>> Health | Cleanliness | Obedience | Humility | Moderation | Spirituality
>> Meaning in life | Inner Harmony | Detachment | Helpfulness | Responsibility | Forgiveness
>> Honesty | Loyalty | Mature Love | True Friendship | Equality | Intelligence
>> Wisdom | A world of beauty | Social Justice | Broad-mindedness | Protecting the Environment | A world at peace
>> Self-discipline | Politeness | Honoring parents and elders | Respect for Tradition | Devoutness | Accepting one's portion in life
> 
> Select top six <br>
> Rank in order of 1 to 6 <br>
> Define each for you <br>
> Give examples <br>
> Ask oneself is this an actual value or an aspired value <br> 
> 
> Dr. John Thompson,
> Professor of Global Leadership,
> Oral Roberts University

Application
-------------------------------------------
Selecting core values from the cards above will help with world building and character development.
 - Three core values chosen for the entire fictional country's culture.
 - Two additional core values each for the various subcultures.
 - One final personal value for each NPC from a shorter list of 5 or so values.
 
 <br> These choices factor into the game rules and mechanics: these values are embedded in the NPC's decision making in response to events. Societal and subcultural values factor into the "cultural tendencies" module of decision making, whereas the individual vlues factor into "outcome awareness."<br>

### The entire society
Three core values for the entire culture of the world that the player visits will be chosen. These choices are subject to change:
- Helpfulness
- Moderation
- Mature Love

### The three subcultures
#### Subculture A
- Intelligence
- Broad-mindedness

#### Subculture B
- Social Order
- Ambition

#### Subculture C
- A sense of belonging
- Respect for tradition

### Individual NPC's
Select one randomly from:
- A varied life
- Social recognition
- Curiosity
- Honoring parents and elders
- A world at peace

Values Defined
--------------------------------
These values manifest in the following ways. **Key aspects are bolded.** _Shadow aspects are italicized._<br>
Note the difference between cultural _integration_ and _accommodation_.

### Helpfulness (entire society)
Making others look good, **practicing medicine**, "it takes a village to raise a child" mindset, giving emotional support, **confiding in someone you trust and being a trustworthy confidant**, **social movements**, prioritizing utility, practicality, _doing something unethical for someone else's sake_.

### Moderation (entire society)
Distaste for extremes, balanced thought, **centrism**, distaste for gluttony and excess, caution toward aesceticism, desire to find common ground, playing the middleman, **reaching compromise**, communicating diplomatically, fearing radical ideas, **good at _accommodating_ foreigners & welcoming newcomers**.

### Mature love (entire society)
Gratitude, compassion, acceptance, patience, **native proverb: "like wax to a flame, lovers adapt"**, overlooking mistakes, accepting flaws, **sustaining relationships**, appreciating passion's afterglow, appreciating shared memories, **remembering first impressions & watching them evolve**, cyclical blooming & withering of intense feelings.

### Intelligence (subculture a)
Academic achievement, vast knowledge, accuracy, sleuth, pattern recognition, _cleverness_, invention, jokes & wordplay, **literacy for everyone**, uncovering truth, appreciating teachers, not revealing one's cards, emphatic when saying what is certian, **curiosity**, _setting traps for others_, tactfully correcting people.

### Broad-mindedness (subculture a)
Openness to perspectives, **openness to new experiences**, a balanced mindset, _desiring omniscience_, _can come off condescending_, **creating the "rennaissance person"**, independent thinking, it's okay to disagree with authority, listening more than speaking, finding the truth for oneself, surrendering easily in arguments, **does not easily totally _integrate_ foreigners**, **remains curious toward a foreigners's different customs and ways of existence**.

### Social order (subculture b)
**Social stability**, heirarchy, political effectiveness, **justice**, social institutions like marriage and parental authority, **loyal friendship**, parental values imposed on children, authority of government, **stasis of law**, _criticism of new ideas_, no-nonsense attitude, **resists totally _integrating_ someone of another culture**, **happy to _accommodate_ someone of another culture**.

### Ambition (subculture b) 
Guts, _opportunism_, swiftness, **determination**, merit, high acheivement, taking risks and learning from failure, tolerating failure when the goalposts are set high, distaste for sloth and laziness, going against the grain of what's easy, **not surrendering easily**.

### A sense of belonging (subculture c)
Community, **adopting and fostering**, family, **fulfilling one's destiny**, **respecting differences**, traveling and seeing the world to find one's place in it, awe for the wider world, has an age-old debate whether one can or should fully integrate with another group vs returning to one's roots, sees youth as an opportunity to break free, appreciates finding oneself, **harmony with nature**, contentment, not reaching for more than one is likely to attain, **happy to _integrate_ foreigners**, maintains clear boundaries between groups which may not be blurred, sees wishy-washy personal identity as a recurring problem.

### Respect for tradition (subculture c)
Reverence, festivity, museums & temples, honoring the dead, **family business & trades**, preservation of myths, **respecting other traditions**, distaste for appropriation, _justifying old ways_, awareness of the origin of things, expecting wisdom from the elderly, **takes good care of the elderly**, preserving architecture, **intellectual conservativism rather than original thought**, belief in cyclical historicism, has a saying that "we must learn from history in order to repeat it properly".
