Meeting Design Doc
========================

A meeting is composed of beats.<br>
Each ANPC initiates one beat per meeting.<br>

Beat Structure
------------------------

Beats are broken down into bits:
1. Initiation.
2. NPC/ANPC Response 1
3. NPC/ANPC Response 2
4. ...
5. NPC/ANPC Response N

<br>After a human player or ANPC hears a fact in bit, they may
press a button to "remember" it, adding it to their arsenal 
of statements. The truth value of these facts will be marked 
with "???".
<br>Facts captured this way go into a "watch list" that includes an 
ordered list of ANPCs/NPCs who stated the fact & where, in temporal order 
(ordered by when the statement was heard in a meeting by this player).

### Initiation Structure

See meeting_response_design_doc.yaml, section one.

### Response Structure

See meeting_response_design_doc.yaml, section two.
