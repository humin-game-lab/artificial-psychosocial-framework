Inspiration for *The Ambassador*
======================================

Mood
--------------------------------------

I have a name for myself in two languages:
one I was born with, and one I went out into the world and earned for myself.

Quotes
--------------------------------------

> One wonders if we travel to find ourselves, or to escape ourselves.
>> Or to remember who we are and what's important.

Anonymous, Facebook, 03/17/2022

Memory Trinkets
--------------------------------------

- The amount of effort and focus it took at first to sit down and read the labels in a foreign language enough to use a household appliance.
- Green light? Nay, "blue light" means go. It would look green to us, though.
- Returning to the United States and feeling irritation and disgust toward all the sugary foods in the American diet.

Stories from abroad (Tokyo, Japan)
--------------------------------------

### Crossing Traffic

Everything has cultural implications in a foreign country. Even crossing traffic has cultural implications.

In Japan, pedestrians are expected to wait until the signal for a street crosswalk even if there are no cars coming.
I remember a night when there was scarcely anyone out on a certain road. It was just me and another, older foreigner, and I needed to cross. I hesitated, and then did. The other man was visibly disappointed by my actions.

As a foreigner in Tokyo, "a model megacity", it's easy to feel obligated to be on one's best behavior at all times. With this pressure to live up to the city's standards, I felt honored to be there, working and living and taking part in it.

References
--------------------------------------

### Gameplay References
#### *Fire Emblem: Three Houses*
#### *The King's Delimma*
#### *Quill*

### Other Media
#### James Joyce's *Dubliners*
#### George R. R. Martin's *Game of Thrones*
