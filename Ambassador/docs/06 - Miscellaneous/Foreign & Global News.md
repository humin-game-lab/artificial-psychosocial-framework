Foreign & Global News
============================

The Japan Times
----------------------------

### U.S. envoy to Japan set to attend Rainbow Pride parade to highlight LGBT rights
April 23, 2020
https://www.japantimes.co.jp/news/2022/04/23/national/politics-diplomacy/rahm-emmanuel-lgbt-rights/

United States ambassador to Japan, Rahm Emanuel will speak at the pride parade march in Tokyo.

> By speaking at the march as an ambassador, Emanuel said he wants to stress what the U.S. values - "the values of inclusion, and creating a larger family and a sense of community. Everyone has something to contribute."

### Japan and U.S. finance chiefs confirm existing agreements as yen plunges
April 22, 2022
https://www.japantimes.co.jp/news/2022/04/22/business/suzuki-us-yen-moves/

Inflation soars in the United States, while the JPY remains weak against the dollar. Japanese trade with the United States becomes more difficult despite Japan's reliance on foreign trade, so corporate profits and household budgets in Japan are negatively affected by inflation in the United States.

> Still, the [incumbent] Liberal Democratic Party faces problems if the central bank policy divergence continues to weaken the yen with an election due in the summer.

### Magic: The Gathering taps into Japanese pop culture
April 16, 2022
https://www.japantimes.co.jp/culture/2022/04/16/general/magic-the-gathering/

Magic: the Gathering remains a popular import in the Japanese trading card game scene. A recent block of expansions, the Kamigawa: Neon Dynasty, sets Japanese legends in a cyberpunk world.

> Yokohama became the first non-U.S. city to host the world championships in 1999, and Japan has hosted 73 of the game's Grand Prix events, drawing praise for its players' manners at the table even in the heat of competition.

### TELL Japan hopes to 'build resilience' with mental health summit
April 1, 2020
https://www.japantimes.co.jp/community/2022/04/01/issues/tell-japan-hopes-build-resilience-mental-health-summit/

> Friday's main speaker is Kathy Matsui, well-known for her research and expertise on gender diversity and the issues facing women in Japan. The economic and psychological burden of the pandemic has been particularly bad on women, such as mothers who were forced to cut back or give up their work in order to care for children when schools went online.

### Japan and Russia strike deal on salmon and trout fishing, despite sanctions
April 23, 2020
https://www.japantimes.co.jp/news/2022/04/23/business/japan-russia-salmon-fishing-deal/

Russia will recieve a fee of between 200 and 300 million yen for Japanese fishing near Hokkaido for fish spawned in Russian waters.

> "This is part of Japan's cooperation in resource management undertaken by Russia," an agency official said. "I don't see any problem with it."

Japan Today
--------------------------

### Kyocera to build its largest plant in Japan
April 23, 2022
https://japantoday.com/category/tech/kyocera-to-build-its-largest-plant-in-japan-increasing-production-of-semiconductor-components

Kyocera will build a facility to produce organic semiconductor and crystal devices as global need arises from smart vehicles, self-driving cars, 5G communication, and other digital products. The facility is scheduled to open in October 2023.

> A signing ceremony held April 20 was attended by Kagoshima Governor Koichi Shiota, Satsumasendai City Mayor Ryoji Tanaka, and Kyocera officials.

### Group of over 100 lawmakers visit Yasukuni shrine
April 22, 2022
https://japantoday.com/category/politics/group-of-over-100-japan-lawmakers-visit-war-linked-yasukuni-shrine

Many Japanese politicians, including Prime Minister of Japan, Fumio Kishida, paid respects to the Yasukuni shrine for its spring festival. Officially, the Liberal Democratic Party stated that the political visitors "prayed for world peace that is on the precipice" in response to the Russian invasion of Ukraine.

> Yasukuni has long been a source of diplomatic friction with China and South Korea as the shrine honors convicted war criminals along with millions of war dead, and Kishina is not expected to visit the site during the festival period through Friday.

Al Jazeera
------------------------

### Photos: The 3-2-1 sports museum opens its doors in Qatar
April 23, 2022
https://www.aljazeera.com/gallery/2022/4/23/photos-3-2-1-qatar-olympic-and-sports-museum

Qatar, set to host the next World Cup, opens the doors of a sports museum designed by a Spanish architect.

> Sheikha Al-Mayassa bint Hamad bin Khalifa Al Thani, chairperson of Qatar Museums (QM): "Culture and sports are two sides of the same coin, and there is no better time to celebrate our nation's investments in both."

### Earth Day: Jordan farmers frustrated over shrinking Dead Sea
April 21, 2022
https://www.aljazeera.com/news/2022/4/21/earth-day-jordan-farmers-frustrated-over-shrinking-of-dead-sea

With the Dead Sea losing three feet of water per year, sinkholes and water deprivation threaten the agricultural workers of Jordan, one of the most water scarce countries in the world, according to the United Nations. An expert, prof Nizar Abu Jaber, attributes the Dead Sea shrinking more to the headwaters of the Jordan River having been diverted by Israel, Jordan and Syria than to increased evaporation from climate change.

> "Our land is destroyed. We tried to look for solutions but did not find any. No one helped ... not even the ministry of environment helped us," [Al-Habashna, agricultural worker] says.

### Egypt cuts TikTok influencer sentence to three years
April 18, 2022
https://www.aljazeera.com/news/2022/4/18/egypt-cuts-tiktok-influencer-sentence-to-3-years

Haneen Hossam's 10 year sentence was reduced, and she was given a 200,000 Egyptian pounds fine. She had been arrested in 2020 for posting a video to Instagram explaining how women could earn money by uploading videos to Likee.

> The targeting of female influencers has rekindled a heated debate in Egypt over what constitutes individual freedoms and social values.

> "There are real and serious cases of human trafficking that must be prosecuted - these TikTok cases are not it."

### Protest forces Libya's national oil firm to close Al-Fil field
April 17, 2022
https://www.aljazeera.com/news/2022/4/17/libya-oil-firm-announces-closure-of-major-oil-field

Protestors at arrived at an oil site and caused production to stand still "until a government appointed by parliament takes office in the capital". Last month, another shutdown brought on by an armed group occurred elsewhere in Libya at its largest oil plant.

> Sunday's forced closure at the Al-Fil oil field comes as the Russian invasion of Ukraine has rattled markets worldwide, causing crude oil prices to soar above $106 per barrel.


Doha News
--------------------------

### Joe Biden nominates new United States Ambassador to Qatar
March 20, 2022
https://dohanews.co/joe-biden-nominates-new-united-states-ambassador-to-qatar/

Biden nominated Timmy T Davis as the new United States ambassador to Qatar, a major non-NATO ally of the United States.

> Qatar has enjoyed its mediating role for the US involvement in several regional issues including the recent Afghanistan escalations and the Iranian nuclear deal or the Joint Comprehensive Plan of Action (JCPOA).

> Dr Trita Parsi ... told Doha News that "Quatar has played an important role in seeking to not only defuse US-Iran tensions around the JCPOA, but also when it comes to a prisoner exchange between the two sides."

### Beyond the words: the role of Palestinian poetry in resistance
April 17, 2022
https://dohanews.co/beyond-the-words-the-role-of-palestinian-poetry-in-resistance/

> As a result of her poem going viral, she was detained and imprisoned by Israeli Occupation Forces for five months. Poetry is clearly deemed a threat, but there is nothing criminal about Palestinian resistance literature... except the occupation it recites," said Al Bast.


