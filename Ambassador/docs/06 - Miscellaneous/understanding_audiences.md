Understanding Audiences
======================================

In order to create the experience of culture shock, we must first understand the cultural reference point that most members of the audience will bring with them into the game world.

The fictional culture of *The Ambassador* will be queer to a Western audience, particularly people from the United States.

Cultural Touchstones
--------------------------------------

### Improv Comedy
A unique theatrical theory and practice to come out of our land, the "watch us all come out on Friday nights and wing it" artform.

Of course, rehearsal and craftsmanship goes into improv performances in reality, most of the time. As an improviser myself, I can say that what is considered "quality" on an improv stage is just different from any other storytelling. The joy of watching a show comes from tracking and identifying with the improvisers themselves, and not the characters.

Improv is a formula for wit, a method for learned spontenaeity, a way to simply make connections. It shows that, as a society, we value freedom and making shit up. We believe in happy endings. 

### Jazz Music
Jazz music and especially jazz chords extended classical music theory, and there's no going back. Modernist musical movements have not really tried to compete with it, preferring to innovate through atonal and abstract methods that break from tradition as opposed to enriching the tonal and frankly, musical qualities of music.

It's easy to argue that jazz music says more about the African American subculture of the United States than it does about our culture as a whole. I'm not qualified to say what it shows about the subculture that created it. I can say that despite racial discrimination in early 20th century America, jazz music was so great that white audiences could not help but listen.

Maya Angelou's famous quote, "the caged bird sings of freedom" brings us back to our core cultural value, even in a social situation where freedom was a mere dream and not a reality.

Objective
--------------------------------------
The fictional culture of *The Ambassador* should be queer to a United States audience. There is a certain not-quiteness.

So if freedom is one of our core values, the society should have a similar concept, albeit one that is fundamentally different, composed of different pieces. From Google, "free" comes from old English "freo" and originally an Indo-European root meaning "to love" and this is a shared root with the word "friend". A friendship is a loving and at-will connection between two parties.

What is the equivalent? Say that the fictional culture has not the word "freedom" but the concept of "happy-choice". How attainable is that to the people living in this world? How desirable is it?
