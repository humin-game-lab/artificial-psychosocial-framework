Feature Tier List
============================
A tiered list of features for progressive versions of a complete game. Organized by game system. Systems include:
- Overworld: (secondary game loop) ANPC's and NPC's movement across the game board.
- Local Scenes: (primary game loop) Information spread around at social gatherings by actions taken by ANPC's and NPC's.
- Messaging: The dialogue options for ANPC's to communicate with each other through fixed statements.
- Expressions: Emotional information (OCC) conveyed by ANPC's via icons at various times.
- Misc: (tertiary game loop) Everything else needed to make the game work, and additional one-off features, if any.

First Tier
----------------------------
Everything needed for first playable.

### Overworld
ANPC players can move around the level. The levels can be created in the Unreal 5 level editor. Levels built out with Unreal assets.

### Local Scenes
ANPC players can take basic speech actions: praise politician/praise ambassador/dock ambassador.

### Messaging
ANPC's can communicate. They can decide to work together, agree to a movement plan, and role claim.

### Expressions
Expressions occur in response to the basic speech actions (having your politician praised, having your ambassador praised or sabotaged). Also in response to messaging communication directly & expected behavior based on messaging communications (working together, movement plan, role claim). Expressions can be bluffed.

### Misc
Setup. Guessing. Scoring. End.


Second Tier
----------------------------
Major features that expand the systems behind core game loops.

### Overworld
Line of sight & occlusion.

### Local Scenes
Scenes built out with Unreal assets. All initiation options. Most response options, except "and...", "but...". The ability for ANPC's to pick up points of information they hear in a scene and repeat it later. 

### Messaging
ANPC's can communicate plans to sabotage another ANPC player together.

### Expressions
Expressions occur in response to new local scene options (same types as before). Expressions occur in response to new messaging options (sabotage). New expressions can be bluffed.

### Misc
Briefs: players are given report info about other ambassadors and politicians at the start of the game.


Third Tier
----------------------------
Core game loop systems completed. Supporting systems expanded.

### Overworld
NPC's can move around the map. Quality of life improvements for the level editor.

### Local Scenes
NPC's participate in dialogue. They can repeat what they hear. They come into the world with some knowledge about politicians (occasionally misinformation).

### Messaging
ANPC's can communicate speculations and unverifiable claims about what they saw happen previously.

### Expressions
Demeanor: each ANPC player chooses a demenaor at the start of the game (level of positivity, level of negativity). 
<br> Expressions in response to NPC's statements in local scenes.

### Misc
Characterization (an abstraction of demeanor) is shown to all players throughout the game.


Fourth Tier
----------------------------
Supporting systems completed.

### Overworld
Ability for ANPC's to place bugs when they pass by other ANPC's.

### Local Scenes

### Messaging
Indication that a conversation is being bugged. Ability to speculate who is listening. Gossiping overheard messages.

### Expressions
Expressions in response to new messaging options (speculating the bugger, gossip).

### Misc


Fifth Tier
----------------------------
Polish, polish, polish.
