Feature Tier List
============================
A tiered list of features needed to have a complete prototype. Organized by materials that need to be prepared.
<br> [1] Info to be penciled in by the player as they go.
<br> [2] Info to be filled in by the storyteller in between rounds.
<br> [3] To be placed on the table as they come into play and removed as they are no longer needed.

First Tier
----------------------------
Everything needed for these features:
- Let the player ask about world events.
- Let NPC's ask the player for help.
- Let the player sign deeds.

### A Guide to World Events
Tells the storyteller which world events to trigger in between rounds.
<br> The storyteller will inform the player of all triggered world events at the start of each round and the player will copy these to a space on their Conversation Options Menu. (First Tier has no other way for the player to discover them.)
- ID Number and Name of the world event
- Conditions to trigger
    - Start of round X
    - Roll a D6 at the start of a round. Trigger if >= 5.
    - If there are at least X NPC's with problem Y.
    - A particular deed has been signed.
    - etc.
- Information on Personal Events it Triggers
    - Who to affect with a personal event
    - Description of the personal event
    - Outcome for the character effected
    - Was the outcome good, neutral or bad for that character?
- Instructions on What to Do When it Triggers
    - Fill out paperwork for each personal event that the world event triggers.
        - 1) Write down the (personal event/outcome) pair on the Conversation Script for this character.
        - 2) Write down the (name of NPC affected/good or bad/the outcome) triple on the Conversation Script for each NPC this character confides in - referring to the Confidant Graph.
        - 3) If the outcome was bad, fill out a Commitment Note for this NPC and each NPC they confide in to put in those decks.


### Menu Options for Conversation
- A question for the player to ask NPC's
    - Ask about a world event
        - Space on the sheet for a list of world events [1]
- Responses to a question type that the player will be asked by an NPC
    - Being asked if the player can do anything about a problem.
        - Options for different levels of commitment to help.
        	- Decline to commit (put the Commitment Note back in the deck)
        	- Circle one of the following commitment options and put the note next to the Character Card, face up.
        	    - 1: "I'll see what I can do."
        	    - 2: "Say no more, I will make it happen."

### Conversation Script for the Storyteller
One for each NPC - it tracks data.
- A question type for the NPC to ask the player
    - "Can you do anything about...?"
    - Draw a Commitment Note from the pile next to this NPC's Character Card
- An answer guide to a question the player can ask the NPC
    - "So how's everything been going since [world event supplied by the player]?"
    - A cascading list of response types: answer with the first applicable
        - "Well, since [world event supplied by the player], [personal event that affected this NPC as a result]. Now, [outcome for this NPC]."
            - A list of (personal event/outcome) pairs, indexed on world event [2]
        - "Oh, it was [good/bad] for [other NPC that confides in this NPC]. [Outcome for the other NPC]."
            - A list of (other NPC/good or bad/the outcome) triples, indexed on world event [2]
        - "I haven't been bothered by it."

### Character Cards [3]
- Name
- Personal Info
    - List of Social Roles (teacher, widower, parent, etc.)
- Social Stats (NPC --> Player)
    - Solidarity Level (-20 to 20)

### Commitment Notes [3]
Place face down in a stack beside a Character Card. Draw from the pile when this character wants to ask the player to do something about a situation. Mark it with the player's commitment level, and place it face up by the character.
- Description of the problem.
- Whose problem it is.
    - Name of a specific character, or social role group, or "general problem"
- Commitment level promised. [1]

### Confidant Graph
For the Storyteller's eyes only.
- Each NPC's name circumscribed in its own node
- Arrows from one NPC to another, indicating that the first confides in the second
- Double-sided arrows between NPC's, indicating that they confide in each other

### Deeds
All deeds start out available to the player and unsigned.
<br> Let the player sign them and move them to a "signed" pile.
- Deeds that benefit the player's home country or satisfy its interests.
	- Cost to this country. (Inbound negotiation points.)
- Deeds that offer something of popular value from the player's home country to this country.
	- Cost to the player's home country. (Outbound negotiation points.)
- Deeds that satisfy the interests of a particular party.
	- Cost to the player's home country. (Outbound negotiation points.)
	- Cost this country. (Inbound negotiation points.)


### Deeds Consequence Guide
For the storyteller's eyes only.
- Tells what problems to alleviate when a deed is signed by the player.

### Rulebook
- General flow of the game
- How many deeds a player may/must sign per round:
	- Spend negotiation points to earn negotiation points.
		- The player has an allowance of points to spend helping this country per round.
			- Inbound negotiation points <= MAX
		- The player can sign deeds recieving things not exceeding the points spent giving help.
			- Outboud negotiation points <= Inbound negotiation points
- End-of-round instructions
    - 1) Trigger world events. Refer to the World Events Guide.
    - 2) Follow through on the world events' consequences as detailed in the guide.
        - Trigger personal events.
	        - Fill out data on the Converation Scripts of the affected character and each NPC they confide in.
	        - Add new Commitment Notes to the decks by the Caracter Cards.
    - 3) Check the Deeds Consequence Guide to remove and resolve Commitment Notes in play.
- Instructions on how to adjust Solidarity when a commitment is promised or fulfilled
    - If level 1: "I'll see what I can do" - give the player +3 Solidarity when the problem goes away
    - If level 2: "Say no more, I will make it happen" - give the player +5 Solidarity when the player commits. Reduce Solidarity at the end of every round by -1 if the problem still exists.

Second Tier
----------------------------

Third Tier
----------------------------

Fourth Tier
----------------------------

Fifth Tier
----------------------------

