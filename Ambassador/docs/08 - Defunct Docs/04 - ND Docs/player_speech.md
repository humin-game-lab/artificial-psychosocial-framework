Player Speech
==========================

**NOTE:** It's probably best to drop this from the design. Since the tertiary loop is more of a strech goal, it's best not to have any aspect of the game depend on its inclusion. (However it would still be great find ways to make player speech imperfect at times to capture the feeling of communicating in a foreign language.)

Imperfect Speech
--------------------------

The player articulates themself differently depending on how proficient (leveled up) they are in a dialect.
The point is to capture the feeling of awkwardness around practicing a foreign language.

Speaking more proficiently with an NPC boosts the player's Solidarity stat with that character and to a lesser extent, their subgroup.

### Tiers

#### Unintelligible Language - MINIMUM
Always negative impact on solidarity.
- How go you never being that to me?

#### Broken Language - MEDIUM
Positive impact on solidarity up to a point, then no impact on solidarity.
- Go down and ask him: "Don't put your hand in."

#### Very Good (still imperfect) - MAXIMUM
Always positive impact on solidarity.
- I will come by with the invitation and drop it at your house.

### Mechanics and Interface

There could be some randomness in how well the player is able to articulate themself in a given moment.
Dialogue choices that the player is likely to say unintelligibly appear in red.
Dialogue choices that the player is likely just get their point accross with appear in yellow.
Dialogue choices that the player is likely to communicate effectively appear in green.
(These colorizations may be discreet or continuous based on the player's stats - however the final speech output always lands in one of the three discreet tiers.) 
This would be similar to charisma checks for dialogue options in *Fallout 4*.

After the player's interaction with the NPC's is designed and templated, will have to come back and decide whether the speech check will be affected by the complexity of what the player is trying to say.
