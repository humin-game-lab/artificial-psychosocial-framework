Design Document for NPC Conversations
=======================================

Conversation Flow
---------------------------------------
Standard flow:
1. Greeting
2. Player or NPC Initiated Segment
3. Player or NPC Initiated Segment

There is typically one player-initiated segment and one npc-initiated segment per conversation. Which comes first 
depends on the npc's level of "dominance" toward the player. At very high levels of "dominance" the npc may get an additional 
npc-initated segment; at low levels of "dominance" the player may get an additional player-initiated segment.

Greetings
---------------------------------------
The player may reingage with the same NPC multiple times per day. However the greeting segment with that NPC only happens once per day.
It gets skipped in follow-up conversations.

~~Greetings are where the player is offered different greeting options of varying levels of complexity. A skill check based on the 
player's language proficiency is carried out. More complex greetings are harder for the player to execute fluently, and simpler greetings
are easier to execute. The more complex the greeting, if successful, the more rapport is built with the NPC. Rapport is always lost for 
unintelligible greetings.~~

Greetings start simple for NPC's that the player has yet to become acquainted with. "Good day." "Nice weather, isn't it?" and the like.
Once the player has a few conversations with an NPC, they are offered new options based on past conversations. For example, if an NPC told the player that they were recently unemployed, the player may open with, "Hey, how's the job search going?" Or if an NPC told the player that they're married to another NPC, the player could open with, "How's so-and-so doing?" 

In general, there are a few types of special greetings that the player can get:
- Following up on an unresolved personal event.
- Asking about another NPC that this NPC has a positive relationship toward.

Player-Initiated Segments
---------------------------------------
There are a few types of questions that the player may ask an NPC: 
- Ask about a world event.
- Ask about that NPC's relationship with another NPC.
- Bring up an NPC's action that the player heard about.

The player first chooses the type of question they want to ask. Then they fill in which world event, or which other NPC they want to ask about.

### Asking about a World Event
At low levels of rapport, the NPC will give the player a general sense of their personal feelings without going into detail. This could include a primary
and secondary emotional response.

With a moderate level of rapport, the NPC will give the player:
- A description of their own personal event caused by the world event and the outcome for themself.
- (If no personal event is associated with this NPC) They'll tell you about a personal event for someone that treats this NPC as a confidant.
  - Just the outcome, not a description of the event. The player will have to ask that NPC about it directly to learn more.
- (If none of the above exist for this NPC) A prospecive consequence, hope/fear, for a group this NPC is affiliated with.
  - AND (If this NPC has strong "solidarity" with the player) Any prospective group response that this NPC's group is considering or has planned.
- (If still none of the above apply) A prospective outcome, hope/fear, for another group.

### Asking about another NPC
#### Rapport Level 1
They'll tell you how familiar they are with this other NPC and how they know each other.
#### Rapport Level 2
If this NPC has weak "familiarity" with the other NPC, they will give the player their attitude relation (original biased thought from APF) toward that NPC.
<br> If this NPC has strong "familiarity" with the other NPC, they will tell the player about their "liking" toward that character.
#### Rapport Level 3
If this NPC has primarily been an onlooker to the other NPC, they will give an overall (average) approval/disapproval toward that NPC's actions.
<br> If this NPC has primarily been a confidant of the other NPC, they will expresss their level of "solidarity" with that other NPC.
#### Rapport Level 4
The NPC will tell the player about a specific action of the other NPC, giving their memory of the event and an expression of admiration/reproach,
<br> OR if they are a confidant, they may alternatively tell the player about a strong emotion that the other NPC was feeling recently and
a hint about the personal event that brought about that emotion (the player will have to go talk to that other NPC to find out more about why 
the other NPC had been feeling that way).
#### Rapport Level 4+
Rapport is at Level 4 and the NPC also feels strong "solidarity" towards the player.
<br> At this level they will let the player know about any planned or prospective group response that this NPC shares with the other NPC.

### Bring Up an NPC's Action that the Player Heard About
This option is not available at low levels of rapport.
<br> If the player has at least moderate rapport with this NPC, the following algorithm applies.
- If the NPC knows about this action...
    - If the NPC was an onlooker for this action...
        - This NPC will express their approval or disapproval of the action.
        - As justificaiton, this NPC will allude to their personal values.
    - If the NPC heard about the action by another NPC confiding in them.
        - This NPC will express their approval or disapproval of the action.
        - As justificaiton, this NPC will express being happy for or upset about the outcome this action had for the NPC that confided in them.
    - If the NPC knows about this event because the NPC who did it confides in them...
        - If this action is deemed to have negative consequences overall...
            - This NPC will give an excuse for the other NPC, saying what event caused them to do it.
        - If this action is deemed to have positive consequences overall...
            - This NPC will affirm with pride that the other NPC did take this action.
- If the NPC does not already know about the action...
    - The NPC expresses that this is new information, and casts approval/disapproval on the action for the player to hear.

NPC-Initiated Segments
---------------------------------------
In these segments, NPC's try to ascertain the player's identity, motives, perspective, and intentions. This is where the NPC reacts emotionally
to the player's choices (OCC model) and the social stats are most affected as a result (LDSF model).

Types of questions that the NPC may ask the player:
- Are you associated with or the cause of [world event]?
- How do you feel about [someone's response to an event]?
- What would you do if [hypothetical or actual personal event]?
- Can you do anything about [a group's outcome to a world event]?
- ~~Would you support [a group response]?~~

LDSF Conversational Influences
---------------------------------------
LDSF: Liking, Dominance, Solidarity, Familiarity

"Rapport" is a product of Liking and Familiarity. It affects how willing the NPC is to share personal details with the player.
<br> Solidarity further affects the NPC's willingness to share confidential information that have to do with the NPC's group affiliations.
<br> Dominance impacts the flow of the conversation as described above.
