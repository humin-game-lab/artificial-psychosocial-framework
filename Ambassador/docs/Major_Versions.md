Major Versions of *The Ambassador*
==================================
Where are we going, where have we been? Already the game has been through a variety of conceptualizations that span multiple genres, each building upon the last. Development has been driven by *theme* above all else - the pillars of culture shock, soft positional bargaining, and utilizing APF came together in pursuit of this character, the ambassador.

First Concept
----------------------------------
The initial GDD was a pie in the sky. It incorporated as many as four interconnected styles of gameplay: conversations with NPC's in the game world, composing letters, negotiating diplomatic treaties, and scheduling activities to fill the calendar.
<br> It was a little bit of this, a little bit of that: *FE: Three Houses*, *Persona*, *The King's Dilemma*, and *Quill* inspired a blend of elements that helped us envision an entire AAA experience that was novel in some ways and derivative in others.
### Key Associated Documents
- GDD_TheAmbassador_v0.5.pdf
	- The Ambassador - Systems Diagram

Second Concept - An Interactive Fiction
----------------------------------
After the GDD was created and we defined our core themes & inspirations, development went forward to flesh out the core gameplay loop, which was traversing the city and conversing with NPC's about their daily lives. Plotlines were drawn out. Features for elaborate, procedural NPC dialogue were planned and triaged. The game was taking shape as an almost purely narrative experience; it was headed towards becoming a socio-cultural commentary like James Joyce's *Dubliners*.
<br> 
### Key Associated Documents
- conversation_design_doc.md
- Event_Planning.xlsx
- Event_System_Diagram.png
- Feature Tier List.md

Third Concept - First Playable - A Hidden Identity / Social Deduction game
----------------------------------
A hard and fast pivot was made when stakeholder expectations for focusing on gameplay over storytelling became clear. Drawing upon the same systems and guided by our inspiration and research, a flurry of design quickly birthed a new pen & paper prototype.

Fourth Concept - Towards a Digital Version - Strategy with Political Intrigue
----------------------------------
A future goal at the time of writing. As we move from pen & paper into the digital realm, we will have to address certain challenges with the social deduction game: namely the absence of other players, and the conveyance of information to the player as diagetically as is feasible. The digial version of the game cannot be a 1:1 port. Even so, we will keep the same systems in tact and only modify them when opportunites to utilize APF present themselves, and then sparingly.
<br> Our focus when making the leap into the digital realm will be twofold. 
* Firstly, we will remove some of the abstractions from the first playable prototype. Most crucially, conversations with NPC's should play out. This will mainly consist of more narrative groundedness to the deeds and events that trigger NPC memories, although it could also involve some light procedural conversation design centering around the persuasive act.
* Secondly, we will have to pivot away from behavioral analysis which is the joy of multiplayer hidden identity games and lean into strategic thinking, critically, without adding new systems. Other ambassadors still can and should antagonize or cooperate with the player, but less often. The player's focus needs to shift from the other ambassadors to the (modestly) complex lives of the civilian NPC's.

Overall, with these two focuses guiding the digital migration, the pace of the game will slow down compared to the board game and the world will come alive to the player.
